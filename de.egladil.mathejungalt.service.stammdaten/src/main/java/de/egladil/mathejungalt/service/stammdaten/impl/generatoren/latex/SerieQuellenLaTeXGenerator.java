/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.Messages;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.MatheAGGeneratorUtils;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.contentprovider.AufgsItemTableContentProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.contentprovider.TableContentProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.BlankTableRowLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.latex.LaTeXQuelleTableLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.latex.QuellenitemsTableCellLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils.LaTeXEmptyHeadingsDeclarationGenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.parts.TextTable;

/**
 * @author winkelv
 */
public class SerieQuellenLaTeXGenerator extends AbstractSerieLaTeXGenerator {

	/** */
	private TextTable table;

	/**
	 * @param pSerie
	 * @param pFontSizeConfiguration LaTeXFontSizeConfiguration
	 */
	public SerieQuellenLaTeXGenerator() {
		super();
		setPagestyleEmpty(true);
	}

	/**
	 *
	 */
	protected void init() {
		if (isInitialized()) {
			return;
		}
		table = new TextTable(1);
		table.setHasHeader(false);
		final LaTeXQuelleTableLabelProvider quelleTableLabelProvider = new LaTeXQuelleTableLabelProvider(Messages.getString("font_quellen"));
		table.setTableLabelProvider(quelleTableLabelProvider);
		table.setCellLabelProvider(new QuellenitemsTableCellLabelProvider("\\n"));

		TableContentProvider tableContentProvider = new AufgsItemTableContentProvider(getSerie(), new int[] { 1, 2, 3,
			4, 5, 6 });

		table.setTableContentProvider(tableContentProvider);
		table.setTableRowLabelProvider(new BlankTableRowLabelProvider());
		setInitialized(true);
		setFontsize(Messages.getString("font_quellen"));
		setHeaderGenerator(new LaTeXEmptyHeadingsDeclarationGenerator());
		setPagestyleEmpty(true);
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathe.core.serviceimpl.generatoren.latex.ILaTeXGenerator#bodyContents()
	 */
	@Override
	public String bodyContents(IStammdatenservice stammdatenservice) throws MatheJungAltException {
		if (!isInitialized()) {
			init();
		}
		return table.render(stammdatenservice);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathe.core.serviceimpl.generatoren.IFilegenerator#getFilename()
	 */
	@Override
	public String getFilename() {
		return MatheAGGeneratorUtils.filenamePrefix(getSerie()) + "quellen.tex"; //$NON-NLS-1$
	}
}
