/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.contentprovider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.comparators.MitgliedNameComparator;

/**
 * @author Winkelv
 */
public class FruehContentProvider {

	/** */
	private final MitgliedNameComparator comparator = new MitgliedNameComparator();

	/**
	 *
	 */
	public FruehContentProvider() {
	}

	/**
	 *
	 * Gibt die Liste der Mitglieder mit Frühstarterpunkten in der aktuellen Runde sortiert nach Vornamen zurück.
	 *
	 * @param stammdatenservice
	 * @return
	 */
	public List<Mitglied> getMitgliederMitFruestartern(IStammdatenservice stammdatenservice) {
		List<Mitglied> mitglieder = stammdatenservice.findMitgliederMitFruehstarterInRunde(stammdatenservice
			.aktuelleRunde());
		Collections.sort(mitglieder, comparator);
		return mitglieder;
	}

	/**
	 *
	 * @param stammdatenservice
	 * @return
	 */
	public List<Serie> getSerieAktuelleRundeSorted(IStammdatenservice stammdatenservice) {
		List<Serie> result = new ArrayList<Serie>();
		Collection<Serie> serien = stammdatenservice.getSerien();

		int runde = stammdatenservice.aktuelleRunde();
		for (Serie s : serien) {
			if (s.getRunde() == runde) {
				result.add(s);
			}
		}

		Collections.sort(result);
		return result;
	}
}
