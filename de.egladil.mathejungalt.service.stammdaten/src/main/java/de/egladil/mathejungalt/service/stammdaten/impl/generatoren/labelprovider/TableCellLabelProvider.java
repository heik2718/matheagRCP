/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider;

/**
 * Label-Provider mit einer Default-Implementierung für die Methode.
 *
 * @author Winkelv
 */
public class TableCellLabelProvider {

	private String zeilenende;

	/**
	 * @param aufgabenabstand String das ist das Zeilenende nach einer Aufgabe, z.B. \\vertbox{...} usw.
	 */
	public TableCellLabelProvider(String aufgabenabstand) {
		this.zeilenende = aufgabenabstand;
	}

	/**
	 * Ein Text zu gegebenem Objekt
	 *
	 * @param pObj {@link Object}
	 * @param pRow der Zeilenindex
	 * @param pColumn der Spaltenindex
	 * @return String
	 */
	public String getText(Object pObj, int pRow, int pColumn) {
		return pObj == null ? "" : pObj.toString();
	}

	/**
	 * Text für eine Headerzelle
	 *
	 * @param pColumn int der Spaltenindex
	 * @return String der Text
	 */
	public String getHeaderText(int pColumn) {
		return "";
	}

	/**
	 *
	 * @return
	 */
	public final String getZeilenende() {
		return zeilenende == null ? "\\n" : zeilenende;
	}

	/**
	 * @param zeilenende the zeilenende to set
	 */
	public void setZeilenende(String zeilenende) {
		this.zeilenende = zeilenende;
	}
}
