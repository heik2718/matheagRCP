/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.reports;

import java.util.Iterator;
import java.util.List;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.mitglieder.Diplom;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.domain.mitglieder.Serienteilnahme;

/**
 * Stellt Funktionslit�t zum Ermitteln von Punkten, Fr�hstarterpunkten usw. zur Verf�gung
 *
 * @author Winkelv
 */
public class MitgliedReportUtil {

	/**
	 *
	 */
	public MitgliedReportUtil() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param pMitglied
	 * @return boolean true, falls das Mitglied ein Diplom braucht, also mehr als 10 nicht diplomierte Punkte hat.
	 */
	public final boolean brauchtDiplom(Mitglied pMitglied) {
		return !punkteReichenNichtFuerDiplom(pMitglied);
	}

	/**
	 * Prüft, ob die verbleibenden Punkte für ein Diplom reichen.
	 *
	 * @param pMitglied IMitglied das Mitglied
	 * @return true, falls sie nicht reichen.
	 * @throws DomainException falls sich die Anzahl der Diplome nicht ermitteln lässt.
	 */
	private boolean punkteReichenNichtFuerDiplom(final Mitglied pMitglied) throws MatheJungAltException {
		int anzahlPunkteGesamt = pMitglied.getErfinderpunkte() + pMitglied.gesamtpunkteSerien();
		int anzahlDiplome = pMitglied.anzahlDiplome();
		return anzahlPunkteGesamt - 10 * anzahlDiplome < 10;
	}

	/**
	 * Berechnet die Anzahl der Erfinderwinkel, die nicht in Diplomen stecken.
	 *
	 * @param pMitglied IMitglied das Mitglied
	 * @return int die gesuchte Anzahl
	 */
	public int getErfinderwinkelrest(final Mitglied pMitglied) {
		return pMitglied.getErfinderpunkte() - anzahlErfinderpunkteInDiplomen(pMitglied);
	}

	/**
	 * Addiert alle Erfinderpunkte in den Diplomen des Mitglieds.
	 *
	 * @param pMitglied {@link Mitglied} das Mitglied
	 * @return int die Anzahl
	 */
	private int anzahlErfinderpunkteInDiplomen(Mitglied pMitglied) {
		List<Diplom> diplome = pMitglied.getDiplome();
		int erfWinkelInDiplom = 0;
		for (Diplom diplom : diplome) {
			erfWinkelInDiplom += diplom.getAnzahlErfinderpunkte();
		}
		return erfWinkelInDiplom;
	}

	/**
	 * Berechnet die Anzahl der Punkte, die noch nicht in Diplomen stecken.
	 *
	 * @param pMitglied {@link Mitglied} - das Mitglied
	 * @return int
	 */
	public int getNormalpunkterest(final Mitglied pMitglied) {
		int anzahlNormalDipl = 10 * pMitglied.anzahlDiplome() - anzahlErfinderpunkteInDiplomen(pMitglied);
		int summe = 0;
		for (Serienteilnahme teilnahme : pMitglied.getSerienteilnahmen()) {
			summe += teilnahme.getPunkte();
		}
		// return pMitglied.anzahlSerienteilnahmen() - anzahlNormalDipl;
		return summe - anzahlNormalDipl;
	}

	/**
	 * @param pMitglied
	 * @return int die Anzahl der Erfinderpunkte f�r das Diplom.
	 */
	public int berechneAnzahlErfinderpunkteFuerDiplom(Mitglied pMitglied) {
		int anzahlErfinderpunkteInDiplomen = 0;
		Iterator<Diplom> diplome = pMitglied.diplomeIterator();
		while (diplome.hasNext())
			anzahlErfinderpunkteInDiplomen += diplome.next().getAnzahlErfinderpunkte();
		return pMitglied.getErfinderpunkte() - anzahlErfinderpunkteInDiplomen;
	}
}
