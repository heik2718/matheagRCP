/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten;

import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;

import de.egladil.base.exceptions.MatheAGObjectNotGeneratableException;
import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.exceptions.MatheJungAltInconsistentEntityException;
import de.egladil.mathejungalt.config.IMatheJungAltConfiguration;
import de.egladil.mathejungalt.config.IMatheJungAltConfiguration.RaetsellisteGeneratorModus;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serienitem;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.domain.mcraetsel.Urheber;
import de.egladil.mathejungalt.domain.medien.AbstractPrintmedium;
import de.egladil.mathejungalt.domain.medien.Autor;
import de.egladil.mathejungalt.domain.medien.Buch;
import de.egladil.mathejungalt.domain.medien.Zeitschrift;
import de.egladil.mathejungalt.domain.mitglieder.Diplom;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.domain.mitglieder.Serienteilnahme;
import de.egladil.mathejungalt.domain.quellen.Buchquelle;
import de.egladil.mathejungalt.domain.quellen.Kindquelle;
import de.egladil.mathejungalt.domain.quellen.MitgliedergruppenQuelle;
import de.egladil.mathejungalt.domain.quellen.NullQuelle;
import de.egladil.mathejungalt.domain.quellen.Zeitschriftquelle;
import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.domain.schulen.Land;
import de.egladil.mathejungalt.domain.schulen.MinikaenguruTeilnahme;
import de.egladil.mathejungalt.domain.schulen.Schule;
import de.egladil.mathejungalt.domain.schulen.Schulkontakt;
import de.egladil.mathejungalt.domain.types.MCAufgabenstufen;
import de.egladil.mathejungalt.service.stammdaten.event.IModelChangeListener;

/**
 * @author aheike
 */
public interface IStammdatenservice extends PropertyChangeListener {

	/** */
	final String PROTOKOLL_DB = "db";

	final String PLUGIN_ID = "de.egladil.mathejungalt.service.stammdaten";

	public static final String PROCESS_HTLATEX_FILENAME_PREFIX = "process_";

	public static final String PROCESS_HTLATEX_FILENAME_SUFFIX = ".bat";

	/**
	 * Prüft auf fachliche Vollständigkeit.
	 *
	 * @param pDomainObject {@link AbstractMatheAGObject} oder {@link AbstractMCDomainObject} das zu pruefende Objekt.
	 * @throws MatheJungAltInconsistentEntityException
	 * @throws MatheJungAltException
	 */
	void checkConsistent(Object pDomainObject) throws MatheJungAltInconsistentEntityException, MatheJungAltException;

	/**
	 * Alle Aufgaben aus der Persistenzschicht laden.
	 *
	 * @return {@link Collection} von {@link Aufgabe}
	 * @throws MatheJungAltException
	 */
	Collection<Aufgabe> getAufgaben() throws MatheJungAltException;

	/**
	 * @return
	 * @throws MatheJungAltException
	 */
	Collection<MCAufgabe> getQuizaufgaben() throws MatheJungAltException;

	/**
	 * @return
	 * @throws MatheJungAltException
	 */
	MCAufgabe quizaufgabeAnlegen() throws MatheJungAltException;

	/**
	 * @param pAufgabe
	 * @throws MatheJungAltException
	 */
	void quizAufgabeSpeichern(MCAufgabe pAufgabe) throws MatheJungAltException;

	/**
	 * @return
	 * @throws MatheJungAltException
	 */
	Collection<MCArchivraetsel> getRaetsel() throws MatheJungAltException;

	/**
	 * @param pRaetsel
	 * @throws MatheJungAltException
	 */
	void raetselSpeichern(MCArchivraetsel pRaetsel) throws MatheJungAltException;

	/**
	 * @param pRaetsel
	 * @return
	 * @throws MatheJungAltException
	 */
	Collection<MCArchivraetselItem> raetselitemsLaden(MCArchivraetsel pRaetsel) throws MatheJungAltException;

	/**
	 * @param pItem
	 * @throws MatheJungAltException
	 */
	void quizaufgabeAusRaetselEntfernen(MCArchivraetselItem pItem) throws MatheJungAltException;

	/**
	 * @param pAufgabe
	 * @return
	 * @throws MatheJungAltException
	 */
	String isGesperrtFuerRaetsel(MCAufgabe pAufgabe) throws MatheJungAltException;

	/**
	 * @return
	 * @throws MatheJungAltException
	 */
	MCArchivraetsel raetselAnlegen() throws MatheJungAltException;

	/**
	 * Aufgabe mit diesem Schluessel suchen.
	 *
	 * @param pSchluessel
	 * @return {@link Aufgabe} oder null (wenn Parameter null oder Aufgabe nicht vorhanden.
	 */
	public Aufgabe aufgabeZuSchluessel(String pSchluessel);

	/**
	 * MCAufgabe mit diesem Schluessel suchen.
	 *
	 * @param pSchluessel
	 * @return {@link Aufgabe} oder null (wenn Parameter null oder MCAufgabe nicht vorhanden.
	 */
	public MCAufgabe mcaufgabeZuSchluessel(String pSchluessel);

	/**
	 * Alle Autoren aus der Persistenzschicht laden.
	 *
	 * @return {@link Collection} von {@link Autor}
	 * @throws MatheJungAltException
	 */
	Collection<Autor> getAutoren() throws MatheJungAltException;

	/**
	 * Alle Medien laden.
	 *
	 * @return {@link Collection} von {@link AbstractPrintmedium}
	 */
	Collection<AbstractPrintmedium> getMedien();

	/**
	 * Alle Mitglieder laden.
	 *
	 * @return {@link Collection} von {@link Mitglied}
	 */
	Collection<Mitglied> getMitglieder();

	/**
	 * Ein neues Mitglied anlegen.
	 *
	 * @return Mitglied
	 * @throws MatheJungAltException
	 */
	Mitglied mitgliedAnlegen() throws MatheJungAltException;

	/**
	 * Läd die Serienteilnahmen zum gegebenen Mitglied.
	 *
	 * @param pMitglied
	 * @return {@link Collection} entweder gefüllt oder leer, aber nicht null.
	 * @throws MatheJungAltException
	 */
	Collection<Serienteilnahme> serienteilnahmenZuMitgliedLaden(Mitglied pMitglied) throws MatheJungAltException;

	/**
	 * Eine neue Aufgabensammlung anlegen.
	 *
	 * @param pClazz Typ
	 * @return IAufgabensammlung
	 * @throws MatheJungAltException
	 */
	IAufgabensammlung aufgabensammlungAnlegen(@SuppressWarnings("rawtypes") Class pClazz) throws MatheJungAltException;

	/**
	 * Die gegebene Serie speichern.
	 *
	 * @param pAufgabensammlung
	 * @throws MatheJungAltException
	 */
	void aufgabensammlungSpeichern(IAufgabensammlung pAufgabensammlung) throws MatheJungAltException;

	/**
	 * Die Items der Aufgabensammlung werden numeriert.
	 *
	 * @param pAufgabensammlung Serie die Serie.
	 */
	void numerieren(IAufgabensammlung pAufgabensammlung) throws MatheJungAltException;

	/**
	 * Die Items zur gegebenen Aufgabensammlung werden geladen.
	 *
	 * @param pAufgabensammlung die Aufgabensammlung
	 * @return {@link Collection} von {@link IAufgabensammlungItem}
	 * @throws MatheJungAltException
	 */
	Collection<IAufgabensammlungItem> aufgabensammlungitemsLaden(IAufgabensammlung pAufgabensammlung)
		throws MatheJungAltException;

	/**
	 * Die gegebene Aufgabe wird zur gegebenen Serie hinzugefuegt.
	 *
	 * @param pAufgabensammlung Serie, darf nicht null oder beendet sein.
	 * @param pAufgabe die Aufgabe. Darf nicht null oder gesperrt sein und muss vom Zweck S sein.
	 * @return {@link Serienitem}
	 * @throws MatheJungAltException bei einem Parameterfehler
	 */
	IAufgabensammlungItem aufgabeZuSammlungHinzufuegen(IAufgabensammlung pAufgabensammlung, Aufgabe pAufgabe)
		throws MatheJungAltException;

	/**
	 * Die Aufgabe wird zum Raetsel hinzugefuegt.
	 *
	 * @param pRaetsel darf nicht null oder veröffentlicht sein.
	 * @param pAufgabe darf nicht null oder gesperrt sein.
	 * @return
	 * @throws MatheJungAltException bei einem Parameterfehler
	 */
	MCArchivraetselItem quizaufgabeZuRaetselHinzufuegen(MCArchivraetsel pRaetsel, MCAufgabe pAufgabe)
		throws MatheJungAltException;

	/**
	 * Entfernt die Aufgabe des {@link IAufgabensammlungItem} aus der Aufgabensammlung.
	 *
	 * @param pItem
	 * @throws MatheJungAltException
	 */
	void aufgabeAusSammlungEntfernen(IAufgabensammlungItem pItem) throws MatheJungAltException;

	/**
	 * Läd die Diplome zum gegebenen Mitglied.
	 *
	 * @param pMitglied
	 * @return {@link Collection} entweder gefüllt oder leer, aber nicht null.
	 * @throws MatheJungAltException
	 */
	Collection<Diplom> diplomeZuMitgliedLaden(Mitglied pMitglied) throws MatheJungAltException;

	/**
	 * Alle Serien aus der Persistenzschicht laden.
	 *
	 * @return {@link Collection} von {@link Autor}
	 * @throws MatheJungAltException
	 */
	Collection<Serie> getSerien() throws MatheJungAltException;

	/**
	 * Alle {@link Minikaenguru} aus der Persistenzschicht laden.
	 *
	 * @return {@link Collection} von {@link Autor}
	 * @throws MatheJungAltException
	 */
	Collection<Minikaenguru> getMinikaengurus() throws MatheJungAltException;

	/**
	 * Alle {@link Arbeitsblatt} aus der Persistenzschicht laden.
	 *
	 * @return {@link Collection} von {@link Autor}
	 * @throws MatheJungAltException
	 */
	Collection<Arbeitsblatt> getArbeitsblaetter() throws MatheJungAltException;

	/**
	 * Alle Schulen aus der DB.
	 *
	 * @return
	 * @throws MatheJungAltException
	 */
	Collection<Schule> getSchulen() throws MatheJungAltException;

	/**
	 * Alle Kontakte aus der DB.
	 *
	 * @return
	 * @throws MatheJungAltException
	 */
	Collection<Kontakt> getKontakte() throws MatheJungAltException;

	/**
	 * @return int die aktuelle Runde.
	 */
	int aktuelleRunde() throws MatheJungAltException;

	/**
	 * Die Quelle zur gegebenen Id laden.
	 *
	 * @param pAufgabe long
	 * @throws MatheJungAltException falls es diese Quelle nicht gibt.
	 */
	void quelleZuAufgabeLaden(Aufgabe pAufgabe) throws MatheJungAltException;

	/**
	 * Neue Aufgabe erzeugten.
	 *
	 * @return {@link Aufgabe}
	 * @throws MatheJungAltException
	 */
	Aufgabe aufgabeAnlegen() throws MatheJungAltException;

	/**
	 * Setzt das Flag zur Anzeige, ob eine Aufgabe zu schlecht für eine Serie ist auf sein Gegenteil.
	 *
	 * @param pAufgabe
	 */
	void aufgabeStatusZuSchlechtFuerSerieAendern(Aufgabe pAufgabe);

	/**
	 * @param pAufgabe Aufgabe.
	 * @return String (J oder N)
	 */
	String isGesperrtFuerArbeitsblatt(Aufgabe pAufgabe);

	/**
	 * Standardmäßig gilt, dass eine Aufgabe sich erst nach 5 Runden wiederholen darf. Für Stufe 6 soll es wegen der
	 * Dauerteilnehmer demnächst möglich sein, dauerhaft zu sperren.
	 *
	 * @param pAufgabe Aufgabe.
	 * @return String (J oder N)
	 */
	String isGesperrtFuerWettbewerb(Aufgabe pAufgabe);

	/**
	 * Sucht Buchquelleneintrag zu gegebenem Buch und gegebener Seite. Falls es keine gibt, wird eine neue erzeugt.
	 *
	 * @param pBuch {@link Buch} das Buch
	 * @param pSeite int die Seitenzahl
	 * @return {@link Buchquelle}
	 * @throws MatheJungAltException
	 */
	public abstract Buchquelle buchquelleFindenOderAnlegen(final Buch pBuch, final int pSeite)
		throws MatheJungAltException;

	/**
	 * Sucht Zeitschriftquelle mit dieser Zeitschrift, Ausgabe, Jahrgang. Falls es keine gibt, wird eine neue erzeugt.
	 *
	 * @param pZeitschrift {@link Zeitschrift}
	 * @param pAusgabe int die Ausgabe
	 * @param pJahrgang int der Jahrgang
	 * @return {@link Zeitschriftquelle}
	 * @throws MatheJungAltException
	 */
	public Zeitschriftquelle zeitschriftquelleFindenOderAnlegen(Zeitschrift pZeitschrift, int pAusgabe, int pJahrgang)
		throws MatheJungAltException;

	/**
	 * Sucht Quelle zu diesem Mitglied mit diesem Alter und dieser Klassenstufe. Falls es keine gibt, wird eine neue
	 * erzeugt.
	 *
	 * @param pMitglied
	 * @param pJahre
	 * @param pKlasse
	 * @return {@link Kindquelle}
	 * @throws MatheJungAltException
	 */
	public Kindquelle kindquelleFindenOderAnlegen(Mitglied pMitglied, int pJahre, int pKlasse)
		throws MatheJungAltException;

	/**
	 * Legt eine Gruppenquelle an.
	 *
	 * @param pMitglieder {@link Collection} darf nicht null sein und muss mindestens 2 Elemente haben.
	 * @return {@link MitgliedergruppenQuelle}
	 * @throws MatheJungAltException
	 */
	public MitgliedergruppenQuelle gruppenquelleAnlegen(Collection<Mitglied> pMitglieder) throws MatheJungAltException;

	/**
	 * Die (einzige) Nullquelle.
	 *
	 * @return {@link NullQuelle}
	 */
	public NullQuelle getNullQuelle() throws MatheJungAltException;

	/**
	 * Neuen Autor erzeugen.
	 *
	 * @return {@link Autor}
	 * @throws MatheJungAltException
	 */
	Autor autorAnlegen() throws MatheJungAltException;

	/**
	 * Neues Buch anlegen.
	 *
	 * @return {@link Buch}
	 * @throws MatheJungAltException
	 */
	Buch buchAnlegen() throws MatheJungAltException;

	/**
	 * Neue Zeitschrift anlegen.
	 *
	 * @return {@link Zeitschrift}
	 * @throws MatheJungAltException
	 */
	Zeitschrift zeitschriftAnlegen() throws MatheJungAltException;

	/**
	 * Speichert die gegbene Aufgabe.
	 *
	 * @param pAufgabe {@link Aufgabe} de zu speichernde Aufgabe.
	 */
	void aufgabeSpeichern(final Aufgabe pAufgabe) throws MatheJungAltException;

	/**
	 * Ermittelt, ob die gegebene Aufgabeninfo editierbar sein soll.
	 *
	 * @param pAufgabe
	 * @return
	 * @throws MatheJungAltException
	 */
	boolean aufgabeEditierbar(final Aufgabe pAufgabe) throws MatheJungAltException;

	/**
	 * Speichert das Printmedium.
	 *
	 * @param pMedium {@link AbstractPrintmedium} der Autor
	 */
	void mediumSpeichern(final AbstractPrintmedium pMedium);

	/**
	 * Speichert den Autor.
	 *
	 * @param pAutor {@link Autor} der Autor
	 */
	void autorSpeichern(final Autor pAutor);

	/**
	 * Speichert das gegebene Mitglied.
	 *
	 * @param pMitglied {@link Mitglied}
	 * @throws MatheJungAltException
	 */
	void mitgliedSpeichern(Mitglied pMitglied) throws MatheJungAltException;

	/**
	 * Ändert den Aktivierungsstatus des Mitglieds.
	 *
	 * @param pMitglied
	 * @throws MatheJungAltException
	 */
	void mitgliedAktivierungAendern(Mitglied pMitglied) throws MatheJungAltException;

	/**
	 * Fuegt die Mailadresse zum Verteiler hinzu.
	 *
	 * @param pAdresse
	 * @throws MatheJungAltException
	 */
	void mailadresseZurMailinglisteHinzufuegen(String pAdresse) throws MatheJungAltException;

	/**
	 * Entfernt die Mailadresse aus dem Verteiler.
	 *
	 * @param pAdresse
	 * @throws MatheJungAltException
	 */
	void mailadresseAusMailinglisteEntfernen(String pAdresse) throws MatheJungAltException;

	/**
	 * Prueft, ob die gegebene Adresse in der Mailingliste enthalten ist.
	 *
	 * @param pAdresse
	 * @return
	 * @throws MatheJungAltException
	 */
	boolean mailadresseInMailinglisteEnthalten(String pAdresse) throws MatheJungAltException;

	/**
	 * Alle Email-Adressen aus der Mailingliste laden.
	 *
	 * @return {@link Collection}
	 * @throws MatheJungAltException
	 */
	Collection<String> getEmailadressen() throws MatheJungAltException;

	/**
	 * Synchronisiert die gecacheten Aufgaben mit der Persistenzschicht.
	 */
	void aufgabenSynchronisieren() throws MatheJungAltException;

	/**
	 * Synchronisiert die gecacheten Autoren mit der Persistenzschicht.
	 */
	void autorenSynchronisieren() throws MatheJungAltException;

	/**
	 * Synchronisiert die gecacheten Medien mit der Persistenzschicht.
	 */
	void medienSynchronisieren() throws MatheJungAltException;

	/**
	 * Synchronisiert die gecacheten Autoren mit der Persistenzschicht.
	 */
	void mitgliederSynchronisieren() throws MatheJungAltException;

	/**
	 * @param pListener
	 */
	void addModelChangeListener(IModelChangeListener pListener);

	/**
	 * @param pListener
	 */
	void removeModelChangeListener(IModelChangeListener pListener);

	/**
	 * Entfernt die gegebene Serienteilnahme wieder.
	 *
	 * @param pMitglied
	 * @param pSerie
	 */
	void removeTeilnahme(Mitglied pMitglied, Serie pSerie) throws MatheJungAltException;

	/**
	 * Erzeugt eine neue {@link Serienteilnahme} für das {@link Mitglied} in der {@link Serie}.
	 *
	 * @param pMitglied {@link Mitglied} das Mitglied
	 * @param pSerie {@link Serie} die Serie
	 */
	void mitgliedNimmtTeil(Mitglied pMitglied, Serie pSerie) throws MatheJungAltException;

	/**
	 * @return
	 */
	Schule schuleAnlegen();

	/**
	 * @return
	 */
	Kontakt kontaktAnlegen();

	/**
	 * @param pSchule
	 * @param pMinikaenguru
	 */
	void minikaenguruTeilnahmeLoeschen(Schule pSchule, Minikaenguru pMinikaenguru);

	/**
	 * @param pSchule
	 * @param pMinikaenguru
	 * @return
	 */
	MinikaenguruTeilnahme schuleNimmtTeil(Schule pSchule, Minikaenguru pMinikaenguru);

	/**
	 * @param pSchule
	 * @return
	 */
	Collection<MinikaenguruTeilnahme> minikaenguruTeilnahmenLaden(Schule pSchule);

	/**
	 * @param pKontakt
	 * @param pSchule
	 */
	void schulkontaktLoschen(Kontakt pKontakt, Schule pSchule);

	/**
	 * @param pKontakt
	 * @param pSchule
	 * @return
	 */
	Schulkontakt kontaktZuSchuleHinzufuegen(Kontakt pKontakt, Schule pSchule);

	/**
	 * @param pKontakt
	 * @return
	 */
	Collection<Schulkontakt> schulkontakteLaden(Kontakt pKontakt);

	/**
	 * Prueft, ob das gegebene Mitglied ein Diplom braucht.
	 *
	 * @param pMitglied
	 * @return boolean
	 * @throws MatheJungAltException
	 */
	boolean mitgliedBrauchtDiplom(Mitglied pMitglied) throws MatheJungAltException;

	/**
	 * Dem Mitglied ein Diplom geben.
	 *
	 * @param pMitglied
	 * @return
	 * @throws MatheJungAltException
	 */
	Diplom diplomGeben(Mitglied pMitglied) throws MatheJungAltException;

	/**
	 * Serienteilnahme zu gegebenem Mitglied und gegebener Serie suchen.
	 *
	 * @param pMitglied {@link Mitglied} das Mitglied
	 * @param pSerie {@link Serie} die Serie
	 * @return {@link Serienteilnahme}
	 */
	Serienteilnahme findSerienteilnahme(Mitglied pMitglied, Serie pSerie) throws MatheJungAltException;

	/**
	 * Ermittelt, ob die Seitengenerierung moeglich ist.
	 *
	 * @param pObject {@link AbstractMatheAGObject} das Objekt, das generiert werden soll. Darf nicht null sein.
	 * @return boolean true, falls generieren moeglich, false sonst.
	 * @throws MatheJungAltException bei Programmfehlern
	 * @throws MatheAGObjectNotGeneratableException falls nicht generiert werden kann. Mit Begruendung
	 */
	boolean canGenerate(AbstractMatheAGObject pObject) throws MatheJungAltException,
		MatheAGObjectNotGeneratableException;

	/**
	 * Generiert die Archivseite.
	 *
	 * @param pPath
	 */
	void generateMatheAGArchivseite(String pPath);

	/**
	 * Generiert eine neue Version der Raetselliste für das Quizspiel
	 *
	 * @param pVersion int die Versionsnummer
	 * @param pPath String der Pfad des Ausgabeverzeichnisses.
	 * @param pPrivat boolean falls true, dann werden alle Rätsel generiert, anderenfalls nur die öffenlichen.
	 * @param pModus TODO
	 */
	IStatus generateRaetselliste(int pVersion, String pPath, boolean pPrivat, RaetsellisteGeneratorModus pModus);

	/**
	 * Generiert die Html- oder LaTeX-Seiten in das gegebene Verzeichnis.
	 *
	 * @param pClassName String voll qualifizierter ClassName der zu generierenden Klasse
	 * @param pPath String absoluter Pfad zu einem Verzeichnis.
	 * @throws MatheJungAltException
	 * @deprecated use typed
	 */
	void generate(String pClassName, String pPath) throws MatheJungAltException;

	/**
	 * Generiert die Seiten in das durch programs.tempdir konfigurierte Verzeichnis.
	 *
	 * @param pTypeName
	 * @param pObjects
	 * @param pathWorkDir Pfad zum Ausgabeverzeichnis der generierten Dateien.
	 * @param pModus TODO
	 * @param pProgressMonitor
	 * @return TODO
	 */
	<T> IStatus generate(String pTypeName, Map<String, T> pObjects, String pathWorkDir,
		RaetsellisteGeneratorModus pModus, IProgressMonitor pProgressMonitor);

	/**
	 * Generiert die Html- oder LaTeX-Seiten zur gegebenen {@link IAufgabensammlung} in das durch programs.tempdir
	 * konfigurierte Verzeichnis.
	 *
	 * @param pAufgabensammlung {@link IAufgabensammlung} die generiert wedrden soll
	 * @param pPath String absoluter Pfad zu einem Verzeichnis.
	 * @throws MatheJungAltException
	 */
	void generate(IAufgabensammlung pAufgabensammlung, IProgressMonitor pProgressMonitor) throws MatheJungAltException;

	/**
	 * Sucht alle Mitglieder einer Klasse.
	 *
	 * @param pKlasse
	 * @param pAktiv boolean wenn true, dann nur die aktiven, sonst alle.
	 * @return {@link List}
	 */
	List<Mitglied> findMitgliederByKlasse(int pKlasse, boolean pAktiv);

	/**
	 * Gibt alle Mitglieder zurueck, die in der gegebenen Runde einen Fruehstarterpunkt haben.
	 *
	 * @param pRunde int die Runde
	 * @return {@link List} von {@link Mitglied}
	 */
	List<Mitglied> findMitgliederMitFruehstarterInRunde(int pRunde) throws MatheJungAltException;

	/**
	 * Alle Serien zu gegebener Runde.
	 *
	 * @param pRunde
	 * @return
	 */
	List<Serie> getSerien(int pRunde);

	/**
	 * Gibt den Namen der Datenbank oder den Pfad des Verzeichnisses, in dem die Domain-Objekte persistiert werden.
	 *
	 * @param pProtokoll String
	 * @return String
	 */
	String getPersistenceLocation(String pProtokoll);

	/**
	 * @return
	 */
	Comparator<IAufgabensammlungItem> getArbeitsblattItemNummerComparator();

	/**
	 * @return
	 */
	IMatheJungAltConfiguration getConfiguration();

	/**
	 * @return
	 */
	Collection<Urheber> getQuizAutoren();

	/**
	 * @param pRaetsel
	 */
	void publish(MCArchivraetsel pRaetsel);

	/**
	 * @param schluesselPrefix
	 * @param anzahl
	 * @param stufe
	 * @return
	 */
	List<MCAufgabe> kaenguruAufgabentemplatesAnlegen(String schluesselPrefix, int anzahl, MCAufgabenstufen stufe);

	/**
	 * @param ja
	 */
	void setStufe6MaximalEinmal(boolean ja);

	/**
	 * @param pSchule
	 */
	void schuleSpeichern(Schule pSchule);

	/**
	 * @param pKontkt
	 */
	void kontaktSpeichern(Kontakt pKontkt);

	/**
	 * @param pMinikaenguru
	 */
	void mailinglisteLaden(Minikaenguru pMinikaenguru);

	/**
	 * @param pSchule
	 * @return
	 */
	List<Kontakt> findKontakteZuSchule(Schule pSchule);

	/**
	 * @param pTeilnahme
	 * @param pMinikaenguru
	 * @param pSchule
	 * @param pAktuelleKontakte
	 */
	void mailinglisteAktualisieren(MinikaenguruTeilnahme pTeilnahme, List<Kontakt> pAktuelleKontakte);

	/**
	 * Kontakte zu einer Minikaenguruteilnahme finden.
	 *
	 * @param teilnahme
	 * @return
	 */
	List<Kontakt> getKontakteMinikaenguru(MinikaenguruTeilnahme teilnahme);

	/**
	 * Generiert die Verzeichnis-Struktur, Latex-Dateien und bat-Dateien, mit denen dann die Html-Dateien in einem
	 * externen Schritt generiert werden können. Wenn alles glatt läuft, befindet sich im Arbeitsverzeichnis eine Datei
	 * namens [raetsel.schluessel]_pre.txt. Dies kann verwendet werden, um das PostHtlatex-Command zu aktivieren.
	 *
	 * @param raetsel
	 * @param path
	 * @param pMonitor
	 * @return
	 */
	IStatus prepareHtlatex(MCArchivraetsel raetsel, String path, IProgressMonitor pMonitor);

	/**
	 * @param raetsel
	 * @param pathWorkDir
	 * @return
	 */
	boolean canRunPostHtlatexProcessor(MCArchivraetsel raetsel, String pathWorkDir);

	/**
	 * Bereitet die durch htlatex vorbereiteten html-Dateien für den gegebenen Modus nach.
	 *
	 * @param raetsel
	 * @param pathWorkDir
	 * @param modus
	 * @param pMonitor
	 * @return
	 */
	IStatus processPostHtlatexTasks(MCArchivraetsel raetsel, String pathWorkDir, RaetsellisteGeneratorModus modus,
		IProgressMonitor pMonitor);

	/**
	 * @param raetsel
	 * @param pathWorkDir
	 * @return
	 */
	boolean canPrepareHtlatex(MCArchivraetsel raetsel, String pathWorkDir);

	/**
	 * Länd die Länderliste.
	 *
	 * @return
	 * @throws MatheJungAltException
	 */
	List<Land> getLaender() throws MatheJungAltException;

	/**
	 * @param label
	 * @return
	 */
	Land findLandByLabel(String label);
}
