/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl;

import java.util.Collection;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.config.IMatheJungAltConfiguration;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serienitem;
import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.GenerierbarkeitsChecker;

/**
 * @author aheike
 */
public class Aufgabensammlungservice extends AbstractLaTeXConcernedService {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(Aufgabensammlungservice.class);

	/** */
	private MinikaenguruserviceDelegate minikaenguruserviceDelegate = new MinikaenguruserviceDelegate();

	/** */
	private ArbeitsblattserviceDelegate arbeitsblattserviceDelegate = new ArbeitsblattserviceDelegate();

	/** */
	private final SerienserviceDelegate serienserviceDelegate;

	/** */
	private final GenerierbarkeitsChecker generierbarkeitsChecker = new GenerierbarkeitsChecker();

	/**
	 * @param pPersistenceservice
	 */
	public Aufgabensammlungservice() {
		super();
		serienserviceDelegate = new SerienserviceDelegate();

	}

	/**
	 * Alle Serien aus der Persistenzschicht laden.
	 *
	 * @return {@link Collection} von {@link Serie}
	 * @throws MatheJungAltException
	 */
	protected Collection<Serie> getSerien() throws MatheJungAltException {
		return serienserviceDelegate.getSerien(getPersistenceservice());
	}

	/**
	 * Alle Minikaengurus aus der Persistenzschicht laden.
	 *
	 * @return {@link Collection} von {@link Minikaenguru}
	 * @throws MatheJungAltException
	 */
	protected Collection<Minikaenguru> getMinikaengurus() throws MatheJungAltException {
		return minikaenguruserviceDelegate.getMinikaengurus(getPersistenceservice());
	}

	/**
	 * Alle Arbeitsblatt aus der Persistenzschicht laden.
	 *
	 * @return {@link Collection} von {@link Minikaenguru}
	 * @throws MatheJungAltException
	 */
	protected Collection<Arbeitsblatt> getArbeitsblaetter() throws MatheJungAltException {
		return arbeitsblattserviceDelegate.getArbeitsblaetter(getPersistenceservice());
	}

	/**
	 * Generiert die Archivseite
	 *
	 * @param pPath
	 */
	protected void generateArchiv(String pPath, IStammdatenservice stammdatenservice) {
		serienserviceDelegate.generateArchiv(pPath, stammdatenservice);
	}

	/**
	 * Sucht alle Serien zur gegebenen Runde.
	 *
	 * @param pRunde
	 */
	protected List<Serie> getSerien(int pRunde) {
		return serienserviceDelegate.getSerien(pRunde, getPersistenceservice());
	}

	/**
	 * Die Items zur gegebenen {@link IAufgabensammlung} werden geladen.
	 *
	 * @param pAufgabensammlung
	 * @return {@link Collection}
	 * @throws MatheJungAltException
	 */
	protected Collection<IAufgabensammlungItem> itemsLaden(IAufgabensammlung pAufgabensammlung)
		throws MatheJungAltException {
		if (pAufgabensammlung == null) {
			String msg = MatheJungAltException.argumentNullMessage("Serie");
			LOG.error(msg);
		}
		if (!pAufgabensammlung.isItemsLoaded()) {
			if (pAufgabensammlung instanceof Serie) {
				getPersistenceservice().getSerienRepository().loadSerienitems((Serie) pAufgabensammlung);
				return pAufgabensammlung.getItems();
			}
			if (pAufgabensammlung instanceof Minikaenguru) {
				getPersistenceservice().getMinikaenguruRepository().loadItems((Minikaenguru) pAufgabensammlung);
				return pAufgabensammlung.getItems();
			}
			if (pAufgabensammlung instanceof Arbeitsblatt) {
				getPersistenceservice().getArbeitsblattRepository().loadItems((Arbeitsblatt) pAufgabensammlung);
				return pAufgabensammlung.getItems();
			}
			String msg = "Ungültiger Typ " + pAufgabensammlung.getClass().getName();
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		return pAufgabensammlung.getItems();
	}

	/**
	 * @param pType
	 * @return
	 */
	protected IAufgabensammlung aufgabensammlungAnlegen(Class<IAufgabensammlung> pType) {
		IAufgabensammlung aufgabensammlung = null;
		if (Serie.class.equals(pType)) {
			aufgabensammlung = serienserviceDelegate.serieAnlegen(getPersistenceservice());
		}
		if (Minikaenguru.class.equals(pType)) {
			aufgabensammlung = minikaenguruserviceDelegate.minikaenguruAnlegen();
		}
		if (Arbeitsblatt.class.equals(pType)) {
			aufgabensammlung = arbeitsblattserviceDelegate.arbeitsblattAnlegen(getPersistenceservice());
		}
		return aufgabensammlung;
	}

	/**
	 * @param pAufgabensammlung
	 */
	protected void aufgabensammlungSpeichern(IAufgabensammlung pAufgabensammlung) {
		boolean neu = pAufgabensammlung.isNew();
		if (pAufgabensammlung instanceof Serie) {
			serienserviceDelegate.serieSpeichern((Serie) pAufgabensammlung, getPersistenceservice());
		}
		if (pAufgabensammlung instanceof Minikaenguru) {
			minikaenguruserviceDelegate
				.minikaenguruSpeichern((Minikaenguru) pAufgabensammlung, getPersistenceservice());
		}
		if (pAufgabensammlung instanceof Arbeitsblatt) {
			arbeitsblattserviceDelegate
				.arbeitsblattSpeichern((Arbeitsblatt) pAufgabensammlung, getPersistenceservice());
		}
		if (neu) {
			fireObjectAddedEvent((AbstractMatheAGObject) pAufgabensammlung);
		}
	}

	/**
	 * @return int die aktuelle Runde.
	 */
	protected int aktuelleRunde() throws MatheJungAltException {
		return serienserviceDelegate.aktuelleRunde(getPersistenceservice());
	}

	/**
	 * Die Items der Aufgabensammlung werden numeriert.
	 *
	 * @param pAufgabensammlung Serie die Serie.
	 */
	protected void itemsNumerieren(IAufgabensammlung pAufgabensammlung) {
		if (pAufgabensammlung == null) {
			final String msg = MatheJungAltException.argumentNullMessage("pAufgabensammlung");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (pAufgabensammlung.getBeendet() == IAufgabensammlung.BEENDET) {
			String message = "Aufgnennummern k\u00F6nnen nicht mehr vergeben werden:\nDie Aufgabensammlung "
				+ pAufgabensammlung + " ist bereits abgeschlossen!";
			throw new MatheJungAltException(message);
		}
		if (!pAufgabensammlung.isItemsLoaded()) {
			itemsLaden(pAufgabensammlung);
		}
		if (pAufgabensammlung instanceof Serie) {
			serienserviceDelegate.itemsNumerieren((Serie) pAufgabensammlung);
		}
		if (pAufgabensammlung instanceof Minikaenguru) {
			minikaenguruserviceDelegate.itemsNumerieren((Minikaenguru) pAufgabensammlung);
		}
		if (pAufgabensammlung instanceof Arbeitsblatt) {
			arbeitsblattserviceDelegate.itemsNumerieren((Arbeitsblatt) pAufgabensammlung);
		}
	}

	/**
	 * Die gegebene Aufgabe wird zur gegebenen Serie hinzugefuegt.
	 *
	 * @param pAufgabensammlung Serie, darf nicht null oder beendet sein.
	 * @param pAufgabe die Aufgabe. Darf nicht null oder gesperrt sein und muss vom Zweck S sein.
	 * @return {@link Serienitem}
	 * @throws MatheJungAltException bei einem Parameterfehler
	 */
	protected IAufgabensammlungItem aufgabeZuSammlungHinzufuegen(IAufgabensammlung pAufgabensammlung, Aufgabe pAufgabe)
		throws MatheJungAltException {
		IAufgabensammlungItem item = null;

		if (pAufgabensammlung instanceof Serie) {
			item = serienserviceDelegate.aufgabeZuSerieHinzufuegen((Serie) pAufgabensammlung, pAufgabe);
		}
		if (pAufgabensammlung instanceof Minikaenguru) {
			item = minikaenguruserviceDelegate.aufgabeHinzufuegen((Minikaenguru) pAufgabensammlung, pAufgabe);
		}
		if (pAufgabensammlung instanceof Arbeitsblatt) {
			item = arbeitsblattserviceDelegate.aufgabeHinzufuegen((Arbeitsblatt) pAufgabensammlung, pAufgabe);
		}
		return item;
	}

	/**
	 * IAufgabensammlungItem wird aus der Aufgabensammlung etfernt.
	 *
	 * @param pItem {@link IAufgabensammlungItem} darf nicht null sein
	 */
	protected void aufgabeAusSammlungEntfernen(IAufgabensammlungItem pItem) throws MatheJungAltException {
		if (pItem == null) {
			String msg = MatheJungAltException.argumentNullMessage("pItem");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		final IAufgabensammlung aufgabensammlung = pItem.getAufgabensammlung();
		boolean removed = aufgabensammlung.removeItem(pItem);
		if (removed) {
			aufgabeFreigeben(pItem, aufgabensammlung);
		}
	}

	/**
	 * Entfernen aus sperrendem Arbeitsblatt oder Wettbewerb gibt Aufgabe wieder frei zum Editieren und Zuordnen
	 *
	 * @param pItem
	 * @param aufgabensammlung
	 */
	private void aufgabeFreigeben(IAufgabensammlungItem pItem, final IAufgabensammlung aufgabensammlung) {
		if (aufgabensammlung instanceof Arbeitsblatt
			&& ((Arbeitsblatt) aufgabensammlung).getSperrend() == Arbeitsblatt.SPERREND) {
			pItem.getAufgabe().setGesperrtFuerArbeitsblatt(Aufgabe.NOT_LOCKED);
		} else {
			pItem.getAufgabe().setGesperrtFuerWettbewerb(Aufgabe.NOT_LOCKED);
		}
	}

	/**
	 * Generiert die Seiten fuers Web.
	 *
	 * @param pPath
	 * @param pAufgabensammlung
	 * @param pProgressMonitor TODO
	 */
	protected void generateAll(String pPath, IAufgabensammlung pAufgabensammlung, IStammdatenservice stammdatenservice,
		IProgressMonitor pProgressMonitor) {
		SubMonitor subMonitor = SubMonitor.convert(pProgressMonitor);
		if (pAufgabensammlung instanceof Serie) {
			subMonitor.setWorkRemaining(2);
			subMonitor.subTask("generiere HTML");
			serienserviceDelegate.generateWebcontent(pPath, (Serie) pAufgabensammlung, stammdatenservice);
			subMonitor.worked(1);
			subMonitor.subTask("generiere LaTeX");
			serienserviceDelegate.generateLaTeX(pPath, (Serie) pAufgabensammlung, stammdatenservice);
			subMonitor.worked(1);
		}
		if (pAufgabensammlung instanceof Arbeitsblatt) {
			subMonitor.setWorkRemaining(1);
			subMonitor.subTask("generiere LaTeX");
			arbeitsblattserviceDelegate.generate(pPath, (Arbeitsblatt) pAufgabensammlung, stammdatenservice);
			subMonitor.worked(1);
		}
		if (pAufgabensammlung instanceof Minikaenguru) {
			subMonitor.setWorkRemaining(1);
			subMonitor.subTask("generiere LaTeX");
			minikaenguruserviceDelegate.generate(pPath, (Minikaenguru) pAufgabensammlung, stammdatenservice);
			subMonitor.worked(1);
		}
	}

	/**
	 * @param pAufgabensammlung Serie die Serie.
	 * @return boolean
	 */
	protected boolean canGenerate(IAufgabensammlung pAufgabensammlung) {
		if (!pAufgabensammlung.isItemsLoaded()) {
			itemsLaden(pAufgabensammlung);
		}
		if (pAufgabensammlung instanceof Serie) {
			generierbarkeitsChecker.visit((Serie) pAufgabensammlung, true);
		}
		if (pAufgabensammlung instanceof Minikaenguru) {
			generierbarkeitsChecker.visit((Minikaenguru) pAufgabensammlung, true);
		}
		if (pAufgabensammlung instanceof Arbeitsblatt) {
			generierbarkeitsChecker.visit((Arbeitsblatt) pAufgabensammlung, true);
		}
		return true;
	}

	/**
	 * @param minikaenguru
	 * @return
	 */
	protected List<Kontakt> mailinglisteLaden(Minikaenguru minikaenguru) {
		return minikaenguruserviceDelegate.mailinglisteLaden(minikaenguru, getPersistenceservice());
	}

	/**
	 * @param pConfiguration the configuration to set
	 */
	public final void setConfiguration(IMatheJungAltConfiguration pConfiguration) {
		serienserviceDelegate.setConfiguration(pConfiguration);
	}
}
