/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.latex;

import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.ITableLabelProvider;

/**
 * @author Winkelv
 */
public abstract class AbstractLaTeXTableLabelProvider implements ITableLabelProvider {

	/** */
	private final String fontSize;

	/**
	 * @param fontSize TODO
	 *
	 */
	public AbstractLaTeXTableLabelProvider(String fontSize) {
		this.fontSize = fontSize;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathe.core.serviceimpl.generatoren.labelprovider.ITableLabelProvider#getStartText()
	 */
	@Override
	public String getEndText() {
		return "}\n";
	}

	/**
	 * @return
	 */
	public String getFontSize() {
		return fontSize;
	}
}
