/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.comparators;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;

/**
 * @author aheike
 */
public class AufgabeStufeComparator extends AbstractMatheAGObjectComparator {

	/**
	 *
	 */
	public AufgabeStufeComparator() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.impl.comparators.AbstractMatheAGObjectComparator#specialCheckType(
	 * de.egladil.mathejungalt.domain.AbstractMatheAGObject, de.egladil.mathejungalt.domain.AbstractMatheAGObject)
	 */
	@Override
	protected void specialCheckType(AbstractMatheAGObject pO1, AbstractMatheAGObject pO2) {
		if (!(pO1 instanceof Aufgabe) && !(pO2 instanceof Aufgabe))
			throw new MatheJungAltException("Falscher Typ: kein " + Aufgabe.class.getName());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.impl.comparators.AbstractMatheAGObjectComparator#specialCompare(de
	 * .egladil.mathejungalt.domain.AbstractMatheAGObject, de.egladil.mathejungalt.domain.AbstractMatheAGObject)
	 */
	@Override
	protected int specialCompare(AbstractMatheAGObject pO1, AbstractMatheAGObject pO2) {
		Aufgabe aufgabe1 = (Aufgabe) pO1;
		Aufgabe aufgabe2 = (Aufgabe) pO2;
		return aufgabe1.getStufe().intValue() - aufgabe2.getStufe().intValue();
	}

}
