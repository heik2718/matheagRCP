/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten;

import java.util.List;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.exceptions.errorhandler.IErrorStateProvider;

/**
 * @author aheike
 */
public class ActionNotPossibleException extends MatheJungAltException {

	/**
	 *
	 */
	private static final long serialVersionUID = -8643782392778353329L;

	/**
	 * @param pMessage
	 * @param pErrorStateProviders
	 * @param pCause
	 */
	public ActionNotPossibleException(String pMessage, List<IErrorStateProvider> pErrorStateProviders, Throwable pCause) {
		super(pMessage, pErrorStateProviders, pCause);
	}

	/**
	 * @param pMessage
	 * @param pErrorStateProviders
	 */
	public ActionNotPossibleException(String pMessage, List<IErrorStateProvider> pErrorStateProviders) {
		super(pMessage, pErrorStateProviders);
	}

	/**
	 * @param pMessage
	 * @param pCause
	 */
	public ActionNotPossibleException(String pMessage, Throwable pCause) {
		super(pMessage, pCause);
	}

	/**
	 * @param pMessage
	 */
	public ActionNotPossibleException(String pMessage) {
		super(pMessage);
	}
}
