/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.consistence;

import org.apache.commons.lang.StringUtils;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.exceptions.MatheJungAltInconsistentEntityException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.DomainObjectProxyClassifier;
import de.egladil.mathejungalt.domain.medien.Autor;
import de.egladil.mathejungalt.domain.medien.IAutorNames;

/**
 * @author Heike Winkelvoss (www.egladil.de)
 */
public class AutorConsistenceChecker implements IConsistencecheckProvider {

	/** */
	private final ConsistenceCheckDelegate consistenceCheckDelegate;

	/**
	 *
	 */
	public AutorConsistenceChecker() {
		consistenceCheckDelegate = new ConsistenceCheckDelegate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.consistencecheck.IConsistencecheckProvider#canCheck(de.egladil.mathejungalt.domain
	 * .AbstractMatheAGObject)
	 */
	@Override
	public boolean canCheck(Object pObject) {
		return DomainObjectProxyClassifier.isAutor(pObject);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.consistencecheck.IConsistencecheckProvider#checkConsistent(de.egladil.mathejungalt
	 * .domain.AbstractMatheAGObject)
	 */
	@Override
	public void checkConsistent(AbstractMatheAGObject pObject) throws MatheJungAltException {
		Autor autor = (Autor) pObject;
		consistenceCheckDelegate.checkStringKey(autor.getKey(), IAutorNames.LENGTH_SCHLUESSEL, "Autor:");
		if (StringUtils.isEmpty(autor.getName()))
			throw new MatheJungAltInconsistentEntityException("Autor: der Name darf nicht null oder leer sein.");
		if (autor.getName().length() > IAutorNames.LENGTH_NAME)
			throw new MatheJungAltInconsistentEntityException("Autor: der Name darf nicht laenger als "
				+ IAutorNames.LENGTH_NAME + " Zeichen sein");

	}
}
