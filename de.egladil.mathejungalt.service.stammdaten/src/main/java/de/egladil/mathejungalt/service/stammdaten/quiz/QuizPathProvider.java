/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.service.stammdaten.quiz;

import java.io.File;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.runtime.Assert;

import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;

/**
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class QuizPathProvider {

	/**
	 * Bildet für das gegebene raetsel einen Verzeichnispfad für alle Dateien zu diesem raetsel.
	 *
	 * @param raetsel MCArchivraetsel darf nicht null sein. Schluessel muss gefüllt sein.
	 * @param pathWorkdir
	 * @return String
	 */
	public static String getPathQuizRoot(MCArchivraetsel raetsel, String pathWorkdir) {
		Assert.isNotNull(raetsel, "raetsel darf nicht null sein");
		Assert.isNotNull(pathWorkdir, "pathWorkdir darf nicht null sein");
		return pathWorkdir + File.separator + getGetRelativPathQuiz(raetsel);
	}

	/**
	 * @param raetsel
	 * @return
	 */
	public static String getGetRelativPathQuiz(MCArchivraetsel raetsel) {
		Assert.isNotNull(raetsel, "raetsel darf nicht null sein");
		Assert.isTrue(!StringUtils.isEmpty(raetsel.getSchluessel()), "raetsel.schluessel muss gesetzt sein.");
		return raetsel.getSchluessel();
	}

	/**
	 * @param item
	 * @return
	 */
	public static String getRelativPathQuizitem(MCArchivraetselItem item) {
		Assert.isNotNull(item, "item darf nicht null sein!");
		Assert.isTrue(!StringUtils.isEmpty(item.getNummer()), "Das item hat keine Nummer.");
		return StringUtils.leftPad(item.getNummer(), 2, "0");
	}

	/**
	 * Absoluter Pfad zum Marker, dass htlatex vorbereitet ist.
	 *
	 * @param raetsel
	 * @param pathWorkDir
	 * @return
	 */
	public static String getPathReadyMarkerHtlatexPrepare(MCArchivraetsel raetsel, String pathWorkDir) {
		Assert.isNotNull(raetsel, "raetsel darf nicht null sein");
		Assert.isNotNull(pathWorkDir, "pathWorkdir darf nicht null sein");
		return pathWorkDir + "\\" + getGetRelativPathQuiz(raetsel) + "_pre.txt";
	}

	/**
	 * Absoluter Pfad zum Marker, dass htlatex fertig ist.
	 *
	 * @param raetsel
	 * @param pathWorkDir
	 * @return
	 */
	public static String getPathReadyMarkerPostHtlatex(MCArchivraetsel raetsel, String pathWorkDir) {
		Assert.isNotNull(raetsel, "raetsel darf nicht null sein");
		Assert.isNotNull(pathWorkDir, "pathWorkdir darf nicht null sein");
		return pathWorkDir + "\\" + getGetRelativPathQuiz(raetsel) + "_post.txt";
	}

	/**
	 * Absoluter Pfad zur Datei quiz.xml für Android.
	 *
	 * @param raetsel
	 * @param pathWorkdir
	 * @return
	 */
	public static String getPathQuizXml(MCArchivraetsel raetsel, String pathWorkdir) {
		String path = pathWorkdir + "\\" + getGetRelativPathQuiz(raetsel) + "\\quiz.xml";
		return path;
	}
}
