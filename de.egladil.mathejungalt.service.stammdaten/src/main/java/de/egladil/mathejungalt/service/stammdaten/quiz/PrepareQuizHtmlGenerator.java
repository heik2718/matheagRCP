/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.service.stammdaten.quiz;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.MatheAGFileWriter;

/**
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class PrepareQuizHtmlGenerator {

	private static final Logger LOG = LoggerFactory.getLogger(PrepareQuizHtmlGenerator.class);

	/**
   *
   */
	public PrepareQuizHtmlGenerator() {
	}

	/**
	 * Generiert die Verzeichnis-Struktur, Latex-Dateien und bat-Dateien, mit denen dann die Html-Dateien in einem
	 * externen Schritt generiert werden können. Wenn alles glatt läuft, befindet sich im Arbeitsverzeichnis eine Datei
	 * namens [raetsel.schluessel]_pre.txt. Dies kann verwendet werden, um das PostHtlatex-Command zu aktivieren.
	 *
	 * @param raetsel
	 * @param pathWorkDir
	 * @return String absoluter Pfad zum Verzeichnis, in dem das Quiz stehen wird.
	 */
	public IStatus generate(MCArchivraetsel raetsel, String pathWorkDir, IProgressMonitor progressMonitor) {
		Assert.isNotNull(raetsel);
		Assert.isNotNull(pathWorkDir);
		String step = "";
		final int workLoop = raetsel.getItems().size() * 2;
		int totalWork = 3 + workLoop;
		SubMonitor subMonitor = SubMonitor.convert(progressMonitor, totalWork);
		try {
			IPrepareHtlatexStep<MCArchivraetsel> firstStep = new QuizDirectoryBuilder();
			subMonitor.beginTask(
				"Generiere Verzeichnisstruktur und Htlatex-Generatoren für " + raetsel.getSchluessel(), totalWork);
			step = "generiere Verzeichnisstruktur";
			subMonitor.subTask(step);
			String pathRaetsel = firstStep.generate(raetsel, pathWorkDir);
			subMonitor.worked(1);
			SubMonitor loopProgress = subMonitor.newChild(workLoop);
			IPrepareHtlatexStep<MCArchivraetselItem> aufgabeLatexStep = new QuizaufgabeLatexGenerator();
			IPrepareHtlatexStep<MCArchivraetselItem> generateProcessCommandStep = new HtLatexProcessorGenerator();
			for (MCArchivraetselItem item : raetsel.getItemsSorted()) {
				step = "generiere LaTeX-Datei für " + item.getNummer();
				loopProgress.subTask(step);
				aufgabeLatexStep.generate(item, pathRaetsel);
				loopProgress.worked(1);

				step = "generiere Process-Htlatex-Datei für " + item.getNummer();
				loopProgress.subTask(step);
				generateProcessCommandStep.generate(item, pathWorkDir);
				loopProgress.worked(1);
			}
			subMonitor.worked(totalWork - 2);
			step = "generiere run-htlatex-Command";
			subMonitor.subTask(step);
			IPrepareHtlatexStep<MCArchivraetsel> lastStep = new RunHtlatexBatGenerator();
			lastStep.generate(raetsel, pathWorkDir);

			step = "generiere readyMarker";
			String pathReadyMarker = QuizPathProvider.getPathReadyMarkerHtlatexPrepare(raetsel, pathWorkDir);
			new MatheAGFileWriter().writeFile(pathReadyMarker, "OK", IFilegenerator.ENCODING_UTF);
			subMonitor.subTask(step);
			subMonitor.worked(totalWork);
			return Status.OK_STATUS;
		} catch (Exception e) {
			LOG.error(e.getMessage());
			String msg = "Fehler bei step '" + step + "': " + e.getMessage();
			IStatus status = new Status(IStatus.ERROR, IStammdatenservice.PLUGIN_ID, msg);
			return status;
		}
	}
}
