/**
 * 
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex;

import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils.ILaTeXDeclarationHeaderGenerator;

/**
 * Ein leerer Declaration-Generator, z.B. für Dateien geeignet, die selbst wieder nur über include in ein Gesamtdokument
 * eingebunden werden.
 * 
 * @author Winkelv
 */
public class BlankHeaderGenerator implements ILaTeXDeclarationHeaderGenerator {

	/**
	 * 
	 */
	public BlankHeaderGenerator() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.core.serviceimpl.internal.generatoren.latex.utils.ILaTeXDeclarationHeaderGenerator#
	 * generateDeclaration(de .egladil.mathe.core.serviceimpl.internal.generatoren.latex.ILaTeXGenerator,
	 * de.egladil.mathe
	 * .core.serviceimpl.internal.generatoren.latex.utils.ILaTeXDeclarationHeaderGenerator.LaTeXSeitenkonfiguration)
	 */
	@Override
	public String generateDeclaration(LaTeXSeitenkonfiguration pSeitenkonfiguration, String pRightSideHeader) {
		return ""; //$NON-NLS-1$
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.serviceimpl.internal.generatoren.latex.utils.ILaTeXDeclarationHeaderGenerator#getIncludePath
	 * ()
	 */
	@Override
	public String getIncludePath() {
		return ""; //$NON-NLS-1$
	}
}
