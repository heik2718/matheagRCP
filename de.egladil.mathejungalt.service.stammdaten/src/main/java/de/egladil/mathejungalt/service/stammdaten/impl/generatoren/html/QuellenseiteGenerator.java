//=====================================================
// Projekt: de.egladil.mathejungalt.service.stammdaten
// (c) Heike Winkelvoß
//=====================================================
// Dateiname: SerieIndexGenerator.java                            $
// $Revision:: 1                                     $
// $Modtime:: 29.03.2015                                $
//=====================================================

package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.service.stammdaten.impl.Messages;

/**
 * QuellenseiteGenerator generiert eine Seite mit dem img mit den Quellenangaben zur Serie. Diese ist nur bei breiten
 * Displays zu erreichen, indem man auf der Archivseite auf den Link namens Q clickt. Die generierte Datei sieht so aus:
 * vorschau/mja_xx-quellen.html
 */
public class QuellenseiteGenerator {

	private static final String HTML_TEMPLATE_PATH = "/html/quellenseite_template.txt";

	private static final String UNTERTITEL_PLACEHOLDER = "PLATZHALTER_UNTERTITEL";

	private static final String IMGLINK_PLACEHOLDER = "IMGLINK";

	private static final Logger LOG = LoggerFactory.getLogger(QuellenseiteGenerator.class);

	/**
	 * Erzeugt eine Instanz von SerieIndexGenerator
	 */
	public QuellenseiteGenerator() {
	}

	/**
	 * Generiert eine Linkliste zu den einzelnen Stufenvorschauen. TODO
	 *
	 * @param stammdatenservice
	 * @return
	 */
	public String generate(Serie serie) {
		InputStream in = null;
		try {
			in = this.getClass().getResourceAsStream(HTML_TEMPLATE_PATH);
			StringWriter sw = new StringWriter();
			IOUtils.copy(in, sw);
			String content = sw.toString();
			content = content.replaceAll(UNTERTITEL_PLACEHOLDER, getReplacementForUntertitel(serie));
			content = content.replaceAll(IMGLINK_PLACEHOLDER, getReplacementForImageLink(serie));
			content = HtmlFactory.setAktuellesDatum(content);
			return content;
		} catch (IOException e) {
			LOG.error(e.getMessage());
			throw new MatheJungAltException("Fehler beim Laden des Templates '" + HTML_TEMPLATE_PATH
				+ "': mal classpath prüfen!");

		} finally {
			IOUtils.closeQuietly(in);
		}
	}

	private String getReplacementForUntertitel(Serie serie) {
		return "Serie " + serie.getNummer() + " - Quellen";
	}

	private String getReplacementForImageLink(Serie serie) {
		String result = Messages.getString("QuellenseiteGenerator_1");
		result = result.replaceAll("13_99", serie.getRunde() + "_" + serie.getNummer());
		return result;
	}

	/**
	 *
	 * @return
	 */
	public String getFilename(Serie serie) {
		return "mja_" + serie.getNummer() + "-quellen.html";
	}
}
