/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.latex;

import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.ITableLabelProvider;

/**
 * LabelProvider f�r die Abstraktion einer Tabelle. Diese umfasst den \section-Deklarationsteil und das }section-ende.
 * Die Schriftgr��e kann gesetzt werden. Der Standard kann konfiguriert werden. Ist er nicht konfiguriert, gilt \\large.
 *
 * @author winkelv
 */
public class LaTeXQuelleTableLabelProvider extends AbstractLaTeXTableLabelProvider implements ITableLabelProvider {

	/**
	 *
	 */
	public LaTeXQuelleTableLabelProvider(String fontSize) {
		super(fontSize);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathe.core.serviceimpl.generatoren.labelprovider.ITableLabelProvider#getStartText()
	 */
	@Override
	public String getStartText() {
		StringBuffer sb = new StringBuffer();

		sb.append("\\qun\n");
		sb.append("{");
		sb.append(getFontSize());

		return sb.toString();
	}
}
