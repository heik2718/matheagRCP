/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.service.stammdaten.quiz;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.EgladilIOException;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;

/**
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class QuizDirectoryBuilder implements IPrepareHtlatexStep<MCArchivraetsel> {

	private static final Logger LOG = LoggerFactory.getLogger(QuizDirectoryBuilder.class);

	/**
   *
   */
	public QuizDirectoryBuilder() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param raetsel
	 * @param pathWorkDir
	 * @return
	 */
	public String generate(MCArchivraetsel raetsel, String pathWorkDir) {
		Assert.isNotNull(raetsel, "raetsel");
		Assert.isNotNull(pathWorkDir, "pathRootDir");
		File quizRoot = null;
		try {
			quizRoot = createSubDir(QuizPathProvider.getGetRelativPathQuiz(raetsel), pathWorkDir);
			String pathQuizRoot = quizRoot.getAbsolutePath();
			for (MCArchivraetselItem item : raetsel.getItems()) {
				createSubDir(QuizPathProvider.getRelativPathQuizitem(item), pathQuizRoot);
			}
			if (LOG.isDebugEnabled()) {
				LOG.debug("Verzeichnisstruktur fertig: [pathQuizRoot=" + pathQuizRoot + "]");
			}
			return pathQuizRoot;
		} catch (IOException e) {
			throw new EgladilIOException("Konnte Verzeichnisbaum fuer Quiz [" + raetsel.getSchluessel()
				+ "] nicht erzeugen:" + e.getMessage(), e);
		}
	}

	/**
	 * @param subdirName
	 * @param parentDir
	 * @throws IOException
	 */
	private File createSubDir(String subdirName, String parentDir) throws IOException {
		File file = new File(parentDir + "/" + subdirName);
		FileUtils.forceMkdir(file);
		return file;
	}
}
