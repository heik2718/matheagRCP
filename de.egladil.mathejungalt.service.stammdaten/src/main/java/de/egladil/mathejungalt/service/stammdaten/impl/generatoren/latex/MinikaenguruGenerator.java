/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.Messages;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.contentprovider.AufgsItemTableContentProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.ITableRowLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.latex.AufgSammlItemTableCellLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.latex.LaTeXMinikaenguruStufeTableLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.latex.MinikaenguruTableRowLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils.ILaTeXDeclarationHeaderGenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.parts.TextTable;

/**
 * @author aheike
 */
public class MinikaenguruGenerator extends AbstractLaTeXGenerator {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(MinikaenguruGenerator.class);

	/** */
	private Minikaenguru minikaenguru;

	/** */
	private TextTable[] aufgabenTextTables;

	/** */
	private final int[] stufen = new int[] { 3, 4, 5 };

	/** */
	private final LaTeXMinikaenguruStufeTableLabelProvider[] tableLabelProviders = new LaTeXMinikaenguruStufeTableLabelProvider[3];

	/** */
	private final ITableRowLabelProvider rowLabelProvider = new MinikaenguruTableRowLabelProvider();

	/** */
	private final ILaTeXDeclarationHeaderGenerator blankHeaderGenerator = new BlankHeaderGenerator();

	/** */
	private boolean ausgabeLoesungen = false;

	/**
	 *
	 */
	public MinikaenguruGenerator() {
		super();
		setPagestyleEmpty(true);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.AbstractLaTeXGenerator#checkInitialized()
	 */
	@Override
	protected void checkInitialized() throws MatheJungAltException {
		if (minikaenguru == null) {
			String msg = "Initialisierungsfehler: erst setMinikaenguru() aufrufen."; //$NON-NLS-1$
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		super.checkInitialized();
	}

	/**
	 * @param pSerie
	 * @param pStufen
	 */
	private void initTableParts() {
		AufgsItemTableContentProvider tableContentProvider;
		aufgabenTextTables = new TextTable[3];

		for (int i = 0; i < stufen.length; i++) {
			aufgabenTextTables[i] = new TextTable(1);
			aufgabenTextTables[i].setHasHeader(false);

			tableLabelProviders[i] = new LaTeXMinikaenguruStufeTableLabelProvider(Messages.getString("font_minikaenguru"));
			tableLabelProviders[i].setStufe(stufen[i]);
			aufgabenTextTables[i].setTableLabelProvider(tableLabelProviders[i]);

			aufgabenTextTables[i].setTableRowLabelProvider(rowLabelProvider);

			AufgSammlItemTableCellLabelProvider cellLabelProvider = new AufgSammlItemTableCellLabelProvider(
				Messages.getString("abstand_minikaenguru"));
			cellLabelProvider.setAnzeigeLoesungen(this.ausgabeLoesungen);
			aufgabenTextTables[i].setCellLabelProvider(cellLabelProvider);

			tableContentProvider = new AufgsItemTableContentProvider(minikaenguru, new int[] { stufen[i] });
			aufgabenTextTables[i].setTableContentProvider(tableContentProvider);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.ILaTeXGenerator#bodyContents()
	 */
	@Override
	public String bodyContents(IStammdatenservice stammdatenservice) throws MatheJungAltException {
		StringBuffer sb = new StringBuffer();
		sb.append("{");
		sb.append(getFontsize());
		sb.append("%==============================================================================\n");
		for (int i = 0; i < aufgabenTextTables.length; i++) {
			sb.append(aufgabenTextTables[i].render(stammdatenservice));
		}
		sb.append("}");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator#generate()
	 */
	@Override
	public String generate(IStammdatenservice stammdatenservice) throws MatheJungAltException {
		checkInitialized();
		initTableParts();
		StringBuffer sb = new StringBuffer();
		sb.append(blankHeaderGenerator.generateDeclaration(null, null));
		sb.append(bodyContents(stammdatenservice));
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator#getFilename()
	 */
	@Override
	public String getFilename() {
		return ausgabeLoesungen ? "loesungen_" + minikaenguru.getJahr() + ".tex" : "aufgaben_" + minikaenguru.getJahr() + ".tex";
	}

	/**
	 * @param pMinikaenguru the minikaenguru to set
	 */
	public void setMinikaenguru(Minikaenguru pMinikaenguru) {
		minikaenguru = pMinikaenguru;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.AbstractLaTeXGenerator#setPagestyleEmpty(boolean
	 * )
	 */
	@Override
	public void setPagestyleEmpty(boolean pPagestyleEmpty) {
		// pagestyle darf nur empty sein.
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.AbstractLaTeXGenerator#getHeaderGenerator()
	 */
	@Override
	public ILaTeXDeclarationHeaderGenerator getHeaderGenerator() {
		return blankHeaderGenerator;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.AbstractLaTeXGenerator#setHeaderGenerator(de
	 * .egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils.ILaTeXDeclarationHeaderGenerator)
	 */
	@Override
	public void setHeaderGenerator(ILaTeXDeclarationHeaderGenerator pHeaderGenerator) {
		// es wird nur der blankHeaderGenerator verwendet
	}

	/**
	 * @param pAusgabeLoesungen the ausgabeLoesungen to set
	 */
	public void setAusgabeLoesungen(boolean pAusgabeLoesungen) {
		ausgabeLoesungen = pAusgabeLoesungen;
	}

}
