//=====================================================
// Projekt: de.egladil.mathejungalt.service.stammdaten
// (c) Heike Winkelvoß
//=====================================================
// Dateiname: SerieIndexGenerator.java                            $
// $Revision:: 1                                     $
// $Modtime:: 29.03.2015                                $
//=====================================================

package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.SimpleDateFormat;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.service.stammdaten.impl.Messages;

/**
 * QuellenseiteGenerator generiert eine Seite mit dem img mit den Quellenangaben zur Serie. Diese ist nur bei breiten
 * Displays zu erreichen, indem man auf der Archivseite auf den Link namens Q clickt. Die generierte Datei sieht so aus:
 * vorschau/mja_xx-quellen.html
 */
public class SerienseiteGenerator {

	private static final String HTML_TEMPLATE_PATH = "/html/serienseite_template.txt";

	private static final String UL_PLACEHOLDER = "PLATZHALTER_UL";

	private static final String EINSENDESCHLUSS_PLACEHOLDER = "PLATZHALTER_EINSENDESCHLUSS";

	private static final Logger LOG = LoggerFactory.getLogger(SerienseiteGenerator.class);

	/**
	 * Erzeugt eine Instanz von SerieIndexGenerator
	 */
	public SerienseiteGenerator() {
	}

	/**
	 * Generiert eine Linkliste zu den einzelnen Stufenvorschauen. TODO
	 *
	 * @param stammdatenservice
	 * @return
	 */
	public String generate(Serie serie) {
		InputStream in = null;
		try {
			in = this.getClass().getResourceAsStream(HTML_TEMPLATE_PATH);
			StringWriter sw = new StringWriter();
			IOUtils.copy(in, sw);
			String content = sw.toString();
			content = content.replaceAll(UL_PLACEHOLDER, getReplacementForHerunterladenLinks(serie));
			content = content.replaceAll(EINSENDESCHLUSS_PLACEHOLDER, getReplacementForEinsendeschluss(serie));
			content = HtmlFactory.setAktuellesDatum(content);
			return content;
		} catch (IOException e) {
			LOG.error(e.getMessage());
			throw new MatheJungAltException("Fehler beim Laden des Templates '" + HTML_TEMPLATE_PATH + "': mal classpath prüfen!");

		} finally {
			IOUtils.closeQuietly(in);
		}
	}

	private String getReplacementForHerunterladenLinks(Serie serie) {
		StringBuffer sb = new StringBuffer();
		sb.append(Messages.getString("SerienseiteGenerator_1"));
		String text2 = Messages.getString("SerienseiteGenerator_2");
		for (int i = 1; i < 7; i++) {
			sb.append(text2);
			sb.append(serie.getRunde());
			sb.append("_");
			sb.append(serie.getNummer());
			sb.append("_");
			sb.append(i);
			sb.append(".pdf\" target=\"_blank\">");
			sb.append(getStufeText(i));
			sb.append("</a></li>");
		}
		sb.append("</ul>");
		return sb.toString();
	}

	private String getReplacementForEinsendeschluss(Serie serie) {
		return new SimpleDateFormat("dd.MM.yyyy").format(serie.getDatum());
	}

	/**
	 *
	 * @return
	 */
	public String getFilename(Serie serie) {
		return "serie.html";
	}

	private String getStufeText(int stufe) {
		switch (stufe) {
		case 1:
			return "Vorschule";
		case 2:
			return "Klassen 1 und 2";
		case 3:
			return "Klassen 3 und 4";
		case 4:
			return "Klassen 5 und 6";
		case 5:
			return "Klassen 7 und 8";
		case 6:
			return "ab Klasse 9";
		default:
			return "FEHLER: falsche Stufe!";
		}
	}
}
