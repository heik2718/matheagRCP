/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.runtime.IStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheAGObjectNotGeneratableException;
import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.file.DirectoryCleaner;
import de.egladil.base.latex.ILaTeXService;
import de.egladil.base.latex.ILaTeXService.LaTeXInteractionModes;
import de.egladil.base.utils.OsgiServiceLocator;
import de.egladil.mathejungalt.config.IMatheJungAltConfiguration;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.mitglieder.Diplom;
import de.egladil.mathejungalt.domain.mitglieder.EmailAdresse;
import de.egladil.mathejungalt.domain.mitglieder.IMitgliedNames;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.domain.mitglieder.Serienteilnahme;
import de.egladil.mathejungalt.persistence.api.IPersistenceservice;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.GenerierbarkeitsChecker;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.MatheAGGeneratorUtils;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html.FruehstarterseiteGenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html.MitgliederseiteGenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.reports.MitgliedReportUtil;

/**
 * @author aheike
 */
public class Mitgliederservice extends AbstractStammdatenservice {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(Mitgliederservice.class);

	/** */
	private Map<String, Mitglied> mitgliederMap;

	/** */
	private int nextMitgliedSchluesselInt = -1;

	/** */
	private MitgliederseiteGenerator mitgliederseiteGenerator = new MitgliederseiteGenerator();

	/** */
	private FruehstarterseiteGenerator fruehstarterseiteGenerator = new FruehstarterseiteGenerator();

	/** */
	private Aufgabensammlungservice aufgabensammlungservice = new Aufgabensammlungservice();

	/** */
	private final GenerierbarkeitsChecker generierbarkeitsChecker = new GenerierbarkeitsChecker();

	/** */
	private final MitgliedReportUtil mitgliedReportUtil = new MitgliedReportUtil();

	/**
	 * @param pPersistenceservice
	 */
	public Mitgliederservice() {
		super();
	}

	/**
	 * @return
	 */
	public Collection<Mitglied> getMitglieder() {
		checkMitgliedermap();
		return mitgliederMap.values();
	}

	/**
	 * @param pKlasse
	 * @param pAktiv boolean falls true, dann nur aktive, sonst alle
	 * @return
	 */
	protected List<Mitglied> findByKlasse(final int pKlasse, final boolean pAktiv) {
		checkMitgliedermap();
		final Collection<Mitglied> alle = mitgliederMap.values();
		final List<Mitglied> result = new ArrayList<Mitglied>();
		for (final Mitglied mitglied : alle) {
			if (mitglied.getKlasse() != null && mitglied.getKlasse().intValue() == pKlasse) {
				if (pAktiv && mitglied.isAktiv()) {
					result.add(mitglied);
				}
				if (!pAktiv) {
					result.add(mitglied);
				}
			}
		}
		return result;
	}

	/**
	 * Falls die MItgliedermap nicht initialisiert ist, wird sie hier initialisiert.
	 */
	private void checkMitgliedermap() {
		if (mitgliederMap == null) {
			mitgliederMap = new HashMap<String, Mitglied>();
			final List<Mitglied> mitglieder = getPersistenceservice().getMitgliederRepository().findAll();
			for (final Mitglied mitglied : mitglieder) {
				if (!mitglied.isDiplomeLoaded()) {
					diplomeZuMitgliedLaden(mitglied);
				}
				if (!mitglied.isSerienteilnahmenLoaded()) {
					serienteilnahmenZuMitgliedLaden(mitglied);
				}
				// mitgliederMap.put(mitglied.getUuid(), mitglied);
				mitgliederMap.put(mitglied.getSchluessel(), mitglied);
			}
		}
	}

	/**
	 * @param pMitglied
	 * @throws MatheJungAltException
	 */
	protected void mitgliedSpeichern(final Mitglied pMitglied) throws MatheJungAltException {
		final boolean neu = pMitglied.isNew();
		getPersistenceservice().getMitgliederRepository().saveMitglied(pMitglied);
		if (neu) {
			// mitgliederMap.put(pMitglied.getUuid(), pMitglied);
			mitgliederMap.put(pMitglied.getSchluessel(), pMitglied);
			fireObjectAddedEvent(pMitglied);
		}
	}

	/**
	 * @param pMitglied
	 * @throws MatheJungAltException
	 */
	protected void mitgliedAktivierungAendern(final Mitglied pMitglied) throws MatheJungAltException {
		getPersistenceservice().getMitgliederRepository().saveMitglied(pMitglied);
		fireObjectAddedEvent(pMitglied);
	}

	/**
	 * @param pAdresse
	 * @throws MatheJungAltException
	 */
	protected void mailadresseAusMailinglisteEntfernen(final String pAdresse) throws MatheJungAltException {
		if (pAdresse == null || pAdresse.length() == 0) {
			final String msg = "Die Mailadresse war null oder leer";
			LOG.warn(msg);
			throw new MatheJungAltException(msg);
		}
		try {
			getPersistenceservice().getMailinglisteRepository().remove(pAdresse);
		} catch (final Exception e) {
			LOG.error(e.getMessage(), e);
			throw new MatheJungAltException("Fehler beim Entfernen eines Kontakts.", e);
		}
	}

	/**
	 * @param pAdresse
	 * @throws MatheJungAltException
	 */
	protected void mailadresseZurMailinglisteHinzufuegen(final String pAdresse) throws MatheJungAltException {
		if (pAdresse == null || pAdresse.length() == 0) {
			final String msg = "Die Mailadresse war null oder leer";
			LOG.warn(msg);
			throw new MatheJungAltException(msg);
		}
		try {
			getPersistenceservice().getMailinglisteRepository().save(pAdresse);
		} catch (final Exception e) {
			LOG.error(e.getMessage(), e);
			throw new MatheJungAltException("Fehler beim Hinzufuegen eines Kontakts:\n" + e.getMessage(), e);
		}
	}

	/**
	 * @return
	 * @throws MatheJungAltException
	 */
	protected List<String> emailadressenLaden() throws MatheJungAltException {
		throw new MatheJungAltException(this.getClass().getName() + "emailadressenLaden() nicht implementiert!");
	}

	/**
	 * Prueft, ob die gegebene Adresse in der Mailingliste enthalten ist.
	 *
	 * @param pAdresse
	 * @return
	 * @throws MatheJungAltException
	 */
	protected boolean mailadresseInMailinglisteEnthalten(final String pAdresse) throws MatheJungAltException {
		if (pAdresse == null || pAdresse.length() == 0) {
			final String msg = "Die Mailadresse war null oder leer";
			LOG.warn(msg);
			throw new MatheJungAltException(msg);
		}
		final EmailAdresse adresse = getPersistenceservice().getMailinglisteRepository().findByAdresse(pAdresse);
		return adresse != null;
	}

	/**
	 * Läd die Serienteilnahmen zum gegebenen Mitglied.
	 *
	 * @param pMitglied
	 * @return {@link Collection} entweder gefällt oder leer, aber nicht null.
	 * @throws MatheJungAltException
	 */
	protected Collection<Serienteilnahme> serienteilnahmenZuMitgliedLaden(final Mitglied pMitglied)
		throws MatheJungAltException {
		if (pMitglied == null) {
			final String msg = MatheJungAltException.argumentNullMessage("Mitglied");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (!pMitglied.isSerienteilnahmenLoaded()) {
			getPersistenceservice().getMitgliederRepository().loadSerienteilnahmen(pMitglied);
		}
		return pMitglied.getSerienteilnahmen();
	}

	/**
	 * Gibt alle Mitglieder zurueck, die in der gegebenen Runde einen Fruehstarterpunkt haben.
	 *
	 * @param pRunde int die Runde
	 * @return {@link List} von {@link Mitglied}
	 */
	protected List<Mitglied> findMitgliederMitFruehstarterInRunde(final int pRunde) throws MatheJungAltException {
		final Collection<Serie> serien = aufgabensammlungservice.getSerien(pRunde);
		checkMitgliedermap();
		final Collection<Mitglied> alle = mitgliederMap.values();
		final List<Mitglied> result = new ArrayList<Mitglied>();
		for (final Mitglied mitglied : alle) {
			if (!mitglied.isSerienteilnahmenLoaded()) {
				serienteilnahmenZuMitgliedLaden(mitglied);
			}
			for (final Serienteilnahme teilnahme : mitglied.getSerienteilnahmen()) {
				if (serien.contains(teilnahme.getSerie()) && teilnahme.getFruehstarter() > 0.0) {
					result.add(mitglied);
					break;
				}
			}

		}
		return result;
	}

	/**
	 * L�d die Diplome zum gegebenen Mitglied.
	 *
	 * @param pMitglied
	 * @return {@link Collection} entweder gef�llt oder leer, aber nicht null.
	 * @throws MatheJungAltException
	 */
	protected Collection<Diplom> diplomeZuMitgliedLaden(final Mitglied pMitglied) throws MatheJungAltException {
		if (pMitglied == null) {
			final String msg = MatheJungAltException.argumentNullMessage("Mitglied");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (!pMitglied.isDiplomeLoaded()) {
			getPersistenceservice().getMitgliederRepository().loadDiplome(pMitglied);
		}
		return pMitglied.getDiplome();
	}

	/**
	 * Entfernt die gegebene Serienteilnahme wieder.
	 *
	 * @param pMitglied
	 * @param pSerie
	 */
	protected void serienteilnahmeLoeschen(final Mitglied pMitglied, final Serie pSerie) {
		if (pMitglied == null || pSerie == null) {
			final String msg = MatheJungAltException.argumentNullMessage("Mitglied oder Serie");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		final Serienteilnahme serienteilnahme = serienteilnahmeFinden(pMitglied, pSerie);
		if (serienteilnahme == null) {
			return;
		}
		if (pMitglied.removeSerienteilnahme(serienteilnahme)) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Teilnahme an Serie " + pSerie.getNummer() + " bei Mitglied " + pMitglied.toString()
					+ " geloescht");
			}
		}
	}

	/**
	 * @param pMitglied
	 * @param pSerie
	 * @return Serienteilnahme
	 */
	protected Serienteilnahme serienteilnahmeFinden(final Mitglied pMitglied, final Serie pSerie) {
		if (!pMitglied.isSerienteilnahmenLoaded()) {
			getPersistenceservice().getMitgliederRepository().loadSerienteilnahmen(pMitglied);
		}
		for (final Serienteilnahme teilnahme : pMitglied.getSerienteilnahmen()) {
			if (teilnahme.getSerie().equals(pSerie)) {
				return teilnahme;
			}
		}
		return null;
	}

	/**
	 * Erzeugt eine neue {@link Serienteilnahme} für das {@link Mitglied} in der {@link Serie}.
	 *
	 * @param pMitglied {@link Mitglied} das Mitglied
	 * @param pSerie {@link Serie} die Serie
	 */
	protected void mitgliedNimmtTeil(final Mitglied pMitglied, final Serie pSerie) throws MatheJungAltException {
		if (pMitglied == null || pSerie == null) {
			final String msg = MatheJungAltException.argumentNullMessage("Mitglied oder Serie");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (serienteilnahmeFinden(pMitglied, pSerie) != null) {
			return;
		}
		final Serienteilnahme teilnahme = new Serienteilnahme();
		teilnahme.setMitglied(pMitglied);
		teilnahme.setSerie(pSerie);
		teilnahme.setPunkte(2);
		teilnahme.setFruehstarter(0.0);
		pMitglied.addSerienteilname(teilnahme);
	}

	/**
	 * @return
	 */
	protected Mitglied mitgliedAnlegen() {
		final Mitglied mitglied = new Mitglied();
		mitglied.setSchluessel(nextKey());
		mitglied.setSerienteilnahmenLoaded(true);
		mitglied.setDiplomeLoaded(true);
		return mitglied;
	}

	/**
	 * Sucht den maximalen fachlichen Schl�ssel.
	 *
	 * @return String
	 */
	private String nextKey() {
		if (nextMitgliedSchluesselInt < 0) {
			nextMitgliedSchluesselInt = Integer.valueOf(getPersistenceservice().getMitgliederRepository().getMaxKey())
				.intValue() + 1;
		}
		String schl = "" + (nextMitgliedSchluesselInt);
		if (schl.length() < IMitgliedNames.LENGTH_SCHLUESSEL) {
			schl = StringUtils.leftPad(schl, IMitgliedNames.LENGTH_SCHLUESSEL, '0');
		}
		nextMitgliedSchluesselInt++;
		return schl;
	}

	/**
	 * Mitgliederseiten sind generierbar, wenn es kein Mitglied gibt, dem ein Diplom fehlt.
	 *
	 * @return
	 */
	protected boolean canGenerate() throws MatheAGObjectNotGeneratableException {
		for (final Mitglied mitglied : mitgliederMap.values()) {
			if (!mitglied.isDiplomeLoaded()) {
				diplomeZuMitgliedLaden(mitglied);
			}
			if (!mitglied.isSerienteilnahmenLoaded()) {
				serienteilnahmenZuMitgliedLaden(mitglied);
			}
			generierbarkeitsChecker.visit(mitglied);
		}
		return true;
	}

	/**
	 * @param pMitglied
	 * @return
	 */
	protected boolean mitgliedBrauchtDiplom(final Mitglied pMitglied) {
		final MitgliedReportUtil reportUtil = new MitgliedReportUtil();
		return reportUtil.brauchtDiplom(pMitglied);
	}

	/**
	 * Dem Mitglied ein Diplom geben.
	 *
	 * @param pMitglied
	 * @return Diplom
	 * @throws MatheJungAltException in diesem Fall wird das zugeordnete Diplom wieder entfernt.
	 */
	protected Diplom diplomGeben(final Mitglied pMitglied) throws MatheJungAltException {
		if (pMitglied == null) {
			final String msg = MatheJungAltException.argumentNullMessage("Mitglied");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (!mitgliedBrauchtDiplom(pMitglied)) {
			throw new MatheJungAltException("Mitglied: die Punkte reichen nicht f�r ein Diplom!");
		}
		final int anzahlDiplomErfinderpunkte = mitgliedReportUtil.berechneAnzahlErfinderpunkteFuerDiplom(pMitglied);
		final Diplom diplom = new Diplom();
		diplom.setDatum(new Date());
		diplom.setAnzahlErfinderpunkte(anzahlDiplomErfinderpunkte);
		diplom.setMitglied(pMitglied);
		pMitglied.addDiplom(diplom);
		try {
			getPersistenceservice().getMitgliederRepository().saveDiplom(diplom);
		} catch (final Exception e) {
			final String msg = "Fehler beim Geben des Diploms: " + e.getMessage();
			LOG.error(msg, e);
			pMitglied.removeDiplom(diplom);
			throw new MatheJungAltException(msg, e);
		}
		return diplom;
	}

	/**
	 * @param pPath
	 */
	protected void generate(final String pPath, final IStammdatenservice stammdatenservice) {
		MatheAGGeneratorUtils.writeOutput(pPath, mitgliederseiteGenerator.getFilename(),
			mitgliederseiteGenerator.generate(stammdatenservice));
		MatheAGGeneratorUtils.writeOutput(pPath, fruehstarterseiteGenerator.getFileName(),
			fruehstarterseiteGenerator.generate(stammdatenservice));
	}

	/**
	 * @param confguration
	 */
	public final void setConfiguration(final IMatheJungAltConfiguration confguration) {
		aufgabensammlungservice.setConfiguration(confguration);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.AbstractStammdatenservice#setPersistenceservice(de.egladil.
	 * mathejungalt.persistence.api.IPersistenceservice)
	 */
	@Override
	public void setPersistenceservice(final IPersistenceservice pPersistenceservice) {
		super.setPersistenceservice(pPersistenceservice);
		aufgabensammlungservice.setPersistenceservice(pPersistenceservice);
	}

	/**
	 * Werfen temporäre Dateien weg.
	 *
	 * @param pPathWorkDir
	 */
	protected void clearTemporaryLaTeXFiles(final String pPathWorkDir) {
		final List<String> suffixes = Arrays.asList(new String[] { "aux", "dvi", "log", "out" });
		final DirectoryCleaner cleaner = new DirectoryCleaner();
		cleaner.deleteFiles(pPathWorkDir, suffixes);
	}

	/**
	 * @param pLatexFilePath
	 * @return IStatus
	 */
	protected IStatus compile(final String pLatexFilePath) {
		final ILaTeXService latexService = new OsgiServiceLocator().getService(ILaTeXService.class);
		final IStatus status = latexService.compile(pLatexFilePath, LaTeXInteractionModes.nonstopmode);
		return status;
	}

	/**
	 * Aufruf von pdflatex
	 *
	 * @param pLatexFilePath
	 * @return IStatus
	 */
	protected IStatus pdflatex(final String pLatexFilePath) {
		final ILaTeXService latexService = new OsgiServiceLocator().getService(ILaTeXService.class);
		final IStatus status = latexService.compile(pLatexFilePath, LaTeXInteractionModes.nonstopmode);
		return status;
	}

	/**
	 * Wandelt dvi in ps um.
	 *
	 * @param pWorkDir
	 * @param dviFilePath
	 * @return IStatus
	 */
	protected IStatus dviPs(final String dviFilePath) {
		final ILaTeXService latexService = new OsgiServiceLocator().getService(ILaTeXService.class);
		final IStatus status = latexService.dviPs(dviFilePath);
		return status;
	}
}
