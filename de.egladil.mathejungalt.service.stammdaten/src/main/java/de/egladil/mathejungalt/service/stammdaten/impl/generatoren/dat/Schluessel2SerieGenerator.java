/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.dat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.Messages;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.contentprovider.AufgsItemTableContentProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.BlankTableLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.BlankTableRowLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.dat.Schluessel2SerieTableCellLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.parts.TextTable;

/**
 * @author winkelv
 */
public class Schluessel2SerieGenerator implements IFilegenerator {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(Schluessel2SerieGenerator.class);

	/** */
	private TextTable table;

	/** */
	private Serie serie;

	/** */
	private boolean initialized = false;

	/**
	 *
	 */
	public Schluessel2SerieGenerator() {
		super();
	}

	/**
	 * @param pSerie
	 * @param pStammdatenservice
	 */
	private void init() {
		table = new TextTable(1);
		table.setTableContentProvider(new AufgsItemTableContentProvider(serie, new int[] { 1, 2, 3, 4, 5, 6 }));
		table.setTableRowLabelProvider(new BlankTableRowLabelProvider());
		table.setHasHeader(false);
		table.setCellLabelProvider(new Schluessel2SerieTableCellLabelProvider("\\n"));
		table.setTableLabelProvider(new BlankTableLabelProvider());
		initialized = true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathe.core.serviceimpl.generatoren.IFilegenerator#generate()
	 */
	@Override
	public String generate(IStammdatenservice stammdatenservice) throws MatheJungAltException {
		if (serie == null) {
			String msg = "Fehler: die Serie ist noch nicht gesetzt: zuerst setSerie() aufrufen!";
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		} else {
			LOG.info("generiere Serie " + serie.getNummer());
		}
		if (!initialized) {
			init();
		}
		return table.render(stammdatenservice);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathe.core.serviceimpl.generatoren.IFilegenerator#getFilename()
	 */
	@Override
	public String getFilename() {
		return "schluessel2serie.dat";
	}

	/**
	 * @param pSerie the serie to set
	 */
	public void setSerie(Serie pSerie) {
		serie = pSerie;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator#getEncoding()
	 */
	@Override
	public String getEncoding() {
		return IFilegenerator.ENCODING_UTF;
	}
}
