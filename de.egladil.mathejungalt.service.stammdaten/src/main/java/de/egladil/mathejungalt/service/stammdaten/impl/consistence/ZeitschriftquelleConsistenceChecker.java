/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.consistence;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.exceptions.MatheJungAltInconsistentEntityException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.DomainObjectProxyClassifier;
import de.egladil.mathejungalt.domain.quellen.Zeitschriftquelle;

/**
 * Kosistenzprüfer für Quelle-Entities.
 *
 * @author Heike Winkelvoss (www.egladil.de)
 */
public class ZeitschriftquelleConsistenceChecker implements IConsistencecheckProvider {

	/** */
	private final ConsistenceCheckDelegate consistenceCheckDelegate;

	/**
	 *
	 */
	public ZeitschriftquelleConsistenceChecker() {
		consistenceCheckDelegate = new ConsistenceCheckDelegate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.consistencecheck.IConsistencecheckProvider#canCheck(de.egladil.mathejungalt.domain
	 * .AbstractMatheAGObject)
	 */
	@Override
	public boolean canCheck(Object pObject) {
		return DomainObjectProxyClassifier.isZeitschriftquelle(pObject);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.consistencecheck.IConsistencecheckProvider#checkConsistent(de.egladil.mathejungalt
	 * .domain.AbstractMatheAGObject)
	 */
	@Override
	public void checkConsistent(AbstractMatheAGObject pObject) throws MatheJungAltException {
		Zeitschriftquelle quelle = (Zeitschriftquelle) pObject;
		consistenceCheckDelegate.checkStringKey(quelle.getKey(), 6, "Zeitschriftquelle:");
		if (quelle.getZeitschrift() == null)
			throw new MatheJungAltInconsistentEntityException("Zeitschtriftquelle: die Zeitschrift fehlt.");
		if (quelle.getJahrgang() == null)
			throw new MatheJungAltInconsistentEntityException("Zeitschtriftquelle: der Jahrgang fehlt.");
		if (quelle.getAusgabe() == null)
			throw new MatheJungAltInconsistentEntityException("Zeitschtriftquelle: die Ausgabe fehlt.");
	}
}
