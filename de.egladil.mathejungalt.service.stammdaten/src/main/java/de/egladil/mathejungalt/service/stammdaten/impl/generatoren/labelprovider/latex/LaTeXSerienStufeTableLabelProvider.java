/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.latex;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.MatheAGGeneratorUtils;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.ITableLabelProvider;

/**
 * LabelProvider f�r die Abstraktion einer Tabelle. Diese umfasst den \section-Deklarationsteil und das }section-ende.
 * Die Schriftgr��e kann gesetzt werden. Der Standard kann konfiguriert werden. Ist er nicht konfiguriert, gilt \\large.
 *
 * @author winkelv
 */
public class LaTeXSerienStufeTableLabelProvider extends AbstractLaTeXTableLabelProvider implements ITableLabelProvider {

	/** */
	private static final Logger log = LoggerFactory.getLogger(LaTeXSerienStufeTableLabelProvider.class);

	/** */
	private int stufe = -1;

	/**
	 *
	 */
	public LaTeXSerienStufeTableLabelProvider(String fontSize) {
		super(fontSize);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathe.core.serviceimpl.generatoren.labelprovider.ITableLabelProvider#getStartText()
	 */
	@Override
	public String getStartText() {
		if (stufe < 1) {
			String msg = "Stufe nicht gesetzt: erst setStufe() aufrufen.";
			log.error(msg);
			throw new MatheJungAltException(msg);
		}
		StringBuffer sb = new StringBuffer();

		int sectCounter = stufe - 1;
		sb.append("\\setcounter{section}{");
		sb.append(sectCounter);
		sb.append("}\n");
		sb.append("\\section{");
		sb.append(MatheAGGeneratorUtils.stufeText(stufe));
		sb.append("}\n{");
		sb.append(getFontSize());
		return sb.toString();
	}

	/**
	 * @param pStufe the stufe to set
	 */
	public void setStufe(int pStufe) {
		if (pStufe < 0 || pStufe > 6) {
			String msg = "ungueltige Stufe " + pStufe + ": nur 1 bis 6 erlaubt";
			log.error(msg);
			throw new MatheJungAltException(msg);
		}
		stufe = pStufe;
	}

}
