package de.egladil.mathejungalt.service.stammdaten.impl.generatoren;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheAGObjectNotGeneratableException;
import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.service.stammdaten.impl.reports.MitgliedReportUtil;

/**
 * Pr�ft ein Domain-Objekt, ob es generierbar ist.
 *
 * @author Heike Winkelvoß (www.egladil.de)
 */
public class GenerierbarkeitsChecker {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(GenerierbarkeitsChecker.class);

	/** */
	private final MitgliedReportUtil mitgliedReportUtil = new MitgliedReportUtil();

	/**
	 *
	 */
	public GenerierbarkeitsChecker() {
		super();
	}

	/**
	 * @param pAufgabe
	 * @throws MatheAGServiceRuntimeException
	 */
	private final void visit(Aufgabe pAufgabe) throws MatheAGObjectNotGeneratableException {
		if (pAufgabe == null) {
			throw new MatheJungAltException(
				"Die Aufgabensammlung kann nicht generiert werden. Eine der Aufgaben ist null!");
		}
		if (pAufgabe.getSchluessel() == null) {
			throw new MatheAGObjectNotGeneratableException(
				"Die Aufgabensammlung kann nicht generiert werden. Der Schluessel einer Aufgabe ist null!");
		}
		if (!pAufgabe.getQuelle().isCompletelyLoaded())
			throw new MatheAGObjectNotGeneratableException(
				"Die Aufgabensammlung kann nicht generiert werden. Die Quelle einer Aufgabe ist noch nicht geladen!");
	}

	/**
	 * Prüft, ob die Serie generiert werden kann.
	 *
	 * @param pSerie eine {@link Serie}.
	 * @param pCheckItems boolean true, falls die Prüfung auch die Serienitems beinhalten soll. Anderenfalls werden alle
	 * Attribute außer den Serieitems geprüft.
	 * @throws MatheAGObjectNotGeneratableException falls ein Item null ist oder etwas gegen das Generieren spricht.
	 * @throws MatheAGAssertionFailedException falls der Parameter null war.
	 */
	public final void visit(final Serie pSerie, boolean pCheckItems) throws MatheAGObjectNotGeneratableException {
		if (pSerie == null) {
			String msg = MatheJungAltException.argumentNullMessage("Serie");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (pSerie.isNew())
			throw new MatheAGObjectNotGeneratableException("Die Serie " + pSerie.toString()
				+ " kann nicht generiert werden. Sie ist noch nicht gespeichert.");
		if (pSerie.getMonatJahrText() == null) {
			throw new MatheAGObjectNotGeneratableException("Die Serie " + pSerie.toString()
				+ " kann nicht generiert werden. Der Monat- Jahr- Text ist null");
		}
		if (pSerie.getMonatJahrText().length() == 0) {
			throw new MatheAGObjectNotGeneratableException("Die Serie " + pSerie.toString()
				+ " kann nicht generiert werden. Der Monat- Jahr- Text fehlt!");
		}
		if (pCheckItems && pSerie.getItems() == null) {
			throw new MatheAGObjectNotGeneratableException("Die Serie " + pSerie.toString()
				+ " kann nicht generiert werden. Die Serienitems sind null!");
		}
		if (pCheckItems && pSerie.anzahlAufgaben() == 0) {
			throw new MatheAGObjectNotGeneratableException("Die Serie " + pSerie.toString()
				+ " kann nicht generiert werden. Es gibt noch keine Items!");
		}
		if (pSerie.getDatum() == null) {
			throw new MatheAGObjectNotGeneratableException("Die Serie " + pSerie.toString()
				+ " kann nicht generiert werden. Der Einsendeschluss ist null");
		}
		if (pCheckItems) {
			for (IAufgabensammlungItem item : pSerie.getItems()) {
				visit(item);
			}
		}
	}

	/**
	 * Prüft, ob {@link Minikaenguru} generiert werden kann.
	 *
	 * @param pMinikaenguru eine {@link Minikaenguru}.
	 * @param pCheckItems boolean true, falls die Prüfung auch die Items beinhalten soll. Anderenfalls werden alle
	 * Attribute außer den Aufgabensammlungitems geprüft.
	 * @throws MatheAGObjectNotGeneratableException falls ein Item null ist oder etwas gegen das Generieren spricht.
	 * @throws MatheAGAssertionFailedException falls der Parameter null war.
	 */
	public final void visit(final Minikaenguru pMinikaenguru, boolean pCheckItems)
		throws MatheAGObjectNotGeneratableException {
		if (pMinikaenguru == null) {
			String msg = MatheJungAltException.argumentNullMessage("Minikaenguru");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (pMinikaenguru.isNew())
			throw new MatheAGObjectNotGeneratableException("Der Minikaenguru-Wettbewerb " + pMinikaenguru.toString()
				+ " kann nicht generiert werden. Sie ist noch nicht gespeichert.");
		if (pMinikaenguru.getJahr() == null) {
			throw new MatheAGObjectNotGeneratableException("Der Minikaenguru-Wettbewerb " + pMinikaenguru.toString()
				+ " kann nicht generiert werden. Das Jahr ist null");
		}
		if (pCheckItems && pMinikaenguru.getItems() == null) {
			throw new MatheAGObjectNotGeneratableException("Der Minikaenguru-Wettbewerb " + pMinikaenguru.toString()
				+ " kann nicht generiert werden. Die Items sind null!");
		}
		if (pCheckItems && pMinikaenguru.anzahlAufgaben() == 0) {
			throw new MatheAGObjectNotGeneratableException("Der Minikaenguru-Wettbewerb " + pMinikaenguru.toString()
				+ " kann nicht generiert werden. Es gibt noch keine Items!");
		}
		if (pCheckItems) {
			for (IAufgabensammlungItem item : pMinikaenguru.getItems()) {
				visit(item);
			}
		}
	}

	/**
	 * Prüft, ob das {@link Arbeitsblatt} generiert werden kann.
	 *
	 * @param pArbeitsblatt eine {@link Arbeitsblatt}.
	 * @param pCheckItems boolean true, falls die Prüfung auch die Items beinhalten soll. Anderenfalls werden alle
	 * Attribute außer den Aufgabensammlungitems geprüft.
	 * @throws MatheAGObjectNotGeneratableException falls ein Item null ist oder etwas gegen das Generieren spricht.
	 * @throws MatheAGAssertionFailedException falls der Parameter null war.
	 */
	public final void visit(final Arbeitsblatt pArbeitsblatt, boolean pCheckItems)
		throws MatheAGObjectNotGeneratableException {
		if (pArbeitsblatt == null) {
			String msg = MatheJungAltException.argumentNullMessage("Arbeitsblatt");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (pArbeitsblatt.isNew())
			throw new MatheAGObjectNotGeneratableException("Das Arbeitsblatt " + pArbeitsblatt.toString()
				+ " kann nicht generiert werden. Sie ist noch nicht gespeichert.");
		if (pArbeitsblatt.getTitel() == null || pArbeitsblatt.getTitel().isEmpty()) {
			throw new MatheAGObjectNotGeneratableException("Das Arbeitsblatt " + pArbeitsblatt.toString()
				+ " kann nicht generiert werden. Der Titel null oder leer");
		}
		if (pCheckItems && pArbeitsblatt.getItems() == null) {
			throw new MatheAGObjectNotGeneratableException("Das Arbeitsblatt " + pArbeitsblatt.toString()
				+ " kann nicht generiert werden. Die Items sind null!");
		}
		if (pCheckItems && pArbeitsblatt.anzahlAufgaben() == 0) {
			throw new MatheAGObjectNotGeneratableException("Das Arbeitsblatt " + pArbeitsblatt.toString()
				+ " kann nicht generiert werden. Es gibt noch keine Items!");
		}
		if (pCheckItems) {
			for (IAufgabensammlungItem item : pArbeitsblatt.getItems()) {
				visit(item);
			}
		}
	}

	/**
	 * @param pItem
	 * @throws MatheAGObjectNotGeneratableException
	 */
	public void visit(IAufgabensammlungItem pItem) throws MatheAGObjectNotGeneratableException {
		if (pItem == null) {
			String msg = MatheJungAltException.argumentNullMessage("IAufgabensammlungItem");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (pItem.getNummer() == null || pItem.getNummer().isEmpty()) {
			throw new MatheAGObjectNotGeneratableException(
				"Die Aufgabensammlung kann nicht generiert werden: beim Item " + pItem.toString()
					+ " ist die Nummer null oder leer.");
		}
		try {
			visit(pItem.getAufgabe());
		} catch (MatheAGObjectNotGeneratableException e) {
			String msg = e.getMessage() + ": item " + pItem.getNummer();
			throw new MatheAGObjectNotGeneratableException(msg);
		}
	}

	/**
	 * Prüft, ob das Mitglied alle nötigen Diplome hat.
	 *
	 * @param pMitglied
	 * @throws MatheAGObjectNotGeneratableException falls sich das Mitglied nicht generieren lässt.
	 */
	public final void visit(Mitglied pMitglied) throws MatheAGObjectNotGeneratableException {
		if (pMitglied == null) {
			String msg = MatheJungAltException.argumentNullMessage("Mitglied");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (mitgliedReportUtil.brauchtDiplom(pMitglied)) {
			throw new MatheAGObjectNotGeneratableException("Das Mitglied " + pMitglied.toString()
				+ " braucht ein Diplom");
		}

	}
}