package de.egladil.mathejungalt.service.stammdaten.impl.generatoren;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * Utilities zur Generierung von Textbausteinen für Files.
 *
 * @author Heike Winkelvoß (www.egladil.de)
 */
public final class MatheAGGeneratorUtils {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(MatheAGGeneratorUtils.class);

	/** */
	// FIXME unbedingt auslagern!
	@Deprecated
	private static final String FILENAME_TRENNER = "_";

	/** */
	private static final String DEFAULT_OUT_DIR = System.getProperty("user.home"); // Platform.getConfigurationLocation()

	// .getDefault().getPath();

	/** */
	private static final MatheAGFileWriter FILEWRITER = new MatheAGFileWriter();

	/**
	 *
	 */
	private MatheAGGeneratorUtils() {
		super();
	}

	/**
	 * Verwandelt einen Titel in Unix- conformen Dateinamen, indem Leerzeichen zu _ gemacht werden, Gro�- in
	 * Kleinbuchstaben transformiert und Umlaute transformiert werden.
	 *
	 * @param pString
	 * @return transformierten String
	 */
	public static String transformToFilename(final String pString) {
		String name = pString.replaceAll(" ", "_").toLowerCase().replaceAll("�", "ae").replaceAll("�", "oe")
			.replaceAll("�", "ue").replaceAll("�", "ss");
		return name;
	}

	/**
	 * @param pStufe int die Stufe
	 * @return String der Stufetext
	 */
	public static String stufeText(int pStufe) {
		StringBuffer sb = new StringBuffer();
		switch (pStufe) {
		case 1:
			sb.append("Vorschule");
			break;
		case 2:
			sb.append("Klassen 1 und 2");
			break;
		case 3:
			sb.append("Klassen 3 und 4");
			break;
		case 4:
			sb.append("Klassen 5 und 6");
			break;
		case 5:
			sb.append("Klassen 7 und 8");
			break;
		default:
			sb.append("Klassen 9 bis 13");
			break;
		}
		return sb.toString();
	}

	/**
	 * Generiert das Prefix für alle Files, die mittels LaTeX- oder Html- Generatoren erzeugt werden. Aktuell ist dies
	 * mja_runde_seriennummer_
	 *
	 * @param pSerie die Serie. Darf nicht null sein!
	 * @return String
	 * @throws IllegalArgumentException falls Parameter null ist.
	 */
	public final static String filenamePrefix(final Serie pSerie) {
		if (pSerie == null) {
			String msg = MatheJungAltException.argumentNullMessage("Serie");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		StringBuffer sb = new StringBuffer("mja");
		sb.append(FILENAME_TRENNER);
		sb.append(String.valueOf(pSerie.getRunde()));
		sb.append(FILENAME_TRENNER);
		sb.append(String.valueOf(pSerie.getNummer()));
		sb.append(FILENAME_TRENNER);
		return sb.toString();
	}

	/**
	 * Ermittelt den Pfad zum Ausgabeverzeichnis. Wenn pPathOutdir null ist, wird versucht, den Pfad aus der
	 * Konfigurationsdatei zu lesen. Wird dieser nicht gefunden, wird eine {@link MatheAGAssertionFailedException}
	 * geworfen.
	 *
	 * @param pPathOutdir String der Pfad zum Ausgabeverzeichnis.
	 * @return String der Pfad zum Ausgabeverzeichnis
	 */
	private static String getOutDirPath(String pPathOutdir) {
		if (pPathOutdir != null) {
			return pPathOutdir;
		}
		return DEFAULT_OUT_DIR;
	}

	/**
	 * Erzeugt die Ausgabedatei.
	 *
	 * @param pPfad String der Pfad zum Verzeichnis, in das die Datei geschrieben werden soll.
	 * @param pGenerator {@link IFilegenerator} ein Generator f�r den Inhalt einer Textdatei
	 * @throws MatheJungAltException falls es zu Fehlern kommt
	 */
	public static void writeOutput(String pPfad, IFilegenerator pGenerator, IStammdatenservice stammdatenservice)
		throws MatheJungAltException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("DEFAULT_OUT_DIR = " + DEFAULT_OUT_DIR);
		}
		String dateiPfad = getOutDirPath(pPfad) + File.separator + pGenerator.getFilename();
		FILEWRITER.writeFile(dateiPfad, pGenerator.generate(stammdatenservice), pGenerator.getEncoding());
	}

	/**
	 *
	 * Schreibt contents in das File pFilename.
	 *
	 * @param pPfad
	 * @param pFilename
	 * @param contents
	 * @throws MatheJungAltException
	 */
	public static void writeOutput(String pPfad, String pFilename, String contents) throws MatheJungAltException {
		if (LOG.isDebugEnabled()) {
			LOG.debug("DEFAULT_OUT_DIR = " + DEFAULT_OUT_DIR);
		}
		String dateiPfad = getOutDirPath(pPfad) + File.separator + pFilename;
		FILEWRITER.writeFile(dateiPfad, contents, "utf-8");
	}

}