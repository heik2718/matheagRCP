/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.EgladilBaseException;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.persistence.api.IPersistenceservice;
import de.egladil.mathejungalt.service.stammdaten.event.IModelChangeListener;
import de.egladil.mathejungalt.service.stammdaten.event.ModelChangeEvent;
import de.egladil.mcmatheraetsel.domain.spiel.Raetsel;
import de.egladil.mcmatheraetsel.domain.spiel.Raetselbibliothek;

/**
 * @author aheike
 */
public abstract class AbstractStammdatenservice {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractStammdatenservice.class);

	public static final String AUFGABE_LATEX_GENERATOR_XSL = "/aufgabeLaTeXGenerator.xsl";

	public static final String PLAIN_AUFGABE_LATEX_GENERATOR_XSL = "/aufgabePlainLaTeXGenerator.xsl";

	public static final String RAETSEL_LATEX_GENERATOR_XSL = "/raetselLaTeXGenerator.xsl";

	public static final String LATEX_GENERATOR_TEMPLATES = "/laTeXTemplates.xsl";

	/** */
	private IPersistenceservice persistenceservice;

	protected final JAXBContext jaxbContext;

	/** */
	private Collection<IModelChangeListener> modelChangeListeners = new ArrayList<IModelChangeListener>();

	/**
	 *
	 */
	public AbstractStammdatenservice() {
		try {
			jaxbContext = JAXBContext.newInstance(MCAufgabe.class, MCArchivraetsel.class, MCArchivraetselItem.class,
				Raetsel.class, Raetselbibliothek.class);
		} catch (JAXBException e) {
			LOG.error(e.getMessage(), e);
			throw new EgladilBaseException("JAXBContext konnte nicht erstellt werden: " + e.getMessage(), e);
		}
	}

	/**
	 * @param pListener
	 */
	public final void addModelChangeListener(IModelChangeListener pListener) {
		if (!modelChangeListeners.contains(pListener)) {
			modelChangeListeners.add(pListener);
		}
	}

	/**
	 * @param pListener
	 */
	public final void removeModelChangeListener(IModelChangeListener pListener) {
		modelChangeListeners.remove(pListener);
	}

	/**
	 * @param pDomainObject
	 */
	protected void fireObjectRemovedEvent(Object pDomainObject) {
		ModelChangeEvent evt = new ModelChangeEvent(ModelChangeEvent.REMOVE_EVENT, this, pDomainObject, null, null);
		notifyListeners(evt);
	}

	/**
	 * @param pDomainObject
	 */
	protected void fireObjectAddedEvent(Object pDomainObject) {
		ModelChangeEvent evt = new ModelChangeEvent(ModelChangeEvent.ADD_EVENT, this, null, pDomainObject,
			pDomainObject.getClass().getName());
		notifyListeners(evt);
	}

	/**
	 * @param pEvt
	 */
	private void notifyListeners(ModelChangeEvent pEvt) {
		for (IModelChangeListener listener : modelChangeListeners) {
			listener.modelChanged(pEvt);
		}
	}

	/**
	 * @return the persistenceservice
	 */
	protected IPersistenceservice getPersistenceservice() {
		return persistenceservice;
	}

	/**
	 * @param pPathWorkDir
	 * @return
	 */
	protected File prepareWorkDir(String pPathWorkDir) {
		final File workDir = new File(pPathWorkDir);
		if (!workDir.isDirectory()) {
			workDir.mkdirs();
		}
		return workDir;
	}

	/**
	 * @param pPath
	 */
	protected void checkLatexTemplates(String pPath) {
		File file = new File(pPath);
		file.deleteOnExit();
		if (file.isFile()) {
			return;
		}
		InputStream latexTemplate = getClass().getResourceAsStream(LATEX_GENERATOR_TEMPLATES);
		OutputStream out = null;
		try {
			out = new FileOutputStream(file);
			IOUtils.copy(latexTemplate, out);
			out.flush();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			throw new EgladilBaseException("Marshalling oder XSL-Transformation fehlgeschlagen.", e);
		} finally {
			IOUtils.closeQuietly(out);
		}
	}

	/**
	 * @param pGenerateAufgabe
	 * @return
	 */
	protected Map<String, String> getXsltParameters(boolean pGenerateAufgabe) {
		Map<String, String> result = new HashMap<>();
		if (pGenerateAufgabe) {
			result.put("latexModus", "aufgabe");
		} else {
			result.put("latexModus", "loesung");
		}
		return result;
	}

	/**
	 * @param pPersistenceservice the persistenceservice to set
	 */
	public void setPersistenceservice(IPersistenceservice pPersistenceservice) {
		persistenceservice = pPersistenceservice;
	}

}
