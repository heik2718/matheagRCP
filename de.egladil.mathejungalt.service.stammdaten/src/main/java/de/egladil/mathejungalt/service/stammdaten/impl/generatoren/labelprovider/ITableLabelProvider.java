/**
 * 
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider;

/**
 * @author winkelv
 */
public interface ITableLabelProvider {

	/**
	 * Ein Anfangstext.
	 * 
	 * @return String
	 */
	public abstract String getStartText();

	/**
	 * Ein Endetext.
	 * 
	 * @return String
	 */
	public abstract String getEndText();

}