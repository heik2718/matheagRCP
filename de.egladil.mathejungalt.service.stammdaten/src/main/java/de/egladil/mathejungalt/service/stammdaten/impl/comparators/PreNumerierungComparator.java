/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.comparators;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;

/**
 * @author heike
 *
 */
public class PreNumerierungComparator extends AbstractMatheAGObjectComparator {

	/* (non-Javadoc)
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.comparators.AbstractMatheAGObjectComparator#specialCompare(de.egladil.mathejungalt.domain.AbstractMatheAGObject, de.egladil.mathejungalt.domain.AbstractMatheAGObject)
	 */
	@Override
	protected int specialCompare(AbstractMatheAGObject pO1, AbstractMatheAGObject pO2) {
		IAufgabensammlungItem item1 = (IAufgabensammlungItem) pO1;
		IAufgabensammlungItem item2 = (IAufgabensammlungItem) pO2;

		int stufe1 = item1.getAufgabe().getStufe();
		int stufe2 = item2.getAufgabe().getStufe();

		if (stufe1 != stufe2){
			return stufe1 - stufe2;
		}

		return item1.getId().intValue() - item2.getId().intValue();
	}

	/* (non-Javadoc)
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.comparators.AbstractMatheAGObjectComparator#specialCheckType(de.egladil.mathejungalt.domain.AbstractMatheAGObject, de.egladil.mathejungalt.domain.AbstractMatheAGObject)
	 */
	@Override
	protected void specialCheckType(AbstractMatheAGObject pO1, AbstractMatheAGObject pO2) {
		if (!(pO1 instanceof IAufgabensammlungItem) && !(pO2 instanceof IAufgabensammlungItem)) {
			throw new MatheJungAltException("Falscher Typ: kein " + IAufgabensammlungItem.class.getName());
		}
	}

}
