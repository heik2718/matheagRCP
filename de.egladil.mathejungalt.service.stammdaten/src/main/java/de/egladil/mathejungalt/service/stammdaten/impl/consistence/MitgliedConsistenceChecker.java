/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.consistence;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.exceptions.MatheJungAltInconsistentEntityException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.DomainObjectProxyClassifier;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;

/**
 * @author Heike Winkelvoss (www.egladil.de)
 */
public class MitgliedConsistenceChecker implements IConsistencecheckProvider {

	/** */
	private final ConsistenceCheckDelegate consistenceCheckDelegate;

	/**
	 *
	 */
	public MitgliedConsistenceChecker() {
		consistenceCheckDelegate = new ConsistenceCheckDelegate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.consistencecheck.IConsistencecheckProvider#canCheck(de.egladil.mathejungalt.domain
	 * .AbstractMatheAGObject)
	 */
	@Override
	public boolean canCheck(Object pObject) {
		return DomainObjectProxyClassifier.isMitglied(pObject);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.consistencecheck.IConsistencecheckProvider#checkConsistent(de.egladil.mathejungalt
	 * .domain.AbstractMatheAGObject)
	 */
	@Override
	public void checkConsistent(AbstractMatheAGObject pObject) throws MatheJungAltException {
		StringBuffer sb = new StringBuffer();
		boolean consistent = true;
		Mitglied mitglied = (Mitglied) pObject;
		try {
			consistenceCheckDelegate.checkStringKey(mitglied.getKey(), 3, "");
		} catch (MatheJungAltInconsistentEntityException e) {
			sb.append(e.getMessage());
			consistent = false;
		}
		if (mitglied.getVorname() == null || mitglied.getVorname().length() == 0) {
			sb.append("\nder Vorname darf nicht null oder leer sein");
			consistent = false;
		}
		if (mitglied.getAlter() == null || mitglied.getAlter() < 0) {
			sb.append("\ndas Alter darf nicht null oder negativ sein");
			consistent = false;
		}
		if (mitglied.getKlasse() == null || mitglied.getKlasse() < 0 || mitglied.getKlasse() > 14) {
			sb.append("\ndie Klasse darf nicht null, negativ oder groesser als 14 sein");
			consistent = false;
		}
		if (mitglied.getKontakt() == null || mitglied.getKontakt().length() == 0) {
			sb.append("\nder Kontakt darf nicht null oder leer sein");
			consistent = false;
		}
		if (!consistent) {
			throw new MatheJungAltInconsistentEntityException("Mitglied:\n" + sb.toString());
		}
	}

}
