/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.service.stammdaten.impl;

import java.util.Arrays;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.file.DirectoryCleaner;
import de.egladil.base.latex.ILaTeXService;
import de.egladil.base.latex.ILaTeXService.LaTeXInteractionModes;
import de.egladil.base.utils.OsgiServiceLocator;

/**
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 *
 */
public abstract class AbstractLaTeXConcernedService extends AbstractStammdatenservice {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractLaTeXConcernedService.class);

	/**
   *
   */
	public AbstractLaTeXConcernedService() {

	}

	/**
	 * Werfen temporäre Dateien weg.
	 *
	 * @param pPathWorkDir
	 */
	protected void clearTemporaryLaTeXFiles(String pPathWorkDir) {
		final List<String> suffixes = Arrays.asList(new String[] { "aux", "dvi", "log", "out" });
		final DirectoryCleaner cleaner = new DirectoryCleaner();
		cleaner.deleteFiles(pPathWorkDir, suffixes);
	}

	/**
	 * @param pLatexFilePath
	 * @return IStatus
	 */
	protected IStatus compile(String pLatexFilePath) {
		ILaTeXService latexService = new OsgiServiceLocator().getService(ILaTeXService.class);
		IStatus status = latexService.compile(pLatexFilePath, LaTeXInteractionModes.nonstopmode);
		return status;
	}

	/**
	 * Aufruf von pdflatex
	 * 
	 * @param pLatexFilePath
	 * @return IStatus
	 */
	protected IStatus pdflatex(String pLatexFilePath) {
		ILaTeXService latexService = new OsgiServiceLocator().getService(ILaTeXService.class);
		IStatus status = latexService.compile(pLatexFilePath, LaTeXInteractionModes.nonstopmode);
		return status;
	}

	/**
	 * Wandelt dvi in ps um.
	 *
	 * @param pWorkDir
	 * @param dviFilePath
	 * @return IStatus
	 */
	protected IStatus dviPs(String dviFilePath) {
		ILaTeXService latexService = new OsgiServiceLocator().getService(ILaTeXService.class);
		IStatus status = latexService.dviPs(dviFilePath);
		return status;
	}
}
