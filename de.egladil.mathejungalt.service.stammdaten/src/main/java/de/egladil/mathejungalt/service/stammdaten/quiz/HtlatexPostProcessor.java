/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.service.stammdaten.quiz;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.bind.Marshaller;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mathejungalt.config.IMatheJungAltConfiguration.RaetsellisteGeneratorModus;
import de.egladil.mathejungalt.domain.JaxbContextProvider;
import de.egladil.mathejungalt.domain.mcraetsel.AndroidRaetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator;

/**
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class HtlatexPostProcessor {

	private static final Logger LOG = LoggerFactory.getLogger(HtlatexPostProcessor.class);

	private final HtlatexAufgabePostProcessor aufgabePostProcessor = new HtlatexAufgabePostProcessor();

	/**
   *
   */
	public HtlatexPostProcessor() {
	}

	/**
	 * Bearbeitet die generierten html-Dateien für den gegebenen Modus auf: ersetzen von Umlauten durch HTML-Entities,
	 * Pfade zu images und resources.
	 *
	 * @param raetsel
	 * @param pathWorkDir
	 * @param modus
	 * @param progressMonitor
	 * @return IStatus
	 */
	public IStatus process(MCArchivraetsel raetsel, String pathWorkDir, RaetsellisteGeneratorModus modus,
		IProgressMonitor progressMonitor) {
		Assert.isNotNull(raetsel);
		Assert.isNotNull(pathWorkDir);

		String step = "";
		int totalWork = raetsel.getItems().size() + 1;
		if (RaetsellisteGeneratorModus.ANDROID == modus) {
			totalWork++;
		}
		SubMonitor loopProgress = SubMonitor.convert(progressMonitor, totalWork);
		try {
			loopProgress.beginTask("Nachbearbeitung für " + raetsel.getSchluessel(), totalWork);
			String pathRootDir = pathWorkDir + "\\" + QuizPathProvider.getGetRelativPathQuiz(raetsel);
			for (MCArchivraetselItem item : raetsel.getItemsSorted()) {
				step = "verarbeite Item Nummer " + item.getNummer();
				loopProgress.subTask(step);
				processItem(item, pathRootDir, modus);
				loopProgress.worked(1);
			}
			if (RaetsellisteGeneratorModus.ANDROID == modus) {
				step = "generiere quiz.xml";
				loopProgress.subTask(step);
				generateQuizXmlFile(pathWorkDir, raetsel);
			}
			step = "lösche temporäre Dateien";
			loopProgress.subTask(step);
			deleteTemporaryFiles(pathWorkDir);
			loopProgress.worked(totalWork);
			return Status.OK_STATUS;
		} catch (Exception e) {
			LOG.error(e.getMessage());
			String msg = "Fehler bei step '" + step + "': " + e.getMessage();
			IStatus status = new Status(IStatus.ERROR, IStammdatenservice.PLUGIN_ID, msg);
			return status;
		}
	}

	void generateQuizXmlFile(String pathWorkDir, MCArchivraetsel raetsel) throws Exception {
		AndroidRaetsel androidRaetsel = AndroidRaetsel.createFromMCArchivRaetsel(raetsel);
		File file = new File(QuizPathProvider.getPathQuizXml(raetsel, pathWorkDir));
		Marshaller marshaller = JaxbContextProvider.getMarshaller();
		marshaller.marshal(androidRaetsel, file);
	}

	/**
	 * @param pathWorkDir
	 */
	private void deleteTemporaryFiles(String pathWorkDir) {
		File workDir = new File(pathWorkDir);
		File[] files = workDir.listFiles();
		for (File f : files) {
			if (!f.isDirectory()) {
				FileUtils.deleteQuietly(f);
			}
		}
	}

	/**
	 * @param item
	 * @param pathRootDir
	 * @param modus
	 * @throws IOException
	 */
	private void processItem(MCArchivraetselItem item, String pathRootDir, RaetsellisteGeneratorModus modus)
		throws IOException {
		String pathAufgabe = pathRootDir + "\\" + QuizPathProvider.getRelativPathQuizitem(item) + "\\aufgabe.html";
		final File file = new File(pathAufgabe);
		InputStream in = null;
		String processedContent = "";
		try {
			in = new FileInputStream(file);
			processedContent = aufgabePostProcessor.process(in, item, modus);
		} finally {
			IOUtils.closeQuietly(in);
		}
		FileUtils.writeStringToFile(file, processedContent, IFilegenerator.ENCODING_UTF);
	}
}
