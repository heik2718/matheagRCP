/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;

/**
 * @author Winkelv
 */
public class MatheAGFileWriter {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(MatheAGFileWriter.class);

	/**
	 *
	 */
	public MatheAGFileWriter() {

	}

	/**
	 * Schreibt den gegebenen Text in die gegebene Datei.
	 *
	 * @param pPath
	 * @param pText
	 * @param pEncoding String the encoding
	 */
	public void writeFile(final String pPath, final String pText, String pEncoding) {
		if (pPath == null) {
			String msg = MatheJungAltException.argumentNullMessage("pPath");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (pText == null) {
			String msg = MatheJungAltException.argumentNullMessage("pText");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		try {
			File file = new File(pPath);
			LOG.info("Schreibe output in " + pPath + ".");
			FileUtils.writeStringToFile(file, pText, pEncoding);
		} catch (IOException e) {
			String msg = "Fehler beim Schreiben von " + pPath + ": " + e.getMessage();
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
	}
}
