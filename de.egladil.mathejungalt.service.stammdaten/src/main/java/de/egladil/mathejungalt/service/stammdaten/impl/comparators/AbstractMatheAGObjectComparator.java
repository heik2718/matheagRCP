/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.comparators;

import java.util.Comparator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;

/**
 * @author Winkelv
 */
public abstract class AbstractMatheAGObjectComparator implements Comparator<AbstractMatheAGObject> {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(AbstractMatheAGObjectComparator.class);

	/**
	 *
	 */
	public AbstractMatheAGObjectComparator() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(AbstractMatheAGObject pO1, AbstractMatheAGObject pO2) {
		if (pO1 == null || pO2 == null) {
			String str1 = pO1 == null ? "pO1=null, " : pO1.toString();
			String str2 = pO2 == null ? "pO2=null" : pO2.toString();
			String msg = "Einer der Parameter ist null: " + str1 + str2;
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (pO1 == pO2) {
			return 0;
		}
		specialCheckType(pO1, pO2);
		return specialCompare(pO1, pO2);
	}

	/**
	 * @param pO1
	 * @param pO2
	 * @return int
	 */
	protected abstract int specialCompare(AbstractMatheAGObject pO1, AbstractMatheAGObject pO2);

	/**
	 * @param pO1
	 * @param pO2
	 */
	protected abstract void specialCheckType(AbstractMatheAGObject pO1, AbstractMatheAGObject pO2);

}
