/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.contentprovider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.comparators.MitgliedNameComparator;

/**
 * @author Winkelv
 */
public class MitgliederTableContentProvider extends TableContentProvider {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(MitgliederTableContentProvider.class);

	/** */
	private int klasse;

	public MitgliederTableContentProvider() {
		super();
	}

	/** */
	private final MitgliedNameComparator comparator = new MitgliedNameComparator();

	/**
	 * @param pKlasse int die Klassenstufe (muss zwischen 0 und 14 liegen)
	 */
	public MitgliederTableContentProvider(int pKlasse) {
		super();
		if (!(pKlasse >= 0 && pKlasse <= 14)) {
			String msg = "Klasse muss zwischen 0 unnd 14 liegen!";
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		klasse = pKlasse;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathe.core.serviceimpl.generatoren.html.contentprovider.TableContentProvider#getContent()
	 */
	@Override
	public List<Object> getContent(IStammdatenservice stammdatenservice) {
		List<Mitglied> mitglieder = stammdatenservice.findMitgliederByKlasse(klasse, true);
		Collections.sort(mitglieder, comparator);
		List<Object> objekte = new ArrayList<Object>();
		for (Mitglied mitglied : mitglieder) {
			if (!mitglied.isSerienteilnahmenLoaded()) {
				stammdatenservice.serienteilnahmenZuMitgliedLaden(mitglied);
			}
			if (!mitglied.isDiplomeLoaded()) {
				stammdatenservice.diplomeZuMitgliedLaden(mitglied);
			}
			objekte.add(mitglied);
		}
		return objekte;
	}

	/**
	 * @param stammdatenservice
	 * @param pKlasse
	 * @return List
	 */
	public List<Mitglied> getAktiveMitgliederSorted(IStammdatenservice stammdatenservice, int pKlasse) {
		if (!(pKlasse >= 0 && pKlasse <= 14)) {
			String msg = "Klasse muss zwischen 0 unnd 14 liegen!";
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		List<Mitglied> mitglieder = stammdatenservice.findMitgliederByKlasse(pKlasse, true);
		Collections.sort(mitglieder, comparator);
		for (Mitglied mitglied : mitglieder) {
			if (mitglied.isAktiv()) {
				if (!mitglied.isSerienteilnahmenLoaded()) {
					stammdatenservice.serienteilnahmenZuMitgliedLaden(mitglied);
				}
				if (!mitglied.isDiplomeLoaded()) {
					stammdatenservice.diplomeZuMitgliedLaden(mitglied);
				}
			}
		}
		return mitglieder;
	}

	/**
	 * @param stammdatenservice
	 * @param pKlasse
	 * @return List
	 */
	public List<Mitglied> getAktiveErwachseneSorted(IStammdatenservice stammdatenservice) {
		List<Mitglied> mitglieder = new ArrayList<Mitglied>();
		for (int klasse = 13; klasse < 15; klasse++) {
			mitglieder.addAll(stammdatenservice.findMitgliederByKlasse(klasse, true));
		}
		Collections.sort(mitglieder, comparator);
		for (Mitglied mitglied : mitglieder) {
			if (mitglied.isAktiv()) {
				if (!mitglied.isSerienteilnahmenLoaded()) {
					stammdatenservice.serienteilnahmenZuMitgliedLaden(mitglied);
				}
				if (!mitglied.isDiplomeLoaded()) {
					stammdatenservice.diplomeZuMitgliedLaden(mitglied);
				}
			}
		}
		return mitglieder;
	}
}
