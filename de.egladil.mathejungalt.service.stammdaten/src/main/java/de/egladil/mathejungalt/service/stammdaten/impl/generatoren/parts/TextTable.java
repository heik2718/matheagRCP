/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.parts;

import java.util.List;

import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.contentprovider.TableContentProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.BlankTableLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.BlankTableRowLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.ITableLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.ITableRowLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.TableCellLabelProvider;

/**
 * Eine Tabellenstruktur zur Ausgabe von Text. Dazu werden Label- und Contentprovider benötigt. Falls keine speziellen
 * Label- oder Contentprovider gesetzt sind, werden Standardimplementierungen verwendet.
 *
 * @author Winkelv
 */
public class TextTable {

	/** */
	private int numberColumns;

	/** */
	private boolean hasHeader = true;

	/** */
	private TableCellLabelProvider cellLabelProvider;

	/** */
	private ITableLabelProvider tableLabelProvider;

	/** */
	private ITableRowLabelProvider tableRowLabelProvider;

	/** */
	private TableContentProvider tableContentProvider;

	/**
	 *
	 */
	public TextTable(int pNumberColumns) {
		numberColumns = pNumberColumns;
	}

	/**
	 * Zeichnet diese Tabelle. Falls keine speziellen Label- oder Contentprovider gesetzt sind, werden
	 * Standardimplementierungen verwendet.
	 *
	 * @return String
	 */
	public String render(IStammdatenservice stammdatenservice) {
		checkInitialized();

		StringBuffer sb = new StringBuffer();
		sb.append(tableLabelProvider.getStartText());

		List<Object> objects = tableContentProvider.getContent(stammdatenservice);

		// Tableheader
		if (hasHeader) {
			sb.append(tableRowLabelProvider.getStartText());
			for (int colIndex = 0; colIndex < numberColumns; colIndex++) {
				sb.append(cellLabelProvider.getHeaderText(colIndex));
			}
			sb.append(tableRowLabelProvider.getEndText());
		}

		if (objects.size() > 0) {
			for (int i = 0; i < objects.size(); i++) {
				sb.append(tableRowLabelProvider.getStartText());
				for (int colIndex = 0; colIndex < numberColumns; colIndex++) {
					sb.append(cellLabelProvider.getText(objects.get(i), i, colIndex));
				}
				sb.append(tableRowLabelProvider.getEndText());
			}
		}
		sb.append(tableLabelProvider.getEndText());
		return sb.toString();
	}

	/**
	 *
	 */
	private void checkInitialized() {
		if (tableLabelProvider == null) {
			tableLabelProvider = new BlankTableLabelProvider();
		}
		if (cellLabelProvider == null) {
			cellLabelProvider = new TableCellLabelProvider("\\n");
		}
		if (tableRowLabelProvider == null) {
			tableRowLabelProvider = new BlankTableRowLabelProvider();
		}
		if (tableContentProvider == null) {
			tableContentProvider = new TableContentProvider();
		}
	}

	/**
	 * @param pLabelProvider the labelProvider to set
	 */
	public void setCellLabelProvider(TableCellLabelProvider pLabelProvider) {
		cellLabelProvider = pLabelProvider;
	}

	/**
	 * @param pTableRowLabelProvider the tableRowLabelProvider to set
	 */
	public void setTableRowLabelProvider(ITableRowLabelProvider pTableRowLabelProvider) {
		tableRowLabelProvider = pTableRowLabelProvider;
	}

	/**
	 * @param pTableLabelProvider the tableLabelProvider to set
	 */
	public void setTableLabelProvider(ITableLabelProvider pTableLabelProvider) {
		tableLabelProvider = pTableLabelProvider;
	}

	/**
	 * @return the tableContentProvider
	 */
	public TableContentProvider getTableContentProvider() {
		return tableContentProvider;
	}

	/**
	 * @param pTableContentProvider the tableContentProvider to set
	 */
	public void setTableContentProvider(TableContentProvider pTableContentProvider) {
		tableContentProvider = pTableContentProvider;
	}

	/**
	 * @param pHasHeader the hasHeader to set
	 */
	public void setHasHeader(boolean pHasHeader) {
		hasHeader = pHasHeader;
	}

	/**
	 * @return the cellLabelProvider
	 */
	public TableCellLabelProvider getCellLabelProvider() {
		return cellLabelProvider;
	}
}
