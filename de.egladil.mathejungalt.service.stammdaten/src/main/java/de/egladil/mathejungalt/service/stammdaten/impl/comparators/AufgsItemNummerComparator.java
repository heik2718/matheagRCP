/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.comparators;

import java.util.Comparator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;

/**
 * @author Winkelv
 */
public class AufgsItemNummerComparator implements Comparator<IAufgabensammlungItem> {

	private static final Logger LOG = LoggerFactory.getLogger(AufgsItemNummerComparator.class);

	/**
	 *
	 */
	public AufgsItemNummerComparator() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(IAufgabensammlungItem pO1, IAufgabensammlungItem pO2) {
		if (pO1 == null || pO2 == null) {
			String str1 = pO1 == null ? "pO1=null, " : pO1.toString();
			String str2 = pO2 == null ? "pO2=null" : pO2.toString();
			String msg = "Einer der Parameter ist null: " + str1 + str2;
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (pO1 == pO2) {
			return 0;
		}
		IAufgabensammlungItem item1 = (IAufgabensammlungItem) pO1;
		IAufgabensammlungItem item2 = (IAufgabensammlungItem) pO2;
		return item1.getNummer().compareToIgnoreCase(item2.getNummer());
	}
}
