//=====================================================
// Projekt: de.egladil.mathejungalt.service.stammdaten
// (c) Heike Winkelvoß
//=====================================================
// Dateiname: SerieIndexGenerator.java                            $
// $Revision:: 1                                     $
// $Modtime:: 29.03.2015                                $
//=====================================================

package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.service.stammdaten.impl.Messages;

/**
 * SerieIndexGenerator generiert eine Seite mit Links zu den Archiv-Seiten der einzelnen Stufen. Diese ist nur bei
 * schmalen Displays zu erreichen, indem man auf der Archivseite auf den Link namens Serie_XX clickt. Die einzelnen
 * Links (Vorschule, Klassen 1 nund 2 usw.) verweisen auf die Archiv-Detailseiten vorschau/mja_serie_xx-1.html,
 * vorschau/mja_serie_xx-2.html usw, die von der Archivseite bei breiten Displays über die Links in der tabellarischen
 * Übersicht erreichbar sind. Diese werden generiert mittels ArchivSerieStufeGenerator.
 */
public class SerieArchivIndexGenerator {

	private static final String HTML_TEMPLATE_PATH = "/html/serie_archiv_index_template.txt";

	private static final String UNTERTITEL_PLACEHOLDER = "PLATZHALTER_UNTERTITEL";

	private static final String UL_PLACEHOLDER = "SERIE_ARCHIV_UL";

	private static final String DOWNLOAD_PLACEHOLDER = "SERIE_ARCHIV_DOWNLOAD";

	private static final Logger LOG = LoggerFactory.getLogger(SerieArchivIndexGenerator.class);

	/**
	 * Erzeugt eine Instanz von SerieIndexGenerator
	 */
	public SerieArchivIndexGenerator() {
	}

	/**
	 * Generiert eine Linkliste zu den einzelnen Stufenvorschauen. TODO
	 *
	 * @param stammdatenservice
	 * @return
	 */
	public String generate(Serie serie) {
		InputStream in = null;
		try {
			in = this.getClass().getResourceAsStream(HTML_TEMPLATE_PATH);
			StringWriter sw = new StringWriter();
			IOUtils.copy(in, sw);
			String content = sw.toString();
			content = content.replaceAll(UNTERTITEL_PLACEHOLDER, getReplacementForUntertitel(serie));
			content = content.replaceAll(UL_PLACEHOLDER, getReplacementForVorschauPart(serie));
			content = content.replaceAll(DOWNLOAD_PLACEHOLDER, getReplacementForDownloadPart(serie));
			content = HtmlFactory.setAktuellesDatum(content);
			return content;
		} catch (IOException e) {
			LOG.error(e.getMessage());
			throw new MatheJungAltException("Fehler beim Laden des Templates '" + HTML_TEMPLATE_PATH
				+ "': mal classpath prüfen!");

		} finally {
			IOUtils.closeQuietly(in);
		}
	}

	private String getReplacementForUntertitel(Serie serie) {
		return "Serie " + serie.getNummer() + " - Archiv";
	}

	private String getReplacementForVorschauPart(Serie serie) {
		String result = Messages.getString("SerieArchivIndexGenerator_1");
		result = result.replaceAll("mja_98", "mja_" + serie.getNummer());
		return result;
	}

	private String getReplacementForDownloadPart(Serie serie) {
		String result = Messages.getString("SerieArchivIndexGenerator_2");
		result = result.replaceAll("mja_13_98", "mja_" + serie.getRunde() + "_" + serie.getNummer());
		return result;
	}

	/**
	 *
	 * @return
	 */
	public String getFilename(Serie serie) {
		return "serie_" + serie.getNummer() + ".html";
	}
}
