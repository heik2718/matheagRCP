/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguruitem;
import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenzweck;
import de.egladil.mathejungalt.persistence.api.IPersistenceservice;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.comparators.AufgabensItemStufeSchluesselComparator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.MatheAGGeneratorUtils;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.MinikaenguruGenerator;

/**
 * @author aheike
 */
public class MinikaenguruserviceDelegate {

	private static final Logger LOG = LoggerFactory.getLogger(MinikaenguruserviceDelegate.class);

	/** */
	private static final String TRENNZEICHEN_AUFGABENNUMMER = "-";

	/** */
	private final AufgabensItemStufeSchluesselComparator comparator = new AufgabensItemStufeSchluesselComparator();

	/** */
	private Map<Integer, Minikaenguru> minikaenguruMap;

	/** */
	private MinikaenguruGenerator minikaenguruGenerator = new MinikaenguruGenerator();

	/**
	 *
	 */
	public MinikaenguruserviceDelegate() {
	}

	/**
	 * Alle Minikaengurus aus der Persistenzschicht laden.
	 *
	 * @return {@link Collection} von {@link Minikaenguru}
	 * @throws MatheJungAltException
	 */
	protected Collection<Minikaenguru> getMinikaengurus(IPersistenceservice pPersistenceService)
		throws MatheJungAltException {
		checkMinikaengurumap(pPersistenceService);
		return minikaenguruMap.values();
	}

	/**
	 * Falls die MinikaenguruMap nicht initialisiert ist, wird sie hier initialisiert.
	 */
	private void checkMinikaengurumap(IPersistenceservice pPersistenceService) {
		if (minikaenguruMap == null) {
			minikaenguruMap = new HashMap<Integer, Minikaenguru>();
			List<Minikaenguru> minikaengurus = pPersistenceService.getMinikaenguruRepository().findAllWettbewerbe();
			for (Minikaenguru mini : minikaengurus) {
				// minikaenguruMap.put(mini.getUuid(), mini);
				minikaenguruMap.put(mini.getJahr(), mini);
			}
		}
	}

	/**
	 * Die Items der Serie werden numeriert.
	 *
	 * @param pMinikaenguru Serie die Serie.
	 */
	protected void itemsNumerieren(Minikaenguru pMinikaenguru) {
		List<Minikaenguruitem> miniItems = new ArrayList<Minikaenguruitem>();
		List<IAufgabensammlungItem> items = pMinikaenguru.getItems();
		for (IAufgabensammlungItem item : items) {
			miniItems.add((Minikaenguruitem) item);
		}
		Collections.sort(miniItems, comparator);
		int aktuelleStufe = 3;
		int laufendeNummer = 1;
		for (Minikaenguruitem item : miniItems) {
			if (item.getAufgabe().getStufe().intValue() > aktuelleStufe) {
				laufendeNummer = 1;
				aktuelleStufe = item.getAufgabe().getStufe().intValue();
			}
			item.setNummer(aktuelleStufe + TRENNZEICHEN_AUFGABENNUMMER + laufendeNummer);
			laufendeNummer++;
		}
	}

	/**
	 * Generiert Aufgaben und Lösungen.
	 *
	 * @param pPath
	 * @param pMinikaenguru
	 */
	protected void generate(String pPath, Minikaenguru pMinikaenguru, IStammdatenservice stammdatenservice) {
		minikaenguruGenerator.setAusgabeLoesungen(false);
		minikaenguruGenerator.setMinikaenguru(pMinikaenguru);
		MatheAGGeneratorUtils.writeOutput(pPath, minikaenguruGenerator, stammdatenservice);

		minikaenguruGenerator.setAusgabeLoesungen(true);
		MatheAGGeneratorUtils.writeOutput(pPath, minikaenguruGenerator, stammdatenservice);
	}

	/**
	 * Eine neuen Minikaenguruwettwerb anlegen.
	 *
	 * @return
	 * @throws MatheJungAltException
	 */
	protected Minikaenguru minikaenguruAnlegen() throws MatheJungAltException {
		Minikaenguru minikaenguru = new Minikaenguru();
		minikaenguru.setBeendet(IAufgabensammlung.NICHT_BEENDET);
		minikaenguru.setItemsLoaded(true);
		minikaenguru.setJahr(Calendar.getInstance().get(Calendar.YEAR));
		return minikaenguru;
	}

	/**
	 * @param pWettbewerb
	 * @throws MatheJungAltException
	 */
	protected void minikaenguruSpeichern(Minikaenguru pWettbewerb, IPersistenceservice pPersistenceService)
		throws MatheJungAltException {
		boolean neu = pWettbewerb.isNew();
		pPersistenceService.getMinikaenguruRepository().save(pWettbewerb);
		if (neu) {
			// minikaenguruMap.put(pWettbewerb.getUuid(), pWettbewerb);
			minikaenguruMap.put(pWettbewerb.getJahr(), pWettbewerb);
		}
	}

	/**
	 * @param pMinikaenguru
	 * @param pAufgabe
	 * @return
	 */
	public IAufgabensammlungItem aufgabeHinzufuegen(Minikaenguru pMinikaenguru, Aufgabe pAufgabe) {
		if (pMinikaenguru == null || pAufgabe == null) {
			String msg = MatheJungAltException
				.argumentNullMessage(pMinikaenguru == null ? "pMinikaenguru" : "pAufgabe");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (pMinikaenguru.getBeendet() != 0) {
			throw new MatheJungAltException(
				"Der Wettbewerb ist bereits beendet. Es kann keine Aufgabe mehr hinzugefuegt werden.");
		}
		if (Aufgabe.LOCKED.equals(pAufgabe.getGesperrtFuerWettbewerb())) {
			throw new MatheJungAltException("Die Aufgabe kann nicht zur Serie zugeordnet werden. Sie ist gesperrt.");
		}
		if (!Aufgabenzweck.K.equals(pAufgabe.getZweck())) {
			throw new MatheJungAltException(
				"Die Aufgabe kann nicht zum Wettbewerb zugeodnet werden. Es ist keine Känguruaufgabe.");
		}
		Minikaenguruitem item = new Minikaenguruitem();
		item.setAufgabe(pAufgabe);
		item.setMinikaenguru(pMinikaenguru);
		pAufgabe.setGesperrtFuerWettbewerb(Aufgabe.LOCKED);
		pMinikaenguru.addItem(item);
		return item;
	}

	/**
	 * @param minikaenguru
	 * @return
	 */
	protected List<Kontakt> mailinglisteLaden(Minikaenguru minikaenguru, IPersistenceservice persistenceService) {
		if (minikaenguru.isMailinglisteLoaded()) {
			return minikaenguru.getMailingliste();
		}
		List<Kontakt> kontakte = persistenceService.getMinikaenguruRepository().loadMailingliste(minikaenguru);
		minikaenguru.setMailinglisteLoaded(true);
		for (Kontakt k : kontakte) {
			minikaenguru.addKontakt(k);
		}
		return kontakte;
	}
}
