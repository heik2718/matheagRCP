/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.service.stammdaten.quiz;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.Assert;

import de.egladil.base.exceptions.EgladilBusinessException;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.Messages;

/**
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class HtLatexProcessorGenerator implements IPrepareHtlatexStep<MCArchivraetselItem> {

	private static final String HTLATEX_COMMAND_KEY = "htlatex_command";

	private static final String[] SUFFIXES_OF_FILES_TO_MOVE = new String[] { "aufgabe.html", "*.png" };

	/**
   *
   */
	public HtLatexProcessorGenerator() {
	}

	/**
	 * Generiert im gegebenen Verzeichnis eine bat-Datei, die das htlatex-Zeug auführen wird.
	 *
	 * @param item {@link MCArchivraetselItem} darf nicht null sein.
	 * @param pathRootDir String absoluter Pfad zum Arbeitsverzeichnis.
	 * @return String absoluter Pfad der Datei process_xx.bat.
	 */
	public String generate(MCArchivraetselItem item, String pathRootDir) {
		Assert.isNotNull(item, "item darf nicht null sein");
		Assert.isNotNull(pathRootDir, "pathRootDir darf nicht null sein");
		String nummer = QuizPathProvider.getRelativPathQuizitem(item);
		String result = pathRootDir + "\\" + IStammdatenservice.PROCESS_HTLATEX_FILENAME_PREFIX + nummer
			+ IStammdatenservice.PROCESS_HTLATEX_FILENAME_SUFFIX;
		List<String> lines = new ArrayList<String>();
		final String relativePathItem = ".\\" + item.getRaetsel().getSchluessel() + "\\" + nummer;
		lines.add(0, "copy " + relativePathItem + ".tex .\\aufgabe.tex");
		final String string = Messages.getString(HTLATEX_COMMAND_KEY);
		lines.add(1, string);
		for (int i = 0; i < SUFFIXES_OF_FILES_TO_MOVE.length; i++) {
			int index = i + 2;
			lines.add(index, "move " + SUFFIXES_OF_FILES_TO_MOVE[i] + " " + relativePathItem);

		}
		int i = 2 + SUFFIXES_OF_FILES_TO_MOVE.length;
		lines.add(i, "del aufgabe*");

		File file = new File(result);
		try {
			FileUtils.writeLines(file, lines);
			return result;
		} catch (IOException e) {
			throw new EgladilBusinessException("Fehler beim Schreiben der Datei [" + result + "]: " + e.getMessage(), e);
		}
	}
}
