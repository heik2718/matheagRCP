/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.latex;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.TableCellLabelProvider;

/**
 * Zeichnet eine Aufgaben- oder Lösungen- Zelle in einer "LaTeX"- Tabelle. Das Zeilenende kann dabei variiert werden, um
 * z.B. einen Zeilenumbruch, einen Seitenumbruch oder einen vertikalen Abstand zu erzeugen. Default ist ein vertikaler
 * Abstand einer konfigurierten Höhe oder 2cm, falls keine Höhe konfiguriert wurde.
 *
 * @author winkelv
 */
public class ArbeitsblattItemTableCellLabelProvider extends TableCellLabelProvider {

	/** */
	private static final Logger log = LoggerFactory.getLogger(ArbeitsblattItemTableCellLabelProvider.class);

	private static final String BLACK_SQUARE = " \\rule[0.4ex]{1ex}{1ex} ";

	/** */
	private boolean anzeigeLoesungen = false;


	/**
	 * default: Ausgabe der Aufgaben und Standardzeilenende
	 */
	public ArbeitsblattItemTableCellLabelProvider(String aufgabenabstand) {
		super(aufgabenabstand);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathe.core.serviceimpl.generatoren.labelprovider.TableCellLabelProvider#getText(java.lang.Object,
	 * int, int)
	 */
	@Override
	public String getText(Object pObj, int pRow, int pColumn) {
		if (!(pObj instanceof IAufgabensammlungItem)) {
			String msg = "falscher Typ: kein " + IAufgabensammlungItem.class.getName() + ", sondern " + pObj == null ? "null"
				: pObj.getClass().getName();
			log.error(msg);
			throw new MatheJungAltException(msg);
		}
		IAufgabensammlungItem item = (IAufgabensammlungItem) pObj;
		if (anzeigeLoesungen) {
			return renderLoesungen(item);
		} else {
			return renderAufgaben(item);
		}
	}

	/**
	 * Zeichnet die Lösungenzelle
	 *
	 * @param pItem {@link IAufgabensammlungItem} das Serienitem
	 * @return String
	 */
	private String renderLoesungen(IAufgabensammlungItem pItem) {
		StringBuffer sb = new StringBuffer();
		sb.append("\\par {\\bf Lösung ");
		sb.append(BLACK_SQUARE);
		sb.append(pItem.getNummer());
		sb.append(BLACK_SQUARE);
		sb.append("} \\input{../loesungsarchiv/");
		sb.append(pItem.getAufgabe().getVerzeichnis());
		sb.append("/");
		sb.append(pItem.getAufgabe().getSchluessel());
		sb.append("_l} ");
		sb.append(getZeilenende());
		return sb.toString();
	}

	/**
	 * Zeichnet die Aufgabenzelle
	 *
	 * @param pItem {@link IAufgabensammlungItem} das Serienitem
	 * @return String
	 */
	private String renderAufgaben(IAufgabensammlungItem pItem) {
		StringBuffer sb = new StringBuffer();
		sb.append("\\par {\\bf Aufgabe ");
		sb.append(BLACK_SQUARE);
		sb.append(pItem.getNummer());
		sb.append(BLACK_SQUARE);
		sb.append("} \\input{../aufgabenarchiv/");
		sb.append(pItem.getAufgabe().getVerzeichnis());
		sb.append("/");
		sb.append(pItem.getAufgabe().getSchluessel());
		sb.append("}");
		sb.append(getZeilenende());
		return sb.toString();
	}

	/**
	 * @param pAnzeigeLoesungen the anzeigeLoesungen to set
	 */
	public void setAnzeigeLoesungen(boolean pAnzeigeLoesungen) {
		anzeigeLoesungen = pAnzeigeLoesungen;
	}
}
