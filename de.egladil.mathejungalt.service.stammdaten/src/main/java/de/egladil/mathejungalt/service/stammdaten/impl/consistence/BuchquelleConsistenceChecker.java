/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.consistence;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.exceptions.MatheJungAltInconsistentEntityException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.DomainObjectProxyClassifier;
import de.egladil.mathejungalt.domain.quellen.Buchquelle;

/**
 * Kosistenzprüfer für Buchquellen.
 *
 * @author Heike Winkelvoss (www.egladil.de)
 */
public class BuchquelleConsistenceChecker implements IConsistencecheckProvider {

	/** */
	private final ConsistenceCheckDelegate consistenceCheckDelegate;

	/**
	 *
	 */
	public BuchquelleConsistenceChecker() {
		consistenceCheckDelegate = new ConsistenceCheckDelegate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.consistencecheck.IConsistencecheckProvider#canCheck(de.egladil.mathejungalt.domain
	 * .AbstractMatheAGObject)
	 */
	@Override
	public boolean canCheck(Object pObject) {
		return DomainObjectProxyClassifier.isBuchquelle(pObject);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.consistencecheck.IConsistencecheckProvider#checkConsistent(de.egladil.mathejungalt
	 * .domain.AbstractMatheAGObject)
	 */
	@Override
	public void checkConsistent(AbstractMatheAGObject pObject) throws MatheJungAltException {
		Buchquelle pQuelle = (Buchquelle) pObject;
		consistenceCheckDelegate.checkStringKey(pQuelle.getKey(), 6, "Buchquelle:");
		if (pQuelle.getBuch() == null)
			throw new MatheJungAltInconsistentEntityException("Buchquelle: das Buch fehlt.");
		if (pQuelle.getSeite() == null)
			throw new MatheJungAltInconsistentEntityException("Buchquelle: die Seite fehlt.");
	}
}
