package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html;

import java.text.SimpleDateFormat;
import java.util.Date;

import de.egladil.mathejungalt.domain.mitglieder.Mitglied;

/**
 * Utilities zur Generierung von Textbausteinen für Files.
 *
 * @author Heike Winkelvoß (www.egladil.de)
 */
public final class HtmlFactory {

	private static final String DATUM_PLACEHOLDER = "DD.MM.YYYY";

	/**
	 *
	 */
	private HtmlFactory() {
		super();
	}

	/**
	 * Vorname, die konfigurierte Anzahl von Buchstaben des Nachnamen, falls vorhanden und Alter in runden Klammern
	 *
	 * @param pMitglied {@link Mitglied}
	 * @param pMitKlasse boolean falls auch die Klasse mit ausgegeben werden muss
	 * @return
	 */
	public static String nameAlterKlasseText(Mitglied pMitglied, final boolean pMitKlasse) {
		StringBuffer sb = new StringBuffer();
		int anzahlStellen = 2;
		int endIndex = 1;
		if (anzahlStellen > 1) {
			endIndex = anzahlStellen;
		}
		sb.append(pMitglied.getVorname());
		if (pMitglied.getNachname() != null && pMitglied.getNachname().length() > 1) {
			sb.append(" "); //$NON-NLS-1$
			sb.append(pMitglied.getNachname().substring(0, endIndex));
			sb.append("."); //$NON-NLS-1$
		}
		if (pMitglied.getNachname() != null && pMitglied.getNachname().length() == 1) {
			sb.append(" "); //$NON-NLS-1$
			sb.append(pMitglied.getNachname().substring(0, 1));
			sb.append("."); //$NON-NLS-1$
		}
		sb.append(" ("); //$NON-NLS-1$
		sb.append(pMitglied.getAlter());
		if (pMitKlasse) {
			sb.append(","); //$NON-NLS-1$
			sb.append(pMitglied.getKlasse());
		}
		sb.append(")"); //$NON-NLS-1$
		return sb.toString();
	}

	/**
	 * Wenn content das Pattern DD.MM.YYYY enthält, wird dieses durch das aktuelle Datum ersetzt. TODO
	 *
	 * @param content
	 * @return
	 */
	public static String setAktuellesDatum(String content) {
		String heute = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
		String result = content.replaceAll(DATUM_PLACEHOLDER, heute);
		return result;
	}


}