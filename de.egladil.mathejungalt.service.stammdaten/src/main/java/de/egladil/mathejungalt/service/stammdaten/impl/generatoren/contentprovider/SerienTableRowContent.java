/**
 * 
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.contentprovider;

import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;

/**
 * Ein Objekt, das weiß, ob es einen Latex-Link, einen Pdf-Link und eine Beschreibung geben muss.
 * 
 * @author Winkelv
 */
public class SerienTableRowContent {

	/** */
	private boolean latexLink;

	/** */
	private boolean pdfLink;

	/** */
	private boolean beschreibung;

	/** */
	private Serie serie;

	/**
	 * Konstruktor nimmt eine {@link Serie} entgegen und 3 Flags, die die Ausgabe der Zelleninhalte triggern.
	 * 
	 * @param pSerie {@link Serie} die Serie
	 * @param pLatexLink boolean true, falls es einen LaTeX-Link geben soll.
	 * @param pPdfLink boolean true, falls es einen PDF-Link geben soll.
	 * @param pBeschreibung boolean true, falls es eine Beschreibung geben soll.
	 */
	public SerienTableRowContent(Serie pSerie, boolean pLatexLink, boolean pPdfLink, boolean pBeschreibung) {
		super();
		serie = pSerie;
		beschreibung = pBeschreibung;
		latexLink = pLatexLink;
		pdfLink = pPdfLink;
	}

	/**
	 * @return the latexLink
	 */
	public boolean isLatexLink() {
		return latexLink;
	}

	/**
	 * @return the pdfLink
	 */
	public boolean isPdfLink() {
		return pdfLink;
	}

	/**
	 * @return the beschreibung
	 */
	public boolean isBeschreibung() {
		return beschreibung;
	}

	/**
	 * @return the serie
	 */
	public Serie getSerie() {
		return serie;
	}
}
