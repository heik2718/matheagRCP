/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.consistence;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.exceptions.MatheJungAltInconsistentEntityException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.DomainObjectProxyClassifier;
import de.egladil.mathejungalt.domain.schulen.Schule;

/**
 * @author Heike Winkelvoss (www.egladil.de)
 */
public class SchuleConsistenceChecker implements IConsistencecheckProvider {

	/** */
	private final ConsistenceCheckDelegate consistenceCheckDelegate;

	/**
	 *
	 */
	public SchuleConsistenceChecker() {
		consistenceCheckDelegate = new ConsistenceCheckDelegate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.consistencecheck.IConsistencecheckProvider#canCheck(de.egladil.mathejungalt.domain
	 * .AbstractMatheAGObject)
	 */
	@Override
	public boolean canCheck(Object pObject) {
		return DomainObjectProxyClassifier.isSchule(pObject);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.consistencecheck.IConsistencecheckProvider#checkConsistent(de.egladil.mathejungalt
	 * .domain.AbstractMatheAGObject)
	 */
	@Override
	public void checkConsistent(AbstractMatheAGObject pObject) throws MatheJungAltException {
		StringBuffer sb = new StringBuffer();
		boolean consistent = true;
		Schule schule = (Schule) pObject;
		try {
			consistenceCheckDelegate.checkStringKey(schule.getKey(), 3, "");
		} catch (MatheJungAltInconsistentEntityException e) {
			sb.append(e.getMessage());
			consistent = false;
		}
		if (schule.getName() == null || schule.getName().trim().length() == 0) {
			sb.append("\nder Name darf nicht null oder leer sein");
			consistent = false;
		}
		if (schule.getAnschrift() == null || schule.getAnschrift().trim().isEmpty()) {
			sb.append("\ndie Anschrift darf nicht null oder negativ sein");
			consistent = false;
		}
		if (schule.getLand() == null) {
			sb.append("\ndas Land darf nicht null sein");
			consistent = false;
		}
		if (!consistent) {
			throw new MatheJungAltInconsistentEntityException("Schule:\n" + sb.toString());
		}
	}

}
