/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.service.stammdaten.quiz;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.eclipse.core.runtime.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.EgladilBaseException;
import de.egladil.base.exceptions.EgladilBusinessException;
import de.egladil.base.xml.XSLProcessor;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;

/**
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class QuizaufgabeLatexGenerator implements IPrepareHtlatexStep<MCArchivraetselItem> {

	private static final Logger LOG = LoggerFactory.getLogger(QuizaufgabeLatexGenerator.class);

	private static final String QUIZ_AUFGABE_LATEX_GENERATOR_XSL = "/quizaufgabePlainLaTeXGenerator.xsl";

	private static final String LATEX_GENERATOR_TEMPLATES = "/laTeXTemplates.xsl";

	private final JAXBContext jaxbContext;

	/**
   *
   */
	public QuizaufgabeLatexGenerator() {
		try {
			jaxbContext = JAXBContext.newInstance(MCArchivraetselItem.class);
		} catch (JAXBException e) {
			LOG.error(e.getMessage(), e);
			throw new EgladilBaseException("JAXBContext konnte nicht erstellt werden: " + e.getMessage(), e);
		}
	}

	/**
	 * Generiert ein latex-File, das dann weiter verarbeitet werden kann.
	 *
	 * @param item
	 * @param pathWorkdir
	 * @return String absoluter Pfad zum Latex-File
	 */
	public String generate(MCArchivraetselItem item, String pathWorkdir) {
		Assert.isNotNull(item, "item darf nicht null sein.");
		try {
			File xmlTempFile = createXmlTempFile();
			marshallItem(item, xmlTempFile);

			final String pathRootDir = xmlTempFile.getParent();
			String generatorXslPath = this.createXsltTempFile(pathRootDir, QUIZ_AUFGABE_LATEX_GENERATOR_XSL,
				"generator.xsl");
			this.createXsltTempFile(pathRootDir, LATEX_GENERATOR_TEMPLATES, "laTeXTemplates.xsl");

			final XSLProcessor processor = new XSLProcessor();
			final String nameLatexFile = QuizPathProvider.getRelativPathQuizitem(item);
			final String pathLatexFile = pathWorkdir + File.separator + nameLatexFile + ".tex";
			deleteGeneratedFileIfExists(pathLatexFile);

			processor.transform(xmlTempFile.getAbsolutePath(), generatorXslPath, pathLatexFile, "UTF-8", null);
			if (LOG.isDebugEnabled()) {
				LOG.debug("mit xslt: Ausgabe nach [" + pathWorkdir + "]");
			}
			return pathLatexFile;
		} catch (Exception e) {
			throw new EgladilBusinessException("Latex zu " + item.getAufgabe().getSchluessel()
				+ " konnte nicht erzeugt werden.", e);
		}
	}

	/**
	 * @param pathLatexFile
	 */
	private void deleteGeneratedFileIfExists(final String pathLatexFile) {
		final File file = new File(pathLatexFile);
		if (file.isFile()) {
			FileUtils.deleteQuietly(file);
		}
	}

	/**
	 * @param pathDir
	 * @param resourceName
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	String createXsltTempFile(String pathDir, String resourceName, String fileName) throws IOException {
		final String pathname = pathDir + File.separator + fileName;
		File file = new File(pathname);
		if (file.canRead()) {
			return pathname;
		}
		OutputStream out = null;
		InputStream in = null;
		try {
			in = getClass().getResourceAsStream(resourceName);

			out = new FileOutputStream(file);
			IOUtils.copy(in, out);
			out.flush();
			return pathname;
		} finally {
			IOUtils.closeQuietly(in);
			IOUtils.closeQuietly(out);
		}
	}

	/**
	 * @param item
	 * @param xmlTempFile
	 * @throws JAXBException
	 */
	private void marshallItem(MCArchivraetselItem item, File xmlTempFile) throws JAXBException {
		final Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.setProperty("jaxb.encoding", "UTF-8");
		marshaller.marshal(item, xmlTempFile);
	}

	/**
	 * @return
	 * @throws IOException
	 */
	File createXmlTempFile() throws IOException {
		File xmlTempFile = File.createTempFile("quizaufgabe", ".xml");
		LOG.debug(xmlTempFile.getAbsolutePath());
		xmlTempFile.deleteOnExit();
		return xmlTempFile;
	}
}
