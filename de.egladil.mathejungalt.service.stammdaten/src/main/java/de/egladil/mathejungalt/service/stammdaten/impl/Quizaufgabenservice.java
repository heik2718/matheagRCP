/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.Marshaller;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.xml.XSLProcessor;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.mcraetsel.GeneratorUtils;
import de.egladil.mathejungalt.domain.mcraetsel.IMCAufgabeNames;
import de.egladil.mathejungalt.domain.mcraetsel.MCAntwortbuchstaben;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabenarten;
import de.egladil.mathejungalt.domain.mcraetsel.MCThemen;
import de.egladil.mathejungalt.domain.types.MCAufgabenstufen;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * @author heike
 */
public class Quizaufgabenservice extends AbstractLaTeXConcernedService {

	private static final Logger LOG = LoggerFactory.getLogger(Quizaufgabenservice.class);

	/** */
	private int nextAufgabeSchluesselInt = -1;

	private Map<String, MCAufgabe> aufgabenMap = new HashMap<String, MCAufgabe>();

	private final Verzeichnisrechner verzeichnisrechner = new Verzeichnisrechner();

	/**
	 * @param pPersistenceservice
	 */
	public Quizaufgabenservice() {
		super();
	}

	protected MCAufgabe aufgabeAnlegen() throws MatheJungAltException {
		MCAufgabe aufgabe = new MCAufgabe();
		String nextAufgabenSchluessel = nextAufgabenSchluessel();
		aufgabe.setSchluessel(nextAufgabenSchluessel);
		aufgabe.setVerzeichnis(verzeichnisrechner.determineVerzeichnisZuSchluessel(nextAufgabenSchluessel));
		aufgabe.setGesperrtFuerRaetsel(AbstractMatheAGObject.NOT_LOCKED);
		return aufgabe;

	}

	/**
	 * @param schluesselPrefix irgendwas mit 2007-78 oder so.
	 * @param anzahl anzahl der Aufgaben
	 * @param stufe {@link MCAufgabenstufen}
	 * @return {@link List} die neuen Aufgaben. sind bereits persistiert.
	 */
	protected List<MCAufgabe> kaenguruAufgabentemplatesAnlegen(String schluesselPrefix, int anzahl,
		MCAufgabenstufen stufe) {
		Assert.isNotNull(schluesselPrefix, "schluesselPrefix");
		Assert.isNotNull(stufe, "stufe");
		List<MCAufgabe> result = new ArrayList<MCAufgabe>();
		int punktknick1 = anzahl / 3;
		int punktknick2 = 2 * punktknick1;
		for (int i = 1; i <= anzahl; i++) {
			MCAufgabe aufgabe = new MCAufgabe();
			String nextAufgabenSchluessel = i < 10 ? schluesselPrefix + "-0" + i : schluesselPrefix + "-" + i;
			aufgabe.setSchluessel(nextAufgabenSchluessel);
			aufgabe.setVerzeichnis("000");
			aufgabe.setStufe(stufe);
			aufgabe.setArt(MCAufgabenarten.Z);
			aufgabe.setQuelle("Känguru der Mathematik e.V.");
			if (i <= punktknick1) {
				aufgabe.setPunkte(3);
			} else if (i <= punktknick2) {
				aufgabe.setPunkte(4);
			} else {
				aufgabe.setPunkte(5);
			}
			aufgabe.setThema(MCThemen.ARITHMETIK);
			aufgabe.setTitel("Käng-" + nextAufgabenSchluessel);
			aufgabe.setTabelleGenerieren(false);
			aufgabe.setLoesungsbuchstabe(MCAntwortbuchstaben.A);
			aufgabe.setGesperrtFuerRaetsel(AbstractMatheAGObject.NOT_LOCKED);
			aufgabeSpeichern(aufgabe, true);
			result.add(aufgabe);
		}

		return result;

	}

	/**
	 * Ermittelt den naechsten freien Schluesselwert fuer eine Aufgabe.
	 *
	 * @return String
	 */
	private String nextAufgabenSchluessel() {
		if (nextAufgabeSchluesselInt < 0) {
			final String maxKey = getPersistenceservice().getMcAufgabenRepository().getMaxKey();
			if (maxKey != null) {
				nextAufgabeSchluesselInt = Integer.valueOf(maxKey).intValue() + 1;
			} else {
				nextAufgabeSchluesselInt = 1;
			}
		}
		String schl = "" + (nextAufgabeSchluesselInt);
		if (schl.length() < IMCAufgabeNames.LENGTH_SCHLUESSEL_DEFAULT) {
			schl = StringUtils.leftPad(schl, IMCAufgabeNames.LENGTH_SCHLUESSEL_DEFAULT, '0');
		}
		nextAufgabeSchluesselInt++;
		return schl;
	}

	/**
	 * @return
	 */
	protected Collection<MCAufgabe> getAufgaben() {
		checkAufgabenmap();
		return aufgabenMap.values();
	}

	/**
	 * Falls die Aufgabenmap nicht initialisiert ist, wird sie hier initialisiert.
	 */
	private void checkAufgabenmap() {
		if (aufgabenMap == null || aufgabenMap.isEmpty()) {
			aufgabenMap = new HashMap<String, MCAufgabe>();
			List<MCAufgabe> aufgaben = getPersistenceservice().getMcAufgabenRepository().findAll();
			for (MCAufgabe aufgabe : aufgaben) {
				// aufgabenMap.put(aufgabe.getUuid(), aufgabe);
				aufgabenMap.put(aufgabe.getSchluessel(), aufgabe);
			}
		}
	}

	/**
	 * @param pAufgabe
	 * @throws MatheJungAltException
	 */
	protected void aufgabeSpeichern(MCAufgabe pAufgabe, boolean quietly) throws MatheJungAltException {
		boolean neu = pAufgabe.isNew();
		getPersistenceservice().getMcAufgabenRepository().save(pAufgabe);
		if (neu) {
			// aufgabenMap.put(pAufgabe.getUuid(), pAufgabe);
			aufgabenMap.put(pAufgabe.getSchluessel(), pAufgabe);
			if (!quietly) {
				fireObjectAddedEvent(pAufgabe);
			}
		}
	}

	/**
	 * @param pAufgabe MCAufgabe
	 * @return String J oder N
	 */
	protected String isGesperrtFuerRaetsel(MCAufgabe pAufgabe) {
		if (pAufgabe.getGesperrtFuerRaetsel() == null) {
			int anzahl = anzahlRaetselMitAufgabe(pAufgabe);
			pAufgabe.setGesperrtFuerRaetsel(anzahl == 0 ? MCAufgabe.NOT_LOCKED : MCAufgabe.LOCKED);
		}
		return pAufgabe.getGesperrtFuerRaetsel();
	}

	/**
	 * @param pAufgabe
	 * @return
	 */
	private int anzahlRaetselMitAufgabe(MCAufgabe pAufgabe) {
		List<Long> serienIds = getPersistenceservice().getMcAufgabenRepository().raetselIdMitAufgabe(pAufgabe);
		return serienIds.isEmpty() ? 0 : 1;
	}

	/**
	 * @param pObjects
	 * @param pPathWorkDir
	 * @param pProgressMonitor
	 * @return TODO
	 */
	protected IStatus generateLaTeXAndTransformToPs(Map<String, MCAufgabe> pObjects, String pPathWorkDir,
		IProgressMonitor pProgressMonitor) {
		Assert.isNotNull(pObjects, "pObjects");
		final int totalWork = pObjects.size() * 2;
		SubMonitor subMonitor = SubMonitor.convert(pProgressMonitor, 80);
		SubMonitor loopProgress = subMonitor.newChild(80).setWorkRemaining(totalWork);
		final File workDir = prepareWorkDir(pPathWorkDir);
		List<IStatus> statusList = new ArrayList<>();
		IStatus status = null;
		for (String key : pObjects.keySet()) {
			loopProgress.subTask("generiere " + key);
			status = processAufgabe(pObjects.get(key), workDir);
			if (!status.isOK()) {
				statusList.add(status);
			}
			loopProgress.worked(1);
		}
		subMonitor.subTask("lösche temporäre Dateien");
		subMonitor.setWorkRemaining(20);
		clearTemporaryLaTeXFiles(pPathWorkDir);
		subMonitor.worked(100);

		if (!statusList.isEmpty()) {
			MultiStatus result = new MultiStatus(IStammdatenservice.PLUGIN_ID, IStatus.WARNING,
				statusList.toArray(new IStatus[0]), "Beim Generieren sind Fehler aufgetreten", null);
			return result;
		}
		return new Status(IStatus.OK, IStammdatenservice.PLUGIN_ID, "alles paletti :)");
	}

	/**
	 * Generiert zur gegebenen Aufgabe ein Aufgabe-LaTeX-File und ein Lösung-LaTeX-File, compiliert und wandelt in ps
	 * um.
	 *
	 * @param pAufgabe
	 * @param pWorkDir
	 * @return
	 */
	private IStatus processAufgabe(MCAufgabe pAufgabe, File pWorkDir) {
		final String pathWorkDir = pWorkDir.getAbsolutePath();
		StringBuffer sb = new StringBuffer("Fehler beim Generieren, Compilieren und Umwandeln:");
		IStatus status = createLaTeX(pAufgabe, pathWorkDir, AUFGABE_LATEX_GENERATOR_XSL, true);
		boolean error = false;
		String path = null;
		final String pathPrefix = pAufgabe.getPathPrefix(pathWorkDir);
		if (status.isOK()) {
			path = GeneratorUtils.getPathPreviewLaTeXAufgabe(pathPrefix);
			status = compile(path);
			if (status.isOK()) {
				path = GeneratorUtils.getPathPreviewDviAufgabe(pathPrefix);
				status = dviPs(path);
				if (!status.isOK()) {
					error = true;
					sb.append("\n");
					sb.append(status.getMessage());
				}
			} else {
				error = true;
				sb.append("\n");
				sb.append(status.getMessage());
			}
		} else {
			error = true;
			sb.append("\n");
			sb.append(status.getMessage());
		}
		status = createLaTeX(pAufgabe, pathWorkDir, AUFGABE_LATEX_GENERATOR_XSL, false);
		if (status.isOK()) {
			path = GeneratorUtils.getPathPreviewLaTeXLoesung(pathPrefix);
			status = compile(path);
			if (status.isOK()) {
				path = GeneratorUtils.getPathPreviewDviLoesung(pathPrefix);
				status = dviPs(path);
				if (!status.isOK()) {
					error = true;
					sb.append("\n");
					sb.append(status.getMessage());
				}
			} else {
				error = true;
				sb.append("\n");
				sb.append(status.getMessage());
			}
		} else {
			error = true;
			sb.append("\n");
			sb.append(status.getMessage());
		}
		return error ? new Status(IStatus.ERROR, IStammdatenservice.PLUGIN_ID, sb.toString()) : new Status(IStatus.OK,
			IStammdatenservice.PLUGIN_ID, "success");
	}

	/**
	 * Generiert für eine einzelne Quizaufgabe 1 LaTeX-Datei.
	 * 
	 * @param pAufgabe
	 * @param pPathWorkDir
	 * @param pTemplate
	 * @param pGenerateAufgabe
	 * @return
	 */
	protected IStatus createLaTeX(MCAufgabe pAufgabe, String pPathWorkDir, String pTemplate, boolean pGenerateAufgabe) {
		Assert.isTrue(pPathWorkDir != null && !pPathWorkDir.isEmpty(), "pPathOutput darf nicht null oder leer sein.");
		final String pathPrefix = pAufgabe.getPathPrefix(pPathWorkDir);
		String pathResult = pGenerateAufgabe ? GeneratorUtils.getPathPreviewLaTeXAufgabe(pathPrefix) : GeneratorUtils
			.getPathPreviewLaTeXLoesung(pathPrefix);

		InputStream xslResource = getClass().getResourceAsStream(pTemplate);

		OutputStream out = null;
		try {
			File xmlTempFile = File.createTempFile("mcaufgabe", ".xml");
			LOG.info(xmlTempFile.getAbsolutePath());
			xmlTempFile.deleteOnExit();

			final String pLatexTemplatesPath = xmlTempFile.getParent() + LATEX_GENERATOR_TEMPLATES;
			checkLatexTemplates(pLatexTemplatesPath);
			final Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.setProperty("jaxb.encoding", "ISO-8859-1");
			marshaller.marshal(pAufgabe, xmlTempFile);
			File xslTempFile = File.createTempFile("transformation", ".xslt");
			xslTempFile.deleteOnExit();
			out = new FileOutputStream(xslTempFile);
			IOUtils.copy(xslResource, out);
			out.flush();
			Map<String, String> parameters = getXsltParameters(pGenerateAufgabe);
			final XSLProcessor processor = new XSLProcessor();
			processor.transform(xmlTempFile.getAbsolutePath(), xslTempFile.getAbsolutePath(), pathResult, null,
				parameters);
			LOG.info("mit xslt: Ausgabe nach [" + pathResult + "]");
			return new Status(IStatus.OK, IStammdatenservice.PLUGIN_ID, pAufgabe.getSchluessel() + " ok");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return new Status(IStatus.ERROR, IStammdatenservice.PLUGIN_ID, pAufgabe.getSchluessel()
				+ ": Marshalling oder XSL-Transformation fehlgeschlagen.");
		} finally {
			IOUtils.closeQuietly(out);
			IOUtils.closeQuietly(xslResource);
		}
	}

	/**
	 * MCAufgabe mit diesem Schluessel suchen.
	 *
	 * @param pSchluessel
	 * @return {@link Aufgabe} oder null (wenn Parameter null oder MCAufgabe nicht vorhanden.
	 */
	protected MCAufgabe aufgabeZuSchluessel(String pSchluessel) {
		if (pSchluessel == null) {
			return null;
		}
		checkAufgabenmap();
		for (MCAufgabe aufgabe : aufgabenMap.values()) {
			if (aufgabe.getSchluessel().equals(pSchluessel)) {
				return aufgabe;
			}
		}
		return null;
	}
}
