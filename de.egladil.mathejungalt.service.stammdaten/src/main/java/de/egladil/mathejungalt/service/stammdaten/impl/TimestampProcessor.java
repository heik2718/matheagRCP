/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.service.stammdaten.impl;

import java.util.Date;

/**
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class TimestampProcessor {

	private Date timestamp;

	private static TimestampProcessor instance;

	/**
   *
   */
	private TimestampProcessor() {
	}

	public Date getTimestampNow() {
		if (timestamp == null) {
			return new Date();
		}
		return timestamp;
	}

	/**
	 * Konfiguriert den Prozessor: setzt ein bestimmtes Datum, dass dann zurückgegeben wird. Parameter null setzt den
	 * Prozessor auf Systemzeit zurück.
	 *
	 * @param timestamp
	 */
	public void configure(Date timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * Setzt Prozessor auf Systemzeit.
	 */
	public void reset() {
		configure(null);
	}

	/**
	 * @return the instance
	 */
	public static TimestampProcessor getInstance() {
		if (instance == null) {
			instance = new TimestampProcessor();
		}
		return instance;
	}
}
