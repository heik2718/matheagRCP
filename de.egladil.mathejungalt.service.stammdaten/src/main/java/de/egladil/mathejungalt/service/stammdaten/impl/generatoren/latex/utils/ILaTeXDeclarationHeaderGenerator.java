package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils;

public interface ILaTeXDeclarationHeaderGenerator {

	public static enum LaTeXSeitenkonfiguration {
		ONESIDE,
		TWOSIDE
	};

	/**
	 * @param pSeitenkonfiguration die {@link LaTeXSeitenkonfiguration} (ONESIDE, TWOSIDE)
	 * @param pHeaderRightPage Bei zweiseitig rechter Seitenheader
	 * @return String
	 */
	public abstract String generateDeclaration(LaTeXSeitenkonfiguration pSeitenkonfiguration, String pHeaderRightPage);

	/**
	 * Der Pfad zum include-Verzeichnis relativ zum aufgabenarchiv bzw. loesungsarchiv. Default ist ../include
	 * 
	 * @return
	 */
	public abstract String getIncludePath();

}