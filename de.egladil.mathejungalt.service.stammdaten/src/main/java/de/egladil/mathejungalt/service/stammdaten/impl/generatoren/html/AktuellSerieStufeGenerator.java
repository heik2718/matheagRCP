//=====================================================
// Projekt: de.egladil.mathejungalt.service.stammdaten
// (c) Heike Winkelvoß
//=====================================================
// Dateiname: SerieIndexGenerator.java                            $
// $Revision:: 1                                     $
// $Modtime:: 29.03.2015                                $
//=====================================================

package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html;

/**
 * AktuellSerieStufeGenerator generiert die Website mit den pngs zu Aufgaben einer Stufe der aktuellen Serie. Diese
 * liegen unter vorschau/aktuell-1.html, vorschau/aktuell-2.html usw.
 */
public class AktuellSerieStufeGenerator extends AbstractSerieStufeGenerator {

	private static final String HTML_TEMPLATE_PATH = "/html/aktuell_serie_stufe_template.txt";

	/**
	 * Erzeugt eine Instanz von SerieIndexGenerator
	 */
	public AktuellSerieStufeGenerator() {
	}

	/**
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html.AbstractSerieStufeGenerator#getTemplatePath()
	 */
	@Override
	String getTemplatePath() {
		return HTML_TEMPLATE_PATH;
	}

	public String getFilename(int stufe) {
		return "aktuell-" + stufe + ".html";
	}

}
