/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * @author heike
 */
public class LoesungGenerator extends AbstractAufgabeGenerator {

	/**
  *
  */
	public LoesungGenerator() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.ILaTeXGenerator#bodyContents()
	 */
	@Override
	public String bodyContents(IStammdatenservice stammdatenservice) throws MatheJungAltException {
		StringBuffer sb = new StringBuffer();
		sb.append("\\setcounter{equation}{0}\n{");
		sb.append("\\bf Loesung ");
		sb.append(getAufgabenNummer());
		sb.append("}\\newline\n");
		sb.append("\\input{");
		sb.append("../loesungsarchiv/");
		sb.append(getAufgabe().getVerzeichnis());
		sb.append("/");
		sb.append(getAufgabe().getSchluessel());
		sb.append("_l}\n");
		sb.append("\\par\n");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator#getFilename()
	 */
	@Override
	public String getFilename() {
		return getAufgabenNummer() + "_l.tex";
	}
}
