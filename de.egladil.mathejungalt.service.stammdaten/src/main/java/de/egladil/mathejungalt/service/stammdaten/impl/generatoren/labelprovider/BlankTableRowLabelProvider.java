/**
 * 
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider;

/**
 * Die TableRow- "Tags" sind leer.
 * 
 * @author winkelv
 */
public class BlankTableRowLabelProvider implements ITableRowLabelProvider {

	/**
	 * 
	 */
	public BlankTableRowLabelProvider() {
		//
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.serviceimpl.generatoren.labelprovider.ITableRowLabelProvider#getEndText()
	 */
	@Override
	public String getEndText() {
		return "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.serviceimpl.generatoren.labelprovider.ITableRowLabelProvider#getStartText()
	 */
	@Override
	public String getStartText() {
		return "";
	}

}
