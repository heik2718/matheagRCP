/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.Messages;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils.ILaTeXDeclarationHeaderGenerator.LaTeXSeitenkonfiguration;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils.LaTeXEmptyHeadingsDeclarationGenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils.LaTeXFactory;

/**
 * Generator für die Serien-LaTeX- Files
 *
 * @author winkelv
 */
public abstract class AbstractSerieLaTeXGenerator extends AbstractLaTeXGenerator {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(AbstractSerieLaTeXGenerator.class);

	/** */
	private Serie serie;

	/** */
	private boolean initialized;

	/**
	 *
	 */
	public AbstractSerieLaTeXGenerator() {
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathe.core.serviceimpl.generatoren.IFilegenerator#generate()
	 */
	@Override
	public String generate(IStammdatenservice stammdatenservice) throws MatheJungAltException {
		checkInitialized();
		StringBuffer sb = new StringBuffer();
		if (isPagestyleEmpty()) {
			sb.append(new LaTeXEmptyHeadingsDeclarationGenerator().generateDeclaration(LaTeXSeitenkonfiguration.ONESIDE,
				"\n"));
		} else {
			String rightSideHeader = getRightSideHeader(); //$NON-NLS-1$
			sb.append(getHeaderGenerator().generateDeclaration(LaTeXSeitenkonfiguration.TWOSIDE, rightSideHeader));
		}
		sb.append(bodyContents(stammdatenservice));
		sb.append(LaTeXFactory.endDocument());
		return sb.toString();
	}

	/**
	 *
	 * @return
	 */
	protected String getRightSideHeader() {
		return "{" + Messages.getString("AbstractSerieLaTeXGenerator.rightsideheading") + Messages.getString("AbstractSerieLaTeXGenerator.serie_heading") + serie.getNummer() + "} - " //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			+ serie.getMonatJahrText() + "}\n";
	}

	/**
	 * @throws MatheJungAltException
	 */
	protected void checkInitialized() throws MatheJungAltException {
		if (serie == null) {
			String msg = "Initialisierungsfehler: erst setSerie() aufrufen."; //$NON-NLS-1$
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		super.checkInitialized();
	}

	/**
	 * @return the serie
	 */
	public Serie getSerie() {
		return serie;
	}

	/**
	 * @param pSerie the serie to set
	 */
	public void setSerie(Serie pSerie) {
		serie = pSerie;
	}

	/**
	 * @return the initialized
	 */
	public boolean isInitialized() {
		return initialized;
	}

	/**
	 * @param pInitialized the initialized to set
	 */
	public void setInitialized(boolean pInitialized) {
		initialized = pInitialized;
	}
}
