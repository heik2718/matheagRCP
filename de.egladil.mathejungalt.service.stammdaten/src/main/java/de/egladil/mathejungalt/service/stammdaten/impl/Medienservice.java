/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IStatus;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.file.DirectoryCleaner;
import de.egladil.base.latex.ILaTeXService;
import de.egladil.base.latex.ILaTeXService.LaTeXInteractionModes;
import de.egladil.base.utils.OsgiServiceLocator;
import de.egladil.mathejungalt.domain.medien.AbstractPrintmedium;
import de.egladil.mathejungalt.domain.medien.Autor;
import de.egladil.mathejungalt.domain.medien.Buch;
import de.egladil.mathejungalt.domain.medien.Zeitschrift;
import de.egladil.mathejungalt.persistence.api.IPersistenceservice;

/**
 * @author aheike
 */
public class Medienservice extends AbstractStammdatenservice {

	/** */
	private Map<Long, AbstractPrintmedium> medienMap;

	/** */
	private Map<Long, Autor> autorenMap;

	/**
	 * @param pPersistenceservice
	 */
	public Medienservice() {
		super();
	}

	/**
	 * @return
	 * @throws MatheJungAltException
	 */
	protected Buch buchAnlegen() throws MatheJungAltException {
		if (medienMap == null) {
			loadMedien();
		}
		return null;
	}

	/**
	 * @return
	 */
	protected Collection<AbstractPrintmedium> getMedien() {
		if (medienMap == null) {
			loadMedien();
		}
		return medienMap.values();
	}

	/**
	 *
	 */
	private void loadMedien() {
		medienMap = new HashMap<Long, AbstractPrintmedium>();
		List<AbstractPrintmedium> medien = getPersistenceservice().getMedienRepository().findAllMedien();
		for (AbstractPrintmedium medium : medien) {
			medienMap.put(medium.getId(), medium);
		}
	}

	/**
	 * @return
	 * @throws MatheJungAltException
	 */
	protected Zeitschrift zeitschriftAnlegen() throws MatheJungAltException {
		if (medienMap == null) {
			loadMedien();
		}
		return null;
	}

	/**
	 * @param pMedium
	 */
	protected void mediumSpeichern(AbstractPrintmedium pMedium) {
		// TODO Auto-generated method stub
	}

	/**
	 * Neuen Autor erzeugen.
	 *
	 * @return {@link Autor}
	 * @throws MatheJungAltException
	 */
	protected Autor autorAnlegen() throws MatheJungAltException {
		if (autorenMap == null) {
			loadAutoren();
		}
		return null;
	}

	/**
	 * Speichert den Autor.
	 *
	 * @param pAutor {@link Autor} der Autor
	 */
	protected void autorSpeichern(final Autor pAutor) {
	}

	/**
	 * @return
	 * @throws MatheJungAltException
	 */
	protected Collection<Autor> getAutoren() throws MatheJungAltException {
		if (autorenMap == null) {
			loadAutoren();
		}
		return autorenMap.values();
	}

	/**
	 *
	 */
	private void loadAutoren() {
		autorenMap = new HashMap<Long, Autor>();
		IPersistenceservice persistenceService = getPersistenceservice();
		List<Autor> autoren = persistenceService.getAutorenRepository().findAllAutoren();
		for (Autor autor : autoren) {
			autorenMap.put(autor.getId(), autor);
		}
	}

	/**
	 * Werfen temporäre Dateien weg.
	 *
	 * @param pPathWorkDir
	 */
	protected void clearTemporaryLaTeXFiles(String pPathWorkDir) {
		final List<String> suffixes = Arrays.asList(new String[] { "aux", "dvi", "log", "out" });
		final DirectoryCleaner cleaner = new DirectoryCleaner();
		cleaner.deleteFiles(pPathWorkDir, suffixes);
	}

	/**
	 * @param pLatexFilePath
	 * @return IStatus
	 */
	protected IStatus compile(String pLatexFilePath) {
		ILaTeXService latexService = new OsgiServiceLocator().getService(ILaTeXService.class);
		IStatus status = latexService.compile(pLatexFilePath, LaTeXInteractionModes.nonstopmode);
		return status;
	}

	/**
	 * Aufruf von pdflatex
	 * 
	 * @param pLatexFilePath
	 * @return IStatus
	 */
	protected IStatus pdflatex(String pLatexFilePath) {
		ILaTeXService latexService = new OsgiServiceLocator().getService(ILaTeXService.class);
		IStatus status = latexService.compile(pLatexFilePath, LaTeXInteractionModes.nonstopmode);
		return status;
	}

	/**
	 * Wandelt dvi in ps um.
	 *
	 * @param pWorkDir
	 * @param dviFilePath
	 * @return IStatus
	 */
	protected IStatus dviPs(String dviFilePath) {
		ILaTeXService latexService = new OsgiServiceLocator().getService(ILaTeXService.class);
		IStatus status = latexService.dviPs(dviFilePath);
		return status;
	}
}
