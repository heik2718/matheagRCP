/**
 * 
 */
package de.egladil.mathejungalt.service.stammdaten.impl.consistence;

import java.util.List;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;

/**
 * @author aheike
 */
public class ConsistencecheckService {

	/**
	 * 
	 */
	public ConsistencecheckService() {
	}

	/**
	 * Ruft den Konsistenzcheck der {@link IConsistencecheckProvider} auf.
	 * 
	 * @param pConsistencecheckProviders Liste von {@link IConsistencecheckProvider}-Objekten
	 * @param pDomainObjects Liste von {@link AbstractMatheAGObject}
	 */
	public void executeConsistencecheck(List<IConsistencecheckProvider> pConsistencecheckProviders,
		List<AbstractMatheAGObject> pDomainObjects) throws MatheJungAltException {
		for (IConsistencecheckProvider provider : pConsistencecheckProviders) {
			for (AbstractMatheAGObject domainObject : pDomainObjects) {
				if (provider.canCheck(domainObject)) {
					provider.checkConsistent(domainObject);
				}
			}
		}
	}

	/**
	 * Ruft den Konsistenzcheck der {@link IConsistencecheckProvider} auf.
	 * 
	 * @param pConsistencecheckProviders Liste von {@link IConsistencecheckProvider}-Objekten
	 * @param pDomainObjects Liste von {@link AbstractMatheAGObject}
	 */
	public void executeConsistencecheckOnAufgabensammlung(List<IConsistencecheckProvider> pConsistencecheckProviders,
		List<IAufgabensammlung> pAufgabensammlungen) throws MatheJungAltException {
		for (IConsistencecheckProvider provider : pConsistencecheckProviders) {
			for (IAufgabensammlung aufgabensammlung : pAufgabensammlungen) {
				if (provider.canCheck(aufgabensammlung)) {
					provider.checkConsistent((AbstractMatheAGObject) aufgabensammlung);
				}
			}
		}
	}
}
