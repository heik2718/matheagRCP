/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.service.stammdaten.quiz;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.runtime.Assert;

import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.domain.types.MCAufgabenstufen;

/**
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class HtmlAndroidImageTagBuilder {

	/**
	 * @param stringToProcess
	 * @param stufe
	 * @param quizschluessel
	 * @param nummer
	 * @return
	 */
	String transform(String stringToProcess, String stufe, String quizschluessel, String nummer) {
		int first = stringToProcess.indexOf("aufgabe");
		int last = StringUtils.length(stringToProcess);
		String startString = stringToProcess.substring(0, first);
		String endString = stringToProcess.substring(first, last);
		String result = startString + "file:///android_asset/files/" + stufe + "/" + quizschluessel + "/" + nummer
			+ "/" + endString;
		return result;
	}

	/**
	 * @param stringToProcess
	 * @param item
	 * @return
	 */
	public String transform(String stringToProcess, MCArchivraetselItem item) {
		Assert.isNotNull(item, "item");
		MCArchivraetsel raetsel = item.getRaetsel();
		String stufe = mcAufgabenstufeToAndroidStufeString(raetsel.getTitel(), raetsel.getStufe());
		String schluessel = raetsel.getSchluessel();
		String nummer = QuizPathProvider.getRelativPathQuizitem(item);
		return this.transform(stringToProcess, stufe, schluessel, nummer);
	}

	/**
	 * @param titel
	 * @param stufe
	 * @return
	 */
	String mcAufgabenstufeToAndroidStufeString(String titel, MCAufgabenstufen stufe) {
		return stufe.mcAufgabenstufeToAndroidStufeString(titel);
	}
}
