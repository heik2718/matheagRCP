/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.service.stammdaten.quiz;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.Assert;

import de.egladil.base.exceptions.EgladilBusinessException;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class RunHtlatexBatGenerator implements IPrepareHtlatexStep<MCArchivraetsel> {

	private static final String FILENAME = "run.bat";

	/**
	 * Generiert eine Datei run.bat, die die process_xx.bat-Dateien aufruft.
	 *
	 * @param raetsel
	 * @param pathRootDir
	 * @return String absoluter Pfad zur bat-Datei.
	 */
	public String generate(MCArchivraetsel raetsel, String pathRootDir) {
		Assert.isNotNull(raetsel);
		Assert.isNotNull(pathRootDir);
		String result = pathRootDir + "\\" + FILENAME;
		List<String> lines = new ArrayList<String>();
		for (MCArchivraetselItem item : raetsel.getItemsSorted()) {
			String nummer = QuizPathProvider.getRelativPathQuizitem(item);
			String line = "call " + IStammdatenservice.PROCESS_HTLATEX_FILENAME_PREFIX + nummer
				+ IStammdatenservice.PROCESS_HTLATEX_FILENAME_SUFFIX;
			lines.add(line);
		}
		lines.add("cd " + ".\\" + raetsel.getSchluessel());
		lines.add("del *.tex");
		lines.add("cd ..");
		lines.add("ren *_pre.txt *_post.txt");
		lines.add("echo fertig");
		lines.add("pause");
		File file = new File(result);
		try {
			FileUtils.writeLines(file, lines);
			return result;
		} catch (IOException e) {
			throw new EgladilBusinessException("Fehler beim Schreiben der Datei [" + result + "]: " + e.getMessage(), e);
		}
	}
}
