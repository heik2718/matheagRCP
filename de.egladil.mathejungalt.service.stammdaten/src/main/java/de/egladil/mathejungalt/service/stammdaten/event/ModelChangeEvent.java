/**
 * 
 */
package de.egladil.mathejungalt.service.stammdaten.event;

import java.util.EventObject;

/**
 * @author aheike
 */
public class ModelChangeEvent extends EventObject {

	/** */
	public static final String ADD_EVENT = "added";

	/** */
	public static final String REMOVE_EVENT = "removed";

	/** */
	public static final String PROPERTY_CHANGE_EVENT = "changed";

	/** */
	private static final long serialVersionUID = 4005721639172576708L;

	/** */
	private String eventType;

	/** */
	private String property;

	/** */
	private Object oldValue;

	/** */
	private Object newValue;

	/**
	 * @param pSource
	 */
	public ModelChangeEvent(Object pSource) {
		super(pSource);
	}

	/**
	 * @param pEventType String die Event-Art.
	 * @param pSource Object Quellobjekt f�r das Event
	 * @param pOldValue Object alter Wert
	 * @param pNewValue Object neuer Wert
	 * @param pProperty String name der ge�nderten Property
	 */
	public ModelChangeEvent(String pEventType, Object pSource, Object pOldValue, Object pNewValue, String pProperty) {
		super(pSource);
		eventType = pEventType;
		oldValue = pOldValue;
		newValue = pNewValue;
		property = pProperty;
	}

	/**
	 * @return the eventType
	 */
	public String getEventType() {
		return eventType;
	}

	/**
	 * @return the property
	 */
	public String getProperty() {
		return property;
	}

	/**
	 * @return the oldValue
	 */
	public Object getOldValue() {
		return oldValue;
	}

	/**
	 * @return the newValue
	 */
	public Object getNewValue() {
		return newValue;
	}
}
