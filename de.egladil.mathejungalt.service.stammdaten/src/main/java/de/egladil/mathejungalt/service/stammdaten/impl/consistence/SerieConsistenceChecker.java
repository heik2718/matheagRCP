/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.consistence;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.exceptions.MatheJungAltInconsistentEntityException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;

/**
 * @author aheike
 */
public class SerieConsistenceChecker implements IConsistencecheckProvider {

	private final AufgabensammlungItemConsistenceCheckDelegate itemsCheckDelegate = new AufgabensammlungItemConsistenceCheckDelegate();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.consistencecheck.IConsistencecheckProvider#canCheck(de.egladil.mathejungalt.domain
	 * .AbstractMatheAGObject)
	 */
	@Override
	public boolean canCheck(Object pObject) {
		return pObject instanceof Serie;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.consistencecheck.IConsistencecheckProvider#checkConsistent(de.egladil.mathejungalt
	 * .domain.AbstractMatheAGObject)
	 */
	@Override
	public void checkConsistent(AbstractMatheAGObject pObject) throws MatheJungAltException {
		Serie serie = (Serie) pObject;
		if (serie.getDatum() == null) {
			throw new MatheJungAltInconsistentEntityException("Serie: das Datum darf nicht null sein.");
		}
		if (serie.getMonatJahrText() == null || serie.getMonatJahrText().length() == 0) {
			throw new MatheJungAltInconsistentEntityException(
				"Serie: das Monat-Jahr-Text darf nicht null oder leer sein.");
		}
		if (serie.getKey() == null) {
			throw new MatheJungAltInconsistentEntityException("Serie: die Nummer darf nicht null sein.");
		}
		if (serie.getRunde() == null) {
			throw new MatheJungAltInconsistentEntityException("Serie: die Runde darf nicht null sein.");
		}
		itemsCheckDelegate.checkItems(serie);
	}
}
