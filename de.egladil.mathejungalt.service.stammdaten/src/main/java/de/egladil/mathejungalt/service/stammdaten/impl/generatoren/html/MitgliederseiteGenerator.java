//=====================================================
// Projekt: de.egladil.mathejungalt.service.stammdaten
// (c) Heike Winkelvoß
//=====================================================
// Dateiname: MItgliederseiteGeneratorNeu.java                            $
// $Revision:: 1                                     $
// $Modtime:: 29.03.2015                                $
//=====================================================

package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.Messages;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.contentprovider.MitgliederTableContentProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.reports.MitgliedReportUtil;

/**
 * MItgliederseiteGeneratorNeu
 */
public class MitgliederseiteGenerator {

	private static final Logger LOG = LoggerFactory.getLogger(MitgliederseiteGenerator.class);

	private static final String HTML_TEMPLATE_PATH = "/html/mitgliederseite_template.txt";

	private static final String TABLE_PLACEHOLDER = "MITGLIEDER_TABLE_REPLACE";

	private MitgliederTableContentProvider contentProvider = new MitgliederTableContentProvider();

	private MitgliedReportUtil reportUtil = new MitgliedReportUtil();

	/**
	 * Erzeugt eine Instanz von MItgliederseiteGeneratorNeu
	 */
	public MitgliederseiteGenerator() {
	}

	/**
	 *
	 * TODO
	 *
	 * @param stammdatenservice
	 * @return
	 */
	public String generate(IStammdatenservice stammdatenservice) {
		InputStream in = null;
		try {
			in = this.getClass().getResourceAsStream(HTML_TEMPLATE_PATH);
			StringWriter sw = new StringWriter();
			IOUtils.copy(in, sw);
			String content = sw.toString();
			content = content.replaceAll(TABLE_PLACEHOLDER, getMitgliederTable(stammdatenservice));
			content = HtmlFactory.setAktuellesDatum(content);
			return content;
		} catch (IOException e) {
			LOG.error(e.getMessage());
			throw new MatheJungAltException("Fehler beim Laden des Templates '" + HTML_TEMPLATE_PATH
				+ "': mal classpath prüfen!");

		} finally {
			IOUtils.closeQuietly(in);
		}
	}

	private String getMitgliederTable(IStammdatenservice stammdatenservice) {
		StringBuffer sb = new StringBuffer();
		for (int klasse = 0; klasse < 13; klasse++) {
			List<Mitglied> mitglieder = contentProvider.getAktiveMitgliederSorted(stammdatenservice, klasse);
			if (mitglieder != null && !mitglieder.isEmpty()) {
				sb.append(getSubsection(mitglieder, klasse));
			}
		}

		List<Mitglied> mitglieder = contentProvider.getAktiveErwachseneSorted(stammdatenservice);
		if (mitglieder != null && !mitglieder.isEmpty()) {
			sb.append(getSubsection(mitglieder, 14));
		}

		return sb.toString();
	}

	/**
	 *
	 * TODO
	 *
	 * @param mitglieder
	 * @param klasse
	 * @return
	 */
	private Object getSubsection(List<Mitglied> mitglieder, int klasse) {
		StringBuffer sb = new StringBuffer();
		switch (klasse) {
		case 0:
			sb.append(Messages.getString("MitgliederseiteGenerator_1"));
//			sb.append("<section><h2>Vorschule</h2><table>");
			break;
		case 14:
			sb.append(Messages.getString("MitgliederseiteGenerator_2"));
//			sb.append("<section><h2>Erwachsene</h2><table>");
			break;
		default:
			sb.append(Messages.getString("MitgliederseiteGenerator_3"));
			sb.append(" ");
			sb.append(klasse);
			sb.append(Messages.getString("MitgliederseiteGenerator_4"));
//			sb.append("</h2><table>");
			break;
		}
		for (int i = 0; i < mitglieder.size(); i++) {
			Mitglied mitglied = mitglieder.get(i);
			sb.append("<tr>");
			sb.append(Messages.getString("MitgliederseiteGenerator_5"));
			sb.append(HtmlFactory.nameAlterKlasseText(mitglied, false));
			sb.append("</td>");
			sb.append(mitgliedPunkte(mitglied));
		}
		sb.append("</tbody></table></section>");
		return sb.toString();
	}

	/**
	 * @param pMitglied
	 * @return
	 */
	private String mitgliedPunkte(Mitglied pMitglied) {
		StringBuffer sb = new StringBuffer();
		sb.append(Messages.getString("MitgliederseiteGenerator_6"));
//		sb.append("<td class=\"imagecell\">");
		int anzahlDiplome = pMitglied.anzahlDiplome();
		for (int i = 0; i < anzahlDiplome; i++) {
			sb.append(Messages.getString("MitgliederseiteGenerator_src_diplom"));
		}
		int anzahl = reportUtil.getNormalpunkterest(pMitglied);
		for (int i = 0; i < anzahl; i++) {
			sb.append(Messages.getString("MitgliederseiteGenerator_src_bonus"));
		}
		anzahl = reportUtil.getErfinderwinkelrest(pMitglied);
		for (int i = 0; i < anzahl; i++) {
			sb.append(Messages.getString("MitgliederseiteGenerator_src_erf"));
		}
		sb.append("</td></tr>");
		return sb.toString();
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param contentProvider neuer Wert der Membervariablen contentProvider
	 */
	public void setContentProvider(MitgliederTableContentProvider contentProvider) {
		this.contentProvider = contentProvider;
	}

	/**
	 *
	 *
	 * @return
	 */
	public String getFilename() {
		return Messages.getString("page_mitglieder"); //$NON-NLS-1$
	}
}
