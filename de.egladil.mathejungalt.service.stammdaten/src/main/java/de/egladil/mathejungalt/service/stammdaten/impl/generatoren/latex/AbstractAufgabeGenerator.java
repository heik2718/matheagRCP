/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex;

import org.eclipse.core.runtime.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils.ILaTeXDeclarationHeaderGenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils.ILaTeXDeclarationHeaderGenerator.LaTeXSeitenkonfiguration;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils.LaTeXEmptyHeadingsDeclarationGenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils.LaTeXFactory;

/**
 * @author heike
 */
public abstract class AbstractAufgabeGenerator extends AbstractLaTeXGenerator {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractAufgabeGenerator.class);

	private Aufgabe aufgabe;

	private String aufgabenNummer;

	private final ILaTeXDeclarationHeaderGenerator headerGenerator;

	/**
   *
   */
	public AbstractAufgabeGenerator() {
		headerGenerator = new LaTeXEmptyHeadingsDeclarationGenerator();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator#generate()
	 */
	@Override
	public String generate(IStammdatenservice stammdatenservice) throws MatheJungAltException {
		StringBuffer sb = new StringBuffer();
		sb.append(headerGenerator.generateDeclaration(LaTeXSeitenkonfiguration.ONESIDE, ""));
		sb.append(bodyContents(stammdatenservice));
		sb.append(LaTeXFactory.endDocument());
		return sb.toString();
	}

	/**
	 * @param pAufgabe Aufgabe darf nicht null sein.
	 * @param pAufgabeNummer String Nummer im LaTeX darf null sein. Wenn null, dann wird schluessel genommen.
	 */
	public void init(Aufgabe pAufgabe, String pAufgabeNummer) {
		Assert.isNotNull(pAufgabe, "pAufgabe darf nicht null sein");
		aufgabe = pAufgabe;
		aufgabenNummer = pAufgabeNummer == null ? aufgabe.getSchluessel() : pAufgabeNummer;
	}

	/**
	 * @return the aufgabe
	 */
	protected final Aufgabe getAufgabe() {
		return aufgabe;
	}

	/**
	 * @return the aufgabenNummer
	 */
	protected final String getAufgabenNummer() {
		return aufgabenNummer;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.AbstractLaTeXGenerator#checkInitialized()
	 */
	@Override
	protected void checkInitialized() throws MatheJungAltException {
		super.checkInitialized();
		if (aufgabe == null) {
			String msg = "nicht korrekt initialisiert! Attribut aufgabe ist null. Vor generate() muss init(Aufgabe, String) aufgerufen werden.";
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
	}
}
