/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html;

import java.text.DecimalFormat;
import java.util.List;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.mcraetsel.MCAntwortbuchstaben;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.Messages;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator;
import de.egladil.mcmatheraetsel.domain.spiel.Raetsel;

/**
 * Generiert ein js-File mit genau einem Quizspiel aus dem gegebenen Raetsel.
 *
 * @author heike
 */
public class QuizJsGenerator implements IFilegenerator {

	private static DecimalFormat DF = new DecimalFormat("#.##");

	private final MCArchivraetsel quiz;

	private final Raetsel raetsel;

	/**
	 * @param pRaetsel
	 */
	public QuizJsGenerator(MCArchivraetsel pRaetsel) {
		quiz = pRaetsel;
		raetsel = Raetsel.createFromArchiv(quiz);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator#getFilename()
	 */
	@Override
	public String getFilename() {
		return quiz.getSchluessel() + ".js";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator#generate()
	 */
	@Override
	public String generate(IStammdatenservice stammdatenservice) throws MatheJungAltException {
		StringBuffer sb = new StringBuffer();
		final int anzahl = quiz.getItems().size();
		sb.append(Messages.getString("quizj1_numques", new Object[] { anzahl }));
		sb.append(Messages.getString("quizjs_credit", new Object[] { getFormated(raetsel.startguthabenBerechnen()) }));
		sb.append(Messages.getString("quizjs_max_punkte",
			new Object[] { getFormated(raetsel.maximalePunktzahlBerechnen()) }));
		sb.append(Messages.getString("quizjs_letters", new Object[] { anzahl }));
		sb.append(Messages.getString("quizjs_answers", new Object[] { anzahl }));
		sb.append(Messages.getString("quizjs_points", new Object[] { anzahl }));
		sb.append(Messages.getString("quizjs_anzChoices", new Object[] { anzahl }));
		sb.append(Messages.getString("quizjs_checked_answers", new Object[] { anzahl }));

		final List<MCArchivraetselItem> itemsSorted = quiz.getItemsSorted();
		for (int i = 0; i < itemsSorted.size(); i++) {
			final MCArchivraetselItem item = itemsSorted.get(i);
			sb.append(generate(item, i));
		}

		sb.append(Messages.getString("quijs_function_mark"));

		return sb.toString();
	}

	private String getFormated(double zahl) {
		String str = DF.format(zahl);
		String result = str.replaceAll(",", ".");
		return result;
	}

	/**
	 * Generiert den Item-Teil der Quizseite
	 *
	 * @param pItem
	 * @return
	 */
	private String generate(MCArchivraetselItem pItem, int index) {
		StringBuffer sb = new StringBuffer();
		final MCAufgabe aufgabe = pItem.getAufgabe();
		sb.append(Messages.getString("quizitem_js_letters", new Object[] { index,
			aufgabe.getLoesungsbuchstabe().toString() }));
		sb.append(Messages.getString("quizitem_js_answers",
			new Object[] { index, findAntwort(aufgabe, aufgabe.getLoesungsbuchstabe()) }));
		sb.append(Messages.getString("quizitem_js_points", new Object[] { index, aufgabe.getPunkte() }));
		sb.append(Messages.getString("quizitem_js_anzChoices",
			new Object[] { index, aufgabe.getAnzahlAntwortvorschlaege() }));
		sb.append(Messages.getString("quizitem_js_checked", new Object[] { index }));

		return sb.toString();
	}

	/**
	 * @param aufgabe
	 * @param letter
	 * @return
	 */
	private String findAntwort(MCAufgabe aufgabe, MCAntwortbuchstaben letter) {
		switch (letter) {
		case A:
			return aufgabe.getAntwortA() == null || aufgabe.getAntwortA().isEmpty() ? "A" : aufgabe.getAntwortA();
		case B:
			return aufgabe.getAntwortB() == null || aufgabe.getAntwortB().isEmpty() ? "B" : aufgabe.getAntwortB();
		case C:
			return aufgabe.getAntwortC() == null || aufgabe.getAntwortC().isEmpty() ? "C" : aufgabe.getAntwortC();
		case D:
			return aufgabe.getAntwortD() == null || aufgabe.getAntwortA().isEmpty() ? "D" : aufgabe.getAntwortD();
		case E:
			return aufgabe.getAntwortE() == null || aufgabe.getAntwortA().isEmpty() ? "E" : aufgabe.getAntwortE();
		default:
			throw new IllegalArgumentException("Falscher MCAntwortbuchstabe [" + letter + "]");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator#getEncoding()
	 */
	@Override
	public String getEncoding() {
		return ENCODING_UTF;
	}
}
