/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.latex;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;

/**
 * @author aheike
 */
public class LaTeXMinikaenguruStufeTableLabelProvider extends AbstractLaTeXTableLabelProvider {

	private static final Logger LOG = LoggerFactory.getLogger(LaTeXMinikaenguruStufeTableLabelProvider.class);

	/** */
	private int stufe = -1;

	/**
	 *
	 */
	public LaTeXMinikaenguruStufeTableLabelProvider(String fontSize) {
		super(fontSize);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.ITableLabelProvider#getStartText()
	 */
	@Override
	public String getStartText() {
		if (stufe < 3) {
			String msg = "Stufe nicht gesetzt: erst setStufe() aufrufen.";
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		StringBuffer sb = new StringBuffer();
		sb.append("%                      ");
		sb.append(stufe);
		sb.append(" PUNKTE\n");
		sb.append("%==============================================================================\n");
		switch (stufe) {
		case 3:
			sb.append("\\bild{7.5cm}{3punkte.eps}\n");
			break;
		case 4:
			sb.append("\\bild{6cm}{4punkte.eps}\n");
			break;
		default:
			sb.append("\\bild{6.5cm}{5punkte.eps}\n");
			break;
		}
		return sb.toString();
	}

	/**
	 * @param pStufe the stufe to set
	 */
	public void setStufe(int pStufe) {
		if (pStufe < 3 || pStufe > 5) {
			String msg = "ungueltige Stufe " + pStufe + ": nur 3 bis 5 erlaubt";
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		stufe = pStufe;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.latex.AbstractLaTeXTableLabelProvider
	 * #getEndText()
	 */
	@Override
	public String getEndText() {
		return "\n";
	}

}
