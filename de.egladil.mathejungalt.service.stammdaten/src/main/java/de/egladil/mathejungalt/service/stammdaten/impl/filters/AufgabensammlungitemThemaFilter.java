/**
 * 
 */
package de.egladil.mathejungalt.service.stammdaten.impl.filters;

import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.types.EnumTypes.Thema;

/**
 * @author aheike
 */
public class AufgabensammlungitemThemaFilter {

	/**
	 * 
	 */
	public AufgabensammlungitemThemaFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param pItem
	 * @param pThema
	 * @return
	 */
	public boolean select(IAufgabensammlungItem pItem, Thema pThema) {
		return pThema.equals(pItem.getAufgabe().getThema());
	}

}
