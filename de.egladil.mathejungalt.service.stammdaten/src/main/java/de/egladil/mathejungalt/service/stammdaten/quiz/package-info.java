/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
/**
 * Enthält sourcen für Generierung von Web-basierten Quizen für Mathequiz im WEB und auf ANDROID
 *
 * @author Heike Winkelvoß (public@egladil.de)
 *
 */
package de.egladil.mathejungalt.service.stammdaten.quiz;