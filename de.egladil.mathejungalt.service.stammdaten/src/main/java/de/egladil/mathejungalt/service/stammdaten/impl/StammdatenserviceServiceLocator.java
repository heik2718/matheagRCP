/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.persistence.api.IPersistenceservice;

/**
 * @author aheike
 */
public class StammdatenserviceServiceLocator {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(StammdatenserviceServiceLocator.class);

	/** */
	private List<IPersistenceservice> persistenceServices;

	/**
	 *
	 */
	public StammdatenserviceServiceLocator() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Sucht den passenden Service zu der Klasse und dem Protokoll.
	 *
	 * @param pClass
	 * @param pProtokoll
	 * @return Object falls Service gefunden.
	 * @throws DaktyVisException falls der Service nicht gefunden wurde.
	 */
	public Object getService(Class<?> pClass, String pProtokoll) throws MatheJungAltException {
		if (pClass.equals(IPersistenceservice.class)) {
			return getPersistenceService(pProtokoll);
		}
		String msg = "Kein Service fuer " + pClass.getName() + " vorhanden";
		LOG.error(msg);
		throw new MatheJungAltException(msg);
	}

	/**
	 * Der erste PersistenceService, der das gegebene Protokoll unterstuetzt, wird zurueckgegeben.
	 *
	 * @param pProtokoll String Protokoll (z.B. db)
	 * @return {@link IPersistenceservice} oder null, wenn es keinen Service gibt, der das Protokoll unterst�tzt
	 */
	private IPersistenceservice getPersistenceService(String pProtokoll) {

		for (IPersistenceservice service : persistenceServices) {
			final String[] protokolle = service.getProtokolle();
			for (int i = 0; i < protokolle.length; i++) {
				if (pProtokoll.equalsIgnoreCase(protokolle[i])) {
					return service;
				}
			}
		}
		String msg = "Das Protokoll '" + pProtokoll + "' wird von keinem PersistenceService unterstützt.";
		LOG.error(msg);
		throw new MatheJungAltException(msg);
	}

	/**
	 * @param pPersistenceServices the persistenceServices to set
	 */
	public void setPersistenceServices(List<IPersistenceservice> pPersistenceServices) {
		persistenceServices = pPersistenceServices;
	}

}
