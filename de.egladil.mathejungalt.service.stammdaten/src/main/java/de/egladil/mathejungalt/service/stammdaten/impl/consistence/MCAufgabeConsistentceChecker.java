/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.consistence;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;

/**
 * @author Heike Winkelvoss (www.egladil.de)
 */
public class MCAufgabeConsistentceChecker implements IConsistencecheckProvider {

	/** */
	private final ConsistenceCheckDelegate consistenceCheckDelegate;

	/**
	 *
	 */
	public MCAufgabeConsistentceChecker() {
		consistenceCheckDelegate = new ConsistenceCheckDelegate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.consistencecheck.IConsistencecheckProvider#canCheck(de.egladil.mathejungalt.domain
	 * .AbstractMatheAGObject)
	 */
	@Override
	public boolean canCheck(Object pObject) {
		try {
			if (pObject == null)
				return false;
			@SuppressWarnings("unused")
			MCAufgabe param = (MCAufgabe) pObject;
			return true;
		} catch (ClassCastException e) {
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.consistencecheck.IConsistencecheckProvider#checkConsistent(de.egladil.mathejungalt
	 * .domain.AbstractMatheAGObject)
	 */
	@Override
	public void checkConsistent(AbstractMatheAGObject pObject) throws MatheJungAltException {
		StringBuffer sb = new StringBuffer();
		boolean consistent = true;
		MCAufgabe aufgabe = (MCAufgabe) pObject;
		aufgabe.checkConsistent();
		// try {
		// consistenceCheckDelegate.checkStringKey(aufgabe.getKey(), 5, "");
		// } catch (MatheJungAltInconsistentEntityException e) {
		// sb.append(e.getMessage());
		// consistent = false;
		// }
		// if (aufgabe.getArt() == null) {
		// sb.append("\ndie Aufgabenart darf nicht null sein");
		// consistent = false;
		// }
		// if (aufgabe.getThema() == null) {
		// sb.append("\ndas Thema darf nicht null sein");
		// consistent = false;
		// }
		// if (aufgabe.getStufe() == null) {
		// sb.append("\ndie Stufe darf nicht null sein");
		// consistent = false;
		// }
		// if (aufgabe.getAntwortA() != null && aufgabe.getAntwortA().length() > 255) {
		// sb.append("\nAntwort A darf nicht laenger als 255 Zeichen sein");
		// consistent = false;
		// }
		// if (aufgabe.getAntwortB() != null && aufgabe.getAntwortB().length() > 255) {
		// sb.append("\nAntwort B darf nicht laenger als 255 Zeichen sein");
		// consistent = false;
		// }
		// if (aufgabe.getAntwortC() != null && aufgabe.getAntwortC().length() > 255) {
		// sb.append("\nAntwort C darf nicht laenger als 255 Zeichen sein");
		// consistent = false;
		// }
		// if (aufgabe.getAntwortD() != null && aufgabe.getAntwortD().length() > 255) {
		// sb.append("\nAntwort D darf nicht laenger als 255 Zeichen sein");
		// consistent = false;
		// }
		// if (aufgabe.getAntwortE() != null && aufgabe.getAntwortE().length() > 255) {
		// sb.append("\nAntwort E darf nicht laenger als 255 Zeichen sein");
		// consistent = false;
		// }
		// if (aufgabe.isTabelleGenerieren()
		// && (aufgabe.getAntwortA() == null || aufgabe.getAntwortA().isEmpty() || aufgabe.getAntwortB() == null
		// || aufgabe.getAntwortB().isEmpty() || aufgabe.getAntwortC() == null || aufgabe.getAntwortC().isEmpty()
		// || aufgabe.getAntwortD() == null || aufgabe.getAntwortD().isEmpty() || aufgabe.getAntwortE() == null ||
		// aufgabe
		// .getAntwortE().isEmpty())) {
		// sb.append("\nalle Antwortvorschläge müssen gefüllt sein");
		// consistent = false;
		// }
		// if (aufgabe.getLoesungsbuchstabe() == null) {
		// sb.append("\nder Lösungsbuchstabe darf nicht null sein");
		// consistent = false;
		// }
		// if (aufgabe.getPunkte() == null) {
		// sb.append("\ndie Punkte dürfen nicht null sein");
		// consistent = false;
		// }
		// if (aufgabe.getQuelle() == null || aufgabe.getQuelle().isEmpty()) {
		// sb.append("\ndie Quelle fehlt");
		// consistent = false;
		// }
		// if (MCAufgabenarten.E.equals(aufgabe.getArt())) {
		// if (!MCAufgabe.DEFAULT_QUELLE.equals(aufgabe.getQuelle())) {
		// sb.append("\nBei Eigenbau ist nur Nullquelle erlaubt!");
		// consistent = false;
		// }
		// } else {
		// if ((aufgabe.getQuelle() == null || aufgabe.getQuelle().isEmpty()) ||
		// MCAufgabe.DEFAULT_QUELLE.equals(aufgabe.getQuelle())) {
		// sb.append("\nZitat oder Nachbau benötigt eine von der Nullquelle verschiedene Quelle!");
		// consistent = false;
		// }
		// }
		// if (aufgabe.getTitel() == null || aufgabe.getTitel().length() == 0 || aufgabe.getTitel().length() > 100) {
		// sb.append("\nder Titel darf nicht null oder leer sein und nicht laenger als 100 Zeichen");
		// consistent = false;
		// }
		// if (aufgabe.getBemerkung() != null && aufgabe.getBemerkung().length() > 255) {
		// sb.append("\ndie Beschreibung darf nicht laenger als 255 Zeichen sein");
		// consistent = false;
		// }
		// if (!consistent) {
		// throw new MatheJungAltInconsistentEntityException("Quizaufgabe:\n" + sb.toString());
		// }
	}
}
