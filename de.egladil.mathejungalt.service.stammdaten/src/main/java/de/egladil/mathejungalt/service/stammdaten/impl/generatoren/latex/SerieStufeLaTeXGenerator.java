/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.Messages;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.MatheAGGeneratorUtils;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.contentprovider.AufgsItemTableContentProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.BlankTableRowLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.ITableRowLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.latex.AufgSammlItemTableCellLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.latex.LaTeXQuelleTableLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.latex.LaTeXSerienStufeTableLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.latex.QuellenitemsTableCellLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.parts.TextTable;

/**
 * Generiert ein LaTeX-File für die Aufgaben einer oder mehrerer Stufen. Bei nur einer Stufe enthält dieses einen
 * Aufgabenteil mit den Aufgaben der Stufe und einen abschließenden Quellenteil. Bei mehr als einer Stufe enthält das
 * File einen mehrteiligen Aufgabenteil und einen einfachen Quellenteil am Ende.<br>
 * <br>
 * Schriftgröße des Aufgaben- und Quellenteils und Zeilenende des Aufgabenteils können mittels Konstruktor gesetzt
 * werden. Falls sie nicht gesetzt werden, werden Standardannahmen getroffen. Dies ist für die Schriftgröße \\large, für
 * das Zeilenende ein vertikaler Abstand von 2cm. Die Schriftgröße ist jeweils für Aufgaben- und Quellenteil gleich.
 *
 * @author Winkelv
 */
public class SerieStufeLaTeXGenerator extends AbstractSerieLaTeXGenerator {

	/** */
	private final Logger log = LoggerFactory.getLogger(SerieStufeLaTeXGenerator.class);

	/** */
	private int[] stufen;

	/** */
	private TextTable[] aufgabenTextTables;

	/** */
	private TextTable quellenTextTable;

	/** */
	private boolean ausgabeLoesungen = false;

	/**
	 * @param pSerie {@link Serie} die Serie
	 * @param pStufen int[] die Stufen (alle zwischen 1 und 6) Mehr als eine Stufe wird verwendet, um Sammelteile zu
	 * erzeugen (z.B. gesamtes Aufgabenblatt)
	 * @param pFontSizeConfiguration {@link LaTeXFontSizeConfiguration} die Konfiguration für die Schriftgröße oder
	 * null. Falls null, dann wird die Standardkonfiguration verwendet
	 * @param pAufgabenabstandConfiguration
	 */
	public SerieStufeLaTeXGenerator() {
		super();
	}

	/**
	 * @param pSerie
	 * @param pStufen
	 */
	private void initTableParts() {
		// Aufgabenteil(e)
		aufgabenTextTables = new TextTable[stufen.length];

		final ITableRowLabelProvider rowLabelProvider = new BlankTableRowLabelProvider();

		AufgsItemTableContentProvider tableContentProvider;

		for (int i = 0; i < stufen.length; i++) {

			aufgabenTextTables[i] = new TextTable(1);
			aufgabenTextTables[i].setHasHeader(false);

			String fontSize = Messages.getString("font_" + stufen[i]);

			final LaTeXSerienStufeTableLabelProvider aufgabeTableLabelProvider = new LaTeXSerienStufeTableLabelProvider(fontSize);
			aufgabeTableLabelProvider.setStufe(stufen[i]);
			aufgabenTextTables[i].setTableLabelProvider(aufgabeTableLabelProvider);


			aufgabenTextTables[i].setTableRowLabelProvider(rowLabelProvider);
			AufgSammlItemTableCellLabelProvider cellLabelProvider = new AufgSammlItemTableCellLabelProvider(Messages.getString("abstand_" + stufen[i]));
			cellLabelProvider.setAnzeigeLoesungen(ausgabeLoesungen);
			aufgabenTextTables[i].setCellLabelProvider(cellLabelProvider);

			int[] argStufen = new int[] { stufen[i] };
			tableContentProvider = new AufgsItemTableContentProvider(getSerie(), argStufen);
			aufgabenTextTables[i].setTableContentProvider(tableContentProvider);
		}

		tableContentProvider = new AufgsItemTableContentProvider(getSerie(), stufen);
		// Quellenteil
		quellenTextTable = new TextTable(1);
		quellenTextTable.setHasHeader(false);
		final LaTeXQuelleTableLabelProvider quelleTableLabelProvider = new LaTeXQuelleTableLabelProvider(Messages.getString("font_quellen"));
		quellenTextTable.setTableLabelProvider(quelleTableLabelProvider);
		quellenTextTable.setCellLabelProvider(new QuellenitemsTableCellLabelProvider(Messages.getString("abstand_quellen")));
		quellenTextTable.setTableContentProvider(tableContentProvider);
		quellenTextTable.setTableRowLabelProvider(rowLabelProvider);
	}

	/**
	 *
	 */
	private void checkAndInitStufen(int[] pStufen) {
		if (pStufen.length == 0) {
			String msg = "Initialisierungsfehler: Es muss mindestens eine Stufe gesetzt sein."; //$NON-NLS-1$
			log.error(msg);
			throw new MatheJungAltException(msg);
		}
		for (int i = 0; i < pStufen.length; i++) {
			if (pStufen[i] < 0 || pStufen[i] > 6) {
				String msg = "Index " + i + ": unzul\u00E4ssige Stufe " + pStufen[i] + " nur 1,...,6 zul\u00E4ssig."; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				log.error(msg);
				throw new MatheJungAltException(msg);
			}
		}
		stufen = pStufen;
		Arrays.sort(stufen);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathe.core.serviceimpl.generatoren.latex.ILaTeXGenerator#bodyContents()
	 */
	@Override
	public String bodyContents(IStammdatenservice stammdatenservice) throws MatheJungAltException {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < aufgabenTextTables.length; i++) {
			sb.append(aufgabenTextTables[i].render(stammdatenservice));
		}
		if (!isAusgabeLoesungen()) {
			sb.append(quellenTextTable.render(stammdatenservice));
		}
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathe.core.serviceimpl.generatoren.IFilegenerator#getFilename()
	 */
	@Override
	public String getFilename() {
		String filename = MatheAGGeneratorUtils.filenamePrefix(getSerie());
		switch (stufen.length) {
		case 1:
			filename += stufen[0];
			filename += Messages.getString("SerieStufeLaTeXGenerator_fileextension_tex"); //$NON-NLS-1$
			break;
		case 6:
			if (ausgabeLoesungen)
				filename += Messages.getString("SerieStufeLaTeXGenerator_loesungen_tex"); //$NON-NLS-1$
			else
				filename += Messages.getString("SerieStufeLaTeXGenerator_aufgaben_tex"); //$NON-NLS-1$
			break;
		default:
			filename += Messages.getString("SerieStufeLaTeXGenerator_aufgaben_einzeln_tex"); //$NON-NLS-1$
			break;
		}
		return filename;
	}

	/**
	 * @param pStufen the stufen to set
	 */
	public void setStufen(int[] pStufen) {
		checkAndInitStufen(pStufen);
		initTableParts();
	}

	/**
	 * @param pAusgabeLoesungen the ausgabeLoesungen to set
	 */
	public void setAusgabeLoesungen(boolean pAusgabeLoesungen) {
		ausgabeLoesungen = pAusgabeLoesungen;
	}

	/**
	 * @return the ausgabeLoesungen
	 */
	public boolean isAusgabeLoesungen() {
		return ausgabeLoesungen;
	}
}
