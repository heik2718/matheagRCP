/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.Messages;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils.ILaTeXDeclarationHeaderGenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils.LaTeXHeadingsDeclarationGenerator;

/**
 * @author aheike
 */
public abstract class AbstractLaTeXGenerator implements ILaTeXGenerator {

	/** */
	private ILaTeXDeclarationHeaderGenerator headerGenerator;

	/** */
	private IStammdatenservice stammdatenservice;

	/** */
	private String fontsize;

    /** */
	private String aufgabenabstand;

	/** */
	private boolean initialized;

	/** */
	private boolean pagestyleEmpty = false;

	/**
	 *
	 */
	public AbstractLaTeXGenerator() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @throws MatheJungAltException
	 */
	protected void checkInitialized() throws MatheJungAltException {
		if (fontsize == null) {
			fontsize = Messages.getString("font_6");
		}
		if (headerGenerator == null) {
			headerGenerator = new LaTeXHeadingsDeclarationGenerator();
		}
		if (aufgabenabstand == null){
			aufgabenabstand = Messages.getString("abstand_6");
		}
	}

	/**
	 * @return the headerGenerator
	 */
	public ILaTeXDeclarationHeaderGenerator getHeaderGenerator() {
		return headerGenerator;
	}

	/**
	 * @param pHeaderGenerator the headerGenerator to set
	 */
	public void setHeaderGenerator(ILaTeXDeclarationHeaderGenerator pHeaderGenerator) {
		headerGenerator = pHeaderGenerator;
	}

	/**
	 * @return the stammdatenservice
	 */
	public IStammdatenservice getStammdatenservice() {
		return stammdatenservice;
	}

	/**
	 * @param pStammdatenservice the stammdatenservice to set
	 */
	public void setStammdatenservice(IStammdatenservice pStammdatenservice) {
		stammdatenservice = pStammdatenservice;
	}

	/**
	 * @return the initialized
	 */
	public boolean isInitialized() {
		return initialized;
	}

	/**
	 * @param pInitialized the initialized to set
	 */
	public void setInitialized(boolean pInitialized) {
		initialized = pInitialized;
	}

	/**
	 * @param pPagestyleEmpty the pagestyleEmpty to set
	 */
	public void setPagestyleEmpty(boolean pPagestyleEmpty) {
		pagestyleEmpty = pPagestyleEmpty;
	}

	/**
	 * @return the pagestyleEmpty
	 */
	public boolean isPagestyleEmpty() {
		return pagestyleEmpty;
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator#getEncoding()
	 */
	@Override
	public String getEncoding() {
		return IFilegenerator.ENCODING_UTF;
	}

	/**
	 * @return the aufgabenabstand
	 */
	public String getAufgabenabstand() {
		return aufgabenabstand;
	}

	/**
	 * @param aufgabenabstand the aufgabenabstand to set
	 */
	public void setAufgabenabstand(String aufgabenabstand) {
		this.aufgabenabstand = aufgabenabstand;
	}

	/**
	 * @return the fontsize
	 */
	public String getFontsize() {
		return fontsize;
	}

	/**
	 * @param fontsize the fontsize to set
	 */
	public void setFontsize(String fontsize) {
		this.fontsize = fontsize;
	}

}
