/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl;

import org.eclipse.core.runtime.Assert;

/**
 * @author heike
 */
public class Verzeichnisrechner {

	/**
   *
   */
	public Verzeichnisrechner() {

	}

	/**
	 * Berechnet anhand des Schlüssels das relativ Verzeichnis für diese Aufgabe.
	 *
	 * @param pSchluessel
	 * @return String 3stelliges Verzeichnis immer eins für 1000 Dateien
	 */
	public String determineVerzeichnisZuSchluessel(String pSchluessel) {
		Assert.isNotNull(pSchluessel);
		Integer schluessel = Integer.valueOf(pSchluessel);
		int verzeichnisNumber = schluessel / 1000 + 1;
		String verzeichnis = "" + verzeichnisNumber;
		while (verzeichnis.length() < 3) {
			verzeichnis = "0" + verzeichnis;
		}
		return verzeichnis;
	}
}
