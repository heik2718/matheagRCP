/**
 * 
 */
package de.egladil.mathejungalt.service.stammdaten.impl;

import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.domain.mitglieder.Serienteilnahme;
import de.egladil.mathejungalt.domain.types.MitgliedSerieComposedKey;

/**
 * @author Heike Winkelvoss (www.egladil.de)
 */
public class SerienteilnahmeFinder {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(SerienteilnahmeFinder.class);

	/**
	 * 
	 */
	public SerienteilnahmeFinder() {
		//
	}

	/**
	 * Sucht die Serienteilnahme zu diesem Mitglied.
	 * 
	 * @param pSerie
	 * @param pMitglied
	 * @return {@link Serienteilnahme} oder null
	 */
	public Serienteilnahme findTeilnahme(Serie pSerie, Mitglied pMitglied) {
		LOG.debug("Start findTeilnahme()");
		Iterator<Serienteilnahme> iterator = pMitglied.serienteilnahmenIterator();
		while (iterator.hasNext()) {
			Serienteilnahme item = iterator.next();
			if (new MitgliedSerieComposedKey(pMitglied, pSerie).equals(item.getKey())) {
				return item;
			}
		}
		return null;
	}
}
