/**
 * 
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils;

import de.egladil.mathejungalt.service.stammdaten.impl.Messages;

/**
 * Generiert die LaTeX-Deklaration mit headings
 * 
 * @author Winkelv
 */
public class LaTeXHeadingsDeclarationGenerator extends LaTeXDeclarationGenerator {

	/**
	 * 
	 */
	public LaTeXHeadingsDeclarationGenerator() {
	}

	/**
	 * @param pGenerator
	 * @param sb
	 */
	protected String hookLeftHeadings() {
		return Messages.getString("LaTeXHeadingsDeclarationGenerator.leftsideheading"); //$NON-NLS-1$
	}
}
