/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.Messages;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.contentprovider.AufgsItemTableContentProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.BlankTableRowLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.latex.LaTeXQuelleTableLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.latex.QuellenitemsTableCellLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils.ILaTeXDeclarationHeaderGenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils.ILaTeXDeclarationHeaderGenerator.LaTeXSeitenkonfiguration;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils.LaTeXEmptyHeadingsDeclarationGenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils.LaTeXFactory;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.parts.TextTable;

/**
 * @author aheike
 */
public class ArbeitsblattQuellenGenerator extends AbstractLaTeXGenerator {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(ArbeitsblattQuellenGenerator.class);

	/** */
	private Arbeitsblatt arbeitsblatt;

	/** */
	private TextTable textTable;

	/** */
	private final ILaTeXDeclarationHeaderGenerator headerGenerator = new LaTeXEmptyHeadingsDeclarationGenerator();

	/**
	 *
	 */
	public ArbeitsblattQuellenGenerator() {
		super();
		setPagestyleEmpty(true);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.AbstractLaTeXGenerator#checkInitialized()
	 */
	@Override
	protected void checkInitialized() throws MatheJungAltException {
		if (arbeitsblatt == null) {
			String msg = "Initialisierungsfehler: erst setArbeitsblatt() aufrufen."; //$NON-NLS-1$
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		super.checkInitialized();
	}

	/**
	 * @param pSerie
	 * @param pStufen
	 */
	private void initTableParts() {
		textTable = new TextTable(1);
		textTable.setHasHeader(false);
		final LaTeXQuelleTableLabelProvider quelleTableLabelProvider = new LaTeXQuelleTableLabelProvider(Messages.getString("font_quellen"));
		textTable.setTableLabelProvider(quelleTableLabelProvider);
		textTable.setCellLabelProvider(new QuellenitemsTableCellLabelProvider("\\n"));

		textTable.setTableContentProvider(new AufgsItemTableContentProvider(arbeitsblatt));
		textTable.setTableRowLabelProvider(new BlankTableRowLabelProvider());
		setInitialized(true);
		setHeaderGenerator(new LaTeXEmptyHeadingsDeclarationGenerator());
		setPagestyleEmpty(true);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.ILaTeXGenerator#bodyContents()
	 */
	@Override
	public String bodyContents(IStammdatenservice stammdatenservice) throws MatheJungAltException {
		StringBuffer sb = new StringBuffer();
		sb.append(textTable.render(stammdatenservice));
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator#generate()
	 */
	@Override
	public String generate(IStammdatenservice stammdatenservice) throws MatheJungAltException {
		checkInitialized();
		initTableParts();
		StringBuffer sb = new StringBuffer();
		sb.append(headerGenerator.generateDeclaration(LaTeXSeitenkonfiguration.TWOSIDE,
			Messages.getString("vendor_url_latex")));
		sb.append(bodyContents(stammdatenservice));
		sb.append("\n");
		sb.append(LaTeXFactory.endDocument());
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator#getFilename()
	 */
	@Override
	public String getFilename() {
		return LaTeXFactory.toFilename(arbeitsblatt.getTitel()) + "_quellen.tex";
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.AbstractLaTeXGenerator#getHeaderGenerator()
	 */
	@Override
	public ILaTeXDeclarationHeaderGenerator getHeaderGenerator() {
		return headerGenerator;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.AbstractLaTeXGenerator#setHeaderGenerator(de
	 * .egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils.ILaTeXDeclarationHeaderGenerator)
	 */
	@Override
	public void setHeaderGenerator(ILaTeXDeclarationHeaderGenerator pHeaderGenerator) {
		//
	}

	/**
	 * @param pArbeitsblatt the arbeitsblatt to set
	 */
	public void setArbeitsblatt(Arbeitsblatt pArbeitsblatt) {
		arbeitsblatt = pArbeitsblatt;
	}

}
