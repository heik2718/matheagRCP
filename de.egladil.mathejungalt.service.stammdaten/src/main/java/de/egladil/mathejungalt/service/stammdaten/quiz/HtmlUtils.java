/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.service.stammdaten.quiz;

import org.apache.commons.lang.StringUtils;

/**
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public abstract class HtmlUtils {

	/**
	 * @param inputString
	 * @return String
	 */
	public static String convertToHtmlEntities(String inputString) {
		String result = inputString;
		result = StringUtils.replace(result, "ä", "&auml;");
		result = StringUtils.replace(result, "Ä", "&Auml;");
		result = StringUtils.replace(result, "ö", "&ouml;");
		result = StringUtils.replace(result, "Ö", "&Ouml;");
		result = StringUtils.replace(result, "ü", "&uuml;");
		result = StringUtils.replace(result, "Ü", "&Uuml;");
		result = StringUtils.replace(result, "ß", "&szlig;");
		return result;
	}

}
