/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.comparators;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.DomainObjectProxyClassifier;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;

/**
 * Mitglieder werden nach Vorname, Nachname und Alter sortiert
 *
 * @author Winkelv
 */
public class MitgliedNameComparator extends AbstractMatheAGObjectComparator {

	/**
	 *
	 */
	public MitgliedNameComparator() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.serviceimpl.internal.comparators.AbstractMatheAGObjectComparator#specialCheckType(de.egladil
	 * .mathe. core.domain.AbstractMatheAGObject, de.egladil.mathe.core.domain.AbstractMatheAGObject)
	 */
	@Override
	protected void specialCheckType(AbstractMatheAGObject pO1, AbstractMatheAGObject pO2) {
		if (!DomainObjectProxyClassifier.isMitglied(pO1) && !DomainObjectProxyClassifier.isMitglied(pO2))
			throw new MatheJungAltException("Falscher Type: keine " + Mitglied.class.getName());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.serviceimpl.internal.comparators.AbstractMatheAGObjectComparator#specialCompare(de.egladil
	 * .mathe.core .domain.AbstractMatheAGObject, de.egladil.mathe.core.domain.AbstractMatheAGObject)
	 */
	@Override
	protected int specialCompare(AbstractMatheAGObject pO1, AbstractMatheAGObject pO2) {
		Mitglied m1 = (Mitglied) pO1;
		Mitglied m2 = (Mitglied) pO2;
		if (m1.getVorname().equals(m2.getVorname())) {
			String nachname1 = m1.getNachname() == null ? "" : m1.getNachname();
			String nachname2 = m2.getNachname() == null ? "" : m2.getNachname();
			if (nachname1.length() > 0 && nachname2.length() > 0)
				return nachname1.compareTo(nachname2);
			if (nachname1.length() == 0 && nachname2.length() == 0) {
				return m1.getAlter().intValue() - m2.getAlter().intValue();
			}
			return nachname1.length() - nachname2.length();
		}
		return m1.getVorname().compareTo(m2.getVorname());
	}
}
