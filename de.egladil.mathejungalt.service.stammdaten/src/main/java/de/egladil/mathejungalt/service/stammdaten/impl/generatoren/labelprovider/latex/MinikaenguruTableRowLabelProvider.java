/**
 * 
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.latex;

import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.ITableRowLabelProvider;

/**
 * @author aheike
 */
public class MinikaenguruTableRowLabelProvider implements ITableRowLabelProvider {

	/** */
	private final MinikaenguruAnkreuztabelleProvider ankreuztabelleProvider = new MinikaenguruAnkreuztabelleProvider();

	/**
	 * 
	 */
	public MinikaenguruTableRowLabelProvider() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.ITableRowLabelProvider#getEndText()
	 */
	@Override
	public String getEndText() {
		StringBuffer sb = new StringBuffer();
		sb.append(ankreuztabelleProvider.getText());
		sb.append("%==============================================================================");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.ITableRowLabelProvider#getStartText()
	 */
	@Override
	public String getStartText() {
		return "\n";
	}
}
