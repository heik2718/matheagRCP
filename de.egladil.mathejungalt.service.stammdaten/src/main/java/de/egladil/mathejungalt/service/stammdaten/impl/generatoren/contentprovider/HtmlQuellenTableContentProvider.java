/**
 * 
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.contentprovider;

import java.util.ArrayList;
import java.util.List;

import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * @author winkelv
 */
public class HtmlQuellenTableContentProvider extends TableContentProvider {

	/** */
	private List<Object> content;

	/**
	 * 
	 */
	public HtmlQuellenTableContentProvider(Serie pSerie) {
		super();
		content = new ArrayList<Object>();
		content.add(pSerie);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.serviceimpl.generatoren.html.contentprovider.TableContentProvider#getContent()
	 */
	@Override
	public List<Object> getContent(IStammdatenservice stammdatenservice) {
		return content;
	}
}
