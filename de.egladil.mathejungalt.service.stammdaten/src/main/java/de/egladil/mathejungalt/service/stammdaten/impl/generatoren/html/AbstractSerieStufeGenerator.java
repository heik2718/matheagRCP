//=====================================================
// Projekt: de.egladil.mathejungalt.service.stammdaten
// (c) Heike Winkelvoß
//=====================================================
// Dateiname: AbstractSerieStufeGenerator.java                            $
// $Revision:: 1                                     $
// $Modtime:: 29.03.2015                                $
//=====================================================

package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.EgladilServerException;
import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serienitem;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.Messages;
import de.egladil.mathejungalt.service.stammdaten.impl.comparators.AufgabensItemNummerComparator;

/**
 * AbstractSerieStufeGenerator
 */
public abstract class AbstractSerieStufeGenerator {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractSerieStufeGenerator.class);

	private static final String UNTERTITEL_PLACEHOLDER = "PLATZHALTER_UNTERTITEL";

	private static final String AUFG_PLACEHOLDER = "AUFGABEN_UL";

	/**
	 * Generiert eine Linkliste zu den einzelnen Stufenvorschauen. TODO
	 *
	 * @param stammdatenservice
	 * @return
	 */
	public String generate(IStammdatenservice stammdatenservice, Serie serie, int stufe) {
		if (!serie.isItemsLoaded()) {
			stammdatenservice.aufgabensammlungitemsLaden(serie);
		}
		if (stufe < 1 || stufe > 6) {
			throw new EgladilServerException("stufe muss zwischen 1 und 6 liegen.");
		}
		List<Serienitem> items = serie.getSerienitemsZuStufe(stufe);
		Collections.sort(items, new AufgabensItemNummerComparator());
		InputStream in = null;
		try {
			in = this.getClass().getResourceAsStream(getTemplatePath());
			StringWriter sw = new StringWriter();
			IOUtils.copy(in, sw);
			String content = sw.toString();
			content = content.replaceAll(UNTERTITEL_PLACEHOLDER, getReplacementForUntertitel(serie, stufe));
			content = content.replaceAll(AUFG_PLACEHOLDER, getReplacementForAufgabenPart(items, stufe));
			content = weitereTransformation(stufe, items, content);
			content = HtmlFactory.setAktuellesDatum(content);
			return content;
		} catch (IOException e) {
			LOG.error(e.getMessage());
			throw new MatheJungAltException("Fehler beim Laden des Templates '" + getTemplatePath()
				+ "': mal classpath prüfen!");

		} finally {
			IOUtils.closeQuietly(in);
		}
	}

	abstract String getTemplatePath();

	private String getReplacementForUntertitel(Serie serie, int stufe) {
		String result = "Serie " + serie.getNummer();
		switch (stufe) {
		case 1:
			result = result + " - Vorschule";
			break;
		case 2:
			result = result + " - Klassen 1 und 2";
			break;
		case 3:
			result = result + " - Klassen 3 und 4";
			break;
		case 4:
			result = result + " - Klassen 5 und 6";
			break;
		case 5:
			result = result + " - Klassen 7 und 8";
			break;
		case 6:
			result = result + " - ab Klasse 9";
			break;
		default:
			break;
		}
		return result;

	}

	private String getReplacementForAufgabenPart(List<Serienitem> items, int stufe) {
		StringBuffer sb = new StringBuffer();
		sb.append(Messages.getString("SerieStufe_generator_1"));

		for (int i = 0; i < items.size(); i++) {
			Serienitem item = items.get(i);
			sb.append(getItemPartAufgabe(item));
		}

		sb.append("</ul>");
		return sb.toString();
	}

	private Object getItemPartAufgabe(IAufgabensammlungItem item) {
		StringBuffer sb = new StringBuffer();
		sb.append("<li><img src=\"../pngdir/");
		sb.append(item.getNummer());
		sb.append(".png\" alt=\"Aufgabentext\"></li>");
		return sb.toString();
	}

	/**
	 * Hook, um weiteren Part zu ersetzen. Default gibt den content einfach wieder zurück. TODO
	 *
	 * @param stufe
	 * @param items
	 * @param content
	 * @return
	 */
	protected String weitereTransformation(int stufe, List<Serienitem> items, String content) {
		return content;
	}

}
