/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.DomainObjectProxyClassifier;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.medien.Autor;
import de.egladil.mathejungalt.domain.medien.Buch;
import de.egladil.mathejungalt.domain.quellen.AbstractQuelle;
import de.egladil.mathejungalt.domain.quellen.Buchquelle;
import de.egladil.mathejungalt.domain.quellen.Kindquelle;
import de.egladil.mathejungalt.domain.quellen.MitgliederQuelleItem;
import de.egladil.mathejungalt.domain.quellen.MitgliedergruppenQuelle;
import de.egladil.mathejungalt.domain.quellen.Zeitschriftquelle;
import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenart;
import de.egladil.mathejungalt.service.stammdaten.impl.Messages;

/**
 * @author Winkelv
 */
public final class LaTeXFactory {

	/** */
	private final static Logger LOG = LoggerFactory.getLogger(LaTeXFactory.class);

	/** */
	private static final String SPACE = " "; //$NON-NLS-1$

	/** */
	private static final String KOMMA_SPACE = ", "; //$NON-NLS-1$

	/** */
	private static final String COLON_SPACE = ": "; //$NON-NLS-1$

	/**
	 *
	 */
	private LaTeXFactory() {

	}

	/**
	 * Ausgabe der Quelle für die LaTeX-Dateien
	 *
	 * @param pAufgabe
	 * @return
	 */
	public static final String quellenangabe(Aufgabe pAufgabe) {
		if (pAufgabe == null) {
			String msg = MatheJungAltException.argumentNullMessage("Aufgabe"); //$NON-NLS-1$
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (!(pAufgabe.getArt().equals(Aufgabenart.Z))) {
			return Messages.getString("vendor_utf8"); //$NON-NLS-1$
		}
		final AbstractQuelle aufgabenquelle = pAufgabe.getQuelle();
		if (DomainObjectProxyClassifier.isBuchquelle(aufgabenquelle)) {
			Buchquelle quelle = (Buchquelle) pAufgabe.getQuelle();
			StringBuffer sb = new StringBuffer();
			if (quelle.getBuch().anzahlAutoren() > 0)
				sb.append(autorenangabe(quelle.getBuch()));
			sb.append(quelle.getBuch().getTitel());
			sb.append(", S."); //$NON-NLS-1$
			sb.append(quelle.getSeite());
			return sb.toString();
		}
		if (DomainObjectProxyClassifier.isZeitschriftquelle(aufgabenquelle)) {
			Zeitschriftquelle quelle = (Zeitschriftquelle) pAufgabe.getQuelle();
			StringBuffer sb = new StringBuffer();
			sb.append(quelle.getZeitschrift().getTitel());
			sb.append(Messages.getString("LaTeXFactory_zeischrift_ausgabe_open")); //$NON-NLS-1$
			sb.append(quelle.getAusgabe());
			sb.append(")}"); //$NON-NLS-1$
			sb.append(quelle.getJahrgang());
			return sb.toString();
		}
		if (DomainObjectProxyClassifier.isKindquelle(aufgabenquelle)) {
			Kindquelle quelle = (Kindquelle) pAufgabe.getQuelle();
			StringBuffer sb = new StringBuffer();
			sb.append(quelle.getMitglied().getVorname());
			sb.append(SPACE);
			if (quelle.getMitglied().getNachname() != null) {
				sb.append(quelle.getMitglied().getNachname());
			}
			sb.append(KOMMA_SPACE);
			sb.append(quelle.getJahre());
			sb.append(Messages.getString("LaTeXFactory_jahre_klasse")); //$NON-NLS-1$
			sb.append(" ");
			sb.append(quelle.getKlasse());
			return sb.toString();
		}
		if (DomainObjectProxyClassifier.isGruppenquelle(aufgabenquelle)) {
			MitgliedergruppenQuelle quelle = (MitgliedergruppenQuelle) pAufgabe.getQuelle();
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < quelle.getItems().size() - 2; i++) {
				MitgliederQuelleItem item = quelle.getItems().get(i);
				sb.append(item.getMitglied().getVorname());
				sb.append(SPACE);
				sb.append(item.getMitglied().getNachname() != null ? item.getMitglied().getNachname() : ""); //$NON-NLS-1$
				sb.append(KOMMA_SPACE);
				sb.append(item.getJahre());
				sb.append(Messages.getString("LaTeXFactory_jahre_klasse")); //$NON-NLS-1$
				sb.append(item.getKlasse());
				sb.append(","); //$NON-NLS-1$
			}
			if (quelle.getItems().size() >= 1) {
				MitgliederQuelleItem item = quelle.getItems().get(quelle.getItems().size() - 1);
				sb.append(item.getMitglied().getVorname());
				sb.append(SPACE);
				sb.append(item.getMitglied().getNachname() != null ? item.getMitglied().getNachname() : ""); //$NON-NLS-1$
				sb.append(KOMMA_SPACE);
				sb.append(item.getJahre());
				sb.append(Messages.getString("LaTeXFactory_jahre_klasse")); //$NON-NLS-1$
				sb.append(item.getKlasse());
			}
			return sb.toString();
		}
		return Messages.getString("vendor_utf8"); //$NON-NLS-1$

	}

	/**
	 * Ausgabe der Autoren zu Buch.
	 *
	 * @param pBuch
	 * @return
	 */
	private static String autorenangabe(Buch pBuch) {
		List<Autor> autoren = pBuch.getAutoren();
		StringBuffer sb = new StringBuffer();
		if (autoren.size() == 1) {
			sb.append(autoren.get(0).getName());
			sb.append(COLON_SPACE);
			return sb.toString();
		}

		int max = autoren.size() - 1;
		for (int i = 0; i < max; i++) {
			sb.append(autoren.get(i).getName());
			sb.append(KOMMA_SPACE);
		}
		sb.append(autoren.get(max).getName());
		sb.append(COLON_SPACE);
		return sb.toString();
	}

	/**
	 * Ersetzt Umlaute in pString. Falls null �bergeben wird, wird der Parameter einfach wieder zur�ckgegeben.
	 *
	 * @param pString
	 * @return
	 */
	public final static String toFilename(final String pString) {
		if (pString == null)
			return pString;
		String erg = pString.toLowerCase();
		erg = StringUtils.replace(erg, "ä", "ae");
		erg = StringUtils.replaceChars(erg, "ö", "oe");
		erg = StringUtils.replaceChars(erg, "ü", "ue");
		erg = StringUtils.replaceChars(erg, "ß", "ss");
		erg = StringUtils.replaceChars(erg, " ", "_");
		return erg;
	}

	/**
	 * @return
	 */
	public static String endDocument() {
		return Messages.getString("LaTeXFactory_document_end"); //$NON-NLS-1$
	}
}
