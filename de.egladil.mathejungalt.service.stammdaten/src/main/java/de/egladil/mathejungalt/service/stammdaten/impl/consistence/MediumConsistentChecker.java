/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.consistence;

import org.apache.commons.lang.StringUtils;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.exceptions.MatheJungAltInconsistentEntityException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.DomainObjectProxyClassifier;
import de.egladil.mathejungalt.domain.medien.AbstractPrintmedium;
import de.egladil.mathejungalt.domain.medien.IAbstractPrintmediumNames;

/**
 * @author Heike Winkelvoss (www.egladil.de)
 */
public class MediumConsistentChecker implements IConsistencecheckProvider {

	/** */
	private final ConsistenceCheckDelegate consistenceCheckDelegate = new ConsistenceCheckDelegate();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.consistencecheck.IConsistencecheckProvider#canCheck(de.egladil.mathejungalt.domain
	 * .AbstractMatheAGObject)
	 */
	@Override
	public boolean canCheck(Object pObject) {
		return DomainObjectProxyClassifier.isBuch(pObject) || DomainObjectProxyClassifier.isZeitschrift(pObject);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.consistencecheck.IConsistencecheckProvider#checkConsistent(de.egladil.mathejungalt
	 * .domain.AbstractMatheAGObject)
	 */
	@Override
	public void checkConsistent(AbstractMatheAGObject pObject) throws MatheJungAltException {
		AbstractPrintmedium medium = (AbstractPrintmedium) pObject;
		final String msgPref = "AbstractPrintmedium: ";
		consistenceCheckDelegate.checkStringKey(medium.getKey(), IAbstractPrintmediumNames.LENGTH_SCHLUESSEL, msgPref);
		if (StringUtils.isEmpty(medium.getTitel()))
			throw new MatheJungAltInconsistentEntityException(msgPref + "der Titel darf nicht null oder leer sein.");
		if (medium.getTitel().length() > IAbstractPrintmediumNames.LENGTH_TITEL)
			throw new MatheJungAltInconsistentEntityException(msgPref + "der Titel darf nicht länger als "
				+ IAbstractPrintmediumNames.LENGTH_TITEL + " Zeichen sein");
	}
}
