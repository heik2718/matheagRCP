/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.contentprovider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import de.egladil.mathejungalt.config.IMatheJungAltConfiguration;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.comparators.SerienNummernComparator;

/**
 * @author Winkelv
 */
public class ArchivTableContentProvider extends TableContentProvider {

	/**
	 * @param pService
	 */
	public ArchivTableContentProvider() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.serviceimpl.generatoren.html.contentprovider.TableContentProvider#getContent()
	 */
	@Override
	public List<Object> getContent(IStammdatenservice pStammdatenservice) {
		Collection<Serie> alle = pStammdatenservice.getSerien();
		final int limitSerieRunde = limitSerieRunde(pStammdatenservice.getConfiguration());
		List<Serie> beendete = new ArrayList<Serie>();
		for (Serie serie : alle) {
			if (serie.getBeendet() == IAufgabensammlung.BEENDET && serie.getRunde() >= limitSerieRunde) {// serie.getNummer().intValue()
																											// > 17) {
				if (!serie.isItemsLoaded()) {
					pStammdatenservice.aufgabensammlungitemsLaden(serie);
				}
				beendete.add(serie);
			}
		}
		Collections.sort(beendete, new SerienNummernComparator());
		List<Object> objekte = new ArrayList<Object>();
		int max = beendete.size() - 1;
		// nur die ausgeben, die höher als limitSerieRunde sind in umgekehrter Reihenfolge
		for (int i = max; i >= 0; i--) {
			if (beendete.get(i).getNummer().intValue() >= limitSerieRunde) {
				objekte.add(beendete.get(i));
			}
		}
		return objekte;
	}

	/**
	 * Blödsinn ohne Generics umgangen für neuen Generator.
	 *
	 * @param pStammdatenservice
	 * @return
	 */
	public List<Serie> getSerien(IStammdatenservice pStammdatenservice) {
		Collection<Serie> alle = pStammdatenservice.getSerien();
		final int limitSerieRunde = limitSerieRunde(pStammdatenservice.getConfiguration());
		List<Serie> beendete = new ArrayList<Serie>();
		List<Serie> result = new ArrayList<Serie>();
		for (Serie serie : alle) {
			if (serie.getBeendet() == IAufgabensammlung.BEENDET && serie.getRunde() >= limitSerieRunde) {// serie.getNummer().intValue()
																											// > 17) {
				if (!serie.isItemsLoaded()) {
					pStammdatenservice.aufgabensammlungitemsLaden(serie);
				}
				beendete.add(serie);
			}
		}
		Collections.sort(beendete, new SerienNummernComparator());
		int max = beendete.size() - 1;
		// nur die ausgeben, die höher als limitSerieRunde sind in umgekehrter Reihenfolge
		for (int i = max; i >= 0; i--) {
			if (beendete.get(i).getNummer().intValue() >= limitSerieRunde) {
				result.add(beendete.get(i));
			}
		}
		return result;

	}

	/**
	 *
	 * @return
	 */
	private int limitSerieRunde(final IMatheJungAltConfiguration configuration) {
		int aktuelleRunde = Integer.valueOf(configuration.getConfigValue("serien.runden.aktuell"));
		int sperrzahl = Integer.valueOf(configuration.getConfigValue("serien.runden.wartedauer"));
		int minSperrend = aktuelleRunde - sperrzahl;
		return minSperrend;
	}
}
