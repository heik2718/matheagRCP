/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils;

import de.egladil.mathejungalt.service.stammdaten.impl.Messages;

/**
 * Generiert die LaTeX-Deklaration mit headings und pageStyle empty
 *
 * @author Winkelv
 */
public class LaTeXEmptyHeadingsDeclarationGenerator extends LaTeXDeclarationGenerator {

	/**
	 *
	 */
	public LaTeXEmptyHeadingsDeclarationGenerator() {
	}

	/**
	 * @param pGenerator
	 * @param sb
	 */
	protected String hookLeftHeadings() {
		String mark = null;
		if (mark == null)
			mark = Messages.getString("LaTeXEmptyHeadingsDeclarationGenerator_pagestyle_empty"); //$NON-NLS-1$
		return mark;
	}
}
