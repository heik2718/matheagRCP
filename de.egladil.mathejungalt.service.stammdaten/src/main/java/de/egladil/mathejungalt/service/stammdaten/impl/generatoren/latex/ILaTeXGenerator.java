package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator;

/**
 * @author Winkelv
 */
public interface ILaTeXGenerator extends IFilegenerator {

	/**
	 * @return String der Inhalt des LaTeX- Files.
	 * @throws MatheAGServiceRuntimeException falls etwas nicht generiert werden kann.
	 */
	public abstract String bodyContents(IStammdatenservice stammdatenservice) throws MatheJungAltException;
}