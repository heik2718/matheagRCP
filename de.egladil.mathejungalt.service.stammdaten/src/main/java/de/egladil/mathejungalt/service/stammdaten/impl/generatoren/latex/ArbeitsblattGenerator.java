/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.Messages;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.contentprovider.AufgsItemTableContentProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.BlankTableRowLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.ITableLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.ITableRowLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.latex.ArbeitsblattItemTableCellLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.latex.LaTeXArbeitsblattTableLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils.ILaTeXDeclarationHeaderGenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils.ILaTeXDeclarationHeaderGenerator.LaTeXSeitenkonfiguration;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils.LaTeXFactory;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils.LaTeXHeadingsDeclarationGenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.parts.TextTable;

/**
 * @author aheike
 */
public class ArbeitsblattGenerator extends AbstractLaTeXGenerator {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(ArbeitsblattGenerator.class);

	/** */
	private Arbeitsblatt arbeitsblatt;

	/** */
	private TextTable textTable;

	/** */
	private final ITableLabelProvider tableLabelProvider = new LaTeXArbeitsblattTableLabelProvider(Messages.getString("font_arbeitsblatt"));

	/** */
	private final ITableRowLabelProvider rowLabelProvider = new BlankTableRowLabelProvider();

	/** */
	private final ILaTeXDeclarationHeaderGenerator headerGenerator = new LaTeXHeadingsDeclarationGenerator();

	/** */
	private boolean ausgabeLoesungen = false;

	/**
	 *
	 */
	public ArbeitsblattGenerator() {
		super();
		setPagestyleEmpty(true);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.AbstractLaTeXGenerator#checkInitialized()
	 */
	@Override
	protected void checkInitialized() throws MatheJungAltException {
		if (arbeitsblatt == null) {
			String msg = "Initialisierungsfehler: erst setArbeitsblatt() aufrufen."; //$NON-NLS-1$
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		super.checkInitialized();
	}

	/**
	 * @param pSerie
	 * @param pStufen
	 */
	private void initTableParts() {
		AufgsItemTableContentProvider tableContentProvider;
		textTable = new TextTable(1);
		textTable.setHasHeader(false);

		textTable.setTableLabelProvider(tableLabelProvider);

		textTable.setTableRowLabelProvider(rowLabelProvider);
		ArbeitsblattItemTableCellLabelProvider cellLabelProvider = new ArbeitsblattItemTableCellLabelProvider(Messages.getString("abstand_arbeitsblatt"));
		cellLabelProvider.setAnzeigeLoesungen(ausgabeLoesungen);
		textTable.setCellLabelProvider(cellLabelProvider);

		tableContentProvider = new AufgsItemTableContentProvider(arbeitsblatt);
		textTable.setTableContentProvider(tableContentProvider);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.ILaTeXGenerator#bodyContents()
	 */
	@Override
	public String bodyContents(IStammdatenservice stammdatenservice) throws MatheJungAltException {
		StringBuffer sb = new StringBuffer();
		sb.append("{");
		sb.append(this.getFontsize());
		sb.append(textTable.render(stammdatenservice));
		sb.append("}");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator#generate()
	 */
	@Override
	public String generate(IStammdatenservice stammdatenservice) throws MatheJungAltException {
		checkInitialized();
		initTableParts();
		StringBuffer sb = new StringBuffer();
		sb.append(headerGenerator.generateDeclaration(LaTeXSeitenkonfiguration.TWOSIDE,
			Messages.getString("vendor_url_latex")));
		sb.append(bodyContents(stammdatenservice));
		sb.append("\n");
		sb.append(LaTeXFactory.endDocument());
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator#getFilename()
	 */
	@Override
	public String getFilename() {
		final String prefix = LaTeXFactory.toFilename(arbeitsblatt.getTitel());
		return ausgabeLoesungen ? prefix + "_loesungen.tex" : prefix + "_aufgaben.tex";
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.AbstractLaTeXGenerator#getHeaderGenerator()
	 */
	@Override
	public ILaTeXDeclarationHeaderGenerator getHeaderGenerator() {
		return headerGenerator;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.AbstractLaTeXGenerator#setHeaderGenerator(de
	 * .egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils.ILaTeXDeclarationHeaderGenerator)
	 */
	@Override
	public void setHeaderGenerator(ILaTeXDeclarationHeaderGenerator pHeaderGenerator) {
		//
	}

	/**
	 * @param pAusgabeLoesungen the ausgabeLoesungen to set
	 */
	public void setAusgabeLoesungen(boolean pAusgabeLoesungen) {
		ausgabeLoesungen = pAusgabeLoesungen;
	}

	/**
	 * @param pArbeitsblatt the arbeitsblatt to set
	 */
	public void setArbeitsblatt(Arbeitsblatt pArbeitsblatt) {
		arbeitsblatt = pArbeitsblatt;
	}

}
