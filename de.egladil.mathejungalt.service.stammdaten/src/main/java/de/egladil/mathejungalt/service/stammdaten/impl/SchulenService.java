/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.service.stammdaten.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.file.DirectoryCleaner;
import de.egladil.base.latex.ILaTeXService;
import de.egladil.base.latex.ILaTeXService.LaTeXInteractionModes;
import de.egladil.base.utils.OsgiServiceLocator;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;
import de.egladil.mathejungalt.domain.schulen.IKontaktNames;
import de.egladil.mathejungalt.domain.schulen.ISchuleNames;
import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.domain.schulen.Land;
import de.egladil.mathejungalt.domain.schulen.MinikaenguruTeilnahme;
import de.egladil.mathejungalt.domain.schulen.Schule;
import de.egladil.mathejungalt.domain.schulen.Schulkontakt;

/**
 * <p>
 * NOTE: THIS WIDGET AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class SchulenService extends AbstractStammdatenservice {

	private static final Logger LOG = LoggerFactory.getLogger(SchulenService.class);

	private Map<String, Schule> schulenmap = new HashMap<String, Schule>();

	private Map<String, Kontakt> kontakteMap = new HashMap<String, Kontakt>();

	private Map<String, Land> laenderMap = new HashMap<String, Land>();

	private int nextSchuleSchluesselInt = -1;

	private int nextKontaktSchluesselInt = -1;

	/**
	 * @param pPersistenceservice
	 */
	public SchulenService() {
		super();
	}

	/**
	 * @return
	 */
	protected Collection<Schule> getSchulen() {
		if (schulenmap.isEmpty()) {
			List<Schule> schulen = getPersistenceservice().getSchulenRepository().findAllSchulen();
			for (Schule s : schulen) {
				// schulenmap.put(s.getUuid(), s);
				schulenmap.put(s.getSchluessel(), s);
			}
		}
		return schulenmap.values();
	}

	protected List<Land> getLaender() {
		if (laenderMap.isEmpty()) {
			List<Land> laender = getPersistenceservice().getSchulenRepository().findAllLaender();
			for (Land l : laender) {
				laenderMap.put(l.getSchluessel(), l);
			}
		}
		List<Land> result = new ArrayList<Land>();
		for (Land l : laenderMap.values()) {
			result.add(l);
		}
		return result;

	}

	/**
	 * @return
	 */
	protected Collection<Kontakt> getKontakte() {
		if (kontakteMap.isEmpty()) {
			List<Kontakt> kontakte = getPersistenceservice().getKontakteRepository().findAllKontakte();
			for (Kontakt k : kontakte) {
				// kontakteMap.put(k.getUuid(), k);
				kontakteMap.put(k.getSchluessel(), k);
			}
		}
		return kontakteMap.values();
	}

	/**
	 * @return
	 */
	protected Schule schuleAnlegen() {
		Schule schule = new Schule();
		schule.setSchluessel(nextSchuleKey());
		schule.setMiniTeilnahmenLoaded(true);
		return schule;
	}

	/**
	 * @return
	 */
	protected Kontakt kontaktAnlegen() {
		Kontakt kontakt = new Kontakt();
		kontakt.setSchluessel(nextKontaktKey());
		kontakt.setSchulkontakteGeladen(true);
		return kontakt;
	}

	/**
	 * Sucht den maximalen fachlichen Schl�ssel.
	 *
	 * @return String
	 */
	private String nextSchuleKey() {
		if (nextSchuleSchluesselInt < 0) {
			nextSchuleSchluesselInt = Integer.valueOf(getPersistenceservice().getSchulenRepository().getMaxKey())
				.intValue() + 1;
		}
		String schl = "" + (nextSchuleSchluesselInt);
		if (schl.length() < ISchuleNames.LENGTH_SCHLUESSEL) {
			schl = StringUtils.leftPad(schl, ISchuleNames.LENGTH_SCHLUESSEL, '0');
		}
		nextSchuleSchluesselInt++;
		return schl;
	}

	/**
	 * Sucht den maximalen fachlichen Schl�ssel.
	 *
	 * @return String
	 */
	private String nextKontaktKey() {
		if (nextKontaktSchluesselInt < 0) {
			nextKontaktSchluesselInt = Integer.valueOf(getPersistenceservice().getKontakteRepository().getMaxKey())
				.intValue() + 1;
		}
		String schl = "" + (nextKontaktSchluesselInt);
		if (schl.length() < IKontaktNames.LENGTH_SCHLUESSEL) {
			schl = StringUtils.leftPad(schl, IKontaktNames.LENGTH_SCHLUESSEL, '0');
		}
		nextKontaktSchluesselInt++;
		return schl;
	}

	/**
	 * @param pSchule
	 * @param pMinikaenguru
	 * @return
	 */
	protected MinikaenguruTeilnahme schuleNimmtTeil(Schule pSchule, Minikaenguru pMinikaenguru) {
		if (pSchule == null || pMinikaenguru == null) {
			String msg = MatheJungAltException.argumentNullMessage("Schule oder Minikaenguru");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (miniteilnahmeFinden(pSchule, pMinikaenguru) != null) {
			return null;
		}
		MinikaenguruTeilnahme teilnahme = new MinikaenguruTeilnahme();
		teilnahme.setSchule(pSchule);
		teilnahme.setMinikaenguru(pMinikaenguru);
		pSchule.addMinikaenguruTeilnahme(teilnahme);
		return teilnahme;
	}

	/**
	 * @param pSchule
	 * @param pMinikaenguru
	 */
	protected void minikaenguruteilnahmeLoeschen(Schule pSchule, Minikaenguru pMinikaenguru) {
		if (pSchule == null || pMinikaenguru == null) {
			String msg = MatheJungAltException.argumentNullMessage("Schule oder Minikaenguru");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		MinikaenguruTeilnahme teilnahme = miniteilnahmeFinden(pSchule, pMinikaenguru);
		if (teilnahme == null) {
			return;
		}
		if (pSchule.removeMinikaenguruTeilnahme(teilnahme)) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Teilnahme an Minikaenguru " + pMinikaenguru.getJahr() + " von Schule " + pSchule.getName()
					+ " geloescht");
			}
		}

	}

	/**
	 * @param pSchule
	 * @param pMinikaenguru
	 * @return
	 */
	private MinikaenguruTeilnahme miniteilnahmeFinden(Schule pSchule, Minikaenguru pMinikaenguru) {
		if (!pSchule.isMiniTeilnahmenLoaded()) {
			getPersistenceservice().getSchulenRepository().loadMinikaenguruteilnahmen(pSchule);
		}
		for (MinikaenguruTeilnahme t : pSchule.getMinikaenguruTeilnahmen()) {
			if (t.getMinikaenguru().equals(pMinikaenguru)) {
				return t;
			}
		}
		return null;
	}

	/**
	 * @param pKontakt
	 * @param pSchule
	 * @return
	 */
	private Schulkontakt schulkontakteFinden(Kontakt pKontakt, Schule pSchule) {
		if (!pKontakt.isSchulkontakteGeladen()) {
			getPersistenceservice().getKontakteRepository().loadSchulkontakte(pKontakt);
		}
		for (Schulkontakt sk : pKontakt.getSchulkontakte()) {
			if (sk.getSchule().equals(pSchule)) {
				return sk;
			}
		}
		return null;
	}

	/**
	 * @param pSchule
	 * @return
	 */
	protected Collection<MinikaenguruTeilnahme> minikaenguruTeilnahmenLaden(Schule pSchule) {
		if (pSchule == null) {
			String msg = MatheJungAltException.argumentNullMessage("Schule");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (!pSchule.isMiniTeilnahmenLoaded()) {
			getPersistenceservice().getSchulenRepository().loadMinikaenguruteilnahmen(pSchule);
		}
		return pSchule.getMinikaenguruTeilnahmen();
	}

	/**
	 * @param pKontakt
	 * @param pSchule
	 */
	protected void schulkontaktLoeschen(Kontakt pKontakt, Schule pSchule) {
		if (pSchule == null || pSchule == null) {
			String msg = MatheJungAltException.argumentNullMessage("Schule oder Kontakt");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		Schulkontakt schulkontakt = schulkontakteFinden(pKontakt, pSchule);
		if (schulkontakt == null) {
			return;
		}
		if (pKontakt.removeSchulkontakt(schulkontakt)) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Kontakt " + pKontakt.getName() + " zu Schule " + pSchule.getName() + " geloescht");
			}
		}
	}

	/**
	 * @param pKontakt
	 * @param pSchule
	 * @return
	 */
	protected Schulkontakt kontaktZuSchuleHinzufuegen(Kontakt pKontakt, Schule pSchule) {
		if (pSchule == null || pKontakt == null) {
			String msg = MatheJungAltException.argumentNullMessage("Schule oder Kontakt");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (schulkontakteFinden(pKontakt, pSchule) != null) {
			return null;
		}
		Schulkontakt schulkontakt = new Schulkontakt();
		schulkontakt.setSchule(pSchule);
		schulkontakt.setKontakt(pKontakt);
		pKontakt.addSchulkontakt(schulkontakt);
		return schulkontakt;
	}

	/**
	 * @param pKontakt
	 * @return
	 */
	protected Collection<Schulkontakt> schulkontakteLaden(Kontakt pKontakt) {
		if (pKontakt == null) {
			String msg = MatheJungAltException.argumentNullMessage("Schule");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (!pKontakt.isSchulkontakteGeladen()) {
			getPersistenceservice().getKontakteRepository().loadSchulkontakte(pKontakt);
		}
		return pKontakt.getSchulkontakte();
	}

	/**
	 * @param pSchule
	 */
	protected void schuleSpeichern(Schule pSchule) {
		boolean neu = pSchule.isNew();
		getPersistenceservice().getSchulenRepository().saveSchule(pSchule);
		if (neu) {
			schulenmap.put(pSchule.getSchluessel(), pSchule);
			fireObjectAddedEvent(pSchule);
		}
	}

	/**
	 * @param pKontakt
	 */
	protected void kontaktSpeichern(Kontakt pKontakt) {
		boolean neu = pKontakt.isNew();
		getPersistenceservice().getKontakteRepository().saveKontakt(pKontakt);
		if (neu) {
			// kontakteMap.put(pKontakt.getUuid(), pKontakt);
			kontakteMap.put(pKontakt.getSchluessel(), pKontakt);
			fireObjectAddedEvent(pKontakt);
		}
	}

	/**
	 * @param pSchule
	 * @return
	 */
	protected List<Kontakt> findKontakteZuSchule(Schule pSchule) {
		Assert.isNotNull(pSchule, "pSchule");
		List<Kontakt> kontakte = getPersistenceservice().getKontakteRepository().findKontakteForSchule(pSchule);
		return kontakte;
	}

	/**
	 * @param pMinikaenguru
	 * @param pSchule
	 * @param pNeueKontakte
	 */
	protected void mailinglisteAktualisieren(MinikaenguruTeilnahme pTeilnahme, List<Kontakt> pNeueKontakte) {
		Assert.isNotNull(pTeilnahme, "pTeilnahme");
		Assert.isNotNull(pNeueKontakte, "pFilteredKontakte");
		List<Kontakt> vorhandeneKontakte = findKontakteZuSchule(pTeilnahme.getSchule());
		getPersistenceservice().getMinikaenguruRepository().mailinglisteAktualisieren(pTeilnahme.getMinikaenguru(),
			vorhandeneKontakte, pNeueKontakte);
	}

	/**
	 * @param teilnahme
	 * @param aufgabensammlungservice
	 * @return
	 */
	protected List<Kontakt> getKontakteMinikaenguru(MinikaenguruTeilnahme teilnahme,
		Aufgabensammlungservice aufgabensammlungservice) {
		Assert.isNotNull(teilnahme);
		List<Kontakt> alleKontakteSchule = findKontakteZuSchule(teilnahme.getSchule());
		Minikaenguru minikaenguru = teilnahme.getMinikaenguru();
		if (!minikaenguru.isItemsLoaded()) {
			aufgabensammlungservice.mailinglisteLaden(minikaenguru);
		}
		List<Kontakt> alleKontakte = minikaenguru.getMailingliste();

		List<Kontakt> result = new ArrayList<Kontakt>();
		for (Kontakt k : alleKontakteSchule) {
			if (alleKontakte.contains(k)) {
				result.add(k);
			}
		}
		return result;
	}

	/**
	 * Werfen temporäre Dateien weg.
	 *
	 * @param pPathWorkDir
	 */
	protected void clearTemporaryLaTeXFiles(String pPathWorkDir) {
		final List<String> suffixes = Arrays.asList(new String[] { "aux", "dvi", "log", "out" });
		final DirectoryCleaner cleaner = new DirectoryCleaner();
		cleaner.deleteFiles(pPathWorkDir, suffixes);
	}

	/**
	 * @param pLatexFilePath
	 * @return IStatus
	 */
	protected IStatus compile(String pLatexFilePath) {
		ILaTeXService latexService = new OsgiServiceLocator().getService(ILaTeXService.class);
		IStatus status = latexService.compile(pLatexFilePath, LaTeXInteractionModes.nonstopmode);
		return status;
	}

	/**
	 * Aufruf von pdflatex
	 *
	 * @param pLatexFilePath
	 * @return IStatus
	 */
	protected IStatus pdflatex(String pLatexFilePath) {
		ILaTeXService latexService = new OsgiServiceLocator().getService(ILaTeXService.class);
		IStatus status = latexService.compile(pLatexFilePath, LaTeXInteractionModes.nonstopmode);
		return status;
	}

	/**
	 * Wandelt dvi in ps um.
	 *
	 * @param pWorkDir
	 * @param dviFilePath
	 * @return IStatus
	 */
	protected IStatus dviPs(String dviFilePath) {
		ILaTeXService latexService = new OsgiServiceLocator().getService(ILaTeXService.class);
		IStatus status = latexService.dviPs(dviFilePath);
		return status;
	}
}
