package de.egladil.mathejungalt.service.stammdaten.impl.consistence;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;

/**
 * Extensioninterface welches die Konsistenzpruefung aller ConsistenceCheckProviders von Kompositionen aufruft.
 * 
 * @author sho
 */
public interface IConsistencecheckProvider {

	/** die ID setzt sich zusammen aus der Plugin-ID und der id des ExtensionPoints */
	String CONSISTENCECHECKROVIDER_EXTENSION_ID = "de.egladil.mathejungalt.service.consistencecheckprovider";

	String CONSISTENCECHECKROVIDER_CONFIGURATION_ID = "consistencecheckprovider";

	String CONSISTENCECHECKROVIDER_CONFIGURATION_CLASS_PARAM = "class";

	/**
	 * Diese Methode ruft den Konsistenzcheck fuer das {@link AbstractMatheAGObject} auf, das eine Komposition ist.
	 * 
	 * @param pObject {@link AbstractMatheAGObject}
	 * @throws MatheJungAltException
	 */
	void checkConsistent(AbstractMatheAGObject pObject) throws MatheJungAltException;

	/**
	 * Stellt fest, ob das gegebene {@link AbstractMatheAGObject} geprüft werden kann.
	 * 
	 * @param pObject {@link AbstractMatheAGObject}
	 * @return boolean
	 */
	boolean canCheck(Object pObject);
}
