/**
 * 
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.contentprovider;

import java.util.ArrayList;
import java.util.List;

import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * @author Winkelv
 */
public class TableContentProvider {

	/**
	 * @param pStammdatenservice
	 */
	public TableContentProvider() {
		super();
	}

	/**
	 * @return
	 */
	public List<Object> getContent(IStammdatenservice pStammdatenservice) {
		return new ArrayList<Object>();
	}
}
