/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.comparators;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.DomainObjectProxyClassifier;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;

/**
 * @author Winkelv
 */
public class AufgabenStufeSchluesselComparator extends AbstractMatheAGObjectComparator {

	/**
	 *
	 */
	public AufgabenStufeSchluesselComparator() {

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathe.core.serviceimpl.internal.comparators.AbstractMatheAGObjectComparator#specialCheckType(de.egladil
	 * .mathe. core.domain.AbstractMatheAGObject, de.egladil.mathe.core.domain.AbstractMatheAGObject)
	 */
	@Override
	protected void specialCheckType(AbstractMatheAGObject pO1, AbstractMatheAGObject pO2) {
		if (!DomainObjectProxyClassifier.isAufgabe(pO1) && !DomainObjectProxyClassifier.isAufgabe(pO2)) {
			throw new MatheJungAltException("Falscher Type: keine " + Aufgabe.class.getName());
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathe.core.serviceimpl.internal.comparators.AbstractMatheAGObjectComparator#specialCompare(de.egladil
	 * .mathe.core .domain.AbstractMatheAGObject, de.egladil.mathe.core.domain.AbstractMatheAGObject)
	 */
	@Override
	protected int specialCompare(AbstractMatheAGObject pO1, AbstractMatheAGObject pO2) {
		Aufgabe aufgabe1 = (Aufgabe) pO1;
		Aufgabe aufgabe2 = (Aufgabe) pO2;
		if (aufgabe1.getStufe().intValue() == aufgabe2.getStufe().intValue())
			return aufgabe1.getSchluessel().compareTo(aufgabe2.getSchluessel());

		return aufgabe1.getStufe().intValue() - aufgabe2.getStufe().intValue();
	}

}
