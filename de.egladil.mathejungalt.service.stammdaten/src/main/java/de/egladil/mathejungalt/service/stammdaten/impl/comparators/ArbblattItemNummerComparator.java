/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.comparators;

import java.util.Comparator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblattitem;

/**
 * @author Winkelv
 */
public class ArbblattItemNummerComparator implements Comparator<IAufgabensammlungItem> {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(ArbblattItemNummerComparator.class);

	/**
	 *
	 */
	public ArbblattItemNummerComparator() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(IAufgabensammlungItem pO1, IAufgabensammlungItem pO2) {
		if (pO1 == null || pO2 == null) {
			String str1 = pO1 == null ? "pO1=null, " : pO1.toString();
			String str2 = pO2 == null ? "pO2=null" : pO2.toString();
			String msg = "Einer der Parameter ist null: " + str1 + str2;
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (!(pO1 instanceof Arbeitsblattitem) || !(pO2 instanceof Arbeitsblattitem)) {
			String msg = "Einer der Parameter ist kein Arbeitsblattitem: " + pO1.getClass().getName() + ","
				+ pO2.getClass().getName();
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (pO1 == pO2) {
			return 0;
		}
		Arbeitsblattitem item1 = (Arbeitsblattitem) pO1;
		Arbeitsblattitem item2 = (Arbeitsblattitem) pO2;

		String nummer1 = item1.getNummer();
		String nummer2 = item2.getNummer();
		Integer lfd1 = null;
		Integer lfd2 = null;
		try {
			lfd1 = Integer.valueOf(nummer1.substring(1));
		} catch (NumberFormatException e) {
			//
		}
		try {
			lfd2 = Integer.valueOf(nummer2.substring(1));
		} catch (NumberFormatException e) {
			//
		}
		if (lfd1 == null || lfd2 == null) {
			// Nummern entsprechen nicht dem Schema Thema gefolgt von laufender Nummer, also alphabetisch nach Nummer
			// sortieren.
			return nummer1.compareToIgnoreCase(nummer2);
		}

		if (!item1.getAufgabe().getThema().equals(item2.getAufgabe().getThema())) {
			return item1.getAufgabe().getThema().compareTo(item2.getAufgabe().getThema());
		}
		return lfd1.compareTo(lfd2);
	}
}
