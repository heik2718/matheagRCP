/**
 * 
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils;

import de.egladil.mathejungalt.service.stammdaten.impl.Messages;

/**
 * Generiert die LaTeX-Deklaration. Diese geht bis zu \begin{document}. Standard ist keine Kopfzeile, und keine Ausgabe
 * des PageStyles
 * 
 * @author Winkelv
 */
public class LaTeXDeclarationGenerator implements ILaTeXDeclarationHeaderGenerator {

	/**
	 * 
	 */
	public LaTeXDeclarationGenerator() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.serviceimpl.generatoren.latex.utils.ILaTeXDeclarationHeaderGenerator#generateDeclaration
	 * (de.egladil .mathe.core.serviceimpl.generatoren.latex.ILaTeXGenerator)
	 */
	public String generateDeclaration(LaTeXSeitenkonfiguration pSeitenkonfiguration, String pRightSideHeader) {
		StringBuffer sb = new StringBuffer();
		String documentclass = null;
		switch (pSeitenkonfiguration) {
		case ONESIDE:
			if (documentclass == null)
				documentclass = Messages.getString("LaTeXDeclarationGenerator_oneside"); //$NON-NLS-1$
			break;
		default:
			if (documentclass == null)
				documentclass = Messages.getString("LaTeXDeclarationGenerator_twoside"); //$NON-NLS-1$
			break;
		}

		sb.append(documentclass);
		sb.append(Messages.getString("LaTeXDeclarationGenerator_include_open")); //$NON-NLS-1$
		sb.append(getIncludePath());
		sb.append(Messages.getString("LaTeXDeclarationGenerator_vorspann")); //$NON-NLS-1$
		sb.append(Messages.getString("LaTeXDeclarationGenerator_include_open")); //$NON-NLS-1$
		sb.append(getIncludePath());
		sb.append(Messages.getString("LaTeXDeclarationGenerator_commands")); //$NON-NLS-1$

		sb.append(hookLeftHeadings());
		sb.append(pRightSideHeader);
		sb.append("\n");
		sb.append(Messages.getString("LaTeXDeclarationGenerator_begin_document")); //$NON-NLS-1$

		sb.append(hookPageStyle());

		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.serviceimpl.generatoren.latex.utils.ILaTeXDeclarationHeaderGenerator#getIncludePath()
	 */
	@Override
	public String getIncludePath() {
		return Messages.getString("LaTeXDeclarationGenerator_include_path"); //$NON-NLS-1$
	}

	/**
	 * @return
	 */
	protected String hookPageStyle() {
		return Messages.getString("newline"); //$NON-NLS-1$
	}

	/**
	 * @param pGenerator
	 * @param sb
	 */
	protected String hookLeftHeadings() {
		return Messages.getString("newline"); //$NON-NLS-1$
	}
}
