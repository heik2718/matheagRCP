package de.egladil.mathejungalt.service.stammdaten.impl.generatoren;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * Alle Methoden, die von einem Filegenerator gebraucht werden.
 *
 * @author Heike Winkelvoß (www.egladil.de)
 */
public interface IFilegenerator {

	/** */
//	public static final String ENCODING_LATIN = "ISO-8859-1";

	/** */
	public static final String ENCODING_UTF = "UTF-8";

	/**
	 * Gibt den Namen des Ausgabefiles zurück.
	 *
	 * @return String
	 */
	abstract String getFilename();

	/**
	 * Erzeugt den Text, der wohin auch immer geschrieben werden soll.
	 *
	 * @return String
	 * @throws MatheAGServiceRuntimeException falls etwas schiefgeht.
	 */
	abstract String generate(IStammdatenservice stammdatenservice) throws MatheJungAltException;

	/**
	 * @return
	 */
	abstract String getEncoding();
}
