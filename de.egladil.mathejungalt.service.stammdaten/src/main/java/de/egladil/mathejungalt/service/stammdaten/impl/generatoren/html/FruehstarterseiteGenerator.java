//=====================================================
// Projekt: de.egladil.mathejungalt.service.stammdaten
// (c) Heike Winkelvoß
//=====================================================
// Dateiname: FruehstarterseiteGenerator.java                            $
// $Revision:: 1                                     $
// $Modtime:: 29.03.2015                                $
//=====================================================

package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.domain.mitglieder.Serienteilnahme;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.Messages;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.contentprovider.FruehContentProvider;

/**
 * FruehstarterseiteGenerator generiert die Frühstarterseite der aktuellen Runde.
 */
public class FruehstarterseiteGenerator {

	private static final Logger LOG = LoggerFactory.getLogger(FruehstarterseiteGenerator.class);

	private static final String HTML_TEMPLATE_PATH = "/html/fruestarter_template.txt";

	private static final String UNTERTITEL_PLACEHOLDER = "PLATZHALTER_UNTERTITEL";

	private static final String TABLE_PLACEHOLDER = "FRUEH_TABLE_REPLACE";

	private FruehContentProvider contentProvider = new FruehContentProvider();

	/**
	 * Erzeugt eine Instanz von FruehstarterseiteGeneratorNeu
	 */
	public FruehstarterseiteGenerator() {
	}

	public String generate(IStammdatenservice stammdatenservice) {
		List<Serie> serien = contentProvider.getSerieAktuelleRundeSorted(stammdatenservice);
		List<Mitglied> mitglieder = contentProvider.getMitgliederMitFruestartern(stammdatenservice);
		InputStream in = null;
		try {
			in = this.getClass().getResourceAsStream(HTML_TEMPLATE_PATH);
			StringWriter sw = new StringWriter();
			IOUtils.copy(in, sw);
			String content = sw.toString();
			content = content.replaceAll(UNTERTITEL_PLACEHOLDER, getReplacementForUntertitel(stammdatenservice));
			content = content.replaceAll(TABLE_PLACEHOLDER,
				getReplacementForTablePart(mitglieder, serien, stammdatenservice));
			content = HtmlFactory.setAktuellesDatum(content);
			return content;
		} catch (IOException e) {
			LOG.error(e.getMessage());
			throw new MatheJungAltException("Fehler beim Laden des Templates '" + HTML_TEMPLATE_PATH
				+ "': mal classpath prüfen!");

		} finally {
			IOUtils.closeQuietly(in);
		}
	}

	private String getReplacementForUntertitel(IStammdatenservice stammdatenservice) {
		return "Fr&uuml;hstarter Runde " + stammdatenservice.aktuelleRunde();
	}

	/**
	 *
	 * @param mitglieder
	 * @return
	 */
	private String getReplacementForTablePart(List<Mitglied> mitglieder, List<Serie> serien,
		IStammdatenservice stammdatenservice) {
		StringBuffer sb = new StringBuffer();
		sb.append(Messages.getString("FruestarterseiteGenerator_1"));
		sb.append(getTableHeaders(serien));
		if (!mitglieder.isEmpty()) {
			sb.append(getMitgliederRows(mitglieder, serien, stammdatenservice));
		}
		sb.append("</tbody></table>");
		return sb.toString();
	}

	private String getTableHeaders(List<Serie> serien) {
		StringBuffer sb = new StringBuffer();
		sb.append(Messages.getString("FruestarterseiteGenerator_2"));
		for (int i = 0; i < serien.size(); i++) {
			sb.append("<th>");
			sb.append(serien.get(i).getNummer());
			sb.append("</th>");
		}
		sb.append("</tr></thead><tbody>");
		return sb.toString();
	}

	private String getMitgliederRows(List<Mitglied> mitglieder, List<Serie> serien, IStammdatenservice stammdatenservice) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < mitglieder.size(); i++) {
			Mitglied mitglied = mitglieder.get(i);
			if (!mitglied.isSerienteilnahmenLoaded()) {
				stammdatenservice.serienteilnahmenZuMitgliedLaden(mitglied);
				sb.append(getMitgliedRow(mitglied, serien));
			}
			sb.append(getMitgliedRow(mitglied, serien));
		}
		return sb.toString();
	}

	/**
	 *
	 * @param mitglied
	 * @param serien
	 * @return
	 */
	private String getMitgliedRow(Mitglied mitglied, List<Serie> serien) {
		List<Serienteilnahme> serienteilnahmen = mitglied.getSerienteilnahmen();
		StringBuffer sb = new StringBuffer();
		sb.append("<tr><td>");
		sb.append(HtmlFactory.nameAlterKlasseText(mitglied, true));
		sb.append("</td>");

		for (int j = 0; j < serien.size(); j++) {
			Serienteilnahme teilnahme = findTeilnahmeZuSerie(serienteilnahmen, serien.get(j));
			if (teilnahme != null && teilnahme.getFruehstarter() != null
				&& teilnahme.getFruehstarter() > Double.valueOf(0.0)) {
				sb.append("<td>");
				sb.append(teilnahme.getFruehstarter().toString());
				sb.append("</td>");
			} else {
				sb.append("<td></td>");
			}
		}
		sb.append("</tr>");
		return sb.toString();
	}

	private Serienteilnahme findTeilnahmeZuSerie(List<Serienteilnahme> serienteilnahmen, Serie serie) {
		for (Serienteilnahme t : serienteilnahmen) {
			if (serie.equals(t.getSerie())) {
				return t;
			}
		}
		return null;
	}

	public String getFileName() {
		return Messages.getString("page_fruehstarter");
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param contentProvider neuer Wert der Membervariablen contentProvider
	 */
	public void setContentProvider(FruehContentProvider contentProvider) {
		this.contentProvider = contentProvider;
	}
}
