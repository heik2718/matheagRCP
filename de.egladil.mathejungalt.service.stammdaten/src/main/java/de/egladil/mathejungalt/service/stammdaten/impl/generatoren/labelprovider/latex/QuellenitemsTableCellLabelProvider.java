/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.latex;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.TableCellLabelProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.utils.LaTeXFactory;

/**
 * Zeichnet die Quellen-Zelle in einer "LaTeX"- Tabelle.
 *
 * @author winkelv
 */
public class QuellenitemsTableCellLabelProvider extends TableCellLabelProvider {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(QuellenitemsTableCellLabelProvider.class);

	/**
	 * default: Ausgabe der Aufgaben und Standardzeilenende
	 */
	public QuellenitemsTableCellLabelProvider(String aufgabenabstand) {
		super(aufgabenabstand);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathe.core.serviceimpl.generatoren.labelprovider.TableCellLabelProvider#getText(java.lang.Object,
	 * int, int)
	 */
	@Override
	public String getText(Object pObj, int pRow, int pColumn) {
		if (!(pObj instanceof IAufgabensammlungItem)) {
			String msg = "falscher Typ: kein " + IAufgabensammlungItem.class.getName() + ", sondern " + pObj == null ? "null"
				: pObj.getClass().getName();
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		IAufgabensammlungItem item = (IAufgabensammlungItem) pObj;
		StringBuffer sb = new StringBuffer();
		sb.append("\\quelle{Aufgabe ");
		sb.append(item.getNummer());
		sb.append("}{");
		sb.append(LaTeXFactory.quellenangabe(item.getAufgabe()));
		sb.append("}\n");
		return sb.toString();
	}

}
