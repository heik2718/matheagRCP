/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblattitem;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.IArbeitsblattNames;
import de.egladil.mathejungalt.domain.types.EnumTypes.Thema;
import de.egladil.mathejungalt.persistence.api.IPersistenceservice;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.comparators.AufgabensItemStufeComparator;
import de.egladil.mathejungalt.service.stammdaten.impl.filters.AufgabensammlungitemThemaFilter;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.MatheAGGeneratorUtils;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.ArbeitsblattGenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.ArbeitsblattQuellenGenerator;

/**
 * @author aheike
 */
public class ArbeitsblattserviceDelegate {

	private static final Logger LOG = LoggerFactory.getLogger(ArbeitsblattserviceDelegate.class);

	/** */
	private Map<String, Arbeitsblatt> arbeitsblaetterMap;

	/** */
	private ArbeitsblattGenerator arbeitsblattGenerator = new ArbeitsblattGenerator();

	/** */
	private ArbeitsblattQuellenGenerator arbeitsblattQuellenGenerator = new ArbeitsblattQuellenGenerator();

	/** */
	private int nextArbeitsblattSchluesselInt = -1;

	/** */
	private AufgabensItemStufeComparator comparator = new AufgabensItemStufeComparator();

	/** */
	private AufgabensammlungitemThemaFilter themaFilter = new AufgabensammlungitemThemaFilter();

	/**
	 *
	 */
	public ArbeitsblattserviceDelegate() {
	}

	/**
	 * Alle Arbeitsblätter aus der Persistenzschicht laden.
	 * 
	 * @return {@link Collection} von {@link Arbeitsblatt}
	 * @throws MatheJungAltException
	 */
	protected Collection<Arbeitsblatt> getArbeitsblaetter(IPersistenceservice pPersistenceService)
		throws MatheJungAltException {
		checkArbeitsblaetterMap(pPersistenceService);
		return arbeitsblaetterMap.values();
	}

	/**
	 * Falls die MinikaenguruMap nicht initialisiert ist, wird sie hier initialisiert.
	 */
	private void checkArbeitsblaetterMap(IPersistenceservice pPersistenceService) {
		if (arbeitsblaetterMap == null) {
			arbeitsblaetterMap = new HashMap<String, Arbeitsblatt>();
			List<Arbeitsblatt> arbeitsblaetter = pPersistenceService.getArbeitsblattRepository()
				.findAllArbeitsblaetter();
			for (Arbeitsblatt arbeitsblatt : arbeitsblaetter) {
				// arbeitsblaetterMap.put(arbeitsblatt.getUuid(), arbeitsblatt);
				arbeitsblaetterMap.put(arbeitsblatt.getSchluessel(), arbeitsblatt);
			}
		}
	}

	/**
	 * Generiert Aufgaben und Lösungen.
	 * 
	 * @param pPath
	 * @param pArbeitsblatt
	 */
	protected void generate(String pPath, Arbeitsblatt pArbeitsblatt, IStammdatenservice stammdatenservice) {
		arbeitsblattGenerator.setAusgabeLoesungen(false);
		arbeitsblattGenerator.setArbeitsblatt(pArbeitsblatt);
		MatheAGGeneratorUtils.writeOutput(pPath, arbeitsblattGenerator, stammdatenservice);

		arbeitsblattGenerator.setAusgabeLoesungen(true);
		MatheAGGeneratorUtils.writeOutput(pPath, arbeitsblattGenerator, stammdatenservice);

		arbeitsblattQuellenGenerator.setArbeitsblatt(pArbeitsblatt);
		MatheAGGeneratorUtils.writeOutput(pPath, arbeitsblattQuellenGenerator, stammdatenservice);
	}

	/**
	 * Die Items des Arbeitsblatts werden numeriert: Nummer beginnt mit Anfangsbuchstabe des Themas und dann wird
	 * fortlaufend numeriert.
	 * 
	 * @param pArbeitsblatt {@link Arbeitsblatt}
	 */
	protected void itemsNumerieren(Arbeitsblatt pArbeitsblatt) {
		List<Arbeitsblattitem> arbblattItems = new ArrayList<Arbeitsblattitem>();
		List<IAufgabensammlungItem> items = pArbeitsblatt.getItems();

		Map<Thema, List<Arbeitsblattitem>> themenlisten = new HashMap<Thema, List<Arbeitsblattitem>>();
		for (Thema thema : Thema.values()) {
			themenlisten.put(thema, getItems(items, thema));
		}

		for (Thema thema : themenlisten.keySet()) {
			arbblattItems = themenlisten.get(thema);
			Collections.sort(arbblattItems, comparator);
			int laufendeNummer = 1;
			for (Arbeitsblattitem item : arbblattItems) {
				item.setNummer("" + thema + laufendeNummer);
				laufendeNummer++;
			}
		}
	}

	/**
	 * @param pItems
	 * @param pThema
	 * @return
	 */
	private List<Arbeitsblattitem> getItems(List<IAufgabensammlungItem> pItems, Thema pThema) {
		List<Arbeitsblattitem> result = new ArrayList<Arbeitsblattitem>();
		for (IAufgabensammlungItem item : pItems) {
			if (themaFilter.select(item, pThema)) {
				result.add((Arbeitsblattitem) item);
			}
		}
		return result;
	}

	/**
	 * Ermittelt den naechsten freien Schluesselwert fuer ein Arbeitsblatt.
	 * 
	 * @param pPersistenceService {@link IPersistenceservice}
	 * @return String
	 */
	private String nextArbeitsblattSchluessel(IPersistenceservice pPersistenceService) {
		if (nextArbeitsblattSchluesselInt < 0) {
			nextArbeitsblattSchluesselInt = Integer
				.valueOf(pPersistenceService.getArbeitsblattRepository().getMaxKey()).intValue() + 1;
		}
		String schl = "" + (nextArbeitsblattSchluesselInt);
		if (schl.length() < IArbeitsblattNames.LENGTH_SCHLUESSEL) {
			schl = StringUtils.leftPad(schl, IArbeitsblattNames.LENGTH_SCHLUESSEL, '0');
		}
		nextArbeitsblattSchluesselInt++;
		return schl;
	}

	/**
	 * Eine neues Arbeitsblatt anlegen.
	 * 
	 * @param pPersistenceservice
	 * @return {@link Arbeitsblatt}
	 * @throws MatheJungAltException
	 */
	protected Arbeitsblatt arbeitsblattAnlegen(IPersistenceservice pPersistenceservice) throws MatheJungAltException {
		Arbeitsblatt arbeitsblatt = new Arbeitsblatt();
		arbeitsblatt.setSchluessel(nextArbeitsblattSchluessel(pPersistenceservice));
		arbeitsblatt.setBeendet(IAufgabensammlung.NICHT_BEENDET);
		arbeitsblatt.setItemsLoaded(true);
		return arbeitsblatt;
	}

	/**
	 * @param pArbeitsblatt
	 * @param pAufgabe
	 * @return
	 */
	public IAufgabensammlungItem aufgabeHinzufuegen(Arbeitsblatt pArbeitsblatt, Aufgabe pAufgabe) {
		if (pArbeitsblatt == null || pAufgabe == null) {
			String msg = MatheJungAltException
				.argumentNullMessage(pArbeitsblatt == null ? "pArbeitsblatt" : "pAufgabe");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (pArbeitsblatt.getBeendet() != 0) {
			throw new MatheJungAltException(
				"Das Arbeitsblatt ist bereits beendet. Es kann keine Aufgabe mehr hinzugefuegt werden.");
		}
		if (Aufgabe.LOCKED.equals(pAufgabe.getGesperrtFuerArbeitsblatt())
			&& Arbeitsblatt.SPERREND == pArbeitsblatt.getSperrend()) {
			throw new MatheJungAltException(
				"Die Aufgabe kann nicht zum Arbeitsblatt zugeordnet werden. Das Arbeitsblatt ist sperrend und die Aufgabe für Arbeitsblätter gesperrt.");
		}
		Arbeitsblattitem item = new Arbeitsblattitem();
		item.setAufgabe(pAufgabe);
		item.setArbeitsblatt(pArbeitsblatt);
		if (pArbeitsblatt.getSperrend() == Arbeitsblatt.SPERREND) {
			pAufgabe.setGesperrtFuerArbeitsblatt(Aufgabe.LOCKED);
		}
		pArbeitsblatt.addItem(item);
		return item;
	}

	/**
	 * @param pArbeitsblatt
	 * @throws MatheJungAltException
	 */
	protected void arbeitsblattSpeichern(Arbeitsblatt pArbeitsblatt, IPersistenceservice pPersistenceService)
		throws MatheJungAltException {
		boolean neu = pArbeitsblatt.isNew();
		pPersistenceService.getArbeitsblattRepository().save(pArbeitsblatt);
		if (neu) {
			// arbeitsblaetterMap.put(pArbeitsblatt.getUuid(), pArbeitsblatt);
			arbeitsblaetterMap.put(pArbeitsblatt.getSchluessel(), pArbeitsblatt);
		}
	}
}
