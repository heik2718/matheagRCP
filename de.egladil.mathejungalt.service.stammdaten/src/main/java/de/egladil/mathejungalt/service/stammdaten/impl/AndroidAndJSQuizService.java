/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.service.stammdaten.impl;

import java.io.File;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;

import de.egladil.mathejungalt.config.IMatheJungAltConfiguration.RaetsellisteGeneratorModus;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.service.stammdaten.quiz.HtlatexPostProcessor;
import de.egladil.mathejungalt.service.stammdaten.quiz.PrepareQuizHtmlGenerator;
import de.egladil.mathejungalt.service.stammdaten.quiz.QuizPathProvider;

/**
 * Stellt Methoden zum Generieren der Dateien zur Verfügung, die für ANDROID-Mathe-Quiz oder ein Web-Quiz erforderlich
 * sind.
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class AndroidAndJSQuizService extends AbstractStammdatenservice {

	/**
   *
   */
	public AndroidAndJSQuizService() {
	}

	/**
	 * Generiert die Verzeichnis-Struktur, Latex-Dateien und bat-Dateien, mit denen dann die Html-Dateien in einem
	 * externen Schritt generiert werden können. Wenn alles glatt läuft, befindet sich im Arbeitsverzeichnis eine Datei
	 * namens [raetsel.schluessel]_pre.txt. Daran kann man im Filesystem sehen, ob runHtlatex.bat ausgeführt werden
	 * kann.
	 *
	 * @param raetsel
	 * @param path
	 * @param pMonitor
	 * @return
	 */
	public IStatus prepareHtlatex(MCArchivraetsel raetsel, String path, IProgressMonitor pMonitor) {
		final PrepareQuizHtmlGenerator prepareQuizHtmlGenerator = new PrepareQuizHtmlGenerator();
		IStatus status = prepareQuizHtmlGenerator.generate(raetsel, path, pMonitor);
		return status;
	}

	/**
	 * PostHtlatex kann ausgeführt werden, wenn es eine entsprechende Marker-Datei gibt.
	 *
	 * @param raetsel
	 * @param pathWorkDir
	 * @return
	 */
	public boolean canRunPostHtlatexProcessor(MCArchivraetsel raetsel, String pathWorkDir) {
		String pathReadyMarker = QuizPathProvider.getPathReadyMarkerPostHtlatex(raetsel, pathWorkDir);
		File file = new File(pathReadyMarker);
		return file.canRead();
	}

	public boolean canPrepareHtlatex(MCArchivraetsel raetsel, String pathWorkDir) {
		String pathReadyMarker = QuizPathProvider.getPathReadyMarkerHtlatexPrepare(raetsel, pathWorkDir);
		File file = new File(pathReadyMarker);
		return !file.canRead();
	}

	/**
	 * @param raetsel
	 * @param path
	 * @param modus
	 * @param pMonitor
	 * @return
	 */
	public IStatus processPostHtlatexTasks(MCArchivraetsel raetsel, String path, RaetsellisteGeneratorModus modus,
		IProgressMonitor pMonitor) {
		final HtlatexPostProcessor postProcessor = new HtlatexPostProcessor();
		IStatus status = postProcessor.process(raetsel, path, modus, pMonitor);
		return status;
	}
}
