/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten;

/**
 * @author aheike
 */
public interface IPreferenceKeyConstants {

	public static final String P_AKTUELLE_RUNDE = "aktuelleRunde";

	public static final String P_AKTUELLES_SCHULJAHR = "aktuelles Schuljahr";

	public static final String P_LATEX_EXE = "Latex-Programm";

	public static final String P_DVIPS_EXE = "DviPS-Programm";
}
