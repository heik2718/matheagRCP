/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.consistence;

import de.egladil.base.exceptions.MatheJungAltInconsistentEntityException;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;

/**
 * @author aheike
 */
public class AufgabensammlungItemConsistenceCheckDelegate {

	/**
	 *
	 */
	public AufgabensammlungItemConsistenceCheckDelegate() {

	}

	/**
	 * Prüft die Items.
	 *
	 * @param pAufgabensammlung
	 * @throws MatheJungAltInconsistentEntityException
	 */
	protected void checkItems(IAufgabensammlung pAufgabensammlung) throws MatheJungAltInconsistentEntityException {
		for (IAufgabensammlungItem item : pAufgabensammlung.getItems()) {
			if (item.getAufgabe() == null) {
				throw new MatheJungAltInconsistentEntityException(
					"IAufgabensammlungItem: die Aufgabe darf nicht null sein.");
			}
			if (item.getAufgabensammlung() == null) {
				throw new MatheJungAltInconsistentEntityException(
					"IAufgabensammlungItem: die Aufgabensammlung darf nicht null sein.");
			}
		}
	}

}
