/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.consistence;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.exceptions.MatheJungAltInconsistentEntityException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;

/**
 * @author aheike
 */
public class MinikaenguruConsistenceChecker implements IConsistencecheckProvider {

	private final AufgabensammlungItemConsistenceCheckDelegate itemsCheckDelegate = new AufgabensammlungItemConsistenceCheckDelegate();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.consistencecheck.IConsistencecheckProvider#canCheck(de.egladil.mathejungalt.domain
	 * .AbstractMatheAGObject)
	 */
	@Override
	public boolean canCheck(Object pObject) {
		return pObject instanceof Minikaenguru;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.consistencecheck.IConsistencecheckProvider#checkConsistent(de.egladil.mathejungalt
	 * .domain.AbstractMatheAGObject)
	 */
	@Override
	public void checkConsistent(AbstractMatheAGObject pObject) throws MatheJungAltException {
		Minikaenguru minikaenguru = (Minikaenguru) pObject;
		if (minikaenguru.getJahr() == null) {
			throw new MatheJungAltInconsistentEntityException("Minikaenguru: das Jahr darf nicht null sein.");
		}
		itemsCheckDelegate.checkItems(minikaenguru);
	}
}
