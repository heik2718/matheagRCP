/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.latex.ILaTeXService;
import de.egladil.base.utils.OsgiServiceLocator;
import de.egladil.mathejungalt.config.IMatheJungAltConfiguration;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgaben.IAufgabeNames;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.medien.Buch;
import de.egladil.mathejungalt.domain.medien.Zeitschrift;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.domain.quellen.AbstractQuelle;
import de.egladil.mathejungalt.domain.quellen.Buchquelle;
import de.egladil.mathejungalt.domain.quellen.IQuelleNames;
import de.egladil.mathejungalt.domain.quellen.Kindquelle;
import de.egladil.mathejungalt.domain.quellen.MitgliederQuelleItem;
import de.egladil.mathejungalt.domain.quellen.MitgliedergruppenQuelle;
import de.egladil.mathejungalt.domain.quellen.NullQuelle;
import de.egladil.mathejungalt.domain.quellen.Zeitschriftquelle;
import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenzweck;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.MatheAGGeneratorUtils;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.AbstractAufgabeGenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.AufgabeGenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.LoesungGenerator;

/**
 * @author aheike
 */
public class Aufgabenservice extends AbstractLaTeXConcernedService {

	/** */
	private Map<String, Aufgabe> aufgabenMap;

	/** */
	private NullQuelle nullQuelle;

	/** */
	private int nextQuelleSchluesselInt = -1;

	/** */
	private int nextAufgabeSchluesselInt = -1;

	private SerienserviceDelegate serienserviceDelegate = new SerienserviceDelegate();

	private final Verzeichnisrechner verzeichnisrechner = new Verzeichnisrechner();

	/**
   *
   */
	public Aufgabenservice() {
		super();
	}

	/**
	 * @return
	 * @throws MatheJungAltException
	 */
	protected Aufgabe aufgabeAnlegen() throws MatheJungAltException {
		Aufgabe aufgabe = new Aufgabe();
		String nextAufgabenSchluessel = nextAufgabenSchluessel();
		aufgabe.setSchluessel(nextAufgabenSchluessel);
		aufgabe.setQuelle(getNullQuelle());
		final String verzeichnis = verzeichnisrechner.determineVerzeichnisZuSchluessel(nextAufgabenSchluessel);
		aufgabe.setVerzeichnis(verzeichnis);
		return aufgabe;
	}

	/**
	 * Die Quelle zur gegebenen Id laden.
	 *
	 * @param pQuelle long
	 * @return AbstractQuelle
	 * @throws MatheJungAltException falls es diese Quelle nicht gibt.
	 */
	protected void quelleNachladen(AbstractQuelle pQuelle) throws MatheJungAltException {
		getPersistenceservice().getQuellenRepository().completeQuelle(pQuelle);
	}

	/**
	 * Aufgabe mit diesem Schluessel suchen.
	 *
	 * @param pSchluessel
	 * @return {@link Aufgabe} oder null (wenn Parameter null oder Aufgabe nicht vorhanden.
	 */
	protected Aufgabe aufgabeZuSchluessel(String pSchluessel) {
		if (pSchluessel == null) {
			return null;
		}
		checkAufgabenmap();
		return aufgabenMap.get(pSchluessel);
	}

	/**
	 * @param zweck
	 * @param stufe
	 * @return
	 */
	protected List<Aufgabe> aufgabenMitZweckUndStufe(Aufgabenzweck zweck, int stufe) {
		checkAufgabenmap();
		List<Aufgabe> result = new ArrayList<>();
		for (Aufgabe aufgabe : aufgabenMap.values()) {
			if (aufgabe.getZweck() == zweck && aufgabe.getStufe() == stufe) {
				result.add(aufgabe);
			}
		}
		return result;
	}

	/**
	 * @param pAufgabenMap
	 * @param pPathWorkDir String Pfad zum temporaeren Arbeitsverzeichnis beim Compilieren und Transformieren
	 * @param pMonitor
	 * @return TODO
	 * @throws MatheJungAltException
	 */
	protected IStatus generateLaTeXAndTransformToPs(Map<String, Aufgabe> pAufgabenMap, String pPathWorkDir,
		IStammdatenservice stammdatenservice, IProgressMonitor pMonitor) throws MatheJungAltException {
		final int totalWork = pAufgabenMap.size();
		SubMonitor subMonitor = SubMonitor.convert(pMonitor, 80);
		final AufgabeGenerator aufgabeGenerator = new AufgabeGenerator();
		final LoesungGenerator loesungGenerator = new LoesungGenerator();
		final File workDir = prepareWorkDir(pPathWorkDir);

		SubMonitor loopProgress = subMonitor.newChild(80).setWorkRemaining(totalWork);
		List<IStatus> statusList = new ArrayList<>();
		for (String key : pAufgabenMap.keySet()) {
			loopProgress.subTask("generiere " + key);
			IStatus status = processAufgabe(pAufgabenMap.get(key), aufgabeGenerator, loesungGenerator, workDir, key,
				stammdatenservice);
			// IStatus status = processAufgabeToPng(pAufgabenMap.get(key), aufgabeGenerator, loesungGenerator, workDir,
			// key, stammdatenservice);
			if (!status.isOK()) {
				statusList.add(status);
			}
			loopProgress.worked(1);
		}
		subMonitor.subTask("lösche temporäre Dateien");
		subMonitor.setWorkRemaining(20);
		clearTemporaryLaTeXFiles(pPathWorkDir);
		subMonitor.worked(100);
		if (!statusList.isEmpty()) {
			MultiStatus result = new MultiStatus(IStammdatenservice.PLUGIN_ID, IStatus.WARNING,
				statusList.toArray(new IStatus[0]), "Beim Generieren sind Fehler aufgetreten", null);
			return result;
		}
		return new Status(IStatus.OK, IStammdatenservice.PLUGIN_ID, "alles paletti :)");
	}

	/**
	 * Generiert für eine Aufgabe 2 LaTeX-Dateien, compiliert diese und wandelt sie um in ps.
	 *
	 * @param aufgabe
	 * @param pPathWorkDir
	 * @param aufgabeGenerator
	 * @param loesungGenerator
	 * @param workDir
	 * @param key
	 * @return
	 */
	private IStatus processAufgabe(Aufgabe aufgabe, final AufgabeGenerator aufgabeGenerator,
		final LoesungGenerator loesungGenerator, final File workDir, String key, IStammdatenservice stammdatenservice) {
		aufgabeGenerator.init(aufgabe, key);
		final String pathWorkDir = workDir.getAbsolutePath();
		String latexFilePathA = generateLaTeXFile(aufgabeGenerator, pathWorkDir, stammdatenservice);
		IStatus status = compile(latexFilePathA);
		StringBuffer sb = new StringBuffer("Fehler beim Compilieren und Umwandeln:");
		boolean error = false;
		if (status.isOK()) {
			String dviFilePath = getDviFilePath(latexFilePathA, key, true);
			status = dviPs(dviFilePath);
			if (!status.isOK()) {
				sb.append("\n");
				sb.append(status.getMessage());
				error = true;
			}
		} else {
			sb.append("\n");
			sb.append(status.getMessage());
			error = true;
		}
		loesungGenerator.init(aufgabe, key);
		String latexFilePathB = generateLaTeXFile(loesungGenerator, pathWorkDir, stammdatenservice);
		status = compile(latexFilePathB);
		if (status.isOK()) {
			String dviFilePath = getDviFilePath(latexFilePathA, key, false);
			status = dviPs(dviFilePath);
			if (!status.isOK()) {
				sb.append("\n");
				sb.append(status.getMessage());
				error = true;
			}
		} else {
			sb.append("\n");
			sb.append(status.getMessage());
			error = true;
		}
		return error ? new Status(IStatus.ERROR, IStammdatenservice.PLUGIN_ID, sb.toString()) : new Status(IStatus.OK,
			IStammdatenservice.PLUGIN_ID, "success");
	}

	/**
	 * Generiert für eine Aufgabe 2 PDF-Dateien und wandelt sie um in png.
	 *
	 * @param aufgabe
	 * @param pPathWorkDir
	 * @param aufgabeGenerator
	 * @param loesungGenerator
	 * @param workDir
	 * @param key
	 * @return
	 */
	private IStatus processAufgabeToPng(Aufgabe aufgabe, final AufgabeGenerator aufgabeGenerator,
		final LoesungGenerator loesungGenerator, final File workDir, String key, IStammdatenservice stammdatenservice) {
		aufgabeGenerator.init(aufgabe, key);
		final String pathWorkDir = workDir.getAbsolutePath();
		String latexFilePathA = generateLaTeXFile(aufgabeGenerator, pathWorkDir, stammdatenservice);
		IStatus status = latex2png(latexFilePathA);
		StringBuffer sb = new StringBuffer("Fehler beim Compilieren und Umwandeln:");
		boolean error = false;
		if (!status.isOK()) {
			sb.append("\n");
			sb.append(status.getMessage());
			error = true;
		}
		loesungGenerator.init(aufgabe, key);
		String latexFilePathB = generateLaTeXFile(loesungGenerator, pathWorkDir, stammdatenservice);
		status = latex2png(latexFilePathB);
		if (!status.isOK()) {
			sb.append("\n");
			sb.append(status.getMessage());
			error = true;
		}
		return error ? new Status(IStatus.ERROR, IStammdatenservice.PLUGIN_ID, sb.toString()) : new Status(IStatus.OK,
			IStammdatenservice.PLUGIN_ID, "success");
	}

	/**
	 * @param latexFilePath
	 * @return
	 */
	private IStatus latex2png(String latexFilePath) {
		ILaTeXService latexService = new OsgiServiceLocator().getService(ILaTeXService.class);
		IStatus status = latexService.latex2Png(latexFilePath);
		return status;
	}

	/**
	 * @param pGenerator
	 * @param pPathWorkDir
	 * @return String den absoluten Pfad der generierten LaTeX-Datei
	 */
	private String generateLaTeXFile(AbstractAufgabeGenerator pGenerator, String pPathWorkDir,
		IStammdatenservice stammdatenservice) {
		MatheAGGeneratorUtils.writeOutput(pPathWorkDir, pGenerator, stammdatenservice);
		return pPathWorkDir + "/" + pGenerator.getFilename();
	}

	/**
	 * @param pLatexFilePath
	 * @param pFilenamePrefix
	 * @param pIsAufgabe
	 * @return
	 */
	private String getDviFilePath(String pLatexFilePath, String pFilenamePrefix, boolean pIsAufgabe) {
		String dviFilePath = new File(pLatexFilePath).getParent() + "/" + pFilenamePrefix;
		if (pIsAufgabe) {
			dviFilePath += ".dvi";
		} else {
			dviFilePath += "_l.dvi";
		}
		return dviFilePath;
	}

	/**
	 * @return
	 * @throws MatheJungAltException
	 */
	protected Collection<Aufgabe> getAufgaben() throws MatheJungAltException {
		checkAufgabenmap();
		return aufgabenMap.values();
	}

	/**
	 * Falls die Aufgabenmap nicht initialisiert ist, wird sie hier initialisiert.
	 */
	private void checkAufgabenmap() {
		if (aufgabenMap == null) {
			aufgabenMap = new HashMap<String, Aufgabe>();
			List<Aufgabe> aufgaben = getPersistenceservice().getAufgabenRepository().findAll();
			for (Aufgabe aufgabe : aufgaben) {
				aufgabenMap.put(aufgabe.getSchluessel(), aufgabe);
			}
		}
	}

	/**
	 * @param pAufgabe
	 * @throws MatheJungAltException
	 */
	protected void aufgabeSpeichern(Aufgabe pAufgabe) throws MatheJungAltException {
		boolean neu = pAufgabe.isNew();
		getPersistenceservice().getAufgabenRepository().save(pAufgabe);
		if (neu) {
			aufgabenMap.put(pAufgabe.getSchluessel(), pAufgabe);
			fireObjectAddedEvent(pAufgabe);
		}
	}

	/**
	 * @param pBuch
	 * @param pSeite
	 * @return
	 * @throws MatheJungAltException
	 */
	protected Buchquelle buchquelleFindenOderAnlegen(Buch pBuch, int pSeite) throws MatheJungAltException {
		Buchquelle bq = getPersistenceservice().getQuellenRepository().findBuchquelle(pBuch, pSeite);
		if (bq == null) {
			bq = new Buchquelle();
			bq.setSchluessel(nextQuellenSchluessel());
			bq.setBuch(pBuch);
			bq.setSeite(pSeite);
			save(bq);
			bq.setCompletelyLoaded(true);
		}
		return bq;
	}

	protected MitgliedergruppenQuelle gruppenquelleAnlegen(Collection<Mitglied> pMitglieder) {
		MitgliedergruppenQuelle quelle = new MitgliedergruppenQuelle();
		quelle.setSchluessel(nextQuellenSchluessel());
		for (Mitglied mitglied : pMitglieder) {
			quelle.addItem(new MitgliederQuelleItem(mitglied, quelle));
		}
		save(quelle);
		quelle.setCompletelyLoaded(true);
		return quelle;
	}

	/**
	 * @param pQuelle
	 */
	private void save(AbstractQuelle pQuelle) {
		getPersistenceservice().getQuellenRepository().save(pQuelle);
	}

	/**
	 * Ermittelt den naechsten freien Schluessel fuer eine Quelle.
	 *
	 * @return String
	 */
	private String nextQuellenSchluessel() {
		if (nextQuelleSchluesselInt < 0) {
			nextQuelleSchluesselInt = Integer.valueOf(getPersistenceservice().getQuellenRepository().getMaxKey())
				.intValue() + 1;
		}
		String schl = "" + (nextQuelleSchluesselInt);
		if (schl.length() < IQuelleNames.LENGTH_SCHLUESSEL) {
			schl = StringUtils.leftPad(schl, IQuelleNames.LENGTH_SCHLUESSEL, '0');
		}
		nextQuelleSchluesselInt++;
		return schl;
	}

	/**
	 * Ermittelt den naechsten freien Schluesselwert fuer eine Aufgabe.
	 *
	 * @return String
	 */
	private String nextAufgabenSchluessel() {
		if (nextAufgabeSchluesselInt < 0) {
			nextAufgabeSchluesselInt = Integer.valueOf(getPersistenceservice().getAufgabenRepository().getMaxKey())
				.intValue() + 1;
		}
		String schl = "" + (nextAufgabeSchluesselInt);
		if (schl.length() < IAufgabeNames.LENGTH_SCHLUESSEL) {
			schl = StringUtils.leftPad(schl, IAufgabeNames.LENGTH_SCHLUESSEL, '0');
		}
		nextAufgabeSchluesselInt++;
		return schl;
	}

	/**
	 * @return
	 * @throws MatheJungAltException
	 */
	protected NullQuelle getNullQuelle() throws MatheJungAltException {
		if (nullQuelle == null) {
			nullQuelle = getPersistenceservice().getQuellenRepository().findNullQuelle();
		}
		return nullQuelle;
	}

	/**
	 * @param pMitglied
	 * @param pJahre
	 * @param pKlasse
	 * @return
	 * @throws MatheJungAltException
	 */
	protected Kindquelle kindquelleFindenOderAnlegen(Mitglied pMitglied, int pJahre, int pKlasse)
		throws MatheJungAltException {
		Kindquelle quelle = getPersistenceservice().getQuellenRepository().findKindquelle(pMitglied, pJahre, pKlasse);
		if (quelle == null) {
			quelle = new Kindquelle();
			quelle.setSchluessel(nextQuellenSchluessel());
			quelle.setMitglied(pMitglied);
			quelle.setJahre(pJahre);
			quelle.setKlasse(pKlasse);
			save(quelle);
			quelle.setCompletelyLoaded(true);
		}
		return quelle;
	}

	/**
	 * @param pZeitschrift
	 * @param pAusgabe
	 * @param pJahrgang
	 * @return
	 * @throws MatheJungAltException
	 */
	protected Zeitschriftquelle zeitschriftquelleFindenOderAnlegen(Zeitschrift pZeitschrift, int pAusgabe, int pJahrgang)
		throws MatheJungAltException {
		Zeitschriftquelle quelle = getPersistenceservice().getQuellenRepository().findZeitschriftquelle(pZeitschrift,
			pAusgabe, pJahrgang);
		if (quelle == null) {
			quelle = new Zeitschriftquelle();
			quelle.setSchluessel(nextQuellenSchluessel());
			quelle.setAusgabe(pAusgabe);
			quelle.setJahrgang(pJahrgang);
			quelle.setZeitschrift(pZeitschrift);
			save(quelle);
			quelle.setCompletelyLoaded(true);
		}
		return quelle;
	}

	/**
	 * @param pAufgabe Aufgabe
	 * @return String J oder N
	 */
	protected String isGesperrtFuerArbeitsblatt(Aufgabe pAufgabe, IMatheJungAltConfiguration configuration) {
		if (pAufgabe.getGesperrtFuerWettbewerb() == null) {
			int anzahl = anzahlSerieWettbewerbMitAufgabe(pAufgabe, false, configuration);
			pAufgabe.setGesperrtFuerWettbewerb(anzahl == 0 ? Aufgabe.NOT_LOCKED : Aufgabe.LOCKED);
		}
		if (pAufgabe.getGesperrtFuerArbeitsblatt() == null) {
			int anzahl = getPersistenceservice().getAufgabenRepository().anzahlSperrendeArbeitsblaetterMitAufgabe(
				pAufgabe);
			pAufgabe.setAnzahlSperrendeReferenzen(anzahl + pAufgabe.getAnzahlSperrendeReferenzen());
			pAufgabe.setGesperrtFuerArbeitsblatt(anzahl == 0 ? Aufgabe.NOT_LOCKED : Aufgabe.LOCKED);
		}
		return pAufgabe.getGesperrtFuerWettbewerb().equals(Aufgabe.LOCKED)
			&& pAufgabe.getGesperrtFuerArbeitsblatt().equals(Aufgabe.NOT_LOCKED) ? Aufgabe.NOT_LOCKED : Aufgabe.LOCKED;
	}

	/**
	 * @param pAufgabe Aufgabe
	 * @param stufe6globalEinmalig TODO
	 * @return String J oder N
	 */
	protected String isGesperrtFuerWettbewerb(Aufgabe pAufgabe, boolean stufe6globalEinmalig,
		IMatheJungAltConfiguration configuration) {
		if (Aufgabe.LOCKED.equals(pAufgabe.getZuSchlechtFuerSerie())) {
			return Aufgabe.LOCKED;
		}
		if (pAufgabe.getGesperrtFuerWettbewerb() == null) {
			int anzahl = anzahlSerieWettbewerbMitAufgabe(pAufgabe, stufe6globalEinmalig, configuration);
			if (anzahl == 0) {
				if (pAufgabe.getGesperrtFuerArbeitsblatt() == null) {
					int anzahlArbblatt = getPersistenceservice().getAufgabenRepository()
						.anzahlSperrendeArbeitsblaetterMitAufgabe(pAufgabe);
					if (anzahlArbblatt == 0) {
						pAufgabe.setGesperrtFuerWettbewerb(Aufgabe.NOT_LOCKED);
					} else {
						pAufgabe.setGesperrtFuerWettbewerb(Aufgabe.LOCKED);
					}
				}
			} else {
				pAufgabe.setGesperrtFuerWettbewerb(Aufgabe.LOCKED);
			}
		}
		return pAufgabe.getGesperrtFuerWettbewerb();
	}

	/**
	 * @param pAufgabe
	 */
	protected void aufgabeStatusZuSchlechtFuerSerieAendern(Aufgabe pAufgabe) {
		Assert.isNotNull(pAufgabe);
		Assert.isNotNull(pAufgabe.getId());
		Assert.isNotNull(pAufgabe.getZuSchlechtFuerSerie());
		getPersistenceservice().getAufgabenRepository().changeZuSchlechtFuerSerie(pAufgabe);
	}

	/**
	 * @param pAufgabe
	 * @param stufe6globalEinmalig TODO
	 * @return
	 */
	private int anzahlSerieWettbewerbMitAufgabe(Aufgabe pAufgabe, boolean stufe6globalEinmalig,
		IMatheJungAltConfiguration configuration) {
		if (pAufgabe.getZweck() == Aufgabenzweck.M) {
			return 1;
		}
		if (pAufgabe.getZweck() == Aufgabenzweck.K) {
			return getPersistenceservice().getAufgabenRepository().anzahlMinikaenguruMitAufgabe(pAufgabe);
		}
		// Stufe 6 soll sich nicht wiederholen.
		if (pAufgabe.getStufe() == 6 && stufe6globalEinmalig) {
			return getPersistenceservice().getAufgabenRepository().serienIdMitAufgabe(pAufgabe).isEmpty() ? 0 : 1;
		}

		List<Long> serienIds = getPersistenceservice().getAufgabenRepository().serienIdMitAufgabe(pAufgabe);
		List<Serie> serien = serienserviceDelegate.getSerienByIds(serienIds, getPersistenceservice());
		int aktuelleRunde = Integer.valueOf(configuration.getConfigValue("serien.runden.aktuell"));
		int sperrzahl = Integer.valueOf(configuration.getConfigValue("serien.runden.wartedauer"));
		int minSperrend = aktuelleRunde - sperrzahl;
		List<Serie> sperrendeSerien = new ArrayList<Serie>();
		for (Serie serie : serien) {
			if (serie.getRunde() >= minSperrend) {
				sperrendeSerien.add(serie);
			}
		}
		pAufgabe.setAnzahlSperrendeReferenzen(sperrendeSerien.size());
		return sperrendeSerien.size();
	}
}
