/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.config.IMatheJungAltConfiguration;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serienitem;
import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenzweck;
import de.egladil.mathejungalt.persistence.api.IPersistenceservice;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.comparators.PreNumerierungComparator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.MatheAGGeneratorUtils;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.dat.Schluessel2SerieGenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html.AktuellSerieStufeGenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html.ArchivSerieStufeGenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html.ArchivseiteGenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html.QuellenseiteGenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html.SerieArchivIndexGenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html.SerienseiteGenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.SerieQuellenLaTeXGenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex.SerieStufeLaTeXGenerator;

/**
 * @author aheike
 */
public class SerienserviceDelegate {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(SerienserviceDelegate.class);

	/** */
	private static final String TRENNZEICHEN_AUFGABENNUMMER = "-";

	/** */
	private Map<Integer, Serie> serienMap;

	/** */
	private int nextSerieSchluesselInt = -1;

	/** */
	private Schluessel2SerieGenerator schluessel2SerieGenerator = new Schluessel2SerieGenerator();

	/** */
	private ArchivseiteGenerator archivseiteGenerator = new ArchivseiteGenerator();

	/** */
	private SerienseiteGenerator serienseiteGenerator = new SerienseiteGenerator();

	private QuellenseiteGenerator quellenseiteGenerator = new QuellenseiteGenerator();

	private SerieArchivIndexGenerator serieArchivIndexGenerator = new SerieArchivIndexGenerator();

	private ArchivSerieStufeGenerator archivSerieStufeGenerator = new ArchivSerieStufeGenerator();

	private AktuellSerieStufeGenerator aktuellSerieStufeGenerator = new AktuellSerieStufeGenerator();

	/** */
	private SerieQuellenLaTeXGenerator serieQuellenLaTeXGenerator = new SerieQuellenLaTeXGenerator();

	/** */
	private SerieStufeLaTeXGenerator serieStufeLaTeXGenerator = new SerieStufeLaTeXGenerator();

	private IMatheJungAltConfiguration configuration;

	/** */
	private final PreNumerierungComparator comparator = new PreNumerierungComparator();

	/**
	 *
	 */
	public SerienserviceDelegate() {
	}

	/**
	 * Alle Serien aus der Persistenzschicht laden.
	 *
	 * @return {@link Collection} von {@link Serie}
	 * @throws MatheJungAltException
	 */
	protected Collection<Serie> getSerien(IPersistenceservice pPersistenceService) throws MatheJungAltException {
		checkSerienmap(pPersistenceService);
		return serienMap.values();
	}

	/**
	 * Sucht alle Serien zur gegebenen Runde.
	 *
	 * @param pRunde
	 * @param pPersistenceService
	 * @return
	 */
	protected List<Serie> getSerien(int pRunde, IPersistenceservice pPersistenceService) {
		checkSerienmap(pPersistenceService);
		ArrayList<Serie> result = new ArrayList<Serie>();
		for (Serie serie : serienMap.values()) {
			if (serie.getRunde() != null && serie.getRunde().intValue() == pRunde) {
				result.add(serie);
			}
		}
		return result;
	}

	/**
	 * Falls die Serienmap nicht initialisiert ist, wird sie hier initialisiert.
	 */
	private void checkSerienmap(IPersistenceservice pPersistenceService) {
		if (serienMap == null) {
			serienMap = new HashMap<Integer, Serie>();
			List<Serie> serien = pPersistenceService.getSerienRepository().findAllSerien();
			for (Serie serie : serien) {
				serienMap.put(serie.getNummer(), serie);
			}
		}
	}

	/**
	 * Die gegebene Aufgabe wird zur gegebenen Serie hinzugefuegt.
	 *
	 * @param pSerie Serie, darf nicht null oder beendet sein.
	 * @param pAufgabe die Aufgabe. Darf nicht null oder gesperrt sein und muss vom Zweck S sein.
	 * @return {@link Serienitem}
	 * @throws MatheJungAltException bei einem Parameterfehler
	 */
	protected Serienitem aufgabeZuSerieHinzufuegen(Serie pSerie, Aufgabe pAufgabe) throws MatheJungAltException {
		if (pSerie == null || pAufgabe == null) {
			String msg = MatheJungAltException.argumentNullMessage(pSerie == null ? "pSerie" : "pAufgabe");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (pSerie.getBeendet() != 0) {
			throw new MatheJungAltException(
				"Die Serie ist bereits beendet. Es kann keine Aufgabe mehr hinzugefuegt werden.");
		}
		if (Aufgabe.LOCKED.equals(pAufgabe.getGesperrtFuerWettbewerb())) {
			throw new MatheJungAltException("Die Aufgabe kann nicht zur Serie zugeordnet werden. Sie ist gesperrt.");
		}
		if (!Aufgabenzweck.S.equals(pAufgabe.getZweck())) {
			throw new MatheJungAltException(
				"Die Aufgabe kann nicht zur Serie zugeodnet werden. Es ist keine Serienaufgabe.");
		}
		Serienitem item = new Serienitem();
		item.setAufgabe(pAufgabe);
		item.setSerie(pSerie);
		pAufgabe.setGesperrtFuerWettbewerb(Aufgabe.LOCKED);
		pSerie.addItem(item);
		return item;
	}

	/**
	 * Sucht den maximalen fachlichen Schlüssel.
	 *
	 * @return String
	 */
	protected Integer nextSerieKey(IPersistenceservice pPersistenceService) {
		if (nextSerieSchluesselInt < 0) {
			nextSerieSchluesselInt = Integer.valueOf(pPersistenceService.getSerienRepository().getMaxKey()).intValue();
		}
		return ++nextSerieSchluesselInt;
	}

	/**
	 * Die gegebene Serie speichern.
	 *
	 * @param pSerie
	 * @throws MatheJungAltException
	 */
	protected void serieSpeichern(Serie pSerie, IPersistenceservice pPersistenceService) throws MatheJungAltException {
		boolean neu = pSerie.isNew();
		pPersistenceService.getSerienRepository().saveSerie(pSerie);
		if (neu) {
			serienMap.put(pSerie.getNummer(), pSerie);
		}
	}

	/**
	 * @param pSerie
	 */
	protected void generateLaTeX(String pPath, Serie pSerie, IStammdatenservice stammdatenservice) {
		schluessel2SerieGenerator.setSerie(pSerie);
		serieQuellenLaTeXGenerator.setSerie(pSerie);
		serieStufeLaTeXGenerator.setSerie(pSerie);

		MatheAGGeneratorUtils.writeOutput(pPath, schluessel2SerieGenerator, stammdatenservice);

		serieQuellenLaTeXGenerator.setFontsize(Messages.getString("font_quellen"));
		MatheAGGeneratorUtils.writeOutput(pPath, serieQuellenLaTeXGenerator, stammdatenservice);

    	serieStufeLaTeXGenerator.setAusgabeLoesungen(false);
		for (int stufe = 1; stufe < 7; stufe++) {
			serieStufeLaTeXGenerator.setStufen(new int[] { stufe });
			serieStufeLaTeXGenerator.setFontsize(Messages.getString("font_" + stufe));
			serieStufeLaTeXGenerator.setAufgabenabstand(Messages.getString("abstand_" + stufe));
			MatheAGGeneratorUtils.writeOutput(pPath, serieStufeLaTeXGenerator, stammdatenservice);
			LOG.info("LaTeX-Aufgaben Stufe " + stufe + " fertig");
		}

		// alle Aufgaben
		final int[] alleStufen = new int[] { 1, 2, 3, 4, 5, 6 };
		serieStufeLaTeXGenerator.setStufen(alleStufen);
		serieStufeLaTeXGenerator.setFontsize(Messages.getString("font_aufgaben"));
		serieStufeLaTeXGenerator.setAufgabenabstand(Messages.getString("abstand_aufgaben"));
		MatheAGGeneratorUtils.writeOutput(pPath, serieStufeLaTeXGenerator, stammdatenservice);
		LOG.info("LaTeX-Aufgaben gesamt fertig");

		// alle Lösungen
		serieStufeLaTeXGenerator.setAusgabeLoesungen(true);
		serieStufeLaTeXGenerator.setFontsize(Messages.getString("font_loesungen"));
		serieStufeLaTeXGenerator.setAufgabenabstand(Messages.getString("abstand_loesungen"));
		MatheAGGeneratorUtils.writeOutput(pPath, serieStufeLaTeXGenerator, stammdatenservice);
		LOG.info("LaTeX-Loesungen fertig");
	}

	/**
	 * @param pPath
	 * @param pSerie
	 */
	protected void generateWebcontent(String pPath, Serie pSerie, IStammdatenservice stammdatenservice) {
		MatheAGGeneratorUtils.writeOutput(pPath, serienseiteGenerator.getFilename(pSerie),
			serienseiteGenerator.generate(pSerie));
		MatheAGGeneratorUtils.writeOutput(pPath, quellenseiteGenerator.getFilename(pSerie),
			quellenseiteGenerator.generate(pSerie));
		MatheAGGeneratorUtils.writeOutput(pPath, serieArchivIndexGenerator.getFilename(pSerie),
			serieArchivIndexGenerator.generate(pSerie));
		for (int i = 1; i < 7; i++) {
			MatheAGGeneratorUtils.writeOutput(pPath, aktuellSerieStufeGenerator.getFilename(i),
				aktuellSerieStufeGenerator.generate(stammdatenservice, pSerie, i));
			MatheAGGeneratorUtils.writeOutput(pPath, archivSerieStufeGenerator.getFilename(pSerie, i),
				archivSerieStufeGenerator.generate(stammdatenservice, pSerie, i));
		}
	}

	/**
	 * Generiert die Archivseite
	 *
	 * @param pPath
	 */
	protected void generateArchiv(String pPath, IStammdatenservice stammdatenservice) {
		MatheAGGeneratorUtils.writeOutput(pPath, archivseiteGenerator.getFilename(),
			archivseiteGenerator.generate(stammdatenservice));
	}

	/**
	 * Die Items der Serie werden numeriert.
	 *
	 * @param pSerie Serie die Serie.
	 */
	protected void itemsNumerieren(Serie pSerie) {
		String prefix = pSerie.getNummer() + TRENNZEICHEN_AUFGABENNUMMER;
		List<Serienitem> serienItems = new ArrayList<Serienitem>();
		List<IAufgabensammlungItem> items = pSerie.getItems();
		for (IAufgabensammlungItem item : items) {
			serienItems.add((Serienitem) item);
		}
		Collections.sort(serienItems, comparator);
		int aktuelleStufe = 1;
		int laufendeNummer = 1;
		for (Serienitem item : serienItems) {
			if (item.getAufgabe().getStufe().intValue() > aktuelleStufe) {
				laufendeNummer = 1;
				aktuelleStufe = item.getAufgabe().getStufe().intValue();
			}
			item.setNummer(prefix + aktuelleStufe + laufendeNummer);
			laufendeNummer++;
		}
	}

	/**
	 * Eine neue Serie anlegen.
	 *
	 * @return
	 * @throws MatheJungAltException
	 */
	protected Serie serieAnlegen(IPersistenceservice pPersistenceService) throws MatheJungAltException {
		Serie serie = new Serie();
		serie.setBeendet(IAufgabensammlung.NICHT_BEENDET);
		serie.setDatum(new Date());
		serie.setMonatJahrText("");
		serie.setRunde(aktuelleRunde(pPersistenceService));
		serie.setNummer(nextSerieKey(pPersistenceService));
		serie.setItemsLoaded(true);
		return serie;
	}

	/**
	 * @return int die aktuelle Runde.
	 */
	protected int aktuelleRunde(IPersistenceservice pPersistenceService) throws MatheJungAltException {
		return Integer.valueOf(configuration.getConfigValue("serien.runden.aktuell"));
	}

	/**
	 * @param pIds
	 * @param pPersistenceService
	 * @return List die Serien.
	 */
	public List<Serie> getSerienByIds(List<Long> pIds, IPersistenceservice pPersistenceService) {
		checkSerienmap(pPersistenceService);
		List<Serie> result = new ArrayList<Serie>();
		for (Serie serie : serienMap.values()) {
			if (pIds.contains(serie.getId()) && !result.contains(serie)) {
				result.add(serie);
			}
		}
		return result;
	}

	/**
	 * @param pConfiguration the configuration to set
	 */
	public final void setConfiguration(IMatheJungAltConfiguration pConfiguration) {
		configuration = pConfiguration;
	}
}
