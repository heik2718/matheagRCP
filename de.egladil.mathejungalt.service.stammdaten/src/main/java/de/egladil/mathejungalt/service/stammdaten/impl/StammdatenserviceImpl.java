/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.EgladilBusinessException;
import de.egladil.base.exceptions.MatheAGObjectNotGeneratableException;
import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.exceptions.MatheJungAltInconsistentEntityException;
import de.egladil.mathejungalt.config.IMatheJungAltConfiguration;
import de.egladil.mathejungalt.config.IMatheJungAltConfiguration.RaetsellisteGeneratorModus;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgaben.IAufgabeNames;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.mcraetsel.IMCArchivraetselNames;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.domain.mcraetsel.Urheber;
import de.egladil.mathejungalt.domain.medien.AbstractPrintmedium;
import de.egladil.mathejungalt.domain.medien.Autor;
import de.egladil.mathejungalt.domain.medien.Buch;
import de.egladil.mathejungalt.domain.medien.Zeitschrift;
import de.egladil.mathejungalt.domain.mitglieder.Diplom;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.domain.mitglieder.Serienteilnahme;
import de.egladil.mathejungalt.domain.quellen.AbstractQuelle;
import de.egladil.mathejungalt.domain.quellen.Buchquelle;
import de.egladil.mathejungalt.domain.quellen.Kindquelle;
import de.egladil.mathejungalt.domain.quellen.MitgliedergruppenQuelle;
import de.egladil.mathejungalt.domain.quellen.NullQuelle;
import de.egladil.mathejungalt.domain.quellen.Zeitschriftquelle;
import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.domain.schulen.Land;
import de.egladil.mathejungalt.domain.schulen.MinikaenguruTeilnahme;
import de.egladil.mathejungalt.domain.schulen.Schule;
import de.egladil.mathejungalt.domain.schulen.Schulkontakt;
import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenzweck;
import de.egladil.mathejungalt.domain.types.MCAufgabenstufen;
import de.egladil.mathejungalt.persistence.api.IPersistenceservice;
import de.egladil.mathejungalt.service.stammdaten.ActionNotPossibleException;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.event.IModelChangeListener;
import de.egladil.mathejungalt.service.stammdaten.event.ModelChangeEvent;
import de.egladil.mathejungalt.service.stammdaten.impl.comparators.ArbblattItemNummerComparator;
import de.egladil.mathejungalt.service.stammdaten.impl.consistence.AufgabeConsistentceChecker;
import de.egladil.mathejungalt.service.stammdaten.impl.consistence.AutorConsistenceChecker;
import de.egladil.mathejungalt.service.stammdaten.impl.consistence.BuchquelleConsistenceChecker;
import de.egladil.mathejungalt.service.stammdaten.impl.consistence.ConsistencecheckService;
import de.egladil.mathejungalt.service.stammdaten.impl.consistence.IConsistencecheckProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.consistence.KindquelleConsistenceChecker;
import de.egladil.mathejungalt.service.stammdaten.impl.consistence.KontaktConsistenceChecker;
import de.egladil.mathejungalt.service.stammdaten.impl.consistence.MCAufgabeConsistentceChecker;
import de.egladil.mathejungalt.service.stammdaten.impl.consistence.MediumConsistentChecker;
import de.egladil.mathejungalt.service.stammdaten.impl.consistence.MitgliedConsistenceChecker;
import de.egladil.mathejungalt.service.stammdaten.impl.consistence.SchuleConsistenceChecker;
import de.egladil.mathejungalt.service.stammdaten.impl.consistence.ZeitschriftquelleConsistenceChecker;

/**
 * Fassade zu mehreren einzelnen Services.
 *
 * @author aheike
 */
public class StammdatenserviceImpl implements IStammdatenservice, IModelChangeListener, PropertyChangeListener {

	private static final Logger LOG = LoggerFactory.getLogger(StammdatenserviceImpl.class);

	private IPersistenceservice persistenceservice;

	private Aufgabenservice aufgabenservice;

	private Raetselservice raetselservice;

	private Quizaufgabenservice quizaufgabenservice;

	private AndroidAndJSQuizService androidAndJSQuizService;

	private Medienservice medienservice;

	private Mitgliederservice mitgliederservice;

	private Aufgabensammlungservice aufgabensammlungservice;

	private SchulenService schulenservice;

	private IMatheJungAltConfiguration configuration;

	private final List<IConsistencecheckProvider> consistenceCheckProviders = new ArrayList<IConsistencecheckProvider>();

	private final List<IModelChangeListener> modelChangeListeners = new ArrayList<IModelChangeListener>();

	private boolean modelIsAdjusting = false;

	private Boolean stufe6MaximalEinmal = null;

	private ConsistencecheckService consistencecheckService = new ConsistencecheckService();

	/**
	 *
	 */
	public StammdatenserviceImpl() {
		consistenceCheckProviders.add(new AufgabeConsistentceChecker());
		consistenceCheckProviders.add(new AutorConsistenceChecker());
		consistenceCheckProviders.add(new BuchquelleConsistenceChecker());
		consistenceCheckProviders.add(new KindquelleConsistenceChecker());
		consistenceCheckProviders.add(new MitgliedConsistenceChecker());
		consistenceCheckProviders.add(new ZeitschriftquelleConsistenceChecker());
		consistenceCheckProviders.add(new MediumConsistentChecker());
		consistenceCheckProviders.add(new MCAufgabeConsistentceChecker());
		consistenceCheckProviders.add(new SchuleConsistenceChecker());
		consistenceCheckProviders.add(new KontaktConsistenceChecker());

		aufgabensammlungservice = new Aufgabensammlungservice();
		aufgabenservice = new Aufgabenservice();
		medienservice = new Medienservice();
		mitgliederservice = new Mitgliederservice();
		quizaufgabenservice = new Quizaufgabenservice();
		raetselservice = new Raetselservice();
		schulenservice = new SchulenService();
		androidAndJSQuizService = new AndroidAndJSQuizService();

		aufgabensammlungservice.addModelChangeListener(this);
		aufgabenservice.addModelChangeListener(this);
		medienservice.addModelChangeListener(this);
		mitgliederservice.addModelChangeListener(this);
		quizaufgabenservice.addModelChangeListener(this);
		raetselservice.addModelChangeListener(this);
		schulenservice.addModelChangeListener(this);

		LOG.info("StammdatenserviceImpl erzeugt");
	}

	protected void activate(final ComponentContext context) {
		LOG.info("inside Stammmdatenservice.activate()");
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#checkConsistent(de.egladil.mathejungalt.domain.
	 * AbstractMatheAGObject)
	 */
	@Override
	public void checkConsistent(final Object pDomainObject) throws MatheJungAltInconsistentEntityException,
		MatheJungAltException {
		if (pDomainObject == null) {
			final String msg = MatheJungAltException.argumentNullMessage("checkConsistent(Object): Parameter");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (pDomainObject instanceof AbstractMatheAGObject) {
			consistencecheckService.executeConsistencecheck(consistenceCheckProviders,
				Arrays.asList(new AbstractMatheAGObject[] { (AbstractMatheAGObject) pDomainObject }));
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#aufgabeAnlegen()
	 */
	@Override
	public Aufgabe aufgabeAnlegen() throws MatheJungAltException {
		final Aufgabe aufgabe = aufgabenservice.aufgabeAnlegen();
		aufgabe.addPropertyChangeListener(this);
		return aufgabe;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#aufgabeEditierbar(de.egladil.mathejungalt.domain
	 * .aufgaben.Aufgabe)
	 */
	@Override
	public boolean aufgabeEditierbar(final Aufgabe pAufgabe) throws MatheJungAltException {
		return pAufgabe.getAnzahlSperrendeReferenzen() == 0;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#aufgabeZuSchluessel(java.lang.String)
	 */
	@Override
	public Aufgabe aufgabeZuSchluessel(final String pSchluessel) {
		return aufgabenservice.aufgabeZuSchluessel(pSchluessel);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#mcaufgabeZuSchluessel(java.lang.String)
	 */
	@Override
	public MCAufgabe mcaufgabeZuSchluessel(final String pSchluessel) {
		return quizaufgabenservice.aufgabeZuSchluessel(pSchluessel);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#isGesperrt(de.egladil.mathejungalt.domain.aufgaben
	 * .Aufgabe)
	 */
	@Override
	public String isGesperrtFuerWettbewerb(final Aufgabe pAufgabe) {
		if (pAufgabe == null) {
			final String msg = MatheJungAltException.argumentNullMessage("Aufgabe");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		return aufgabenservice.isGesperrtFuerWettbewerb(pAufgabe, stufe6MaximalEinmal, configuration);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#isGesperrtFuerArbeitsblatt(de.egladil.mathejungalt
	 * .domain.aufgaben.Aufgabe)
	 */
	@Override
	public String isGesperrtFuerArbeitsblatt(final Aufgabe pAufgabe) {
		if (pAufgabe == null) {
			final String msg = MatheJungAltException.argumentNullMessage("Aufgabe");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		return aufgabenservice.isGesperrtFuerArbeitsblatt(pAufgabe, configuration);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#aufgabeStatusZuSchlechtFuerSerieAendern(de.egladil
	 * .mathejungalt .domain.aufgaben.Aufgabe)
	 */
	@Override
	public void aufgabeStatusZuSchlechtFuerSerieAendern(final Aufgabe pAufgabe) {
		final String oldValue = pAufgabe.getZuSchlechtFuerSerie();
		aufgabenservice.aufgabeStatusZuSchlechtFuerSerieAendern(pAufgabe);
		notifyModelChangeListeners(new ModelChangeEvent(ModelChangeEvent.PROPERTY_CHANGE_EVENT, pAufgabe, oldValue,
			pAufgabe.getZuSchlechtFuerSerie(), IAufgabeNames.PROP_ZU_SCHLECHT));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#quelleLaden(long)
	 */
	@Override
	public void quelleZuAufgabeLaden(final Aufgabe pAufgabe) throws MatheJungAltException {
		if (pAufgabe == null) {
			final String msg = MatheJungAltException.argumentNullMessage("Aufgabe");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		final AbstractQuelle quelle = pAufgabe.getQuelle();
		if (quelle.isCompletelyLoaded()) {
			return;
		}
		aufgabenservice.quelleNachladen(quelle);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#getQuizaufgaben()
	 */
	@Override
	public Collection<MCAufgabe> getQuizaufgaben() throws MatheJungAltException {
		modelIsAdjusting = true;
		try {
			return quizaufgabenservice.getAufgaben();
		} finally {
			modelIsAdjusting = false;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#quizaufgabeAnlegen()
	 */
	@Override
	public MCAufgabe quizaufgabeAnlegen() throws MatheJungAltException {
		final MCAufgabe aufgabe = quizaufgabenservice.aufgabeAnlegen();
		aufgabe.addPropertyChangeListener(this);
		return aufgabe;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#quizAufgabeSpeichern(de.egladil.mcmatheraetsel.
	 * archivdomain.archiv .MCAufgabe)
	 */
	@Override
	public void quizAufgabeSpeichern(final MCAufgabe pAufgabe) throws MatheJungAltException {
		checkConsistent(pAufgabe);
		quizaufgabenservice.aufgabeSpeichern(pAufgabe, false);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#getRaetsel()
	 */
	@Override
	public Collection<MCArchivraetsel> getRaetsel() throws MatheJungAltException {
		final Collection<MCArchivraetsel> raetsel = raetselservice.getRaetsel();
		return raetsel;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#getSchulen()
	 */
	@Override
	public Collection<Schule> getSchulen() throws MatheJungAltException {
		return schulenservice.getSchulen();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#getLaender()
	 */
	@Override
	public List<Land> getLaender() throws MatheJungAltException {
		return schulenservice.getLaender();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#getKontakte()
	 */
	@Override
	public Collection<Kontakt> getKontakte() throws MatheJungAltException {
		return schulenservice.getKontakte();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#raetselSpeichern(de.egladil.mcmatheraetsel.archivdomain
	 * .archiv .MCArchivraetsel)
	 */
	@Override
	public void raetselSpeichern(final MCArchivraetsel pRaetsel) throws MatheJungAltException {
		if (pRaetsel == null) {
			final String msg = MatheJungAltException.argumentNullMessage("pRaetsel");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		pRaetsel.checkConsistent();
		raetselservice.raetselSpeichern(pRaetsel);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#raetselitemsLaden(de.egladil.mcmatheraetsel.
	 * archivdomain.archiv .MCArchivraetsel)
	 */
	@Override
	public Collection<MCArchivraetselItem> raetselitemsLaden(final MCArchivraetsel pRaetsel) throws MatheJungAltException {
		if (pRaetsel.isItemsLoaded()) {
			return pRaetsel.getItems();
		}
		final Collection<MCArchivraetselItem> raetselitems = raetselservice.raetselitemsLaden(pRaetsel);
		pRaetsel.setItemsLoaded(true);
		return raetselitems;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#aufgabeZuRaetselHinzufuegen(de.egladil.mcmatheraetsel
	 * .archivdomain .archiv.MCArchivraetsel, de.egladil.mcmatheraetsel.archivdomain.archiv.MCAufgabe)
	 */
	@Override
	public MCArchivraetselItem quizaufgabeZuRaetselHinzufuegen(final MCArchivraetsel pRaetsel, final MCAufgabe pAufgabe)
		throws MatheJungAltException {
		if (pRaetsel == null || pAufgabe == null) {
			final String msg = MatheJungAltException.argumentNullMessage(pRaetsel == null ? "pAufgabensammlung" : "pAufgbe");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (!pRaetsel.canAddAufgabe(pAufgabe)) {
			throw new ActionNotPossibleException("Die Aufgabe kann dem Raetsel nicht hinzugefuegt werden.");
		}
		final MCArchivraetselItem item = raetselservice.aufgabeZuRaetselHinzufuegen(pRaetsel, pAufgabe);
		item.addPropertyChangeListener(this);
		return item;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#quizaufgabeAusRaetselEntfernen(de.egladil.
	 * mcmatheraetsel.archivdomain .archiv.MCAufgabe)
	 */
	@Override
	public void quizaufgabeAusRaetselEntfernen(final MCArchivraetselItem pItem) throws MatheJungAltException {
		raetselservice.aufgabeAusRaetselEntfernen(pItem);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#isGesperrtFuerRaetsel(de.egladil.mcmatheraetsel
	 * .archivdomain. archiv.MCAufgabe)
	 */
	@Override
	public String isGesperrtFuerRaetsel(final MCAufgabe pAufgabe) throws MatheJungAltException {
		if (pAufgabe == null) {
			final String msg = MatheJungAltException.argumentNullMessage("MCAufgabe");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		return quizaufgabenservice.isGesperrtFuerRaetsel(pAufgabe);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#raetselAnlegen()
	 */
	@Override
	public MCArchivraetsel raetselAnlegen() throws MatheJungAltException {
		final MCArchivraetsel raetsel = raetselservice.raetselAnlegen();
		raetsel.addPropertyChangeListener(this);
		notifyModelChangeListeners(new ModelChangeEvent(ModelChangeEvent.ADD_EVENT, this, null, raetsel, null));
		return raetsel;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#aufgabenLaden()
	 */
	@Override
	public Collection<Aufgabe> getAufgaben() throws MatheJungAltException {
		modelIsAdjusting = true;
		try {
			return aufgabenservice.getAufgaben();
		} finally {
			modelIsAdjusting = false;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#getSerien()
	 */
	@Override
	public Collection<Serie> getSerien() throws MatheJungAltException {
		modelIsAdjusting = true;
		try {
			return aufgabensammlungservice.getSerien();
		} finally {
			modelIsAdjusting = false;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#getMinikaengurus()
	 */
	@Override
	public Collection<Minikaenguru> getMinikaengurus() throws MatheJungAltException {
		modelIsAdjusting = true;
		try {
			return aufgabensammlungservice.getMinikaengurus();
		} finally {
			modelIsAdjusting = false;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#getArbeitsblaetter()
	 */
	@Override
	public Collection<Arbeitsblatt> getArbeitsblaetter() throws MatheJungAltException {
		modelIsAdjusting = true;
		try {
			return aufgabensammlungservice.getArbeitsblaetter();
		} finally {
			modelIsAdjusting = false;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#aufgabensammlungitemsLaden(de.egladil.mathejungalt
	 * .domain.aufgabensammlungen.IAufgabensammlung)
	 */
	@Override
	public Collection<IAufgabensammlungItem> aufgabensammlungitemsLaden(final IAufgabensammlung pAufgabensammlung)
		throws MatheJungAltException {
		return aufgabensammlungservice.itemsLaden(pAufgabensammlung);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#serieAnlegen()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public IAufgabensammlung aufgabensammlungAnlegen(final Class pClazz) throws MatheJungAltException {
		@SuppressWarnings("unchecked")
		final
		IAufgabensammlung aufgabensammlung = aufgabensammlungservice.aufgabensammlungAnlegen(pClazz);
		if (aufgabensammlung != null) {
			aufgabensammlung.addPropertyChangeListener(this);
		}
		return aufgabensammlung;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @seede.egladil.mathejungalt.service.stammdaten.IStammdatenservice#serieSpeichern(de.egladil.mathejungalt.domain.
	 * aufgabensammlungen.serien.Serie)
	 */
	@Override
	public void aufgabensammlungSpeichern(final IAufgabensammlung pAufgabensammlung) throws MatheJungAltException {
		if (pAufgabensammlung == null) {
			final String msg = MatheJungAltException.argumentNullMessage("IAufgabensammlung");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		checkConsistent(pAufgabensammlung);
		aufgabensammlungservice.aufgabensammlungSpeichern(pAufgabensammlung);
	}

	/**
	 * @param pAufgabensammlung
	 */
	private void checkConsistent(final IAufgabensammlung pAufgabensammlung) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 *
	 * @seede.egladil.mathejungalt.service.stammdaten.IStammdatenservice#numerieren(de.egladil.mathejungalt.domain.
	 * aufgabensammlungen.serien.Serie)
	 */
	@Override
	public void numerieren(final IAufgabensammlung pAufgabensammlung) throws MatheJungAltException {
		if (pAufgabensammlung == null) {
			final String msg = MatheJungAltException.MSG_ARG_NULL;
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		modelIsAdjusting = true;
		try {
			aufgabensammlungservice.itemsNumerieren(pAufgabensammlung);
			aufgabensammlungSpeichern(pAufgabensammlung);
		} finally {
			modelIsAdjusting = false;
			for (final IAufgabensammlungItem item : pAufgabensammlung.getItems()) {
				notifyModelChangeListeners(new ModelChangeEvent(ModelChangeEvent.PROPERTY_CHANGE_EVENT, item, null,
					item.getNummer(), IAufgabensammlungItem.PROP_NUMMER));
			}
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#aufgabeAusSammlungEntfernen(de.egladil.mathejungalt
	 * .domain.aufgabensammlungen.IAufgabensammlungItem)
	 */
	@Override
	public void aufgabeAusSammlungEntfernen(final IAufgabensammlungItem pItem) throws MatheJungAltException {
		if (pItem == null) {
			final String msg = MatheJungAltException.argumentNullMessage("pItem");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (IAufgabensammlung.BEENDET == pItem.getAufgabensammlung().getBeendet()) {
			throw new ActionNotPossibleException(
				"Die Aufgabe aus der Aufgabensammlung nicht entfernt werden (beendet).");
		}
		aufgabensammlungservice.aufgabeAusSammlungEntfernen(pItem);
		pItem.removePropertyChangeListener(this);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#aufgabeZuSerieHinzufuegen(de.egladil.mathejungalt
	 * .domain.aufgabensammlungen.serien.Serie, de.egladil.mathejungalt.domain.aufgaben.Aufgabe)
	 */
	@Override
	public IAufgabensammlungItem aufgabeZuSammlungHinzufuegen(final IAufgabensammlung pAufgabensammlung, final Aufgabe pAufgabe)
		throws MatheJungAltException, ActionNotPossibleException {
		if (pAufgabensammlung == null || pAufgabe == null) {
			final String msg = MatheJungAltException.argumentNullMessage(pAufgabensammlung == null ? "pAufgabensammlung"
				: "pAufgbe");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (!pAufgabensammlung.canAdd(pAufgabe)) {
			throw new ActionNotPossibleException("Die Aufgabe kann der Aufgabensammlung nicht hinzugefuegt werden.");
		}
		final IAufgabensammlungItem item = aufgabensammlungservice.aufgabeZuSammlungHinzufuegen(pAufgabensammlung, pAufgabe);
		item.addPropertyChangeListener(this);
		return item;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#getSerien(int)
	 */
	@Override
	public List<Serie> getSerien(final int pRunde) {
		modelIsAdjusting = true;
		try {
			return aufgabensammlungservice.getSerien(pRunde);
		} finally {
			modelIsAdjusting = false;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#aktuelleRunde()
	 */
	@Override
	public int aktuelleRunde() throws MatheJungAltException {
		return aufgabensammlungservice.aktuelleRunde();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#aufgabeSpeichern(de.egladil.mathejungalt.domain
	 * .aufgaben.Aufgabe)
	 */
	@Override
	public void aufgabeSpeichern(final Aufgabe pAufgabe) throws MatheJungAltException {
		checkConsistent(pAufgabe);
		aufgabenservice.aufgabeSpeichern(pAufgabe);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#autorAnlegen()
	 */
	@Override
	public Autor autorAnlegen() throws MatheJungAltException {
		return medienservice.autorAnlegen();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#autorenLaden()
	 */
	@Override
	public Collection<Autor> getAutoren() throws MatheJungAltException {
		modelIsAdjusting = true;
		try {
			return medienservice.getAutoren();
		} finally {
			modelIsAdjusting = false;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#autorSpeichern(de.egladil.mathejungalt.domain.medien
	 * .Autor)
	 */
	@Override
	public void autorSpeichern(final Autor pAutor) throws MatheJungAltException {
		checkConsistent(pAutor);
		medienservice.autorSpeichern(pAutor);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#buchAnlegen()
	 */
	@Override
	public Buch buchAnlegen() throws MatheJungAltException {
		return medienservice.buchAnlegen();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#buchquelleFindenOderAnlegen(de.egladil.mathejungalt
	 * .domain.medien.Buch, int)
	 */
	@Override
	public Buchquelle buchquelleFindenOderAnlegen(final Buch pBuch, final int pSeite) throws MatheJungAltException {
		return aufgabenservice.buchquelleFindenOderAnlegen(pBuch, pSeite);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#emailadressenLaden()
	 */
	@Override
	public Collection<String> getEmailadressen() throws MatheJungAltException {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#kindquelleFindenOderAnlegen(de.egladil.mathejungalt
	 * .domain.mitglieder.Mitglied, int, int)
	 */
	@Override
	public Kindquelle kindquelleFindenOderAnlegen(final Mitglied pMitglied, final int pJahre, final int pKlasse)
		throws MatheJungAltException {
		return aufgabenservice.kindquelleFindenOderAnlegen(pMitglied, pJahre, pKlasse);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#gruppenquelleAnlegen(java.util.Collection)
	 */
	@Override
	public MitgliedergruppenQuelle gruppenquelleAnlegen(final Collection<Mitglied> pMitglieder) throws MatheJungAltException {
		if (pMitglieder == null || pMitglieder.size() < 2) {
			throw new MatheJungAltException("Gruppenquellen dürfen nur zu mindestens 2 Mitgliedern angelegt werden");
		}
		return aufgabenservice.gruppenquelleAnlegen(pMitglieder);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#mailadresseAusMailinglisteEntfernen(java.lang.String
	 * )
	 */
	@Override
	public void mailadresseAusMailinglisteEntfernen(final String pAdresse) throws MatheJungAltException {
		mitgliederservice.mailadresseAusMailinglisteEntfernen(pAdresse);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#mailadresseZurMailinglisteHinzufuegen(java.lang
	 * .String)
	 */
	@Override
	public void mailadresseZurMailinglisteHinzufuegen(final String pAdresse) throws MatheJungAltException {
		mitgliederservice.mailadresseZurMailinglisteHinzufuegen(pAdresse);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#mailadresseInMailinglisteEnthalten(java.lang.String
	 * )
	 */
	@Override
	public boolean mailadresseInMailinglisteEnthalten(final String pAdresse) throws MatheJungAltException {
		return mitgliederservice.mailadresseInMailinglisteEnthalten(pAdresse);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#medienLaden()
	 */
	@Override
	public Collection<AbstractPrintmedium> getMedien() {
		modelIsAdjusting = true;
		try {
			return medienservice.getMedien();
		} finally {
			modelIsAdjusting = false;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#mediumSpeichern(de.egladil.mathejungalt.domain.
	 * medien.AbstractPrintmedium)
	 */
	@Override
	public void mediumSpeichern(final AbstractPrintmedium pMedium) {
		checkConsistent(pMedium);
		medienservice.mediumSpeichern(pMedium);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#mitgliederLaden()
	 */
	@Override
	public Collection<Mitglied> getMitglieder() {
		modelIsAdjusting = true;
		try {
			return mitgliederservice.getMitglieder();
		} finally {
			modelIsAdjusting = false;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#findMitgliederByKlasse(int)
	 */
	@Override
	public List<Mitglied> findMitgliederByKlasse(final int pKlasse, final boolean pAktiv) {
		return mitgliederservice.findByKlasse(pKlasse, true);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#findMitgliederMitFruehstarterInRunde(int)
	 */
	@Override
	public List<Mitglied> findMitgliederMitFruehstarterInRunde(final int pRunde) throws MatheJungAltException {
		return mitgliederservice.findMitgliederMitFruehstarterInRunde(pRunde);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#serienteilnahmenZuMitgliedLaden(de.egladil.mathejungalt
	 * .domain.mitglieder.Mitglied)
	 */
	@Override
	public Collection<Serienteilnahme> serienteilnahmenZuMitgliedLaden(final Mitglied pMitglied) throws MatheJungAltException {
		if (pMitglied == null) {
			final String msg = MatheJungAltException.argumentNullMessage("Mitglied");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		return mitgliederservice.serienteilnahmenZuMitgliedLaden(pMitglied);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#diplomeZuMitgliedLaden(de.egladil.mathejungalt.
	 * domain.mitglieder.Mitglied)
	 */
	@Override
	public Collection<Diplom> diplomeZuMitgliedLaden(final Mitglied pMitglied) throws MatheJungAltException {
		if (pMitglied == null) {
			final String msg = MatheJungAltException.argumentNullMessage("Mitglied");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		return mitgliederservice.diplomeZuMitgliedLaden(pMitglied);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#removeTeilnahme(de.egladil.mathejungalt.domain.
	 * mitglieder.Mitglied, de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie)
	 */
	@Override
	public void removeTeilnahme(final Mitglied pMitglied, final Serie pSerie) {
		final String msg = checkMitgliedSerieParams(pMitglied, pSerie);
		if (msg.length() > 0) {
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}

		mitgliederservice.serienteilnahmeLoeschen(pMitglied, pSerie);
	}

	/**
	 * @param pMitglied
	 * @param pSerie
	 * @return
	 */
	private String checkMitgliedSerieParams(final Mitglied pMitglied, final Serie pSerie) {
		String msg = "";
		if (pMitglied == null) {
			msg = MatheJungAltException.argumentNullMessage("Mitglied");
		}
		if (pSerie == null) {
			msg += ", " + MatheJungAltException.argumentNullMessage("Serie");
		}
		return msg;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#mitgliedNimmtTeil(de.egladil.mathejungalt.domain
	 * .mitglieder.Mitglied, de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie)
	 */
	@Override
	public void mitgliedNimmtTeil(final Mitglied pMitglied, final Serie pSerie) throws MatheJungAltException {
		final String msg = checkMitgliedSerieParams(pMitglied, pSerie);
		if (msg.length() > 0) {
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		mitgliederservice.mitgliedNimmtTeil(pMitglied, pSerie);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#mitgliedBrauchtDiplom(de.egladil.mathejungalt.domain
	 * .mitglieder.Mitglied)
	 */
	@Override
	public boolean mitgliedBrauchtDiplom(final Mitglied pMitglied) throws MatheJungAltException {
		return mitgliederservice.mitgliedBrauchtDiplom(pMitglied);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#diplomGeben(de.egladil.mathejungalt.domain.mitglieder
	 * .Mitglied)
	 */
	@Override
	public Diplom diplomGeben(final Mitglied pMitglied) throws MatheJungAltException {
		return mitgliederservice.diplomGeben(pMitglied);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#mitgliedAnlegen()
	 */
	@Override
	public Mitglied mitgliedAnlegen() throws MatheJungAltException {
		final Mitglied mitglied = mitgliederservice.mitgliedAnlegen();
		mitglied.addPropertyChangeListener(this);
		return mitglied;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#findSerienteilnahme(de.egladil.mathejungalt.domain
	 * .mitglieder.Mitglied, de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie)
	 */
	@Override
	public Serienteilnahme findSerienteilnahme(final Mitglied pMitglied, final Serie pSerie) throws MatheJungAltException {
		final String msg = checkMitgliedSerieParams(pMitglied, pSerie);
		if (msg.length() > 0) {
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		return mitgliederservice.serienteilnahmeFinden(pMitglied, pSerie);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#mitgliedSpeichern(de.egladil.mathejungalt.domain
	 * .mitglieder.Mitglied)
	 */
	@Override
	public void mitgliedSpeichern(final Mitglied pMitglied) throws MatheJungAltException {
		checkConsistent(pMitglied);
		mitgliederservice.mitgliedSpeichern(pMitglied);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#mitgliedAktivierungAendern(de.egladil.mathejungalt
	 * .domain.mitglieder .Mitglied)
	 */
	@Override
	public void mitgliedAktivierungAendern(final Mitglied pMitglied) throws MatheJungAltException {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#nullquelleLaden()
	 */
	@Override
	public NullQuelle getNullQuelle() throws MatheJungAltException {
		return aufgabenservice.getNullQuelle();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#zeitschriftAnlegen()
	 */
	@Override
	public Zeitschrift zeitschriftAnlegen() throws MatheJungAltException {
		return medienservice.zeitschriftAnlegen();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @seede.egladil.mathejungalt.service.stammdaten.IStammdatenservice#zeitschriftquelleFindenOderAnlegen(de.egladil.
	 * mathejungalt.domain.medien.Zeitschrift, int, int)
	 */
	@Override
	public Zeitschriftquelle zeitschriftquelleFindenOderAnlegen(final Zeitschrift pZeitschrift, final int pAusgabe, final int pJahrgang)
		throws MatheJungAltException {
		return aufgabenservice.zeitschriftquelleFindenOderAnlegen(pZeitschrift, pAusgabe, pJahrgang);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#refreshAufgaben()
	 */
	@Override
	public void aufgabenSynchronisieren() throws MatheJungAltException {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#refreshAutoren()
	 */
	@Override
	public void autorenSynchronisieren() throws MatheJungAltException {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#refreshMedien()
	 */
	@Override
	public void medienSynchronisieren() throws MatheJungAltException {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#refreshMitglieder()
	 */
	@Override
	public void mitgliederSynchronisieren() throws MatheJungAltException {
		// TODO Auto-generated method stub

	}

	/**
	 * @param pAufgabenservice the aufgabenservice to set
	 */
	public void setAufgabenservice(final Aufgabenservice pAufgabenservice) {
		aufgabenservice = pAufgabenservice;
		aufgabenservice.addModelChangeListener(this);
	}

	/**
	 * @param pMedienservice the medienservice to set
	 */
	public void setMedienservice(final Medienservice pMedienservice) {
		medienservice = pMedienservice;
	}

	/**
	 * @param pMitgliederservice the mitgliederservice to set
	 */
	public void setMitgliederservice(final Mitgliederservice pMitgliederservice) {
		mitgliederservice = pMitgliederservice;
		mitgliederservice.addModelChangeListener(this);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#addModelChangeListener(de.egladil.mathejungalt.
	 * service.stammdaten.event.IModelChangeListener)
	 */
	@Override
	public void addModelChangeListener(final IModelChangeListener pListener) {
		if (pListener != null && !modelChangeListeners.contains(pListener)) {
			modelChangeListeners.add(pListener);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#removeModelChangeListener(de.egladil.mathejungalt
	 * .service.stammdaten.event.IModelChangeListener)
	 */
	@Override
	public void removeModelChangeListener(final IModelChangeListener pListener) {
		modelChangeListeners.remove(pListener);
	}

	/**
	 * @param pSerienservice the serienservice to set
	 */
	public void setAufgabensammlungservice(final Aufgabensammlungservice pSerienservice) {
		aufgabensammlungservice = pSerienservice;
		aufgabensammlungservice.addModelChangeListener(this);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @seede.egladil.mathejungalt.service.stammdaten.IStammdatenservice#canGenerate(de.egladil.mathejungalt.domain.
	 * AbstractMatheAGObject)
	 */
	@Override
	public boolean canGenerate(final AbstractMatheAGObject pObject) throws MatheJungAltException,
		MatheAGObjectNotGeneratableException {
		if (pObject == null) {
			final String msg = MatheJungAltException.argumentNullMessage("pObject");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (pObject instanceof IAufgabensammlung) {
			return aufgabensammlungservice.canGenerate((IAufgabensammlung) pObject);
		}
		if (pObject instanceof Mitglied) {
			return mitgliederservice.canGenerate();
		}
		if (pObject instanceof MCArchivraetsel) {
			return ((MCArchivraetsel) pObject).canGenerate();
		}
		if (pObject instanceof MCAufgabe) {
			return ((MCAufgabe) pObject).canGenerate();
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#generate(java.lang.Class, java.lang.String)
	 */
	@Override
	public void generate(final String pClassName, final String pPath) throws MatheJungAltException {
		String msg = "Fehler beim Generieren: ";
		if (pClassName == null) {
			msg += MatheJungAltException.argumentNullMessage("Class");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (pPath == null || !new File(pPath).exists()) {
			msg += "pPath ist null oder kein gueltiges Verzeichnis";
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (Mitglied.class.equals(pClassName)) {
			mitgliederservice.generate(pPath, this);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#generate(java.lang.Class, java.util.Map,
	 * org.eclipse.core.runtime.IProgressMonitor)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> IStatus generate(final String pTypeName, final Map<String, T> pObjects, final String pathWorkDir,
		final RaetsellisteGeneratorModus pModus, final IProgressMonitor pProgressMonitor) {
		Assert.isNotNull(pTypeName, "pTypeName");
		Assert.isNotNull(pObjects, "pObjects");
		Assert.isNotNull(pathWorkDir, "pathWorkDir");
		Assert.isNotNull(pModus, "pModus");

		LOG.info("generieren nach {}", pathWorkDir);

		if (Aufgabe.class.getName().equals(pTypeName)) {
			return aufgabenservice.generateLaTeXAndTransformToPs((Map<String, Aufgabe>) pObjects, pathWorkDir, this,
				pProgressMonitor);
		}
		if (MCAufgabe.class.getName().equals(pTypeName)) {
			return quizaufgabenservice.generateLaTeXAndTransformToPs((Map<String, MCAufgabe>) pObjects, pathWorkDir,
				pProgressMonitor);
		}
		if (Mitglied.class.getName().equals(pTypeName)) {
			mitgliederservice.generate(pathWorkDir, this);
			return new Status(IStatus.OK, PLUGIN_ID, "Mitgliederseiten wurden generiiert");
		}
		if (MCArchivraetsel.class.getName().equals(pTypeName)) {
			return raetselservice.generateLaTeXAndTransformToPs((Map<String, MCArchivraetsel>) pObjects, pathWorkDir,
				pModus, this, pProgressMonitor);
		}
		throw new EgladilBusinessException("kein Generator fuer " + pTypeName + " implementiert");
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#generate(de.egladil.mathejungalt.domain.
	 * aufgabensammlungen. IAufgabensammlung, org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public void generate(final IAufgabensammlung pAufgabensammlung, final IProgressMonitor pProgressMonitor)
		throws MatheJungAltException {
		final String workDir = configuration.getConfigValue(IMatheJungAltConfiguration.KEY_EXTERNAL_PROGRAMS_TEMPDIR);
		String msg = "Fehler beim Generieren: ";
		if (pAufgabensammlung == null) {
			msg += "pAufgabensammlung ist null.";
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		aufgabensammlungservice.generateAll(workDir, pAufgabensammlung, this, pProgressMonitor);

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#generateMatheAGArchivseite(java.lang.String)
	 */
	@Override
	public void generateMatheAGArchivseite(final String pPath) {
		String msg = "Fehler beim Generieren: ";
		if (pPath == null || !new File(pPath).exists()) {
			msg += "pPath ist null oder kein gueltiges Verzeichnis";
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		aufgabensammlungservice.generateArchiv(pPath, this);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#generateRaetselliste(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public IStatus generateRaetselliste(final int pVersion, final String pPath, final boolean pPrivat, final RaetsellisteGeneratorModus pModus) {
		Assert.isTrue(pPath != null && !pPath.isEmpty(), "pPath darf nicht null oder leer sein");
		Assert.isTrue(pModus != null, "pModus darf nicht null");
		final IStatus status = raetselservice.generateRaetselliste(pVersion, pPath, pPrivat, pModus, this);
		return status;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.event.IModelChangeListener#modelChanged(de.egladil.mathejungalt.service
	 * .stammdaten.event.ModelChangeEvent)
	 */
	@Override
	public void modelChanged(final ModelChangeEvent pEvt) {
		notifyModelChangeListeners(pEvt);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(final PropertyChangeEvent pEvt) {
		if (modelIsAdjusting) {
			return;
		}
		if (pEvt.getOldValue() == null && pEvt.getNewValue() != null || pEvt.getOldValue() != null
			&& pEvt.getNewValue() == null || !pEvt.getOldValue().equals(pEvt.getNewValue())) {
			notifyModelChangeListeners(new ModelChangeEvent(ModelChangeEvent.PROPERTY_CHANGE_EVENT, pEvt.getSource(),
				pEvt.getOldValue(), pEvt.getNewValue(), pEvt.getPropertyName()));
		}
	}

	/**
	 * Reicht das Event an die Listener durch.
	 *
	 * @param pEvt
	 */
	private void notifyModelChangeListeners(final ModelChangeEvent pEvt) {
		if (modelIsAdjusting) {
			return;
		}
		for (final IModelChangeListener listener : modelChangeListeners) {
			listener.modelChanged(pEvt);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#getPersistenceLocation(java.lang.String)
	 */
	@Override
	public String getPersistenceLocation(final String pProtokoll) {
		return persistenceservice.getPersistenceLocation();
	}

	/**
	 * @param pPersistenceservice the persistenceservice to set
	 */
	public void setPersistenceservice(final IPersistenceservice pPersistenceservice) {
		persistenceservice = pPersistenceservice;

		aufgabensammlungservice.setPersistenceservice(persistenceservice);
		aufgabenservice.setPersistenceservice(persistenceservice);
		raetselservice.setPersistenceservice(persistenceservice);
		mitgliederservice.setPersistenceservice(persistenceservice);
		quizaufgabenservice.setPersistenceservice(persistenceservice);
		schulenservice.setPersistenceservice(persistenceservice);
		medienservice.setPersistenceservice(persistenceservice);

	}

	/**
	 * @param pPersistenceservice the persistenceservice to set
	 */
	public void unsetPersistenceservice(final IPersistenceservice pPersistenceservice) {
		if (persistenceservice != null) {
			persistenceservice.shutDown();
		}
		persistenceservice = null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#getArbeitsblattItemNummerComparator()
	 */
	@Override
	public Comparator<IAufgabensammlungItem> getArbeitsblattItemNummerComparator() {
		return new ArbblattItemNummerComparator();
	}

	/**
	 * @return the configuration
	 */
	@Override
	public final IMatheJungAltConfiguration getConfiguration() {
		return configuration;
	}

	/**
	 * used by spring
	 *
	 * @param pConfiguration the configuration to set
	 */
	public final void setConfiguration(final IMatheJungAltConfiguration pConfiguration) {
		configuration = pConfiguration;
		if (LOG.isDebugEnabled()) {
			LOG.debug(configuration.getConfigValue(IMatheJungAltConfiguration.KEY_EXTERNAL_PROGRAMS_TEMPDIR));
		}
		aufgabensammlungservice.setConfiguration(pConfiguration);
		mitgliederservice.setConfiguration(pConfiguration);
	}

	/**
	 * used by spring
	 *
	 * @param pConfiguration the configuration to set
	 */
	public final void unsetConfiguration(final IMatheJungAltConfiguration pConfiguration) {
		configuration = null;
	}

	/**
	 * @param pRaetselservice the raetselservice to set
	 */
	public final void setRaetselservice(final Raetselservice pRaetselservice) {
		raetselservice = pRaetselservice;
		raetselservice.addModelChangeListener(this);
	}

	/**
	 * @param pQuizaufgabenservice the quizaufgabenservice to set
	 */
	public final void setQuizaufgabenservice(final Quizaufgabenservice pQuizaufgabenservice) {
		quizaufgabenservice = pQuizaufgabenservice;
		quizaufgabenservice.addModelChangeListener(this);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#getQuizAutoren()
	 */
	@Override
	public Collection<Urheber> getQuizAutoren() {
		return raetselservice.getRaetselautoren();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#publish(de.egladil.mcmatheraetsel.archivdomain.
	 * archiv.MCArchivraetsel )
	 */
	@Override
	public void publish(final MCArchivraetsel pRaetsel) {
		raetselservice.publish(pRaetsel);
		notifyModelChangeListeners(new ModelChangeEvent(ModelChangeEvent.PROPERTY_CHANGE_EVENT, pRaetsel, null,
			pRaetsel.isVeroeffentlicht(), IMCArchivraetselNames.PROP_VEROEFFENTLICHT));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#kaenguruAufgabentemplatesAnlegen(java.lang.String,
	 * int, de.egladil.mathejungalt.domain.types.MCAufgabenstufen)
	 */
	@Override
	public List<MCAufgabe> kaenguruAufgabentemplatesAnlegen(final String pSchluesselPrefix, final int pAnzahl,
		final MCAufgabenstufen pStufe) {
		final List<MCAufgabe> result = quizaufgabenservice.kaenguruAufgabentemplatesAnlegen(pSchluesselPrefix, pAnzahl,
			pStufe);
		for (final MCAufgabe aufgabe : result) {
			aufgabe.addPropertyChangeListener(this);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#setStufe6MaximalEinmal(boolean)
	 */
	@Override
	public void setStufe6MaximalEinmal(final boolean pJa) {
		if (stufe6MaximalEinmal == null) {
			stufe6MaximalEinmal = Boolean.valueOf(pJa);
		}
		if (stufe6MaximalEinmal != pJa) {
			final List<Aufgabe> aufgaben = aufgabenservice.aufgabenMitZweckUndStufe(Aufgabenzweck.S, 6);
			for (final Aufgabe aufgabe : aufgaben) {
				aufgabe.setGesperrtFuerWettbewerb(null);
				aufgabenservice.isGesperrtFuerWettbewerb(aufgabe, pJa, configuration);
			}
		}
		stufe6MaximalEinmal = pJa;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#schuleAnlegen()
	 */
	@Override
	public Schule schuleAnlegen() {
		final Schule schule = schulenservice.schuleAnlegen();
		schule.addPropertyChangeListener(this);
		return schule;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#kontaktAnlegen()
	 */
	@Override
	public Kontakt kontaktAnlegen() {
		final Kontakt kontakt = schulenservice.kontaktAnlegen();
		kontakt.addPropertyChangeListener(this);
		return kontakt;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#removeMinikaenguruTeilnahme(de.egladil.mathejungalt
	 * .domain.schulen .Schule, de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru)
	 */
	@Override
	public void minikaenguruTeilnahmeLoeschen(final Schule pSchule, final Minikaenguru pMinikaenguru) {
		schulenservice.minikaenguruteilnahmeLoeschen(pSchule, pMinikaenguru);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#schuleNimmtTeil(de.egladil.mathejungalt.domain.
	 * schulen.Schule, de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru)
	 */
	@Override
	public MinikaenguruTeilnahme schuleNimmtTeil(final Schule pSchule, final Minikaenguru pMinikaenguru) {
		return schulenservice.schuleNimmtTeil(pSchule, pMinikaenguru);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#minikaenguruTeilnahmenLaden(de.egladil.mathejungalt
	 * .domain.schulen .Schule)
	 */
	@Override
	public Collection<MinikaenguruTeilnahme> minikaenguruTeilnahmenLaden(final Schule pSchule) {
		return schulenservice.minikaenguruTeilnahmenLaden(pSchule);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#removeSchulkontakt(de.egladil.mathejungalt.domain
	 * .schulen.Kontakt , de.egladil.mathejungalt.domain.schulen.Schule)
	 */
	@Override
	public void schulkontaktLoschen(final Kontakt pKontakt, final Schule pSchule) {
		schulenservice.schulkontaktLoeschen(pKontakt, pSchule);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#kontaktZuSchuleHinzufuegen(de.egladil.mathejungalt
	 * .domain.schulen .Kontakt, de.egladil.mathejungalt.domain.schulen.Schule)
	 */
	@Override
	public Schulkontakt kontaktZuSchuleHinzufuegen(final Kontakt pKontakt, final Schule pSchule) {
		return schulenservice.kontaktZuSchuleHinzufuegen(pKontakt, pSchule);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#schulkontakteLaden(de.egladil.mathejungalt.domain
	 * .schulen.Kontakt )
	 */
	@Override
	public Collection<Schulkontakt> schulkontakteLaden(final Kontakt pKontakt) {
		return schulenservice.schulkontakteLaden(pKontakt);
	}

	/**
	 * @param pSchulenservice the schulenservice to set
	 */
	public final void setSchulenservice(final SchulenService pSchulenservice) {
		schulenservice = pSchulenservice;
		schulenservice.addModelChangeListener(this);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#schuleSpeichern(de.egladil.mathejungalt.domain.
	 * schulen.Schule)
	 */
	@Override
	public void schuleSpeichern(final Schule pSchule) {
		checkConsistent(pSchule);
		schulenservice.schuleSpeichern(pSchule);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#kontaktSpeichern(de.egladil.mathejungalt.domain
	 * .schulen.Kontakt)
	 */
	@Override
	public void kontaktSpeichern(final Kontakt pKontakt) {
		checkConsistent(pKontakt);
		schulenservice.kontaktSpeichern(pKontakt);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#mailinglisteLaden(de.egladil.mathejungalt.domain
	 * .aufgabensammlungen .minikaenguru.Minikaenguru)
	 */
	@Override
	public void mailinglisteLaden(final Minikaenguru pMinikaenguru) {
		aufgabensammlungservice.mailinglisteLaden(pMinikaenguru);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#getKontakteMinikaenguru(de.egladil.mathejungalt
	 * .domain.schulen .MinikaenguruTeilnahme)
	 */
	@Override
	public List<Kontakt> getKontakteMinikaenguru(final MinikaenguruTeilnahme teilnahme) {
		return schulenservice.getKontakteMinikaenguru(teilnahme, aufgabensammlungservice);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#findKontakteZuSchule(de.egladil.mathejungalt.domain
	 * .schulen.Schule )
	 */
	@Override
	public List<Kontakt> findKontakteZuSchule(final Schule pSchule) {
		return schulenservice.findKontakteZuSchule(pSchule);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#mailinglisteAktualisieren(de.egladil.mathejungalt
	 * .domain. aufgabensammlungen.minikaenguru.Minikaenguru, de.egladil.mathejungalt.domain.schulen.Schule,
	 * java.util.List)
	 */
	@Override
	public void mailinglisteAktualisieren(final MinikaenguruTeilnahme pTeilnahme, final List<Kontakt> pFilteredKontakte) {
		schulenservice.mailinglisteAktualisieren(pTeilnahme, pFilteredKontakte);
		pTeilnahme.getMinikaenguru().clearMailingliste();
		mailinglisteLaden(pTeilnahme.getMinikaenguru());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#prepareHtlatex(de.egladil.mathejungalt.domain.mcraetsel
	 * . MCArchivraetsel, java.lang.String, org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public IStatus prepareHtlatex(final MCArchivraetsel raetsel, final String pathWorkingDir, final IProgressMonitor pMonitor) {
		final IStatus status = androidAndJSQuizService.prepareHtlatex(raetsel, pathWorkingDir, pMonitor);
		return status;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#canRunPostHtlatexProcessor(de.egladil.mathejungalt
	 * .domain.mcraetsel .MCArchivraetsel, java.lang.String)
	 */
	@Override
	public boolean canRunPostHtlatexProcessor(final MCArchivraetsel raetsel, final String pathWorkDir) {
		return androidAndJSQuizService.canRunPostHtlatexProcessor(raetsel, pathWorkDir);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#processPostHtlatexTasks(de.egladil.mathejungalt
	 * .domain.mcraetsel .MCArchivraetsel, java.lang.String,
	 * de.egladil.mathejungalt.config.IMatheJungAltConfiguration.RaetsellisteGeneratorModus,
	 * org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public IStatus processPostHtlatexTasks(final MCArchivraetsel raetsel, final String pathWorkDir,
		final RaetsellisteGeneratorModus modus, final IProgressMonitor pMonitor) {
		if (RaetsellisteGeneratorModus.ANDROID == modus && !raetsel.isItemsLoaded()) {
			persistenceservice.getRaetselRepository().loadItems(raetsel);
		}
		return androidAndJSQuizService.processPostHtlatexTasks(raetsel, pathWorkDir, modus, pMonitor);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#canPrepareHtlatex(de.egladil.mathejungalt.domain
	 * .mcraetsel. MCArchivraetsel)
	 */
	@Override
	public boolean canPrepareHtlatex(final MCArchivraetsel raetsel, final String pathWorkDir) {
		if (!this.canGenerate(raetsel)) {
			return false;
		}
		return androidAndJSQuizService.canPrepareHtlatex(raetsel, pathWorkDir);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.IStammdatenservice#findLandByLabel(java.lang.String)
	 */
	@Override
	public Land findLandByLabel(final String label) {
		for (final Land l : this.schulenservice.getLaender()) {
			if (l.getLabel().equals(label)) {
				return l;
			}
		}
		return null;
	}
}
