/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.service.stammdaten.quiz;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.EgladilServerException;
import de.egladil.mathejungalt.config.IMatheJungAltConfiguration.RaetsellisteGeneratorModus;
import de.egladil.mathejungalt.domain.mcraetsel.MCAntwortbuchstaben;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;

/**
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class HtlatexAufgabePostProcessor {

	private static final Logger LOG = LoggerFactory.getLogger(HtlatexAufgabePostProcessor.class);

	private Pattern patternImage;

	private final HtmlAndroidImageTagBuilder imageTagBuilder;

	/**
   *
   */
	public HtlatexAufgabePostProcessor() {
		patternImage = Pattern.compile("aufgabe\\dx\\.png");
		imageTagBuilder = new HtmlAndroidImageTagBuilder();
	}

	/**
	 * @param in
	 * @param item
	 * @param modus
	 * @param fileName
	 * @param pathWorkDir
	 */
	public String process(InputStream in, MCArchivraetselItem item, RaetsellisteGeneratorModus modus) {
		try {
			@SuppressWarnings("unchecked")
			List<String> lines = IOUtils.readLines(in, "iso-8859-1");

			StringBuffer sb = new StringBuffer();
			sb.append("<!DOCTYPE html>");
			for (String line : lines) {
				line = HtmlUtils.convertToHtmlEntities(line);
				if (StringUtils.contains(line, "<title>")) {
					sb.append("<head>\n");
					if (RaetsellisteGeneratorModus.ANDROID == modus) {
						sb.append("<script src=\"file:///android_asset/files/js/aufgabe.js\" type=\"text/javascript\"></script>");
					} else {
						sb.append("<script src=\"../../js/aufgabe.js\" type=\"text/javascript\"></script>");
					}
					continue;
				}
				if (StringUtils.contains(line, "<meta name=")) {
					continue;
				}

				if (StringUtils.startsWith(line, "<link rel=")) {
					if (RaetsellisteGeneratorModus.ANDROID == modus) {
						sb.append("<link rel=\"stylesheet\" type=\"text/css\"  href=\"file:///android_asset/files/css/aufgabe.css\">\n");
					} else {
						sb.append("<link rel=\"stylesheet\" type=\"text/css\"  href=\"../../css/aufgabe.css\">\n");
					}
					continue;
				}
				Matcher matcher = patternImage.matcher(line);
				if (RaetsellisteGeneratorModus.ANDROID == modus && matcher.find()) {
					line = imageTagBuilder.transform(line, item);
				}
				if (StringUtils.contains(line, "</html>")) {
					String antwortteil = buildAntwortteil(item);
					sb.append(antwortteil);
					sb.append("\n");
				}
				sb.append(line);
				sb.append("\n");
			}
			String result = sb.toString();
			if (LOG.isDebugEnabled()) {
				LOG.debug("\n\n" + result);
			}
			return result;
		} catch (IOException e) {
			throw new EgladilServerException(e.getMessage(), e);
		}
	}

	private String buildAntwortteil(MCArchivraetselItem item) {
		StringBuffer sb = new StringBuffer();
		sb.append("<!-- jetzt kommt der Antwortteil -->\n<br><br>\n<div class=\"answers\">\n<!-- name-Attribut sorgt dafuer, dass die Buttons als RadioGroup agieren -->\n");

		MCAufgabe aufgabe = item.getAufgabe();
		int anz = aufgabe.getAnzahlAntwortvorschlaege();
		for (int i = 0; i < anz; i++) {
			MCAntwortbuchstaben antwortBuchstabe = MCAntwortbuchstaben.valueOfIndex(i);
			sb.append("<input type=\"radio\" id=\"r");
			sb.append(i);
			sb.append("\" name=\"q0\" value=\"");
			sb.append(antwortBuchstabe.toString());
			sb.append("\" onClick=\"answer('");
			sb.append(antwortBuchstabe.toString());
			sb.append("');\"><label for=\"r");
			sb.append(i);
			sb.append("\">");
			switch (antwortBuchstabe) {
			case A:
				sb.append(aufgabe.getAntwortA());
				break;
			case B:
				sb.append(aufgabe.getAntwortB());
				break;
			case C:
				sb.append(aufgabe.getAntwortC());
				break;
			case D:
				sb.append(aufgabe.getAntwortD());
				break;
			case E:
				sb.append(aufgabe.getAntwortE());
				break;
			default:
				break;
			}
			sb.append("</label>\n");
		}

		sb.append("</div>\n<br><br>\n");
		sb.append("<input id=\"skipbutton\" type=\"submit\" onclick=\"skip()\" value=\"weglassen\" />\n");
		sb.append("<input id=\"gobutton\" type=\"submit\" onclick=\"proceed()\" value=\"weiter\" />\n");
		return sb.toString();

	}

}
