/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.contentprovider;

import java.util.ArrayList;
import java.util.List;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * Der Inhalt einer Zeile besteht aus einem {@link SerienTableRowContent}
 *
 * @author Winkelv
 */
public class HtmlAufgabenTableContentProvider extends TableContentProvider {

	/** */
	private Serie serie;

	/**
	 *
	 */
	public HtmlAufgabenTableContentProvider(Serie pSerie) {
		super();
		serie = pSerie;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.serviceimpl.generatoren.html.contentprovider.TableContentProvider#getContent()
	 */
	@Override
	public List<Object> getContent(IStammdatenservice stammdatenservice) {
		if (serie == null)
			throw new MatheJungAltException("Die Serie ist noch nicht gesetzt!");
		List<Object> liste = new ArrayList<Object>();

		for (int i = 0; i < 6; i++) {
			liste.add(new SerienTableRowContent(serie, true, true, true));
		}
		liste.add(new SerienTableRowContent(serie, false, true, true));
		liste.add(new SerienTableRowContent(serie, false, true, true));
		return liste;
	}
}
