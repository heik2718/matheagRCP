/**
 * 
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.latex;

/**
 * Generiert die Tabelle mit 5 Buchstaben zum Ankreuzen.
 * 
 * @author aheike
 */
public class MinikaenguruAnkreuztabelleProvider {

	/**
	 * 
	 */
	public MinikaenguruAnkreuztabelleProvider() {
	}

	/**
	 * @return
	 */
	public String getText() {
		StringBuffer sb = new StringBuffer();
		sb.append("\\begin{center}\n\\begin{tabular}{*{5}{|c}|}\n\\hline\n");
		sb.append("A & B & C & D & E\\\\\\hline\n");
		for (int i = 0; i < 4; i++) {
			sb.append("\\rule[-0.1ex]{0em}{0.1ex}\\rule{2em}{0ex} &\n");
		}
		sb.append("\\rule[-0.1ex]{0em}{0.1ex}\\rule{2em}{0ex}\\\\\n");
		sb.append("\\hline\n");
		sb.append("\\end{tabular}\n\\end{center}\n");
		sb.append("\\vspace{2cm}\n");
		return sb.toString();
	}
}
