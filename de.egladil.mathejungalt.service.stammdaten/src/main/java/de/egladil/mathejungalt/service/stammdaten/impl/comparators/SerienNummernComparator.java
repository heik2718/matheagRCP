/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.comparators;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.DomainObjectProxyClassifier;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;

/**
 * Sortiert die Serien nach Nummern beginnend mit der kleinsten
 *
 * @author Winkelv
 */
public class SerienNummernComparator extends AbstractMatheAGObjectComparator {

	/**
	 *
	 */
	public SerienNummernComparator() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.serviceimpl.internal.comparators.AbstractDomainObjectComparator#specialCheckType(de.egladil
	 * .mathe.core .domain.AbstractMatheAGObject, de.egladil.mathe.core.domain.AbstractMatheAGObject)
	 */
	@Override
	protected void specialCheckType(AbstractMatheAGObject pO1, AbstractMatheAGObject pO2) {
		if (!DomainObjectProxyClassifier.isSerie(pO1) && !DomainObjectProxyClassifier.isSerie(pO2))
			throw new MatheJungAltException("Falscher Type: keine " + Serie.class.getName());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.serviceimpl.internal.comparators.AbstractDomainObjectComparator#specialCompare(de.egladil
	 * .mathe.core .domain.AbstractMatheAGObject, de.egladil.mathe.core.domain.AbstractMatheAGObject)
	 */
	@Override
	protected int specialCompare(AbstractMatheAGObject pO1, AbstractMatheAGObject pO2) {
		Serie serie1 = (Serie) pO1;
		Serie serie2 = (Serie) pO2;
		return serie1.getNummer() - serie2.getNummer();
	}

}
