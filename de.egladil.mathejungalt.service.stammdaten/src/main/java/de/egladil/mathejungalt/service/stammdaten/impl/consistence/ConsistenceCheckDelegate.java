/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.consistence;

import de.egladil.base.exceptions.MatheJungAltInconsistentEntityException;

/**
 * @author Heike Winkelvoss (www.egladil.de)
 */
public class ConsistenceCheckDelegate {

	/**
	 *
	 */
	public ConsistenceCheckDelegate() {
	}

	/**
	 * Prüft String-Keys auf geforderte Laenge.
	 *
	 * @param pKey
	 * @param pLength
	 * @param pMsgPref
	 */
	public void checkStringKey(final Object pKey, final int pLength, final String pMsgPref) {
		if (pKey == null || !pKey.getClass().isAssignableFrom(String.class) || ((String) pKey).length() != pLength)
			throw new MatheJungAltInconsistentEntityException(pMsgPref + " der Schluessel muss ein String der Laenge "
				+ pLength + " sein.");
	}
}
