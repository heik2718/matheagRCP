/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.dat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.DomainObjectProxyClassifier;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serienitem;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.MatheAGGeneratorUtils;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.TableCellLabelProvider;

/**
 * @author winkelv
 */
public class Schluessel2SerieTableCellLabelProvider extends TableCellLabelProvider {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(Schluessel2SerieTableCellLabelProvider.class);

	/**
	 *
	 */
	public Schluessel2SerieTableCellLabelProvider(String aufgabenabstand) {
		super(aufgabenabstand);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathe.core.serviceimpl.generatoren.labelprovider.TableCellLabelProvider#getText(java.lang.Object,
	 * int, int)
	 */
	@Override
	public String getText(Object pObj, int pRow, int pColumn) {
		if (!DomainObjectProxyClassifier.isSerienitem(pObj)) {
			String msg = "falscher Typ: kein " + Serienitem.class.getName() + " sondern " + pObj == null ? "null"
				: pObj.getClass().getName();
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		Serienitem item = (Serienitem) pObj;
		StringBuffer sb = new StringBuffer();
		sb.append(item.getAufgabe().getVerzeichnis());
		sb.append(";");
		sb.append(item.getAufgabe().getSchluessel());
		sb.append(";");
		sb.append(item.getNummer());
		sb.append(";Serie ");
		sb.append(item.getSerie().getNummer());
		sb.append(";(");
		sb.append(MatheAGGeneratorUtils.stufeText(item.getAufgabe().getStufe()));
		sb.append(")\n");
		return sb.toString();
	}
}
