/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.contentprovider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.comparators.ArbblattItemNummerComparator;
import de.egladil.mathejungalt.service.stammdaten.impl.comparators.AufgsItemNummerComparator;

/**
 * IAufgabensammlungItems einer Aufgabensammlung zu einer oder mehreren Stufen. Kann verwendet werden im Generator der
 * Vorschauseiten für die Homepage oder in den LaTeX-Generatoren für die Serie. Die gewünschten Stufen werden
 * imKonstruktor als Array mitgegeben.
 *
 * @author winkelv
 */
public class AufgsItemTableContentProvider extends TableContentProvider {

	private static final AufgsItemNummerComparator NUMMER_COMPARATOR = new AufgsItemNummerComparator();

	private static final ArbblattItemNummerComparator ARBBLATT_COMPARATOR = new ArbblattItemNummerComparator();

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(AufgsItemTableContentProvider.class);

	/** */
	private Collection<Integer> stufen = new ArrayList<Integer>();

	/** */
	private IAufgabensammlung aufgabensammlung;

	/** */
	private List<Object> aufgabensammlungItems;

	/**
	 * @param pAufgabensammlung
	 * @param pStammdatenservice
	 */
	public AufgsItemTableContentProvider(IAufgabensammlung pAufgabensammlung) {
		super();
		if (pAufgabensammlung == null) {
			String msg = MatheJungAltException.argumentNullMessage("IAufgabensammlung");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		aufgabensammlung = pAufgabensammlung;
	}

	/**
	 * @param pAufgabensammlung {@link Serie}
	 * @param pStufe die Stufe
	 */
	public AufgsItemTableContentProvider(IAufgabensammlung pAufgabensammlung, int[] pStufen) {
		this(pAufgabensammlung);
		for (int i = 0; i < pStufen.length; i++) {
			stufen.add(pStufen[i]);
		}
	}

	/**
	 *
	 */
	private void initItems(IStammdatenservice stammdatenservice) {
		if (!aufgabensammlung.isItemsLoaded()) {
			stammdatenservice.aufgabensammlungitemsLaden(aufgabensammlung);
		}
		final List<IAufgabensammlungItem> items = aufgabensammlung.getItems();
		final List<IAufgabensammlungItem> alleItems = new ArrayList<IAufgabensammlungItem>();
		for (IAufgabensammlungItem item : items) {
			alleItems.add((IAufgabensammlungItem) item);
		}
		if (aufgabensammlung instanceof Arbeitsblatt) {
			Collections.sort(alleItems, ARBBLATT_COMPARATOR);
		} else {
			Collections.sort(alleItems, NUMMER_COMPARATOR);
		}
		aufgabensammlungItems = new ArrayList<Object>();
		for (int i = 0; i < alleItems.size(); i++) {
			final IAufgabensammlungItem aufgabensammlungItem = alleItems.get(i);
			if (!stufen.isEmpty()) {
				if (stufen.contains(aufgabensammlungItem.getAufgabe().getStufe())) {
					aufgabensammlungItems.add(aufgabensammlungItem);
				}
			} else {
				aufgabensammlungItems.add(aufgabensammlungItem);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.serviceimpl.generatoren.html.contentprovider.TableContentProvider#getContent()
	 */
	@Override
	public List<Object> getContent(IStammdatenservice stammdatenservice) {
		if (aufgabensammlungItems == null || aufgabensammlungItems.isEmpty()) {
			initItems(stammdatenservice);
		}
		return aufgabensammlungItems;
	}
}
