/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.consistence;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.exceptions.MatheJungAltInconsistentEntityException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.DomainObjectProxyClassifier;
import de.egladil.mathejungalt.domain.schulen.Kontakt;

/**
 * @author Heike Winkelvoss (www.egladil.de)
 */
public class KontaktConsistenceChecker implements IConsistencecheckProvider {

	/** */
	private final ConsistenceCheckDelegate consistenceCheckDelegate;

	/**
	 *
	 */
	public KontaktConsistenceChecker() {
		consistenceCheckDelegate = new ConsistenceCheckDelegate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.consistencecheck.IConsistencecheckProvider#canCheck(de.egladil.mathejungalt.domain
	 * .AbstractMatheAGObject)
	 */
	@Override
	public boolean canCheck(Object pObject) {
		return DomainObjectProxyClassifier.isKontakt(pObject);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.consistencecheck.IConsistencecheckProvider#checkConsistent(de.egladil.mathejungalt
	 * .domain.AbstractMatheAGObject)
	 */
	@Override
	public void checkConsistent(AbstractMatheAGObject pObject) throws MatheJungAltException {
		StringBuffer sb = new StringBuffer();
		boolean consistent = true;
		Kontakt kontakt = (Kontakt) pObject;
		try {
			consistenceCheckDelegate.checkStringKey(kontakt.getKey(), 3, "");
		} catch (MatheJungAltInconsistentEntityException e) {
			sb.append(e.getMessage());
			consistent = false;
		}
		if (kontakt.getName() == null || kontakt.getName().trim().length() == 0) {
			sb.append("\nder Name darf nicht null oder leer sein");
			consistent = false;
		}
		if (kontakt.getEmail() == null || kontakt.getEmail().trim().isEmpty()) {
			sb.append("\ndie Email darf nicht null oder negativ sein");
			consistent = false;
		}
		if (!consistent) {
			throw new MatheJungAltInconsistentEntityException("Kotakt:\n" + sb.toString());
		}
	}

}
