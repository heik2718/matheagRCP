//=====================================================
// Projekt: de.egladil.mathejungalt.service.stammdaten
// (c) Heike Winkelvoß
//=====================================================
// Dateiname: SerieIndexGenerator.java                            $
// $Revision:: 1                                     $
// $Modtime:: 29.03.2015                                $
//=====================================================

package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html;

import java.util.List;

import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serienitem;
import de.egladil.mathejungalt.service.stammdaten.impl.Messages;

/**
 * ArchivSerieStufeGenerator generiert die Website mit den pngs zu Aufgaben und Lösungen einer Serienstufe für das
 * Archiv. Diese liegen unter vorschau/mja_nn-1.html, vorschau/mja_nn-2.html usw.
 */
public class ArchivSerieStufeGenerator extends AbstractSerieStufeGenerator {

	private static final String HTML_TEMPLATE_PATH = "/html/archiv_serie_stufe_template.txt";

	private static final String LOES_PLACEHOLDER = "LOESUNGEN_UL";

	/**
	 * Erzeugt eine Instanz von SerieIndexGenerator
	 */
	public ArchivSerieStufeGenerator() {
	}

	/**
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html.AbstractSerieStufeGenerator#getTemplatePath()
	 */
	@Override
	String getTemplatePath() {
		return HTML_TEMPLATE_PATH;
	}

	/**
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html.AbstractSerieStufeGenerator#weitereTransformation(int,
	 * java.util.List, java.lang.String)
	 */
	@Override
	protected String weitereTransformation(int stufe, List<Serienitem> items, String content) {
		String result = content.replaceAll(LOES_PLACEHOLDER, getReplacementForLoesungPart(items, stufe));
		return result;
	}

	private String getReplacementForLoesungPart(List<Serienitem> items, int stufe) {
		StringBuffer sb = new StringBuffer();
		sb.append(Messages.getString("SerieStufe_generator_1"));

		for (int i = 0; i < items.size(); i++) {
			Serienitem item = items.get(i);
			sb.append(getItemPartLoesung(item));
		}

		sb.append("</ul>");
		return sb.toString();
	}

	private Object getItemPartLoesung(IAufgabensammlungItem item) {
		StringBuffer sb = new StringBuffer();
		sb.append("<li><img src=\"../pngdir/");
		sb.append(item.getNummer());
		sb.append("_l.png\" alt=\"L&ouml;sungstext\"></li>");
		return sb.toString();
	}

	/**
	 *
	 * @return
	 */
	public String getFilename(Serie serie, int stufe) {
		return "mja_" + serie.getNummer() + "-" + stufe + ".html";
	}
}
