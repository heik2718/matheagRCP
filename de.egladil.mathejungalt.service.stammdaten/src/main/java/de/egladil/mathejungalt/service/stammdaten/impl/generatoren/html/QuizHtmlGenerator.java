/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html;

import java.text.SimpleDateFormat;
import java.util.List;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.Messages;
import de.egladil.mathejungalt.service.stammdaten.impl.TimestampProcessor;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator;
import de.egladil.mcmatheraetsel.domain.spiel.Raetsel;

/**
 * Generiert ein html-File mit genau einem Quizspiel aus dem gegebenen Raetsel.
 *
 * @author heike
 */
public class QuizHtmlGenerator implements IFilegenerator {

	private static final SimpleDateFormat SDF = new SimpleDateFormat("dd.MM.yyyy");

	private final MCArchivraetsel quiz;

	private final Raetsel raetsel;

	/**
	 * @param pRaetsel
	 */
	public QuizHtmlGenerator(MCArchivraetsel pRaetsel) {
		quiz = pRaetsel;
		raetsel = Raetsel.createFromArchiv(quiz);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator#getFilename()
	 */
	@Override
	public String getFilename() {
		return quiz.getSchluessel() + ".html";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator#generate()
	 */
	@Override
	public String generate(IStammdatenservice stammdatenservice) throws MatheJungAltException {
		StringBuffer sb = new StringBuffer();
		sb.append(Messages.getString("quiz_part1"));
		sb.append(Messages.getString("quiz_part2", new Object[] { quiz.getSchluessel(), quiz.getSchluessel() }));
		sb.append(Messages.getString("quiz_part3"));
		sb.append(Messages.getString("quiz_part4", new Object[] { quiz.getTitel(), quiz.getStufe().getLabel(),
			quiz.getAutor().getName() }));
		final String url = quiz.getAutor().getUrl();
		if (url != null && !url.isEmpty()) {
			sb.append(" (" + url + ")</li>");
		} else {
			sb.append("</li>");
		}
		double zeit = empfohleneZeitInMinuten();
		sb.append(Messages.getString("quiz_part5", new Object[] { zeit }));
		final String licenseShort = quiz.getLicenseShort();
		if (licenseShort != null && !licenseShort.isEmpty()) {
			sb.append(Messages.getString("quiz_part6", new Object[] { licenseShort }));
		}
		sb.append(Messages.getString("quiz_part7"));
		final List<MCArchivraetselItem> itemsSorted = quiz.getItemsSorted();
		for (int i = 0; i < itemsSorted.size(); i++) {
			final MCArchivraetselItem item = itemsSorted.get(i);
			sb.append(generate(item, i));
		}
		String datum = SDF.format(TimestampProcessor.getInstance().getTimestampNow());
		sb.append(Messages.getString("quiz_part8", new Object[] { datum }));
		return sb.toString();
	}

	/**
	 * Generiert den Item-Teil der Quizseite
	 *
	 * @param pItem
	 * @return
	 */
	private String generate(MCArchivraetselItem pItem, int index) {
		StringBuffer sb = new StringBuffer();
		final MCAufgabe aufgabe = pItem.getAufgabe();
		sb.append(Messages.getString("quizitem_part1", new Object[] { aufgabe.getSchluessel() }));
		{
			// Antwort A
			final String letter = "A";
			final String label = aufgabe.isTabelleGenerieren() ? aufgabe.getAntwortA() : letter;
			label.replaceAll("\\eu", "Euro");
			sb.append(generateAntwort(label, letter, index));
		}
		{
			// Antwort B
			final String letter = "B";
			final String label = aufgabe.isTabelleGenerieren() ? aufgabe.getAntwortB() : letter;
			sb.append(generateAntwort(label, letter, index));
		}

		int anzahlAntworten = aufgabe.getAnzahlAntwortvorschlaege();
		switch (anzahlAntworten) {
		case 3: {
			{
				// Antwort C
				final String letter = "C";
				final String label = aufgabe.isTabelleGenerieren() ? aufgabe.getAntwortC() : letter;
				sb.append(generateAntwort(label, letter, index));
			}
			break;
		}
		case 4: {
			{
				// Antwort C
				final String letter = "C";
				final String label = aufgabe.isTabelleGenerieren() ? aufgabe.getAntwortC() : letter;
				sb.append(generateAntwort(label, letter, index));
			}
			{
				// Antwort D
				final String letter = "D";
				final String label = aufgabe.isTabelleGenerieren() ? aufgabe.getAntwortD() : letter;
				sb.append(generateAntwort(label, letter, index));
			}
			break;
		}
		case 5: {
			{
				// Antwort C
				final String letter = "C";
				final String label = aufgabe.isTabelleGenerieren() ? aufgabe.getAntwortC() : letter;
				sb.append(generateAntwort(label, letter, index));
			}
			{
				// Antwort D
				final String letter = "D";
				final String label = aufgabe.isTabelleGenerieren() ? aufgabe.getAntwortD() : letter;
				sb.append(generateAntwort(label, letter, index));
			}
			{
				// Antwort E
				final String letter = "E";
				final String label = aufgabe.isTabelleGenerieren() ? aufgabe.getAntwortE() : letter;
				sb.append(generateAntwort(label, letter, index));
			}
			break;
		}
		default:
			sb.append(Messages.getString("quizitem_part4", new Object[] { index, index }));
			break;
		}
		sb.append(Messages.getString("quizitem_part4", new Object[] { index }));
		sb.append(" onClick=\"markAnswer('");
		sb.append(index);
		sb.append("','N');\"");
		sb.append(Messages.getString("quizitem_part5"));

		sb.append(Messages.getString("quizitem_part6", new Object[] { index, index, index }));
		return sb.toString();
	}

	/**
	 * @param aufgabe
	 * @param label
	 * @param letter
	 * @param index
	 * @return
	 */
	private String generateAntwort(String label, String letter, int index) {
		StringBuffer sb = new StringBuffer();
		sb.append(Messages.getString("quizitem_part2", new Object[] { index, letter }));
		sb.append(" onClick=\"markAnswer('");
		sb.append(index);
		sb.append("','");
		sb.append(letter);
		sb.append("');\">");
		sb.append(Messages.getString("quizitem_part3", new Object[] { label }));
		return sb.toString();
	}

	private double empfohleneZeitInMinuten() {
		long millis = raetsel.vorgabezeitInMillisBerechnen();
		double result = millis / 60000;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator#getEncoding()
	 */
	@Override
	public String getEncoding() {
		return ENCODING_UTF;
	}
}
