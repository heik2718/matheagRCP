//=====================================================
// Projekt: de.egladil.mathejungalt.service.stammdaten
// (c) Heike Winkelvoß
//=====================================================
// Dateiname: ArchivseiteGeneratorNeu.java                            $
// $Revision:: 1                                     $
// $Modtime:: 28.03.2015                                $
//=====================================================

package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.Messages;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.contentprovider.ArchivTableContentProvider;

/**
 * ArchivseiteGeneratorNeu benutzt die Datei /html/archiv_html_template.txt zum Einlesen der konstanten Anteile. Die
 * Datei enthält die Platzhalter $ARCHIV_UL_SMALL$ und $ARCHIV_TABLE_WIDE$. Nur diese werden während der Generierung
 * ersetzt.
 */
public class ArchivseiteGenerator {

	private static final String HTML_TEMPLATE_PATH = "/html/archiv_html_template.txt";

	private static final String UL_SMALL_PLACEHOLDER = "ARCHIV_UL_SMALL";

	private static final String TAB_WIDE_PLACEHOLDER = "ARCHIV_TABLE_WIDE";

	private static final Logger LOG = LoggerFactory.getLogger(ArchivseiteGenerator.class);

	private ArchivTableContentProvider contentProvider = new ArchivTableContentProvider();

	/**
	 * Erzeugt eine Instanz von ArchivseiteGeneratorNeu
	 */
	public ArchivseiteGenerator() {
	}

	/**
	 *
	 *
	 * @param stammdatenservice
	 * @return
	 */
	public String generate(IStammdatenservice stammdatenservice) {
		InputStream in = null;
		try {
			in = this.getClass().getResourceAsStream(HTML_TEMPLATE_PATH);
			StringWriter sw = new StringWriter();
			IOUtils.copy(in, sw);
			List<Serie> serien = contentProvider.getSerien(stammdatenservice);
			String content = sw.toString();
			content = content.replaceAll(UL_SMALL_PLACEHOLDER, getReplacementForSmallDevices(serien));
			content = content.replaceAll(TAB_WIDE_PLACEHOLDER, getReplacementForWideDevices(serien));
			content = HtmlFactory.setAktuellesDatum(content);
			return content;
		} catch (IOException e) {
			LOG.error(e.getMessage());
			throw new MatheJungAltException("Fehler beim Laden des Templates '" + HTML_TEMPLATE_PATH
				+ "': mal classpath prüfen!");

		} finally {
			IOUtils.closeQuietly(in);
		}
	}

	String getReplacementForSmallDevices(List<Serie> serien) {
		StringBuffer sb = new StringBuffer();
		sb.append(Messages.getString("ArchivseiteGenerator_1"));
		for (Serie s : serien) {
			final Integer nummer = s.getNummer();
			sb.append(Messages.getString("ArchivseiteGenerator_2"));
			sb.append(nummer);
			sb.append(".html\">Serie ");
			sb.append(nummer);
			sb.append("</a></li>");
		}
		sb.append("</ul>");
		return sb.toString();
	}

	String getReplacementForWideDevices(List<Serie> serien) {
		StringBuffer sb = new StringBuffer();

		sb.append(Messages.getString("ArchivseiteGenerator_3"));
		for (Serie s : serien) {
			sb.append(getTableRow(s));
		}
		sb.append("</tbody></table>");
		return sb.toString();
	}

	String getTableRow(Serie serie) {
		final Integer nummer = serie.getNummer();
		final Integer runde = serie.getRunde();
		StringBuffer sb = new StringBuffer();
		sb.append(Messages.getString("ArchivseiteGenerator_4"));
		sb.append(" ");
		sb.append(nummer);
		sb.append(":</td><td><a href=\"aufgaben/mja_");
		sb.append(runde);
		sb.append("_");
		sb.append(nummer);
		sb.append("_aufgaben.pdf\" target=\"_blank\">A</a></td><td><a href=\"loesungen/mja_");
		sb.append(runde);
		sb.append("_");
		sb.append(nummer);
		sb.append("_loesungen.pdf\" target=\"_blank\">L</a></td><td><a href=\"vorschau/mja_");
		sb.append(nummer);
		sb.append("-1.html\"> 1 </a></td><td><a href=\"vorschau/mja_");
		sb.append(nummer);
		sb.append("-2.html\"> 2 </a></td><td><a href=\"vorschau/mja_");
		sb.append(nummer);
		sb.append("-3.html\"> 3 </a></td><td><a href=\"vorschau/mja_");
		sb.append(nummer);
		sb.append("-4.html\"> 4 </a></td><td><a href=\"vorschau/mja_");
		sb.append(nummer);
		sb.append("-5.html\"> 5 </a></td><td><a href=\"vorschau/mja_");
		sb.append(nummer);
		sb.append("-6.html\"> 6 </a></td><td><a href=\"vorschau/mja_");
		sb.append(nummer);
		sb.append("-quellen.html\">Q</a></td></tr>");
		return sb.toString();
	}

	/**
	 * Setzt die Membervariable
	 *
	 * @param contentProvider neuer Wert der Membervariablen contentProvider
	 */
	public void setContentProvider(ArchivTableContentProvider contentProvider) {
		this.contentProvider = contentProvider;
	}

	/**
	 *
	 * @return
	 */
	public String getFilename() {
		return Messages.getString("page_archiv"); //$NON-NLS-1$
	}

}
