/**
 * 
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider;

/**
 * @author winkelv
 */
public interface ITableRowLabelProvider {

	/**
	 * @return String den Starttext.
	 */
	public abstract String getStartText();

	/**
	 * @return String den Endetext.
	 */
	public abstract String getEndText();

}