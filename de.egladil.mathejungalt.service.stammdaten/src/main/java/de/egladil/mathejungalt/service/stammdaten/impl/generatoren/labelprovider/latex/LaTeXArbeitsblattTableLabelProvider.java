/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.latex;

/**
 * @author aheike
 */
public class LaTeXArbeitsblattTableLabelProvider extends AbstractLaTeXTableLabelProvider {

	/**
	 *
	 */
	public LaTeXArbeitsblattTableLabelProvider(String fontSize) {
		super(fontSize);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.ITableLabelProvider#getStartText()
	 */
	@Override
	public String getStartText() {
		return "\n";
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider.latex.AbstractLaTeXTableLabelProvider
	 * #getEndText()
	 */
	@Override
	public String getEndText() {
		return "\n";
	}

}
