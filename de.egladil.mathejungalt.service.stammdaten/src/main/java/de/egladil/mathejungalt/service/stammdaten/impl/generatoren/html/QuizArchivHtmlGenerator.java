/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.types.MCAufgabenstufen;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.Messages;
import de.egladil.mathejungalt.service.stammdaten.impl.TimestampProcessor;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator;

/**
 * Generiert ein html-File mit genau einem Quizspiel aus dem gegebenen Raetsel.
 *
 * @author heike
 */
public class QuizArchivHtmlGenerator implements IFilegenerator {

	private static final SimpleDateFormat SDF = new SimpleDateFormat("dd.MM.yyyy");

	private final Collection<MCArchivraetsel> alleRaetsel;

	/**
	 * @param pRaetsel
	 */
	public QuizArchivHtmlGenerator(Collection<MCArchivraetsel> pRaetsel) {
		alleRaetsel = pRaetsel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator#getFilename()
	 */
	@Override
	public String getFilename() {
		return "raetsel.html";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator#generate()
	 */
	@Override
	public String generate(IStammdatenservice stammdatenservice) throws MatheJungAltException {
		StringBuffer sb = new StringBuffer();
		sb.append(Messages.getString("quizarchiv_part1"));

		for (MCAufgabenstufen stufe : MCAufgabenstufen.valuesSorted()) {
			List<MCArchivraetsel> raetsel = findPublishedByStufe(stufe);
			if (raetsel.size() > 0) {
				final String htmlIdSuffix = stufe.getHtmlIdSuffix();
				sb.append(Messages.getString("quizarchiv_part2", new String[] { htmlIdSuffix }));
				sb.append("('quizlinks");
				sb.append(htmlIdSuffix);
				sb.append("','btnExpand");
				sb.append(htmlIdSuffix);
				sb.append("')\" /> ");
				sb.append(Messages.getString("quizarchiv_part3", new Object[] { stufe.getLabel(), htmlIdSuffix }));

				for (int i = 0; i < raetsel.size(); i++) {
					MCArchivraetsel r = raetsel.get(i);
					final String schluessel = r.getSchluessel();
					sb.append(Messages.getString("quizarchiv_part4",
						new Object[] { schluessel, schluessel, schluessel }));
				}
				sb.append(Messages.getString("quizarchiv_part5"));
			}
		}

		sb.append(Messages.getString("quizarchiv_part6"));
		sb.append(Messages.getString("quizarchiv_part7",
			new Object[] { SDF.format(TimestampProcessor.getInstance().getTimestampNow()) }));
		return sb.toString();
	}

	/**
	 * @param pStufe
	 * @return
	 */
	private List<MCArchivraetsel> findPublishedByStufe(MCAufgabenstufen pStufe) {
		List<MCArchivraetsel> result = new ArrayList<>();
		for (MCArchivraetsel raetsel : alleRaetsel) {
			if (pStufe == raetsel.getStufe() && !raetsel.isPrivat() && raetsel.isVeroeffentlicht()) {
				result.add(raetsel);
			}
		}
		Collections.sort(result, new Comparator<MCArchivraetsel>() {

			@Override
			public int compare(MCArchivraetsel pO1, MCArchivraetsel pO2) {
				return pO1.getSchluessel().compareTo(pO2.getSchluessel());
			}

		});
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator#getEncoding()
	 */
	@Override
	public String getEncoding() {
		return ENCODING_UTF;
	}
}
