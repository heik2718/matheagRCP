/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.consistence;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.exceptions.MatheJungAltInconsistentEntityException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.DomainObjectProxyClassifier;
import de.egladil.mathejungalt.domain.quellen.Kindquelle;

/**
 * Konsistenzprüfung für {@link Kindquelle}
 *
 * @author Heike Winkelvoss (www.egladil.de)
 */
public class KindquelleConsistenceChecker implements IConsistencecheckProvider {

	/** */
	private final ConsistenceCheckDelegate consistenceCheckDelegate;

	/**
	 *
	 */
	public KindquelleConsistenceChecker() {
		consistenceCheckDelegate = new ConsistenceCheckDelegate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.consistencecheck.IConsistencecheckProvider#canCheck(de.egladil.mathejungalt.domain
	 * .AbstractMatheAGObject)
	 */
	@Override
	public boolean canCheck(Object pObject) {
		return DomainObjectProxyClassifier.isKindquelle(pObject);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.consistencecheck.IConsistencecheckProvider#checkConsistent(de.egladil.mathejungalt
	 * .domain.AbstractMatheAGObject)
	 */
	@Override
	public void checkConsistent(AbstractMatheAGObject pObject) throws MatheJungAltException {
		Kindquelle pQuelle = (Kindquelle) pObject;
		consistenceCheckDelegate.checkStringKey(pQuelle.getKey(), 6, "Kindquelle:");
		if (pQuelle.getMitglied() == null)
			throw new MatheJungAltInconsistentEntityException("Kindquelle: das Kind fehlt");
		if (pQuelle.getJahre() == null)
			throw new MatheJungAltInconsistentEntityException("Kindquelle: das Alter fehlt");
		if (pQuelle.getKlasse() == null)
			throw new MatheJungAltInconsistentEntityException("Kindquelle: die Klasse fehlt");
	}
}
