/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.consistence;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.exceptions.MatheJungAltInconsistentEntityException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;

/**
 * @author aheike
 */
public class ArbeitsblattConsistenceChecker implements IConsistencecheckProvider {

	private final AufgabensammlungItemConsistenceCheckDelegate itemsCheckDelegate = new AufgabensammlungItemConsistenceCheckDelegate();

	/** */
	private final ConsistenceCheckDelegate consistenceCheckDelegate = new ConsistenceCheckDelegate();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.consistencecheck.IConsistencecheckProvider#canCheck(de.egladil.mathejungalt.domain
	 * .AbstractMatheAGObject)
	 */
	@Override
	public boolean canCheck(Object pObject) {
		return pObject instanceof Arbeitsblatt;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.consistencecheck.IConsistencecheckProvider#checkConsistent(de.egladil.mathejungalt
	 * .domain.AbstractMatheAGObject)
	 */
	@Override
	public void checkConsistent(AbstractMatheAGObject pObject) throws MatheJungAltException {
		Arbeitsblatt arbeitsblatt = (Arbeitsblatt) pObject;
		consistenceCheckDelegate.checkStringKey(arbeitsblatt.getKey(), 4, "Arbeitsblatt:");
		if (arbeitsblatt.getTitel() == null || arbeitsblatt.getTitel().isEmpty()
			|| arbeitsblatt.getTitel().length() > 100) {
			throw new MatheJungAltInconsistentEntityException(
				"Arbeitsblatt: der Titel darf nicht null oder leer sein und nicht laenger als 100 Zeichen.");
		}
		if (arbeitsblatt.getKey() == null) {
			throw new MatheJungAltInconsistentEntityException("Arbeitsblatt: der Schluessel darf nicht null sein.");
		}
		if (arbeitsblatt.getSperrend() != 0 && arbeitsblatt.getSperrend() != 1) {
			throw new MatheJungAltInconsistentEntityException(
				"Arbeitsblatt: sperrend darf nur 0 oder 1 sein, ist aber " + arbeitsblatt.getSperrend());
		}
		itemsCheckDelegate.checkItems(arbeitsblatt);
	}
}
