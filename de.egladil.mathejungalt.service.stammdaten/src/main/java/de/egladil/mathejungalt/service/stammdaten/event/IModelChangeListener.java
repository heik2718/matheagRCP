/**
 * 
 */
package de.egladil.mathejungalt.service.stammdaten.event;

/**
 * @author aheike
 */
public interface IModelChangeListener {

	/**
	 * @param pEvt
	 */
	public abstract void modelChanged(ModelChangeEvent pEvt);
}
