/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.EgladilServerException;
import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.xml.XSLProcessor;
import de.egladil.mathejungalt.config.IMatheJungAltConfiguration.RaetsellisteGeneratorModus;
import de.egladil.mathejungalt.domain.mcraetsel.GeneratorUtils;
import de.egladil.mathejungalt.domain.mcraetsel.IMCArchivraetselNames;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.domain.mcraetsel.Urheber;
import de.egladil.mathejungalt.domain.types.MCAufgabenstufen;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.MatheAGGeneratorUtils;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html.QuizArchivHtmlGenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html.QuizHtmlGenerator;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html.QuizJsGenerator;
import de.egladil.mcmatheraetsel.domain.spiel.Raetsel;
import de.egladil.mcmatheraetsel.domain.spiel.Raetselbibliothek;

/**
 * @author heike
 */
public class Raetselservice extends AbstractLaTeXConcernedService {

	private static final Logger LOG = LoggerFactory.getLogger(Raetselservice.class);

	private static final String RAETSELLISTE_XML_FILENAME = "raetselliste.xml";

	private int nextRaetselSchluesselInt = -1;

	private Map<String, MCArchivraetsel> raetselMap;

	private Map<Long, Urheber> urheberMap;

	/**
	 * @param pPersistenceservice
	 */
	public Raetselservice() {
		super();
	}

	/**
	 * @param pAufgabe Aufgabe
	 * @return String J oder N
	 */
	protected String isGesperrtFuerRaetsel(MCAufgabe pAufgabe) {
		if (pAufgabe.getGesperrtFuerRaetsel() == null) {
			int anzahl = anzahlRaetselMitAufgabe(pAufgabe);
			if (anzahl == 0) {
				pAufgabe.setGesperrtFuerRaetsel(MCAufgabe.NOT_LOCKED);
			} else {
				pAufgabe.setGesperrtFuerRaetsel(MCAufgabe.LOCKED);
			}
		}
		return pAufgabe.getGesperrtFuerRaetsel();
	}

	/**
	 * @param pAufgabe
	 * @return
	 */
	private int anzahlRaetselMitAufgabe(MCAufgabe pAufgabe) {
		List<Long> raetselIds = getPersistenceservice().getMcAufgabenRepository().raetselIdMitAufgabe(pAufgabe);
		return raetselIds.size();
	}

	/**
	 * @param pRaetsel
	 * @param pAufgabe
	 * @return
	 */
	protected MCArchivraetselItem aufgabeZuRaetselHinzufuegen(MCArchivraetsel pRaetsel, MCAufgabe pAufgabe) {
		if (pRaetsel == null || pAufgabe == null) {
			String msg = MatheJungAltException.argumentNullMessage(pRaetsel == null ? "pRaetsel" : "pAufgabe");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (pRaetsel.isVeroeffentlicht()) {
			throw new MatheJungAltException(
				"Das Raetsel ist bereits veroeffentlicht. Es kann keine Aufgabe mehr hinzugefuegt werden.");
		}
		if (MCAufgabe.LOCKED.equals(pAufgabe.getGesperrtFuerRaetsel())) {
			throw new MatheJungAltException("Die Aufgabe kann nicht zum Raetsel zugeordnet werden. Sie ist gesperrt.");
		}
		MCArchivraetselItem item = new MCArchivraetselItem();
		item.setAufgabe(pAufgabe);
		item.setRaetsel(pRaetsel);
		pAufgabe.setGesperrtFuerRaetsel(MCAufgabe.LOCKED);
		pRaetsel.addItem(item);
		return item;
	}

	/**
	 * @param pItem
	 */
	public void aufgabeAusRaetselEntfernen(MCArchivraetselItem pItem) {
		if (pItem == null) {
			String msg = MatheJungAltException.argumentNullMessage("pItem");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		final MCArchivraetsel raetsel = pItem.getRaetsel();
		raetsel.removeItem(pItem);
		aufgabeFreigeben(pItem);
	}

	/**
	 * @param pItem
	 */
	private void aufgabeFreigeben(MCArchivraetselItem pItem) {
		pItem.getAufgabe().setGesperrtFuerRaetsel(MCAufgabe.NOT_LOCKED);
	}

	/**
	 * @return
	 */
	protected Collection<Urheber> getRaetselautoren() {
		if (urheberMap == null) {
			loadUrheber();
		}
		return urheberMap.values();
	}

	/**
	 *
	 */
	private void loadUrheber() {
		urheberMap = new HashMap<Long, Urheber>();
		List<Urheber> medien = getPersistenceservice().getUrheberRepository().findAllUrheber();
		for (Urheber urheber : medien) {
			urheberMap.put(urheber.getId(), urheber);
		}
	}

	/**
	 * @param pRaetsel
	 */
	protected void publish(MCArchivraetsel pRaetsel) {
		try {
			getPersistenceservice().getRaetselRepository().markAsPublished(pRaetsel);
			pRaetsel.setVeroeffentlicht(true);
		} catch (Exception e) {
			throw new EgladilServerException(e.getMessage(), e);
		}
	}

	/**
	 * @param pVersion
	 * @param pPath
	 * @param pPrivat TODO
	 * @param pModus TODO
	 * @return
	 */
	protected IStatus generateRaetselliste(int pVersion, String pPath, boolean pPrivat,
		RaetsellisteGeneratorModus pModus, IStammdatenservice stammdatenservice) {
		if (raetselMap == null) {
			loadRaetsel();
		}
		final File workDir = prepareWorkDir(pPath);
		if (RaetsellisteGeneratorModus.WEB == pModus) {
			return generateRaetselarchivHtml(workDir, pPrivat, stammdatenservice);
		} else {
			return generateRaetsellisteForRCP(pVersion, workDir, pPrivat);
		}
	}

	/**
	 * @param workDir
	 * @param pPrivat
	 * @return
	 */
	private IStatus generateRaetselarchivHtml(File workDir, boolean pPrivat, IStammdatenservice stammdatenservice) {
		Collection<MCArchivraetsel> alle = raetselMap.values();
		Collection<MCArchivraetsel> raetsel = new ArrayList<>();
		for (MCArchivraetsel r : alle) {
			if (raetselGenerieren(r.isPrivat(), pPrivat)) {
				raetsel.add(r);
			}
		}
		final QuizArchivHtmlGenerator generator = new QuizArchivHtmlGenerator(raetsel);
		try {
			MatheAGGeneratorUtils.writeOutput(workDir.getAbsolutePath(), generator, stammdatenservice);
			return new Status(IStatus.OK, IStammdatenservice.PLUGIN_ID, "Raetselliste ok");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return new Status(IStatus.ERROR, IStammdatenservice.PLUGIN_ID,
				": Raetselliste: Marshalling fehlgeschlagen.");
		}
	}

	/**
	 * @param pVersion
	 * @param pPath
	 * @param pPrivat
	 * @return
	 */
	private IStatus generateRaetsellisteForRCP(int pVersion, File workDir, boolean pPrivat) {
		File out = new File(workDir + "/" + RAETSELLISTE_XML_FILENAME);
		Raetselbibliothek raetselbibliothek = new Raetselbibliothek();
		List<String> kategorien = new ArrayList<String>();
		for (MCAufgabenstufen stufe : MCAufgabenstufen.valuesSorted()) {
			kategorien.add(stufe.getLabel());
		}
		String titel = "Mathe für jung und alt Quiz";
		if (pPrivat) {
			titel = "Private Quizsammlung Heike";
		}
		raetselbibliothek.setTitel(titel);
		raetselbibliothek.setKategorien(kategorien);
		// TODO hier noch die Kategorien einfügen
		raetselbibliothek.setVersion(pVersion);
		for (MCArchivraetsel ar : raetselMap.values()) {
			if (ar.isVeroeffentlicht()) {
				if (!ar.isItemsLoaded()) {
					raetselitemsLaden(ar);
				}
				boolean generieren = raetselGenerieren(ar.isPrivat(), pPrivat);
				if (generieren) {
					Raetsel r = Raetsel.createFromArchiv(ar);
					raetselbibliothek.add(r);
				}
			}
		}
		try {
			final Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.setProperty("jaxb.encoding", "ISO-8859-1");
			marshaller.marshal(raetselbibliothek, out);
			return new Status(IStatus.OK, IStammdatenservice.PLUGIN_ID, "Raetselliste ok");
		} catch (JAXBException e) {
			LOG.error(e.getMessage(), e);
			return new Status(IStatus.ERROR, IStammdatenservice.PLUGIN_ID,
				": Raetselliste: Marshalling fehlgeschlagen.");
		}
	}

	/**
	 * Entscheidet, ob das gegebene Rätsel in die Rätselliste aufgenommen werden soll.
	 *
	 * @param pRaetselIstPrivat
	 * @param pGenerierePrivat
	 * @return
	 */
	boolean raetselGenerieren(boolean pRaetselIstPrivat, boolean pGenerierePrivat) {
		if (pRaetselIstPrivat && !pGenerierePrivat) {
			return false;
		}
		return true;
	}

	/**
	 * @return
	 * @throws MatheJungAltException
	 */
	protected Collection<MCArchivraetsel> getRaetsel() throws MatheJungAltException {
		if (raetselMap == null) {
			loadRaetsel();
		}
		return raetselMap.values();
	}

	/**
   *
   */
	private void loadRaetsel() {
		raetselMap = new HashMap<String, MCArchivraetsel>();
		List<MCArchivraetsel> raetsel = getPersistenceservice().getRaetselRepository().loadAllRaetsel();
		if (raetsel != null) {
			for (MCArchivraetsel r : raetsel) {
				// raetselMap.put(r.getUuid(), r);
				raetselMap.put(r.getSchluessel(), r);
			}
		}
	}

	/**
	 * @return
	 * @throws MatheJungAltException
	 */
	protected MCArchivraetsel raetselAnlegen() throws MatheJungAltException {
		MCArchivraetsel raetsel = new MCArchivraetsel();
		raetsel.setSchluessel(nextRaetselSchluessel());
		if (raetsel.getAutor().getId() == null) {
			Urheber u = findUrheberByName(raetsel.getAutor().getName());
			if (u != null) {
				raetsel.setAutor(u);
			} else {
				throw new MatheJungAltException("Kein Urheber mit [name=" + raetsel.getAutor().getName()
					+ "] in der DB");
			}
		}
		return raetsel;
	}

	private Urheber findUrheberByName(String pName) {
		if (urheberMap == null) {
			loadUrheber();
		}
		for (Urheber u : urheberMap.values()) {
			if (u.getName().equals(pName)) {
				return u;
			}
		}
		return null;
	}

	/**
	 * Ermittelt den naechsten freien Schluesselwert fuer eine Aufgabe.
	 *
	 * @return String
	 */
	private String nextRaetselSchluessel() {
		if (nextRaetselSchluesselInt < 0) {
			final String maxKey = getPersistenceservice().getRaetselRepository().getMaxKey();
			if (maxKey != null) {
				nextRaetselSchluesselInt = Integer.valueOf(maxKey).intValue() + 1;
			} else {
				nextRaetselSchluesselInt = 1;
			}
		}
		String schl = "" + (nextRaetselSchluesselInt);
		if (schl.length() < IMCArchivraetselNames.LENGTH_SCHLUESSEL_DEFAULT) {
			schl = StringUtils.leftPad(schl, IMCArchivraetselNames.LENGTH_SCHLUESSEL_DEFAULT, '0');
		}
		nextRaetselSchluesselInt++;
		return schl;
	}

	protected void raetselSpeichern(MCArchivraetsel pRaetsel) throws MatheJungAltException {
		boolean neu = pRaetsel.isNew();
		getPersistenceservice().getRaetselRepository().saveRaetsel(pRaetsel);
		if (neu) {
			// raetselMap.put(pRaetsel.getUuid(), pRaetsel);
			raetselMap.put(pRaetsel.getSchluessel(), pRaetsel);
			fireObjectAddedEvent(pRaetsel);
		}
	}

	/**
	 * @param pRaetsel
	 * @return
	 * @throws MatheJungAltException
	 */
	protected Collection<MCArchivraetselItem> raetselitemsLaden(MCArchivraetsel pRaetsel) throws MatheJungAltException {
		List<MCArchivraetselItem> items = getPersistenceservice().getRaetselRepository().loadItems(pRaetsel);
		return items;
	}

	/**
	 * Generiert für jedes RaetselItem zwei LaTeX-Files und daraus ps und generiert zwei LaTeX-Files für das Raetsel
	 * selbst, ohne es zu compilieren (wegen Raumauftelung)
	 *
	 * @param pObjects
	 * @param pWorkDir
	 * @param pModus TODO
	 * @param pProgressMonitor
	 * @return
	 */
	protected IStatus generateLaTeXAndTransformToPs(Map<String, MCArchivraetsel> pObjects, String pWorkDir,
		RaetsellisteGeneratorModus pModus, IStammdatenservice stammdatenservice, IProgressMonitor pProgressMonitor) {
		int totalWork = pObjects.size();
		SubMonitor subMonitor = SubMonitor.convert(pProgressMonitor, 80);
		SubMonitor loopProgress = subMonitor.newChild(80).setWorkRemaining(totalWork);
		final File workDir = prepareWorkDir(pWorkDir);
		List<IStatus> statusList = new ArrayList<>();
		IStatus status = null;
		for (String key : pObjects.keySet()) {
			MCArchivraetsel raetsel = pObjects.get(key);
			if (!raetsel.isItemsLoaded()) {
				raetselitemsLaden(raetsel);
			}
			if (RaetsellisteGeneratorModus.WEB.equals(pModus)) {
				status = processRaetsel(raetsel, workDir, stammdatenservice, loopProgress.newChild(1), true);
			} else {
				status = processRaetsel(raetsel, workDir, stammdatenservice, loopProgress.newChild(1), false);
			}
			if (!status.isOK()) {
				statusList.add(status);
			}
			loopProgress.worked(1);
		}
		subMonitor.subTask("lösche temporäre Dateien");
		subMonitor.setWorkRemaining(20);
		clearTemporaryLaTeXFiles(pWorkDir);
		subMonitor.worked(100);

		if (!statusList.isEmpty()) {
			MultiStatus result = new MultiStatus(IStammdatenservice.PLUGIN_ID, IStatus.WARNING,
				statusList.toArray(new IStatus[0]), "Beim Generieren sind Fehler aufgetreten", null);
			return result;
		}
		return new Status(IStatus.OK, IStammdatenservice.PLUGIN_ID, "alles paletti :)");
	}

	/**
	 * @param pRaetsel
	 * @param pWorkDir
	 * @param pProgressMonitor
	 * @return
	 */
	private IStatus processRaetsel(MCArchivraetsel pRaetsel, File pWorkDir, IStammdatenservice stammdatenservice,
		IProgressMonitor pProgressMonitor, boolean pWeb) {
		final String pathWorkDir = pWorkDir.getAbsolutePath();
		StringBuffer sb = new StringBuffer("Fehler beim Generieren, Compilieren und Umwandeln:");
		SubMonitor subMonitor = SubMonitor.convert(pProgressMonitor, 80);
		int totalWork = pRaetsel.getItems().size() * 2 + 4;
		if (pWeb) {
			totalWork += 2;
		}
		SubMonitor loopProgress = subMonitor.newChild(80).setWorkRemaining(totalWork);
		List<IStatus> statusList = new ArrayList<>();
		IStatus status = null;
		boolean error = false;
		for (MCArchivraetselItem item : pRaetsel.getItemsSorted()) {
			loopProgress.subTask("generiere " + pRaetsel.getSchluessel() + "/" + item.getNummer());
			status = processItem(item, pWorkDir, pWeb);
			if (!status.isOK()) {
				statusList.add(status);
				error = true;
			}
			loopProgress.worked(2);
		}
		subMonitor.worked(80);

		subMonitor.subTask("generiere Aufgaben Gesamtansicht" + pRaetsel.getSchluessel());
		status = createLaTeX(pRaetsel, pathWorkDir, true);
		if (!status.isOK()) {
			statusList.add(status);
			error = true;
		}
		subMonitor.worked(1);
		subMonitor.subTask("generiere Loesungen Gesamtansicht" + pRaetsel.getSchluessel());
		status = createLaTeX(pRaetsel, pathWorkDir, false);
		if (!status.isOK()) {
			statusList.add(status);
			error = true;
		}
		subMonitor.worked(1);

		subMonitor.subTask("generiere Raetsel-xml fuer Quiz " + pRaetsel.getSchluessel());
		status = generateRaetselFuerQuiz(pRaetsel, pathWorkDir);
		if (!status.isOK()) {
			statusList.add(status);
			error = true;
		}
		subMonitor.worked(1);

		if (pWeb) {
			{
				subMonitor.subTask("generiere Raetsel-Html fuer WEB-Quiz " + pRaetsel.getSchluessel());
				status = createHtml(pRaetsel, pathWorkDir, stammdatenservice);
				if (!status.isOK()) {
					statusList.add(status);
					error = true;
				}
				subMonitor.worked(1);
			}
			{
				subMonitor.subTask("generiere Raetsel-JavaScript fuer WEB-Quiz " + pRaetsel.getSchluessel());
				status = createJS(pRaetsel, pathWorkDir, stammdatenservice);
				if (!status.isOK()) {
					statusList.add(status);
					error = true;
				}
				subMonitor.worked(1);
			}
		}

		return error ? new Status(IStatus.ERROR, IStammdatenservice.PLUGIN_ID, sb.toString()) : new Status(IStatus.OK,
			IStammdatenservice.PLUGIN_ID, "success");
	}

	/**
	 * Erzeugt das xml-File für das Quiz und zip es.
	 *
	 * @param pRaetsel
	 * @param pPathWorkDir
	 * @return
	 */
	private IStatus generateRaetselFuerQuiz(MCArchivraetsel pRaetsel, String pPathWorkDir) {
		Raetsel spielraetesel = Raetsel.createFromArchiv(pRaetsel);
		File xmlFile = new File(pPathWorkDir + "/" + pRaetsel.getSchluessel() + ".xml");
		try {
			final Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.setProperty("jaxb.encoding", "ISO-8859-1");
			marshaller.marshal(spielraetesel, xmlFile);
			return new Status(IStatus.OK, IStammdatenservice.PLUGIN_ID, pRaetsel.getSchluessel() + " spielraetsel ok");
		} catch (JAXBException e) {
			LOG.error(e.getMessage(), e);
			return new Status(IStatus.ERROR, IStammdatenservice.PLUGIN_ID, pRaetsel.getSchluessel()
				+ ": spielraetsel: Marshalling fehlgeschlagen.");
		}

	}

	/**
	 * Generiert das Html-File für das Web-Quiz
	 *
	 * @param raetsel
	 * @param pPathWorkDir
	 * @return
	 */
	protected IStatus createHtml(MCArchivraetsel raetsel, String pPathWorkDir, IStammdatenservice stammdatenservice) {
		final QuizHtmlGenerator generator = new QuizHtmlGenerator(raetsel);
		final String resultFilePath = pPathWorkDir + File.separator + generator.getFilename();
		try {
			MatheAGGeneratorUtils.writeOutput(pPathWorkDir, generator, stammdatenservice);
			return new Status(IStatus.OK, IStammdatenservice.PLUGIN_ID, resultFilePath + " ok");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return new Status(IStatus.ERROR, IStammdatenservice.PLUGIN_ID, resultFilePath
				+ ": Xml-Html-Transformation fehlgeschlagen.");
		}
	}

	/**
	 * Generiert das Js-File für das Web-Quiz
	 *
	 * @param raetsel
	 * @param pPathWorkDir
	 * @return
	 */
	protected IStatus createJS(MCArchivraetsel raetsel, String pPathWorkDir, IStammdatenservice stammdatenservice) {
		final QuizJsGenerator generator = new QuizJsGenerator(raetsel);
		final String resultFilePath = pPathWorkDir + File.separator + generator.getFilename();
		try {
			MatheAGGeneratorUtils.writeOutput(pPathWorkDir, generator, stammdatenservice);
			return new Status(IStatus.OK, IStammdatenservice.PLUGIN_ID, resultFilePath + " ok");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return new Status(IStatus.ERROR, IStammdatenservice.PLUGIN_ID, resultFilePath
				+ ": Xml-Html-Transformation fehlgeschlagen.");
		}
	}

	/**
	 * Generiert das komplette LaTeX-File für das Raetsel.
	 *
	 * @param pRaetsel
	 * @param pPathWorkDir
	 * @param pGenerateAufgabe
	 * @return
	 */
	protected IStatus createLaTeX(MCArchivraetsel pRaetsel, String pPathWorkDir, boolean pGenerateAufgabe) {
		Assert.isTrue(pPathWorkDir != null && !pPathWorkDir.isEmpty(), "pPathOutput darf nicht null oder leer sein.");
		final String pathPrefix = pRaetsel.getPathPrefix(pPathWorkDir);
		String pathResult = pGenerateAufgabe ? GeneratorUtils.getPathPreviewLaTeXAufgabe(pathPrefix) : GeneratorUtils
			.getPathPreviewLaTeXLoesung(pathPrefix);

		InputStream xslResource = getClass().getResourceAsStream(RAETSEL_LATEX_GENERATOR_XSL);

		OutputStream out = null;
		try {
			File xmlTempFile = File.createTempFile("mcraetsel", ".xml");
			xmlTempFile.deleteOnExit();

			final String pLatexTemplatesPath = xmlTempFile.getParent() + LATEX_GENERATOR_TEMPLATES;
			checkLatexTemplates(pLatexTemplatesPath);
			final Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.setProperty("jaxb.encoding", "ISO-8859-1");
			marshaller.marshal(pRaetsel, xmlTempFile);
			File xslTempFile = File.createTempFile("transformation", ".xslt");
			xslTempFile.deleteOnExit();
			out = new FileOutputStream(xslTempFile);
			IOUtils.copy(xslResource, out);
			out.flush();
			Map<String, String> parameters = getXsltParameters(pGenerateAufgabe);
			final XSLProcessor processor = new XSLProcessor();
			processor.transform(xmlTempFile.getAbsolutePath(), xslTempFile.getAbsolutePath(), pathResult, null,
				parameters);
			LOG.info("mit xslt: Ausgabe nach [" + pathResult + "]");
			return new Status(IStatus.OK, IStammdatenservice.PLUGIN_ID, pRaetsel.getSchluessel() + " ok");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return new Status(IStatus.ERROR, IStammdatenservice.PLUGIN_ID, pRaetsel.getSchluessel()
				+ ": Marshalling oder XSL-Transformation fehlgeschlagen.");
		} finally {
			IOUtils.closeQuietly(out);
			IOUtils.closeQuietly(xslResource);
		}
	}

	/**
	 * Seit Apr. 2013 werden die Aufgaben in 2 Varianten generiert: einmal für die RCP-Anwendung, einmal für die
	 * Webanwendung.
	 *
	 * @param pItem
	 * @param pWorkDir
	 * @return
	 */
	private IStatus processItem(MCArchivraetselItem pItem, File pWorkDir, boolean pWeb) {
		final String pathWorkDir = pWorkDir.getAbsolutePath();
		StringBuffer sb = new StringBuffer("Fehler beim Generieren, Compilieren und Umwandeln:");
		IStatus status = createLaTeX(pItem, pathWorkDir, true, pWeb);
		boolean error = false;
		String path = null;
		final String pathPrefix = pItem.getPathPrefix(pathWorkDir);
		if (status.isOK()) {
			path = GeneratorUtils.getPathPreviewLaTeXAufgabe(pathPrefix);
			status = compile(path);
			if (status.isOK()) {
				path = GeneratorUtils.getPathPreviewDviAufgabe(pathPrefix);
				status = dviPs(path);
				if (!status.isOK()) {
					error = true;
					sb.append("\n");
					sb.append(status.getMessage());
				}
			} else {
				error = true;
				sb.append("\n");
				sb.append(status.getMessage());
			}
		} else {
			error = true;
			sb.append("\n");
			sb.append(status.getMessage());
		}
		status = createLaTeX(pItem, pathWorkDir, false, pWeb);
		if (status.isOK()) {
			path = GeneratorUtils.getPathPreviewLaTeXLoesung(pathPrefix);
			status = compile(path);
			if (status.isOK()) {
				path = GeneratorUtils.getPathPreviewDviLoesung(pathPrefix);
				status = dviPs(path);
				if (!status.isOK()) {
					error = true;
					sb.append("\n");
					sb.append(status.getMessage());
				}
			} else {
				error = true;
				sb.append("\n");
				sb.append(status.getMessage());
			}
		} else {
			error = true;
			sb.append("\n");
			sb.append(status.getMessage());
		}
		return error ? new Status(IStatus.ERROR, IStammdatenservice.PLUGIN_ID, sb.toString()) : new Status(IStatus.OK,
			IStammdatenservice.PLUGIN_ID, "success");
	}

	/**
	 * @param pItem
	 * @param pPathWorkDir
	 * @param pGenerateAufgabe
	 * @param pWeb boolean: wenn true, dann die Aufgaben ohne Antworttabelle fürs WebQuizspiel, sonst Aufgabe mit
	 * Antworttabelle fürs RCP-Quizspiel.
	 * @return
	 */
	protected IStatus createLaTeX(MCArchivraetselItem pItem, String pPathWorkDir, boolean pGenerateAufgabe, boolean pWeb) {
		Assert.isTrue(pPathWorkDir != null && !pPathWorkDir.isEmpty(), "pPathOutput darf nicht null oder leer sein.");
		final String pathPrefix = pItem.getPathPrefix(pPathWorkDir);
		String pathResult = pGenerateAufgabe ? GeneratorUtils.getPathPreviewLaTeXAufgabe(pathPrefix) : GeneratorUtils
			.getPathPreviewLaTeXLoesung(pathPrefix);

		InputStream xslResource = null;
		if (pWeb) {
			xslResource = getClass().getResourceAsStream(PLAIN_AUFGABE_LATEX_GENERATOR_XSL);
		} else {
			xslResource = getClass().getResourceAsStream(AUFGABE_LATEX_GENERATOR_XSL);
		}

		OutputStream out = null;
		try {
			File xmlTempFile = File.createTempFile("mcraetselitem", ".xml");
			xmlTempFile.deleteOnExit();
			final String pLatexTemplatesPath = xmlTempFile.getParent() + LATEX_GENERATOR_TEMPLATES;
			checkLatexTemplates(pLatexTemplatesPath);
			final Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.setProperty("jaxb.encoding", "ISO-8859-1");
			marshaller.marshal(pItem, xmlTempFile);
			File xslTempFile = File.createTempFile("transformation", ".xslt");
			xslTempFile.deleteOnExit();
			out = new FileOutputStream(xslTempFile);
			IOUtils.copy(xslResource, out);
			out.flush();
			Map<String, String> parameters = getXsltParameters(pGenerateAufgabe);
			final XSLProcessor processor = new XSLProcessor();
			processor.transform(xmlTempFile.getAbsolutePath(), xslTempFile.getAbsolutePath(), pathResult, null,
				parameters);
			LOG.info("mit xslt: Ausgabe nach [" + pathResult + "]");
			return new Status(IStatus.OK, IStammdatenservice.PLUGIN_ID, pItem.getRaetsel().getSchluessel() + "/"
				+ pItem.getNummer() + " ok");
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return new Status(IStatus.ERROR, IStammdatenservice.PLUGIN_ID, pItem.getRaetsel().getSchluessel() + "/"
				+ pItem.getNummer() + ": Marshalling oder XSL-Transformation fehlgeschlagen.");
		} finally {
			IOUtils.closeQuietly(out);
			IOUtils.closeQuietly(xslResource);
		}
	}
}
