/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.labelprovider;


/**
 * Ein mit leeren Anfangs- und End-Tags.
 *
 * @author winkelv
 */
public class BlankTableLabelProvider implements ITableLabelProvider {

	/**
	 *
	 */
	public BlankTableLabelProvider() {
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathe.core.serviceimpl.generatoren.labelprovider.ITableLabelProvider#getEndText()
	 */
	@Override
	public String getEndText() {
		return "";
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathe.core.serviceimpl.generatoren.labelprovider.ITableLabelProvider#getStartText()
	 */
	@Override
	public String getStartText() {
		return "";
	}

}
