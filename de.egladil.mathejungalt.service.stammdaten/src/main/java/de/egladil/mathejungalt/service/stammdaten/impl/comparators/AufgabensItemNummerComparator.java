//=====================================================
// Projekt: de.egladil.mathejungalt.service.stammdaten
// (c) Heike Winkelvoß
//=====================================================
// Dateiname: AufgabensammlungItemNummerComparator.java                            $
// $Revision:: 1                                     $
// $Modtime:: 30.03.2015                                $
//=====================================================

package de.egladil.mathejungalt.service.stammdaten.impl.comparators;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.DomainObjectProxyClassifier;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;

/**
 * AufgabensammlungItemNummerComparator
 */
public class AufgabensItemNummerComparator extends AbstractMatheAGObjectComparator {

	/**
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.comparators.AbstractMatheAGObjectComparator#specialCompare(de.egladil.mathejungalt.domain.AbstractMatheAGObject,
	 * de.egladil.mathejungalt.domain.AbstractMatheAGObject)
	 */
	@Override
	protected int specialCompare(AbstractMatheAGObject pO1, AbstractMatheAGObject pO2) {
		IAufgabensammlungItem i1 = (IAufgabensammlungItem) pO1;
		IAufgabensammlungItem i2 = (IAufgabensammlungItem) pO2;
		return i1.getNummer().compareTo(i2.getNummer());
	}

	/**
	 * @see de.egladil.mathejungalt.service.stammdaten.impl.comparators.AbstractMatheAGObjectComparator#specialCheckType(de.egladil.mathejungalt.domain.AbstractMatheAGObject,
	 * de.egladil.mathejungalt.domain.AbstractMatheAGObject)
	 */
	@Override
	protected void specialCheckType(AbstractMatheAGObject pO1, AbstractMatheAGObject pO2) {
		if (!DomainObjectProxyClassifier.isAufgabensammlungItem(pO1)
			&& !DomainObjectProxyClassifier.isAufgabensammlungItem(pO2)) {
			throw new MatheJungAltException("Falscher Type: keine " + IAufgabensammlungItem.class.getName());
		}
	}

}
