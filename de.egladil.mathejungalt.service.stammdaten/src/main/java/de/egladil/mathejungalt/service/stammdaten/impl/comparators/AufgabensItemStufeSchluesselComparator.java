/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.comparators;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;

/**
 * @author Winkelv
 */
public class AufgabensItemStufeSchluesselComparator extends AbstractMatheAGObjectComparator {

	/** */
	private AufgabenStufeSchluesselComparator aufgabenstufeSchluesselComparator;

	/**
	 *
	 */
	public AufgabensItemStufeSchluesselComparator() {
		aufgabenstufeSchluesselComparator = new AufgabenStufeSchluesselComparator();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathe.core.serviceimpl.internal.comparators.AbstractMatheAGObjectComparator#specialCheckType(de.egladil
	 * .mathe. core.domain.AbstractMatheAGObject, de.egladil.mathe.core.domain.AbstractMatheAGObject)
	 */
	@Override
	protected void specialCheckType(AbstractMatheAGObject pO1, AbstractMatheAGObject pO2) {
		if (!(pO1 instanceof IAufgabensammlungItem) && !(pO2 instanceof IAufgabensammlungItem)) {
			throw new MatheJungAltException("Falscher Typ: kein " + IAufgabensammlungItem.class.getName());
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathe.core.serviceimpl.internal.comparators.AbstractMatheAGObjectComparator#specialCompare(de.egladil
	 * .mathe.core .domain.AbstractMatheAGObject, de.egladil.mathe.core.domain.AbstractMatheAGObject)
	 */
	@Override
	protected int specialCompare(AbstractMatheAGObject pO1, AbstractMatheAGObject pO2) {
		IAufgabensammlungItem item1 = (IAufgabensammlungItem) pO1;
		IAufgabensammlungItem item2 = (IAufgabensammlungItem) pO2;
		return aufgabenstufeSchluesselComparator.compare(item1.getAufgabe(), item2.getAufgabe());
	}
}
