/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.consistence;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.exceptions.MatheJungAltInconsistentEntityException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.DomainObjectProxyClassifier;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenart;
import de.egladil.mathejungalt.domain.types.EnumTypes.Quellenart;

/**
 * @author Heike Winkelvoss (www.egladil.de)
 */
public class AufgabeConsistentceChecker implements IConsistencecheckProvider {

	/** */
	private final ConsistenceCheckDelegate consistenceCheckDelegate;

	/**
	 *
	 */
	public AufgabeConsistentceChecker() {
		consistenceCheckDelegate = new ConsistenceCheckDelegate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.consistencecheck.IConsistencecheckProvider#canCheck(de.egladil.mathejungalt.domain
	 * .AbstractMatheAGObject)
	 */
	@Override
	public boolean canCheck(Object pObject) {
		return DomainObjectProxyClassifier.isAufgabe(pObject);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.consistencecheck.IConsistencecheckProvider#checkConsistent(de.egladil.mathejungalt
	 * .domain.AbstractMatheAGObject)
	 */
	@Override
	public void checkConsistent(AbstractMatheAGObject pObject) throws MatheJungAltException {
		StringBuffer sb = new StringBuffer();
		boolean consistent = true;
		Aufgabe aufgabe = (Aufgabe) pObject;
		try {
			consistenceCheckDelegate.checkStringKey(aufgabe.getKey(), 5, "");
		} catch (MatheJungAltInconsistentEntityException e) {
			sb.append(e.getMessage());
			consistent = false;
		}
		if (aufgabe.getArt() == null) {
			sb.append("\ndie Aufgabenart darf nicht null sein");
			consistent = false;
		}
		if (aufgabe.getThema() == null) {
			sb.append("\ndas Thema darf nicht null sein");
			consistent = false;
		}
		if (aufgabe.getTitel() == null || aufgabe.getTitel().length() == 0 || aufgabe.getTitel().length() > 100) {
			sb.append("\nder Titel darf nicht null oder leer sein und nicht laenger als 100 Zeichen");
			consistent = false;
		}
		if (aufgabe.getBeschreibung() != null && aufgabe.getBeschreibung().length() > 255) {
			sb.append("\ndie Beschreibung darf nicht laenger als 255 Zeichen sein");
			consistent = false;
		}
		if (aufgabe.getStufe() == null || aufgabe.getStufe().intValue() <= 0 || aufgabe.getStufe().intValue() >= 7) {
			sb.append("\ndie Stufe darf nicht null sein und muss zwischen 1 und 6 liegen");
			consistent = false;
		}
		if (aufgabe.getQuelle() == null) {
			sb.append("\ndie Quelle darf nicht null sein");
			consistent = false;
		}
		if ((Aufgabenart.N.equals(aufgabe.getArt()) || Aufgabenart.Z.equals(aufgabe.getArt()))
			&& Quellenart.N.equals(aufgabe.getQuelle().getArt())) {
			sb.append("\nZitat oder Nachbau benötigt eine von der Nullquelle verschiedene Quelle!");
			consistent = false;
		}
		if (Aufgabenart.E.equals(aufgabe.getArt()) && !(Quellenart.N.equals(aufgabe.getQuelle().getArt()))) {
			sb.append("\nBei Eigenbau ist nur Nullquelle erlaubt");
			consistent = false;
		}
		if (!consistent) {
			throw new MatheJungAltInconsistentEntityException("Aufgabe:\n" + sb.toString());
		}
	}
}
