<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text" indent="no" omit-xml-declaration="yes" />

	<xsl:template name="latexStart">
\documentclass[12pt,a4paper,oneside,german]{article}
\input{../include/vorspann}
\input{../include/commands}
		<xsl:choose>
			<xsl:when test="$pagestyle='empty'">
\pagestyle{empty}
			</xsl:when>
			<xsl:otherwise>
\markright{
				<xsl:value-of select="$autor" />
- Lizenz:
				<xsl:value-of select="$licence" />
\hfill}
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="headline">
		<xsl:param name="stufe">
			call headline: Stufe vergessen
		</xsl:param>
		<xsl:param name="titel">
			Titel vergessen
		</xsl:param>
		<xsl:param name="nummer">
			12345
		</xsl:param>
		<xsl:choose>
			<xsl:when test="$stufe='STUFE_12'">
{<xsl:call-template name="font"><xsl:with-param name="weight" select="'b'" /></xsl:call-template><xsl:value-of select="$titel" /><xsl:call-template name="whitespace" /><xsl:value-of select="$nummer" />:}\par
			</xsl:when>
			<xsl:otherwise>
{\bf <xsl:value-of select="$titel" /><xsl:call-template name="whitespace" /><xsl:value-of select="$nummer" />:}\par
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="latexEnd">
\end{document}
	</xsl:template>

	<xsl:template name="inputAufgabe">
		<xsl:param name="stufe">
			inputAufgabe: stufe vergessen
		</xsl:param>
		<xsl:param name="verzeichnis">
			inputAufgabe: verzeichnis vergessen
		</xsl:param>
		<xsl:param name="schluessel">
			inputAufgabe: schluessel vergessen
		</xsl:param>
		<xsl:choose>
			<xsl:when test="$stufe='STUFE_12'">
				<xsl:call-template name="font">
					<xsl:with-param name="weight" select="'m'" />
				</xsl:call-template>
			</xsl:when>
		</xsl:choose>
		<xsl:text>
\input{../mcaufgabenarchiv/</xsl:text><xsl:value-of select="$verzeichnis" />/mc_<xsl:value-of select="$schluessel" />
		<xsl:text>}\par</xsl:text>
	</xsl:template>

	<xsl:template name="inputLoesung">
		<xsl:param name="stufe">
			inputLoesung: stufe vergessen
		</xsl:param>
		<xsl:param name="verzeichnis">
			inputLoesung: verzeichnis vergessen
		</xsl:param>
		<xsl:param name="schluessel">
			inputLoesung: schluessel vergessen
		</xsl:param>
		<xsl:choose>
			<xsl:when test="$stufe='STUFE_12'">
				<xsl:call-template name="font">
					<xsl:with-param name="weight" select="'m'" />
				</xsl:call-template>
			</xsl:when>
		</xsl:choose>
		<xsl:text>
\input{../mcloesungsarchiv/</xsl:text><xsl:value-of select="$verzeichnis" />/mc_<xsl:value-of select="$schluessel" /><xsl:text>_l}\par</xsl:text>
	</xsl:template>

	<xsl:template name="tableStart">
		<xsl:call-template name="hlines" />
\par
\begin{tabular}{*{2}{l}}
	</xsl:template>

	<xsl:template name="tableEnd">
\end{tabular}
\par
		<xsl:call-template name="hlines" />
	</xsl:template>


	<xsl:template name="tableRow">
		<xsl:param name="stufe">
			call tableRow: Stufe vergessen
		</xsl:param>
		<xsl:param name="buchstabe" select="'A'" />
		<xsl:param name="wert" select="'Wert'" />
		<xsl:param name="letzte" select="'false'" />
		<xsl:choose>
			<xsl:when test="$stufe='STUFE_12'">
{<xsl:call-template name="font"><xsl:with-param name="weight" select="'b'" /></xsl:call-template>(<xsl:value-of select="$buchstabe" />)}<xsl:call-template name="and" />
			</xsl:when>
			<xsl:otherwise>
{\bf (<xsl:value-of select="$buchstabe" />)}<xsl:call-template name="and" />
			</xsl:otherwise>
		</xsl:choose>
		<xsl:value-of select="$wert" />
		<xsl:choose>
			<xsl:when test="$letzte='true'" />
			<xsl:otherwise>
				<xsl:call-template name="rowdistance" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="loesungsbuchstabe">
		<xsl:param name="stufe">
			call loesungsbuchstabe: Stufe vergessen
		</xsl:param>
		<xsl:param name="wert" select="'Wert'" />
		<xsl:choose>
			<xsl:when test="$stufe='STUFE_12'">
				\fbox{{
				<xsl:call-template name="font">
					<xsl:with-param name="weight" select="'m'" />
				</xsl:call-template>
				Lösungsbuchstabe: }
				<xsl:call-template name="whitespace" />
				<xsl:value-of select="$wert" />
				}
			</xsl:when>
			<xsl:otherwise>
				\fbox{Lösungsbuchstabe:
				<xsl:call-template name="whitespace" />
				<xsl:value-of select="$wert" />
				}
			</xsl:otherwise>
		</xsl:choose>
\par
	</xsl:template>

	<xsl:template name="and">
		<xsl:text> <![CDATA[&]]> </xsl:text>
	</xsl:template>

	<xsl:template name="rowdistance">
		<xsl:text> <![CDATA[\\[1ex]]]> </xsl:text>
	</xsl:template>

	<xsl:template name="whitespace">
		<xsl:text> <![CDATA[ ]]> </xsl:text>
	</xsl:template>

	<xsl:template name="hlines">
<xsl:text> <![CDATA[\rule{16cm}{1pt}]]> </xsl:text>
	</xsl:template>

	<xsl:template name="font">
		<xsl:param name="weight" select="'b'" />
		<xsl:param name="direction" select="'n'" />
\changefont{<xsl:value-of select="$fontfamily" />}{<xsl:value-of select="$weight" />}{<xsl:value-of select="$direction" />}
	</xsl:template>

	<xsl:template name="latexAufgabe">
		<xsl:param name="stufe">
			call latexAufgabe: Stufe vergessen
		</xsl:param>
		<xsl:param name="verzeichnis" select="'verzeichnis vergessen'" />
		<xsl:param name="nummer" select="'nummer vergessen'" />
		<xsl:param name="punkte" select="'keine'" />
		<xsl:param name="schluessel" select="'schluessel vergessen'" />
		<xsl:param name="tabelleGenerieren" select="'tabelleGenerieren vergessen'" />
		<xsl:param name="anzahlAntwortvorschlaege" select="'anzahlAntwortvorschlaege vergessen'" />
		<xsl:param name="loesungsbuchstabe" select="'loesungsbuchstabe vergessen'" />
		<xsl:param name="antwortA" select="'antwortA vergessen'" />
		<xsl:param name="antwortB" select="'antwortB vergessen'" />
		<xsl:param name="antwortC" select="'antwortC vergessen'" />
		<xsl:param name="antwortD" select="'antwortD vergessen'" />
		<xsl:param name="antwortE" select="'antwortE vergessen'" />
		<xsl:choose>
			<xsl:when test="$latexModus='aufgabe'">
				<xsl:choose>
					<xsl:when test="$punkte='keine'">
						<xsl:call-template name="headline">
							<xsl:with-param name="stufe">
								<xsl:value-of select="$stufe" />
							</xsl:with-param>
							<xsl:with-param name="titel" select="'Aufgabe'" />
							<xsl:with-param name="nummer">
								<xsl:value-of select="$nummer" />
							</xsl:with-param>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="headline">
							<xsl:with-param name="stufe">
								<xsl:value-of select="$stufe" />
							</xsl:with-param>
							<xsl:with-param name="titel" select="'Aufgabe'" /><xsl:with-param name="nummer"><xsl:value-of select="$nummer" /><xsl:call-template name="whitespace" />(<xsl:value-of select="$punkte" /><xsl:call-template name="whitespace" />Punkte)</xsl:with-param></xsl:call-template>
					   </xsl:otherwise>
				</xsl:choose>

				<xsl:call-template name="inputAufgabe">
					<xsl:with-param name="stufe">
						<xsl:value-of select="$stufe" />
					</xsl:with-param>
					<xsl:with-param name="verzeichnis">
						<xsl:value-of select="$verzeichnis" />
					</xsl:with-param>
					<xsl:with-param name="schluessel">
						<xsl:value-of select="$schluessel" />
					</xsl:with-param>
				</xsl:call-template>

				<xsl:choose>
					<xsl:when test="$tabelleGenerieren='true'">
						<xsl:call-template name="tableStart" />

						<xsl:choose>

							<xsl:when test="$anzahlAntwortvorschlaege='2'">
								<xsl:call-template name="tableRow">
									<xsl:with-param name="stufe">
										<xsl:value-of select="$stufe" />
									</xsl:with-param>
									<xsl:with-param name="buchstabe" select="'A'" />
									<xsl:with-param name="wert">
										<xsl:value-of select="$antwortA" />
									</xsl:with-param>
								</xsl:call-template>

								<xsl:call-template name="tableRow">
									<xsl:with-param name="stufe">
										<xsl:value-of select="$stufe" />
									</xsl:with-param>
									<xsl:with-param name="buchstabe" select="'B'" />
									<xsl:with-param name="wert">
										<xsl:value-of select="$antwortB" />
									</xsl:with-param>
									<xsl:with-param name="letzte" select="'true'" />
								</xsl:call-template>
							</xsl:when>

							<xsl:when test="$anzahlAntwortvorschlaege='3'">
								<xsl:call-template name="tableRow">
									<xsl:with-param name="stufe">
										<xsl:value-of select="$stufe" />
									</xsl:with-param>
									<xsl:with-param name="buchstabe" select="'A'" />
									<xsl:with-param name="wert">
										<xsl:value-of select="$antwortA" />
									</xsl:with-param>
								</xsl:call-template>

								<xsl:call-template name="tableRow">
									<xsl:with-param name="stufe">
										<xsl:value-of select="$stufe" />
									</xsl:with-param>
									<xsl:with-param name="buchstabe" select="'B'" />
									<xsl:with-param name="wert">
										<xsl:value-of select="$antwortB" />
									</xsl:with-param>
								</xsl:call-template>

								<xsl:call-template name="tableRow">
									<xsl:with-param name="stufe">
										<xsl:value-of select="$stufe" />
									</xsl:with-param>
									<xsl:with-param name="buchstabe" select="'C'" />
									<xsl:with-param name="wert">
										<xsl:value-of select="$antwortC" />
									</xsl:with-param>
									<xsl:with-param name="letzte" select="'true'" />
								</xsl:call-template>
							</xsl:when>

							<xsl:when test="$anzahlAntwortvorschlaege='4'">
								<xsl:call-template name="tableRow">
									<xsl:with-param name="stufe">
										<xsl:value-of select="$stufe" />
									</xsl:with-param>
									<xsl:with-param name="buchstabe" select="'A'" />
									<xsl:with-param name="wert">
										<xsl:value-of select="$antwortA" />
									</xsl:with-param>
								</xsl:call-template>

								<xsl:call-template name="tableRow">
									<xsl:with-param name="stufe">
										<xsl:value-of select="$stufe" />
									</xsl:with-param>
									<xsl:with-param name="buchstabe" select="'B'" />
									<xsl:with-param name="wert">
										<xsl:value-of select="$antwortB" />
									</xsl:with-param>
								</xsl:call-template>

								<xsl:call-template name="tableRow">
									<xsl:with-param name="stufe">
										<xsl:value-of select="$stufe" />
									</xsl:with-param>
									<xsl:with-param name="buchstabe" select="'C'" />
									<xsl:with-param name="wert">
										<xsl:value-of select="$antwortC" />
									</xsl:with-param>
								</xsl:call-template>

								<xsl:call-template name="tableRow">
									<xsl:with-param name="stufe">
										<xsl:value-of select="$stufe" />
									</xsl:with-param>
									<xsl:with-param name="buchstabe" select="'D'" />
									<xsl:with-param name="wert">
										<xsl:value-of select="$antwortD" />
									</xsl:with-param>
									<xsl:with-param name="letzte" select="'true'" />
								</xsl:call-template>
							</xsl:when>

							<xsl:otherwise>
% hier in 5
								<xsl:call-template name="tableRow">
									<xsl:with-param name="stufe">
										<xsl:value-of select="$stufe" />
									</xsl:with-param>
									<xsl:with-param name="buchstabe" select="'A'" />
									<xsl:with-param name="wert">
										<xsl:value-of select="$antwortA" />
									</xsl:with-param>
								</xsl:call-template>

								<xsl:call-template name="tableRow">
									<xsl:with-param name="stufe">
										<xsl:value-of select="$stufe" />
									</xsl:with-param>
									<xsl:with-param name="buchstabe" select="'B'" />
									<xsl:with-param name="wert">
										<xsl:value-of select="$antwortB" />
									</xsl:with-param>
								</xsl:call-template>

								<xsl:call-template name="tableRow">
									<xsl:with-param name="stufe">
										<xsl:value-of select="$stufe" />
									</xsl:with-param>
									<xsl:with-param name="buchstabe" select="'C'" />
									<xsl:with-param name="wert">
										<xsl:value-of select="$antwortC" />
									</xsl:with-param>
								</xsl:call-template>

								<xsl:call-template name="tableRow">
									<xsl:with-param name="stufe">
										<xsl:value-of select="$stufe" />
									</xsl:with-param>
									<xsl:with-param name="buchstabe" select="'D'" />
									<xsl:with-param name="wert">
										<xsl:value-of select="$antwortD" />
									</xsl:with-param>
								</xsl:call-template>

								<xsl:call-template name="tableRow">
									<xsl:with-param name="stufe">
										<xsl:value-of select="$stufe" />
									</xsl:with-param>
									<xsl:with-param name="buchstabe" select="'E'" />
									<xsl:with-param name="wert">
										<xsl:value-of select="$antwortE" />
									</xsl:with-param>
									<xsl:with-param name="letzte" select="'true'" />
								</xsl:call-template>
							</xsl:otherwise>
						</xsl:choose>

						<xsl:call-template name="tableEnd" />
					</xsl:when>
					<xsl:otherwise>
						\vertbox{0.5cm}
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="headline">
					<xsl:with-param name="stufe">
						<xsl:value-of select="$stufe" />
					</xsl:with-param>
					<xsl:with-param name="titel" select="'Lösung zu Aufgabe'" />
					<xsl:with-param name="nummer">
						<xsl:value-of select="$nummer" />
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="loesungsbuchstabe">
					<xsl:with-param name="stufe">
						<xsl:value-of select="$stufe" />
					</xsl:with-param>
					<xsl:with-param name="wert">
						<xsl:value-of select="$loesungsbuchstabe" />
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="inputLoesung">
					<xsl:with-param name="stufe">
						<xsl:value-of select="$stufe" />
					</xsl:with-param>
					<xsl:with-param name="verzeichnis">
						<xsl:value-of select="$verzeichnis" />
					</xsl:with-param>
					<xsl:with-param name="schluessel">
						<xsl:value-of select="$schluessel" />
					</xsl:with-param>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>


	<xsl:template name="latexPlainAufgabe">
		<xsl:param name="stufe">
			call latexAufgabe: Stufe vergessen
		</xsl:param>
		<xsl:param name="verzeichnis" select="'verzeichnis vergessen'" />
		<xsl:param name="nummer" select="'nummer vergessen'" />
		<xsl:param name="punkte" select="'keine'" />
		<xsl:param name="schluessel" select="'schluessel vergessen'" />
		<xsl:param name="loesungsbuchstabe" select="'loesungsbuchstabe vergessen'" />
		<xsl:with-param name="stufe" select="stufe"/>
		<xsl:choose>
			<xsl:when test="$latexModus='aufgabe'">
				<xsl:choose>
					<xsl:when test="$punkte='keine'">
						<xsl:call-template name="headline">
							<xsl:with-param name="stufe">
								<xsl:value-of select="$stufe" />
							</xsl:with-param>
							<xsl:with-param name="titel" select="'Aufgabe'" />
							<xsl:with-param name="nummer">
								<xsl:value-of select="$nummer" />
							</xsl:with-param>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="headline">
							<xsl:with-param name="stufe">
								<xsl:value-of select="$stufe" />
							</xsl:with-param>
							<xsl:with-param name="titel" select="'Aufgabe'" />
							<xsl:with-param name="nummer">
								<xsl:value-of select="$nummer" />
								<xsl:call-template name="whitespace" />
								(
								<xsl:value-of select="$punkte" />
								<xsl:call-template name="whitespace" />
								Punkte)
							</xsl:with-param>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>

				<xsl:call-template name="inputAufgabe">
					<xsl:with-param name="stufe">
						<xsl:value-of select="$stufe" />
					</xsl:with-param>
					<xsl:with-param name="verzeichnis">
						<xsl:value-of select="$verzeichnis" />
					</xsl:with-param>
					<xsl:with-param name="schluessel">
						<xsl:value-of select="$schluessel" />
					</xsl:with-param>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="headline">
					<xsl:with-param name="stufe">
						<xsl:value-of select="$stufe" />
					</xsl:with-param>
					<xsl:with-param name="titel" select="'Lösung zu Aufgabe'" />
					<xsl:with-param name="nummer">
						<xsl:value-of select="$nummer" />
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="loesungsbuchstabe">
					<xsl:with-param name="stufe">
						<xsl:value-of select="$stufe" />
					</xsl:with-param>
					<xsl:with-param name="wert">
						<xsl:value-of select="$loesungsbuchstabe" />
					</xsl:with-param>
				</xsl:call-template>
				<xsl:call-template name="inputLoesung">
					<xsl:with-param name="stufe">
						<xsl:value-of select="$stufe" />
					</xsl:with-param>
					<xsl:with-param name="verzeichnis">
						<xsl:value-of select="$verzeichnis" />
					</xsl:with-param>
					<xsl:with-param name="schluessel">
						<xsl:value-of select="$schluessel" />
					</xsl:with-param>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

<!-- Fuer ANDROID -->
	<xsl:template name="quizStartLatex">
\documentclass[oneside,german]{article}
\input{../include/vorspann}
\input{../include/commands}
	</xsl:template>

	<xsl:template name="inputAufgabeWithoutHeader">
		<xsl:param name="verzeichnis">
			inputAufgabe: verzeichnis vergessen
		</xsl:param>
		<xsl:param name="schluessel">
			inputAufgabe: schluessel vergessen
		</xsl:param>
		<xsl:text>
\input{../mcaufgabenarchiv/</xsl:text><xsl:value-of select="$verzeichnis" />/mc_<xsl:value-of select="$schluessel" />
		<xsl:text>}</xsl:text>
	</xsl:template>

	<xsl:template name="quizLatexAufgabe">
		<xsl:param name="verzeichnis" select="'verzeichnis vergessen'" />
		<xsl:param name="schluessel" select="'schluessel vergessen'" />
		<xsl:call-template name="inputAufgabeWithoutHeader">
			<xsl:with-param name="verzeichnis">
				<xsl:value-of select="$verzeichnis" />
			</xsl:with-param>
			<xsl:with-param name="schluessel">
				<xsl:value-of select="$schluessel" />
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>

</xsl:stylesheet>