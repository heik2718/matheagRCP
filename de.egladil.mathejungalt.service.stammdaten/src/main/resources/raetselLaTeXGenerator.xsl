<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text" indent="no" omit-xml-declaration="yes" />
	<xsl:import href="laTeXTemplates.xsl"/>


    <!-- latexModus kann die werte 'aufgabe' oder 'loesung' annehmen -->
    <xsl:param name="latexModus"/>
    <xsl:param name="fontfamily">pag</xsl:param>
    <xsl:param name="pagestyle">headline</xsl:param>
	<xsl:param name="autor">Heike Winkelvoß (www.egladil.de)</xsl:param>
	<xsl:param name="licence">CC-BY-SA</xsl:param>

	<xsl:template match="/">
    	<xsl:call-template name="latexStart"/>
    	<xsl:apply-templates/>
     	<xsl:call-template name="latexEnd"/>
    </xsl:template>

    <xsl:template match="mcMatheRaetsel">
    	<xsl:call-template name="raetselPart">
    		<xsl:with-param name="titel" select="titel"/>
    		<xsl:with-param name="stufe" select="stufe"/>
    	</xsl:call-template>
    	<xsl:for-each select="mcRaetselItem">
			<xsl:call-template name="latexAufgabe">
				<xsl:with-param name="verzeichnis" select="aufgabe/verzeichnis"/>
				<xsl:with-param name="nummer" select="nummer"/>
				<xsl:with-param name="punkte" select="aufgabe/punkte"/>
				<xsl:with-param name="schluessel" select="aufgabe/schluessel"/>
				<xsl:with-param name="tabelleGenerieren" select="aufgabe/tabelleGenerieren"/>
				<xsl:with-param name="anzahlAntwortvorschlaege" select="aufgabe/anzahlAntwortvorschlaege"/>
				<xsl:with-param name="loesungsbuchstabe" select="aufgabe/loesung"/>
				<xsl:with-param name="antwortA" select="aufgabe/antwortA"/>
				<xsl:with-param name="antwortB" select="aufgabe/antwortB"/>
				<xsl:with-param name="antwortC" select="aufgabe/antwortC"/>
				<xsl:with-param name="antwortD" select="aufgabe/antwortD"/>
				<xsl:with-param name="antwortE" select="aufgabe/antwortE"/>
				<xsl:with-param name="stufe" select="aufgabe/stufe"/>
		</xsl:call-template>
    	</xsl:for-each>
    </xsl:template>

    <xsl:template name="raetselPart">
        <xsl:param name="stufe">Stufe vergessen</xsl:param>
    	<xsl:param name="titel">Titel vergessen</xsl:param>
\begin{document}
\begin{minipage}{2cm}
\input{../branding/aglogo_level_4_20mm}
\end{minipage}
\begin{minipage}[c]{10cm}
\begin{center}
		<xsl:choose>
			<xsl:when test="$stufe='STUFE_12'">
{<xsl:call-template name="font"><xsl:with-param name="weight" select="'b'"/></xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
{
			</xsl:otherwise>
		</xsl:choose>
\LARGE <xsl:value-of select="$titel"/><xsl:call-template name="whitespace"/>
\\
		<xsl:choose>
    		<xsl:when test="$stufe='STUFE_12'"><xsl:text>(Klassen 1 und 2)</xsl:text></xsl:when>
    		<xsl:when test="$stufe='STUFE_34'"><xsl:text>(Klassen 3 und 4)</xsl:text></xsl:when>
    		<xsl:when test="$stufe='STUFE_56'"><xsl:text>(Klassen 5 und 6)</xsl:text></xsl:when>
    		<xsl:when test="$stufe='STUFE_78'"><xsl:text>(Klassen 7 und 8)</xsl:text></xsl:when>
    		<xsl:when test="$stufe='STUFE_910'"><xsl:text>(Klassen 9 und 10)</xsl:text></xsl:when>
    		<xsl:otherwise><xsl:text>(Erwachsene)</xsl:text></xsl:otherwise>
    	</xsl:choose>
}
\end{center}
\end{minipage}
\begin{minipage}{3.5cm}
\bild{3.0cm}{../resources/001/00140.eps}
\end{minipage}
\par
		<xsl:choose>
			<xsl:when test="$stufe='STUFE_12'">
			<xsl:call-template name="font"><xsl:with-param name="weight" select="'m'"/></xsl:call-template>
		</xsl:when>
	</xsl:choose>
    </xsl:template>

</xsl:stylesheet>