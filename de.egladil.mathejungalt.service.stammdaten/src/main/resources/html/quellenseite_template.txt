<!DOCTYPE html>
<html lang="de">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Virtuelle Mathe- AG von Mathe f&uuml;r jung und alt</title>
<link
	rel="shortcut icon"
	href="../illustrationen/favicon.ico" />
<meta name="robots" content="index, follow">
<meta
	name="allow-search"
	content="YES" />
<meta
	name="description"
	content="Minik&auml;nguru-Wettbewerb f&uuml;r Erst- und Zweitkl&auml;ssler" />
<meta
	name="keywords"
	lang="de"
	content="Mathematik, Mathe, R&auml;tsel, Knobelaufgaben, Wettbewerb, hochbegabt, Arbeitsbl&auml;tter, Knobeleien, K&auml;nguruwettbewerb, Minik&auml;nguru" />
<meta
	name="keywords"
	lang="en"
	content="mathematics, mathematical riddles, mazes, gifted" />
<meta
	name="classification"
	content="Bildung, Schule, Wissen, Mathematik, Unterhaltung, Freizeit, Hochbegabung, K&auml;nguruwettbewerb, Minik&auml;nguru" />
<!--[if lt IE 9]>
<script src="../js/html5shiv.js"></script>
<![endif]-->

<link rel="stylesheet" href="../../css/bootstrap.min.css">
<script src="../../js/jquery-1.11.3.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<link rel="stylesheet" href="../../css/mja-custom.css">
<link rel="stylesheet" href="../css/ag-custom.css">
</head>
<body>
<!-- Fixierte Navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navigation" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Navigation ein-/ausblenden</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
		  <a class="navbar-brand" href="../index.html" alt="Startseite"><img src="../../img/mja_logo_3.svg" style="width:35px"></a>
        </div>
        <div id="main-navigation"  role="navigation" class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-left">
				<li><a href="../serie.html">Serie</a></li>
				<li><a href="../archiv.html">Archiv</a></li>
				<li class="dropdown">
					<a href="#" data-toggle="dropdown" class="dropdown-toggle">Punkte <b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="../mitglieder.html">Mitglieder</a></li>
						<li><a href="../fruehstarter.html">Fr&uuml;hstarter</a></li>
						<li><a href="../pfiffis.html">Pfiffikusse</a></li>
						<li><a href="../preise.html">Preise</a></li>
					</ul>
				</li>
				<li><a href="../ablauf.html">Ablauf</a></li>
				<li><a href="../download.html">Materialien</a></li>
				<li><a href="../datenschutz.html">Datenschutz</a></li>
		  </ul>
          <ul class="nav navbar-nav navbar-right">
			<li><a href="../../links.html" target="_blank">Links</a></li>
			<li><a href="http://mathe-jung-alt.de/minikaenguru/index.html" target="_blank">Minik&auml;nguru</a></li>
			<li><a href="http://www.egladil.de/quiz/index.html"	target="blank">Mathequiz</a></li>
			<li><a href="#top" alt"Seitenanfang"><span class="glyphicon glyphicon-arrow-up"></span></a></li>
			<li><a href="#bottom" alt="Seitenende"><span class="glyphicon glyphicon-arrow-down"></span></a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
<!-- Part 1: Wrap all page content here -->
<div id="top"></div>
<div id="wrap">
	<!-- preamble -->
	<div class="container boxed">
	       <div class="row">
			<div class="col-sm-2">
				<a href="../index.html" alt="zur Startseite"><img src="../../img/mja_logo_2.svg" class="img-responsive"alt="logo"></a>
			</div>

			<div class="col-sm-8">
				<div>
					<h1 class="huge">Virtuelle Mathe- AG</h1>
					<p class="lead">PLATZHALTER_UNTERTITEL</p>
				</div>
			</div>
			<div class="col-sm-2">
				<img src="../../img/mja_bunt.png" class="img-responsive" width="50pt" alt="logo">
			</div>

		</div>
	</div> <!--/.preamble-->

	<div class="container boxed">
		<div class="row">
			<div class="col-md-12">
			<section>
				<h2 class="sr-only">Aufgabenquellen zur Serie</h2>
				IMGLINK
			</section>
			</div>
		</div>
	</div>
</div>

<!-- start footer -->
<div id="bottom"></div>
<nav class="navbar navbar-default">
    <div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#footer">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		  <a class="navbar-brand" href="../index.html" alt="Startseite"><img src="../../img/mja_logo_3.svg" style="width:35px"></a>
		</div>
		<div role="navigation" class="collapse navbar-collapse" id="footer">
			<ul class="nav navbar-nav">
				<li><a href="../../impressum.html" target="_blank">Impressum</a></li>
				<li><a href="../quellen.html">Quellen</a></li>
				<li><a href="../../lizenz.html" target="_blank">Lizenz</a></li>
				<li><a href="../../disclaimer.html" target="_blank">Disclaimer</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#top"><span class="glyphicon glyphicon-arrow-up"></span></a></li>
			</ul>
		</div>
		<div class="row">
		    <div class="col-md-6 text-left">Heike Winkelvo&szlig; (CC-BY-SA)</div>
			<div class="col-md-6 text-right">Letzte &Auml;nderung: DD.MM.YYYY</div>
		</div>
	</div>
    </nav>
</body>
</html>