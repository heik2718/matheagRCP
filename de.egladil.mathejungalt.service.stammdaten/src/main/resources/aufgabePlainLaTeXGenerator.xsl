<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text" indent="no" omit-xml-declaration="yes" />
	<xsl:include href="laTeXTemplates.xsl"/>

    <!-- latexModus kann die werte 'aufgabe' oder 'loesung' annehmen -->
    <xsl:param name="latexModus"/>
    <xsl:param name="fontfamily">pag</xsl:param>
    <xsl:param name="pagestyle">empty</xsl:param>
	<xsl:param name="autor">Heike Winkelvoß (www.egladil.de)</xsl:param>
	<xsl:param name="licence">CC-BY-SA</xsl:param>

    <xsl:template match="/">
    	<xsl:call-template name="latexStart"/>
\begin{document}
     	<xsl:apply-templates/>
     	<xsl:call-template name="latexEnd"/>
    </xsl:template>

	<xsl:template match="mcMatheAufgabe">
	   	<xsl:call-template name="latexPlainAufgabe">
			<xsl:with-param name="verzeichnis" select="verzeichnis"/>
			<xsl:with-param name="nummer" select="schluessel"/>
			<xsl:with-param name="punkte" select="'keine'"/>
			<xsl:with-param name="schluessel" select="schluessel"/>
			<xsl:with-param name="loesungsbuchstabe" select="loesung"/>
			<xsl:with-param name="stufe" select="stufe"/>
		</xsl:call-template>
   	</xsl:template>

   	<xsl:template match="mcRaetselItem">
   		<xsl:call-template name="latexPlainAufgabe">
			<xsl:with-param name="verzeichnis" select="aufgabe/verzeichnis"/>
			<xsl:with-param name="nummer" select="nummer"/>
			<xsl:with-param name="punkte" select="aufgabe/punkte"/>
			<xsl:with-param name="schluessel" select="aufgabe/schluessel"/>
			<xsl:with-param name="loesungsbuchstabe" select="aufgabe/loesung"/>
			<xsl:with-param name="stufe" select="aufgabe/stufe"/>
		</xsl:call-template>
   	</xsl:template>
</xsl:stylesheet>