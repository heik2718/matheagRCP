function getScore(form, pCredit, pMaxPunktzahl, myAnswers, letters, pAnswers, points, anzChoices) {
	var score = parseFloat(pCredit);
	var wertung = Array(myAnswers.length);
	if (myAnswers.length != letters.length || myAnswers.length != points.length || myAnswers.length != anzChoices.length){
		alert("Programmfehler");
		return;
	}
	var penalty;
	for (i = 0; i < myAnswers.length; i++){
		switch (points[i]) {
		case 3:
			penalty = 0.75;
			break;
		case 4:
			penalty = 1.0;
			break;
		default:
			penalty = 1.25;
		}
		if (myAnswers[i] != "N") {
			if (myAnswers[i] == letters[i]) {
				score += points[i];
				wertung[i] = "r";
			} else {
				score -= penalty;
				wertung[i] = "f";
			}
		} else {
			wertung[i] = "n";
		}
//		console.log("step=" + i + ", score=" + score);
	}
	form.punkte.value = score + "";
	form.maximalePunktzahl.value = parseFloat(pMaxPunktzahl) + "";
	for (i = 0; i < pAnswers.length; i++){
		form.elements["loesung" + i].value = pAnswers[i];
		if (wertung[i] != "n"){
			if (wertung[i] == "r"){
				form.elements["wertung" + i].style.backgroundColor="green";
				form.elements["smily" + i].value=":)"
			} else {
				form.elements["wertung" + i].style.backgroundColor="red";
				form.elements["smily" + i].value=":("
			}

		}
	}
}

function analyseForm(myAnswers){
	for (i = 0; i < myAnswers.length; i++) {
		console.log("myAnswers[" + i + "]=" + myAnswers[i]);
	}
}