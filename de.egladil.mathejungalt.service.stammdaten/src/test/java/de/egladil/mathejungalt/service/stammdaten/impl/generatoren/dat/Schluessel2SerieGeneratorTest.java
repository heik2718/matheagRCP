/**
 * 
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.dat;

import java.io.File;
import java.io.IOException;

import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.service.stammdaten.impl.AbstractStammdatenserviceTest;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.MatheAGGeneratorUtils;

/**
 * @author aheike
 */
public class Schluessel2SerieGeneratorTest extends AbstractStammdatenserviceTest {

	/**
	 * @throws IOException
	 */
	public void testGenerate() throws IOException {
		Serie serie = createSerie();
		final Schluessel2SerieGenerator schluessel2SerieGenerator = new Schluessel2SerieGenerator();
		schluessel2SerieGenerator.setSerie(serie);
		File dir = new File("C:/_develop/temp");
		final File file = new File(dir + File.separator + "schluessel2serie.dat");
		file.delete();
		System.out.println("Ausgabe nach '" + dir.getAbsolutePath() + "'");
		MatheAGGeneratorUtils.writeOutput(dir.getAbsolutePath(), schluessel2SerieGenerator, getStammdatenservice());
		assertTrue(file.isFile());
	}
}
