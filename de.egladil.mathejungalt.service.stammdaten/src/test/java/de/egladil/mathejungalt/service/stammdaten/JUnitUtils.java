/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.service.stammdaten;

import de.egladil.mathejungalt.domain.mcraetsel.MCAntwortbuchstaben;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabenarten;
import de.egladil.mathejungalt.domain.mcraetsel.MCThemen;
import de.egladil.mathejungalt.domain.types.MCAufgabenstufen;

/**
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public abstract class JUnitUtils {

	/**
	 * <ul>
	 * <li><b>Quiz.schluessel: </b> 9999</li>
	 * <li><b>Aufgabe.schluessel: </b> 00001</li>
	 * <li><b>Aufgabe.verzeichnis: </b> 001</li>
	 * <li><b>Item.nummer: </b>1</li>
	 * </ul>
	 *
	 * @return
	 */
	public static MCArchivraetselItem prepareCompleteRaetselitemForTest() {
		MCAufgabe aufgabe = new MCAufgabe();
		aufgabe.setId(1l);
		aufgabe.setSchluessel("00997");
		aufgabe.setStufe(MCAufgabenstufen.STUFE_12);
		aufgabe.setThema(MCThemen.ARITHMETIK);
		aufgabe.setArt(MCAufgabenarten.E);
		aufgabe.setAntwortA("erste");
		aufgabe.setAntwortB("zweite");
		aufgabe.setAntwortC("dritte");
		aufgabe.setAntwortD("vierte");
		aufgabe.setAntwortE("fünfte Bla  Bla Bla Bla Bla Bla Bla Bla Bla Bla");
		aufgabe.setTitel("Apfelstücke");
		aufgabe.setBemerkung("Bemrkung zur Aufgabe 1");
		aufgabe.setQuelle(MCAufgabe.DEFAULT_QUELLE);
		aufgabe.setLoesungsbuchstabe(MCAntwortbuchstaben.D);
		aufgabe.setPunkte(4);
		aufgabe.setVerzeichnis("001");

		MCArchivraetselItem item = new MCArchivraetselItem();
		item.setAufgabe(aufgabe);
		item.setId(1l);
		item.setNummer("1");

		MCArchivraetsel raetsel = new MCArchivraetsel();
		raetsel.setSchluessel("9999");
		raetsel.setStufe(aufgabe.getStufe());
		raetsel.setTitel("Mathe jung alt 1");
		raetsel.addItem(item);

		return item;
	}

	public static MCAufgabe prepareAufgabe() {
		MCAufgabe aufgabe = new MCAufgabe();
		aufgabe.setId(1l);
		aufgabe.setSchluessel("00001");
		aufgabe.setStufe(MCAufgabenstufen.STUFE_12);
		aufgabe.setThema(MCThemen.ARITHMETIK);
		aufgabe.setArt(MCAufgabenarten.E);
		aufgabe.setAntwortA("erste");
		aufgabe.setAntwortB("zweite");
		aufgabe.setAntwortC("dritte");
		aufgabe.setAntwortD("vierte");
		aufgabe.setAntwortE("fünfte");
		aufgabe.setTitel("Apfelstücke");
		aufgabe.setBemerkung("Bemrkung zur Aufgabe 1");
		aufgabe.setQuelle(MCAufgabe.DEFAULT_QUELLE);
		aufgabe.setLoesungsbuchstabe(MCAntwortbuchstaben.D);
		aufgabe.setPunkte(4);
		aufgabe.setVerzeichnis("001");
		return aufgabe;
	}

	/**
	 * Generiert ein Rätsel mit Schlüssel 9000 und 2 vollständigen Items. Nummern sind 3 und 4.
	 *
	 * @return
	 */
	public static MCArchivraetsel prepareArchivRaetsel() {
		MCArchivraetsel raetsel = new MCArchivraetsel();
		raetsel.setStufe(MCAufgabenstufen.STUFE_12);
		raetsel.setSchluessel("9000");
		raetsel.setTitel("Minikänguru 2009");

		{
			MCAufgabe aufgabe = new MCAufgabe();
			aufgabe.setId(1l);
			aufgabe.setSchluessel("00001");
			aufgabe.setStufe(MCAufgabenstufen.STUFE_12);
			aufgabe.setThema(MCThemen.ARITHMETIK);
			aufgabe.setArt(MCAufgabenarten.E);
			aufgabe.setAntwortA("erste");
			aufgabe.setAntwortB("zweite");
			aufgabe.setAntwortC("dritte");
			aufgabe.setAntwortD("vierte");
			aufgabe.setAntwortE("fünfte");
			aufgabe.setTitel("Zahnpasta");
			aufgabe.setBemerkung("Bemrkung zur Aufgabe 1");
			aufgabe.setQuelle(MCAufgabe.DEFAULT_QUELLE);
			aufgabe.setLoesungsbuchstabe(MCAntwortbuchstaben.D);
			aufgabe.setPunkte(4);
			aufgabe.setVerzeichnis("001");

			MCArchivraetselItem item = new MCArchivraetselItem();
			item.setAufgabe(aufgabe);
			item.setId(1l);
			item.setNummer("3");
			raetsel.addItem(item);
		}

		{
			MCAufgabe aufgabe = new MCAufgabe();
			aufgabe.setId(1l);
			aufgabe.setSchluessel("00002");
			aufgabe.setStufe(MCAufgabenstufen.STUFE_12);
			aufgabe.setThema(MCThemen.GEOMETRIE);
			aufgabe.setArt(MCAufgabenarten.E);
			aufgabe.setAntwortA("bla");
			aufgabe.setAntwortB("blubb");
			aufgabe.setAntwortC("grummel");
			aufgabe.setTitel("Labyrinth");
			aufgabe.setBemerkung("Bemrkung zur Aufgabe 2");
			aufgabe.setQuelle(MCAufgabe.DEFAULT_QUELLE);
			aufgabe.setLoesungsbuchstabe(MCAntwortbuchstaben.A);
			aufgabe.setPunkte(5);
			aufgabe.setVerzeichnis("001");

			MCArchivraetselItem item = new MCArchivraetselItem();
			item.setAufgabe(aufgabe);
			item.setId(1l);
			item.setNummer("4");
			raetsel.addItem(item);
		}

		return raetsel;
	}

}
