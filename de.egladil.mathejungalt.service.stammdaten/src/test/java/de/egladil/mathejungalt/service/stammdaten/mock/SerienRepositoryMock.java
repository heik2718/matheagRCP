/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.mock;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serienitem;
import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenzweck;
import de.egladil.mathejungalt.persistence.api.IAufgabenRepository;
import de.egladil.mathejungalt.persistence.api.ISerienRepository;

/**
 * @author aheike
 */
public class SerienRepositoryMock implements ISerienRepository {

	private static final Logger LOG = LoggerFactory.getLogger(SerienRepositoryMock.class);

	/** */
	private IAufgabenRepository aufgabenRepository;

	/**
	 *
	 */
	public SerienRepositoryMock() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.ISerienRepository#findAllSerien()
	 */
	@Override
	public List<Serie> findAllSerien() {
		List<Serie> serien = new ArrayList<Serie>();
		Serie serie = new Serie();
		serie.setBeendet(IAufgabensammlung.BEENDET);
		serie.setNummer(100);
		serie.setRunde(10);
		loadSerienitems(serie);
		serien.add(serie);

		serie = new Serie();
		serie.setBeendet(IAufgabensammlung.BEENDET);
		serie.setNummer(101);
		serie.setRunde(10);
		loadSerienitems(serie);
		serien.add(serie);

		serie = new Serie();
		serie.setBeendet(IAufgabensammlung.NICHT_BEENDET);
		serie.setNummer(102);
		serie.setRunde(10);
		loadSerienitems(serie);
		serien.add(serie);

		return serien;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.ISerienRepository#findByAufgabe(de.egladil.mathejungalt.domain.aufgaben
	 * .Aufgabe)
	 */
	@Override
	public Serienitem findByAufgabe(Aufgabe pAufgabe) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.ISerienRepository#findById(java.lang.Long)
	 */
	@Override
	public Serie findById(Long pId) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.ISerienRepository#findByKey(java.lang.Integer)
	 */
	@Override
	public Serie findByKey(Integer pKey) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.ISerienRepository#findByRunde(int)
	 */
	@Override
	public List<Serie> findByRunde(int pRunde) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.ISerienRepository#getMaxKey()
	 */
	@Override
	public Integer getMaxKey() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.persistence.api.ISerienRepository#loadSerienitems(de.egladil.mathejungalt.domain.
	 * aufgabensammlungen.serien.Serie)
	 */
	@Override
	public void loadSerienitems(Serie pSerie) {
		int count = 0;
		if (pSerie.isItemsLoaded()) {
			return;
		}
		LOG.info("Anzahl items = " + pSerie.anzahlAufgaben());
		List<Aufgabe> aufgaben = aufgabenRepository.findAll();
		LOG.info("Anzahl neue Aufgaben: " + aufgaben.size());
		for (Aufgabe aufgabe : aufgaben) {
			if (Aufgabenzweck.S.equals(aufgabe.getZweck())) {
				count++;
				Serienitem item = new Serienitem();
				item.setAufgabe(aufgabe);
				item.setNummer(pSerie.getNummer() + "-" + aufgabe.getStufe() + count);
				item.setSerie(pSerie);
				pSerie.addItem(item);
				if (pSerie.anzahlAufgaben() > aufgaben.size()) {
					LOG.warn("Es kommen zu viele Aufgaben dazu!!!!!!");
				}
			}
		}
		LOG.info("Anzah items = " + pSerie.anzahlAufgaben());
		pSerie.setItemsLoaded(true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.ISerienRepository#saveSerie(de.egladil.mathejungalt.domain.aufgabensammlungen
	 * .serien.Serie)
	 */
	@Override
	public void saveSerie(Serie pSerie) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.ISerienRepository#setAufgabenRepository(de.egladil.mathejungalt.persistence
	 * .api.IAufgabenRepository)
	 */
	@Override
	public void setAufgabenRepository(IAufgabenRepository pAufgabenRepository) {
		aufgabenRepository = pAufgabenRepository;
	}

}
