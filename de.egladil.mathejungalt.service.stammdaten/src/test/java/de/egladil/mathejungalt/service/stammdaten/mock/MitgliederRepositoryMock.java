/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.mock;

import java.util.List;
import java.util.Map;

import de.egladil.mathejungalt.domain.mitglieder.Diplom;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.domain.mitglieder.Serienteilnahme;
import de.egladil.mathejungalt.persistence.api.IMitgliederRepository;
import de.egladil.mathejungalt.persistence.api.ISerienRepository;

/**
 * @author aheike
 */
public class MitgliederRepositoryMock implements IMitgliederRepository {

	/**
	 *
	 */
	public MitgliederRepositoryMock() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IMitgliederRepository#findAll()
	 */
	@Override
	public List<Mitglied> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IMitgliederRepository#findAllWithFruehstarterInRunde(int)
	 */
	@Override
	public List<Mitglied> findAllWithFruehstarterInRunde(int pRunde) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IMitgliederRepository#findByAttributeMap(java.util.Map, boolean,
	 * boolean)
	 */
	@Override
	public List<Mitglied> findByAttributeMap(Map<String, Object> pAttributeMap, boolean pExact, boolean pAnd) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IMitgliederRepository#findById(long)
	 */
	@Override
	public Mitglied findById(long pId) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IMitgliederRepository#getMaxKey()
	 */
	@Override
	public String getMaxKey() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IMitgliederRepository#loadDiplome(de.egladil.mathejungalt.domain.mitglieder
	 * .Mitglied)
	 */
	@Override
	public void loadDiplome(Mitglied pMitglied) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IMitgliederRepository#loadSerienteilnahmen(de.egladil.mathejungalt.domain
	 * .mitglieder.Mitglied)
	 */
	@Override
	public void loadSerienteilnahmen(Mitglied pMitglied) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IMitgliederRepository#removeDiplom(de.egladil.mathejungalt.domain.mitglieder
	 * .Diplom)
	 */
	@Override
	public void removeDiplom(Diplom pDiplom) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IMitgliederRepository#removeSerienteilnahme(de.egladil.mathejungalt.domain
	 * .mitglieder.Serienteilnahme)
	 */
	@Override
	public void removeSerienteilnahme(Serienteilnahme pTeilnahme) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IMitgliederRepository#saveDiplom(de.egladil.mathejungalt.domain.mitglieder
	 * .Diplom)
	 */
	@Override
	public void saveDiplom(Diplom pDiplom) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IMitgliederRepository#saveMitglied(de.egladil.mathejungalt.domain.mitglieder
	 * .Mitglied)
	 */
	@Override
	public void saveMitglied(Mitglied pMitglied) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IMitgliederRepository#saveSerienteilnahme(de.egladil.mathejungalt.domain
	 * .mitglieder.Serienteilnahme)
	 */
	@Override
	public void saveSerienteilnahme(Serienteilnahme pSerienteilnahme) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IMitgliederRepository#setSerienRepository(de.egladil.mathejungalt.persistence
	 * .api.ISerienRepository)
	 */
	@Override
	public void setSerienRepository(ISerienRepository pSerienRepository) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IMitgliederRepository#changeMitgliedActivation(de.egladil.mathejungalt
	 * .domain.mitglieder.Mitglied)
	 */
	@Override
	public void changeMitgliedActivation(Mitglied pMitglied) {
		// TODO Auto-generated method stub

	}

}
