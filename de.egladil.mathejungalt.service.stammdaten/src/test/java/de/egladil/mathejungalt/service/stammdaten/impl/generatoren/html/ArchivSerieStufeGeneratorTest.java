//=====================================================
// Projekt: de.egladil.mathejungalt.service.stammdaten
// (c) Heike Winkelvoß
//=====================================================
// Dateiname: SerieIndexGeneratorTest.java                            $
// $Revision:: 1                                     $
// $Modtime:: 29.03.2015                                $
//=====================================================

package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html;

import java.util.Date;

import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serienitem;
import de.egladil.mathejungalt.service.stammdaten.impl.AbstractStammdatenserviceTest;

/**
 * SerieIndexGeneratorTest
 */
public class ArchivSerieStufeGeneratorTest extends AbstractStammdatenserviceTest {

	public void testGenerat() {
		Serie serie = new Serie();
		serie.setBeendet(1);
		serie.setDatum(new Date());
		serie.setId(Long.valueOf(1l));
		serie.setItemsLoaded(true);
		serie.setMonatJahrText("SEPT/OKT 2013");
		serie.setNummer(1);
		serie.setRunde(12);

		{
			Serienitem item = new Serienitem();
			item.setId(Long.valueOf(1l));
			item.setNummer("1-11");
			Aufgabe aufgabe = new Aufgabe();
			aufgabe.setStufe(1);
			aufgabe.setId(Long.valueOf(1l));
			aufgabe.setSchluessel("00001");
			item.setAufgabe(aufgabe);
			serie.addItem(item);
		}
		{
			Serienitem item = new Serienitem();
			item.setId(Long.valueOf(7l));
			item.setNummer("1-12");
			Aufgabe aufgabe = new Aufgabe();
			aufgabe.setStufe(1);
			aufgabe.setId(Long.valueOf(7l));
			aufgabe.setSchluessel("00022");
			item.setAufgabe(aufgabe);
			serie.addItem(item);
		}
		{
			Serienitem item = new Serienitem();
			item.setId(Long.valueOf(2l));
			item.setNummer("1-21");
			Aufgabe aufgabe = new Aufgabe();
			aufgabe.setStufe(2);
			aufgabe.setId(Long.valueOf(1l));
			aufgabe.setSchluessel("00002");
			item.setAufgabe(aufgabe);
			serie.addItem(item);
		}
		{
			Serienitem item = new Serienitem();
			item.setId(Long.valueOf(3l));
			item.setNummer("1-31");
			Aufgabe aufgabe = new Aufgabe();
			aufgabe.setStufe(3);
			aufgabe.setId(Long.valueOf(3l));
			aufgabe.setSchluessel("00003");
			item.setAufgabe(aufgabe);
			serie.addItem(item);
		}
		{
			Serienitem item = new Serienitem();
			item.setId(Long.valueOf(4l));
			item.setNummer("1-41");
			Aufgabe aufgabe = new Aufgabe();
			aufgabe.setStufe(4);
			aufgabe.setId(Long.valueOf(4l));
			aufgabe.setSchluessel("00004");
			item.setAufgabe(aufgabe);
			serie.addItem(item);
		}
		{
			Serienitem item = new Serienitem();
			item.setId(Long.valueOf(5l));
			item.setNummer("1-51");
			Aufgabe aufgabe = new Aufgabe();
			aufgabe.setStufe(5);
			aufgabe.setId(Long.valueOf(5l));
			aufgabe.setSchluessel("00005");
			item.setAufgabe(aufgabe);
			serie.addItem(item);
		}
		{
			Serienitem item = new Serienitem();
			item.setId(Long.valueOf(1l));
			item.setNummer("1-61");
			Aufgabe aufgabe = new Aufgabe();
			aufgabe.setStufe(6);
			aufgabe.setId(Long.valueOf(6l));
			aufgabe.setSchluessel("00006");
			item.setAufgabe(aufgabe);
			serie.addItem(item);
		}
		serie.setItemsLoaded(true);

		ArchivSerieStufeGenerator generator = new ArchivSerieStufeGenerator();

		String content = generator.generate(getStammdatenservice(), serie, 1);

		assertNotNull(content);

		System.out.println(content);

	}

}
