/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.service.stammdaten.quiz;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import junitx.framework.FileAssert;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.junit.Test;

import de.egladil.mathejungalt.config.IMatheJungAltConfiguration.RaetsellisteGeneratorModus;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.service.stammdaten.JUnitUtils;

/**
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class HtlatexPostProcessorTest {

	private static final String PATH_VERZEICHNIS_TEMPLATE = "C:\\work\\knobelarchiv_2\\latex\\HtlatexPostProcessorTestTemplate";

	private static final String PATH_WORK_DIR = "C:\\work\\HtlatexPostProcessorTest";

	private static final String PATH_EXPECTED_QUIZ_XML = "C:\\work\\_develop\\_sandbox\\matheagRCPJUnit\\expected_quiz.xml";

	@Test
	public void testGenerate_Happy_Hour_JS() throws IOException {
		// Arrange
		MCArchivraetsel raetsel = JUnitUtils.prepareArchivRaetsel();
		String pathRootDir = PATH_WORK_DIR;
		System.out.println("QuizDirectoryBuilderTest.testCreateDirectoryStructurForQuiz_Happy_Hour(): [pathRootDir="
			+ pathRootDir + "]");
		File workDir = new File(pathRootDir);
		FileUtils.deleteDirectory(workDir);
		FileUtils.copyDirectory(new File(PATH_VERZEICHNIS_TEMPLATE), workDir);
		RaetsellisteGeneratorModus modus = RaetsellisteGeneratorModus.JAVASCRIPT;
		HtlatexPostProcessor processor = new HtlatexPostProcessor();

		// Act
		IProgressMonitor progressMonitor = null;
		IStatus status = processor.process(raetsel, pathRootDir, modus, progressMonitor);

		// Assert
		assertEquals(IStatus.OK, status.getSeverity());
		assertFalse(new File(pathRootDir + "\\9000_post.txt").canRead());
		assertFalse(new File(pathRootDir + "\\process_03.bat").canRead());
		assertFalse(new File(pathRootDir + "\\process_04.bat").canRead());
		assertFalse(new File(pathRootDir + "\\run.bat").canRead());
	}

	@Test
	public void testGenerate_Happy_Hour_Android() throws IOException {
		// Arrange
		MCArchivraetsel raetsel = JUnitUtils.prepareArchivRaetsel();
		String pathRootDir = PATH_WORK_DIR;
		System.out.println("QuizDirectoryBuilderTest.testCreateDirectoryStructurForQuiz_Happy_Hour(): [pathRootDir="
			+ pathRootDir + "]");
		File workDir = new File(pathRootDir);
		FileUtils.deleteDirectory(workDir);
		FileUtils.copyDirectory(new File(PATH_VERZEICHNIS_TEMPLATE), workDir);
		RaetsellisteGeneratorModus modus = RaetsellisteGeneratorModus.ANDROID;
		HtlatexPostProcessor processor = new HtlatexPostProcessor();

		// Act
		IProgressMonitor progressMonitor = null;
		IStatus status = processor.process(raetsel, pathRootDir, modus, progressMonitor);

		// Assert
		assertEquals(IStatus.OK, status.getSeverity());
		assertFalse(new File(pathRootDir + "\\9000_post.txt").canRead());
		assertFalse(new File(pathRootDir + "\\process_03.bat").canRead());
		assertFalse(new File(pathRootDir + "\\process_04.bat").canRead());
		assertFalse(new File(pathRootDir + "\\run.bat").canRead());
		assertTrue(new File(pathRootDir + "\\9000\\quiz.xml").canRead());
	}

	@Test
	public void testGenerateQuizXmlFile_Happy_Hour() throws Exception {
		// Arrange
		MCArchivraetsel raetsel = JUnitUtils.prepareArchivRaetsel();
		String pathRootDir = PATH_WORK_DIR;
		System.out.println("QuizDirectoryBuilderTest.testCreateDirectoryStructurForQuiz_Happy_Hour(): [pathRootDir="
			+ pathRootDir + "]");
		File workDir = new File(pathRootDir);
		FileUtils.deleteDirectory(workDir);
		FileUtils.copyDirectory(new File(PATH_VERZEICHNIS_TEMPLATE), workDir);
		HtlatexPostProcessor processor = new HtlatexPostProcessor();
		File expectedFile = new File(PATH_EXPECTED_QUIZ_XML);

		// Act
		processor.generateQuizXmlFile(pathRootDir, raetsel);

		// Assert
		final File actualFile = new File(pathRootDir + "\\9000\\quiz.xml");
		assertTrue(actualFile.canRead());
		FileAssert.assertEquals(expectedFile, actualFile);
	}
}
