/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.service.stammdaten.quiz;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.eclipse.core.runtime.AssertionFailedException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.domain.types.MCAufgabenstufen;

/**
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class QuizDirectoryBuilderTest {

	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	@Test
	public void testCreateDirectoryStructurForQuiz_Happy_Hour() {
		// Arrange:
		MCArchivraetsel raetsel = createGenerierbaresRaetselMitZweiItems();
		String pathRootDir = folder.getRoot().getAbsolutePath();
		System.out.println("QuizDirectoryBuilderTest.testCreateDirectoryStructurForQuiz_Happy_Hour(): [pathRootDir="
			+ pathRootDir + "]");
		final String expectedPathQuizRoot = pathRootDir + "\\9999";
		QuizDirectoryBuilder quizDirectoryBuilder = new QuizDirectoryBuilder();
		// Act:
		String pathQuizRoot = quizDirectoryBuilder.generate(raetsel, pathRootDir);
		// Assert:
		assertEquals(expectedPathQuizRoot, pathQuizRoot);
		assertTrue(new File(pathQuizRoot).isDirectory());
		assertTrue("Fehler bei Aufgabe 1", new File(pathQuizRoot + "/01").isDirectory());
		assertTrue("Fehler bei Aufgabe 2", new File(pathQuizRoot + "/02").isDirectory());
	}

	@Test
	public void testCreateDirectoryStructurForQuiz_zweistelligeItemnummern() {
		// Arrange:
		MCArchivraetsel raetsel = createGenerierbaresRaetselMitZweiItems();
		{
			MCArchivraetselItem item = new MCArchivraetselItem();
			item.setNummer("11");
			item.setId(11l);
			MCAufgabe aufgabe = new MCAufgabe();
			aufgabe.setId(1l);
			aufgabe.setSchluessel("00011");
			aufgabe.setStufe(MCAufgabenstufen.STUFE_56);
			item.setAufgabe(aufgabe);
			raetsel.addItem(item);
		}
		String pathRootDir = folder.getRoot().getAbsolutePath();
		System.out.println("QuizDirectoryBuilderTest.testCreateDirectoryStructurForQuiz_Happy_Hour(): [pathRootDir="
			+ pathRootDir + "]");
		final String expectedPathQuizRoot = pathRootDir + "\\9999";
		QuizDirectoryBuilder quizDirectoryBuilder = new QuizDirectoryBuilder();
		// Act:
		String pathQuizRoot = quizDirectoryBuilder.generate(raetsel, pathRootDir);
		// Assert:
		assertEquals(expectedPathQuizRoot, pathQuizRoot);
		assertTrue(new File(pathQuizRoot).isDirectory());
		assertTrue("Fehler bei Aufgabe 1", new File(pathQuizRoot + "/01").isDirectory());
		assertTrue("Fehler bei Aufgabe 2", new File(pathQuizRoot + "/02").isDirectory());
		assertTrue("Fehler bei Aufgabe 11", new File(pathQuizRoot + "/11").isDirectory());
	}

	@Test(expected = AssertionFailedException.class)
	public void testCreateDirectoryStructurForQuiz_raetselNull() {
		// Arrange:
		MCArchivraetsel raetsel = null;
		String pathRootDir = folder.getRoot().getAbsolutePath();
		System.out.println("QuizDirectoryBuilderTest.testCreateDirectoryStructurForQuiz_raetselNull(): [pathRootDir="
			+ pathRootDir + "]");
		QuizDirectoryBuilder quizDirectoryBuilder = new QuizDirectoryBuilder();
		// Act:
		quizDirectoryBuilder.generate(raetsel, pathRootDir);
		// Assert: oben
	}

	@Test(expected = AssertionFailedException.class)
	public void testCreateDirectoryStructurForQuiz_pathRootDirNull() {
		// Arrange:
		MCArchivraetsel raetsel = createGenerierbaresRaetselMitZweiItems();
		QuizDirectoryBuilder quizDirectoryBuilder = new QuizDirectoryBuilder();
		// Act:
		quizDirectoryBuilder.generate(raetsel, null);
		// Assert: oben
	}

	/**
	 * @return
	 */
	private MCArchivraetsel createGenerierbaresRaetselMitZweiItems() {
		String schluessel = "9999";
		MCArchivraetsel raetsel = new MCArchivraetsel();
		raetsel.setStufe(MCAufgabenstufen.STUFE_56);
		raetsel.setId(1l);
		raetsel.setSchluessel(schluessel);
		{
			MCArchivraetselItem item = new MCArchivraetselItem();
			item.setNummer("1");
			item.setId(1l);
			MCAufgabe aufgabe = new MCAufgabe();
			aufgabe.setId(1l);
			aufgabe.setSchluessel("00001");
			aufgabe.setStufe(MCAufgabenstufen.STUFE_56);
			item.setAufgabe(aufgabe);
			raetsel.addItem(item);
		}
		{
			MCArchivraetselItem item = new MCArchivraetselItem();
			item.setNummer("2");
			item.setId(2l);
			MCAufgabe aufgabe = new MCAufgabe();
			aufgabe.setId(2l);
			aufgabe.setSchluessel("00002");
			aufgabe.setStufe(MCAufgabenstufen.STUFE_56);
			item.setAufgabe(aufgabe);
			raetsel.addItem(item);
		}
		return raetsel;
	}
}
