/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.service.stammdaten.quiz;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 *
 */
public class HtmlUtilsTest {

	@Test
	public void testConvertToHtmlEntities_Happy_Hour() {
		// Arrange:
		String inputString = "BläBlübFÖoTÜfTÄfßsst";
		String expectedOutput = "Bl&auml;Bl&uuml;bF&Ouml;oT&Uuml;fT&Auml;f&szlig;sst";
		// Act:
		String outputString = HtmlUtils.convertToHtmlEntities(inputString);
		// Assert:
		assertEquals(expectedOutput, outputString);

	}
}
