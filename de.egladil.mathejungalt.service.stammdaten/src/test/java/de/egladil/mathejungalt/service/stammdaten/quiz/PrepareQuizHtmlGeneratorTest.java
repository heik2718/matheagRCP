/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.service.stammdaten.quiz;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.runtime.AssertionFailedException;
import org.eclipse.core.runtime.IStatus;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.service.stammdaten.JUnitUtils;

/**
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 *
 */
public class PrepareQuizHtmlGeneratorTest {

	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	@Test
	public void testGenerate_Hapy_Hour() throws IOException {
		// Arrange
		MCArchivraetsel raetsel = JUnitUtils.prepareArchivRaetsel();
		String pathRootDir = folder.getRoot().getAbsolutePath();
		System.out.println("QuizDirectoryBuilderTest.testCreateDirectoryStructurForQuiz_Happy_Hour(): [pathRootDir="
			+ pathRootDir + "]");
		final String pathQuizRoot = pathRootDir + "\\9000";
		PrepareQuizHtmlGenerator generator = new PrepareQuizHtmlGenerator();

		// Act
		IStatus status = generator.generate(raetsel, pathRootDir, null);

		// Assert
		assertEquals(IStatus.OK, status.getSeverity());
		assertTrue(new File(pathQuizRoot).isDirectory());
		assertTrue("Fehler bei Aufgabe 3 - Dir", new File(pathQuizRoot + "/03").isDirectory());
		assertTrue("Fehler bei Aufgabe 4 - Dir", new File(pathQuizRoot + "/04").isDirectory());
		assertTrue("Fehler bei Aufgabe 3 - File", new File(pathQuizRoot + "/03.tex").canRead());
		assertTrue("Fehler bei Aufgabe 4 - File", new File(pathQuizRoot + "/04.tex").canRead());
		assertTrue("Fehler bei Aufgabe 3 - File", new File(pathRootDir + "/process_03.bat").canRead());
		assertTrue("Fehler bei Aufgabe 4 - File", new File(pathRootDir + "/process_04.bat").canRead());
		final File fileRun = new File(pathRootDir + "/run.bat");
		assertTrue("Fehler bei run.bat", fileRun.canRead());
		final File readyMarker = new File(pathRootDir + "\\9000_pre.txt");
		assertTrue("Fehler bei readyMarker", readyMarker.canRead());
	}

	@Test(expected = AssertionFailedException.class)
	public void testGenerate_Raetsel_Null() throws IOException {
		// Arrange
		String pathRootDir = folder.getRoot().getAbsolutePath();
		System.out.println("QuizDirectoryBuilderTest.testCreateDirectoryStructurForQuiz_Happy_Hour(): [pathRootDir="
			+ pathRootDir + "]");
		PrepareQuizHtmlGenerator generator = new PrepareQuizHtmlGenerator();

		// Act
		generator.generate(null, pathRootDir, null);

		// Assert
	}

	@Test(expected = AssertionFailedException.class)
	public void testGenerate_Path_Null() throws IOException {
		// Arrange
		MCArchivraetsel raetsel = JUnitUtils.prepareArchivRaetsel();
		PrepareQuizHtmlGenerator generator = new PrepareQuizHtmlGenerator();

		// Act
		generator.generate(raetsel, null, null);

		// Assert
	}
}
