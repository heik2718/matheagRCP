/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;

/**
 * @author heike
 */
public class RaetselserviceTest extends AbstractStammdatenserviceTest {

	private static final Logger LOG = LoggerFactory.getLogger(RaetselserviceTest.class);

	private static final String PATH_WORKDIR = "/home/heike/work/knobelarchiv_2/latex/tempdir_junit";

	private Raetselservice raetselservice;

	private JAXBContext jaxbContext;

	/*
	 * (non-Javadoc)
	 *
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		raetselservice = new Raetselservice();
		jaxbContext = JAXBContext.newInstance(MCArchivraetsel.class, MCArchivraetselItem.class);
	}

	public void testRaetselGenerierenRaetselIstPrivatGenerierePrivat() {
		boolean result = raetselservice.raetselGenerieren(true, true);
		assertTrue(result);
	}

	public void testRaetselGenerierenRaetselIstOeffentlichGenerierePrivat() {
		boolean result = raetselservice.raetselGenerieren(false, true);
		assertTrue(result);
	}

	public void testRaetselGenerierenRaetselIstPrivatGeneriereOeffentlich() {
		boolean result = raetselservice.raetselGenerieren(true, false);
		assertFalse(result);
	}

	public void testRaetselGenerierenRaetselIstOeffentlichGeneriereOeffentlich() {
		boolean result = raetselservice.raetselGenerieren(false, false);
		assertTrue(result);
	}

	public void testGenerateRaetsel() throws Exception {
		MCArchivraetsel pRaetsel = prepareRaetsel();
		String aufgabeLaTeXFileName = "quiz_" + pRaetsel.getSchluessel() + ".tex";

		File aufgabeFile = new File(PATH_WORKDIR + "/" + aufgabeLaTeXFileName);

		aufgabeFile.delete();

		assertTrue("Testsetting: " + aufgabeFile.getAbsolutePath() + " existiert schon", !aufgabeFile.canRead());

		LOG.info("Datei wird generiert nach '" + PATH_WORKDIR + "'");

		raetselservice.createLaTeX(pRaetsel, PATH_WORKDIR, true);
	}

	public void testGenerateLaTeXAndTransformToPs() throws Exception {
		MCArchivraetsel pRaetsel = prepareRaetsel();
		Map<String, MCArchivraetsel> items = new HashMap<String, MCArchivraetsel>();
		items.put(pRaetsel.getSchluessel(), pRaetsel);

		raetselservice.generateLaTeXAndTransformToPs(items, PATH_WORKDIR, null, this.getStammdatenservice(), null);
	}

	private MCArchivraetselItem prepareItem() throws Exception {
		return (MCArchivraetselItem) jaxbContext.createUnmarshaller().unmarshal(
			getClass().getResource("/mcArchivRaetselItem5Antworten.xml"));
	}

	private MCArchivraetsel prepareRaetsel() throws Exception {
		return (MCArchivraetsel) jaxbContext.createUnmarshaller().unmarshal(
			getClass().getResource("/mcarchivraetsel.xml"));
	}
}
