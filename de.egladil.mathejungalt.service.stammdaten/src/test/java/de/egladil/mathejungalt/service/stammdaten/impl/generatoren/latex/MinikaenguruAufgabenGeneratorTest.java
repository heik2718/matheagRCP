/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex;

import java.io.File;

import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;
import de.egladil.mathejungalt.service.stammdaten.impl.AbstractStammdatenserviceTest;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.MatheAGGeneratorUtils;

/**
 * @author aheike
 */
public class MinikaenguruAufgabenGeneratorTest extends AbstractStammdatenserviceTest {

	/** */
	private MinikaenguruGenerator minikaenguruGenerator;

	/** */
	private Minikaenguru minikaenguru;

	/**
	 *
	 */
	public MinikaenguruAufgabenGeneratorTest() {

	}

	public void testGenerateAufgaben() {
		minikaenguru = createMinikaenguru();
		minikaenguruGenerator.setMinikaenguru(minikaenguru);
		minikaenguruGenerator.setFontsize("\\Large\n");
		minikaenguruGenerator.setAufgabenabstand("\\newpage");
		File dir = new File("C:/_develop/temp");
		File file = new File(dir + File.separator + "aufgaben_1999.tex");
		file.delete();
		System.out.println("Ausgabe nach '" + dir.getAbsolutePath() + "'");
		MatheAGGeneratorUtils.writeOutput(dir.getAbsolutePath(), minikaenguruGenerator, getStammdatenservice());
		assertTrue(file.isFile());
	}

	public void testGenerateLoesungen() {
		minikaenguru = createMinikaenguru();
		minikaenguruGenerator.setMinikaenguru(minikaenguru);
		minikaenguruGenerator.setFontsize("\\Large\n");
		minikaenguruGenerator.setAufgabenabstand("\\newpage");
		minikaenguruGenerator.setAusgabeLoesungen(true);
		File dir = new File("C:/_develop/temp");
		File file = new File(dir + File.separator + "loesungen_1999.tex");
		file.delete();
		System.out.println("Ausgabe nach '" + dir.getAbsolutePath() + "'");
		MatheAGGeneratorUtils.writeOutput(dir.getAbsolutePath(), minikaenguruGenerator, getStammdatenservice());
		assertTrue(file.isFile());
	}

	/**
	 * @param pMinikaenguruAufgabenGenerator the minikaenguruAufgabenGenerator to set
	 */
	public void setMinikaenguruGenerator(MinikaenguruGenerator pMinikaenguruAufgabenGenerator) {
		minikaenguruGenerator = pMinikaenguruAufgabenGenerator;
	}

}
