/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.service.stammdaten.quiz;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.AssertionFailedException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.service.stammdaten.JUnitUtils;

/**
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class HtlatexProcessorGeneratorTest {

	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	@Test
	public void testGenerate_Happy_Hour() throws IOException {

		// Arrange
		MCArchivraetselItem item = JUnitUtils.prepareCompleteRaetselitemForTest();
		String pathRootDir = folder.getRoot().getAbsolutePath();
		System.out.println("QuizDirectoryBuilderTest.testCreateDirectoryStructurForQuiz_Happy_Hour(): [pathRootDir="
			+ pathRootDir + "]");
		String expectedPathProcessBat = pathRootDir + "\\process_01.bat";
		HtLatexProcessorGenerator generator = new HtLatexProcessorGenerator();

		// Act
		String actualPathBat = generator.generate(item, pathRootDir);

		// Assert
		assertEquals(expectedPathProcessBat, actualPathBat);
		File file = new File(actualPathBat);
		@SuppressWarnings("unchecked")
		List<String> lines = FileUtils.readLines(file);
		assertEquals("copy .\\9999\\01.tex .\\aufgabe.tex", lines.get(0));
		assertEquals("htlatex aufgabe.tex \"html,no-DOCTYPE\"  \"iso8859/1/charset/uni/!\"", lines.get(1));
		assertEquals("move aufgabe.html .\\9999\\01", lines.get(2));
		assertEquals("move *.png .\\9999\\01", lines.get(3));
		assertEquals("del aufgabe*", lines.get(4));
	}

	@Test(expected = AssertionFailedException.class)
	public void testGenerate_Item_Null() throws IOException {

		// Arrange
		String pathRootDir = folder.getRoot().getAbsolutePath();
		HtLatexProcessorGenerator generator = new HtLatexProcessorGenerator();

		// Act
		generator.generate(null, pathRootDir);

		// Assert siehe oben.
	}

	@Test(expected = AssertionFailedException.class)
	public void testGenerate_Path_Null() throws IOException {

		// Arrange
		MCArchivraetselItem item = JUnitUtils.prepareCompleteRaetselitemForTest();
		HtLatexProcessorGenerator generator = new HtLatexProcessorGenerator();

		// Act
		generator.generate(item, null);

		// Assert siehe oben.
	}
}
