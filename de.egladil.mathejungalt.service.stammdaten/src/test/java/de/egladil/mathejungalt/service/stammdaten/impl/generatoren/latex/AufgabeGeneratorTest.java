/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex;

import java.io.File;

import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.service.stammdaten.impl.AbstractStammdatenserviceTest;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.MatheAGGeneratorUtils;

/**
 * @author heike
 */
public class AufgabeGeneratorTest extends AbstractStammdatenserviceTest {

	private AufgabeGenerator aufgabeGenerator = new AufgabeGenerator();

	public void testGenerateNormal() {
		Aufgabe aufgabe = new Aufgabe();
		aufgabe.setSchluessel("01653");
		aufgabeGenerator.init(aufgabe, null);
		File dir = new File("C:/work/knobel_archiv/latex/buildertemp");
		File file = new File(dir + File.separator + aufgabeGenerator.getFilename());
		file.delete();
		System.out.println("Ausgabe nach '" + dir.getAbsolutePath() + "'");
		MatheAGGeneratorUtils.writeOutput(dir.getAbsolutePath(), aufgabeGenerator, getStammdatenservice());
	}
}
