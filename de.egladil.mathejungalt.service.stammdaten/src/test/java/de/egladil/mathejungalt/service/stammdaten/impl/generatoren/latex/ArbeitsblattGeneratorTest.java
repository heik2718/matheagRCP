/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex;

import java.io.File;

import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.service.stammdaten.impl.AbstractStammdatenserviceTest;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.MatheAGGeneratorUtils;

/**
 * @author aheike
 */
public class ArbeitsblattGeneratorTest extends AbstractStammdatenserviceTest {

	/** */
	private ArbeitsblattGenerator arbeitsblattGenerator;

	/** */
	private Arbeitsblatt arbeitsblatt;

	/**
	 *
	 */
	public ArbeitsblattGeneratorTest() {

	}

	public void testGenerateAufgaben() {
		arbeitsblatt = createArbeitsblatt();
		arbeitsblattGenerator.setArbeitsblatt(arbeitsblatt);
		arbeitsblattGenerator.setFontsize("\\LARGE\n");
		arbeitsblattGenerator.setAufgabenabstand("\\newpage");
		File dir = new File("C:/_develop/temp");
		File file = new File(dir + File.separator + "ein_tolles_arbeitsblatt_aufgaben.tex");
		file.delete();
		System.out.println("Ausgabe nach '" + dir.getAbsolutePath() + "'");
		MatheAGGeneratorUtils.writeOutput(dir.getAbsolutePath(), arbeitsblattGenerator, getStammdatenservice());
		assertTrue(file.isFile());
	}

	public void testGenerateLoesungen() {
		arbeitsblatt = createArbeitsblatt();
		arbeitsblattGenerator.setArbeitsblatt(arbeitsblatt);
		arbeitsblattGenerator.setFontsize("\\LARGE\n");
		arbeitsblattGenerator.setAufgabenabstand("\\newpage");
		arbeitsblattGenerator.setAusgabeLoesungen(true);
		File dir = new File("C:/_develop/temp");
		File file = new File(dir + File.separator + "ein_tolles_arbeitsblatt_loesungen.tex");
		file.delete();
		System.out.println("Ausgabe nach '" + dir.getAbsolutePath() + "'");
		MatheAGGeneratorUtils.writeOutput(dir.getAbsolutePath(), arbeitsblattGenerator, getStammdatenservice());
		assertTrue(file.isFile());
	}

	/**
	 * @param pArbeitsblaetterGenerator the arbeitsblaetterGenerator to set
	 */
	public void setArbeitsblattGenerator(ArbeitsblattGenerator pArbeitsblaetterGenerator) {
		arbeitsblattGenerator = pArbeitsblaetterGenerator;
	}

}
