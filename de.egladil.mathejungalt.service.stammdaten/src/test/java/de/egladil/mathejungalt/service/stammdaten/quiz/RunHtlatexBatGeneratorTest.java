/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.service.stammdaten.quiz;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.AssertionFailedException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.service.stammdaten.JUnitUtils;

/**
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class RunHtlatexBatGeneratorTest {

	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	@Test
	public void testGenerate_Happy_Hour() throws IOException {
		// Arrange
		MCArchivraetsel raetsel = JUnitUtils.prepareArchivRaetsel();
		String pathRootDir = folder.getRoot().getAbsolutePath();
		System.out.println("QuizDirectoryBuilderTest.testCreateDirectoryStructurForQuiz_Happy_Hour(): [pathRootDir="
			+ pathRootDir + "]");
		String expectedPathRundBat = pathRootDir + "\\run.bat";
		RunHtlatexBatGenerator generator = new RunHtlatexBatGenerator();

		// Act
		String actualPathRunBat = generator.generate(raetsel, pathRootDir);

		// Assert
		assertEquals(expectedPathRundBat, actualPathRunBat);
		File batDatei = new File(actualPathRunBat);
		@SuppressWarnings("unchecked")
		List<String> lines = FileUtils.readLines(batDatei);
		assertEquals("call process_03.bat", lines.get(0));
		assertEquals("call process_04.bat", lines.get(1));
		assertEquals("cd .\\9000", lines.get(2));
		assertEquals("del *.tex", lines.get(3));
		assertEquals("cd ..", lines.get(4));
		assertEquals("ren *_pre.txt *_post.txt", lines.get(5));
		assertEquals("echo fertig", lines.get(6));
		assertEquals("pause", lines.get(7));
	}

	@Test(expected = AssertionFailedException.class)
	public void testGenerate_Raetsel_Null() throws IOException {
		// Arrange
		String pathRootDir = folder.getRoot().getAbsolutePath();
		System.out.println("QuizDirectoryBuilderTest.testCreateDirectoryStructurForQuiz_Happy_Hour(): [pathRootDir="
			+ pathRootDir + "]");
		RunHtlatexBatGenerator generator = new RunHtlatexBatGenerator();

		// Act
		generator.generate(null, pathRootDir);

		// Assert
	}

	@Test(expected = AssertionFailedException.class)
	public void testGenerate_Path_Null() throws IOException {
		// Arrange
		MCArchivraetsel raetsel = JUnitUtils.prepareArchivRaetsel();
		RunHtlatexBatGenerator generator = new RunHtlatexBatGenerator();

		// Act
		generator.generate(raetsel, null);

		// Assert
	}
}
