/**
 * 
 */
package de.egladil.mathejungalt.service.stammdaten.mock;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguruitem;
import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenzweck;
import de.egladil.mathejungalt.persistence.api.IAufgabenRepository;
import de.egladil.mathejungalt.persistence.api.IMinikaenguruRepository;

/**
 * @author aheike
 */
public class MinikaenguruRepositoryMock implements IMinikaenguruRepository {

	private static final Logger LOG = LoggerFactory.getLogger(MinikaenguruRepositoryMock.class);

	/** */
	private IAufgabenRepository aufgabenRepository;

	/**
	 * 
	 */
	public MinikaenguruRepositoryMock() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IMinikaenguruRepository#findAllWettbewerbe()
	 */
	@Override
	public List<Minikaenguru> findAllWettbewerbe() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IMinikaenguruRepository#findByAufgabe(de.egladil.mathejungalt.domain.
	 * aufgaben.Aufgabe)
	 */
	@Override
	public Minikaenguruitem findByAufgabe(Aufgabe pAufgabe) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IMinikaenguruRepository#findById(java.lang.Long)
	 */
	@Override
	public Minikaenguru findById(Long pId) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IMinikaenguruRepository#getMaxKey()
	 */
	@Override
	public Integer getMaxKey() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.persistence.api.IMinikaenguruRepository#loadItems(de.egladil.mathejungalt.domain.
	 * aufgabensammlungen.minikaenguru.Minikaenguru)
	 */
	@Override
	public void loadItems(Minikaenguru pWettbewerb) {
		int count = 0;
		if (pWettbewerb.isItemsLoaded()) {
			return;
		}
		LOG.info("Anzahl items = " + pWettbewerb.anzahlAufgaben());
		List<Aufgabe> aufgaben = aufgabenRepository.findAll();
		LOG.info("Anzahl neue Aufgaben: " + aufgaben.size());
		for (Aufgabe aufgabe : aufgaben) {
			if (Aufgabenzweck.K.equals(aufgabe.getZweck())) {
				count++;
				Minikaenguruitem item = new Minikaenguruitem();
				item.setAufgabe(aufgabe);
				item.setNummer(aufgabe.getStufe() + "-" + count);
				item.setMinikaenguru(pWettbewerb);
				pWettbewerb.addItem(item);
				if (pWettbewerb.anzahlAufgaben() > aufgaben.size()) {
					LOG.warn("Es kommen zu viele Aufgaben dazu!!!!!!");
				}
			}
		}
		LOG.info("Anzah items = " + pWettbewerb.anzahlAufgaben());
		pWettbewerb.setItemsLoaded(true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.persistence.api.IMinikaenguruRepository#save(de.egladil.mathejungalt.domain.
	 * aufgabensammlungen.minikaenguru.Minikaenguru)
	 */
	@Override
	public void save(Minikaenguru pWettbewerb) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.persistence.api.IMinikaenguruRepository#saveItem(de.egladil.mathejungalt.domain.
	 * aufgabensammlungen.minikaenguru.Minikaenguruitem)
	 */
	@Override
	public void saveItem(Minikaenguruitem pItem) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IMinikaenguruRepository#setAufgabenRepository(de.egladil.mathejungalt
	 * .persistence.api.IAufgabenRepository)
	 */
	@Override
	public void setAufgabenRepository(IAufgabenRepository pAufgabenRepository) {
		aufgabenRepository = pAufgabenRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IMinikaenguruRepository#mailinglisteAktualisieren(de.egladil.mathejungalt
	 * .domain.aufgabensammlungen.minikaenguru.Minikaenguru, java.util.List, java.util.List)
	 */
	@Override
	public void mailinglisteAktualisieren(Minikaenguru pMinikaenguru, List<Kontakt> pLoeschliste,
		List<Kontakt> pZugangsliste) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IMinikaenguruRepository#loadMailingliste(de.egladil.mathejungalt.domain
	 * .aufgabensammlungen.minikaenguru.Minikaenguru)
	 */
	@Override
	public List<Kontakt> loadMailingliste(Minikaenguru pMinikaenguru) {
		// TODO Auto-generated method stub
		return null;
	}

}
