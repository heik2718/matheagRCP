//=====================================================
// Projekt: de.egladil.mathejungalt.service.stammdaten
// (c) Heike Winkelvoß
//=====================================================
// Dateiname: MItgliederseiteGeneratorTest.java                            $
// $Revision:: 1                                     $
// $Modtime:: 29.03.2015                                $
//=====================================================

package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html;

import java.util.ArrayList;
import java.util.List;

import org.mockito.Mockito;

import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.domain.mitglieder.Serienteilnahme;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.contentprovider.FruehContentProvider;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.contentprovider.MitgliederTableContentProvider;
import junit.framework.TestCase;

/**
 * MItgliederseiteGeneratorTest
 */
public class MitgliederseiteGeneratorTest extends TestCase {

	public void testGenerate() {

		IStammdatenservice stammdatenservice = Mockito.mock(IStammdatenservice.class);
		MitgliederTableContentProvider contentProvider = Mockito.mock(MitgliederTableContentProvider.class);

		List<Serie> serien = new ArrayList<Serie>();

		{
			Serie serie = new Serie();
			serie.setId(Long.valueOf(96));
			serie.setNummer(96);
			serien.add(serie);
		}
		{
			Serie serie = new Serie();
			serie.setId(Long.valueOf(97));
			serie.setNummer(97);
			serien.add(serie);
		}
		{
			Serie serie = new Serie();
			serie.setId(Long.valueOf(98));
			serie.setNummer(98);
			serien.add(serie);
		}
		{
			Serie serie = new Serie();
			serie.setId(Long.valueOf(99));
			serie.setNummer(99);
			serien.add(serie);
		}

		List<Mitglied> mitglieder = new ArrayList<Mitglied>();
		{
			Mitglied mitglied = new Mitglied();
			mitglied.setId(Long.valueOf(1l));
			mitglied.setSerienteilnahmenLoaded(true);
			mitglied.setAnzDiplome(1);
			mitglied.setErfinderpunkte(2);
			mitglied.setVorname("Anja");
			mitglied.setAlter(7);
			mitglied.setKlasse(1);

			{
				Serie serie = serien.get(1);
				Serienteilnahme teilnahme = new Serienteilnahme();
				teilnahme.setMitglied(mitglied);
				teilnahme.setSerie(serie);
				teilnahme.setFruehstarter(Double.valueOf("1.33"));
				teilnahme.setPunkte(2);
				mitglied.addSerienteilname(teilnahme);
			}
			{
				Serie serie = serien.get(2);
				Serienteilnahme teilnahme = new Serienteilnahme();
				teilnahme.setMitglied(mitglied);
				teilnahme.setSerie(serie);
				teilnahme.setPunkte(2);
				mitglied.addSerienteilname(teilnahme);
			}
			mitglieder.add(mitglied);
		}

		{
			Mitglied mitglied = new Mitglied();
			mitglied.setId(Long.valueOf(1l));
			mitglied.setSerienteilnahmenLoaded(true);
			mitglied.setVorname("Ben");
			mitglied.setNachname("O");
			mitglied.setAlter(6);
			mitglied.setKlasse(1);

			{
				Serie serie = serien.get(0);
				Serienteilnahme teilnahme = new Serienteilnahme();
				teilnahme.setMitglied(mitglied);
				teilnahme.setSerie(serie);
				teilnahme.setFruehstarter(Double.valueOf("2.0"));
				mitglied.addSerienteilname(teilnahme);
			}
			mitglieder.add(mitglied);
		}

		{
			Mitglied mitglied = new Mitglied();
			mitglied.setId(Long.valueOf(1l));
			mitglied.setSerienteilnahmenLoaded(true);
			mitglied.setVorname("Catalina");
			mitglied.setNachname("Fr");
			mitglied.setAlter(6);
			mitglied.setKlasse(1);

			{
				Serie serie = serien.get(3);
				Serienteilnahme teilnahme = new Serienteilnahme();
				teilnahme.setMitglied(mitglied);
				teilnahme.setSerie(serie);
				teilnahme.setFruehstarter(Double.valueOf("2.5"));
				mitglied.addSerienteilname(teilnahme);
			}
			mitglieder.add(mitglied);
		}

		Mockito.when(contentProvider.getAktiveMitgliederSorted(stammdatenservice, 0)).thenReturn(
			new ArrayList<Mitglied>());
		Mockito.when(contentProvider.getAktiveMitgliederSorted(stammdatenservice, 1)).thenReturn(mitglieder);
		for (int i = 2; i < 15; i++) {
			Mockito.when(contentProvider.getAktiveMitgliederSorted(stammdatenservice, i)).thenReturn(
				new ArrayList<Mitglied>());
		}

		MitgliederseiteGenerator generator = new MitgliederseiteGenerator();
		generator.setContentProvider(contentProvider);

		String content = generator.generate(stammdatenservice);

		assertNotNull(content);

		System.out.println(content);

	}

}
