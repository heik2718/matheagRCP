/**
 * 
 */
package de.egladil.mathejungalt.service.stammdaten.mock;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblattitem;
import de.egladil.mathejungalt.persistence.api.IArbeitsblattRepository;
import de.egladil.mathejungalt.persistence.api.IAufgabenRepository;

/**
 * @author aheike
 */
public class ArbeitsblattRepositoryMock implements IArbeitsblattRepository {

	private static final Logger LOG = LoggerFactory.getLogger(ArbeitsblattRepositoryMock.class);

	/** */
	private IAufgabenRepository aufgabenRepository;

	/**
	 * 
	 */
	public ArbeitsblattRepositoryMock() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IArbeitsblattRepository#findAllArbeitsblaetter()
	 */
	@Override
	public List<Arbeitsblatt> findAllArbeitsblaetter() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IArbeitsblattRepository#findByAufgabe(de.egladil.mathejungalt.domain.
	 * aufgaben.Aufgabe)
	 */
	@Override
	public Arbeitsblattitem findByAufgabe(Aufgabe pAufgabe) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IArbeitsblattRepository#findById(java.lang.Long)
	 */
	@Override
	public Arbeitsblatt findById(Long pId) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IArbeitsblattRepository#getMaxKey()
	 */
	@Override
	public String getMaxKey() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.persistence.api.IArbeitsblattRepository#loadItems(de.egladil.mathejungalt.domain.
	 * aufgabensammlungen.arbeitsblaetter.Arbeitsblatt)
	 */
	@Override
	public void loadItems(Arbeitsblatt pArbeitsblatt) {
		if (pArbeitsblatt.isItemsLoaded()) {
			return;
		}
		int countA = 0;
		int countG = 0;
		int countL = 0;
		LOG.info("Anzahl items = " + pArbeitsblatt.anzahlAufgaben());
		List<Aufgabe> aufgaben = aufgabenRepository.findAll();
		LOG.info("Anzahl neue Aufgaben: " + aufgaben.size());
		for (Aufgabe aufgabe : aufgaben) {
			switch (aufgabe.getThema()) {
			case G:
				countG++;
				break;
			case L:
				countL++;
				break;
			default:
				countA++;
				break;
			}
			Arbeitsblattitem item = new Arbeitsblattitem();
			item.setAufgabe(aufgabe);
			switch (aufgabe.getThema()) {
			case G:
				item.setNummer(aufgabe.getThema().toString() + countG);
				break;
			case L:
				item.setNummer(aufgabe.getThema().toString() + countL);
				break;
			default:
				item.setNummer(aufgabe.getThema().toString() + countA);
				break;
			}
			item.setArbeitsblatt(pArbeitsblatt);
			pArbeitsblatt.addItem(item);
			if (pArbeitsblatt.anzahlAufgaben() > aufgaben.size()) {
				LOG.warn("Es kommen zu viele Aufgaben dazu!!!!!!");
			}
		}
		LOG.info("Anzah items = " + pArbeitsblatt.anzahlAufgaben());
		pArbeitsblatt.setItemsLoaded(true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.persistence.api.IArbeitsblattRepository#save(de.egladil.mathejungalt.domain.
	 * aufgabensammlungen.arbeitsblaetter.Arbeitsblatt)
	 */
	@Override
	public void save(Arbeitsblatt pArbeitsblatt) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IArbeitsblattRepository#setAufgabenRepository(de.egladil.mathejungalt
	 * .persistence.api.IAufgabenRepository)
	 */
	@Override
	public void setAufgabenRepository(IAufgabenRepository pAufgabenRepository) {
		aufgabenRepository = pAufgabenRepository;
	}

}
