/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.mock;

import de.egladil.mathejungalt.persistence.api.IArbeitsblattRepository;
import de.egladil.mathejungalt.persistence.api.IAufgabenRepository;
import de.egladil.mathejungalt.persistence.api.IAutorenRepository;
import de.egladil.mathejungalt.persistence.api.IHefteRepository;
import de.egladil.mathejungalt.persistence.api.IKontakteRepository;
import de.egladil.mathejungalt.persistence.api.IMailinglisteRepository;
import de.egladil.mathejungalt.persistence.api.IMcAufgabenRepository;
import de.egladil.mathejungalt.persistence.api.IMcRaetselRepository;
import de.egladil.mathejungalt.persistence.api.IMedienRepository;
import de.egladil.mathejungalt.persistence.api.IMinikaenguruRepository;
import de.egladil.mathejungalt.persistence.api.IMitgliederRepository;
import de.egladil.mathejungalt.persistence.api.IPersistenceservice;
import de.egladil.mathejungalt.persistence.api.IQuellenRepository;
import de.egladil.mathejungalt.persistence.api.ISchulenRepository;
import de.egladil.mathejungalt.persistence.api.ISerienRepository;
import de.egladil.mathejungalt.persistence.api.IUrheberRepository;
import de.egladil.mathejungalt.service.stammdaten.mock.AufgabenRepositoryMock;

/**
 * @author aheike
 */
public class PersistenceServiceMock implements IPersistenceservice {

	/** */
	private ISerienRepository serienRepository = new SerienRepositoryMock();

	/** */
	private IAufgabenRepository aufgabenRepository = new AufgabenRepositoryMock();

	/** */
	private IArbeitsblattRepository arbeitsblattRepository = new ArbeitsblattRepositoryMock();

	/** */
	private IMinikaenguruRepository minikaenguruRepository = new MinikaenguruRepositoryMock();

	/**
	 *
	 */
	public PersistenceServiceMock() {

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getArbeitsblattRepository()
	 */
	@Override
	public IArbeitsblattRepository getArbeitsblattRepository() {
		return arbeitsblattRepository;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getAufgabenRepository()
	 */
	@Override
	public IAufgabenRepository getAufgabenRepository() {
		return aufgabenRepository;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getAutorenRepository()
	 */
	@Override
	public IAutorenRepository getAutorenRepository() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getHefteRepository()
	 */
	@Override
	public IHefteRepository getHefteRepository() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getMailinglisteRepository()
	 */
	@Override
	public IMailinglisteRepository getMailinglisteRepository() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getMedienRepository()
	 */
	@Override
	public IMedienRepository getMedienRepository() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getMinikaenguruRepository()
	 */
	@Override
	public IMinikaenguruRepository getMinikaenguruRepository() {
		return minikaenguruRepository;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getMitgliederRepository()
	 */
	@Override
	public IMitgliederRepository getMitgliederRepository() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getProtokolle()
	 */
	@Override
	public String[] getProtokolle() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getQuellenRepository()
	 */
	@Override
	public IQuellenRepository getQuellenRepository() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getSerienRepository()
	 */
	@Override
	public ISerienRepository getSerienRepository() {
		return serienRepository;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#shutDown()
	 */
	@Override
	public boolean shutDown() {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * @param pSerienRepository the serienRepository to set
	 */
	public void setSerienRepository(ISerienRepository pSerienRepository) {
		serienRepository = pSerienRepository;
	}

	/**
	 * @param pAufgabenRepository the aufgabenRepository to set
	 */
	public void setAufgabenRepository(IAufgabenRepository pAufgabenRepository) {
		aufgabenRepository = pAufgabenRepository;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getPersistenceLocation()
	 */
	@Override
	public String getPersistenceLocation() {
		return "MOCK";
	}

	/**
	 * @param pMinikaenguruRepository the minikaenguruRepository to set
	 */
	public void setMinikaenguruRepository(IMinikaenguruRepository pMinikaenguruRepository) {
		minikaenguruRepository = pMinikaenguruRepository;
	}

	/**
	 * @param pArbeitsblattRepository the arbeitsblattRepository to set
	 */
	public void setArbeitsblattRepository(IArbeitsblattRepository pArbeitsblattRepository) {
		arbeitsblattRepository = pArbeitsblattRepository;
	}

	public final IUrheberRepository getUrheberRepository() {
		return null;
	}

	public final IMcRaetselRepository getRaetselRepository() {
		return null;
	}

	public final IMcAufgabenRepository getMcAufgabenRepository() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getSchulenRepository()
	 */
	@Override
	public ISchulenRepository getSchulenRepository() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getKontakteRepository()
	 */
	@Override
	public IKontakteRepository getKontakteRepository() {
		// TODO Auto-generated method stub
		return null;
	}
}
