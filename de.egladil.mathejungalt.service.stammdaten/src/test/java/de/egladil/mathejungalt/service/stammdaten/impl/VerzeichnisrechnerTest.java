/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl;

import junit.framework.TestCase;

/**
 * @author heike
 */
public class VerzeichnisrechnerTest extends TestCase {

	/*
	 * (non-Javadoc)
	 * 
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	public void testDetermineVerzeichnisZuSchluessel01() {
		Verzeichnisrechner verzeichnisrechner = new Verzeichnisrechner();
		String schluessel = "00999";
		String expected = "001";
		String actual = verzeichnisrechner.determineVerzeichnisZuSchluessel(schluessel);
		assertEquals(expected, actual);
	}

	public void testDetermineVerzeichnisZuSchluessel02() {
		Verzeichnisrechner verzeichnisrechner = new Verzeichnisrechner();
		String schluessel = "01000";
		String expected = "002";
		String actual = verzeichnisrechner.determineVerzeichnisZuSchluessel(schluessel);
		assertEquals(expected, actual);
	}

	public void testDetermineVerzeichnisZuSchluesselAll() {
		Verzeichnisrechner verzeichnisrechner = new Verzeichnisrechner();
		for (int i = 1; i < 100000; i++) {
			String schluessel = i + "";
			while (schluessel.length() < 5) {
				schluessel = "0" + schluessel;
			}
			int expectedNumber = i / 1000 + 1;
			String expected = expectedNumber + "";
			while (expected.length() < 3) {
				expected = "0" + expected;
			}
			String actual = verzeichnisrechner.determineVerzeichnisZuSchluessel(schluessel);
			assertEquals("Fehler bei " + i, expected, actual);
		}
	}
}
