/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgaben.Heft;
import de.egladil.mathejungalt.domain.quellen.NullQuelle;
import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenart;
import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenzweck;
import de.egladil.mathejungalt.domain.types.EnumTypes.Thema;
import de.egladil.mathejungalt.persistence.api.IAufgabenRepository;
import de.egladil.mathejungalt.persistence.api.IHefteRepository;
import de.egladil.mathejungalt.persistence.api.IQuellenRepository;

/**
 * @author aheike
 */
public class AufgabenRepositoryMock implements IAufgabenRepository {

	/**
	 *
	 */
	public AufgabenRepositoryMock() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IAufgabenRepository#anzahlSerienWettbewerbeMitAufgabe(de.egladil.mathejungalt
	 * .domain.aufgaben.Aufgabe)
	 */
	@Override
	public int anzahlMinikaenguruMitAufgabe(Aufgabe pAufgabe) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IAufgabenRepository#anzahlSperrendeArbeitsblaetterMitAufgabe(de.egladil
	 * .mathejungalt .domain.aufgaben.Aufgabe)
	 */
	@Override
	public int anzahlSperrendeArbeitsblaetterMitAufgabe(Aufgabe pAufgabe) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IAufgabenRepository#findAll()
	 */
	@Override
	public List<Aufgabe> findAll() {
		List<Aufgabe> aufgaben = new ArrayList<Aufgabe>();
		NullQuelle nullQuelle = new NullQuelle();
		Aufgabe aufg = new Aufgabe();
		aufg.setSchluessel("00001");
		aufg.setArt(Aufgabenart.E);
		aufg.setQuelle(nullQuelle);
		aufg.setStufe(1);
		aufgaben.add(aufg);

		aufg = new Aufgabe();
		aufg.setSchluessel("00002");
		aufg.setArt(Aufgabenart.E);
		aufg.setQuelle(nullQuelle);
		aufg.setStufe(1);
		aufgaben.add(aufg);

		aufg = new Aufgabe();
		aufg.setSchluessel("00003");
		aufg.setArt(Aufgabenart.E);
		aufg.setQuelle(nullQuelle);
		aufg.setStufe(2);
		aufgaben.add(aufg);

		aufg = new Aufgabe();
		aufg.setSchluessel("00004");
		aufg.setArt(Aufgabenart.E);
		aufg.setQuelle(nullQuelle);
		aufg.setStufe(2);
		aufg.setThema(Thema.G);
		aufgaben.add(aufg);

		aufg = new Aufgabe();
		aufg.setSchluessel("00005");
		aufg.setArt(Aufgabenart.E);
		aufg.setQuelle(nullQuelle);
		aufg.setStufe(3);
		aufg.setThema(Thema.G);
		aufgaben.add(aufg);

		aufg = new Aufgabe();
		aufg.setSchluessel("00006");
		aufg.setArt(Aufgabenart.E);
		aufg.setQuelle(nullQuelle);
		aufg.setStufe(3);
		aufgaben.add(aufg);

		aufg = new Aufgabe();
		aufg.setSchluessel("00007");
		aufg.setArt(Aufgabenart.E);
		aufg.setQuelle(nullQuelle);
		aufg.setStufe(4);
		aufgaben.add(aufg);

		aufg = new Aufgabe();
		aufg.setSchluessel("00008");
		aufg.setArt(Aufgabenart.E);
		aufg.setQuelle(nullQuelle);
		aufg.setStufe(4);
		aufg.setThema(Thema.L);
		aufgaben.add(aufg);

		aufg = new Aufgabe();
		aufg.setSchluessel("00009");
		aufg.setArt(Aufgabenart.E);
		aufg.setQuelle(nullQuelle);
		aufg.setStufe(5);
		aufgaben.add(aufg);

		aufg = new Aufgabe();
		aufg.setSchluessel("00010");
		aufg.setArt(Aufgabenart.E);
		aufg.setQuelle(nullQuelle);
		aufg.setStufe(5);
		aufg.setThema(Thema.L);
		aufgaben.add(aufg);

		aufg = new Aufgabe();
		aufg.setSchluessel("00011");
		aufg.setArt(Aufgabenart.E);
		aufg.setQuelle(nullQuelle);
		aufg.setStufe(6);
		aufg.setThema(Thema.G);
		aufgaben.add(aufg);

		aufg = new Aufgabe();
		aufg.setSchluessel("00012");
		aufg.setArt(Aufgabenart.E);
		aufg.setQuelle(nullQuelle);
		aufg.setStufe(6);
		aufgaben.add(aufg);

		// Minikaenguru
		aufg = new Aufgabe();
		aufg.setSchluessel("00013");
		aufg.setArt(Aufgabenart.E);
		aufg.setQuelle(nullQuelle);
		aufg.setStufe(3);
		aufg.setZweck(Aufgabenzweck.K);
		aufgaben.add(aufg);

		aufg = new Aufgabe();
		aufg.setSchluessel("00014");
		aufg.setArt(Aufgabenart.E);
		aufg.setQuelle(nullQuelle);
		aufg.setStufe(3);
		aufg.setZweck(Aufgabenzweck.K);
		aufgaben.add(aufg);

		aufg = new Aufgabe();
		aufg.setSchluessel("00015");
		aufg.setArt(Aufgabenart.E);
		aufg.setQuelle(nullQuelle);
		aufg.setStufe(4);
		aufg.setZweck(Aufgabenzweck.K);
		aufgaben.add(aufg);

		aufg = new Aufgabe();
		aufg.setSchluessel("00016");
		aufg.setArt(Aufgabenart.E);
		aufg.setQuelle(nullQuelle);
		aufg.setStufe(4);
		aufg.setZweck(Aufgabenzweck.K);
		aufgaben.add(aufg);

		aufg = new Aufgabe();
		aufg.setSchluessel("00017");
		aufg.setArt(Aufgabenart.E);
		aufg.setQuelle(nullQuelle);
		aufg.setStufe(5);
		aufg.setZweck(Aufgabenzweck.K);
		aufgaben.add(aufg);

		aufg = new Aufgabe();
		aufg.setSchluessel("00018");
		aufg.setArt(Aufgabenart.E);
		aufg.setQuelle(nullQuelle);
		aufg.setStufe(5);
		aufg.setZweck(Aufgabenzweck.K);
		aufgaben.add(aufg);

		return aufgaben;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IAufgabenRepository#findByAttributeMap(java.util.Map, boolean,
	 * boolean)
	 */
	@Override
	public List<Aufgabe> findByAttributeMap(Map<String, Object> pAttributeMap, boolean pExact, boolean pAnd) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IAufgabenRepository#findById(long)
	 */
	@Override
	public Aufgabe findById(long pId) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IAufgabenRepository#findByKey(java.lang.String)
	 */
	@Override
	public Aufgabe findByKey(String pString) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IAufgabenRepository#getMaxKey()
	 */
	@Override
	public String getMaxKey() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IAufgabenRepository#loadAufgabenZuHeft(de.egladil.mathejungalt.domain
	 * .aufgaben.Heft)
	 */
	@Override
	public void loadAufgabenZuHeft(Heft pHeft) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IAufgabenRepository#save(de.egladil.mathejungalt.domain.aufgaben.Aufgabe)
	 */
	@Override
	public void save(Aufgabe pAufgabe) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IAufgabenRepository#setHefteRepository(de.egladil.mathejungalt.persistence
	 * .api.IHefteRepository)
	 */
	@Override
	public void setHefteRepository(IHefteRepository pHefteRepository) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IAufgabenRepository#setQuellenRepository(de.egladil.mathejungalt.persistence
	 * .api.IQuellenRepository)
	 */
	@Override
	public void setQuellenRepository(IQuellenRepository pQuellenRepository) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IAufgabenRepository#serienIdMitAufgabe(de.egladil.mathejungalt.domain
	 * .aufgaben.Aufgabe)
	 */
	@Override
	public List<Long> serienIdMitAufgabe(Aufgabe pAufgabe) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IAufgabenRepository#changeZuSchlechtFuerSerie(de.egladil.mathejungalt
	 * .domain.aufgaben.Aufgabe)
	 */
	@Override
	public void changeZuSchlechtFuerSerie(Aufgabe pAufgabe) {
		// TODO Auto-generated method stub

	}

}
