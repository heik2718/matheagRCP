//=====================================================
// Projekt: de.egladil.mathejungalt.service.stammdaten
// (c) Heike Winkelvoß
//=====================================================
// Dateiname: FruestarterseiteGeneratorTest.java                            $
// $Revision:: 1                                     $
// $Modtime:: 29.03.2015                                $
//=====================================================

package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html;

import java.util.ArrayList;
import java.util.List;

import org.mockito.Mockito;

import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.domain.mitglieder.Serienteilnahme;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.contentprovider.FruehContentProvider;
import junit.framework.TestCase;

/**
 * FruestarterseiteGeneratorTest
 */
public class FruestarterseiteGeneratorTest extends TestCase {

	public void testGenerate() {
		IStammdatenservice stammdatenservice = Mockito.mock(IStammdatenservice.class);
		FruehContentProvider contentProvider = Mockito.mock(FruehContentProvider.class);

		List<Serie> serien = new ArrayList<Serie>();

		{
			Serie serie = new Serie();
			serie.setId(Long.valueOf(96));
			serie.setNummer(96);
			serien.add(serie);
		}
		{
			Serie serie = new Serie();
			serie.setId(Long.valueOf(97));
			serie.setNummer(97);
			serien.add(serie);
		}
		{
			Serie serie = new Serie();
			serie.setId(Long.valueOf(98));
			serie.setNummer(98);
			serien.add(serie);
		}
		{
			Serie serie = new Serie();
			serie.setId(Long.valueOf(99));
			serie.setNummer(99);
			serien.add(serie);
		}

		List<Mitglied> mitglieder = new ArrayList<Mitglied>();
		{
			Mitglied mitglied = new Mitglied();
			mitglied.setId(Long.valueOf(1l));
			mitglied.setSerienteilnahmenLoaded(true);
			mitglied.setVorname("Anja");
			mitglied.setAlter(8);
			mitglied.setKlasse(3);

			{
				Serie serie = serien.get(1);
				Serienteilnahme teilnahme = new Serienteilnahme();
				teilnahme.setMitglied(mitglied);
				teilnahme.setSerie(serie);
				teilnahme.setFruehstarter(Double.valueOf("1.33"));
				mitglied.addSerienteilname(teilnahme);
			}
			{
				Serie serie = serien.get(2);
				Serienteilnahme teilnahme = new Serienteilnahme();
				teilnahme.setMitglied(mitglied);
				teilnahme.setSerie(serie);
				mitglied.addSerienteilname(teilnahme);
			}
			mitglieder.add(mitglied);
		}

		{
			Mitglied mitglied = new Mitglied();
			mitglied.setId(Long.valueOf(1l));
			mitglied.setSerienteilnahmenLoaded(true);
			mitglied.setVorname("Ben");
			mitglied.setNachname("O");
			mitglied.setAlter(6);
			mitglied.setKlasse(1);

			{
				Serie serie = serien.get(0);
				Serienteilnahme teilnahme = new Serienteilnahme();
				teilnahme.setMitglied(mitglied);
				teilnahme.setSerie(serie);
				teilnahme.setFruehstarter(Double.valueOf("2.0"));
				mitglied.addSerienteilname(teilnahme);
			}
			mitglieder.add(mitglied);
		}

		{
			Mitglied mitglied = new Mitglied();
			mitglied.setId(Long.valueOf(1l));
			mitglied.setSerienteilnahmenLoaded(true);
			mitglied.setVorname("Catalina");
			mitglied.setNachname("Fr");
			mitglied.setAlter(6);
			mitglied.setKlasse(1);

			{
				Serie serie = serien.get(3);
				Serienteilnahme teilnahme = new Serienteilnahme();
				teilnahme.setMitglied(mitglied);
				teilnahme.setSerie(serie);
				teilnahme.setFruehstarter(Double.valueOf("2.5"));
				mitglied.addSerienteilname(teilnahme);
			}
			mitglieder.add(mitglied);
		}



		Mockito.when(contentProvider.getMitgliederMitFruestartern(stammdatenservice)).thenReturn(mitglieder);
		Mockito.when(contentProvider.getSerieAktuelleRundeSorted(stammdatenservice)).thenReturn(serien);

		FruehstarterseiteGenerator generator = new FruehstarterseiteGenerator();
		generator.setContentProvider(contentProvider);

		String content = generator.generate(stammdatenservice);

		assertNotNull(content);

		System.out.println(content);
	}
}
