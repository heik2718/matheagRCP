/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.service.stammdaten.quiz;

import static org.junit.Assert.assertEquals;

import org.eclipse.core.runtime.AssertionFailedException;
import org.junit.Test;

import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.domain.types.MCAufgabenstufen;
import de.egladil.mathejungalt.service.stammdaten.JUnitUtils;

/**
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 *
 */
public class HtmlAndroidImageTagBuilderTest {

	@Test
	public void testProcess_Happy_Hour() {
		// Arrange
		String stringToProcess = "src=\"aufgabe0x.png\" alt=\"PIC\" class=\"graphics\" width=\"227.62204pt\" height=\"131.94295pt\" ><!--tex4ht:graphics";
		String stufe = "MINI";
		String quizschluessel = "0001";
		String nummer = "01";
		String expected = "src=\"file:///android_asset/files/MINI/0001/01/aufgabe0x.png\" alt=\"PIC\" class=\"graphics\" width=\"227.62204pt\" height=\"131.94295pt\" ><!--tex4ht:graphics";
		HtmlAndroidImageTagBuilder builder = new HtmlAndroidImageTagBuilder();
		// Act
		String actual = builder.transform(stringToProcess, stufe, quizschluessel, nummer);
		// Assert
		assertEquals(expected, actual);
		System.out.println(actual);
	}

	@Test
	public void testTransform_Hapy_Hour() {
		// Arrange
		MCArchivraetselItem item = JUnitUtils.prepareCompleteRaetselitemForTest();
		String stringToProcess = "src=\"aufgabe0x.png\" alt=\"PIC\" class=\"graphics\" width=\"227.62204pt\" height=\"131.94295pt\" ><!--tex4ht:graphics";
		String expected = "src=\"file:///android_asset/files/STUFE_2/9999/01/aufgabe0x.png\" alt=\"PIC\" class=\"graphics\" width=\"227.62204pt\" height=\"131.94295pt\" ><!--tex4ht:graphics";
		HtmlAndroidImageTagBuilder builder = new HtmlAndroidImageTagBuilder();
		// Act
		String actual = builder.transform(stringToProcess, item);
		// Assert
		assertEquals(expected, actual);
		System.out.println(actual);
	}

	@Test(expected = AssertionFailedException.class)
	public void testTransform_ItemNull() {
		// Arrange
		MCArchivraetselItem item = null;
		String stringToProcess = "src=\"aufgabe0x.png\" alt=\"PIC\" class=\"graphics\" width=\"227.62204pt\" height=\"131.94295pt\" ><!--tex4ht:graphics";
		HtmlAndroidImageTagBuilder builder = new HtmlAndroidImageTagBuilder();
		// Act
		builder.transform(stringToProcess, item);
		// Assert
	}

	@Test(expected = AssertionFailedException.class)
	public void testTransform_StufeNull() {
		// Arrange
		MCArchivraetselItem item = JUnitUtils.prepareCompleteRaetselitemForTest();
		item.getAufgabe().setStufe(null);
		String stringToProcess = "src=\"aufgabe0x.png\" alt=\"PIC\" class=\"graphics\" width=\"227.62204pt\" height=\"131.94295pt\" ><!--tex4ht:graphics";
		HtmlAndroidImageTagBuilder builder = new HtmlAndroidImageTagBuilder();
		// Act
		builder.transform(stringToProcess, item);
		// Assert
	}

	@Test
	public void testMcAufgabenstufeToAndroidStufeString_mini() {
		// Arrange
		String titel = "Minikänguru 2010";
		MCAufgabenstufen stufe = MCAufgabenstufen.STUFE_12;
		String expected = "MINI";
		HtmlAndroidImageTagBuilder builder = new HtmlAndroidImageTagBuilder();
		// Act
		String actual = builder.mcAufgabenstufeToAndroidStufeString(titel, stufe);
		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void testMcAufgabenstufeToAndroidStufeString_stufe2() {
		// Arrange
		String titel = "Mathe jung alt 6";
		MCAufgabenstufen stufe = MCAufgabenstufen.STUFE_12;
		String expected = "STUFE_2";
		HtmlAndroidImageTagBuilder builder = new HtmlAndroidImageTagBuilder();
		// Act
		String actual = builder.mcAufgabenstufeToAndroidStufeString(titel, stufe);
		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void testMcAufgabenstufeToAndroidStufeString_stufe3() {
		// Arrange
		String titel = "Mathe jung alt 6";
		MCAufgabenstufen stufe = MCAufgabenstufen.STUFE_34;
		String expected = "STUFE_3";
		HtmlAndroidImageTagBuilder builder = new HtmlAndroidImageTagBuilder();
		// Act
		String actual = builder.mcAufgabenstufeToAndroidStufeString(titel, stufe);
		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void testMcAufgabenstufeToAndroidStufeString_stufe4() {
		// Arrange
		String titel = "Mathe jung alt 6";
		MCAufgabenstufen stufe = MCAufgabenstufen.STUFE_56;
		String expected = "STUFE_4";
		HtmlAndroidImageTagBuilder builder = new HtmlAndroidImageTagBuilder();
		// Act
		String actual = builder.mcAufgabenstufeToAndroidStufeString(titel, stufe);
		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void testMcAufgabenstufeToAndroidStufeString_stufe5() {
		// Arrange
		String titel = "Mathe jung alt 6";
		MCAufgabenstufen stufe = MCAufgabenstufen.STUFE_78;
		String expected = "STUFE_5";
		HtmlAndroidImageTagBuilder builder = new HtmlAndroidImageTagBuilder();
		// Act
		String actual = builder.mcAufgabenstufeToAndroidStufeString(titel, stufe);
		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void testMcAufgabenstufeToAndroidStufeString_stufe6_910() {
		// Arrange
		String titel = "Mathe jung alt 6";
		MCAufgabenstufen stufe = MCAufgabenstufen.STUFE_910;
		String expected = "STUFE_6";
		HtmlAndroidImageTagBuilder builder = new HtmlAndroidImageTagBuilder();
		// Act
		String actual = builder.mcAufgabenstufeToAndroidStufeString(titel, stufe);
		// Assert
		assertEquals(expected, actual);
	}

	@Test
	public void testMcAufgabenstufeToAndroidStufeString_stufe6_erw() {
		// Arrange
		String titel = "Mathe jung alt 6";
		MCAufgabenstufen stufe = MCAufgabenstufen.STUFE_ERW;
		String expected = "STUFE_6";
		HtmlAndroidImageTagBuilder builder = new HtmlAndroidImageTagBuilder();
		// Act
		String actual = builder.mcAufgabenstufeToAndroidStufeString(titel, stufe);
		// Assert
		assertEquals(expected, actual);
	}
}
