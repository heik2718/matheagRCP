/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl;

import java.util.Date;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serienitem;
import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenart;
import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenzweck;
import de.egladil.mathejungalt.domain.types.EnumTypes.Thema;

/**
 * @author aheike
 */
public class SerienserviceTest extends AbstractStammdatenserviceTest {

	private Aufgabe getAufgabe() {
		Aufgabe aufgabe = new Aufgabe();
		aufgabe.setArt(Aufgabenart.E);
		aufgabe.setBeschreibung("bjdhsh");
		aufgabe.setGesperrtFuerWettbewerb("N");
		aufgabe.setId(62189l);
		aufgabe.setSchluessel("98263");
		aufgabe.setStufe(5);
		aufgabe.setThema(Thema.G);
		aufgabe.setTitel("ljdsh");
		aufgabe.setZweck(Aufgabenzweck.S);
		return aufgabe;
	}

	private Serie getSerie() {
		Serie serie = new Serie();
		serie.setBeendet(IAufgabensammlung.NICHT_BEENDET);
		serie.setDatum(new Date());
		serie.setId(621916l);
		serie.setMonatJahrText("GZUFZU GF");
		serie.setNummer(671);
		serie.setRunde(78);
		serie.setItemsLoaded(true);
		return serie;
	}

	/**
	 *
	 */
	public void testAddAufgabeZuSerieSerieNull() {
		try {
			getStammdatenservice().aufgabeZuSammlungHinzufuegen(null, getAufgabe());
			fail("keine MatheJungAltException");
		} catch (MatheJungAltException e) {
			assertTrue(e.getMessage().contains("null"));
			System.out.println(e.getMessage());
		}
	}

	/**
	 *
	 */
	public void testAddAufgabeZuSerieSerieBeendet() {
		Serie serie = getSerie();
		serie.setBeendet(IAufgabensammlung.BEENDET);
		try {
			getStammdatenservice().aufgabeZuSammlungHinzufuegen(serie, getAufgabe());
			fail("keine MatheJungAltException");
		} catch (MatheJungAltException e) {
			assertTrue(e.getMessage().contains("beendet"));
			System.out.println(e.getMessage());
		}
	}

	/**
	 *
	 */
	public void testAddAufgabeZuSerieAufgabeGesperrt() {
		Serie serie = getSerie();
		Aufgabe aufgabe = getAufgabe();
		aufgabe.setGesperrtFuerWettbewerb("J");
		try {
			getStammdatenservice().aufgabeZuSammlungHinzufuegen(serie, aufgabe);
			fail("keine MatheJungAltException");
		} catch (MatheJungAltException e) {
			assertTrue(e.getMessage().contains("gesperrt"));
			System.out.println(e.getMessage());
		}
	}

	/**
	 *
	 */
	public void testAddAufgabeZuSerieAufgabeFalscherZweckK() {
		Serie serie = getSerie();
		Aufgabe aufgabe = getAufgabe();
		aufgabe.setZweck(Aufgabenzweck.K);
		try {
			getStammdatenservice().aufgabeZuSammlungHinzufuegen(serie, aufgabe);
			fail("keine MatheJungAltException");
		} catch (MatheJungAltException e) {
			assertTrue(e.getMessage().contains("Serienaufgabe"));
			System.out.println(e.getMessage());
		}
	}

	/**
	 *
	 */
	public void testAddAufgabeZuSerieAufgabeFalscherZweckM() {
		Serie serie = getSerie();
		Aufgabe aufgabe = getAufgabe();
		aufgabe.setZweck(Aufgabenzweck.M);
		try {
			getStammdatenservice().aufgabeZuSammlungHinzufuegen(serie, aufgabe);
			fail("keine MatheJungAltException");
		} catch (MatheJungAltException e) {
			assertTrue(e.getMessage().contains("Serienaufgabe"));
			System.out.println(e.getMessage());
		}
	}

	/**
	 *
	 */
	public void testAddAufgabeZuSerieufgabeNull() {
		try {
			getStammdatenservice().aufgabeZuSammlungHinzufuegen(getSerie(), null);
			fail("keine MatheJungAltException");
		} catch (MatheJungAltException e) {
			assertTrue(e.getMessage().contains("null"));
			System.out.println(e.getMessage());
		}
	}

	public void testAddAufgabeZuSerie() {
		Aufgabe aufgabe = getAufgabe();
		Serie serie = getSerie();
		Serienitem item = (Serienitem) getStammdatenservice().aufgabeZuSammlungHinzufuegen(serie, aufgabe);
		assertNotNull(item);
		assertEquals(aufgabe, item.getAufgabe());
		assertEquals(serie, item.getSerie());
		assertTrue(serie.getItems().contains(item));
		assertEquals("J", aufgabe.getGesperrtFuerWettbewerb());
	}

	public void testAufgabeAusSerieEntfernenItemNull() {
		try {
			getStammdatenservice().aufgabeAusSammlungEntfernen(null);
			fail("keine MatheJungAltException");
		} catch (MatheJungAltException e) {
			System.out.println(e.getMessage());
			assertTrue(e.getMessage().contains("null"));
		}
	}

	public void testAufgabeAusSerieEntfernen() {
		Aufgabe aufgabe = getAufgabe();
		Serie serie = getSerie();
		Serienitem item = new Serienitem();
		item.setAufgabe(aufgabe);
		item.setSerie(serie);
		serie.addItem(item);
		aufgabe.setGesperrtFuerWettbewerb("J");

		getStammdatenservice().aufgabeAusSammlungEntfernen(item);
		assertFalse(serie.getItems().contains(item));
		assertEquals("N", aufgabe.getGesperrtFuerWettbewerb());
	}
}
