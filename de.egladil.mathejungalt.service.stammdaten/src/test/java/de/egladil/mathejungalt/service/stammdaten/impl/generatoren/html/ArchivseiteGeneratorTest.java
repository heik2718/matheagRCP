//=====================================================
// Projekt: de.egladil.mathejungalt.service.stammdaten
// (c) Heike Winkelvoß
//=====================================================
// Dateiname: ArchivseiteGeneratorNeuTest.java                            $
// $Revision:: 1                                     $
// $Modtime:: 28.03.2015                                $
//=====================================================

package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.mockito.Mockito;

import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.impl.AbstractStammdatenserviceTest;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.contentprovider.ArchivTableContentProvider;

/**
 * ArchivseiteGeneratorNeuTest
 */
public class ArchivseiteGeneratorTest extends AbstractStammdatenserviceTest {

	/**
	 *
	 */
	public void testGenerate() {
		// Arrange
		ArchivseiteGenerator generator = new ArchivseiteGenerator();
		ArchivTableContentProvider contentProvider = Mockito.mock(ArchivTableContentProvider.class);

		List<Serie> serien = new ArrayList<Serie>();
		{
			Serie serie = new Serie();
			serie.setBeendet(1);
			serie.setDatum(new Date());
			serie.setId(Long.valueOf(1l));
			serie.setItemsLoaded(true);
			serie.setMonatJahrText("SEPT/OKT 2013");
			serie.setNummer(1);
			serie.setRunde(12);
			serien.add(serie);
		}
		{
			Serie serie = new Serie();
			serie.setBeendet(1);
			serie.setDatum(new Date());
			serie.setId(Long.valueOf(2l));
			serie.setItemsLoaded(true);
			serie.setMonatJahrText("NOV/DEZ 2013");
			serie.setNummer(2);
			serie.setRunde(12);
			serien.add(serie);
		}
		{
			Serie serie = new Serie();
			serie.setBeendet(1);
			serie.setDatum(new Date());
			serie.setId(Long.valueOf(3l));
			serie.setItemsLoaded(true);
			serie.setMonatJahrText("JAN/FEB 2014");
			serie.setNummer(3);
			serie.setRunde(12);
			serien.add(serie);
		}
		{
			Serie serie = new Serie();
			serie.setBeendet(1);
			serie.setDatum(new Date());
			serie.setId(Long.valueOf(4l));
			serie.setItemsLoaded(true);
			serie.setMonatJahrText("MRZ/APR 2014");
			serie.setNummer(4);
			serie.setRunde(12);
			serien.add(serie);
		}

		IStammdatenservice stammdatenservice = getStammdatenservice();
		Mockito.when(contentProvider.getSerien(stammdatenservice)).thenReturn(serien);

		generator.setContentProvider(contentProvider);

		// Act
		String contents = generator.generate(stammdatenservice);

		// Assert
		assertNotNull(contents);

		System.out.println(contents);

	}
}
