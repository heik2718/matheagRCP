//=====================================================
// Projekt: de.egladil.mathejungalt.service.stammdaten
// (c) Heike Winkelvoß
//=====================================================
// Dateiname: SerieIndexGeneratorTest.java                            $
// $Revision:: 1                                     $
// $Modtime:: 29.03.2015                                $
//=====================================================

package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html;

import java.util.Date;

import junit.framework.TestCase;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;

/**
 * SerieIndexGeneratorTest
 */
public class SerienseiteGeneratorTest extends TestCase {

	public void testGenerat() {
		Serie serie = new Serie();
		serie.setBeendet(99);
		serie.setDatum(new Date());
		serie.setId(Long.valueOf(1l));
		serie.setItemsLoaded(true);
		serie.setMonatJahrText("MÄRZ/APRIL 2015");
		serie.setNummer(99);
		serie.setRunde(13);



		SerienseiteGenerator generator = new SerienseiteGenerator();

		String content = generator.generate(serie);

		assertNotNull(content);

		System.out.println(content);

	}

}
