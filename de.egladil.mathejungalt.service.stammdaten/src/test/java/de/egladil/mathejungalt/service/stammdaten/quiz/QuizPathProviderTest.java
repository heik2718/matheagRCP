/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.service.stammdaten.quiz;

import static org.junit.Assert.assertEquals;

import org.eclipse.core.runtime.AssertionFailedException;
import org.junit.Test;

import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.service.stammdaten.JUnitUtils;

/**
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class QuizPathProviderTest {

	private static final String PATH_WORKDIR = "C:\\work\\knobelarchiv_2\\latex\\tempdir_junit";

	@Test
	public void testGetPathQuizRoot_Happy_Hour() {
		// Arrange
		MCArchivraetsel raetsel = new MCArchivraetsel();
		raetsel.setSchluessel("9997");
		String expected = PATH_WORKDIR + "\\9997";

		// Act
		String path = QuizPathProvider.getPathQuizRoot(raetsel, PATH_WORKDIR);

		// Assert
		assertEquals(expected, path);
	}

	@Test(expected = AssertionFailedException.class)
	public void testGetPathQuizRoot_SchluesselNull() {
		// Arrange
		MCArchivraetsel raetsel = new MCArchivraetsel();
		raetsel.setSchluessel(null);

		// Act
		QuizPathProvider.getPathQuizRoot(raetsel, PATH_WORKDIR);
	}

	@Test(expected = AssertionFailedException.class)
	public void testGetPathQuizRoot_SchluesselEmpty() {
		// Arrange
		MCArchivraetsel raetsel = new MCArchivraetsel();
		raetsel.setSchluessel("");
		// Act
		QuizPathProvider.getPathQuizRoot(raetsel, PATH_WORKDIR);
	}

	@Test(expected = AssertionFailedException.class)
	public void testGetPathQuizRoot_QuizNull() {
		// Arrange

		// Act
		QuizPathProvider.getPathQuizRoot(null, PATH_WORKDIR);
	}

	@Test
	public void testGetRelativPathQuiz_Happy_Hour() {
		// Arrange
		MCArchivraetsel raetsel = new MCArchivraetsel();
		raetsel.setSchluessel("9997");
		String expected = "9997";
		// Act
		String path = QuizPathProvider.getGetRelativPathQuiz(raetsel);

		// Assert
		assertEquals(expected, path);
	}

	@Test(expected = AssertionFailedException.class)
	public void testGetRelativPathQuiz_RaetselNull() {
		// Arrange
		// Act
		QuizPathProvider.getGetRelativPathQuiz(null);

		// Assert
	}

	@Test(expected = AssertionFailedException.class)
	public void testGetRelativPathQuiz_SchluesselNull() {
		// Arrange
		MCArchivraetsel raetsel = new MCArchivraetsel();
		// Act
		QuizPathProvider.getGetRelativPathQuiz(raetsel);

		// Assert
	}

	@Test(expected = AssertionFailedException.class)
	public void testGetRelativPathQuiz_SchluesselLeer() {
		// Arrange
		MCArchivraetsel raetsel = new MCArchivraetsel();
		raetsel.setSchluessel("");
		// Act
		QuizPathProvider.getGetRelativPathQuiz(raetsel);

		// Assert
	}

	@Test
	public void testGetRelativPathQuizitem_Hapy_Hour() {
		// Arrange
		MCArchivraetselItem item = JUnitUtils.prepareCompleteRaetselitemForTest();

		String expected = "01";

		// Act
		String path = QuizPathProvider.getRelativPathQuizitem(item);

		// Assert
		assertEquals(expected, path);
	}

	@Test(expected = AssertionFailedException.class)
	public void testGetRelativPathQuizitem_ItemNull() {
		// Arrange
		// Act
		QuizPathProvider.getRelativPathQuizitem(null);

		// Assert
	}

	@Test(expected = AssertionFailedException.class)
	public void testGetRelativPathQuizitem_ItemNummerNull() {
		// Arrange
		MCArchivraetselItem item = new MCArchivraetselItem();
		item.setNummer(null);
		// Act
		QuizPathProvider.getRelativPathQuizitem(item);

		// Assert
	}

	@Test(expected = AssertionFailedException.class)
	public void testGetRelativPathQuizitem_ItemNummerLeer() {
		// Arrange
		MCArchivraetselItem item = new MCArchivraetselItem();
		item.setNummer("");
		// Act
		QuizPathProvider.getRelativPathQuizitem(item);

		// Assert
	}

	@Test
	public void testGetPathReadyMarkerHtlatexPrepare_Happy_Hour() {
		// Arrange
		MCArchivraetsel raetsel = new MCArchivraetsel();
		raetsel.setSchluessel("9997");
		String expected = PATH_WORKDIR + "\\9997_pre.txt";

		// Act
		String path = QuizPathProvider.getPathReadyMarkerHtlatexPrepare(raetsel, PATH_WORKDIR);

		// Assert
		assertEquals(expected, path);
	}

	@Test
	public void testGetPathReadyMarkerPostHtlatex_Happy_Hour() {
		// Arrange
		MCArchivraetsel raetsel = new MCArchivraetsel();
		raetsel.setSchluessel("9997");
		String expected = PATH_WORKDIR + "\\9997_post.txt";

		// Act
		String path = QuizPathProvider.getPathReadyMarkerPostHtlatex(raetsel, PATH_WORKDIR);

		// Assert
		assertEquals(expected, path);
	}

	@Test
	public void testGetPathQuizXml_Happy_Hour() {
		// Arrange
		MCArchivraetsel raetsel = new MCArchivraetsel();
		raetsel.setSchluessel("9997");
		String expected = PATH_WORKDIR + "\\9997\\quiz.xml";

		// Act
		String path = QuizPathProvider.getPathQuizXml(raetsel, PATH_WORKDIR);

		// Assert
		assertEquals(expected, path);
	}

}
