/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.service.stammdaten.quiz;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import junitx.framework.FileAssert;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import de.egladil.mathejungalt.config.IMatheJungAltConfiguration.RaetsellisteGeneratorModus;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.service.stammdaten.JUnitUtils;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.IFilegenerator;

/**
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class HtlatexAufgabePostProcessorTest {

	private static final String RECOURCE_NAME = "/postprocessHtml.html";

	private static final String PATH_EXPECTED_JS = "C:\\work\\_develop\\_sandbox\\matheagRCPJUnit\\expectedJS.html";

	private static final String PATH_EXPECTED_ANDROID = "C:\\work\\_develop\\_sandbox\\matheagRCPJUnit\\expectedANDROID.html";

	@Test
	public void testProcessHtml_Happy_Hour_JS() throws IOException {
		// Arrange
		InputStream in = getClass().getResourceAsStream(RECOURCE_NAME);
		HtlatexAufgabePostProcessor processor = new HtlatexAufgabePostProcessor();
		MCArchivraetselItem item = JUnitUtils.prepareCompleteRaetselitemForTest();
		RaetsellisteGeneratorModus modus = RaetsellisteGeneratorModus.JAVASCRIPT;
		File expectedFile = new File(PATH_EXPECTED_JS);

		// Act
		String processedLines = "";
		try {
			processedLines = processor.process(in, item, modus);
		} finally {
			IOUtils.closeQuietly(in);
		}

		// Assert
		File actualFile = File.createTempFile("actual", ".html");
		actualFile.deleteOnExit();

		System.out.println("actualFilePath=" + actualFile.getAbsolutePath());

		FileUtils.writeStringToFile(actualFile, processedLines, IFilegenerator.ENCODING_UTF);
		FileAssert.assertEquals(expectedFile, actualFile);
	}

	@Test
	public void testProcessHtml_Happy_Hour_ANDROID() throws IOException {
		// Arrange
		InputStream in = getClass().getResourceAsStream(RECOURCE_NAME);
		HtlatexAufgabePostProcessor processor = new HtlatexAufgabePostProcessor();
		MCArchivraetselItem item = JUnitUtils.prepareCompleteRaetselitemForTest();
		RaetsellisteGeneratorModus modus = RaetsellisteGeneratorModus.ANDROID;
		File expectedFile = new File(PATH_EXPECTED_ANDROID);

		// Act
		String processedLines = "";
		try {
			processedLines = processor.process(in, item, modus);
		} finally {
			IOUtils.closeQuietly(in);
		}

		// Assert
		File actualFile = File.createTempFile("actual", ".html");
		actualFile.deleteOnExit();

		System.out.println("actualFilePath=" + actualFile.getAbsolutePath());

		FileUtils.writeStringToFile(actualFile, processedLines, IFilegenerator.ENCODING_UTF);
		FileAssert.assertEquals(expectedFile, actualFile);

	}
}
