/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.html;

import java.io.File;

import javax.xml.bind.JAXBContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.service.stammdaten.impl.AbstractStammdatenserviceTest;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.MatheAGGeneratorUtils;

/**
 * @author aheike
 */
public class QuizJsGeneratorTest extends AbstractStammdatenserviceTest {

	private static final Logger LOG = LoggerFactory.getLogger(QuizJsGeneratorTest.class);

	private static final String PATH_WORKDIR = "C:/work/_develop/_sandbox/web/mjaquiz/alle/test";

	private JAXBContext jaxbContext;

	/** */
	private QuizJsGenerator generator;

	/*
	 * (non-Javadoc)
	 * 
	 * @see junit.framework.TestCase#setUp()
	 */
	@Override
	protected void setUp() throws Exception {
		jaxbContext = JAXBContext.newInstance(MCArchivraetsel.class, MCArchivraetselItem.class);
	}

	/**
	 * @throws Exception
	 *
	 */
	public void testGenerate() throws Exception {
		MCArchivraetsel raetsel = prepareRaetsel();
		generator = new QuizJsGenerator(raetsel);
		File dir = new File(PATH_WORKDIR);
		final File file = new File(dir + File.separator + generator.getFilename());
		file.delete();
		LOG.info("Ausgabe nach '" + dir.getAbsolutePath() + "'");
		MatheAGGeneratorUtils.writeOutput(dir.getAbsolutePath(), generator, getStammdatenservice());
		assertTrue(file.isFile());
	}

	private MCArchivraetsel prepareRaetsel() throws Exception {
		return (MCArchivraetsel) jaxbContext.createUnmarshaller().unmarshal(
			getClass().getResource("/mcarchivraetsel.xml"));
	}
}
