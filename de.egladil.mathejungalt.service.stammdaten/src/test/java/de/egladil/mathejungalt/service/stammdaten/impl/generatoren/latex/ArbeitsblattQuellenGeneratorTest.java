/**
 * 
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex;

import java.io.File;

import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.service.stammdaten.impl.AbstractStammdatenserviceTest;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.MatheAGGeneratorUtils;

/**
 * @author aheike
 */
public class ArbeitsblattQuellenGeneratorTest extends AbstractStammdatenserviceTest {

	private ArbeitsblattQuellenGenerator arbeitsblattQuellenGenerator;

	/** */
	private Arbeitsblatt arbeitsblatt;

	/**
	 * 
	 */
	public ArbeitsblattQuellenGeneratorTest() {

	}

	public void testGenerateQuellen() {
		arbeitsblatt = createArbeitsblatt();
		arbeitsblattQuellenGenerator.setArbeitsblatt(arbeitsblatt);
		File dir = new File("C:/_develop/temp");
		File file = new File(dir + File.separator + "ein_tolles_arbeitsblatt_quellen.tex");
		file.delete();
		System.out.println("Ausgabe nach '" + dir.getAbsolutePath() + "'");
		MatheAGGeneratorUtils.writeOutput(dir.getAbsolutePath(), arbeitsblattQuellenGenerator, getStammdatenservice());
		assertTrue(file.isFile());
	}

	/**
	 * @param pArbeitsblattQuellenGenerator the arbeitsblattQuellenGenerator to set
	 */
	public void setArbeitsblattQuellenGenerator(ArbeitsblattQuellenGenerator pArbeitsblattQuellenGenerator) {
		arbeitsblattQuellenGenerator = pArbeitsblattQuellenGenerator;
	}

}
