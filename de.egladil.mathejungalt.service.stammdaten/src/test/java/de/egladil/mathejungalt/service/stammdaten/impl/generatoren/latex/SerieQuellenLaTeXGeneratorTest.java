/**
 * 
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex;

import java.io.File;

import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.service.stammdaten.impl.AbstractStammdatenserviceTest;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.MatheAGGeneratorUtils;

/**
 * @author aheike
 */
public class SerieQuellenLaTeXGeneratorTest extends AbstractStammdatenserviceTest {

	/** */
	private SerieQuellenLaTeXGenerator serieQuellenLaTeXGenerator;

	/**
	 * 
	 */
	public SerieQuellenLaTeXGeneratorTest() {
		//
	}

	/**
	 * 
	 */
	public void testGenerate() {
		Serie serie = createSerie();
		serieQuellenLaTeXGenerator.setSerie(serie);
		File dir = new File("C:/_develop/temp");
		final File file = new File(dir + File.separator + "mja_99_5000_quellen.tex");
		file.delete();
		System.out.println("Ausgabe nach '" + dir.getAbsolutePath() + "'");
		MatheAGGeneratorUtils.writeOutput(dir.getAbsolutePath(), serieQuellenLaTeXGenerator, getStammdatenservice());
		assertTrue(file.isFile());
	}

	/**
	 * @param pSerieQuellenLaTeXGenerator the serieQuellenLaTeXGenerator to set
	 */
	public void setSerieQuellenLaTeXGenerator(SerieQuellenLaTeXGenerator pSerieQuellenLaTeXGenerator) {
		serieQuellenLaTeXGenerator = pSerieQuellenLaTeXGenerator;
	}

}
