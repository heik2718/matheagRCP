/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.service.stammdaten.quiz;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;

import junitx.framework.FileAssert;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.AssertionFailedException;
import org.junit.Test;

import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.service.stammdaten.JUnitUtils;

/**
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class QuizaufgabeLatexGeneratorTest {

	private static final String PATH_WORKDIR = "C:\\work\\knobelarchiv_2\\latex\\tempdir_junit";

	@Test
	public void testGenerateLatex_Happy_Hour() {
		// Arrange
		MCArchivraetselItem item = JUnitUtils.prepareCompleteRaetselitemForTest();
		QuizaufgabeLatexGenerator generator = new QuizaufgabeLatexGenerator();
		File expectedFile = new File(PATH_WORKDIR + "\\01.tex");
		FileUtils.deleteQuietly(expectedFile);
		// Act
		String result = generator.generate(item, PATH_WORKDIR);
		// Assert
		assertNotNull(result);
		File generatedFile = new File(result);
		assertTrue(generatedFile.isFile());
		assertTrue(generatedFile.canRead());
		assertTrue(generatedFile.length() > 0l);
		FileAssert.assertEquals(expectedFile, generatedFile);
		System.out.println("QuizaufgabeLatexGeneratorTest.testGenerateLatex_Happy_Hour(): "
			+ generatedFile.getAbsolutePath() + " size=" + generatedFile.length());
	}

	@Test(expected = AssertionFailedException.class)
	public void testGenerateLatex_raetselitemNull() {
		// Arrange
		MCArchivraetselItem item = null;
		QuizaufgabeLatexGenerator generator = new QuizaufgabeLatexGenerator();
		File expectedFile = new File(PATH_WORKDIR + "\\01.tex");
		FileUtils.deleteQuietly(expectedFile);
		// Act
		generator.generate(item, PATH_WORKDIR);
		// Assert
	}
}
