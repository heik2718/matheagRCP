/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl;

import java.io.File;

import junit.framework.TestCase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.service.stammdaten.JUnitUtils;

/**
 * @author heike
 */
public class QuizaufgabenserviceTest extends TestCase {

	private static final Logger LOG = LoggerFactory.getLogger(QuizaufgabenserviceTest.class);

	private static final String PATH_WORKDIR = "/home/heike/work/knobelarchiv_2/latex/tempdir_junit";

	/*
	 * (non-Javadoc)
	 *
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testCreateLaTeXAufgabeMitTabelle() {
		MCAufgabe aufgabe = JUnitUtils.prepareAufgabe();
		aufgabe.setTabelleGenerieren(true);
		String aufgabeLaTeXFileName = "prev_" + aufgabe.getSchluessel() + ".tex";

		File aufgabeFile = new File(PATH_WORKDIR + "/" + aufgabeLaTeXFileName);
		aufgabeFile.delete();

		assertTrue("Testsetting: " + aufgabeFile.getAbsolutePath() + " existiert schon", !aufgabeFile.canRead());

		LOG.info("Datei wird generiert nach '" + PATH_WORKDIR + "'");
		Quizaufgabenservice service = new Quizaufgabenservice();
		service.createLaTeX(aufgabe, PATH_WORKDIR, AbstractStammdatenservice.AUFGABE_LATEX_GENERATOR_XSL, true);

		assertTrue("Result: " + aufgabeFile.getAbsolutePath() + " existiert nicht", aufgabeFile.canRead());
	}

	public void testCreateLaTeXAufgabeOhneTabelle() {
		MCAufgabe aufgabe = JUnitUtils.prepareAufgabe();
		aufgabe.setTabelleGenerieren(false);
		String aufgabeLaTeXFileName = "prev_" + aufgabe.getSchluessel() + ".tex";

		File aufgabeFile = new File(PATH_WORKDIR + "/" + aufgabeLaTeXFileName);

		aufgabeFile.delete();

		assertTrue("Testsetting: " + aufgabeFile.getAbsolutePath() + " existiert schon", !aufgabeFile.canRead());

		LOG.info("Datei wird generiert nach '" + PATH_WORKDIR + "'");
		Quizaufgabenservice service = new Quizaufgabenservice();
		service.createLaTeX(aufgabe, PATH_WORKDIR, AbstractStammdatenservice.AUFGABE_LATEX_GENERATOR_XSL, true);

		assertTrue("Result: " + aufgabeFile.getAbsolutePath() + " existiert nicht", aufgabeFile.canRead());
	}

	public void testCreatePlainLaTeXAufgabe() {
		MCAufgabe aufgabe = JUnitUtils.prepareAufgabe();
		aufgabe.setTabelleGenerieren(false);
		String aufgabeLaTeXFileName = "prev_" + aufgabe.getSchluessel() + ".tex";

		File aufgabeFile = new File(PATH_WORKDIR + "/" + aufgabeLaTeXFileName);

		aufgabeFile.delete();

		assertTrue("Testsetting: " + aufgabeFile.getAbsolutePath() + " existiert schon", !aufgabeFile.canRead());

		LOG.info("Datei wird generiert nach '" + PATH_WORKDIR + "'");
		Quizaufgabenservice service = new Quizaufgabenservice();
		service.createLaTeX(aufgabe, PATH_WORKDIR, AbstractStammdatenservice.PLAIN_AUFGABE_LATEX_GENERATOR_XSL, true);

		assertTrue("Result: " + aufgabeFile.getAbsolutePath() + " existiert nicht", aufgabeFile.canRead());
	}

	public void testCreateLaTeXLoesung() {
		MCAufgabe aufgabe = JUnitUtils.prepareAufgabe();
		aufgabe.setTabelleGenerieren(true);
		String loesungLaTeXFileName = "prev_" + aufgabe.getSchluessel() + "_l.tex";

		File loesungFile = new File(PATH_WORKDIR + "/" + loesungLaTeXFileName);

		loesungFile.delete();

		assertTrue("Testsetting: " + loesungFile.getAbsolutePath() + " existiert schon", !loesungFile.canRead());

		LOG.info("Datei wird generiert nach '" + PATH_WORKDIR + "'");
		Quizaufgabenservice service = new Quizaufgabenservice();
		service.createLaTeX(aufgabe, PATH_WORKDIR, AbstractStammdatenservice.AUFGABE_LATEX_GENERATOR_XSL, false);

		assertTrue("Result: " + loesungFile.getAbsolutePath() + " existiert nicht", loesungFile.canRead());
	}
}
