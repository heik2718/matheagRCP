/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl.generatoren.latex;

import java.io.File;

import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.service.stammdaten.impl.AbstractStammdatenserviceTest;
import de.egladil.mathejungalt.service.stammdaten.impl.generatoren.MatheAGGeneratorUtils;

/**
 * @author aheike
 */
public class SerieStufeLaTeXGeneratorTest extends AbstractStammdatenserviceTest {

	/** */
	private SerieStufeLaTeXGenerator serieStufeLaTeXGenerator;

	/**
	 *
	 */
	public SerieStufeLaTeXGeneratorTest() {

	}

	/**
	 *
	 */
	public void testGenerate() {
		Serie serie = createSerie();
		serieStufeLaTeXGenerator.setSerie(serie);
		serieStufeLaTeXGenerator.setStufen(new int[] { 1 });
		serieStufeLaTeXGenerator.setFontsize("\\Large\n");
		serieStufeLaTeXGenerator.setAufgabenabstand("\\newpage");
		File dir = new File("C:/_develop/temp");
		File file = new File(dir + File.separator + "mja_99_5000_1.tex");
		file.delete();
		System.out.println("Ausgabe nach '" + dir.getAbsolutePath() + "'");
		MatheAGGeneratorUtils.writeOutput(dir.getAbsolutePath(), serieStufeLaTeXGenerator, getStammdatenservice());
		assertTrue(file.isFile());

		serieStufeLaTeXGenerator.setStufen(new int[] { 2 });
		file = new File(dir + File.separator + "mja_99_5000_2.tex");
		file.delete();
		System.out.println("Ausgabe nach '" + dir.getAbsolutePath() + "'");
		MatheAGGeneratorUtils.writeOutput(dir.getAbsolutePath(), serieStufeLaTeXGenerator, getStammdatenservice());
		assertTrue(file.isFile());

		serieStufeLaTeXGenerator.setStufen(new int[] { 3, 4, 5, 6 });
		file = new File(dir + File.separator + "mja_99_5000_aufgaben_einzeln.tex");
		file.delete();
		System.out.println("Ausgabe nach '" + dir.getAbsolutePath() + "'");
		MatheAGGeneratorUtils.writeOutput(dir.getAbsolutePath(), serieStufeLaTeXGenerator, getStammdatenservice());
		assertTrue(file.isFile());

		serieStufeLaTeXGenerator.setAufgabenabstand("\\vertbox{1cm}");
		serieStufeLaTeXGenerator.setFontsize("\n");
		serieStufeLaTeXGenerator.setStufen(new int[] { 1, 2, 3, 4, 5, 6 });
		file = new File(dir + File.separator + "mja_99_5000_aufgaben.tex");
		file.delete();
		System.out.println("Ausgabe nach '" + dir.getAbsolutePath() + "'");
		MatheAGGeneratorUtils.writeOutput(dir.getAbsolutePath(), serieStufeLaTeXGenerator, getStammdatenservice());
		assertTrue(file.isFile());

		serieStufeLaTeXGenerator.setAusgabeLoesungen(true);
		file = new File(dir + File.separator + "mja_99_5000_loesungen.tex");
		file.delete();
		System.out.println("Ausgabe nach '" + dir.getAbsolutePath() + "'");
		MatheAGGeneratorUtils.writeOutput(dir.getAbsolutePath(), serieStufeLaTeXGenerator, getStammdatenservice());
		assertTrue(file.isFile());
	}

	/**
	 * @param pSerieStufeLaTeXGenerator the serieStufeLaTeXGenerator to set
	 */
	public void setSerieStufeLaTeXGenerator(SerieStufeLaTeXGenerator pSerieStufeLaTeXGenerator) {
		serieStufeLaTeXGenerator = pSerieStufeLaTeXGenerator;
	}
}
