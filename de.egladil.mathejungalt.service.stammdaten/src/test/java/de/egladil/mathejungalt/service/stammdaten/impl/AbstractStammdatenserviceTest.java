/**
 *
 */
package de.egladil.mathejungalt.service.stammdaten.impl;

import java.util.Date;

import junit.framework.TestCase;
import de.egladil.mathejungalt.config.internal.MatheJungAltConfiguration;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.persistence.api.IPersistenceservice;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.mock.PersistenceServiceMock;

/**
 * @author aheike
 */
public abstract class AbstractStammdatenserviceTest extends TestCase {

	/** */
	private IStammdatenservice stammdatenservice;

	/**
	 *
	 */
	public AbstractStammdatenserviceTest() {
	}

	/**
	 * @return
	 */
	protected Serie createSerie() {
		Serie serie = new Serie();
		serie.setDatum(new Date());
		serie.setMonatJahrText("AUGUST 2050");
		serie.setNummer(5000);
		serie.setRunde(99);
		stammdatenservice.aufgabensammlungitemsLaden(serie);
		return serie;
	}

	/**
	 * @return
	 */
	protected Minikaenguru createMinikaenguru() {
		Minikaenguru minikaenguru = new Minikaenguru();
		minikaenguru.setJahr(1999);
		stammdatenservice.aufgabensammlungitemsLaden(minikaenguru);
		return minikaenguru;
	}

	protected Arbeitsblatt createArbeitsblatt() {
		Arbeitsblatt arbeitsblatt = new Arbeitsblatt();
		arbeitsblatt.setTitel("Ein tolles Arbeitsblatt");
		stammdatenservice.aufgabensammlungitemsLaden(arbeitsblatt);
		return arbeitsblatt;
	}

	/**
	 * @return the stammdatenservice
	 */
	protected final IStammdatenservice getStammdatenservice() {
		if (stammdatenservice == null) {
			stammdatenservice = new StammdatenserviceImpl();
			IPersistenceservice persistenceService = new PersistenceServiceMock();
			((StammdatenserviceImpl) stammdatenservice).setPersistenceservice(persistenceService);
			((StammdatenserviceImpl) stammdatenservice).setConfiguration(new MatheJungAltConfiguration());

		}
		return stammdatenservice;
	}

}
