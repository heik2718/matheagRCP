/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.persistence.db.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.mysql.IJdbcTemplateWrapper;
import de.egladil.mathejungalt.domain.schulen.Land;
import de.egladil.mathejungalt.domain.schulen.MinikaenguruTeilnahme;
import de.egladil.mathejungalt.domain.schulen.Schule;
import de.egladil.mathejungalt.persistence.api.IMinikaenguruRepository;
import de.egladil.mathejungalt.persistence.api.ISchulenRepository;
import de.egladil.mathejungalt.persistence.db.dao.mappers.LandRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.LongRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.MiniTeilnahmenRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.SchluesselRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.SchuleRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.sql.ISchulenStmts;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMinikaenguruteilnahmenTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.ISchulenTableNames;
import de.egladil.mathejungalt.persistence.db.dao.util.SqlQueryUtil;

/**
 * <p>
 * NOTE: THIS WIDGET AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class SchulenDao extends AbstractDao implements ISchulenRepository {

	private static final Logger LOG = LoggerFactory.getLogger(SchulenDao.class);

	private IMinikaenguruRepository minikaenguruRepository;

	/** */
	private Map<Long, Schule> schulenMap;

	private Map<String, Land> laenderMap;

	/**
	 * @param pJdbcTemplateWrapper
	 */
	public SchulenDao(IJdbcTemplateWrapper pJdbcTemplateWrapper, IdGenerator idGenerator) {
		super(pJdbcTemplateWrapper, idGenerator);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.ISchulenRepository#findAllSchulen()
	 */
	@Override
	public List<Schule> findAllSchulen() {
		if (schulenMap == null) {
			loadSchulen();
		}
		List<Schule> result = new ArrayList<Schule>();
		for (Schule s : schulenMap.values()) {
			result.add(s);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.ISchulenRepository#findAllLaender()
	 */
	@Override
	public List<Land> findAllLaender() {
		if (laenderMap == null) {
			loadLaender();
		}
		List<Land> result = new ArrayList<Land>();
		for (Land l : laenderMap.values()) {
			result.add(l);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.ISchulenRepository#saveSchule(de.egladil.mathejungalt.domain.schulen.
	 * Schule)
	 */
	@Override
	public void saveSchule(Schule pSchule) {
		if (pSchule == null) {
			throw new MatheJungAltException("null an saveSchule() uebergeben");
		}
		if (pSchule.isNew()) {
			persistSchule(pSchule);
		} else {
			mergeSchule(pSchule);
		}

	}

	private void persistSchule(final Schule pSchule) {
		if (schulenMap == null) {
			loadSchulen();
		}
		final Long schuleId = getIdGenerator().nextId(ISchulenTableNames.TABLE);
		getTransactionTemplate().execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(TransactionStatus pArg0) {
				getJdbcTemplate().update(
					ISchulenStmts.INSERT_STMT,
					new Object[] { pSchule.getSchluessel(), pSchule.getName(), pSchule.getAnschrift(),
						pSchule.getTelefonnummer(), pSchule.getLand().getSchluessel(), schuleId });

				for (MinikaenguruTeilnahme miniTeilnahme : pSchule.getMinikaenguruTeilnahmen()) {
					persistMinikaenguruTeilnahme(miniTeilnahme);
				}
				pSchule.setId(schuleId);
				schulenMap.put(schuleId, pSchule);
				getIdGenerator().increase(ISchulenTableNames.TABLE);
			}

		});

	}

	private void mergeSchule(final Schule pSchule) {
		final Map<MinikaenguruTeilnahme, SqlQueryUtil.ACTION> teilnahmeActions = initSerienitemActions(pSchule);
		getTransactionTemplate().execute(new TransactionCallbackWithoutResult() {
			@Override
			protected void doInTransactionWithoutResult(TransactionStatus pArg0) {
				getJdbcTemplate().update(
					ISchulenStmts.UPDATE_STMT,
					new Object[] { pSchule.getSchluessel(), pSchule.getName(), pSchule.getAnschrift(),
						pSchule.getTelefonnummer(), pSchule.getLand().getSchluessel(), pSchule.getId() });

				for (MinikaenguruTeilnahme teilnahme : teilnahmeActions.keySet()) {
					switch (teilnahmeActions.get(teilnahme)) {
					case DELETE:
						removeMinikaenguruTeilnahme(teilnahme);
						break;
					case INSERT:
						persistMinikaenguruTeilnahme(teilnahme);
						break;
					default:
						mergeMinikaenguruTeilnahme(teilnahme);
						break;
					}
				}
			}
		});

	}

	/**
	 * @param pMiniTeilnahme
	 */
	private void persistMinikaenguruTeilnahme(MinikaenguruTeilnahme pMiniTeilnahme) {
		Long id = getIdGenerator().nextId(IMinikaenguruteilnahmenTableNames.TABLE);
		int rueckmeldung = pMiniTeilnahme.isRueckmeldung() ? 1 : 0;
		getJdbcTemplate().update(
			ISchulenStmts.INSERT_MINI_TEILNAHME,
			new Object[] { pMiniTeilnahme.getSchule().getId(), pMiniTeilnahme.getMinikaenguru().getId(), rueckmeldung,
				id });
		pMiniTeilnahme.setId(id);
		getIdGenerator().increase(IMinikaenguruteilnahmenTableNames.TABLE);
	}

	/**
	 * @param pMiniTeilnahme
	 */
	private void removeMinikaenguruTeilnahme(MinikaenguruTeilnahme pMiniTeilnahme) {
		// FIXME
		// getJdbcTemplate().update(ISchulenStmts.DELETE_KONTAKTE_FROM_MAILINGLISTE, new Object[]{});
		getJdbcTemplate().update(ISchulenStmts.DELETE_MINI_TEILNAHME, new Object[] { pMiniTeilnahme.getId() });
	}

	/**
	 * @param pMiniTeilnahme
	 */
	private void mergeMinikaenguruTeilnahme(MinikaenguruTeilnahme pMiniTeilnahme) {
		int rueckmeldung = pMiniTeilnahme.isRueckmeldung() ? 1 : 0;
		getJdbcTemplate().update(ISchulenStmts.UPDATE_MINI_TEILNAHME,
			new Object[] { rueckmeldung, pMiniTeilnahme.getId() });
	}

	/**
	 * @param pTeilnahme
	 * @return
	 */
	private Map<MinikaenguruTeilnahme, SqlQueryUtil.ACTION> initSerienitemActions(Schule pSchule) {
		Map<MinikaenguruTeilnahme, SqlQueryUtil.ACTION> map = new HashMap<MinikaenguruTeilnahme, SqlQueryUtil.ACTION>();
		List<Long> idList = getJdbcTemplate().query(ISchulenStmts.SELECT_MINI_TEILNAHMEN_ID_ZU_SCHULE,
			new LongRowMapper(), new Object[] { pSchule.getId() });
		List<Long> teilnahmeItmemsIDs = new ArrayList<Long>();
		// INSERTs und UPDATEs
		for (MinikaenguruTeilnahme teilnahme : pSchule.getMinikaenguruTeilnahmen()) {
			teilnahmeItmemsIDs.add(teilnahme.getId());
			if (teilnahme.isNew()) {
				map.put(teilnahme, SqlQueryUtil.ACTION.INSERT);
			} else {
				if (idList.contains(teilnahme.getId())) {
					map.put(teilnahme, SqlQueryUtil.ACTION.UPDATE);
				}
			}
		}
		// Deletes sind die, die nicht mehr in der SerieItemsIDs-Liste sind
		for (Long dbID : idList) {
			if (!teilnahmeItmemsIDs.contains(dbID)) {
				MinikaenguruTeilnahme tempItem = new MinikaenguruTeilnahme();
				tempItem.setId(dbID);
				map.put(tempItem, SqlQueryUtil.ACTION.DELETE);
			}
		}
		return map;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.ISchulenRepository#findSchuleById(java.lang.Long)
	 */
	@Override
	public Schule findSchuleById(Long pId) {
		if (schulenMap == null) {
			loadSchulen();
		}
		return schulenMap.get(pId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.ISchulenRepository#loadLaender()
	 */
	@Override
	public List<Land> loadLaender() {
		List<Land> laender = getJdbcTemplate().query(ISchulenStmts.SELECT_LAENDER_STMT, new LandRowMapper());
		laenderMap = new HashMap<String, Land>();
		for (Land land : laender) {
			laenderMap.put(land.getSchluessel(), land);
		}
		return laender;
	}

	/**
   *
   */
	private void loadSchulen() {
		if (laenderMap == null || laenderMap.isEmpty()) {
			loadLaender();
		}

		List<Schule> liste = getJdbcTemplate().query(ISchulenStmts.SELECT_STMT, new SchuleRowMapper(laenderMap));
		schulenMap = new HashMap<Long, Schule>();
		for (Schule schule : liste) {
			schulenMap.put(schule.getId(), schule);
		}
		LOG.info(liste.size() + " Schule(n) geladen");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.ISchulenRepository#loadMinikaenguruteilnahmen(de.egladil.mathejungalt
	 * .domain.schulen .Schule)
	 */
	@Override
	public void loadMinikaenguruteilnahmen(Schule pSchule) {
		if (pSchule == null || pSchule.getId() == null) {
			throw new MatheJungAltException(SerienDao.class.getName()
				+ ".loadMinikaenguruteilnahmen(): Parameter ist null oder Parameter.ID ist null");
		}
		if (pSchule.isMiniTeilnahmenLoaded()) {
			return;
		}
		if (schulenMap == null) {
			loadSchulen();
		}
		if (!schulenMap.containsValue(pSchule)) {
			throw new MatheJungAltException(SchulenDao.class.getName()
				+ ".loadMinikaenguruteilnahmen(): schwerwiegender Programmfehler: es gibt die Schule " + pSchule
				+ " nicht im Repository!");
		}
		LOG.info("Lade Minikaenguruteilnahmen zu Schule [" + pSchule.getName() + "]");
		MiniTeilnahmenRowMapper rowMapper = new MiniTeilnahmenRowMapper(minikaenguruRepository, pSchule);
		List<MinikaenguruTeilnahme> teilnahmen = getJdbcTemplate().query(
			ISchulenStmts.SELECT_MINI_TEILNAHMEN_ZU_SCHULE, rowMapper, new Object[] { pSchule.getId() });
		pSchule.setMiniTeilnahmenLoaded(true);
		if (LOG.isDebugEnabled()) {
			LOG.debug(teilnahmen.size() + " Teilnahme(n) zur Schule geladen");
		}
	}

	/**
	 * @param pMinikaenguruRepository the minikaenguruRepository to set
	 */
	public final void setMinikaenguruRepository(IMinikaenguruRepository pMinikaenguruRepository) {
		minikaenguruRepository = pMinikaenguruRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.ISchulenRepository#getMaxKey()
	 */
	@Override
	public String getMaxKey() {
		return getJdbcTemplate().queryForObject(ISchulenStmts.SELECT_MAX_KEY_STMT, new SchluesselRowMapper());
	}
}
