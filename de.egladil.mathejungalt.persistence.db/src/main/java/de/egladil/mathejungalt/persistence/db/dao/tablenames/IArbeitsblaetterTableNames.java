/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.tablenames;

/**
 * @author Winkelv
 */
public interface IArbeitsblaetterTableNames extends IColumnNamesMitSchluessel {

	/** */
	public static final String TABLE = "arbeitsblaetter";

	/** */
	public static final String COL_TITEL = "titel";

	/** */
	public static final String COL_BEENDET = "beendet";

	/** */
	public static final String COL_SPERREND = "sperrend";

}
