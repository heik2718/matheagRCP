/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.tablenames;

/**
 * @author Winkelv
 */
public interface ISerienTableNames extends ICommonColumnNamesOhneSchluessel {

	/** */
	public static final String TABLE = "serien";

	/** */
	public static final String COL_NUMMER = "nummer";

	/** */
	public static final String COL_RUNDE = "runde";

	/** */
	public static final String COL_DATUM = "datum";

	/** */
	public static final String COL_BEENDET = "beendet";

	/** */
	public static final String COL_MONAT_JAHR = "monat_jahr_text";
}
