/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.tablenames;

/**
 * @author Winkelv
 */
public interface IArbeitsblattitemsTableNames extends ICommonColumnNamesOhneSchluessel {

	/** */
	public static final String TABLE = "arbeitsblattitems";

	/** */
	public static final String COL_NUMMER = "nummer";

	/** */
	public static final String COL_ARBEITSBLATT_ID = "arbblatt_id";

	/** */
	public static final String COL_AUFGABE_ID = "aufgabe_id";
}
