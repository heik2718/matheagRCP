/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.mysql.IJdbcTemplateWrapper;
import de.egladil.mathejungalt.domain.mitglieder.Diplom;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.domain.mitglieder.Serienteilnahme;
import de.egladil.mathejungalt.persistence.api.IMitgliederRepository;
import de.egladil.mathejungalt.persistence.api.ISerienRepository;
import de.egladil.mathejungalt.persistence.db.dao.mappers.DiplomRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.LongRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.MitgliedRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.SchluesselRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.SerienteilnahmeRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.sql.IMitgliederStmts;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IDiplomeTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMitgliederTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.ISerienitemsTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.ISerienteilnahmenTableNames;
import de.egladil.mathejungalt.persistence.db.dao.typehandler.DateTypeHandler;
import de.egladil.mathejungalt.persistence.db.dao.util.SqlQueryUtil;

/**
 * @author winkelv
 */
public class MitgliederDao extends AbstractDao implements IMitgliederRepository {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(MitgliederDao.class);

	/** */
	private ISerienRepository serienRepository;

	/** */
	private Map<Long, Mitglied> mitgliederMap;

	/**
	 * @param pJdbcTemplateWrapper
	 */
	public MitgliederDao(final IJdbcTemplateWrapper pJdbcTemplateWrapper, final IdGenerator idGenerator) {
		super(pJdbcTemplateWrapper, idGenerator);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathe.core.persistenceapi.mitglieder.IMitgliederRepository#findAll()
	 */
	@Override
	public List<Mitglied> findAll() {
		if (mitgliederMap == null) {
			loadMitglieder();
		}
		final List<Mitglied> liste = new ArrayList<Mitglied>();
		final Iterator<Long> iter = mitgliederMap.keySet().iterator();
		while (iter.hasNext()) {
			liste.add(mitgliederMap.get(iter.next()));
		}
		return liste;
	}

	/**
	 *
	 */
	private void loadMitglieder() {
		final List<Mitglied> liste = getJdbcTemplate().query(IMitgliederStmts.SELECT_STMT, new MitgliedRowMapper());
		mitgliederMap = new HashMap<Long, Mitglied>();
		for (final Mitglied mitglied : liste) {
			mitgliederMap.put(mitglied.getId(), mitglied);
		}
		LOG.info(liste.size() + " " + IMitgliederTableNames.TABLE + " geladen");

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathe.core.persistenceapi.mitglieder.IMitgliederRepository#findByAttributeMap(java.util.Map,
	 * boolean, boolean)
	 */
	@Override
	public List<Mitglied> findByAttributeMap(final Map<String, Object> pAttributeMap, final boolean pExact, final boolean pAnd) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathe.core.persistenceapi.mitglieder.IMitgliederRepository#findAllWithFruehstarterInRunde(int)
	 */
	@Override
	public List<Mitglied> findAllWithFruehstarterInRunde(final int pRunde) {
		if (mitgliederMap == null)
			loadMitglieder();
		LOG.info(IMitgliederStmts.SELECT_MITGLIED_ID_ZU_FRUEHSTARTER_STMT);
		final List<Long> idListe = getJdbcTemplate().query(IMitgliederStmts.SELECT_MITGLIED_ID_ZU_FRUEHSTARTER_STMT,
			new LongRowMapper(), new Object[] { pRunde, 0 });
		final List<Mitglied> mitglieder = new ArrayList<Mitglied>();
		for (final Long id : idListe) {
			final Mitglied mitgl = mitgliederMap.get(id);
			if (!mitglieder.contains(mitgl))
				mitglieder.add(mitgl);
		}
		LOG.info(mitglieder.size() + " Mitglieder mit Fr�hstarterpunkten in Runde " + pRunde + " geladen");
		return mitglieder;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathe.core.persistenceapi.mitglieder.IMitgliederRepository#findById(long)
	 */
	@Override
	public Mitglied findById(final long pId) {
		if (mitgliederMap == null)
			loadMitglieder();
		if (!mitgliederMap.containsKey(pId))
			throw new MatheJungAltException("Es gibt kein Mitglied mit der Id " + pId);
		return mitgliederMap.get(pId);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathe.core.persistenceapi.mitglieder.IMitgliederRepository#getMaxKey()
	 */
	@Override
	public String getMaxKey() {
		return getJdbcTemplate().queryForObject(IMitgliederStmts.SELECT_MAX_KEY_STMT, new SchluesselRowMapper());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathe.core.persistenceapi.mitglieder.IMitgliederRepository#loadDiplome(de.egladil.mathe.core.domain
	 * .mitglieder .Mitglied)
	 */
	@Override
	public void loadDiplome(final Mitglied pMitglied) {
		if (pMitglied == null)
			throw new MatheJungAltException(MitgliederDao.class.getName()
				+ ".loadDiplome(): Parameter ist null oder Parameter.ID ist null");
		if (pMitglied.isDiplomeLoaded()) {
			return;
		}
		if (mitgliederMap == null) {
			loadMitglieder();
		}

		if (!mitgliederMap.containsValue(pMitglied)) {
			throw new MatheJungAltException(MitgliederDao.class.getName()
				+ ".loadDiplome(): schwerwiegender Programmfehler: es gibt das Mitlied" + pMitglied
				+ " nicht im Repository!");
		}

		pMitglied.initDiplome();
		final DiplomRowMapper rowMapper = new DiplomRowMapper(pMitglied);
		final List<Diplom> liste = getJdbcTemplate().query(IMitgliederStmts.SELECT_DIPLOME_VON_MITGLIED, rowMapper,
			new Object[] { pMitglied.getId() });
		pMitglied.setDiplomeLoaded(true);
		if (LOG.isDebugEnabled()) {
			LOG.debug(liste.size() + " " + IDiplomeTableNames.TABLE + " geladen");
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @seede.egladil.mathe.core.persistenceapi.mitglieder.IMitgliederRepository#loadSerienteilnahmen(de.egladil.mathe.core
	 * .domain. mitglieder.Mitglied)
	 */
	@Override
	public void loadSerienteilnahmen(final Mitglied pMitglied) {
		if (pMitglied == null)
			throw new MatheJungAltException(MitgliederDao.class.getName()
				+ ".loadSerienteilnahmen(): Parameter ist null.");
		if (pMitglied.isSerienteilnahmenLoaded())
			return;
		if (mitgliederMap == null)
			loadMitglieder();

		if (!mitgliederMap.containsValue(pMitglied))
			throw new MatheJungAltException(MitgliederDao.class.getName()
				+ ".loadSerienteilnahmen(): schwerwiegender Programmfehler: es gibt das Mitlied" + pMitglied
				+ " nicht im Repository!");

		pMitglied.initSerienteilnahmen();
		final SerienteilnahmeRowMapper rowMapper = new SerienteilnahmeRowMapper(serienRepository, pMitglied);
		final List<Serienteilnahme> itemList = getJdbcTemplate().query(IMitgliederStmts.SELECT_TEILNAHMEN_VON_MITGLIED_STMT,
			rowMapper, new Object[] { pMitglied.getId() });
		pMitglied.setSerienteilnahmenLoaded(true);
		if (LOG.isDebugEnabled())
			LOG.debug(itemList.size() + " " + ISerienitemsTableNames.TABLE + " geladen");
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathe.core.persistenceapi.mitglieder.IMitgliederRepository#removeSerienteilnahme(de.egladil.mathe.
	 * core.domain. mitglieder.Serienteilnahme)
	 */
	@Override
	public void removeSerienteilnahme(final Serienteilnahme pTeilnahme) {
		if (pTeilnahme == null || pTeilnahme.isNew())
			return;
		removeSerienteilnahme(pTeilnahme.getId());
	}

	/**
	 * Löscht Serienteilnahme mit gegebener ID
	 *
	 * @param pId
	 */
	private void removeSerienteilnahme(final Long pId) {
		getJdbcTemplate().update(IMitgliederStmts.DELETE_SERIENTEILNAHME_STMT, new Object[] { pId });
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathe.core.persistenceapi.mitglieder.IMitgliederRepository#saveDiplom(de.egladil.mathe.core.domain
	 * .mitglieder. Diplom)
	 */
	@Override
	public void saveDiplom(final Diplom pDiplom) {
		if (pDiplom == null)
			throw new MatheJungAltException("null an saveDiplom() uebergeben");
		if (pDiplom.isNew())
			persistDiplom(pDiplom);
		else
			mergeDiplom(pDiplom);

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathe.core.persistenceapi.mitglieder.IMitgliederRepository#saveMitglied(de.egladil.mathe.core.domain
	 * .mitglieder .Mitglied)
	 */
	@Override
	public void saveMitglied(final Mitglied pMitglied) {
		if (pMitglied == null)
			throw new MatheJungAltException("null an saveMitglied() übergeben");
		if (pMitglied.getId() == null) {
			persist(pMitglied);
		} else {
			merge(pMitglied);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IMitgliederRepository#changeMitgliedActivation(de.egladil.mathejungalt
	 * .domain.mitglieder .Mitglied)
	 */
	@Override
	public void changeMitgliedActivation(final Mitglied pMitglied) {
		Assert.isNotNull(pMitglied);
		final Long mitgliedId = pMitglied.getId();
		Assert.isNotNull(mitgliedId, "Das Mitglied ist noch nicht persistent!");
		final int aktivNeu = pMitglied.isAktiv() ? 0 : 1;
		getJdbcTemplate().update(IMitgliederStmts.CHANGE_MITGLIED_ACTIVATION, new Object[] { aktivNeu, mitgliedId });
	}

	/**
	 * update
	 *
	 * @param pMitglied
	 */
	private void merge(final Mitglied pMitglied) {
		final Map<Serienteilnahme, SqlQueryUtil.ACTION> serienteilnahmeActions = initSerienteilnahmeActions(pMitglied);
		final Map<Diplom, SqlQueryUtil.ACTION> diplomeActions = initDiplomeActions(pMitglied);
		getTransactionTemplate().execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(final TransactionStatus pArg0) {
				getJdbcTemplate().update(
					IMitgliederStmts.UPDATE_MITGLIED_STMT,
					new Object[] { pMitglied.getSchluessel(), pMitglied.getVorname(), pMitglied.getNachname(),
						pMitglied.getKlasse(), pMitglied.getAlter(), pMitglied.getErfinderpunkte(),
						pMitglied.getGeburtsmonat(), pMitglied.getGeburtsjahr(), pMitglied.getAktiv(), pMitglied.getKontakt(),
						pMitglied.getFototitel(), pMitglied.getId() });

				final Iterator<Serienteilnahme> teilnIter = serienteilnahmeActions.keySet().iterator();
				while (teilnIter.hasNext()) {
					final Serienteilnahme teiln = teilnIter.next();
					switch (serienteilnahmeActions.get(teiln)) {
					case DELETE:
						removeSerienteilnahme(teiln);
						break;
					case INSERT:
						persistSerienteilnahme(teiln);
					default:
						mergeSerienteilnahme(teiln);
						break;
					}
				}

				final Iterator<Diplom> diplomIter = diplomeActions.keySet().iterator();
				while (diplomIter.hasNext()) {
					final Diplom diplom = diplomIter.next();
					switch (diplomeActions.get(diplom)) {
					case DELETE:
						removeDiplom(diplom);
						break;
					case INSERT:
						persistDiplom(diplom);
					default:
						mergeDiplom(diplom);
						break;
					}
				}
			}
		});

	}

	/**
	 * @param pMitglied
	 * @return
	 */
	private Map<Serienteilnahme, SqlQueryUtil.ACTION> initSerienteilnahmeActions(final Mitglied pMitglied) {
		final Map<Serienteilnahme, SqlQueryUtil.ACTION> map = new HashMap<Serienteilnahme, SqlQueryUtil.ACTION>();
		final List<Long> idList = getJdbcTemplate().query(IMitgliederStmts.SELECT_SERIENTEILNAHMEN_ID_ZU_MITGLIED_STMT,
			new LongRowMapper(), new Object[] { pMitglied.getId() });
		final List<Long> mitgliedTeilnahmeIDs = new ArrayList<Long>();
		// INSERTs und UPDATEs
		for (final Serienteilnahme teilnahme : pMitglied.getSerienteilnahmen()) {
			mitgliedTeilnahmeIDs.add(teilnahme.getId());
			if (teilnahme.isNew())
				map.put(teilnahme, SqlQueryUtil.ACTION.INSERT);
			else {
				if (idList.contains(teilnahme.getId()))
					map.put(teilnahme, SqlQueryUtil.ACTION.UPDATE);

			}
		}
		// Deletes sind die, die nicht mehr in der MitgliedSerienteilnahmeID-Liste sind
		for (final Long dbID : idList) {
			if (!mitgliedTeilnahmeIDs.contains(dbID)) {
				final Serienteilnahme tempTeilnahme = new Serienteilnahme();
				tempTeilnahme.setId(dbID);
				map.put(tempTeilnahme, SqlQueryUtil.ACTION.DELETE);
			}
		}
		return map;
	}

	/**
	 * @param pMitglied
	 * @return
	 */
	private Map<Diplom, SqlQueryUtil.ACTION> initDiplomeActions(final Mitglied pMitglied) {
		final Map<Diplom, SqlQueryUtil.ACTION> map = new HashMap<Diplom, SqlQueryUtil.ACTION>();
		final List<Long> idList = getJdbcTemplate().query(IMitgliederStmts.SELECT_DIPLOME_ID_ZU_MITGLIED_STMT,
			new LongRowMapper(), new Object[] { pMitglied.getId() });
		final List<Long> mitgliedDiplomIDs = new ArrayList<Long>();
		for (final Diplom diplom : pMitglied.getDiplome()) {
			mitgliedDiplomIDs.add(diplom.getId());
			if (diplom.isNew())
				map.put(diplom, SqlQueryUtil.ACTION.INSERT);
			else {
				if (idList.contains(diplom.getId())) {
					map.put(diplom, SqlQueryUtil.ACTION.UPDATE);
				}
			}
		}
		for (final Long dbID : idList) {
			if (!mitgliedDiplomIDs.contains(dbID)) {
				final Diplom tempDiplom = new Diplom();
				tempDiplom.setId(dbID);
				map.put(tempDiplom, SqlQueryUtil.ACTION.DELETE);
			}
		}
		return map;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathe.core.persistenceapi.mitglieder.IMitgliederRepository#removeDiplom(de.egladil.mathe.core.domain
	 * .mitglieder .Diplom)
	 */
	@Override
	public void removeDiplom(final Diplom pDiplom) {
		if (pDiplom == null || pDiplom.isNew())
			return;
		removeDiplom(pDiplom.getId());
	}

	private final void removeDiplom(final Long pId) {
		getJdbcTemplate().update(IMitgliederStmts.DELETE_DIPLOM_STMT, new Object[] { pId });
	}

	/**
	 * Insert
	 *
	 * @param pMitglied
	 */
	private void persist(final Mitglied pMitglied) {
		// persist ist einfach: alles muss neu angelegt werden
		final Long mitgliedId = getIdGenerator().nextId(IMitgliederTableNames.TABLE);
		getTransactionTemplate().execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(final TransactionStatus pArg0) {
				getJdbcTemplate().update(
					IMitgliederStmts.INSERT_MITGLIED_STMT,
					new Object[] { pMitglied.getSchluessel(), pMitglied.getVorname(), pMitglied.getNachname(),
						pMitglied.getKlasse(), pMitglied.getAlter(), pMitglied.getErfinderpunkte(),
						pMitglied.getGeburtsmonat(), pMitglied.getGeburtsjahr(), pMitglied.getKontakt(),
						pMitglied.getFototitel(), mitgliedId });
				pMitglied.setId(mitgliedId);

				for (final Serienteilnahme serienteilnahme : pMitglied.getSerienteilnahmen()) {
					persistSerienteilnahme(serienteilnahme);
				}

				for (final Diplom diplom : pMitglied.getDiplome()) {
					persistDiplom(diplom);
				}

				mitgliederMap.put(mitgliedId, pMitglied);
				getIdGenerator().increase(IMitgliederTableNames.TABLE);
			}
		});
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathe.core.persistenceapi.mitglieder.IMitgliederRepository#saveSerienteilnahme(de.egladil.mathe.core
	 * .domain.mitglieder .Serienteilnahme)
	 */
	@Override
	public void saveSerienteilnahme(final Serienteilnahme pSerienteilnahme) {
		if (pSerienteilnahme == null)
			throw new MatheJungAltException("null an saveSerienteilnahme() uebergeben");
		if (pSerienteilnahme.isNew())
			persistSerienteilnahme(pSerienteilnahme);
		else
			mergeSerienteilnahme(pSerienteilnahme);
	}

	/**
	 * @param pSerienteilnahme
	 */
	private void persistSerienteilnahme(final Serienteilnahme pSerienteilnahme) {
		final Long id = getIdGenerator().nextId(ISerienteilnahmenTableNames.TABLE);
		getJdbcTemplate().update(
			IMitgliederStmts.INSERT_SERIENTEILNAHME_STMT,
			new Object[] { pSerienteilnahme.getFruehstarter(), pSerienteilnahme.getMitglied().getId(),
				pSerienteilnahme.getPunkte(), pSerienteilnahme.getSerie().getId(), id });
		pSerienteilnahme.setId(id);
		getIdGenerator().increase(ISerienteilnahmenTableNames.TABLE);
	}

	/**
	 * @param pSerienteilnahme
	 */
	private void mergeSerienteilnahme(final Serienteilnahme pSerienteilnahme) {
		getJdbcTemplate().update(
			IMitgliederStmts.UPDATE_SERIENTEILNAHME_STMT,
			new Object[] { pSerienteilnahme.getFruehstarter(), pSerienteilnahme.getMitglied().getId(),
				pSerienteilnahme.getPunkte(), pSerienteilnahme.getSerie().getId(), pSerienteilnahme.getId() });
	}

	/**
	 * @param pDiplom
	 */
	private void persistDiplom(final Diplom pDiplom) {
		final Long id = getIdGenerator().nextId(IDiplomeTableNames.TABLE);
		getJdbcTemplate().update(
			IMitgliederStmts.INSERT_DIPLOM_STMT,
			new Object[] { pDiplom.getAnzahlErfinderpunkte(), new DateTypeHandler().getSqlValue(pDiplom.getDatum()),
				pDiplom.getMitglied().getId(), id });
		pDiplom.setId(id);
		getIdGenerator().increase(IDiplomeTableNames.TABLE);
	}

	/**
	 * @param pDiplom
	 */
	private void mergeDiplom(final Diplom pDiplom) {
		getJdbcTemplate().update(
			IMitgliederStmts.UPDATE_DIPLOM_STMT,
			new Object[] { pDiplom.getAnzahlErfinderpunkte(), new DateTypeHandler().getSqlValue(pDiplom.getDatum()),
				pDiplom.getMitglied().getId(), pDiplom.getId() });

	}

	/**
	 * @param pSerienRepository the serienRepository to set
	 */
	@Override
	public void setSerienRepository(final ISerienRepository pSerienRepository) {
		serienRepository = pSerienRepository;
	}

	/**
	 * @param pMitglied
	 */
	public void removeMitglied(final Mitglied pMitglied) {
		if (pMitglied == null || pMitglied.isNew() || !pMitglied.isDiplomeLoaded()
			|| !pMitglied.isSerienteilnahmenLoaded())
			throw new MatheJungAltException(
				"removeDiplome: Mitglied oder Mitglied-ID sind null oder Mitglied ist nicht komplett geladen");
		// wegen kaskadierenden Löschens wprde es eigentlich reichen, das Mitglied zu l�schen. Aber die DB ist manchmal
		// nicht
		// korrekt
		// konfiguriert, so dass wir sicherheitshalber zuerst die Teilnahmen und Diplome l�schen
		getTransactionTemplate().execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(final TransactionStatus pArg0) {

				for (final Serienteilnahme teilnahme : pMitglied.getSerienteilnahmen()) {
					removeSerienteilnahme(teilnahme);
				}

				for (final Diplom diplom : pMitglied.getDiplome()) {
					removeDiplom(diplom);
				}

				getJdbcTemplate().update(IMitgliederStmts.DELETE_MITGLIED_STMT, new Object[] { pMitglied.getId() });

				mitgliederMap.remove(pMitglied.getId());
			}
		});
	}
}
