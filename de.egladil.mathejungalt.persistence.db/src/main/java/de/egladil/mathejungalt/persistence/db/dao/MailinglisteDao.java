/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.mysql.IJdbcTemplateWrapper;
import de.egladil.mathejungalt.domain.mitglieder.EmailAdresse;
import de.egladil.mathejungalt.persistence.api.IMailinglisteRepository;
import de.egladil.mathejungalt.persistence.db.dao.mappers.MailinglisteRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.sql.IMailingListeStmts;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMailinglisteTableNames;

/**
 * @author winkelv
 */
public class MailinglisteDao extends AbstractDao implements IMailinglisteRepository {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(MailinglisteDao.class);

	/** */
	private Map<Long, EmailAdresse> emailAdressenMap;

	/**
	 * @param pJdbcTemplateWrapper
	 */
	public MailinglisteDao(IJdbcTemplateWrapper pJdbcTemplateWrapper, IdGenerator idGenerator) {
		super(pJdbcTemplateWrapper, idGenerator);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.mitglieder.IMailinglisteRepository#findAll()
	 */
	@Override
	public List<EmailAdresse> findAll() {
		if (emailAdressenMap == null)
			loadEmailAdressen();
		List<EmailAdresse> liste = new ArrayList<EmailAdresse>();
		Iterator<Long> iter = emailAdressenMap.keySet().iterator();
		while (iter.hasNext())
			liste.add(emailAdressenMap.get(iter.next()));
		return liste;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.mitglieder.IMailinglisteRepository#findByAdresse(java.lang.String)
	 */
	@Override
	public EmailAdresse findByAdresse(String pAdresse) {
		List<EmailAdresse> alle = findAll();
		for (EmailAdresse email : alle) {
			if (email.getEmail().equalsIgnoreCase(pAdresse)) {
				return email;
			}
		}
		return null;
	}

	/**
	 * 
	 */
	private void loadEmailAdressen() {
		emailAdressenMap = new HashMap<Long, EmailAdresse>();
		List<EmailAdresse> liste = getJdbcTemplate().query(IMailingListeStmts.SELECT_STMT, new MailinglisteRowMapper());
		for (EmailAdresse mail : liste)
			emailAdressenMap.put(mail.getId(), mail);
		LOG.info(liste.size() + " " + IMailinglisteTableNames.TABLE + " geladen");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.core.persistenceapi.mitglieder.IMailinglisteRepository#remove(de.egladil.mathe.core.domain.
	 * mitglieder. EmailAdresse)
	 */
	@Override
	public void remove(EmailAdresse pAdresse) {
		if (pAdresse == null || pAdresse.isNew())
			return;
		getJdbcTemplate().update(IMailingListeStmts.DELETE_STMT, new Object[] { pAdresse.getId() });
		if (emailAdressenMap != null)
			emailAdressenMap.remove(pAdresse.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IMailinglisteRepository#remove(java.lang.String)
	 */
	@Override
	public void remove(String pAdresse) {
		EmailAdresse adresse = findByAdresse(pAdresse);
		if (adresse != null) {
			remove(adresse);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.persistenceapi.mitglieder.IMailinglisteRepository#save(de.egladil.mathe.core.domain.mitglieder
	 * .EmailAdresse )
	 */
	@Override
	public void save(EmailAdresse pAdresse) {
		if (pAdresse == null)
			throw new MatheJungAltException("null an save(EmailAdresse) uebergeben");
		if (emailAdressenMap == null)
			loadEmailAdressen();
		if (pAdresse.isNew()) {
			persist(pAdresse);
		} else {
			merge(pAdresse);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IMailinglisteRepository#save(java.lang.String)
	 */
	@Override
	public void save(String pAdresse) {
		EmailAdresse adresse = new EmailAdresse();
		adresse.setEmail(pAdresse);
		save(adresse);
	}

	/**
	 * @param pAdresse
	 */
	private void persist(EmailAdresse pAdresse) {
		Long id = getIdGenerator().nextId(IMailinglisteTableNames.TABLE);
		Object[] args = new Object[] { pAdresse.getEmail(), id };
		getJdbcTemplate().update(IMailingListeStmts.INSERT_STMT, args);
		pAdresse.setId(id);
		emailAdressenMap.put(id, pAdresse);
		getIdGenerator().increase(IMailinglisteTableNames.TABLE);
	}

	/**
	 * Update
	 * 
	 * @param pAdresse
	 */
	private void merge(EmailAdresse pAdresse) {
		Object[] args = new Object[] { pAdresse.getEmail(), pAdresse };
		getJdbcTemplate().update(IMailingListeStmts.UPDATE_STMT, args);
	}
}
