/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.tablenames;

/**
 * @author heike
 *
 */
public interface IMcRaetselTableNames extends IColumnNamesMitSchluessel {

	/** */
	String TABLE = "MC_RAETSEL";

	/** */
	String COL_TITEL = "titel";

	/** */
	String COL_LICENSE_FULL = "license_full";

	/** */
	String COL_LICENSE_SHORT = "license_short";

	/** */
	String COL_AUTOR_ID = "autor_id";

	/** */
	String COL_STUFE = "stufe";

	/** */
	String COL_PUBLISHED = "published";

	/** */
	String COL_PRIVAT = "privat";

}
