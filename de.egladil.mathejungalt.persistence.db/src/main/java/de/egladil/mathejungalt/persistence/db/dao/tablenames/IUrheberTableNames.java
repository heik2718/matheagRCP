/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.tablenames;

/**
 * @author Winkelv
 */
public interface IUrheberTableNames {

	/** */
	String TABLE = "mc_raetselautoren";

	String COL_ID = "id";

	/** */
	String COL_NAME = "name";

	/** */
	String COL_URL = "url";
}
