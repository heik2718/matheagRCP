/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao.tablenames;

/**
 * @author Winkelv
 */
public interface ICommonColumnNamesOhneSchluessel {

	public final static String COL_ID = "id";
}
