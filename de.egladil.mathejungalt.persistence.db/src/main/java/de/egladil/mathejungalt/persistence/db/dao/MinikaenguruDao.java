/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.mysql.IJdbcTemplateWrapper;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguruitem;
import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.persistence.api.IAufgabenRepository;
import de.egladil.mathejungalt.persistence.api.IMinikaenguruRepository;
import de.egladil.mathejungalt.persistence.db.dao.mappers.KontaktRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.LongRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.MinikaenguruRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.MinikaenguruitemRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.sql.IMinikaenguruStmts;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMinikaenguruTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMinikaenguruitemsTableNames;
import de.egladil.mathejungalt.persistence.db.dao.util.SqlQueryUtil;

/**
 * @author Winkelv
 */
public class MinikaenguruDao extends AbstractDao implements IMinikaenguruRepository {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(MinikaenguruDao.class);

	/** */
	private IAufgabenRepository aufgabenRepository;

	/** */
	private Map<Long, Minikaenguru> minikaenguruMap;

	/**
	 * @param pJdbcTemplateWrapper
	 */
	public MinikaenguruDao(IJdbcTemplateWrapper pJdbcTemplateWrapper, IdGenerator idGenerator) {
		super(pJdbcTemplateWrapper, idGenerator);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.aufgabensammlungen.IMinikaenguruRepository#findAllWettbewerbe()
	 */
	@Override
	public List<Minikaenguru> findAllWettbewerbe() {
		if (minikaenguruMap == null)
			loadWettbewerbe();
		List<Minikaenguru> minikaengurus = new ArrayList<Minikaenguru>();
		Iterator<Long> iter = minikaenguruMap.keySet().iterator();
		while (iter.hasNext())
			minikaengurus.add(minikaenguruMap.get(iter.next()));
		return minikaengurus;
	}

	/**
	 * 
	 */
	private void loadWettbewerbe() {
		List<Minikaenguru> liste = getJdbcTemplate().query(IMinikaenguruStmts.SELECT_STMT, new MinikaenguruRowMapper());
		minikaenguruMap = new HashMap<Long, Minikaenguru>();
		for (Minikaenguru minikaenguru : liste) {
			minikaenguruMap.put(minikaenguru.getId(), minikaenguru);
		}
		LOG.info(liste.size() + " " + IMinikaenguruTableNames.TABLE + " geladen");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.aufgabensammlungen.IMinikaenguruRepository#findById(java.lang.Long)
	 */
	@Override
	public Minikaenguru findById(Long pId) {
		if (minikaenguruMap == null)
			loadWettbewerbe();
		if (!minikaenguruMap.containsKey(pId))
			throw new MatheJungAltException("Es gibt keinen Minikänguruwettbewerb mit Id " + pId);
		return minikaenguruMap.get(pId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.persistenceapi.aufgabensammlungen.IMinikaenguruRepository#findByAufgabe(de.egladil.mathe
	 * .core.domain .aufgaben.Aufgabe)
	 */
	@Override
	public Minikaenguruitem findByAufgabe(Aufgabe pAufgabe) {
		if (pAufgabe == null || pAufgabe.isNew())
			throw new MatheJungAltException("Die Aufgabe wurde noch nicht gespeichert.");
		if (minikaenguruMap == null)
			loadWettbewerbe();
		List<Long> miniIds = getJdbcTemplate().query(IMinikaenguruStmts.SELECT_MINI_ID_ZU_AUFGABE, new LongRowMapper(),
			new Object[] { pAufgabe.getId() });
		if (miniIds.size() == 0)
			return null;
		if (miniIds.size() > 1)
			throw new MatheJungAltException("Inkonsistente Daten: die Aufgabe mit id" + pAufgabe.getId()
				+ " ist mehr als einem Minikänguruwettbewerb zugeornet");
		Minikaenguru mini = minikaenguruMap.get(miniIds.get(0));
		for (Minikaenguruitem item : mini.getMinikaenguruitems())
			if (pAufgabe.equals(item.getAufgabe()))
				return item;
		throw new MatheJungAltException("Programmfehler: die Aufgabe mit id" + pAufgabe.getId()
			+ " ist einem Minikänguruwettbewerb zugeornet. Dieser kann aber nicht gefunden werden");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.core.persistenceapi.aufgabensammlungen.IMinikaenguruRepository#loadItems(de.egladil.mathe.core
	 * .domain. aufgabensammlungen.minikaenguru.Minikaenguru)
	 */
	@Override
	public void loadItems(Minikaenguru pMinikaenguru) {
		if (pMinikaenguru == null || pMinikaenguru.getId() == null)
			throw new MatheJungAltException(MinikaenguruDao.class.getName()
				+ ".loadItems(): Parameter ist null oder Parameter.ID ist null");
		if (pMinikaenguru.isItemsLoaded())
			return;
		if (minikaenguruMap == null)
			loadWettbewerbe();

		if (!minikaenguruMap.containsValue(pMinikaenguru))
			throw new MatheJungAltException(MinikaenguruDao.class.getName()
				+ ".loadItems(): schwerwiegender Programmfehler: es gibt den Minikaenguruwettbewerb " + pMinikaenguru
				+ " nicht im Repository!");

		pMinikaenguru.initItems();
		MinikaenguruitemRowMapper rowMapper = new MinikaenguruitemRowMapper(aufgabenRepository, pMinikaenguru);
		List<Minikaenguruitem> itemList = getJdbcTemplate().query(IMinikaenguruStmts.SELECT_ITEMS_ZU_MINIKAENGURU_STMT,
			rowMapper, new Object[] { pMinikaenguru.getId() });
		pMinikaenguru.setItemsLoaded(true);
		LOG.info(itemList.size() + " " + IMinikaenguruTableNames.TABLE + " geladen");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.core.persistenceapi.aufgabensammlungen.IMinikaenguruRepository#save(de.egladil.mathe.core.domain
	 * . aufgabensammlungen.minikaenguru.Minikaenguruitem)
	 */
	@Override
	public void saveItem(Minikaenguruitem pItem) {
		if (pItem == null)
			throw new MatheJungAltException("null an save(Minikaenguruitem) uebergaben");
		if (pItem.isNew())
			persistMinikaenguruitem(pItem);
		else
			mergeMinikaenguruitem(pItem);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.aufgabensammlungen.IMinikaenguruRepository#getMaxKey()
	 */
	@Override
	public Integer getMaxKey() {
		return getJdbcTemplate().queryForInt(IMinikaenguruStmts.SELECT_MAX_KEY_STMT);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.core.persistenceapi.aufgabensammlungen.IMinikaenguruRepository#save(de.egladil.mathe.core.domain
	 * . aufgabensammlungen.minikaenguru.Minikaenguru)
	 */
	@Override
	public void save(Minikaenguru pMinikaenguru) {
		if (pMinikaenguru == null)
			throw new MatheJungAltException("null an save(Minikaenguru) übergeben");
		if (pMinikaenguru.isNew())
			persistMinikaenguru(pMinikaenguru);
		else
			mergeMinikaenguru(pMinikaenguru);

	}

	/**
	 * @param pMinikaenguru
	 */
	private void mergeMinikaenguru(final Minikaenguru pMinikaenguru) {
		final Map<Minikaenguruitem, SqlQueryUtil.ACTION> minikaenguruitemsActions = initMinikaenguruitemActions(pMinikaenguru);
		getTransactionTemplate().execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(TransactionStatus pArg0) {
				getJdbcTemplate().update(IMinikaenguruStmts.UPDATE_MINIKAENGURU_STMT,
					new Object[] { pMinikaenguru.getJahr(), pMinikaenguru.getBeendet(), pMinikaenguru.getId() });

				Iterator<Minikaenguruitem> itemIter = minikaenguruitemsActions.keySet().iterator();
				while (itemIter.hasNext()) {
					Minikaenguruitem item = itemIter.next();
					switch (minikaenguruitemsActions.get(item)) {
					case DELETE:
						removeMinikaenguruitem(item);
						break;
					case INSERT:
						persistMinikaenguruitem(item);
					default:
						mergeMinikaenguruitem(item);
						break;
					}
				}
			}
		});

	}

	/**
	 * @param pMinikaenguru
	 * @return
	 */
	private Map<Minikaenguruitem, SqlQueryUtil.ACTION> initMinikaenguruitemActions(Minikaenguru pMinikaenguru) {
		Map<Minikaenguruitem, SqlQueryUtil.ACTION> map = new HashMap<Minikaenguruitem, SqlQueryUtil.ACTION>();
		List<Long> idList = getJdbcTemplate().query(IMinikaenguruStmts.SELECT_MINIKAENGURUITEM_ID_ZU_MINIKAENGURU_STMT,
			new LongRowMapper(), new Object[] { pMinikaenguru.getId() });
		List<Long> minikaenguruItmemsIDs = new ArrayList<Long>();
		// INSERTs und UPDATEs
		for (Minikaenguruitem item : pMinikaenguru.getMinikaenguruitems()) {
			minikaenguruItmemsIDs.add(item.getId());
			if (item.isNew())
				map.put(item, SqlQueryUtil.ACTION.INSERT);
			else {
				if (idList.contains(item.getId()))
					map.put(item, SqlQueryUtil.ACTION.UPDATE);

			}
		}
		// Deletes sind die, die nicht mehr in der minikaenguruItmemsIDs-Liste sind
		for (Long dbID : idList) {
			if (!minikaenguruItmemsIDs.contains(dbID)) {
				Minikaenguruitem tempItem = new Minikaenguruitem();
				tempItem.setId(dbID);
				map.put(tempItem, SqlQueryUtil.ACTION.DELETE);
			}
		}
		return map;
	}

	/**
	 * @param pMinikaenguru
	 */
	private void persistMinikaenguru(final Minikaenguru pMinikaenguru) {
		if (minikaenguruMap == null)
			loadWettbewerbe();
		final Long neueId = getIdGenerator().nextId(IMinikaenguruTableNames.TABLE);
		getTransactionTemplate().execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(TransactionStatus pArg0) {
				getJdbcTemplate().update(IMinikaenguruStmts.INSERT_MINIKAENGURU_STMT,
					new Object[] { pMinikaenguru.getJahr(), pMinikaenguru.getBeendet(), neueId });
				pMinikaenguru.setId(neueId);

				for (Minikaenguruitem item : pMinikaenguru.getMinikaenguruitems()) {
					persistMinikaenguruitem(item);
				}

				minikaenguruMap.put(neueId, pMinikaenguru);
				getIdGenerator().increase(IMinikaenguruTableNames.TABLE);
			}
		});

	}

	/**
	 * Neues Minikaenguruitem speichern
	 * 
	 * @param pItem
	 */
	private void persistMinikaenguruitem(final Minikaenguruitem pItem) {
		Long id = getIdGenerator().nextId(IMinikaenguruitemsTableNames.TABLE);
		final Object[] args = new Object[] { pItem.getMinikaenguru().getId(), pItem.getAufgabe().getId(),
			pItem.getNummer(), id };
		getJdbcTemplate().update(IMinikaenguruStmts.INSERT_MINIKAENGURUITEM_STMT, args);
		pItem.setId(id);
		getIdGenerator().increase(IMinikaenguruitemsTableNames.TABLE);
	}

	/**
	 * �nderung speichern
	 * 
	 * @param pItem
	 */
	private void mergeMinikaenguruitem(final Minikaenguruitem pItem) {
		getJdbcTemplate().update(IMinikaenguruStmts.UPDATE_MINIKAENGURUITEM_STMT,
			new Object[] { pItem.getNummer(), pItem.getId() });
	}

	/**
	 * @param pMinikaenguru
	 */
	public void removeMinikaenguru(final Minikaenguru pMinikaenguru) {
		if (pMinikaenguru == null || pMinikaenguru.isNew())
			return;
		// wegen kaskadierenden Löschens würde es eigentlich reichen, pMinikaenguru zu l�schen. Aber die DB ist manchmal
		// nicht
		// korrekt
		// konfiguriert, so dass wir sicherheitshalber zuerst die Items l�schen
		getTransactionTemplate().execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(TransactionStatus pArg0) {

				for (Minikaenguruitem item : pMinikaenguru.getMinikaenguruitems()) {
					removeMinikaenguruitem(item);
				}

				getJdbcTemplate().update(IMinikaenguruStmts.DELETE_SMINIKAENGURU_STMT,
					new Object[] { pMinikaenguru.getId() });

				minikaenguruMap.remove(pMinikaenguru.getId());
			}
		});
	}

	private void removeMinikaenguruitem(Minikaenguruitem pMinikaenguruitem) {
		if (pMinikaenguruitem == null || pMinikaenguruitem.isNew())
			return;
		getJdbcTemplate().update(IMinikaenguruStmts.DELETE_MINIKAENGURUITEM_STMT,
			new Object[] { pMinikaenguruitem.getId() });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.persistenceapi.aufgabensammlungen.IMinikaenguruRepository#setAufgabenRepository(de.egladil
	 * .mathe.core .persistenceapi.aufgaben.IAufgabenRepository)
	 */
	@Override
	public void setAufgabenRepository(IAufgabenRepository pAufgabenRepository) {
		aufgabenRepository = pAufgabenRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IMinikaenguruRepository#mailinglisteAktualisieren(de.egladil.mathejungalt
	 * .domain. aufgabensammlungen.minikaenguru.Minikaenguru, java.util.List, java.util.List)
	 */
	@Override
	public void mailinglisteAktualisieren(final Minikaenguru pMinikaenguru, final List<Kontakt> pLoeschliste,
		final List<Kontakt> pZugangsliste) {
		Assert.isNotNull(pMinikaenguru, "pMinikaenguru");
		Assert.isNotNull(pLoeschliste, "pLoeschliste");
		Assert.isNotNull(pZugangsliste, "pZugangsliste");
		getTransactionTemplate().execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(TransactionStatus pArg0) {
				for (Kontakt lk : pLoeschliste) {
					deleteKonakt(lk, pMinikaenguru);
				}
				for (Kontakt zk : pZugangsliste) {
					insertKontakt(zk, pMinikaenguru);
				}
			}
		});
		if (LOG.isDebugEnabled()) {
			LOG.debug(pLoeschliste.size() + " Kontakt(e) aus Mailingliste entfernt, " + pZugangsliste.size()
				+ " Kontakt(e) zu Mailingliste hinzugefuegt");
		}
	}

	private void deleteKonakt(Kontakt kontakt, Minikaenguru minikaenguru) {
		getJdbcTemplate().update(IMinikaenguruStmts.DELETE_KONTAKT_MAILINGLISTE,
			new Object[] { minikaenguru.getId(), kontakt.getId() });
	}

	private void insertKontakt(Kontakt kontakt, Minikaenguru minikaenguru) {
		getJdbcTemplate().update(IMinikaenguruStmts.INSERT_KONTAKT_MAILINGLISTE,
			new Object[] { minikaenguru.getId(), kontakt.getId() });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IMinikaenguruRepository#loadMailingliste(de.egladil.mathejungalt.domain
	 * .aufgabensammlungen .minikaenguru.Minikaenguru)
	 */
	@Override
	public List<Kontakt> loadMailingliste(Minikaenguru pMinikaenguru) {
		Assert.isNotNull(pMinikaenguru);
		return getJdbcTemplate().query(IMinikaenguruStmts.SELECT_MAILINGLISTE_STMT, new KontaktRowMapper(),
			new Object[] { pMinikaenguru.getId() });
	}

}
