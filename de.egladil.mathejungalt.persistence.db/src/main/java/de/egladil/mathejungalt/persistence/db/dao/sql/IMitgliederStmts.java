/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.sql;

import de.egladil.mathejungalt.persistence.db.dao.tablenames.IDiplomeTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMitgliederTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.ISerienTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.ISerienteilnahmenTableNames;

/**
 * @author aheike
 */
public interface IMitgliederStmts {

	/** */
	public static final String SELECT_MAX_KEY_STMT = "select max(" + IMitgliederTableNames.COL_SCHLUESSEL + ") as "
		+ IMitgliederTableNames.COL_SCHLUESSEL + " from " + IMitgliederTableNames.TABLE;

	/** */
	public static final String SELECT_STMT = "select " + IMitgliederTableNames.COL_ID + ", " + IMitgliederTableNames.COL_SCHLUESSEL
		+ ", " + IMitgliederTableNames.COL_VORNAME + ", " + IMitgliederTableNames.COL_NACHNAME + ", "
		+ IMitgliederTableNames.COL_KLASSE + ", " + IMitgliederTableNames.COL_JAHRE + ", "
		+ IMitgliederTableNames.COL_ERFINDERPUNKTE + ", " + IMitgliederTableNames.COL_GEBURTSMONAT + ", "
		+ IMitgliederTableNames.COL_GEBURTSJAHR + ", " + IMitgliederTableNames.COL_KONTAKT + ", "
		+ IMitgliederTableNames.COL_FOTOTITEL + ", " + IMitgliederTableNames.COL_AKTIV + " from " + IMitgliederTableNames.TABLE;

	/** */
	public static final String SELECT_TEILNAHMEN_VON_MITGLIED_STMT = "select " + ISerienteilnahmenTableNames.COL_ID + ", "
		+ ISerienteilnahmenTableNames.COL_SERIE_ID + ", " + ISerienteilnahmenTableNames.COL_PUNKTE + ", "
		+ ISerienteilnahmenTableNames.COL_FRUEHSTARTER + " from " + ISerienteilnahmenTableNames.TABLE + " where "
		+ ISerienteilnahmenTableNames.COL_MITGLIED_ID + " = ?";

	public static final String SELECT_DIPLOME_VON_MITGLIED = "select " + IDiplomeTableNames.COL_ID + ", "
		+ IDiplomeTableNames.COL_DATUM + ", " + IDiplomeTableNames.COL_ERFINDERPUNKTE + " from " + IDiplomeTableNames.TABLE
		+ " where " + IDiplomeTableNames.COL_MITGLIED_ID + " = ?";

	/** */
	public static final String SELECT_SERIENTEILNAHMEN_ID_ZU_MITGLIED_STMT = "select " + ISerienteilnahmenTableNames.COL_ID
		+ " from " + ISerienteilnahmenTableNames.TABLE + " where " + ISerienteilnahmenTableNames.COL_MITGLIED_ID + " = ?";

	/** */
	public static final String SELECT_MITGLIED_ID_ZU_FRUEHSTARTER_STMT = "select t." + ISerienteilnahmenTableNames.COL_MITGLIED_ID
		+ ", sum(t." + ISerienteilnahmenTableNames.COL_FRUEHSTARTER + ") anz from " + ISerienteilnahmenTableNames.TABLE + " t, "
		+ ISerienTableNames.TABLE + " s where t." + ISerienteilnahmenTableNames.COL_SERIE_ID + " = s." + ISerienTableNames.COL_ID
		+ " and s." + ISerienTableNames.COL_RUNDE + " = ? group by t." + ISerienteilnahmenTableNames.COL_MITGLIED_ID
		+ " having anz > ?";

	/** */
	public static final String SELECT_DIPLOME_ID_ZU_MITGLIED_STMT = "select " + IDiplomeTableNames.COL_ID + " from "
		+ IDiplomeTableNames.TABLE + " where " + IDiplomeTableNames.COL_MITGLIED_ID + " = ?";

	/** */
	public static final String INSERT_MITGLIED_STMT = "insert into " + IMitgliederTableNames.TABLE + " ("
		+ IMitgliederTableNames.COL_SCHLUESSEL + ", " + IMitgliederTableNames.COL_VORNAME + ", "
		+ IMitgliederTableNames.COL_NACHNAME + ", " + IMitgliederTableNames.COL_KLASSE + ", " + IMitgliederTableNames.COL_JAHRE
		+ ", " + IMitgliederTableNames.COL_ERFINDERPUNKTE + ", " + IMitgliederTableNames.COL_GEBURTSMONAT + ", "
		+ IMitgliederTableNames.COL_GEBURTSJAHR + ", " + IMitgliederTableNames.COL_KONTAKT + ", "
		+ IMitgliederTableNames.COL_FOTOTITEL + ", " + IMitgliederTableNames.COL_ID + " ) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	public static final String CHANGE_MITGLIED_ACTIVATION = "update " + IMitgliederTableNames.TABLE + " set aktiv = ? where id = ?";

	/** */
	public static final String UPDATE_MITGLIED_STMT = "update " + IMitgliederTableNames.TABLE + " set "
		+ IMitgliederTableNames.COL_SCHLUESSEL + " = ?, " + IMitgliederTableNames.COL_VORNAME + " = ?, "
		+ IMitgliederTableNames.COL_NACHNAME + " = ?, " + IMitgliederTableNames.COL_KLASSE + " = ?, "
		+ IMitgliederTableNames.COL_JAHRE + " = ?, " + IMitgliederTableNames.COL_ERFINDERPUNKTE + " = ?, "
		+ IMitgliederTableNames.COL_GEBURTSMONAT + " = ?, " + IMitgliederTableNames.COL_GEBURTSJAHR + " = ?, "
		+ IMitgliederTableNames.COL_AKTIV + " = ?, " + IMitgliederTableNames.COL_KONTAKT + " = ?, "
		+ IMitgliederTableNames.COL_FOTOTITEL + " = ? where " + IMitgliederTableNames.COL_ID + " = ?";

	public static final String DELETE_MITGLIED_STMT = "delete from " + IMitgliederTableNames.TABLE + " where "
		+ IMitgliederTableNames.COL_ID + " = ?";

	public static final String INSERT_DIPLOM_STMT = "insert into " + IDiplomeTableNames.TABLE + " ("
		+ IDiplomeTableNames.COL_ERFINDERPUNKTE + ", " + IDiplomeTableNames.COL_DATUM + ", " + IDiplomeTableNames.COL_MITGLIED_ID
		+ ", " + IDiplomeTableNames.COL_ID + ") values (?, ?, ?, ?)";

	public static final String UPDATE_DIPLOM_STMT = "update " + IDiplomeTableNames.TABLE + " set "
		+ IDiplomeTableNames.COL_ERFINDERPUNKTE + " = ?, " + IDiplomeTableNames.COL_DATUM + " = ?, "
		+ IDiplomeTableNames.COL_MITGLIED_ID + " = ? where " + IDiplomeTableNames.COL_ID + " = ?";

	public static final String DELETE_DIPLOM_STMT = "delete from " + IDiplomeTableNames.TABLE + " where "
		+ IDiplomeTableNames.COL_ID + " = ?";

	public static final String INSERT_SERIENTEILNAHME_STMT = "insert into " + ISerienteilnahmenTableNames.TABLE + " ("
		+ ISerienteilnahmenTableNames.COL_FRUEHSTARTER + ", " + ISerienteilnahmenTableNames.COL_MITGLIED_ID + ", "
		+ ISerienteilnahmenTableNames.COL_PUNKTE + ", " + ISerienteilnahmenTableNames.COL_SERIE_ID + ", "
		+ ISerienteilnahmenTableNames.COL_ID + ") values (?, ?, ?, ?, ?)";

	public static final String UPDATE_SERIENTEILNAHME_STMT = "update " + ISerienteilnahmenTableNames.TABLE + " set "
		+ ISerienteilnahmenTableNames.COL_FRUEHSTARTER + " = ?, " + ISerienteilnahmenTableNames.COL_MITGLIED_ID + " = ?, "
		+ ISerienteilnahmenTableNames.COL_PUNKTE + " = ?, " + ISerienteilnahmenTableNames.COL_SERIE_ID + " = ? where "
		+ ISerienteilnahmenTableNames.COL_ID + " = ?";

	public static final String DELETE_SERIENTEILNAHME_STMT = "delete from " + ISerienteilnahmenTableNames.TABLE + " where "
		+ ISerienteilnahmenTableNames.COL_ID + " = ?";

}
