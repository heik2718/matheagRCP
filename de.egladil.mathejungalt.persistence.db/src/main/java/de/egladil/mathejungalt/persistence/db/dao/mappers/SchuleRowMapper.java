/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import de.egladil.base.exceptions.MatheJungAltConfigurationException;
import de.egladil.mathejungalt.domain.schulen.Land;
import de.egladil.mathejungalt.domain.schulen.Schule;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.ISchulenTableNames;

/**
 * @author winkelv
 */
public class SchuleRowMapper implements ParameterizedRowMapper<Schule> {

	private Map<String, Land> laenderMap;

	/**
	 * @param laenderMap
	 */
	public SchuleRowMapper(Map<String, Land> laenderMap) {
		this.laenderMap = laenderMap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public Schule mapRow(ResultSet pArg0, int pArg1) throws SQLException {
		Schule schule = new Schule();
		schule.setId(pArg0.getLong(ISchulenTableNames.COL_ID));
		schule.setSchluessel(pArg0.getString(ISchulenTableNames.COL_SCHLUESSEL));
		schule.setName(pArg0.getString(ISchulenTableNames.COL_NAME));
		schule.setAnschrift(pArg0.getString(ISchulenTableNames.COL_ANSCHRIFT));
		schule.setTelefonnummer(pArg0.getString(ISchulenTableNames.COL_TELEFON));
		schule.setLand(findLand(pArg0.getString(ISchulenTableNames.COL_BUNDESLAND)));
		return schule;
	}

	private Land findLand(String schluessel) {
		for (String kenner : laenderMap.keySet()) {
			if (kenner.equals(schluessel)) {
				return laenderMap.get(kenner);
			}
		}
		throw new MatheJungAltConfigurationException("Land mit schluessel '" + schluessel + "' gibt es nicht in der DB");
	}

}
