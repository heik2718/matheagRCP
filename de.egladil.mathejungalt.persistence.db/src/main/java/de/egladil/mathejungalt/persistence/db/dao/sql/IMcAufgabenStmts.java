/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.sql;

import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMcAufgabenTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMcRaetselitemsTableNames;

/**
 * @author aheike
 */
public interface IMcAufgabenStmts {

	/** ... from aufgaben a left join quellen q on a.quelle_id = q.id left outer join hefte h on a.heft_id = h.id */
	String SELECT_STMT = "select a." + IMcAufgabenTableNames.COL_ID + ", a." + IMcAufgabenTableNames.COL_SCHLUESSEL
		+ ", a." + IMcAufgabenTableNames.COL_THEMA + ", a." + IMcAufgabenTableNames.COL_STUFE + ", a."
		+ IMcAufgabenTableNames.COL_PUNKTE + ", a." + IMcAufgabenTableNames.COL_ART + ", a."
		+ IMcAufgabenTableNames.COL_TITEL + ", a." + IMcAufgabenTableNames.COL_ANTWORT1 + ", a."
		+ IMcAufgabenTableNames.COL_ANTWORT2 + ", a." + IMcAufgabenTableNames.COL_ANTWORT3 + ", a."
		+ IMcAufgabenTableNames.COL_ANTWORT4 + ", a." + IMcAufgabenTableNames.COL_ANTWORT5 + ", a."
		+ IMcAufgabenTableNames.COL_LOESUNG + ", a." + IMcAufgabenTableNames.COL_BEMERKUNG + ", a."
		+ IMcAufgabenTableNames.COL_QUELLE + ", a." + IMcAufgabenTableNames.COL_VERZEICHNIS + ", a."
		+ IMcAufgabenTableNames.COL_TABELLE_GENERIEREN + ", a." + IMcAufgabenTableNames.COL_ANZAHL_ANTWORTEN + " from "
		+ IMcAufgabenTableNames.TABLE + " a";

	/** */
	String SELECT_MAX_KEY_STMT = "select max(" + IMcAufgabenTableNames.COL_SCHLUESSEL + ") as "
		+ IMcAufgabenTableNames.COL_SCHLUESSEL + " from " + IMcAufgabenTableNames.TABLE + " where "
		+ IMcAufgabenTableNames.COL_SCHLUESSEL + " not like ?";

	/** */
	String INSERT_STMT = "insert into " + IMcAufgabenTableNames.TABLE + " (" + IMcAufgabenTableNames.COL_SCHLUESSEL
		+ ", " + IMcAufgabenTableNames.COL_THEMA + ", " + IMcAufgabenTableNames.COL_STUFE + ", "
		+ IMcAufgabenTableNames.COL_PUNKTE + ", " + IMcAufgabenTableNames.COL_ART + ", "
		+ IMcAufgabenTableNames.COL_TITEL + ", " + IMcAufgabenTableNames.COL_ANTWORT1 + ", "
		+ IMcAufgabenTableNames.COL_ANTWORT2 + ", " + IMcAufgabenTableNames.COL_ANTWORT3 + ", "
		+ IMcAufgabenTableNames.COL_ANTWORT4 + ", " + IMcAufgabenTableNames.COL_ANTWORT5 + ", "
		+ IMcAufgabenTableNames.COL_LOESUNG + ", " + IMcAufgabenTableNames.COL_BEMERKUNG + ", "
		+ IMcAufgabenTableNames.COL_QUELLE + ", " + IMcAufgabenTableNames.COL_VERZEICHNIS + ", "
		+ IMcAufgabenTableNames.COL_TABELLE_GENERIEREN + ", " + IMcAufgabenTableNames.COL_ANZAHL_ANTWORTEN + ", "
		+ IMcAufgabenTableNames.COL_ID + ") values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	/** */
	String UPDATE_STMT = "update " + IMcAufgabenTableNames.TABLE + " set " + IMcAufgabenTableNames.COL_SCHLUESSEL
		+ " = ?, " + IMcAufgabenTableNames.COL_THEMA + " = ?, " + IMcAufgabenTableNames.COL_STUFE + " = ?, "
		+ IMcAufgabenTableNames.COL_PUNKTE + " = ?, " + IMcAufgabenTableNames.COL_ART + " = ?, "
		+ IMcAufgabenTableNames.COL_TITEL + " = ?, " + IMcAufgabenTableNames.COL_ANTWORT1 + " = ?, "
		+ IMcAufgabenTableNames.COL_ANTWORT2 + " = ?, " + IMcAufgabenTableNames.COL_ANTWORT3 + " = ?, "
		+ IMcAufgabenTableNames.COL_ANTWORT4 + " = ?, " + IMcAufgabenTableNames.COL_ANTWORT5 + " = ?, "
		+ IMcAufgabenTableNames.COL_LOESUNG + " = ?, " + IMcAufgabenTableNames.COL_BEMERKUNG + " = ?, "
		+ IMcAufgabenTableNames.COL_QUELLE + " = ?, " + IMcAufgabenTableNames.COL_VERZEICHNIS + " = ?, "
		+ IMcAufgabenTableNames.COL_TABELLE_GENERIEREN + " = ?, " + IMcAufgabenTableNames.COL_ANZAHL_ANTWORTEN
		+ " = ? " + "where " + IMcAufgabenTableNames.COL_ID + " = ?";

	/** */
	String DELETE_STMT = "delete from " + IMcAufgabenTableNames.TABLE + " where " + IMcAufgabenTableNames.COL_ID
		+ " = ?";

	/** TODO */
	String SELECT_ANZAHL_RAETSELITEM = "select count(" + IMcRaetselitemsTableNames.COL_ID + ") as anzahl from "
		+ IMcRaetselitemsTableNames.TABLE + " where " + IMcRaetselitemsTableNames.COL_MC_AUFGABE_ID + " = ?";

	String FIND_RAESTEL_ID_BY_AUFGABE = "select i.raetsel_id from " + IMcRaetselitemsTableNames.TABLE
		+ " i where i.mc_aufgabe_id = ?";
}
