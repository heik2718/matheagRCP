/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.mysql.IJdbcTemplateWrapper;
import de.egladil.mathejungalt.domain.medien.AbstractPrintmedium;
import de.egladil.mathejungalt.domain.medien.Autor;
import de.egladil.mathejungalt.domain.medien.Buch;
import de.egladil.mathejungalt.domain.types.EnumTypes.Medienart;
import de.egladil.mathejungalt.persistence.api.IAutorenRepository;
import de.egladil.mathejungalt.persistence.api.IMedienRepository;
import de.egladil.mathejungalt.persistence.db.dao.mappers.LongRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.MedienRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.SchluesselRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.sql.IMedienStmts;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMedienTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IQuellenTableNames;
import de.egladil.mathejungalt.persistence.db.dao.typehandler.MedienartTypeHandler;
import de.egladil.mathejungalt.persistence.db.dao.util.SqlQueryUtil;

/**
 * @author Winkelv
 */
public class MedienDao extends AbstractDao implements IMedienRepository {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(MedienDao.class);

	/** */
	private IAutorenRepository autorenRepository;

	/** */
	private Map<Long, AbstractPrintmedium> medienMap;

	/**
	 * @param pJdbcTemplateWrapper
	 */
	public MedienDao(IJdbcTemplateWrapper pJdbcTemplateWrapper, IdGenerator idGenerator) {
		super(pJdbcTemplateWrapper, idGenerator);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.core.persistenceapi.medien.IMedienRepository#anzahlQuellen(de.egladil.mathe.core.domain.medien
	 * . AbstractPrintmedium)
	 */
	@Override
	public long anzahlQuellen(AbstractPrintmedium pMedium) {
		String sql = "select count(*) from " + IQuellenTableNames.TABLE + " where ";
		switch (pMedium.getArt()) {
		case B:
			sql += IQuellenTableNames.COL_BUCH_ID;
			break;

		default:
			sql += IQuellenTableNames.COL_ZEITSCHR_ID;
			break;
		}
		sql += " = ?";
		return getJdbcTemplate().queryForLong(sql, new Object[] { pMedium.getId() });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.medien.IMedienRepository#findAllMedien()
	 */
	@Override
	public List<AbstractPrintmedium> findAllMedien() {
		if (medienMap == null)
			loadMedien();
		Iterator<Long> iter = medienMap.keySet().iterator();
		List<AbstractPrintmedium> liste = new ArrayList<AbstractPrintmedium>();
		while (iter.hasNext())
			liste.add(medienMap.get(iter.next()));
		return liste;
	}

	/**
	 * 
	 */
	private void loadMedien() {
		List<AbstractPrintmedium> liste = getJdbcTemplate().query(IMedienStmts.SELECT_STMT, new MedienRowMapper());
		medienMap = new HashMap<Long, AbstractPrintmedium>();
		for (AbstractPrintmedium medium : liste)
			medienMap.put(medium.getId(), medium);
		LOG.info(liste.size() + " " + IMedienTableNames.TABLE + " geladen");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.persistenceapi.medien.IMedienRepository#findBuecherByAutor(de.egladil.mathe.core.domain
	 * .medien.Autor)
	 */
	@Override
	public List<Buch> findBuecherByAutor(Autor pAutor) {
		throw new MatheJungAltException("Bisher gab es keine Verwendung für diese Methode. Daher nicht implementiert");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.medien.IMedienRepository#findBuecherByAutorName(java.lang.String)
	 */
	@Override
	public List<Buch> findBuecherByAutorName(String pString) {
		throw new MatheJungAltException("Bisher gab es keine Verwendung für diese Methode. Daher nicht implementiert");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.medien.IMedienRepository#findByTitel(java.lang.String)
	 */
	@Override
	public List<AbstractPrintmedium> findByTitel(String pTitel) {
		if (medienMap == null)
			loadMedien();
		String paramString = SqlQueryUtil.lowerAndWrap(pTitel);
		List<Long> queryResult = getJdbcTemplate().query(IMedienStmts.SELECT_BY_STMT, new LongRowMapper(),
			new Object[] { paramString });
		List<AbstractPrintmedium> result = new ArrayList<AbstractPrintmedium>();
		for (Long lo : queryResult)
			result.add(medienMap.get(lo));
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.persistenceapi.medien.IMedienRepository#findMedienByArt(de.egladil.mathe.core.domain.types
	 * .EnumTypes .Medienart)
	 */
	@Override
	public List<AbstractPrintmedium> findMedienByArt(Medienart pArt) {
		throw new MatheJungAltException("Bisher gab es keine Verwendung für diese Methode. Daher nicht implementiert");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.medien.IMedienRepository#findMediumById(java.lang.Long)
	 */
	@Override
	public AbstractPrintmedium findMediumById(Long pId) {
		if (medienMap == null)
			loadMedien();
		AbstractPrintmedium medium = medienMap.get(pId);
		if (medium == null) {
			return null;
		}
		if (Medienart.B.equals(medium.getArt()) && !((Buch) medium).isAutorenLoaded()) {
			autorenRepository.loadAutorenZuBuch((Buch) medium);
		}
		return medienMap.get(pId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.medien.IMedienRepository#getMaxKey()
	 */
	@Override
	public String getMaxKey() {
		return getJdbcTemplate().queryForObject(IMedienStmts.SELECT_MAX_KEY, new SchluesselRowMapper());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.core.persistenceapi.medien.IMedienRepository#removeMedium(de.egladil.mathe.core.domain.medien
	 * . AbstractPrintmedium)
	 */
	@Override
	public void removeMedium(AbstractPrintmedium pMedium) {
		if (anzahlQuellen(pMedium) > 0)
			throw new MatheJungAltException("Es gibt Quellenreferenzen zu " + pMedium);
		switch (pMedium.getArt()) {
		case B: {
			removeBuch((Buch) pMedium);
			break;
		}

		default:
			removePrintmedium(pMedium);
			break;
		}

	}

	/**
	 * L�scht das Buch nebst Verweisen auf Autoren
	 * 
	 * @param pMedium
	 */
	private void removeBuch(final Buch pMedium) {
		getTransactionTemplate().execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(TransactionStatus pArg0) {
				getJdbcTemplate().update(IMedienStmts.DELETE_B2A_STMT, new Object[] { pMedium.getId() });
				removePrintmedium(pMedium);
			}
		});

	}

	/**
	 * @param pMedium
	 */
	private void removePrintmedium(AbstractPrintmedium pMedium) {
		getJdbcTemplate().update(IMedienStmts.DELETE_STMT, new Object[] { pMedium.getId() });
		medienMap.remove(pMedium.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.persistenceapi.medien.IMedienRepository#saveMedium(de.egladil.mathe.core.domain.medien.
	 * AbstractPrintmedium )
	 */
	@Override
	public void saveMedium(AbstractPrintmedium pMedium) {
		if (pMedium.getId() == null)
			persist(pMedium);
		else
			merge(pMedium);
	}

	/**
	 * @param pMedium
	 */
	private void merge(AbstractPrintmedium pMedium) {
		switch (pMedium.getArt()) {
		case B:
			mergeBuch((Buch) pMedium);
			break;

		default:
			mergePrintmedium(pMedium);
			break;
		}

	}

	private void mergeBuch(final Buch pMedium) {
		if (pMedium.getAutoren() == null)
			autorenRepository.loadAutorenZuBuch(pMedium);
		getTransactionTemplate().execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(TransactionStatus pArg0) {
				getJdbcTemplate().update(IMedienStmts.DELETE_B2A_STMT, new Object[] { pMedium.getId() });

				mergePrintmedium(pMedium);
				for (Autor autor : pMedium.getAutoren()) {
					autorenRepository.saveAutor(autor);
					getJdbcTemplate().update(IMedienStmts.INSERT_B2A_STMT,
						new Object[] { pMedium.getId(), autor.getId() });
				}
			}
		});

	}

	/**
	 * @param pMedium
	 */
	private void mergePrintmedium(AbstractPrintmedium pMedium) {
		Object[] args = new Object[] { pMedium.getSchluessel(), pMedium.getTitel(), pMedium.getId() };
		getJdbcTemplate().update(IMedienStmts.UPDATE_STMT, args);
	}

	/**
	 * @param pMedium
	 */
	private void persist(AbstractPrintmedium pMedium) {
		switch (pMedium.getArt()) {
		case Z:
			persistPrintmedium(pMedium);
			break;

		default: {
			Buch buch = (Buch) pMedium;
			// Bei (neuem) Buch ohne Autoren sind die inserts wie bei neiner Zeitschrift
			if (buch.getAutoren() == null || buch.anzahlAutoren() == 0)
				persistPrintmedium(buch);
			else
				persistBuch(buch);
		}
			break;
		}

	}

	/**
	 * @param pMedium
	 */
	private void persistPrintmedium(AbstractPrintmedium pMedium) {
		Long id = getIdGenerator().nextId(IMedienTableNames.TABLE);
		Object[] args = new Object[] { id, pMedium.getSchluessel(),
			new MedienartTypeHandler().getSqlValue(pMedium.getArt()), pMedium.getTitel() };
		getJdbcTemplate().update(IMedienStmts.INSERT_STMT, args);
		pMedium.setId(id);
		getIdGenerator().increase(IMedienTableNames.TABLE);
		medienMap.put(id, pMedium);
	}

	/**
	 * @param pMedium
	 */
	private void persistBuch(final Buch pMedium) {
		getTransactionTemplate().execute(new TransactionCallbackWithoutResult() {
			@Override
			protected void doInTransactionWithoutResult(TransactionStatus pStatus) {
				persistPrintmedium(pMedium);
				if (pMedium.getAutoren() != null) {
					for (Autor autor : pMedium.getAutoren()) {
						autorenRepository.saveAutor(autor);
						getJdbcTemplate().update(IMedienStmts.INSERT_B2A_STMT,
							new Object[] { pMedium.getId(), autor.getId() });
					}
				}
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.core.persistenceapi.medien.IMedienRepository#setAutorenRepository(de.egladil.mathe.core.
	 * persistenceapi.medien .IAutorenRepository)
	 */
	@Override
	public void setAutorenRepository(IAutorenRepository pAutorenRepository) {
		autorenRepository = pAutorenRepository;
	}
}
