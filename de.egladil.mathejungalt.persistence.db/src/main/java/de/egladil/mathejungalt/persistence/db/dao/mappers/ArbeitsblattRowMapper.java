/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IArbeitsblaetterTableNames;

/**
 * @author Winkelv
 */
public class ArbeitsblattRowMapper implements ParameterizedRowMapper<Arbeitsblatt> {

	/**
	 * 
	 */
	public ArbeitsblattRowMapper() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public Arbeitsblatt mapRow(ResultSet pArg0, int pArg1) throws SQLException {
		Arbeitsblatt arbeitsblatt = new Arbeitsblatt();
		arbeitsblatt.setId(pArg0.getLong(IArbeitsblaetterTableNames.COL_ID));
		arbeitsblatt.setSchluessel(pArg0.getString(IArbeitsblaetterTableNames.COL_SCHLUESSEL));
		arbeitsblatt.setTitel(pArg0.getString(IArbeitsblaetterTableNames.COL_TITEL));
		arbeitsblatt.setBeendet(pArg0.getInt(IArbeitsblaetterTableNames.COL_BEENDET));
		arbeitsblatt.setSperrend(pArg0.getInt(IArbeitsblaetterTableNames.COL_SPERREND));
		return arbeitsblatt;
	}

}
