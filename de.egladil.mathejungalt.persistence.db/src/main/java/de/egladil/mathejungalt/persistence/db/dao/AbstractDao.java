/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao;

import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.transaction.support.TransactionTemplate;

import de.egladil.base.mysql.IJdbcTemplateWrapper;

/**
 * @author aheike
 */
public abstract class AbstractDao {

	/** */
	private final IdGenerator idGenerator;

	/**
   * 
   */
	private final IJdbcTemplateWrapper jdbcTemplateWrapper;

	/**
	 * @param pJdbcTemplateWrapper
	 * @param idGenerator TODO
	 */
	public AbstractDao(IJdbcTemplateWrapper pJdbcTemplateWrapper, IdGenerator idGenerator) {
		jdbcTemplateWrapper = pJdbcTemplateWrapper;
		this.idGenerator = idGenerator;
	}

	protected SimpleJdbcTemplate getJdbcTemplate() {
		return jdbcTemplateWrapper.getJdbcTemplate();
	}

	/**
	 * @return the transactionTemplate
	 */
	protected TransactionTemplate getTransactionTemplate() {
		return jdbcTemplateWrapper.getTransactionTemplate();
	}

	/**
	 * @return the idGenerator
	 */
	protected IdGenerator getIdGenerator() {
		return idGenerator;
	}
}
