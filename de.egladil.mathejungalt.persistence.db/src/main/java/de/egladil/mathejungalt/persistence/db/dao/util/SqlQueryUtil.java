/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.util;

import org.apache.commons.lang.StringUtils;

/**
 * @author Winkelv
 */
public final class SqlQueryUtil {

	public enum ACTION {
		INSERT,
		UPDATE,
		DELETE
	}

	/**
	 *
	 */
	private SqlQueryUtil() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Setzt pString in Kleinbuchstaben um, ersetzt Umlaute und packt ihn in '%' ein. Falls null oder ein leerer String
	 * übergeben wird, wird ein einfaches '%' zurueckgegeben.
	 *
	 * @param pString
	 * @return
	 */
	public final static String removeUmlauteAndWrap(final String pString) {
		if (pString == null || pString.length() == 0) {
			return "%";
		}
		String erg = StringUtils.replace(pString.toLowerCase(), "ä", "ae");
		erg = StringUtils.replaceChars(erg, "ö", "oe");
		erg = StringUtils.replaceChars(erg, "ü", "ue");
		erg = StringUtils.replaceChars(erg, "ß", "ss");
		return "%" + erg + "%";
	}

	/**
	 * Ersetzt Umlaute in pString. Falls null �bergeben wird, wird der Parameter einfach wieder zur�ckgegeben.
	 *
	 * @param pString
	 * @return
	 */
	public final static String removeUmlaute(final String pString) {
		if (pString == null)
			return pString;
		String erg = StringUtils.replace(pString, "ä", "ae");
		erg = StringUtils.replaceChars(erg, "ö", "oe");
		erg = StringUtils.replaceChars(erg, "ü", "ue");
		erg = StringUtils.replaceChars(erg, "ß", "ss");
		return erg;
	}

	/**
	 * Setzt pString in Kleinbuchstaben um und packt ihn in '%' ein. Falls null oder ein leerer String übergeben wird,
	 * wird ein einfaches '%' zurückgegeben.
	 *
	 * @param pString
	 * @return
	 */
	public final static String lowerAndWrap(final String pString) {
		if (pString == null || pString.length() == 0) {
			return "%";
		}
		String erg = pString.toLowerCase();
		return "%" + erg + "%";
	}

}
