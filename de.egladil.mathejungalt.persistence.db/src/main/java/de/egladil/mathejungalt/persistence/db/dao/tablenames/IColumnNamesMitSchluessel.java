/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao.tablenames;

/**
 * @author Winkelv
 */
public interface IColumnNamesMitSchluessel extends ICommonColumnNamesOhneSchluessel {

	public final static String COL_SCHLUESSEL = "schluessel";
}
