/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.persistence.db.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.domain.schulen.Schule;
import de.egladil.mathejungalt.domain.schulen.Schulkontakt;
import de.egladil.mathejungalt.persistence.api.ISchulenRepository;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.ISchulkontakteTableNames;

/**
 * <p>
 * NOTE: THIS WIDGET AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 * 
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class SchulkontakteRowMapper implements ParameterizedRowMapper<Schulkontakt> {

	private ISchulenRepository schulenRepository;

	private Kontakt kontakt;

	/**
	 * @param pMinikaenguruRepository
	 * @param pKontakt
	 */
	public SchulkontakteRowMapper(ISchulenRepository pSchulenRepository, Kontakt pKontakt) {
		super();
		schulenRepository = pSchulenRepository;
		kontakt = pKontakt;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public Schulkontakt mapRow(ResultSet pArg0, int pArg1) throws SQLException {
		Schulkontakt schulkontakt = new Schulkontakt();
		schulkontakt.setId(pArg0.getLong(ISchulkontakteTableNames.COL_ID));

		Long schuleId = pArg0.getLong(ISchulkontakteTableNames.COL_SCHULE_ID);
		Schule schule = schulenRepository.findSchuleById(schuleId);
		schulkontakt.setSchule(schule);

		schulkontakt.setKontakt(kontakt);
		kontakt.addSchulkontakt(schulkontakt);

		return schulkontakt;
	}
}
