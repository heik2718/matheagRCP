/**
 *
 */
package de.egladil.mathejungalt.persistence.db;

import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mathejungalt.persistence.api.IArbeitsblattRepository;
import de.egladil.mathejungalt.persistence.api.IAufgabenRepository;
import de.egladil.mathejungalt.persistence.api.IAutorenRepository;
import de.egladil.mathejungalt.persistence.api.IHefteRepository;
import de.egladil.mathejungalt.persistence.api.IKontakteRepository;
import de.egladil.mathejungalt.persistence.api.IMailinglisteRepository;
import de.egladil.mathejungalt.persistence.api.IMcAufgabenRepository;
import de.egladil.mathejungalt.persistence.api.IMcRaetselRepository;
import de.egladil.mathejungalt.persistence.api.IMedienRepository;
import de.egladil.mathejungalt.persistence.api.IMinikaenguruRepository;
import de.egladil.mathejungalt.persistence.api.IMitgliederRepository;
import de.egladil.mathejungalt.persistence.api.IPersistenceservice;
import de.egladil.mathejungalt.persistence.api.IQuellenRepository;
import de.egladil.mathejungalt.persistence.api.ISchulenRepository;
import de.egladil.mathejungalt.persistence.api.ISerienRepository;
import de.egladil.mathejungalt.persistence.api.IUrheberRepository;
import de.egladil.mathejungalt.persistence.db.dao.IdGenerator;

/**
 * @author Winkelv
 */
public class MatheAGJdbcPersistenceFacade implements IPersistenceservice {

	private final static Logger LOG = LoggerFactory.getLogger(MatheAGJdbcPersistenceFacade.class);

	private IAufgabenRepository aufgabenRepository;

	private IHefteRepository hefteRepository;

	private IMinikaenguruRepository minikaenguruRepository;

	private ISerienRepository serienRepository;

	private IMedienRepository medienRepository;

	private IAutorenRepository autorenRepository;

	private IMitgliederRepository mitgliederRepository;

	private IQuellenRepository quellenRepository;

	private IMailinglisteRepository mailinglisteRepository;

	private IArbeitsblattRepository arbeitsblattRepository;

	private IMcAufgabenRepository mcAufgabenRepository;

	private IMcRaetselRepository raetselRepository;

	private IUrheberRepository urheberRepository;

	private ISchulenRepository schulenRepository;

	private IKontakteRepository kontakteRepository;

	/** */
	private IdGenerator idGenerator;

	/** */
	private Properties jdbcProperties = new Properties();

	/** */
	private boolean jdbcPropertiesLoaded = false;

	/**
	 *
	 */
	public MatheAGJdbcPersistenceFacade() {
		System.out.println("Starte MatheAGJdbcPersistenceFacade");
	}

	@Override
	public String[] getProtokolle() {
		return new String[] { "db" };
	}

	/**
	 * @return the aufgabenRepository
	 */
	public IAufgabenRepository getAufgabenRepository() {
		return aufgabenRepository;
	}

	/**
	 * @param pAufgabenRepository the aufgabenRepository to set
	 */
	public void setAufgabenRepository(IAufgabenRepository pAufgabenRepository) {
		aufgabenRepository = pAufgabenRepository;
	}

	/**
	 * @return the hefteRepository
	 */
	public IHefteRepository getHefteRepository() {
		return hefteRepository;
	}

	/**
	 * @param pHefteRepository the hefteRepository to set
	 */
	public void setHefteRepository(IHefteRepository pHefteRepository) {
		hefteRepository = pHefteRepository;
	}

	/**
	 * @return the minikaenguruRepository
	 */
	public IMinikaenguruRepository getMinikaenguruRepository() {
		return minikaenguruRepository;
	}

	/**
	 * @param pMinikaenguruRepository the minikaenguruRepository to set
	 */
	public void setMinikaenguruRepository(IMinikaenguruRepository pMinikaenguruRepository) {
		minikaenguruRepository = pMinikaenguruRepository;
	}

	/**
	 * @return the serienRepository
	 */
	public ISerienRepository getSerienRepository() {
		return serienRepository;
	}

	/**
	 * @param pSerienRepository the serienRepository to set
	 */
	public void setSerienRepository(ISerienRepository pSerienRepository) {
		serienRepository = pSerienRepository;
	}

	/**
	 * @return the medienRepository
	 */
	public IMedienRepository getMedienRepository() {
		return medienRepository;
	}

	/**
	 * @param pMedienRepository the medienRepository to set
	 */
	public void setMedienRepository(IMedienRepository pMedienRepository) {
		medienRepository = pMedienRepository;
	}

	/**
	 * @return the autorenRepository
	 */
	public IAutorenRepository getAutorenRepository() {
		return autorenRepository;
	}

	/**
	 * @param pAutorenRepository the autorenRepository to set
	 */
	public void setAutorenRepository(IAutorenRepository pAutorenRepository) {
		autorenRepository = pAutorenRepository;
	}

	/**
	 * @return the mitgliederRepository
	 */
	public IMitgliederRepository getMitgliederRepository() {
		return mitgliederRepository;
	}

	/**
	 * @param pMitgliederRepository the mitgliederRepository to set
	 */
	public void setMitgliederRepository(IMitgliederRepository pMitgliederRepository) {
		mitgliederRepository = pMitgliederRepository;
	}

	/**
	 * @return the quellenRepository
	 */
	public IQuellenRepository getQuellenRepository() {
		return quellenRepository;
	}

	/**
	 * @param pQuellenRepository the quellenRepository to set
	 */
	public void setQuellenRepository(IQuellenRepository pQuellenRepository) {
		quellenRepository = pQuellenRepository;
	}

	/**
	 * @return the mailinglisteRepository
	 */
	public IMailinglisteRepository getMailinglisteRepository() {
		return mailinglisteRepository;
	}

	/**
	 * @param pMailinglisteRepository the mailinglisteRepository to set
	 */
	public void setMailinglisteRepository(IMailinglisteRepository pMailinglisteRepository) {
		mailinglisteRepository = pMailinglisteRepository;
	}

	/**
	 * @return the arbeitsblattRepository
	 */
	public IArbeitsblattRepository getArbeitsblattRepository() {
		return arbeitsblattRepository;
	}

	/**
	 * @param pArbeitsblattRepository the arbeitsblattRepository to set
	 */
	public void setArbeitsblattRepository(IArbeitsblattRepository pArbeitsblattRepository) {
		arbeitsblattRepository = pArbeitsblattRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IMatheAGPersistenceFacade#shutDown()
	 */
	@Override
	public boolean shutDown() {
		try {
			idGenerator.flush();
			return true;
		} catch (Exception e) {
			LOG.error("Fehler beim Speichern des IdGenerators: " + e.getMessage(), e);
			return false;
		}
	}

	/**
	 * @param pIdGenerator
	 */
	public void setIdGenerator(IdGenerator pIdGenerator) {
		idGenerator = pIdGenerator;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getPersistenceLocation(java.lang.String)
	 */
	@Override
	public String getPersistenceLocation() {
		if (!jdbcPropertiesLoaded) {
			final String jdbcPropertiesLocation = "/META-INF/jdbc/jdbc.properties";
			try {
				jdbcProperties.load(MatheAGJdbcPersistenceFacade.class.getResourceAsStream(jdbcPropertiesLocation));
			} catch (IOException e1) {
				String msg = "jdbc.prioerties konnte nicht geladen werden: '" + jdbcPropertiesLocation + "'";
				LOG.warn(msg);

			}
			jdbcPropertiesLoaded = true;
		}
		String db = jdbcProperties.getProperty("jdbc.url");
		LOG.info("DB = " + db);
		return db == null ? "null" : db;
	}

	/**
	 * @return the mcAufgabenRepository
	 */
	@Override
	public final IMcAufgabenRepository getMcAufgabenRepository() {
		return mcAufgabenRepository;
	}

	/**
	 * @param pMcAufgabenRepository the mcAufgabenRepository to set
	 */
	public final void setMcAufgabenRepository(IMcAufgabenRepository pMcAufgabenRepository) {
		mcAufgabenRepository = pMcAufgabenRepository;
	}

	/**
	 * @return the raetselRepository
	 */
	@Override
	public final IMcRaetselRepository getRaetselRepository() {
		return raetselRepository;
	}

	/**
	 * @param pRaetselRepository the raetselRepository to set
	 */
	public final void setRaetselRepository(IMcRaetselRepository pRaetselRepository) {
		raetselRepository = pRaetselRepository;
	}

	/**
	 * @return the urheberRepository
	 */
	@Override
	public final IUrheberRepository getUrheberRepository() {
		return urheberRepository;
	}

	/**
	 * @param pUrheberRepository the urheberRepository to set
	 */
	public final void setUrheberRepository(IUrheberRepository pUrheberRepository) {
		urheberRepository = pUrheberRepository;
	}

	/**
	 * @return the schulenRepository
	 */
	public final ISchulenRepository getSchulenRepository() {
		return schulenRepository;
	}

	/**
	 * @param pSchulenRepository the schulenRepository to set
	 */
	public final void setSchulenRepository(ISchulenRepository pSchulenRepository) {
		schulenRepository = pSchulenRepository;
	}

	/**
	 * @return the kontakteRepository
	 */
	public final IKontakteRepository getKontakteRepository() {
		return kontakteRepository;
	}

	/**
	 * @param pKontakteRepository the kontakteRepository to set
	 */
	public final void setKontakteRepository(IKontakteRepository pKontakteRepository) {
		kontakteRepository = pKontakteRepository;
	}
}
