/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.mysql.IJdbcTemplateWrapper;
import de.egladil.mathejungalt.domain.medien.Autor;
import de.egladil.mathejungalt.domain.medien.Buch;
import de.egladil.mathejungalt.persistence.api.IAutorenRepository;
import de.egladil.mathejungalt.persistence.db.dao.mappers.AutorRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.SchluesselRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.sql.IAutorenStmts;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IAutorenTableNames;

/**
 * @author Winkelv
 */
public class AutorenDAO extends AbstractDao implements IAutorenRepository {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(AutorenDAO.class);

	/**
	 * @param pJdbcTemplateWrapper
	 */
	public AutorenDAO(IJdbcTemplateWrapper pJdbcTemplateWrapper, IdGenerator idGenerator) {
		super(pJdbcTemplateWrapper, idGenerator);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.persistenceapi.medien.IAutorenRepository#anzahlBuecher(de.egladil.mathe.core.domain.medien
	 * .Autor)
	 */
	@Override
	public long anzahlBuecher(Autor pAutor) {
		return getJdbcTemplate()
			.queryForLong(IAutorenStmts.SELECT_ANZAHL_BUECHER_STMT, new Object[] { pAutor.getId() });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.medien.IAutorenRepository#findAllAutoren()
	 */
	@Override
	public List<Autor> findAllAutoren() {
		List<Autor> liste = getJdbcTemplate().query(IAutorenStmts.SELECT_STMT, new AutorRowMapper());
		LOG.info(liste.size() + " " + IAutorenTableNames.TABLE + " geladen");
		return liste;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.persistenceapi.medien.IAutorenRepository#autorenZuBuch(de.egladil.mathe.core.domain.medien
	 * .Buch)
	 */
	@Override
	public void loadAutorenZuBuch(Buch pBuch) {
		if (pBuch.isAutorenLoaded())
			return;
		List<Autor> autoren = getJdbcTemplate().query(IAutorenStmts.SELECT_AUTOR_ZU_BUCH_STMT, new AutorRowMapper(),
			new Object[] { pBuch.getId() });
		pBuch.initAutoren();
		for (Autor autor : autoren) {
			pBuch.addAutor(autor);
		}
		pBuch.setAutorenLoaded(true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.medien.IAutorenRepository#getMaxAutorenKey()
	 */
	@Override
	public String getMaxKey() {
		return getJdbcTemplate().queryForObject(IAutorenStmts.SELECT_MAX_KEY_STMT, new SchluesselRowMapper());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.persistenceapi.medien.IAutorenRepository#removeAutor(de.egladil.mathe.core.domain.medien
	 * .Autor)
	 */
	@Override
	public void removeAutor(Autor pAutor) {
		if (anzahlBuecher(pAutor) > 0)
			throw new MatheJungAltException("Es gibt Bücher mit dem Autor " + pAutor);
		getJdbcTemplate().update(IAutorenStmts.DELETE_STMT, new Object[] { pAutor.getId() });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.persistenceapi.medien.IAutorenRepository#saveAutor(de.egladil.mathe.core.domain.medien.
	 * Autor)
	 */
	@Override
	public void saveAutor(Autor pAutor) {
		if (pAutor.getId() == null)
			persist(pAutor);
		else
			merge(pAutor);
	}

	/**
	 * update
	 * 
	 * @param pAutor
	 */
	private void merge(Autor pAutor) {
		Object[] args = new Object[] { pAutor.getSchluessel(), pAutor.getName(), pAutor.getId() };
		getJdbcTemplate().update(IAutorenStmts.UPDATE_STMT, args);
	}

	/**
	 * insert
	 * 
	 * @param pAutor
	 */
	private void persist(Autor pAutor) {
		Long id = getIdGenerator().nextId(IAutorenTableNames.TABLE);
		Object[] params = new Object[] { id, pAutor.getSchluessel(), pAutor.getName() };
		getJdbcTemplate().update(IAutorenStmts.INSERT_STMT, params);
		pAutor.setId(id);
		getIdGenerator().increase(IAutorenTableNames.TABLE);
	}
}
