/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import de.egladil.mathejungalt.domain.mitglieder.Diplom;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IDiplomeTableNames;
import de.egladil.mathejungalt.persistence.db.dao.typehandler.DateTypeHandler;

/**
 * @author Winkelv
 */
public class DiplomRowMapper implements ParameterizedRowMapper<Diplom> {

	/** */
	private Mitglied mitglied;

	private DateTypeHandler dateTypehandler;

	/**
	 * @param pMitglied
	 */
	public DiplomRowMapper(Mitglied pMitglied) {
		super();
		mitglied = pMitglied;
		dateTypehandler = new DateTypeHandler();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public Diplom mapRow(ResultSet pArg0, int pArg1) throws SQLException {
		Diplom diplom = new Diplom();
		diplom.setId(pArg0.getLong(IDiplomeTableNames.COL_ID));
		diplom.setAnzahlErfinderpunkte(pArg0.getInt(IDiplomeTableNames.COL_ERFINDERPUNKTE));
		diplom.setDatum(dateTypehandler.getDomainValue(pArg0.getDate(IDiplomeTableNames.COL_DATUM)));
		diplom.setMitglied(mitglied);
		mitglied.addDiplom(diplom);
		return diplom;
	}

}
