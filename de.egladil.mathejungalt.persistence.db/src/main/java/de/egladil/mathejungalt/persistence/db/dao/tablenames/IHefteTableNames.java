/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.tablenames;

/**
 * @author Winkelv
 */
public interface IHefteTableNames extends IColumnNamesMitSchluessel {

	/** */
	public static final String TABLE = "hefte";

	/** */
	public static final String COL_TITEL = "titel";

	/** */
	public static final String COL_BEENDET = "beendet";
}
