/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao.sql;

import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMailinglisteTableNames;

/**
 * @author aheike
 */
public interface IMailingListeStmts {

	/** */
	public static final String SELECT_STMT = "select " + IMailinglisteTableNames.COL_ID + ", "
		+ IMailinglisteTableNames.COL_EMAIL + " from " + IMailinglisteTableNames.TABLE;

	/** */
	public static final String INSERT_STMT = "insert into " + IMailinglisteTableNames.TABLE + " ("
		+ IMailinglisteTableNames.COL_EMAIL + ", " + IMailinglisteTableNames.COL_ID + ") values (?,?)";

	/** */
	public static final String UPDATE_STMT = "update " + IMailinglisteTableNames.TABLE + " set "
		+ IMailinglisteTableNames.COL_EMAIL + " = ? where " + IMailinglisteTableNames.COL_ID + " = ?";

	/** */
	public static final String DELETE_STMT = "delete from " + IMailinglisteTableNames.TABLE + " where "
		+ IMailinglisteTableNames.COL_ID + " = ? ";

}
