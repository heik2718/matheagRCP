/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.tablenames;

/**
 * @author Winkelv
 */
public interface IMinikaenguruteilnahmenTableNames extends ICommonColumnNamesOhneSchluessel {

	/** */
	public static final String TABLE = "mini_teilnahmen";

	/** */
	public static final String COL_RUECKMELDUNG = "rueckmeldung";

	/** */
	public static final String COL_SCHULE_ID = "schule_id";

	/** */
	public static final String COL_MINIKAENGURU_ID = "mini_id";
}
