/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.sql;

import de.egladil.mathejungalt.persistence.db.dao.tablenames.ISerienTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.ISerienitemsTableNames;

/**
 * @author aheike
 */
public interface ISerienStmts {

	/** */
	public static final String SELECT_STMT = "select " + ISerienTableNames.COL_ID + ", " + ISerienTableNames.COL_NUMMER
		+ ", " + ISerienTableNames.COL_RUNDE + ", " + ISerienTableNames.COL_DATUM + ", "
		+ ISerienTableNames.COL_BEENDET + ", " + ISerienTableNames.COL_MONAT_JAHR + " from " + ISerienTableNames.TABLE;

	/** */
	public static final String SELECT_ITEMS_ZU_SERIE_STMT = "select " + ISerienitemsTableNames.COL_ID + ", "
		+ ISerienitemsTableNames.COL_AUFGABE_ID + ", " + ISerienitemsTableNames.COL_NUMMER + " from "
		+ ISerienitemsTableNames.TABLE + " where " + ISerienitemsTableNames.COL_SERIE_ID + " = ?";

	/** */
	public static final String SELECT_MAX_KEY_STMT = "select max(" + ISerienTableNames.COL_NUMMER + ") from "
		+ ISerienTableNames.TABLE;

	/** */
	public static final String SELECT_SERIENITEM_ID_ZU_SERIE_STMT = "select " + ISerienitemsTableNames.COL_ID
		+ " from " + ISerienitemsTableNames.TABLE + " where " + ISerienitemsTableNames.COL_SERIE_ID + " = ?";

	/** */
	public static final String INSERT_SERIE_STMT = "insert into " + ISerienTableNames.TABLE + " ("
		+ ISerienTableNames.COL_NUMMER + ", " + ISerienTableNames.COL_RUNDE + ", " + ISerienTableNames.COL_DATUM + ", "
		+ ISerienTableNames.COL_BEENDET + ", " + ISerienTableNames.COL_MONAT_JAHR + ", " + ISerienTableNames.COL_ID
		+ " ) values (?, ?, ?, ?, ?, ?)";

	/** */
	public static final String UPDATE_SERIE_STMT = "update " + ISerienTableNames.TABLE + " set "
		+ ISerienTableNames.COL_NUMMER + " = ?, " + ISerienTableNames.COL_RUNDE + " = ?, "
		+ ISerienTableNames.COL_DATUM + " = ?, " + ISerienTableNames.COL_BEENDET + " = ?, "
		+ ISerienTableNames.COL_MONAT_JAHR + " = ? where " + ISerienTableNames.COL_ID + " = ?";

	/** */
	public static final String INSERT_SERIENITEM_STMT = "insert into " + ISerienitemsTableNames.TABLE + " ("
		+ ISerienitemsTableNames.COL_SERIE_ID + ", " + ISerienitemsTableNames.COL_AUFGABE_ID + ", "
		+ ISerienitemsTableNames.COL_NUMMER + ", " + ISerienitemsTableNames.COL_ID + " ) values (?, ?, ?, ?)";

	/** */
	public static final String UPDATE_SERIENITEM_STMT = "update " + ISerienitemsTableNames.TABLE + " set "
		+ ISerienitemsTableNames.COL_SERIE_ID + " = ?, " + ISerienitemsTableNames.COL_AUFGABE_ID + " = ?, "
		+ ISerienitemsTableNames.COL_NUMMER + " = ? where " + ISerienitemsTableNames.COL_ID + " = ?";

	public static final String DELETE_SERIE_STMT = "delete from " + ISerienTableNames.TABLE + " where "
		+ ISerienTableNames.COL_ID + " = ?";

	public static final String DELETE_SERIENITEM_STMT = "delete from " + ISerienitemsTableNames.TABLE + " where "
		+ ISerienitemsTableNames.COL_ID + " = ?";

}
