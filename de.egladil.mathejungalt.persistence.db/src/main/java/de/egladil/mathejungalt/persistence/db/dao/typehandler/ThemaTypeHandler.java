/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao.typehandler;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenart;
import de.egladil.mathejungalt.domain.types.EnumTypes.Medienart;
import de.egladil.mathejungalt.domain.types.EnumTypes.Thema;

/**
 * @author Winkelv
 */
public class ThemaTypeHandler {

	/**
	 * 
	 */
	public ThemaTypeHandler() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Wandelt String in Medienart um.
	 * 
	 * @param pSqlValue
	 * @return {@link Medienart}
	 * @throws MatheJungAltException
	 */
	public Thema getDomainValue(final String pSqlValue) throws MatheJungAltException {
		if (pSqlValue == null)
			throw new MatheJungAltException("Parameter darf nicht null sein");
		if ("A".equals(pSqlValue))
			return Thema.A;
		if ("G".equals(pSqlValue))
			return Thema.G;
		if ("K".equals(pSqlValue))
			return Thema.K;
		if ("L".equals(pSqlValue))
			return Thema.L;
		if ("W".equals(pSqlValue))
			return Thema.W;
		if ("Z".equals(pSqlValue))
			return Thema.Z;

		throw new MatheJungAltException("Ungültiger String " + pSqlValue + ": nur A, G, K, L, W oder Z erlaubt.");
	}

	/**
	 * Wandelt {@link Medienart} in String um.
	 * 
	 * @param pDomainValue {@link Aufgabenart}
	 * @return String
	 * @throws MatheJungAltException
	 */
	public String getSqlValue(Thema pDomainValue) throws MatheJungAltException {
		if (pDomainValue == null)
			throw new MatheJungAltException("Parameter darf nicht null sein");
		return pDomainValue.toString();
	}

}
