package de.egladil.mathejungalt.persistence.db.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.mysql.IJdbcTemplateWrapper;
import de.egladil.mathejungalt.domain.mcraetsel.Urheber;
import de.egladil.mathejungalt.persistence.api.IUrheberRepository;
import de.egladil.mathejungalt.persistence.db.dao.mappers.UrheberRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.sql.IUrheberStmts;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IUrheberTableNames;

public class UrheberDao extends AbstractDao implements IUrheberRepository {

	private static final Logger LOG = LoggerFactory.getLogger(UrheberDao.class);

	private Map<Long, Urheber> urheberMap = new HashMap<Long, Urheber>();

	/**
	 * @param pJdbcTemplateWrapper
	 */
	public UrheberDao(IJdbcTemplateWrapper pJdbcTemplateWrapper, IdGenerator idGenerator) {
		super(pJdbcTemplateWrapper, idGenerator);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IUrheberRepository#findAllUrheber()
	 */
	@Override
	public List<Urheber> findAllUrheber() {
		if (urheberMap.isEmpty()) {
			loadUrheber();
		}
		List<Urheber> result = new ArrayList<Urheber>();
		for (Urheber u : urheberMap.values()) {
			result.add(u);
		}
		return result;
	}

	private void loadUrheber() {
		List<Urheber> liste = getJdbcTemplate().query(IUrheberStmts.SELECT_STMT, new UrheberRowMapper());
		for (Urheber u : liste) {
			urheberMap.put(u.getId(), u);
		}
		LOG.info(liste.size() + " " + IUrheberTableNames.TABLE + " geladen");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IUrheberRepository#findById(java.lang.Long)
	 */
	@Override
	public Urheber findById(Long pId) {
		if (urheberMap.isEmpty()) {
			loadUrheber();
		}
		return urheberMap.get(pId);
	}
}
