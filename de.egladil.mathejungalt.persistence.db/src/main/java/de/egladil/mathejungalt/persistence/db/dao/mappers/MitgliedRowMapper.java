/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMitgliederTableNames;

/**
 * @author winkelv
 */
public class MitgliedRowMapper implements ParameterizedRowMapper<Mitglied> {

	/**
	 *
	 */
	public MitgliedRowMapper() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public Mitglied mapRow(ResultSet pArg0, int pArg1) throws SQLException {
		Mitglied mitglied = new Mitglied();
		mitglied.setId(pArg0.getLong(IMitgliederTableNames.COL_ID));
		mitglied.setSchluessel(pArg0.getString(IMitgliederTableNames.COL_SCHLUESSEL));
		mitglied.setAlter(pArg0.getInt(IMitgliederTableNames.COL_JAHRE));
		mitglied.setErfinderpunkte(pArg0.getInt(IMitgliederTableNames.COL_ERFINDERPUNKTE));
		mitglied.setFototitel(pArg0.getString(IMitgliederTableNames.COL_FOTOTITEL));
		mitglied.setGeburtsjahr(pArg0.getInt(IMitgliederTableNames.COL_GEBURTSJAHR));
		mitglied.setGeburtsmonat(pArg0.getInt(IMitgliederTableNames.COL_GEBURTSMONAT));
		mitglied.setKlasse(pArg0.getInt(IMitgliederTableNames.COL_KLASSE));
		mitglied.setKontakt(pArg0.getString(IMitgliederTableNames.COL_KONTAKT));
		mitglied.setNachname(pArg0.getString(IMitgliederTableNames.COL_NACHNAME));
		mitglied.setVorname(pArg0.getString(IMitgliederTableNames.COL_VORNAME));
		mitglied.setAktiv(pArg0.getInt(IMitgliederTableNames.COL_AKTIV));
		mitglied.setSerienteilnahmenLoaded(false);
		mitglied.setDiplomeLoaded(false);
		return mitglied;
	}

}
