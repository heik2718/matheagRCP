package de.egladil.mathejungalt.persistence.db.dao.sql;

import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMitgliederquellenitemsTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IQuellenTableNames;

/**
 * @author aheike
 */
public interface IQuellenStmts {

	/** */
	public static final String SELECT_Z_QUELLE_BY_ID_STMT = "select " + IQuellenTableNames.COL_SCHLUESSEL + ", "
		+ IQuellenTableNames.COL_ZEITSCHR_ID + ", " + IQuellenTableNames.COL_AUSGABE + ", "
		+ IQuellenTableNames.COL_JAHRGANG + " from " + IQuellenTableNames.TABLE + " where " + IQuellenTableNames.COL_ID
		+ " = ?";

	/** */
	public static final String SELECT_Z_QUELLE_BY_ZEITSCHR_STMT = "select " + IQuellenTableNames.COL_ID + ", "
		+ IQuellenTableNames.COL_ART + ", " + IQuellenTableNames.COL_SCHLUESSEL + ", "
		+ IQuellenTableNames.COL_ZEITSCHR_ID + ", " + IQuellenTableNames.COL_AUSGABE + ", "
		+ IQuellenTableNames.COL_JAHRGANG + " from " + IQuellenTableNames.TABLE + " where "
		+ IQuellenTableNames.COL_ZEITSCHR_ID + " = ?";

	/** */
	public static final String SELECT_Z_QUELLE_BY_ZEITSCHR_AUSG_JAHRG_STMT = "select " + IQuellenTableNames.COL_ID
		+ ", " + IQuellenTableNames.COL_ART + ", " + IQuellenTableNames.COL_SCHLUESSEL + ", "
		+ IQuellenTableNames.COL_ZEITSCHR_ID + ", " + IQuellenTableNames.COL_AUSGABE + ", "
		+ IQuellenTableNames.COL_JAHRGANG + " from " + IQuellenTableNames.TABLE + " where "
		+ IQuellenTableNames.COL_ZEITSCHR_ID + " = ? and " + IQuellenTableNames.COL_AUSGABE + " = ? and "
		+ IQuellenTableNames.COL_JAHRGANG + " = ?";

	/** */
	public static final String SELECT_B_QUELLE_BY_ID_STMT = "select " + IQuellenTableNames.COL_SCHLUESSEL + ", "
		+ IQuellenTableNames.COL_BUCH_ID + ", " + IQuellenTableNames.COL_SEITE + " from " + IQuellenTableNames.TABLE
		+ " where " + IQuellenTableNames.COL_ID + " = ?";

	/** */
	public static final String SELECT_G_QUELLE_BY_ID_STMT = "select i." + IMitgliederquellenitemsTableNames.COL_ID
		+ ", i." + IMitgliederquellenitemsTableNames.COL_MITGLIED_ID + ", i."
		+ IMitgliederquellenitemsTableNames.COL_JAHRE + ", i." + IMitgliederquellenitemsTableNames.COL_KLASSE
		+ " from " + IMitgliederquellenitemsTableNames.TABLE + " i where i."
		+ IMitgliederquellenitemsTableNames.COL_QUELLE_ID + " = ?";

	/** */
	public static final String SELECT_B_QUELLE_BY_BUCH_STMT = "select " + IQuellenTableNames.COL_ID + ", "
		+ IQuellenTableNames.COL_ART + ", " + IQuellenTableNames.COL_SCHLUESSEL + ", " + IQuellenTableNames.COL_BUCH_ID
		+ ", " + IQuellenTableNames.COL_SEITE + " from " + IQuellenTableNames.TABLE + " where "
		+ IQuellenTableNames.COL_BUCH_ID + " = ?";

	/** */
	public static final String SELECT_B_QUELLE_BY_BUCH_SEITE_STMT = "select " + IQuellenTableNames.COL_ID + ", "
		+ IQuellenTableNames.COL_ART + ", " + IQuellenTableNames.COL_SCHLUESSEL + ", " + IQuellenTableNames.COL_BUCH_ID
		+ ", " + IQuellenTableNames.COL_SEITE + " from " + IQuellenTableNames.TABLE + " where "
		+ IQuellenTableNames.COL_BUCH_ID + " = ? and " + IQuellenTableNames.COL_SEITE + " = ?";

	/** */
	public static final String SELECT_K_QUELLE_BY_ID_STMT = "select " + IQuellenTableNames.COL_SCHLUESSEL + ", "
		+ IQuellenTableNames.COL_MITGLIED_ID + ", " + IQuellenTableNames.COL_KLASSE + ", "
		+ IQuellenTableNames.COL_JAHRE + " from " + IQuellenTableNames.TABLE + " where " + IQuellenTableNames.COL_ID
		+ " = ?";

	/** */
	public static final String SELECT_K_QUELLE_BY_KIND_STMT = "select " + IQuellenTableNames.COL_ID + ", "
		+ IQuellenTableNames.COL_ART + ", " + IQuellenTableNames.COL_SCHLUESSEL + ", "
		+ IQuellenTableNames.COL_MITGLIED_ID + ", " + IQuellenTableNames.COL_KLASSE + ", "
		+ IQuellenTableNames.COL_JAHRE + " from " + IQuellenTableNames.TABLE + " where "
		+ IQuellenTableNames.COL_MITGLIED_ID + " = ?";

	/** */
	public static final String SELECT_K_QUELLE_BY_KIND_ALTER_KLASSE_STMT = "select " + IQuellenTableNames.COL_ID + ", "
		+ IQuellenTableNames.COL_ART + ", " + IQuellenTableNames.COL_SCHLUESSEL + ", "
		+ IQuellenTableNames.COL_MITGLIED_ID + ", " + IQuellenTableNames.COL_KLASSE + ", "
		+ IQuellenTableNames.COL_JAHRE + " from " + IQuellenTableNames.TABLE + " where "
		+ IQuellenTableNames.COL_MITGLIED_ID + " = ? and " + IQuellenTableNames.COL_JAHRE + " = ? and "
		+ IQuellenTableNames.COL_KLASSE + " = ?";

	/** */
	public static final String SELECT_N_QUELLE_BY_ID_STMT = "select " + IQuellenTableNames.COL_SCHLUESSEL + " from "
		+ IQuellenTableNames.TABLE + " where " + IQuellenTableNames.COL_ID + " = ?";

	/** */
	public static final String SELECT_N_QUELLE = "select " + IQuellenTableNames.COL_ID + ", "
		+ IQuellenTableNames.COL_SCHLUESSEL + ", " + IQuellenTableNames.COL_ART + " from " + IQuellenTableNames.TABLE
		+ " where " + IQuellenTableNames.COL_ART + " = ?";

	/** */
	public static final String SELECT_MAX_KEY_STMT = "select max(" + IQuellenTableNames.COL_SCHLUESSEL + ") as "
		+ IQuellenTableNames.COL_SCHLUESSEL + " from " + IQuellenTableNames.TABLE;

	/** */
	public static final String INSERT_BUCHQUELLE_STMT = "insert into " + IQuellenTableNames.TABLE + " ("
		+ IQuellenTableNames.COL_SCHLUESSEL + ", " + IQuellenTableNames.COL_ART + ", " + IQuellenTableNames.COL_BUCH_ID
		+ ", " + IQuellenTableNames.COL_SEITE + ", " + IQuellenTableNames.COL_ID + ") values (?, ?, ?, ?, ?)";

	/** */
	public static final String INSERT_GRUPPENQUELLE_STMT = "insert into " + IQuellenTableNames.TABLE + " ("
		+ IQuellenTableNames.COL_SCHLUESSEL + ", " + IQuellenTableNames.COL_ART + ", " + IQuellenTableNames.COL_ID
		+ ") values (?, ?, ?)";

	/** */
	public static final String INSERT_KINDQUELLE_STMT = "insert into " + IQuellenTableNames.TABLE + " ("
		+ IQuellenTableNames.COL_SCHLUESSEL + ", " + IQuellenTableNames.COL_ART + ", "
		+ IQuellenTableNames.COL_MITGLIED_ID + ", " + IQuellenTableNames.COL_KLASSE + ", "
		+ IQuellenTableNames.COL_JAHRE + ", " + IQuellenTableNames.COL_ID + ") values (?, ?, ?, ?, ?, ?)";

	/** */
	public static final String INSERT_ZEITSCHRIFTQUELLE_STMT = "insert into " + IQuellenTableNames.TABLE + " ("
		+ IQuellenTableNames.COL_SCHLUESSEL + ", " + IQuellenTableNames.COL_ART + ", "
		+ IQuellenTableNames.COL_ZEITSCHR_ID + ", " + IQuellenTableNames.COL_AUSGABE + ", "
		+ IQuellenTableNames.COL_JAHRGANG + ", " + IQuellenTableNames.COL_ID + ") values (?, ?, ?, ?, ?, ?)";

	/** */
	public static final String DELETE_STMT = "delete from " + IQuellenTableNames.TABLE + " where "
		+ IQuellenTableNames.COL_ID + " = ?";

	public static final String INSERT_GRUPPENQUELLEN_ITEM = "insert into " + IMitgliederquellenitemsTableNames.TABLE
		+ " (" + IMitgliederquellenitemsTableNames.COL_ID + ", " + IMitgliederquellenitemsTableNames.COL_MITGLIED_ID
		+ ", " + IMitgliederquellenitemsTableNames.COL_QUELLE_ID + ", " + IMitgliederquellenitemsTableNames.COL_KLASSE
		+ ", " + IMitgliederquellenitemsTableNames.COL_JAHRE + ") values (?, ?, ?, ?, ?)";

}
