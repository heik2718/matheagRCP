/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.domain.mitglieder.Serienteilnahme;
import de.egladil.mathejungalt.persistence.api.ISerienRepository;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.ISerienteilnahmenTableNames;

/**
 * @author winkelv
 */
public class SerienteilnahmeRowMapper implements ParameterizedRowMapper<Serienteilnahme> {

	/** */
	private ISerienRepository serienRepository;

	/** */
	private Mitglied mitglied;

	/**
	 * @param pSerienRepository
	 * @param pMitglied
	 */
	public SerienteilnahmeRowMapper(ISerienRepository pSerienRepository, Mitglied pMitglied) {
		super();
		mitglied = pMitglied;
		serienRepository = pSerienRepository;
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public Serienteilnahme mapRow(ResultSet pArg0, int pArg1) throws SQLException {
		Serienteilnahme teilnahme = new Serienteilnahme();
		teilnahme.setId(pArg0.getLong(ISerienteilnahmenTableNames.COL_ID));
		teilnahme.setFruehstarter(pArg0.getDouble(ISerienteilnahmenTableNames.COL_FRUEHSTARTER));
		teilnahme.setPunkte(pArg0.getInt(ISerienteilnahmenTableNames.COL_PUNKTE));

		Long serieId = pArg0.getLong(ISerienteilnahmenTableNames.COL_SERIE_ID);
		Serie serie = serienRepository.findById(serieId);
		teilnahme.setSerie(serie);

		teilnahme.setMitglied(mitglied);
		mitglied.addSerienteilname(teilnahme);
		return teilnahme;
	}

}
