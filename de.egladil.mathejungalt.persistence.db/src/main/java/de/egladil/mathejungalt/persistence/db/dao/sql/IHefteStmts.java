package de.egladil.mathejungalt.persistence.db.dao.sql;

import de.egladil.mathejungalt.persistence.db.dao.tablenames.IHefteTableNames;

/**
 * @author aheike
 */
public interface IHefteStmts {

	/** */
	public static final String SELECT_MAX_KEY_STMT = "select max(" + IHefteTableNames.COL_SCHLUESSEL + ") as "
		+ IHefteTableNames.COL_SCHLUESSEL + " from " + IHefteTableNames.TABLE;

	/** */
	public static final String INSERT_STMT = "insert into " + IHefteTableNames.TABLE + " ("
		+ IHefteTableNames.COL_SCHLUESSEL + ", " + IHefteTableNames.COL_TITEL + ", " + IHefteTableNames.COL_ID
		+ ") values (?, ?, ?)";

	/** */
	public static final String UPDATE_STMT = "update " + IHefteTableNames.TABLE + " set "
		+ IHefteTableNames.COL_SCHLUESSEL + " = ?, " + IHefteTableNames.COL_TITEL + " = ? where "
		+ IHefteTableNames.COL_ID + " = ?";

	public static final String DELETE_STMT = "delete from " + IHefteTableNames.TABLE + " where "
		+ IHefteTableNames.COL_ID + " = ? ";

}
