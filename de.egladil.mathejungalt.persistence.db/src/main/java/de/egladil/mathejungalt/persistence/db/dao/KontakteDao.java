/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.persistence.db.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.mysql.IJdbcTemplateWrapper;
import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.domain.schulen.Schule;
import de.egladil.mathejungalt.domain.schulen.Schulkontakt;
import de.egladil.mathejungalt.persistence.api.IKontakteRepository;
import de.egladil.mathejungalt.persistence.api.ISchulenRepository;
import de.egladil.mathejungalt.persistence.db.dao.mappers.KontaktRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.LongRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.SchluesselRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.SchulkontakteRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.sql.IKontakteStmts;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IKontakteTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.ISchulkontakteTableNames;
import de.egladil.mathejungalt.persistence.db.dao.util.SqlQueryUtil;

/**
 * <p>
 * NOTE: THIS WIDGET AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 * 
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class KontakteDao extends AbstractDao implements IKontakteRepository {

	private static final Logger LOG = LoggerFactory.getLogger(KontakteDao.class);

	private Map<Long, Kontakt> kontakteMap;

	private ISchulenRepository schulenRepository;

	/**
	 * @param pJdbcTemplateWrapper
	 */
	public KontakteDao(IJdbcTemplateWrapper pJdbcTemplateWrapper, IdGenerator idGenerator) {
		super(pJdbcTemplateWrapper, idGenerator);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IKontakteRepository#findAllKontakte()
	 */
	@Override
	public List<Kontakt> findAllKontakte() {
		if (kontakteMap == null) {
			loadKontakte();
		}
		List<Kontakt> result = new ArrayList<Kontakt>();
		for (Kontakt k : kontakteMap.values()) {
			result.add(k);
		}
		return result;
	}

	private void loadKontakte() {
		List<Kontakt> liste = getJdbcTemplate().query(IKontakteStmts.SELECT_STMT, new KontaktRowMapper());
		kontakteMap = new HashMap<Long, Kontakt>();
		for (Kontakt k : liste) {
			kontakteMap.put(k.getId(), k);
		}
		LOG.info(liste.size() + " Kontakt(e) geladen");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IKontakteRepository#saveKontakt(de.egladil.mathejungalt.domain.schulen
	 * .Kontakt)
	 */
	@Override
	public void saveKontakt(Kontakt pKontakt) {
		if (pKontakt == null) {
			throw new MatheJungAltException("null an saveKontakt() uebergeben");
		}
		if (pKontakt.isNew()) {
			persistKontakt(pKontakt);
		} else {
			mergeKontakt(pKontakt);
		}
	}

	private void persistKontakt(final Kontakt pKontakt) {
		if (kontakteMap == null) {
			loadKontakte();
		}
		final Long kontaktId = getIdGenerator().nextId(IKontakteTableNames.TABLE);
		getTransactionTemplate().execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(TransactionStatus pArg0) {
				int abo = pKontakt.isAbo() ? 1 : 0;
				getJdbcTemplate().update(IKontakteStmts.INSERT_STMT,
					new Object[] { pKontakt.getSchluessel(), pKontakt.getName(), pKontakt.getEmail(), abo, kontaktId });
				pKontakt.setId(kontaktId);

				for (Schulkontakt schulkontakt : pKontakt.getSchulkontakte()) {
					persistSchulkontakt(schulkontakt);
				}

			}
		});
		kontakteMap.put(kontaktId, pKontakt);
		getIdGenerator().increase(IKontakteTableNames.TABLE);
	}

	private void mergeKontakt(final Kontakt pKontakt) {
		final Map<Schulkontakt, SqlQueryUtil.ACTION> actionMap = initSchulkontakteActionMap(pKontakt);
		getTransactionTemplate().execute(new TransactionCallbackWithoutResult() {
			@Override
			protected void doInTransactionWithoutResult(TransactionStatus pArg0) {
				int abo = pKontakt.isAbo() ? 1 : 0;
				getJdbcTemplate().update(
					IKontakteStmts.UPDATE_STMT,
					new Object[] { pKontakt.getSchluessel(), pKontakt.getName(), pKontakt.getEmail(), abo,
						pKontakt.getId() });

				for (Schulkontakt schulkontakt : actionMap.keySet()) {
					SqlQueryUtil.ACTION action = actionMap.get(schulkontakt);
					switch (action) {
					case DELETE:
						removeSchulkontakt(schulkontakt);
						break;
					case INSERT:
						persistSchulkontakt(schulkontakt);
						break;
					default:
						break;
					}
				}
			}
		});

	}

	/**
	 * @param pKontakt
	 * @return
	 */
	private Map<Schulkontakt, SqlQueryUtil.ACTION> initSchulkontakteActionMap(Kontakt pKontakt) {
		Map<Schulkontakt, SqlQueryUtil.ACTION> map = new HashMap<Schulkontakt, SqlQueryUtil.ACTION>();
		List<Long> idList = getJdbcTemplate().query(IKontakteStmts.SELECT_SCHULKONTAKT_ID_ZU_KONTAKT_STMT,
			new LongRowMapper(), new Object[] { pKontakt.getId() });
		List<Long> schulkontaktIDs = new ArrayList<Long>();
		// INSERTs und UPDATEs
		for (Schulkontakt item : pKontakt.getSchulkontakte()) {
			schulkontaktIDs.add(item.getId());
			if (item.isNew()) {
				map.put(item, SqlQueryUtil.ACTION.INSERT);
			} else {
				if (idList.contains(item.getId())) {
					map.put(item, SqlQueryUtil.ACTION.UPDATE);
				}
			}
		}
		// Deletes sind die, die nicht mehr in der SerieItemsIDs-Liste sind
		for (Long dbID : idList) {
			if (!schulkontaktIDs.contains(dbID)) {
				Schulkontakt tempItem = new Schulkontakt();
				tempItem.setId(dbID);
				map.put(tempItem, SqlQueryUtil.ACTION.DELETE);
			}
		}
		return map;
	}

	/**
	 * @param pSchulkontakt
	 */
	private void persistSchulkontakt(Schulkontakt pSchulkontakt) {
		final Long id = getIdGenerator().nextId(ISchulkontakteTableNames.TABLE);
		getJdbcTemplate().update(IKontakteStmts.INSERT_SCHULKONTAKT_STMT,
			new Object[] { pSchulkontakt.getSchule().getId(), pSchulkontakt.getKontakt().getId(), id });
		pSchulkontakt.setId(id);
		getIdGenerator().increase(ISchulkontakteTableNames.TABLE);

	}

	private void removeSchulkontakt(Schulkontakt pSchulkontakt) {
		getJdbcTemplate().update(IKontakteStmts.DELETE_SCHULKONTAKT_STMT, new Object[] { pSchulkontakt.getId() });

	}

	private void mergeSchulkontakt(Schulkontakt pSchulkontakt) {
		// sollte nichts zu mergen geben, weil es noch keine weiteren Attribute gibt.
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IKontakteRepository#loadSchulkontakte(de.egladil.mathejungalt.domain.
	 * schulen.Kontakt)
	 */
	@Override
	public void loadSchulkontakte(Kontakt pKontakt) {
		if (pKontakt == null || pKontakt.getId() == null) {
			throw new MatheJungAltException(SerienDao.class.getName()
				+ ".loadSchulkontakte(): Parameter ist null oder Parameter.ID ist null");
		}
		if (pKontakt.isSchulkontakteGeladen()) {
			return;
		}
		if (kontakteMap == null) {
			loadKontakte();
		}
		if (!kontakteMap.containsValue(pKontakt)) {
			throw new MatheJungAltException(SchulenDao.class.getName()
				+ ".loadSchulkontakte(): schwerwiegender Programmfehler: es gibt den Kontakt " + pKontakt.getEmail()
				+ " nicht im Repository!");
		}
		LOG.info("Lade Schulkontakte zu Kontakt [" + pKontakt.getName() + "]");
		SchulkontakteRowMapper rowMapper = new SchulkontakteRowMapper(schulenRepository, pKontakt);
		List<Schulkontakt> schulen = getJdbcTemplate().query(IKontakteStmts.SELECT_SCHULEN_ZU_KONTAKT_STMT, rowMapper,
			new Object[] { pKontakt.getId() });
		pKontakt.setSchulkontakteGeladen(true);
		if (LOG.isDebugEnabled()) {
			LOG.debug(schulen.size() + " Schulkontakte(n) zur geladen");
		}
	}

	/**
	 * @param pSchulenRepository the schulenRepository to set
	 */
	public final void setSchulenRepository(ISchulenRepository pSchulenRepository) {
		schulenRepository = pSchulenRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IKontakteRepository#getMaxKey()
	 */
	@Override
	public String getMaxKey() {
		return getJdbcTemplate().queryForObject(IKontakteStmts.SELECT_MAX_KEY_STMT, new SchluesselRowMapper());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IKontakteRepository#findKontakteForSchule(de.egladil.mathejungalt.domain
	 * .schulen.Schule )
	 */
	@Override
	public List<Kontakt> findKontakteForSchule(Schule pSchule) {
		Assert.isNotNull(pSchule);
		return getJdbcTemplate().query(IKontakteStmts.FIND_KONTAKTE_FOR_SCHULE, new KontaktRowMapper(),
			new Object[] { pSchule.getId() });
	}
}
