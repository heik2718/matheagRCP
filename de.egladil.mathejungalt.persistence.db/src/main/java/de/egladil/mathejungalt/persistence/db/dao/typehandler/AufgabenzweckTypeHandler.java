/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao.typehandler;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenzweck;
import de.egladil.mathejungalt.domain.types.EnumTypes.Medienart;

/**
 * @author Winkelv
 */
public class AufgabenzweckTypeHandler {

	/**
	 * 
	 */
	public AufgabenzweckTypeHandler() {
	}

	/**
	 * Wandelt String in Medienart um.
	 * 
	 * @param pSqlValue
	 * @return {@link Medienart}
	 * @throws MatheJungAltException
	 */
	public Aufgabenzweck getDomainValue(final String pSqlValue) throws MatheJungAltException {
		if (pSqlValue == null)
			throw new MatheJungAltException("Parameter darf nicht null sein");
		if ("K".equals(pSqlValue))
			return Aufgabenzweck.K;
		if ("S".equals(pSqlValue))
			return Aufgabenzweck.S;
		if ("M".equals(pSqlValue))
			return Aufgabenzweck.M;
		throw new MatheJungAltException("Ungültiger String " + pSqlValue + ": nur K, M oder S erlaubt.");
	}

	/**
	 * Wandelt {@link Medienart} in String um.
	 * 
	 * @param pDomainValue {@link Aufgabenzweck}
	 * @return String
	 * @throws MatheJungAltException
	 */
	public String getSqlValue(Aufgabenzweck pDomainValue) throws MatheJungAltException {
		if (pDomainValue == null)
			throw new MatheJungAltException("Parameter darf nicht null sein");
		return pDomainValue.toString();
	}

}
