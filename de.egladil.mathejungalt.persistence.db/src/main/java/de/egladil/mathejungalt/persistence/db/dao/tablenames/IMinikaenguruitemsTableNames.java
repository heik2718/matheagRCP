/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.tablenames;

/**
 * @author Winkelv
 */
public interface IMinikaenguruitemsTableNames extends ICommonColumnNamesOhneSchluessel {

	/** */
	public static final String TABLE = "minikaenguruitems";

	/** */
	public static final String COL_NUMMER = "nummer";

	/** */
	public static final String COL_PUNKTE = "punkte";

	/** */
	public static final String COL_MINIKAENGURU_ID = "mini_id";

	/** */
	public static final String COL_AUFGABE_ID = "aufgabe_id";
}
