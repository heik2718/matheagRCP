/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.sql;

import de.egladil.mathejungalt.persistence.db.dao.tablenames.IKontakteTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.ISchulkontakteTableNames;

/**
 * @author aheike
 */
public interface IKontakteStmts {

	public static final String SELECT_MAX_KEY_STMT = "select max(" + IKontakteTableNames.COL_SCHLUESSEL + ") as "
		+ IKontakteTableNames.COL_SCHLUESSEL + " from " + IKontakteTableNames.TABLE;

	/** */
	public static final String SELECT_STMT = "select k." + IKontakteTableNames.COL_ID + ", k."
		+ IKontakteTableNames.COL_SCHLUESSEL + ", k." + IKontakteTableNames.COL_NAME + ", k."
		+ IKontakteTableNames.COL_EMAIL + ", k." + IKontakteTableNames.COL_ABO + " from " + IKontakteTableNames.TABLE
		+ " k";

	/** */
	public static final String INSERT_STMT = "insert into " + IKontakteTableNames.TABLE + " ("
		+ IKontakteTableNames.COL_SCHLUESSEL + ", " + IKontakteTableNames.COL_NAME + ", "
		+ IKontakteTableNames.COL_EMAIL + ", " + IKontakteTableNames.COL_ABO + ", " + IKontakteTableNames.COL_ID
		+ ") values (?, ?, ?, ?, ?)";

	/** */
	public static final String UPDATE_STMT = "update " + IKontakteTableNames.TABLE + " set "
		+ IKontakteTableNames.COL_SCHLUESSEL + " = ?, " + IKontakteTableNames.COL_NAME + " = ?, "
		+ IKontakteTableNames.COL_EMAIL + " = ?, " + IKontakteTableNames.COL_ABO + " = ? " + "where "
		+ IKontakteTableNames.COL_ID + " = ?";

	/* select sk.id, sk.schule_id, sk.kontakt_id from schulen_kontakte sk where sk.kontakt_id = 10 */
	public static final String SELECT_SCHULEN_ZU_KONTAKT_STMT = "select k." + ISchulkontakteTableNames.COL_ID + ", k."
		+ ISchulkontakteTableNames.COL_SCHULE_ID + ", k." + ISchulkontakteTableNames.COL_KONTAKT_ID + " from "
		+ ISchulkontakteTableNames.TABLE + " k where k." + ISchulkontakteTableNames.COL_KONTAKT_ID + " = ?";

	public static final String SELECT_SCHULKONTAKT_ID_ZU_KONTAKT_STMT = "select k." + ISchulkontakteTableNames.COL_ID
		+ " from " + ISchulkontakteTableNames.TABLE + " k where k." + ISchulkontakteTableNames.COL_KONTAKT_ID + " = ?";

	public static final String INSERT_SCHULKONTAKT_STMT = "insert into " + ISchulkontakteTableNames.TABLE + " ("
		+ ISchulkontakteTableNames.COL_SCHULE_ID + ", " + ISchulkontakteTableNames.COL_KONTAKT_ID + ", "
		+ ISchulkontakteTableNames.COL_ID + ") values (?,?,?)";

	public static final String DELETE_SCHULKONTAKT_STMT = "delete from " + ISchulkontakteTableNames.TABLE + " where "
		+ ISchulkontakteTableNames.COL_ID + " = ?";

	public static final String FIND_KONTAKTE_FOR_SCHULE = "select k." + IKontakteTableNames.COL_ID + ", k."
		+ IKontakteTableNames.COL_SCHLUESSEL + ", k." + IKontakteTableNames.COL_NAME + ", k."
		+ IKontakteTableNames.COL_EMAIL + ", k." + IKontakteTableNames.COL_ABO + " from " + IKontakteTableNames.TABLE
		+ " k, " + ISchulkontakteTableNames.TABLE + " sk where k." + IKontakteTableNames.COL_ID + " = sk."
		+ ISchulkontakteTableNames.COL_KONTAKT_ID + " and sk." + ISchulkontakteTableNames.COL_SCHULE_ID + " = ?";

}
