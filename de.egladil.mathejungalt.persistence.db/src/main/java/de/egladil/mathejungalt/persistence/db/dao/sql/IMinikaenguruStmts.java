/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao.sql;

import de.egladil.mathejungalt.persistence.db.dao.tablenames.IKontakteTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMinikaenguruTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMinikaenguruitemsTableNames;

/**
 * @author aheike
 */
public interface IMinikaenguruStmts {

	/** */
	public static final String SELECT_STMT = "select " + IMinikaenguruTableNames.COL_ID + ", "
		+ IMinikaenguruTableNames.COL_JAHR + ", " + IMinikaenguruTableNames.COL_BEENDET + " from "
		+ IMinikaenguruTableNames.TABLE;

	/** */
	public static final String SELECT_ITEMS_ZU_MINIKAENGURU_STMT = "select " + IMinikaenguruitemsTableNames.COL_ID
		+ ", " + IMinikaenguruitemsTableNames.COL_AUFGABE_ID + ", " + IMinikaenguruitemsTableNames.COL_NUMMER
		+ " from " + IMinikaenguruitemsTableNames.TABLE + " where " + IMinikaenguruitemsTableNames.COL_MINIKAENGURU_ID
		+ " = ?";

	/**
	 * 
	 */
	public static final String SELECT_MINI_ID_ZU_AUFGABE = "select " + IMinikaenguruitemsTableNames.COL_MINIKAENGURU_ID
		+ " from " + IMinikaenguruitemsTableNames.TABLE + " where " + IMinikaenguruitemsTableNames.COL_AUFGABE_ID
		+ " = ?";

	/** */
	public static final String SELECT_MAX_KEY_STMT = "select max(" + IMinikaenguruTableNames.COL_JAHR + ") from "
		+ IMinikaenguruTableNames.TABLE;

	/** */
	public static final String SELECT_MINIKAENGURUITEM_ID_ZU_MINIKAENGURU_STMT = "select "
		+ IMinikaenguruitemsTableNames.COL_ID + " from " + IMinikaenguruitemsTableNames.TABLE + " where "
		+ IMinikaenguruitemsTableNames.COL_MINIKAENGURU_ID + " = ?";

	/** */
	public static final String INSERT_MINIKAENGURU_STMT = "insert into " + IMinikaenguruTableNames.TABLE + " ("
		+ IMinikaenguruTableNames.COL_JAHR + ", " + IMinikaenguruTableNames.COL_BEENDET + ", "
		+ IMinikaenguruTableNames.COL_ID + " ) values (?, ?, ?)";

	/** */
	public static final String UPDATE_MINIKAENGURU_STMT = "update " + IMinikaenguruTableNames.TABLE + " set "
		+ IMinikaenguruTableNames.COL_JAHR + " = ?, " + IMinikaenguruTableNames.COL_BEENDET + " = ? where "
		+ IMinikaenguruTableNames.COL_ID + " = ?";

	/** */
	public static final String INSERT_MINIKAENGURUITEM_STMT = "insert into " + IMinikaenguruitemsTableNames.TABLE
		+ " (" + IMinikaenguruitemsTableNames.COL_MINIKAENGURU_ID + ", " + IMinikaenguruitemsTableNames.COL_AUFGABE_ID
		+ ", " + IMinikaenguruitemsTableNames.COL_NUMMER + ", " + IMinikaenguruitemsTableNames.COL_ID
		+ " ) values (?, ?, ?, ?)";

	/** */
	public static final String UPDATE_MINIKAENGURUITEM_STMT = "update " + IMinikaenguruitemsTableNames.TABLE + " set "
		+ IMinikaenguruitemsTableNames.COL_NUMMER + " = ? where " + IMinikaenguruitemsTableNames.COL_ID + " = ?";

	public static final String DELETE_SMINIKAENGURU_STMT = "delete from " + IMinikaenguruTableNames.TABLE + " where "
		+ IMinikaenguruTableNames.COL_ID + " = ?";

	public static final String DELETE_MINIKAENGURUITEM_STMT = "delete from " + IMinikaenguruitemsTableNames.TABLE
		+ " where " + IMinikaenguruitemsTableNames.COL_ID + " = ?";

	public static final String INSERT_KONTAKT_MAILINGLISTE = "insert into minimails (mini_id, kontakt_id) values (?,?)";

	public static final String DELETE_KONTAKT_MAILINGLISTE = "delete from minimails where mini_id = ? and kontakt_id = ?";

	public static final String SELECT_MAILINGLISTE_STMT = "select k." + IKontakteTableNames.COL_ID + ", k."
		+ IKontakteTableNames.COL_SCHLUESSEL + ", k." + IKontakteTableNames.COL_NAME + ", k."
		+ IKontakteTableNames.COL_EMAIL + ", k." + IKontakteTableNames.COL_ABO + " from " + IKontakteTableNames.TABLE
		+ " k, minimails m where k." + IKontakteTableNames.COL_ID + " = m.kontakt_id and m.mini_id = ?";

}
