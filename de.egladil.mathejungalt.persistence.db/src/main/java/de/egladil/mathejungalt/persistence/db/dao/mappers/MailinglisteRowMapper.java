/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import de.egladil.mathejungalt.domain.mitglieder.EmailAdresse;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMailinglisteTableNames;

/**
 * @author winkelv
 */
public class MailinglisteRowMapper implements ParameterizedRowMapper<EmailAdresse> {

	/**
	 * 
	 */
	public MailinglisteRowMapper() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public EmailAdresse mapRow(ResultSet pArg0, int pArg1) throws SQLException {
		EmailAdresse adresse = new EmailAdresse();
		adresse.setId(pArg0.getLong(IMailinglisteTableNames.COL_ID));
		adresse.setEmail(pArg0.getString(IMailinglisteTableNames.COL_EMAIL));
		return adresse;
	}

}
