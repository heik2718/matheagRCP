/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.tablenames;

/**
 * @author Winkelv
 */
public interface IAufgabenTableNames extends IColumnNamesMitSchluessel {

	/** */
	final static String TABLE = "aufgaben";

	/** */
	final static String COL_THEMA = "thema";

	/** */
	final static String COL_STUFE = "stufe";

	/** */
	final static String COL_JAHRESZEIT = "jahreszeit";

	/** */
	final static String COL_TITEL = "titel";

	/** */
	final static String COL_HEFT_ID = "heft_id";

	/** */
	final static String COL_HEFT_SCHLUESSEL = "heft_schluessel";

	/** */
	final static String COL_HEFT_TITEL = "heft_titel";

	/** */
	final static String COL_HEFT_BEENDET = "heft_beendet";

	/** */
	final static String COL_QUELLE_ID = "quelle_id";

	/** */
	final static String COL_QUELLE_ART = "quelle_art";

	/** */
	final static String COL_BESCHREIBUNG = "beschreibung";

	/** */
	final static String COL_ART = "art";

	/** */
	final static String COL_ZWECK = "zweck";

	final static String COL_ABGELAUFEN_SERIE = "abgelaufen_serie";

	final static String COL_VERZEICHNIS = "verzeichnis";
}
