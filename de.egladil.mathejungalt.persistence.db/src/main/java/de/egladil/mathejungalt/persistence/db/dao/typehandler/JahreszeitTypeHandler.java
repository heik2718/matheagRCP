/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao.typehandler;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenart;
import de.egladil.mathejungalt.domain.types.EnumTypes.Jahreszeit;
import de.egladil.mathejungalt.domain.types.EnumTypes.Medienart;

/**
 * @author Winkelv
 */
public class JahreszeitTypeHandler {

	/**
	 * 
	 */
	public JahreszeitTypeHandler() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Wandelt String in Medienart um.
	 * 
	 * @param pSqlValue
	 * @return {@link Medienart}
	 * @throws MatheJungAltException
	 */
	public Jahreszeit getDomainValue(final String pSqlValue) throws MatheJungAltException {
		if (pSqlValue == null)
			throw new MatheJungAltException("Parameter darf nicht null sein");
		if ("F".equals(pSqlValue))
			return Jahreszeit.F;
		if ("H".equals(pSqlValue))
			return Jahreszeit.H;
		if ("I".equals(pSqlValue))
			return Jahreszeit.I;
		if ("K".equals(pSqlValue))
			return Jahreszeit.K;
		if ("O".equals(pSqlValue))
			return Jahreszeit.O;
		if ("S".equals(pSqlValue))
			return Jahreszeit.S;
		if ("W".equals(pSqlValue))
			return Jahreszeit.W;
		if ("X".equals(pSqlValue))
			return Jahreszeit.X;

		throw new MatheJungAltException("Ungültiger String " + pSqlValue + ": nur F, H, I, K, O, S, W oder X erlaubt.");
	}

	/**
	 * Wandelt {@link Medienart} in String um.
	 * 
	 * @param pDomainValue {@link Aufgabenart}
	 * @return String
	 * @throws MatheJungAltException
	 */
	public String getSqlValue(Jahreszeit pDomainValue) throws MatheJungAltException {
		if (pDomainValue == null)
			throw new MatheJungAltException("Parameter darf nicht null sein");
		return pDomainValue.toString();
	}

}
