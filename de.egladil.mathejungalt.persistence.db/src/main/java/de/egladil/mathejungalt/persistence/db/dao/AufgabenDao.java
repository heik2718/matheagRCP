/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.logging.DebugPrinter;
import de.egladil.base.mysql.IJdbcTemplateWrapper;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgaben.Heft;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.persistence.api.IAufgabenRepository;
import de.egladil.mathejungalt.persistence.api.IHefteRepository;
import de.egladil.mathejungalt.persistence.api.IQuellenRepository;
import de.egladil.mathejungalt.persistence.db.dao.mappers.AufgabeRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.LongRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.SchluesselRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.sql.IAufgabenStmts;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IAufgabenTableNames;
import de.egladil.mathejungalt.persistence.db.dao.typehandler.AufgabenArtTypeHandler;
import de.egladil.mathejungalt.persistence.db.dao.typehandler.AufgabenzweckTypeHandler;
import de.egladil.mathejungalt.persistence.db.dao.typehandler.JahreszeitTypeHandler;
import de.egladil.mathejungalt.persistence.db.dao.typehandler.ThemaTypeHandler;

/**
 * @author Winkelv
 */
public class AufgabenDao extends AbstractDao implements IAufgabenRepository {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(AufgabenDao.class);

	/** */
	private IQuellenRepository quellenRepository;

	/** */
	private IHefteRepository hefteRepository;

	/** */
	private Map<Long, Aufgabe> aufgabenMap;

	/**
	 * @param pJdbcTemplateWrapper
	 */
	public AufgabenDao(IJdbcTemplateWrapper pJdbcTemplateWrapper, IdGenerator idGenerator) {
		super(pJdbcTemplateWrapper, idGenerator);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.aufgaben.IAufgabenRepository#findAll()
	 */
	@Override
	public List<Aufgabe> findAll() {
		if (aufgabenMap == null) {
			loadAufgaben(null);
		}
		List<Aufgabe> liste = new ArrayList<Aufgabe>();
		Iterator<Long> iter = aufgabenMap.keySet().iterator();
		while (iter.hasNext())
			liste.add(aufgabenMap.get(iter.next()));
		return liste;
	}

	/**
	 * @param pNewParam TODO
	 */
	private void loadAufgaben(Object pNewParam) {
		List<Heft> hefte = new ArrayList<Heft>();
		aufgabenMap = new HashMap<Long, Aufgabe>();
		List<Aufgabe> liste = getJdbcTemplate().query(IAufgabenStmts.SELECT_STMT,
			new AufgabeRowMapper(quellenRepository, hefteRepository));
		fillHefteListAndAufgabenMap(hefte, liste);
		LOG.info(liste.size() + " " + IAufgabenTableNames.TABLE + " geladen");
		for (Heft heft : hefte) {
			heft.setAufgabenGeladen(true);
		}
	}

	/**
	 * Fügt die Aufgaben der gegebenen Liste der aufgabenMap hinzu und füllt die hefteListe mit Heften auf, die noch
	 * nicht enthalten sind
	 *
	 * @param pHefteListe
	 * @param pAufgabenListe
	 */
	private void fillHefteListAndAufgabenMap(List<Heft> pHefteListe, List<Aufgabe> pAufgabenListe) {
		for (Aufgabe aufgabe : pAufgabenListe) {
			aufgabe.setGesperrtFuerArbeitsblatt(null);
			aufgabe.setGesperrtFuerWettbewerb(null);
			aufgabenMap.put(aufgabe.getId(), aufgabe);
			Heft heft = aufgabe.getHeft();
			if (heft != null && !pHefteListe.contains(heft)) {
				pHefteListe.add(aufgabe.getHeft());
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.core.persistenceapi.aufgaben.IAufgabenRepository#loadAufgabenZuHeft(de.egladil.mathe.core.domain
	 * . aufgabensammlungen.hefte.Heft)
	 */
	@Override
	public void loadAufgabenZuHeft(Heft pHeft) {
		if (pHeft == null || pHeft.getId() == null)
			throw new MatheJungAltException(AufgabenDao.class.getName()
				+ ".loadAufgabenZuHeft(): Parameter oder Parameter.ID ist null!");
		// in diesem Fall (da es wenige Hefte mit jeweils vielen Aufgaben sind, reicht es, alle Aufgaben zu laden. Dann
		// sind die
		// Aufgaben in den Heften ebenfalls geladen
		if (aufgabenMap == null)
			loadAufgaben(null);
		if (hefteRepository.findById(pHeft.getId()) == null)
			throw new MatheJungAltException(AufgabenDao.class.getName() + ".loadAufgabenZuHeft(): Das Heft mit der Id "
				+ pHeft.getId() + " existiert nicht!");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.aufgaben.IAufgabenRepository#findByAttributeMap(java.util.Map, boolean,
	 * boolean)
	 */
	@Override
	public List<Aufgabe> findByAttributeMap(Map<String, Object> pAttributeMap, boolean pExact, boolean pAnd) {
		throw new MatheJungAltException("Bisher gab es keine Verwendung für diese Methode. Daher nicht implementiert");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.aufgaben.IAufgabenRepository#findById(long)
	 */
	@Override
	public Aufgabe findById(long pId) {
		if (aufgabenMap == null) {
			loadAufgaben(null);
		}
		if (!aufgabenMap.containsKey(pId)) {
			throw new MatheJungAltException("Es gibt keine Aufgabe mit der Id " + pId);
		}
		Aufgabe aufgabe = aufgabenMap.get(pId);
		if (!aufgabe.getQuelle().isCompletelyLoaded()) {
			quellenRepository.completeQuelle(aufgabe.getQuelle());
		}
		return aufgabenMap.get(pId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.aufgaben.IAufgabenRepository#findByKey(java.lang.String)
	 */
	@Override
	public Aufgabe findByKey(String pString) {
		if (pString == null) {
			String msg = MatheJungAltException.argumentNullMessage("findByKey(String");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (aufgabenMap == null) {
			loadAufgaben(null);
		}
		Long id = getJdbcTemplate().queryForLong(IAufgabenStmts.SELECT_ID_BY_SCHLUESSEL_STMT, new Object[] { pString });
		return aufgabenMap.get(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.aufgaben.IAufgabenRepository#getMaxKey()
	 */
	@Override
	public String getMaxKey() {
		return getJdbcTemplate().queryForObject(IAufgabenStmts.SELECT_MAX_KEY_STMT, new SchluesselRowMapper());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.persistenceapi.aufgaben.IAufgabenRepository#save(de.egladil.mathe.core.domain.aufgaben.
	 * AbstractAufgabe)
	 */
	@Override
	public void save(Aufgabe pAufgabe) {
		if (pAufgabe == null)
			throw new MatheJungAltException("null an save(Aufgabe) uebergeben");
		if (pAufgabe.isNew()) {
			persist(pAufgabe);
		} else {
			merge(pAufgabe);
		}

	}

	/**
	 * Neu anlegen (insert)
	 *
	 * @param pAufgabe
	 */
	private void persist(final Aufgabe pAufgabe) {
		if (aufgabenMap == null)
			loadAufgaben(null);
		final Long id = getIdGenerator().nextId(IAufgabenTableNames.TABLE);
		getTransactionTemplate().execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(TransactionStatus pArg0) {
				quellenRepository.save(pAufgabe.getQuelle());
				if (pAufgabe.getHeft() != null)
					hefteRepository.save(pAufgabe.getHeft());

				Long heftId = pAufgabe.getHeft() == null ? null : pAufgabe.getHeft().getId();

				Object[] args = new Object[] { pAufgabe.getSchluessel(),
					new ThemaTypeHandler().getSqlValue(pAufgabe.getThema()), pAufgabe.getStufe(),
					new JahreszeitTypeHandler().getSqlValue(pAufgabe.getJahreszeit()),
					new AufgabenArtTypeHandler().getSqlValue(pAufgabe.getArt()),
					new AufgabenzweckTypeHandler().getSqlValue(pAufgabe.getZweck()), pAufgabe.getTitel(), heftId,
					pAufgabe.getQuelle().getId(), pAufgabe.getBeschreibung(), pAufgabe.getZuSchlechtFuerSerie(),
					pAufgabe.getVerzeichnis(), id };
				try {
					getJdbcTemplate().update(IAufgabenStmts.INSERT_STMT, args);
				} catch (DataAccessException e) {
					DebugPrinter debugPrinter = new DebugPrinter(pAufgabe, 1);
					String msg = "DataAccessException beim Speichern der Aufgabe. ";
					LOG.error(msg + debugPrinter.dump());
					throw e;
				}
			}
		});
		pAufgabe.setId(id);
		aufgabenMap.put(id, pAufgabe);
		getIdGenerator().increase(IAufgabenTableNames.TABLE);
	}

	/**
	 * �ndern (update)
	 *
	 * @param pAufgabe
	 */
	private void merge(final Aufgabe pAufgabe) {
		getTransactionTemplate().execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(TransactionStatus pArg0) {
				quellenRepository.save(pAufgabe.getQuelle());

				Long heftId = pAufgabe.getHeft() == null ? null : pAufgabe.getHeft().getId();

				Object[] args = new Object[] { pAufgabe.getSchluessel(),
					new ThemaTypeHandler().getSqlValue(pAufgabe.getThema()), pAufgabe.getStufe(),
					new JahreszeitTypeHandler().getSqlValue(pAufgabe.getJahreszeit()),
					new AufgabenArtTypeHandler().getSqlValue(pAufgabe.getArt()),
					new AufgabenzweckTypeHandler().getSqlValue(pAufgabe.getZweck()), pAufgabe.getTitel(), heftId,
					pAufgabe.getQuelle().getId(), pAufgabe.getBeschreibung(), pAufgabe.getZuSchlechtFuerSerie(),
					pAufgabe.getVerzeichnis(), pAufgabe.getId() };
				getJdbcTemplate().update(IAufgabenStmts.UPDATE_STMT, args);
			}
		});
	}

	public void remove(Aufgabe pAufgabe) {
		if (pAufgabe == null || pAufgabe.isNew())
			return;
		getJdbcTemplate().update(IAufgabenStmts.DELETE_STMT, new Object[] { pAufgabe.getId() });
		if (aufgabenMap != null)
			aufgabenMap.remove(pAufgabe.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.persistenceapi.aufgaben.IAufgabenRepository#anzahlSerienMitAufgabe(de.egladil.mathe.core
	 * .domain.aufgaben .Aufgabe)
	 */
	@Override
	public int anzahlMinikaenguruMitAufgabe(Aufgabe pAufgabe) {
		String stmt = IAufgabenStmts.SELECT_ANZAHL_MINIKAENGURUITEM;
		Object[] params = new Object[] { pAufgabe.getId() };
		return getJdbcTemplate().queryForInt(stmt, params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IAufgabenRepository#serienIdMitAufgabe(de.egladil.mathejungalt.domain
	 * .aufgaben.Aufgabe)
	 */
	@Override
	public List<Long> serienIdMitAufgabe(Aufgabe pAufgabe) {
		String stmt = IAufgabenStmts.FIND_SERIE_ID_BY_AUFGABE;
		Object[] params = new Object[] { pAufgabe.getId() };
		List<Long> result = getJdbcTemplate().query(stmt, new LongRowMapper(), params);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IAufgabenRepository#anzahlSperrendeArbeitsblaetterMitAufgabe(de.egladil
	 * .mathejungalt.domain.aufgaben.Aufgabe)
	 */
	public int anzahlSperrendeArbeitsblaetterMitAufgabe(Aufgabe pAufgabe) {
		String stmt = IAufgabenStmts.SELECT_ANZAHL_ARBEITSBLAETTER;
		return getJdbcTemplate().queryForInt(stmt, new Object[] { Arbeitsblatt.SPERREND, pAufgabe.getId() });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.persistenceapi.aufgaben.IAufgabenRepository#setQuellenRepository(de.egladil.mathe.core.
	 * persistenceapi .quellen.IQuellenRepository)
	 */
	@Override
	public void setQuellenRepository(IQuellenRepository pQuellenRepository) {
		quellenRepository = pQuellenRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.core.persistenceapi.aufgaben.IAufgabenRepository#setHefteRepository(de.egladil.mathe.core.
	 * persistenceapi .aufgabensammlungen.IHefteRepository)
	 */
	@Override
	public void setHefteRepository(IHefteRepository pHefteRepository) {
		hefteRepository = pHefteRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IAufgabenRepository#switchZuSchlechtFuerSerieAendern(de.egladil.mathejungalt
	 * .domain .aufgaben.Aufgabe)
	 */
	@Override
	public void changeZuSchlechtFuerSerie(Aufgabe pAufgabe) {
		Assert.isNotNull(pAufgabe);
		final Long aufgabeId = pAufgabe.getId();
		Assert.isNotNull(aufgabeId, "Die Aufgabe ist noch nicht persistent!");
		String zuSchlecht = Aufgabe.LOCKED.equals(pAufgabe.getZuSchlechtFuerSerie()) ? Aufgabe.NOT_LOCKED
			: Aufgabe.LOCKED;
		getJdbcTemplate().update(IAufgabenStmts.CHANGE_ZU_SCHLECHT, new Object[] { zuSchlecht, aufgabeId });
		pAufgabe.setZuSchlechtFuerSerie(zuSchlecht);
	}
}
