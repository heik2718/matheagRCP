/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;

import de.egladil.base.logging.DebugPrinter;
import de.egladil.base.mysql.IJdbcTemplateWrapper;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.persistence.api.IMcAufgabenRepository;
import de.egladil.mathejungalt.persistence.db.dao.mappers.LongRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.McAufgabenRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.SchluesselRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.sql.IMcAufgabenStmts;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMcAufgabenTableNames;

/**
 * @author Winkelv
 */
public class McAufgabenDao extends AbstractDao implements IMcAufgabenRepository {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(McAufgabenDao.class);

	private Map<Long, MCAufgabe> aufgabenMap;

	/**
	 * @param pJdbcTemplateWrapper
	 */
	public McAufgabenDao(IJdbcTemplateWrapper pJdbcTemplateWrapper, IdGenerator idGenerator) {
		super(pJdbcTemplateWrapper, idGenerator);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IMcAufgabenRepository#loadAll()
	 */
	@Override
	public List<MCAufgabe> findAll() {
		if (aufgabenMap == null) {
			loadAufgaben();
		}
		List<MCAufgabe> liste = new ArrayList<MCAufgabe>();
		for (MCAufgabe aufgabe : aufgabenMap.values()) {
			liste.add(aufgabe);
		}
		return liste;
	}

	private void loadAufgaben() {
		aufgabenMap = new HashMap<Long, MCAufgabe>();
		List<MCAufgabe> liste = getJdbcTemplate().query(IMcAufgabenStmts.SELECT_STMT, new McAufgabenRowMapper());
		for (MCAufgabe aufgabe : liste) {
			aufgabenMap.put(aufgabe.getId(), aufgabe);
		}
		LOG.info(liste.size() + " Objekte aus" + IMcAufgabenTableNames.TABLE + " geladen.");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IMcAufgabenRepository#findById(java.lang.Long)
	 */
	@Override
	public MCAufgabe findById(Long pId) {
		if (aufgabenMap == null) {
			loadAufgaben();
		}
		return aufgabenMap.get(pId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IMcAufgabenRepository#getMaxKey()
	 */
	@Override
	public String getMaxKey() {
		return getJdbcTemplate().queryForObject(IMcAufgabenStmts.SELECT_MAX_KEY_STMT, new SchluesselRowMapper(),
			new Object[] { "%-%" });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IMcAufgabenRepository#save(de.egladil.mcmatheraetsel.archivdomain.archiv
	 * .MCAufgabe)
	 */
	@Override
	public void save(MCAufgabe pAufgabe) {
		Assert.isNotNull(pAufgabe, "pAufgabe");
		if (pAufgabe.getId() == null) {
			persistAufgabe(pAufgabe);
		} else {
			mergeAufgabe(pAufgabe);
		}
	}

	/**
	 * @param pAufgabe
	 */
	private void mergeAufgabe(final MCAufgabe pAufgabe) {
		getTransactionTemplate().execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(TransactionStatus pArg0) {
				Object[] args = getParams(pAufgabe, pAufgabe.getId());
				getJdbcTemplate().update(IMcAufgabenStmts.UPDATE_STMT, args);
			}
		});
	}

	/**
	 * @param pAufgabe
	 */
	private void persistAufgabe(final MCAufgabe pAufgabe) {
		if (aufgabenMap == null) {
			loadAufgaben();
		}
		final Long id = getIdGenerator().nextId(IMcAufgabenTableNames.TABLE);
		getTransactionTemplate().execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(TransactionStatus pArg0) {
				Object[] args = getParams(pAufgabe, id);
				try {
					getJdbcTemplate().update(IMcAufgabenStmts.INSERT_STMT, args);
				} catch (DataAccessException e) {
					DebugPrinter debugPrinter = new DebugPrinter(pAufgabe, 1);
					String msg = "DataAccessException beim Speichern der MCAufgabe. ";
					LOG.error(msg + debugPrinter.dump());
					throw e;
				}
			}
		});
		pAufgabe.setId(id);
		aufgabenMap.put(id, pAufgabe);
		getIdGenerator().increase(IMcAufgabenTableNames.TABLE);
	}

	/**
	 * @param pAufgabe
	 * @param pId
	 * @return
	 */
	private Object[] getParams(MCAufgabe pAufgabe, Long pId) {
		String tabelleGenerieren = pAufgabe.isTabelleGenerieren() ? "J" : "N";
		return new Object[] { pAufgabe.getSchluessel(), pAufgabe.getThema().getKurzbezeichnung(),
			pAufgabe.getStufe().getStufe(), pAufgabe.getPunkte(), pAufgabe.getArt().toString(), pAufgabe.getTitel(),
			pAufgabe.getAntwortA(), pAufgabe.getAntwortB(), pAufgabe.getAntwortC(), pAufgabe.getAntwortD(),
			pAufgabe.getAntwortE(), pAufgabe.getLoesungsbuchstabe().getBezeichnung(), pAufgabe.getBemerkung(),
			pAufgabe.getQuelle(), pAufgabe.getVerzeichnis(), tabelleGenerieren, pAufgabe.getAnzahlAntwortvorschlaege(),
			pId };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IMcAufgabenRepository#raetselIdMitAufgabe(de.egladil.mcmatheraetsel.
	 * archivdomain.archiv .MCAufgabe)
	 */
	@Override
	public List<Long> raetselIdMitAufgabe(MCAufgabe pAufgabe) {
		String stmt = IMcAufgabenStmts.FIND_RAESTEL_ID_BY_AUFGABE;
		Object[] params = new Object[] { pAufgabe.getId() };
		List<Long> result = getJdbcTemplate().query(stmt, new LongRowMapper(), params);
		return result;
	}
}
