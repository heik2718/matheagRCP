/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.tablenames;

/**
 * @author Winkelv
 */
public interface IMinikaenguruTableNames extends ICommonColumnNamesOhneSchluessel {

	/** */
	public static final String TABLE = "minikaenguru";

	/** */
	public static final String COL_BEENDET = "beendet";

	/** */
	public static final String COL_JAHR = "jahr";
}
