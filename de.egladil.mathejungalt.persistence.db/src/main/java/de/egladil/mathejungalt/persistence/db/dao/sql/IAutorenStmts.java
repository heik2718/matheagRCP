/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao.sql;

import de.egladil.mathejungalt.persistence.db.dao.tablenames.IAutorenTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IColumnNamesMitSchluessel;

/**
 * @author aheike
 */
public interface IAutorenStmts {

	/** */
	public static final String SELECT_STMT = "select " + IAutorenTableNames.COL_ID + ", "
		+ IAutorenTableNames.COL_SCHLUESSEL + ", " + IAutorenTableNames.COL_NAME + " from " + IAutorenTableNames.TABLE;

	/** */
	public static final String SELECT_AUTOR_ZU_BUCH_STMT = "select a." + IAutorenTableNames.COL_ID + ", a."
		+ IAutorenTableNames.COL_SCHLUESSEL + ", a." + IAutorenTableNames.COL_NAME + " from "
		+ IAutorenTableNames.TABLE + " a, " + IAutorenTableNames.TABLE_B2A + " b where a." + IAutorenTableNames.COL_ID
		+ " = b." + IAutorenTableNames.COL_AUTOR_ID + " and b." + IAutorenTableNames.COL_BUCH_ID + " = ?";

	/** */
	public static final String SELECT_ANZAHL_BUECHER_STMT = "select count(*) from " + IAutorenTableNames.TABLE_B2A
		+ " where " + IAutorenTableNames.COL_AUTOR_ID + " = ?";

	/** */
	public static final String SELECT_MAX_KEY_STMT = "select max(" + IAutorenTableNames.COL_SCHLUESSEL + ") as "
		+ IAutorenTableNames.COL_SCHLUESSEL + " from " + IAutorenTableNames.TABLE;

	/** */
	public static final String DELETE_STMT = "DELETE FROM " + IAutorenTableNames.TABLE + " WHERE "
		+ IColumnNamesMitSchluessel.COL_ID + " = ?";

	/** */
	public static final String UPDATE_STMT = "UPDATE " + IAutorenTableNames.TABLE + " SET "
		+ IAutorenTableNames.COL_SCHLUESSEL + " = ?, " + IAutorenTableNames.COL_NAME + " = ? WHERE "
		+ IAutorenTableNames.COL_ID + " = ?";

	/** */
	public static final String INSERT_STMT = "INSERT INTO " + IAutorenTableNames.TABLE + " ("
		+ IColumnNamesMitSchluessel.COL_ID + "," + IColumnNamesMitSchluessel.COL_SCHLUESSEL + ","
		+ IAutorenTableNames.COL_NAME + ") VALUES (?,?,?)";

}
