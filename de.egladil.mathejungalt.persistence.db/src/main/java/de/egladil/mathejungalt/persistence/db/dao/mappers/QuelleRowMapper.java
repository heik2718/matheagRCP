/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import de.egladil.mathejungalt.domain.medien.Buch;
import de.egladil.mathejungalt.domain.medien.Zeitschrift;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.domain.quellen.AbstractQuelle;
import de.egladil.mathejungalt.domain.quellen.Buchquelle;
import de.egladil.mathejungalt.domain.quellen.Kindquelle;
import de.egladil.mathejungalt.domain.quellen.MitgliedergruppenQuelle;
import de.egladil.mathejungalt.domain.quellen.NullQuelle;
import de.egladil.mathejungalt.domain.quellen.Zeitschriftquelle;
import de.egladil.mathejungalt.domain.types.EnumTypes.Quellenart;
import de.egladil.mathejungalt.persistence.api.IMedienRepository;
import de.egladil.mathejungalt.persistence.api.IMitgliederRepository;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IQuellenTableNames;
import de.egladil.mathejungalt.persistence.db.dao.typehandler.QuellenArtTypeHandler;

/**
 * @author Winkelv
 */
public class QuelleRowMapper implements ParameterizedRowMapper<AbstractQuelle> {

	/** */
	private IMitgliederRepository mitgliederRepository;

	/** */
	private IMedienRepository medienRepository;

	/** */
	private AbstractQuelle quelle;

	/** */
	private QuellenArtTypeHandler quellenArtTypeHandler;

	/**
	 * 
	 */
	public QuelleRowMapper() {
		super();
		quellenArtTypeHandler = new QuellenArtTypeHandler();
	}

	/**
	 * @param pMedienRepository
	 */
	public QuelleRowMapper(IMedienRepository pMedienRepository) {
		this();
		medienRepository = pMedienRepository;
	}

	/**
	 * @param pMitgliederRepository
	 */
	public QuelleRowMapper(IMitgliederRepository pMitgliederRepository) {
		this();
		mitgliederRepository = pMitgliederRepository;
	}

	/**
	 * 
	 */
	public QuelleRowMapper(IMitgliederRepository pMitgliederRepository, IMedienRepository pMedienRepository,
		final AbstractQuelle pQuelle) {
		this();
		mitgliederRepository = pMitgliederRepository;
		medienRepository = pMedienRepository;
		quelle = pQuelle;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public AbstractQuelle mapRow(ResultSet pArg0, int pArg1) throws SQLException {
		initQuelle(pArg0);
		return completeQuelle(pArg0);
	}

	/**
	 * @param pArg0
	 * @throws SQLException
	 */
	private void initQuelle(ResultSet pArg0) throws SQLException {
		if (quelle == null) {
			Quellenart art = quellenArtTypeHandler.getDomainValue(pArg0.getString(IQuellenTableNames.COL_ART));
			switch (art) {
			case B:
				quelle = new Buchquelle();
				break;
			case G:
				quelle = new MitgliedergruppenQuelle();
				break;
			case K:
				quelle = new Kindquelle();
				break;
			case N:
				quelle = new NullQuelle();
				break;
			case Z:
				quelle = new Zeitschriftquelle();
				break;
			default:
				break;
			}
			quelle.setId(pArg0.getLong(IQuellenTableNames.COL_ID));
		}
	}

	/**
	 * @param pArg0
	 * @return
	 * @throws SQLException
	 */
	private AbstractQuelle completeQuelle(ResultSet pArg0) throws SQLException {
		quelle.setSchluessel(pArg0.getString(IQuellenTableNames.COL_SCHLUESSEL));
		switch (quelle.getArt()) {
		case K:
			Kindquelle kindquelle = (Kindquelle) quelle;
			Mitglied mitglied = mitgliederRepository.findById(pArg0.getLong(IQuellenTableNames.COL_MITGLIED_ID));
			kindquelle.setMitglied(mitglied);
			kindquelle.setJahre(pArg0.getInt(IQuellenTableNames.COL_JAHRE));
			kindquelle.setKlasse(pArg0.getInt(IQuellenTableNames.COL_KLASSE));
			break;
		case B:
			Buchquelle buchquelle = (Buchquelle) quelle;
			Buch buch = (Buch) medienRepository.findMediumById(pArg0.getLong(IQuellenTableNames.COL_BUCH_ID));
			buchquelle.setBuch(buch);
			buchquelle.setSeite(pArg0.getInt(IQuellenTableNames.COL_SEITE));
			break;
		case Z:
			Zeitschriftquelle zeitschriftquelle = (Zeitschriftquelle) quelle;
			Zeitschrift zeitschrift = (Zeitschrift) medienRepository.findMediumById(pArg0
				.getLong(IQuellenTableNames.COL_ZEITSCHR_ID));
			zeitschriftquelle.setZeitschrift(zeitschrift);
			zeitschriftquelle.setJahrgang(pArg0.getInt(IQuellenTableNames.COL_JAHRGANG));
			zeitschriftquelle.setAusgabe(pArg0.getInt(IQuellenTableNames.COL_AUSGABE));
			break;
		default:
			break;
		}
		return quelle;
	}
}
