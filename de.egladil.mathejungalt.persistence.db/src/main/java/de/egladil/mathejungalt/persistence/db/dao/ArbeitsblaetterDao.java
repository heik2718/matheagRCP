/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.mysql.IJdbcTemplateWrapper;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblattitem;
import de.egladil.mathejungalt.persistence.api.IArbeitsblattRepository;
import de.egladil.mathejungalt.persistence.api.IAufgabenRepository;
import de.egladil.mathejungalt.persistence.db.dao.mappers.ArbeitsblattRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.ArbeitsblattitemRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.LongRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.SchluesselRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.sql.IArbeitsblaetterStmts;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IArbeitsblaetterTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IArbeitsblattitemsTableNames;
import de.egladil.mathejungalt.persistence.db.dao.util.SqlQueryUtil;

/**
 * @author Winkelv
 */
public class ArbeitsblaetterDao extends AbstractDao implements IArbeitsblattRepository {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(ArbeitsblaetterDao.class);

	/** */
	private IAufgabenRepository aufgabenRepository;

	/** */
	private Map<Long, Arbeitsblatt> arbeitsblattMap;

	/**
	 * @param pJdbcTemplateWrapper
	 */
	public ArbeitsblaetterDao(IJdbcTemplateWrapper pJdbcTemplateWrapper, IdGenerator idGenerator) {
		super(pJdbcTemplateWrapper, idGenerator);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.aufgabensammlungen.IArbeitsblattRepository#findAllWettbewerbe()
	 */
	@Override
	public List<Arbeitsblatt> findAllArbeitsblaetter() {
		if (arbeitsblattMap == null)
			loadArbeitsblaetter();
		List<Arbeitsblatt> liste = new ArrayList<Arbeitsblatt>();
		Iterator<Long> iter = arbeitsblattMap.keySet().iterator();
		while (iter.hasNext())
			liste.add(arbeitsblattMap.get(iter.next()));
		return liste;
	}

	/**
	 * 
	 */
	private void loadArbeitsblaetter() {
		List<Arbeitsblatt> liste = getJdbcTemplate().query(IArbeitsblaetterStmts.SELECT_STMT,
			new ArbeitsblattRowMapper());
		arbeitsblattMap = new HashMap<Long, Arbeitsblatt>();
		for (Arbeitsblatt blatt : liste)
			arbeitsblattMap.put(blatt.getId(), blatt);
		LOG.info(liste.size() + " " + IArbeitsblaetterTableNames.TABLE + " geladen");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.persistenceapi.aufgabensammlungen.IArbeitsblattRepository#findByAufgabe(de.egladil.mathe
	 * .core.domain .aufgaben.Aufgabe)
	 */
	@Override
	public Arbeitsblattitem findByAufgabe(Aufgabe pAufgabe) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.aufgabensammlungen.IArbeitsblattRepository#findById(java.lang.Long)
	 */
	@Override
	public Arbeitsblatt findById(Long pId) {
		if (arbeitsblattMap == null)
			loadArbeitsblaetter();
		if (!arbeitsblattMap.containsKey(pId))
			throw new MatheJungAltException("Es gibt kein Arbeitsblatt mit Id " + pId);
		return arbeitsblattMap.get(pId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.aufgabensammlungen.IArbeitsblattRepository#getMaxKey()
	 */
	@Override
	public String getMaxKey() {
		return getJdbcTemplate().queryForObject(IArbeitsblaetterStmts.SELECT_MAX_KEY_STMT, new SchluesselRowMapper());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.core.persistenceapi.aufgabensammlungen.IArbeitsblattRepository#loadItems(de.egladil.mathe.core
	 * .domain. aufgabensammlungen.arbeitsblaetter.Arbeitsblatt)
	 */
	@Override
	public void loadItems(Arbeitsblatt pArbeitsblatt) {
		if (pArbeitsblatt == null || pArbeitsblatt.getId() == null)
			throw new MatheJungAltException(MinikaenguruDao.class.getName()
				+ ".loadItems(): Parameter ist null oder Parameter.ID ist null");
		if (pArbeitsblatt.isItemsLoaded())
			return;
		if (arbeitsblattMap == null)
			loadArbeitsblaetter();

		if (!arbeitsblattMap.containsValue(pArbeitsblatt))
			throw new MatheJungAltException(MinikaenguruDao.class.getName()
				+ ".loadItems(): schwerwiegender Programmfehler: es gibt das Arbeitsblatt " + pArbeitsblatt
				+ " nicht im Repository!");

		pArbeitsblatt.initItems();
		ArbeitsblattitemRowMapper rowMapper = new ArbeitsblattitemRowMapper(aufgabenRepository, pArbeitsblatt);
		List<Arbeitsblattitem> itemList = getJdbcTemplate().query(IArbeitsblaetterStmts.SELECT_ITEMS_ZU_ARBBLATT_STMT,
			rowMapper, new Object[] { pArbeitsblatt.getId() });
		pArbeitsblatt.setItemsLoaded(true);
		LOG.info(itemList.size() + " " + IArbeitsblattitemsTableNames.TABLE + " geladen");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.core.persistenceapi.aufgabensammlungen.IArbeitsblattRepository#save(de.egladil.mathe.core.domain
	 * . aufgabensammlungen.arbeitsblaetter.Arbeitsblatt)
	 */
	@Override
	public void save(Arbeitsblatt pArbeitsblatt) {
		if (pArbeitsblatt == null)
			throw new MatheJungAltException("null an save(Arbeitsblatt) �bergeben");
		if (pArbeitsblatt.isNew())
			persistArbeitsblatt(pArbeitsblatt);
		else
			mergeArbeitsblatt(pArbeitsblatt);
	}

	/**
	 * @param pArbeitsblatt
	 */
	private void persistArbeitsblatt(final Arbeitsblatt pArbeitsblatt) {
		if (arbeitsblattMap == null)
			loadArbeitsblaetter();
		final Long neueId = getIdGenerator().nextId(IArbeitsblaetterTableNames.TABLE);
		getTransactionTemplate().execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(TransactionStatus pArg0) {
				getJdbcTemplate().update(
					IArbeitsblaetterStmts.INSERT_ARBBLATT_STMT,
					new Object[] { pArbeitsblatt.getSchluessel(), pArbeitsblatt.getTitel(), pArbeitsblatt.getBeendet(),
						pArbeitsblatt.getSperrend(), neueId });
				pArbeitsblatt.setId(neueId);

				for (Arbeitsblattitem item : pArbeitsblatt.getArbeitsblattitems()) {
					persistArbeitsblattitem(item);
				}

				arbeitsblattMap.put(neueId, pArbeitsblatt);
				getIdGenerator().increase(IArbeitsblaetterTableNames.TABLE);
			}
		});

	}

	/**
	 * @param pItem
	 */
	private void persistArbeitsblattitem(final Arbeitsblattitem pItem) {
		Long neueId = getIdGenerator().nextId(IArbeitsblattitemsTableNames.TABLE);
		getJdbcTemplate().update(IArbeitsblaetterStmts.INSERT_ITEM_STMT,
			new Object[] { pItem.getNummer(), pItem.getArbeitsblatt().getId(), pItem.getAufgabe().getId(), neueId });
		pItem.setId(neueId);
		getIdGenerator().increase(IArbeitsblattitemsTableNames.TABLE);
	}

	/**
	 * @param pArbeitsblatt
	 * @return
	 */
	private Map<Arbeitsblattitem, SqlQueryUtil.ACTION> initArbeitsblattitemActions(Arbeitsblatt pArbeitsblatt) {
		Map<Arbeitsblattitem, SqlQueryUtil.ACTION> map = new HashMap<Arbeitsblattitem, SqlQueryUtil.ACTION>();
		List<Long> idList = getJdbcTemplate().query(IArbeitsblaetterStmts.SELECT_ITEM_ID_ZU_ARBBLATT_STMT,
			new LongRowMapper(), new Object[] { pArbeitsblatt.getId() });
		List<Long> itmemsIDs = new ArrayList<Long>();
		// INSERTs und UPDATEs
		for (Arbeitsblattitem item : pArbeitsblatt.getArbeitsblattitems()) {
			itmemsIDs.add(item.getId());
			if (item.isNew())
				map.put(item, SqlQueryUtil.ACTION.INSERT);
			else {
				if (idList.contains(item.getId()))
					map.put(item, SqlQueryUtil.ACTION.UPDATE);

			}
		}
		// Deletes sind die, die nicht mehr in der minikaenguruItmemsIDs-Liste sind
		for (Long dbID : idList) {
			if (!itmemsIDs.contains(dbID)) {
				Arbeitsblattitem tempItem = new Arbeitsblattitem();
				tempItem.setId(dbID);
				map.put(tempItem, SqlQueryUtil.ACTION.DELETE);
			}
		}
		return map;
	}

	/**
	 * @param pArbeitsblatt
	 */
	private void mergeArbeitsblatt(final Arbeitsblatt pArbeitsblatt) {
		final Map<Arbeitsblattitem, SqlQueryUtil.ACTION> map = initArbeitsblattitemActions(pArbeitsblatt);
		getTransactionTemplate().execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(TransactionStatus pArg0) {
				getJdbcTemplate().update(
					IArbeitsblaetterStmts.UPDATE_ARBBLATT_STMT,
					new Object[] { pArbeitsblatt.getSchluessel(), pArbeitsblatt.getTitel(), pArbeitsblatt.getBeendet(),
						pArbeitsblatt.getSperrend(), pArbeitsblatt.getId() });

				Iterator<Arbeitsblattitem> itemIter = map.keySet().iterator();
				while (itemIter.hasNext()) {
					Arbeitsblattitem item = itemIter.next();
					switch (map.get(item)) {
					case DELETE:
						removeArbeitsblattitem(item);
						break;
					case INSERT:
						persistArbeitsblattitem(item);
					default:
						mergeArbeitsblattitem(item);
						break;
					}
				}
			}
		});
	}

	/**
	 * @param pItem
	 */
	private void removeArbeitsblattitem(Arbeitsblattitem pItem) {
		getJdbcTemplate().update(IArbeitsblaetterStmts.DELETE_ITEM_STMT, new Object[] { pItem.getId() });
	}

	/**
	 * @param pItem
	 */
	private void mergeArbeitsblattitem(Arbeitsblattitem pItem) {
		getJdbcTemplate().update(IArbeitsblaetterStmts.UPDATE_ITEM_STMT,
			new Object[] { pItem.getNummer(), pItem.getId() });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.persistenceapi.aufgabensammlungen.IArbeitsblattRepository#setAufgabenRepository(de.egladil
	 * .mathe.core .persistenceapi.aufgaben.IAufgabenRepository)
	 */
	@Override
	public void setAufgabenRepository(IAufgabenRepository pAufgabenRepository) {
		aufgabenRepository = pAufgabenRepository;
	}

	/**
	 * @param pArbblatt
	 */
	public void removeArbeitsblatt(Arbeitsblatt pArbblatt) {
		if (pArbblatt == null || pArbblatt.isNew())
			return;
		getJdbcTemplate().update(IArbeitsblaetterStmts.DELETE_ARBBLATT_STMT, pArbblatt.getId());
		arbeitsblattMap.remove(pArbblatt.getId());
	}

}
