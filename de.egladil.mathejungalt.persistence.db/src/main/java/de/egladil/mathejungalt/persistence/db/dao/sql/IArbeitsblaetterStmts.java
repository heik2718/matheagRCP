package de.egladil.mathejungalt.persistence.db.dao.sql;

import de.egladil.mathejungalt.persistence.db.dao.tablenames.IArbeitsblaetterTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IArbeitsblattitemsTableNames;

/**
 * @author aheike
 */
public interface IArbeitsblaetterStmts {

	/** */
	public static final String SELECT_STMT = "select " + IArbeitsblaetterTableNames.COL_ID + ", "
		+ IArbeitsblaetterTableNames.COL_SCHLUESSEL + ", " + IArbeitsblaetterTableNames.COL_TITEL + ", "
		+ IArbeitsblaetterTableNames.COL_BEENDET + ", " + IArbeitsblaetterTableNames.COL_SPERREND + " from "
		+ IArbeitsblaetterTableNames.TABLE;

	/** */
	public static final String SELECT_MAX_KEY_STMT = "select max(" + IArbeitsblaetterTableNames.COL_SCHLUESSEL
		+ ") as " + IArbeitsblaetterTableNames.COL_SCHLUESSEL + " from " + IArbeitsblaetterTableNames.TABLE;

	/** */
	public static final String SELECT_ITEMS_ZU_ARBBLATT_STMT = "select " + IArbeitsblattitemsTableNames.COL_ID + ", "
		+ IArbeitsblattitemsTableNames.COL_NUMMER + ", " + IArbeitsblattitemsTableNames.COL_AUFGABE_ID + " from "
		+ IArbeitsblattitemsTableNames.TABLE + " where " + IArbeitsblattitemsTableNames.COL_ARBEITSBLATT_ID + " = ?";

	/** */
	public static final String SELECT_ITEM_ID_ZU_ARBBLATT_STMT = "select " + IArbeitsblattitemsTableNames.COL_ID
		+ " from " + IArbeitsblattitemsTableNames.TABLE + " where " + IArbeitsblattitemsTableNames.COL_ARBEITSBLATT_ID
		+ " = ?";

	/** */
	public static final String INSERT_ARBBLATT_STMT = "insert into " + IArbeitsblaetterTableNames.TABLE + " ("
		+ IArbeitsblaetterTableNames.COL_SCHLUESSEL + ", " + IArbeitsblaetterTableNames.COL_TITEL + ", "
		+ IArbeitsblaetterTableNames.COL_BEENDET + ", " + IArbeitsblaetterTableNames.COL_SPERREND + ", "
		+ IArbeitsblaetterTableNames.COL_ID + ") values (?, ?, ?, ?, ?)";

	/** */
	public static final String UPDATE_ARBBLATT_STMT = "update " + IArbeitsblaetterTableNames.TABLE + " set "
		+ IArbeitsblaetterTableNames.COL_SCHLUESSEL + " = ?, " + IArbeitsblaetterTableNames.COL_TITEL + " = ?, "
		+ IArbeitsblaetterTableNames.COL_BEENDET + " = ?, " + IArbeitsblaetterTableNames.COL_SPERREND + " = ? where  "
		+ IArbeitsblaetterTableNames.COL_ID + " = ?";

	/** */
	public static final String INSERT_ITEM_STMT = "insert into " + IArbeitsblattitemsTableNames.TABLE + " ("
		+ IArbeitsblattitemsTableNames.COL_NUMMER + ", " + IArbeitsblattitemsTableNames.COL_ARBEITSBLATT_ID + ", "
		+ IArbeitsblattitemsTableNames.COL_AUFGABE_ID + ", " + IArbeitsblattitemsTableNames.COL_ID
		+ ") values (?, ?, ?, ?)";

	/** */
	public static final String UPDATE_ITEM_STMT = "update " + IArbeitsblattitemsTableNames.TABLE + " set "
		+ IArbeitsblattitemsTableNames.COL_NUMMER + " = ? where " + IArbeitsblattitemsTableNames.COL_ID + " = ?";

	/** */
	public static final String DELETE_ARBBLATT_STMT = "delete from " + IArbeitsblaetterTableNames.TABLE + " where "
		+ IArbeitsblaetterTableNames.COL_ID + " = ?";

	/** */
	public static final String DELETE_ITEM_STMT = "delete from " + IArbeitsblattitemsTableNames.TABLE + " where "
		+ IArbeitsblattitemsTableNames.COL_ID + " = ?";

}
