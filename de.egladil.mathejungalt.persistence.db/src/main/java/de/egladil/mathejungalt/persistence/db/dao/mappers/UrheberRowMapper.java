/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import de.egladil.mathejungalt.domain.mcraetsel.Urheber;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IUrheberTableNames;

/**
 * @author Winkelv
 */
public class UrheberRowMapper implements ParameterizedRowMapper<Urheber> {

	/**
	 *
	 */
	public UrheberRowMapper() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public Urheber mapRow(ResultSet pArg0, int pArg1) throws SQLException {
		Urheber urheber = new Urheber();
		urheber.setId(pArg0.getLong(IUrheberTableNames.COL_ID));
		urheber.setName(pArg0.getString(IUrheberTableNames.COL_NAME));
		urheber.setUrl(pArg0.getString(IUrheberTableNames.COL_URL));
		return urheber;
	}

}
