/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.tablenames;

/**
 * @author heike
 *
 */
public interface IMcRaetselitemsTableNames extends ICommonColumnNamesOhneSchluessel {

	/** */
	String TABLE = "mc_raetsel_items";

	/** */
	String COL_RAETSEL_ID = "raetsel_id";

	/** */
	String COL_MC_AUFGABE_ID = "mc_aufgabe_id";

	/** */
	String COL_NUMMER = "nummer";
}
