/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgaben.Heft;
import de.egladil.mathejungalt.domain.quellen.AbstractQuelle;
import de.egladil.mathejungalt.domain.quellen.Buchquelle;
import de.egladil.mathejungalt.domain.quellen.Kindquelle;
import de.egladil.mathejungalt.domain.quellen.MitgliedergruppenQuelle;
import de.egladil.mathejungalt.domain.quellen.NullQuelle;
import de.egladil.mathejungalt.domain.quellen.Zeitschriftquelle;
import de.egladil.mathejungalt.domain.types.EnumTypes.Quellenart;
import de.egladil.mathejungalt.persistence.api.IHefteRepository;
import de.egladil.mathejungalt.persistence.api.IQuellenRepository;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IAufgabenTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IQuellenTableNames;
import de.egladil.mathejungalt.persistence.db.dao.typehandler.AufgabenArtTypeHandler;
import de.egladil.mathejungalt.persistence.db.dao.typehandler.AufgabenzweckTypeHandler;
import de.egladil.mathejungalt.persistence.db.dao.typehandler.JahreszeitTypeHandler;
import de.egladil.mathejungalt.persistence.db.dao.typehandler.QuellenArtTypeHandler;
import de.egladil.mathejungalt.persistence.db.dao.typehandler.ThemaTypeHandler;

/**
 * @author Winkelv
 */
public class AufgabeRowMapper implements ParameterizedRowMapper<Aufgabe> {

	/** */
	private AufgabenArtTypeHandler aufgabenArtTypeHandler = new AufgabenArtTypeHandler();

	/** */
	private AufgabenzweckTypeHandler zweckTypeHandler = new AufgabenzweckTypeHandler();

	/** */
	private JahreszeitTypeHandler jahreszeitTypeHandler = new JahreszeitTypeHandler();

	/** */
	private ThemaTypeHandler themaTypeHandler = new ThemaTypeHandler();

	/** */
	private QuellenArtTypeHandler quellenArtTypeHandler = new QuellenArtTypeHandler();

	/** */
	private IHefteRepository hefteRepository;

	/** */
	private IQuellenRepository quellenRepository;

	/**
	 *
	 */
	public AufgabeRowMapper(IQuellenRepository pQuellenRepository, IHefteRepository pHefteRepository) {
		hefteRepository = pHefteRepository;
		quellenRepository = pQuellenRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public Aufgabe mapRow(ResultSet pArg0, int pArg1) throws SQLException {
		Aufgabe aufgabe = new Aufgabe();
		aufgabe.setId(pArg0.getLong(IAufgabenTableNames.COL_ID));
		aufgabe.setZweck(zweckTypeHandler.getDomainValue(pArg0.getString(IAufgabenTableNames.COL_ZWECK)));
		aufgabe.setArt(aufgabenArtTypeHandler.getDomainValue(pArg0.getString(IAufgabenTableNames.COL_ART)));
		aufgabe.setBeschreibung(pArg0.getString(IAufgabenTableNames.COL_BESCHREIBUNG));
		aufgabe
			.setJahreszeit(jahreszeitTypeHandler.getDomainValue(pArg0.getString(IAufgabenTableNames.COL_JAHRESZEIT)));
		aufgabe.setSchluessel(pArg0.getString(IAufgabenTableNames.COL_SCHLUESSEL));
		aufgabe.setStufe(pArg0.getInt(IAufgabenTableNames.COL_STUFE));
		aufgabe.setThema(themaTypeHandler.getDomainValue(pArg0.getString(IAufgabenTableNames.COL_THEMA)));
		aufgabe.setTitel(pArg0.getString(IAufgabenTableNames.COL_TITEL));
		aufgabe.setZuSchlechtFuerSerie(pArg0.getString(IAufgabenTableNames.COL_ABGELAUFEN_SERIE));
		aufgabe.setVerzeichnis(pArg0.getString(IAufgabenTableNames.COL_VERZEICHNIS));

		Quellenart quellenart = quellenArtTypeHandler.getDomainValue(pArg0
			.getString(IAufgabenTableNames.COL_QUELLE_ART));
		Long quelleId = pArg0.getLong(IAufgabenTableNames.COL_QUELLE_ID);
		AbstractQuelle quelle = quellenRepository.findById(quelleId);

		if (quelle == null) {
			switch (quellenart) {
			case B:
				quelle = new Buchquelle();
				break;
			case K:
				quelle = new Kindquelle();
				break;
			case Z:
				quelle = new Zeitschriftquelle();
				break;
			case G:
				quelle = new MitgliedergruppenQuelle();
				break;
			default:
				quelle = new NullQuelle();
				break;
			}
			quelle.setId(pArg0.getLong(IAufgabenTableNames.COL_QUELLE_ID));
			quelle.setSchluessel(pArg0.getString(IQuellenTableNames.ALIAS_QUELLE_SCHLUESSEL));
			quelle.setCompletelyLoaded(false);
			quellenRepository.register(quelle);
		}
		aufgabe.setQuelle(quelle);

		Long heftId = pArg0.getLong(IAufgabenTableNames.COL_HEFT_ID);
		if (heftId == null || heftId == 0)
			return aufgabe;

		Heft heft = hefteRepository.findById(heftId);

		if (heft == null) {
			heft = new Heft();
			heft.setId(heftId);
			heft.setBeendet(pArg0.getInt(IAufgabenTableNames.COL_HEFT_BEENDET));
			heft.setTitel(pArg0.getString(IAufgabenTableNames.COL_HEFT_TITEL));
			heft.setSchluessel(pArg0.getString(IAufgabenTableNames.COL_HEFT_SCHLUESSEL));
			hefteRepository.register(heft);
		}
		aufgabe.setHeft(heft);
		heft.addAufgabe(aufgabe);

		aufgabe.setGesperrtFuerArbeitsblatt(null);
		aufgabe.setGesperrtFuerWettbewerb(null);
		return aufgabe;
	}
}
