/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.tablenames;

/**
 * @author Winkelv
 */
public interface IQuellenTableNames extends IColumnNamesMitSchluessel {

	/** */
	public static final String TABLE = "quellen";

	/** */
	public static final String COL_ART = "art";

	/** */
	public static final String COL_BUCH_ID = "buch_id";

	/** */
	public static final String COL_ZEITSCHR_ID = "zeitschrift_id";

	/** */
	public static final String COL_AUSGABE = "ausgabe";

	/** */
	public static final String COL_JAHRGANG = "jahrgang";

	/** */
	public static final String COL_SEITE = "seite";

	/** */
	public static final String COL_JAHRE = "jahre";

	/** */
	public static final String COL_KLASSE = "klasse";

	/** */
	public static final String COL_MITGLIED_ID = "mitglied_id";

	public static final String ALIAS_QUELLE_SCHLUESSEL = "quelle_schluessel";
}
