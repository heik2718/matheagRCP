/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.tablenames;

/**
 * @author Winkelv
 */
public interface IAutorenTableNames extends IColumnNamesMitSchluessel {

	/** */
	public static final String TABLE = "autoren";

	/** */
	public static final String TABLE_B2A = "buecher2autoren";

	/** */
	public static final String COL_NAME = "namen";

	/** */
	public static final String COL_BUCH_ID = "buch_id";

	/** */
	public static final String COL_AUTOR_ID = "autor_id";
}
