/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.sql;

import de.egladil.mathejungalt.persistence.db.dao.tablenames.IUrheberTableNames;

/**
 * @author aheike
 */
public interface IUrheberStmts {

	/** */
	public static final String SELECT_STMT = "select " + IUrheberTableNames.COL_ID + ", " + IUrheberTableNames.COL_NAME
		+ ", " + IUrheberTableNames.COL_URL + " from " + IUrheberTableNames.TABLE;

	/** */
	public static final String UPDATE_STMT = "UPDATE " + IUrheberTableNames.TABLE + " SET "
		+ IUrheberTableNames.COL_NAME + " = ?, " + IUrheberTableNames.COL_URL + " = ? WHERE "
		+ IUrheberTableNames.COL_ID + " = ?";

	/** */
	public static final String INSERT_STMT = "INSERT INTO " + IUrheberTableNames.TABLE + " ("
		+ IUrheberTableNames.COL_NAME + "," + IUrheberTableNames.COL_URL + "," + IUrheberTableNames.COL_ID
		+ ") VALUES (?,?,?)";

}
