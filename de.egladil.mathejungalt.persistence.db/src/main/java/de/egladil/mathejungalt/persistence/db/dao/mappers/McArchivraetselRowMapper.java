package de.egladil.mathejungalt.persistence.db.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.types.MCAufgabenstufen;
import de.egladil.mathejungalt.persistence.api.IUrheberRepository;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMcRaetselTableNames;

public class McArchivraetselRowMapper implements ParameterizedRowMapper<MCArchivraetsel> {

	private final IUrheberRepository urheberRepository;

	/**
	 * @param pUrheberRepository
	 */
	public McArchivraetselRowMapper(IUrheberRepository pUrheberRepository) {
		urheberRepository = pUrheberRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public MCArchivraetsel mapRow(ResultSet pRs, int pRowNum) throws SQLException {
		MCArchivraetsel raetsel = new MCArchivraetsel();
		raetsel.setId(pRs.getLong(IMcRaetselTableNames.COL_ID));
		raetsel.setLicenseShort(pRs.getString(IMcRaetselTableNames.COL_LICENSE_SHORT));
		raetsel.setLicenseFull(pRs.getString(IMcRaetselTableNames.COL_LICENSE_FULL));
		raetsel.setTitel(pRs.getString(IMcRaetselTableNames.COL_TITEL));
		raetsel.setSchluessel(pRs.getString(IMcRaetselTableNames.COL_SCHLUESSEL));
		Integer published = pRs.getInt(IMcRaetselTableNames.COL_PUBLISHED);
		Integer privat = pRs.getInt(IMcRaetselTableNames.COL_PRIVAT);
		raetsel.setVeroeffentlicht(published == 1);
		raetsel.setPrivat(privat == 1);
		int stufeInt = pRs.getInt(IMcRaetselTableNames.COL_STUFE);
		MCAufgabenstufen stufe = MCAufgabenstufen.valueOf(stufeInt);
		raetsel.setStufe(stufe);
		Long urheberId = pRs.getLong(IMcRaetselTableNames.COL_AUTOR_ID);
		raetsel.setAutor(urheberRepository.findById(urheberId));
		return raetsel;
	}
}
