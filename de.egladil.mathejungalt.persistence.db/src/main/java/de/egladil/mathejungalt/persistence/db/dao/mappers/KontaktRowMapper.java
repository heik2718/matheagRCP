/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.persistence.db.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IKontakteTableNames;

/**
 * <p>
 * NOTE: THIS WIDGET AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 * 
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class KontaktRowMapper implements ParameterizedRowMapper<Kontakt> {

	/**
   * 
   */
	public KontaktRowMapper() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public Kontakt mapRow(ResultSet pArg0, int pArg1) throws SQLException {
		Kontakt kontakt = new Kontakt();
		kontakt.setId(pArg0.getLong(IKontakteTableNames.COL_ID));
		kontakt.setSchluessel(pArg0.getString(IKontakteTableNames.COL_SCHLUESSEL));
		kontakt.setName(pArg0.getString(IKontakteTableNames.COL_NAME));
		kontakt.setEmail(pArg0.getString(IKontakteTableNames.COL_EMAIL));
		int abo = pArg0.getInt(IKontakteTableNames.COL_ABO);
		kontakt.setAbo(abo > 0);
		return kontakt;
	}
}
