/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import de.egladil.mathejungalt.domain.medien.AbstractPrintmedium;
import de.egladil.mathejungalt.domain.medien.Buch;
import de.egladil.mathejungalt.domain.medien.Zeitschrift;
import de.egladil.mathejungalt.domain.types.EnumTypes.Medienart;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMedienTableNames;
import de.egladil.mathejungalt.persistence.db.dao.typehandler.MedienartTypeHandler;

/**
 * @author Winkelv
 */
public class MedienRowMapper implements ParameterizedRowMapper<AbstractPrintmedium> {

	private MedienartTypeHandler typeHandler;

	/**
	 * 
	 */
	public MedienRowMapper() {
		typeHandler = new MedienartTypeHandler();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public AbstractPrintmedium mapRow(ResultSet pArg0, int pArg1) throws SQLException {
		Medienart art = typeHandler.getDomainValue(pArg0.getString(IMedienTableNames.COL_ART));
		AbstractPrintmedium medium;
		switch (art) {
		case B:
			medium = new Buch();
			break;
		default:
			medium = new Zeitschrift();
			break;
		}
		medium.setSchluessel(pArg0.getString(IMedienTableNames.COL_SCHLUESSEL));
		medium.setId(pArg0.getLong(IMedienTableNames.COL_ID));
		medium.setTitel(pArg0.getString(IMedienTableNames.COL_TITEL));
		return medium;
	}

}
