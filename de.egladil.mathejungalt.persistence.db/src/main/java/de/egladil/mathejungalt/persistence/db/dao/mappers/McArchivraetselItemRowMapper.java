/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.persistence.api.IMcAufgabenRepository;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMcRaetselitemsTableNames;

/**
 * @author winkelv
 */
public class McArchivraetselItemRowMapper implements ParameterizedRowMapper<MCArchivraetselItem> {

	/** */
	private MCArchivraetsel raetsel;

	/** */
	private IMcAufgabenRepository mcAufgabenRepository;

	/**
	 * @param pAufgabenRepository
	 * @param pSerie
	 */
	public McArchivraetselItemRowMapper(IMcAufgabenRepository pAufgabenRepository, MCArchivraetsel pSerie) {
		super();
		mcAufgabenRepository = pAufgabenRepository;
		raetsel = pSerie;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public MCArchivraetselItem mapRow(ResultSet pArg0, int pArg1) throws SQLException {
		MCArchivraetselItem item = new MCArchivraetselItem();
		item.setId(pArg0.getLong(IMcRaetselitemsTableNames.COL_ID));
		item.setNummer(pArg0.getString(IMcRaetselitemsTableNames.COL_NUMMER));
		item.setRaetsel(raetsel);

		Long aufgId = pArg0.getLong(IMcRaetselitemsTableNames.COL_MC_AUFGABE_ID);
		item.setAufgabe(mcAufgabenRepository.findById(aufgId));
		raetsel.addItem(item);

		return item;
	}

}
