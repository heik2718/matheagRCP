/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.mysql.IJdbcTemplateWrapper;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serienitem;
import de.egladil.mathejungalt.persistence.api.IAufgabenRepository;
import de.egladil.mathejungalt.persistence.api.ISerienRepository;
import de.egladil.mathejungalt.persistence.db.dao.mappers.LongRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.SerieRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.SerienitemRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.sql.ISerienStmts;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.ISerienTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.ISerienitemsTableNames;
import de.egladil.mathejungalt.persistence.db.dao.typehandler.DateTypeHandler;
import de.egladil.mathejungalt.persistence.db.dao.util.SqlQueryUtil;

/**
 * @author Winkelv
 */
public class SerienDao extends AbstractDao implements ISerienRepository {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(SerienDao.class);

	/** */
	private IAufgabenRepository aufgabenRepository;

	/** */
	private Map<Long, Serie> serienMap;

	/**
	 * @param pJdbcTemplateWrapper
	 */
	public SerienDao(IJdbcTemplateWrapper pJdbcTemplateWrapper, IdGenerator idGenerator) {
		super(pJdbcTemplateWrapper, idGenerator);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.aufgabensammlungen.ISerienRepository#findAllSerien()
	 */
	@Override
	public List<Serie> findAllSerien() {
		if (serienMap == null) {
			loadSerien();
		}
		List<Serie> serien = new ArrayList<Serie>();
		for (Serie serie : serienMap.values()) {
			serien.add(serie);
		}
		return serien;
	}

	/**
	 *
	 */
	private void loadSerien() {
		List<Serie> liste = getJdbcTemplate().query(ISerienStmts.SELECT_STMT, new SerieRowMapper());
		serienMap = new HashMap<Long, Serie>();
		for (Serie serie : liste) {
			serienMap.put(serie.getId(), serie);
		}
		LOG.info(liste.size() + " " + ISerienTableNames.TABLE + " geladen");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.persistenceapi.aufgabensammlungen.ISerienRepository#findByAufgabe(de.egladil.mathe.core
	 * .domain.aufgaben .Serienaufgabe)
	 */
	@Override
	public Serienitem findByAufgabe(Aufgabe pAufgabe) {
		throw new MatheJungAltException("Bisher gab es keine Verwendung fuer diese Methode. Daher nicht implementiert");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.aufgabensammlungen.ISerienRepository#findByKey(java.lang.Integer)
	 */
	@Override
	public Serie findByKey(Integer pKey) {
		throw new MatheJungAltException("Bisher gab es keine Verwendung fuer diese Methode. Daher nicht implementiert");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.aufgabensammlungen.ISerienRepository#findById(java.lang.Long)
	 */
	@Override
	public Serie findById(Long pId) {
		if (serienMap == null)
			loadSerien();
		if (!serienMap.containsKey(pId))
			throw new MatheJungAltException("Es gibt keine Serie mit der Id " + pId);
		return serienMap.get(pId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.aufgabensammlungen.ISerienRepository#findByRunde(int)
	 */
	@Override
	public List<Serie> findByRunde(int pRunde) {
		throw new MatheJungAltException("Bisher gab es keine Verwendung fuer diese Methode. Daher nicht implementiert");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.core.persistenceapi.aufgabensammlungen.ISerienRepository#findItems(de.egladil.mathe.core.domain
	 * . aufgabensammlungen.serien.Serie)
	 */
	@Override
	public void loadSerienitems(Serie pSerie) {
		if (pSerie == null || pSerie.getId() == null) {
			throw new MatheJungAltException(SerienDao.class.getName()
				+ ".loadSerienitems(): Parameter ist null oder Parameter.ID ist null");
		}
		if (pSerie.isItemsLoaded()) {
			return;
		}
		if (serienMap == null) {
			loadSerien();
		}

		if (!serienMap.containsValue(pSerie)) {
			throw new MatheJungAltException(SerienDao.class.getName()
				+ ".loadSerienitems(): schwerwiegender Programmfehler: es gibt die Serie " + pSerie
				+ " nicht im Repository!");
		}

		LOG.info("Lade items zu " + pSerie.toString());
		pSerie.initItems();
		SerienitemRowMapper rowMapper = new SerienitemRowMapper(aufgabenRepository, pSerie);
		List<Serienitem> itemList = getJdbcTemplate().query(ISerienStmts.SELECT_ITEMS_ZU_SERIE_STMT, rowMapper,
			new Object[] { pSerie.getId() });
		pSerie.setItemsLoaded(true);
		if (LOG.isDebugEnabled()) {
			LOG.debug(itemList.size() + " " + ISerienitemsTableNames.TABLE + " geladen (" + pSerie.toString() + ")");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.aufgabensammlungen.ISerienRepository#getMaxKey()
	 */
	@Override
	public Integer getMaxKey() {
		return getJdbcTemplate().queryForInt(ISerienStmts.SELECT_MAX_KEY_STMT);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.core.persistenceapi.aufgabensammlungen.ISerienRepository#save(de.egladil.mathe.core.domain.
	 * aufgabensammlungen .serien.Serie)
	 */
	@Override
	public void saveSerie(Serie pSerie) {
		if (pSerie == null) {
			throw new MatheJungAltException("null an saveSerie() uebergeben");
		}
		if (pSerie.isNew()) {
			persistSerie(pSerie);
		} else {
			mergeSerie(pSerie);
		}

	}

	/**
	 * @param pSerie
	 */
	private void mergeSerie(final Serie pSerie) {
		final Map<Serienitem, SqlQueryUtil.ACTION> serienitemsActions = initSerienitemActions(pSerie);
		getTransactionTemplate().execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(TransactionStatus pArg0) {
				getJdbcTemplate().update(
					ISerienStmts.UPDATE_SERIE_STMT,
					new Object[] { pSerie.getNummer(), pSerie.getRunde(),
						new DateTypeHandler().getSqlValue(pSerie.getDatum()), pSerie.getBeendet(),
						pSerie.getMonatJahrText(), pSerie.getId() });

				Iterator<Serienitem> itemIter = serienitemsActions.keySet().iterator();
				while (itemIter.hasNext()) {
					Serienitem item = itemIter.next();
					switch (serienitemsActions.get(item)) {
					case DELETE:
						removeSerienitem(item);
						break;
					case INSERT:
						persistSerienitem(item);
					default:
						mergeSerienitem(item);
						break;
					}
				}
			}
		});

	}

	private void removeSerienitem(Serienitem pSerienitem) {
		if (pSerienitem == null || pSerienitem.isNew())
			return;
		getJdbcTemplate().update(ISerienStmts.DELETE_SERIENITEM_STMT, new Object[] { pSerienitem.getId() });
	}

	/**
	 * @param pSerie
	 * @return
	 */
	private Map<Serienitem, SqlQueryUtil.ACTION> initSerienitemActions(Serie pSerie) {
		Map<Serienitem, SqlQueryUtil.ACTION> map = new HashMap<Serienitem, SqlQueryUtil.ACTION>();
		List<Long> idList = getJdbcTemplate().query(ISerienStmts.SELECT_SERIENITEM_ID_ZU_SERIE_STMT,
			new LongRowMapper(), new Object[] { pSerie.getId() });
		List<Long> serieItmemsIDs = new ArrayList<Long>();
		// INSERTs und UPDATEs
		for (IAufgabensammlungItem item : pSerie.getItems()) {
			Serienitem serienitem = (Serienitem) item;
			serieItmemsIDs.add(serienitem.getId());
			if (serienitem.isNew()) {
				map.put(serienitem, SqlQueryUtil.ACTION.INSERT);
			} else {
				if (idList.contains(serienitem.getId()))
					map.put(serienitem, SqlQueryUtil.ACTION.UPDATE);

			}
		}
		// Deletes sind die, die nicht mehr in der SerieItemsIDs-Liste sind
		for (Long dbID : idList) {
			if (!serieItmemsIDs.contains(dbID)) {
				Serienitem tempItem = new Serienitem();
				tempItem.setId(dbID);
				map.put(tempItem, SqlQueryUtil.ACTION.DELETE);
			}
		}
		return map;
	}

	/**
	 * @param pSerie
	 */
	private void persistSerie(final Serie pSerie) {
		if (serienMap == null) {
			loadSerien();
		}
		final Long serieId = getIdGenerator().nextId(ISerienTableNames.TABLE);
		getTransactionTemplate().execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(TransactionStatus pArg0) {
				getJdbcTemplate().update(
					ISerienStmts.INSERT_SERIE_STMT,
					new Object[] { pSerie.getNummer(), pSerie.getRunde(),
						new DateTypeHandler().getSqlValue(pSerie.getDatum()), pSerie.getBeendet(),
						pSerie.getMonatJahrText(), serieId });
				pSerie.setId(serieId);

				for (IAufgabensammlungItem item : pSerie.getItems()) {
					persistSerienitem((Serienitem) item);
				}

				serienMap.put(serieId, pSerie);
				getIdGenerator().increase(ISerienTableNames.TABLE);
			}
		});

	}

	/**
	 * Neues Serienitem speichern
	 *
	 * @param pItem
	 */
	private void persistSerienitem(final Serienitem pItem) {
		Long id = getIdGenerator().nextId(ISerienitemsTableNames.TABLE);
		getJdbcTemplate().update(ISerienStmts.INSERT_SERIENITEM_STMT,
			new Object[] { pItem.getSerie().getId(), pItem.getAufgabe().getId(), pItem.getNummer(), id });
		pItem.setId(id);
		getIdGenerator().increase(ISerienitemsTableNames.TABLE);
	}

	/**
	 * Änderung speichern
	 *
	 * @param pItem
	 */
	private void mergeSerienitem(final Serienitem pItem) {
		getJdbcTemplate().update(ISerienStmts.UPDATE_SERIENITEM_STMT,
			new Object[] { pItem.getSerie().getId(), pItem.getAufgabe().getId(), pItem.getNummer(), pItem.getId() });
	}

	public void removeSerie(final Serie pSerie) {
		if (pSerie == null || pSerie.isNew())
			return;
		// wegen kaskadierenden Löschens wprde es eigentlich reichen, die Serie zu löschen. Aber die DB ist manchmal
		// nicht korrekt
		// konfiguriert, so dass wir sicherheitshalber zuerst die Items löschen
		getTransactionTemplate().execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(TransactionStatus pArg0) {

				for (IAufgabensammlungItem item : pSerie.getItems()) {
					removeSerienitem((Serienitem) item);
				}

				getJdbcTemplate().update(ISerienStmts.DELETE_SERIE_STMT, new Object[] { pSerie.getId() });

				serienMap.remove(pSerie.getId());
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.core.persistenceapi.aufgabensammlungen.ISerienRepository#setAufgabenRepository(de.egladil.mathe
	 * .core. persistenceapi.aufgaben.IAufgabenRepository)
	 */
	@Override
	public void setAufgabenRepository(IAufgabenRepository pAufgabenRepository) {
		aufgabenRepository = pAufgabenRepository;
	}
}
