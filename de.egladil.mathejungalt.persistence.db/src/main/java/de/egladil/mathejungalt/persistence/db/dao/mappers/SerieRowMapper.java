/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao.mappers;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.ISerienTableNames;
import de.egladil.mathejungalt.persistence.db.dao.typehandler.DateTypeHandler;

/**
 * @author Winkelv
 */
public class SerieRowMapper implements ParameterizedRowMapper<Serie> {

	/** */
	private DateTypeHandler dateTypeHandler;

	/**
	 * 
	 */
	public SerieRowMapper() {
		dateTypeHandler = new DateTypeHandler();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public Serie mapRow(ResultSet pArg0, int pArg1) throws SQLException {
		Serie serie = new Serie();
		serie.setId(pArg0.getLong(ISerienTableNames.COL_ID));
		serie.setBeendet(pArg0.getInt(ISerienTableNames.COL_BEENDET));
		serie.setDatum(dateTypeHandler.getDomainValue((Date) pArg0.getObject(ISerienTableNames.COL_DATUM)));
		serie.setMonatJahrText(pArg0.getString(ISerienTableNames.COL_MONAT_JAHR));
		serie.setNummer(pArg0.getInt(ISerienTableNames.COL_NUMMER));
		serie.setRunde(pArg0.getInt(ISerienTableNames.COL_RUNDE));
		return serie;
	}

}
