/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.typehandler;

import java.sql.Date;

/**
 * wandelt java.sql.Date in java.util.Date und umgekehrt
 *
 * @author Winkelv
 */
public class DateTypeHandler {

	/**
	 *
	 */
	public DateTypeHandler() {
	}

	/**
	 * @param pSqlValue {@link Date}
	 * @return {@link java.util.Date}
	 */
	public java.util.Date getDomainValue(Date pSqlValue) {
		java.util.Date result = new java.util.Date();
		result.setTime(pSqlValue.getTime());
		return result;
	}

	/**
	 * @param pDomainValue {@link Date}
	 * @return {@link java.util.Date}
	 */
	public Date getSqlValue(java.util.Date pDomainValue) {
		return new java.sql.Date(pDomainValue.getTime());
	}
}
