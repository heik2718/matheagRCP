/**
 * 
 */
package de.egladil.mathejungalt.persistence.db;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Properties;

import org.osgi.service.component.ComponentFactory;
import org.osgi.service.component.ComponentInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.transaction.support.TransactionTemplate;

import de.egladil.base.mysql.IJdbcTemplateWrapper;
import de.egladil.mathejungalt.persistence.api.IArbeitsblattRepository;
import de.egladil.mathejungalt.persistence.api.IAufgabenRepository;
import de.egladil.mathejungalt.persistence.api.IAutorenRepository;
import de.egladil.mathejungalt.persistence.api.IHefteRepository;
import de.egladil.mathejungalt.persistence.api.IKontakteRepository;
import de.egladil.mathejungalt.persistence.api.IMailinglisteRepository;
import de.egladil.mathejungalt.persistence.api.IMcAufgabenRepository;
import de.egladil.mathejungalt.persistence.api.IMcRaetselRepository;
import de.egladil.mathejungalt.persistence.api.IMedienRepository;
import de.egladil.mathejungalt.persistence.api.IMinikaenguruRepository;
import de.egladil.mathejungalt.persistence.api.IMitgliederRepository;
import de.egladil.mathejungalt.persistence.api.IPersistenceservice;
import de.egladil.mathejungalt.persistence.api.IQuellenRepository;
import de.egladil.mathejungalt.persistence.api.ISchulenRepository;
import de.egladil.mathejungalt.persistence.api.ISerienRepository;
import de.egladil.mathejungalt.persistence.api.IUrheberRepository;
import de.egladil.mathejungalt.persistence.db.dao.ArbeitsblaetterDao;
import de.egladil.mathejungalt.persistence.db.dao.AufgabenDao;
import de.egladil.mathejungalt.persistence.db.dao.AutorenDAO;
import de.egladil.mathejungalt.persistence.db.dao.HefteDao;
import de.egladil.mathejungalt.persistence.db.dao.IdGenerator;
import de.egladil.mathejungalt.persistence.db.dao.KontakteDao;
import de.egladil.mathejungalt.persistence.db.dao.MailinglisteDao;
import de.egladil.mathejungalt.persistence.db.dao.McAufgabenDao;
import de.egladil.mathejungalt.persistence.db.dao.McRaetselDao;
import de.egladil.mathejungalt.persistence.db.dao.MedienDao;
import de.egladil.mathejungalt.persistence.db.dao.MinikaenguruDao;
import de.egladil.mathejungalt.persistence.db.dao.MitgliederDao;
import de.egladil.mathejungalt.persistence.db.dao.QuellenDao;
import de.egladil.mathejungalt.persistence.db.dao.SchulenDao;
import de.egladil.mathejungalt.persistence.db.dao.SerienDao;
import de.egladil.mathejungalt.persistence.db.dao.UrheberDao;

/**
 * @author aheike
 */
public class MysqlPersistenceService implements IPersistenceservice {

	private static final Logger LOG = LoggerFactory.getLogger(MysqlPersistenceService.class);

	private IAufgabenRepository aufgabenRepository;

	private IHefteRepository hefteRepository;

	private IMinikaenguruRepository minikaenguruRepository;

	private ISerienRepository serienRepository;

	private IMedienRepository medienRepository;

	private IAutorenRepository autorenRepository;

	private IMitgliederRepository mitgliederRepository;

	private IQuellenRepository quellenRepository;

	private IMailinglisteRepository mailinglisteRepository;

	private IArbeitsblattRepository arbeitsblattRepository;

	private IMcAufgabenRepository mcAufgabenRepository;

	private IMcRaetselRepository raetselRepository;

	private IUrheberRepository urheberRepository;

	private ISchulenRepository schulenRepository;

	private IKontakteRepository kontakteRepository;

	/** */
	private IdGenerator idGenerator;

	/**
   * 
   */
	private IJdbcTemplateWrapper jdbcTemplateWrapper;

	/**
	 * 
	 */
	public MysqlPersistenceService() {
		LOG.info("inside constructor");
	}

	/**
	 * @param pJdbcTemplateWrapper the jdbcTemplateWrapper to set
	 */
	protected final void setJdbcTemplateWrapper(ComponentFactory pServiceFactory) {
		// Hier werden die vom IJdbcTemplateWrapper benötigten
		// Konfigurationsparameter über das framework gesetzt.
		ComponentInstance instance = pServiceFactory.newInstance(getProps());
		// gibt eine korrekt konfigurierte Instanz des
		// IJdbcTemplateWrapper zurück.
		jdbcTemplateWrapper = (IJdbcTemplateWrapper) instance.getInstance();
		System.out.println(getClass().getSimpleName() + ".setJdbcTemplateWrapper()");
		idGenerator = new IdGenerator();
		idGenerator.setDataSource(jdbcTemplateWrapper.getDataSource());

		arbeitsblattRepository = new ArbeitsblaetterDao(jdbcTemplateWrapper, idGenerator);
		aufgabenRepository = new AufgabenDao(jdbcTemplateWrapper, idGenerator);
		autorenRepository = new AutorenDAO(jdbcTemplateWrapper, idGenerator);
		hefteRepository = new HefteDao(jdbcTemplateWrapper, idGenerator);
		kontakteRepository = new KontakteDao(jdbcTemplateWrapper, idGenerator);
		mailinglisteRepository = new MailinglisteDao(jdbcTemplateWrapper, idGenerator);
		mcAufgabenRepository = new McAufgabenDao(jdbcTemplateWrapper, idGenerator);
		medienRepository = new MedienDao(jdbcTemplateWrapper, idGenerator);
		minikaenguruRepository = new MinikaenguruDao(jdbcTemplateWrapper, idGenerator);
		mitgliederRepository = new MitgliederDao(jdbcTemplateWrapper, idGenerator);
		quellenRepository = new QuellenDao(jdbcTemplateWrapper, idGenerator);
		raetselRepository = new McRaetselDao(jdbcTemplateWrapper, idGenerator);
		schulenRepository = new SchulenDao(jdbcTemplateWrapper, idGenerator);
		serienRepository = new SerienDao(jdbcTemplateWrapper, idGenerator);
		urheberRepository = new UrheberDao(jdbcTemplateWrapper, idGenerator);

		serienRepository.setAufgabenRepository(aufgabenRepository);
		arbeitsblattRepository.setAufgabenRepository(aufgabenRepository);
		minikaenguruRepository.setAufgabenRepository(aufgabenRepository);
		aufgabenRepository.setHefteRepository(hefteRepository);
		aufgabenRepository.setQuellenRepository(quellenRepository);
		mitgliederRepository.setSerienRepository(serienRepository);
		medienRepository.setAutorenRepository(autorenRepository);
		quellenRepository.setMedienRepository(medienRepository);
		quellenRepository.setMitgliederRepository(mitgliederRepository);
		raetselRepository.setMcAufgabenRepository(mcAufgabenRepository);
		raetselRepository.setUrheberRepository(urheberRepository);
		schulenRepository.setMinikaenguruRepository(minikaenguruRepository);
		kontakteRepository.setSchulenRepository(schulenRepository);

	}

	/**
	 * Die Konfigurationsparameter, die der {@link IAuthenticationBusinessService} benötigt, werden aus der
	 * Konfiguration dieses Clients gelesen.
	 * 
	 * @return {@link Dictionary}
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Dictionary getProps() {
		Dictionary result = new Hashtable();
		Properties props = new Properties();
		try {
			props.load(getClass().getResourceAsStream("/jdbc.properties"));
			if (props.get(IJdbcTemplateWrapper.JDBC_ENV_KEY) != null) {
				result.put(IJdbcTemplateWrapper.JDBC_ENV_KEY, props.get(IJdbcTemplateWrapper.JDBC_ENV_KEY));
			}
			result.put(IJdbcTemplateWrapper.JDBC_URL_KEY, props.get(IJdbcTemplateWrapper.JDBC_URL_KEY));
			result.put(IJdbcTemplateWrapper.JDBC_USER_KEY, props.get(IJdbcTemplateWrapper.JDBC_USER_KEY));
			result.put(IJdbcTemplateWrapper.JDBC_PWD_KEY, props.get(IJdbcTemplateWrapper.JDBC_PWD_KEY));
			result.put(IJdbcTemplateWrapper.JDBC_GENERATE_DDL_KEY,
				props.get(IJdbcTemplateWrapper.JDBC_GENERATE_DDL_KEY));
			result.put(IJdbcTemplateWrapper.JDBC_SHOW_SQL_KEY, props.get(IJdbcTemplateWrapper.JDBC_SHOW_SQL_KEY));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * @param pJdbcTemplateWrapper the jdbcTemplateWrapper to set
	 */
	protected final void unsetJdbcTemplateWrapper(ComponentFactory pServiceFactory) {
		jdbcTemplateWrapper = null;
		System.out.println("JdbcExampleClient.unsetJdbcTemplateWrapper()");
	}

	/**
	 * @return the idGenerator
	 */
	public IdGenerator getIdGenerator() {
		if (idGenerator == null) {
			idGenerator = new IdGenerator();
			idGenerator.setDataSource(jdbcTemplateWrapper.getDataSource());
		}
		return idGenerator;
	}

	public SimpleJdbcTemplate getJdbcTemplate() {
		return jdbcTemplateWrapper.getJdbcTemplate();
	}

	/**
	 * @return the transactionTemplate
	 */
	public TransactionTemplate getTransactionTemplate() {
		return jdbcTemplateWrapper.getTransactionTemplate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getAufgabenRepository()
	 */
	@Override
	public IAufgabenRepository getAufgabenRepository() {
		return aufgabenRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getHefteRepository()
	 */
	@Override
	public IHefteRepository getHefteRepository() {
		return hefteRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getMinikaenguruRepository()
	 */
	@Override
	public IMinikaenguruRepository getMinikaenguruRepository() {
		return minikaenguruRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getSerienRepository()
	 */
	@Override
	public ISerienRepository getSerienRepository() {
		return serienRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getMedienRepository()
	 */
	@Override
	public IMedienRepository getMedienRepository() {
		return medienRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getAutorenRepository()
	 */
	@Override
	public IAutorenRepository getAutorenRepository() {
		return autorenRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getMitgliederRepository()
	 */
	@Override
	public IMitgliederRepository getMitgliederRepository() {
		return mitgliederRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getQuellenRepository()
	 */
	@Override
	public IQuellenRepository getQuellenRepository() {
		return quellenRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getMailinglisteRepository()
	 */
	@Override
	public IMailinglisteRepository getMailinglisteRepository() {
		return mailinglisteRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getArbeitsblattRepository()
	 */
	@Override
	public IArbeitsblattRepository getArbeitsblattRepository() {
		return arbeitsblattRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getSchulenRepository()
	 */
	@Override
	public ISchulenRepository getSchulenRepository() {
		return schulenRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getKontakteRepository()
	 */
	@Override
	public IKontakteRepository getKontakteRepository() {
		return kontakteRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#shutDown()
	 */
	@Override
	public boolean shutDown() {
		try {
			idGenerator.flush();
			return true;
		} catch (Exception e) {
			LOG.error("Fehler beim Aktualisieren der sequence_table: " + e.getMessage(), e);
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getProtokolle()
	 */
	@Override
	public String[] getProtokolle() {
		return new String[] { "db" };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getPersistenceLocation()
	 */
	@Override
	public String getPersistenceLocation() {
		return jdbcTemplateWrapper.getDBUrl() + "\n" + jdbcTemplateWrapper.getDBUserName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getUrheberRepository()
	 */
	@Override
	public IUrheberRepository getUrheberRepository() {
		return urheberRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getRaetselRepository()
	 */
	@Override
	public IMcRaetselRepository getRaetselRepository() {
		return raetselRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IPersistenceservice#getMcAufgabenRepository()
	 */
	@Override
	public IMcAufgabenRepository getMcAufgabenRepository() {
		return mcAufgabenRepository;
	}
}
