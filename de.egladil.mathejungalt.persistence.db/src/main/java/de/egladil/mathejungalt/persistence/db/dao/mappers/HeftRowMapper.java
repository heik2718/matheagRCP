/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import de.egladil.mathejungalt.domain.aufgaben.Heft;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IHefteTableNames;

/**
 * @author winkelv
 */
public class HeftRowMapper implements ParameterizedRowMapper<Heft> {

	/**
	 * 
	 */
	public HeftRowMapper() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public Heft mapRow(ResultSet pArg0, int pArg1) throws SQLException {
		Heft heft = new Heft();
		heft.setId(pArg0.getLong(IHefteTableNames.COL_ID));
		heft.setSchluessel(pArg0.getString(IHefteTableNames.COL_SCHLUESSEL));
		heft.setBeendet(pArg0.getInt(IHefteTableNames.COL_BEENDET));
		heft.setTitel(pArg0.getString(IHefteTableNames.COL_TITEL));
		heft.setAufgabenGeladen(false);
		return heft;
	}

}
