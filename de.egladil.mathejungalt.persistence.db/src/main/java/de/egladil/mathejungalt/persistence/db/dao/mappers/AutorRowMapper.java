/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import de.egladil.mathejungalt.domain.medien.Autor;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IAutorenTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IColumnNamesMitSchluessel;

/**
 * @author Winkelv
 */
public class AutorRowMapper implements ParameterizedRowMapper<Autor> {

	/**
	 * 
	 */
	public AutorRowMapper() {
		super();
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public Autor mapRow(ResultSet pArg0, int pArg1) throws SQLException {
		Autor autor = new Autor();
		autor.setId(pArg0.getLong(IColumnNamesMitSchluessel.COL_ID));
		autor.setSchluessel(pArg0.getString(IColumnNamesMitSchluessel.COL_SCHLUESSEL));
		autor.setName(pArg0.getString(IAutorenTableNames.COL_NAME));
		return autor;
	}

}
