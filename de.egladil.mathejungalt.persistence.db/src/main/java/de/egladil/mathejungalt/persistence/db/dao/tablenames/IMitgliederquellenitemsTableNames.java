/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.tablenames;

/**
 * @author Winkelv
 */
public interface IMitgliederquellenitemsTableNames extends ICommonColumnNamesOhneSchluessel {

	/** */
	public static final String TABLE = "mitgliederquellenitems";

	/** */
	public static final String COL_MITGLIED_ID = "mitglied_id";

	/** */
	public static final String COL_QUELLE_ID = "quelle_id";

	/** */
	public static final String COL_JAHRE = "jahre";

	/** */
	public static final String COL_KLASSE = "klasse";
}
