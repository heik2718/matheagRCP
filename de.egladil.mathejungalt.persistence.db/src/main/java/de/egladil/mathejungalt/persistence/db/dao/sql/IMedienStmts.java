/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao.sql;

import de.egladil.mathejungalt.persistence.db.dao.tablenames.IAutorenTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMedienTableNames;

/**
 * @author aheike
 */
public interface IMedienStmts {

	/** */
	public static final String SELECT_STMT = "select " + IMedienTableNames.COL_ID + ", "
		+ IMedienTableNames.COL_SCHLUESSEL + ", " + IMedienTableNames.COL_ART + ", " + IMedienTableNames.COL_TITEL
		+ " from " + IMedienTableNames.TABLE;

	/** */
	public static final String SELECT_BY_STMT = "select " + IMedienTableNames.COL_ID + " from "
		+ IMedienTableNames.TABLE + " where lower(" + IMedienTableNames.COL_TITEL + ") like ?";

	/** */
	public static final String SELECT_MAX_KEY = "select max(" + IMedienTableNames.COL_SCHLUESSEL + ") as "
		+ IMedienTableNames.COL_SCHLUESSEL + " from " + IMedienTableNames.TABLE;

	/** */
	public static final String INSERT_STMT = "insert into " + IMedienTableNames.TABLE + " (" + IMedienTableNames.COL_ID
		+ ", " + IMedienTableNames.COL_SCHLUESSEL + ", " + IMedienTableNames.COL_ART + ", "
		+ IMedienTableNames.COL_TITEL + ") values (?, ?, ?, ?)";

	/** */
	public static final String UPDATE_STMT = "update " + IMedienTableNames.TABLE + " set "
		+ IMedienTableNames.COL_SCHLUESSEL + " = ?, " + IMedienTableNames.COL_TITEL + " = ? where "
		+ IMedienTableNames.COL_ID + " = ? ";

	/** */
	public static final String DELETE_STMT = "delete from " + IMedienTableNames.TABLE + " where "
		+ IMedienTableNames.COL_ID + " = ?";

	/** */
	public static final String INSERT_B2A_STMT = "insert into " + IAutorenTableNames.TABLE_B2A + " ("
		+ IAutorenTableNames.COL_BUCH_ID + ", " + IAutorenTableNames.COL_AUTOR_ID + ") values (?, ?)";

	/** **/
	public static final String DELETE_B2A_STMT = "delete from " + IAutorenTableNames.TABLE_B2A + " where "
		+ IAutorenTableNames.COL_BUCH_ID + " = ? ";

}
