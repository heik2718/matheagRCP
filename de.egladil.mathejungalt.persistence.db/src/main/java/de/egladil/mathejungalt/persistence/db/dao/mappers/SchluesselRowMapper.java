/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import de.egladil.mathejungalt.persistence.db.dao.tablenames.IColumnNamesMitSchluessel;

/**
 * @author Winkelv
 */
public class SchluesselRowMapper implements ParameterizedRowMapper<String> {

	/**
	 *
	 */
	public SchluesselRowMapper() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public String mapRow(ResultSet pArg0, int pArg1) throws SQLException {
		return pArg0.getString(IColumnNamesMitSchluessel.COL_SCHLUESSEL);
	}
}
