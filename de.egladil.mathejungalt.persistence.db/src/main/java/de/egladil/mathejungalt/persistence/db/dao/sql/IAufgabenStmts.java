/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.sql;

import de.egladil.mathejungalt.persistence.db.dao.tablenames.IArbeitsblaetterTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IArbeitsblattitemsTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IAufgabenTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IHefteTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMinikaenguruitemsTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IQuellenTableNames;

/**
 * @author aheike
 */
public interface IAufgabenStmts {

	/** ... from aufgaben a left join quellen q on a.quelle_id = q.id left outer join hefte h on a.heft_id = h.id */
	public static final String SELECT_STMT = "select a." + IAufgabenTableNames.COL_ID + ", a."
		+ IAufgabenTableNames.COL_VERZEICHNIS + ", a." + IAufgabenTableNames.COL_ABGELAUFEN_SERIE + ", a."
		+ IAufgabenTableNames.COL_SCHLUESSEL + ", a." + IAufgabenTableNames.COL_THEMA + ", a."
		+ IAufgabenTableNames.COL_STUFE + ", a." + IAufgabenTableNames.COL_JAHRESZEIT + ", a."
		+ IAufgabenTableNames.COL_ART + ", a." + IAufgabenTableNames.COL_ZWECK + ", a." + IAufgabenTableNames.COL_TITEL
		+ ", a." + IAufgabenTableNames.COL_QUELLE_ID + ", a." + IAufgabenTableNames.COL_HEFT_ID + ", a."
		+ IAufgabenTableNames.COL_BESCHREIBUNG + ", h." + IHefteTableNames.COL_SCHLUESSEL + " as "
		+ IAufgabenTableNames.COL_HEFT_SCHLUESSEL + ", h." + IHefteTableNames.COL_TITEL + " as "
		+ IAufgabenTableNames.COL_HEFT_TITEL + ", h." + IHefteTableNames.COL_BEENDET + " as "
		+ IAufgabenTableNames.COL_HEFT_BEENDET + ", q." + IQuellenTableNames.COL_ART + " as "
		+ IAufgabenTableNames.COL_QUELLE_ART + ", q." + IQuellenTableNames.COL_SCHLUESSEL + " as "
		+ IQuellenTableNames.ALIAS_QUELLE_SCHLUESSEL + " from " + IAufgabenTableNames.TABLE + " a left join "
		+ IQuellenTableNames.TABLE + " q on a." + IAufgabenTableNames.COL_QUELLE_ID + " = q."
		+ IQuellenTableNames.COL_ID + " left outer join " + IHefteTableNames.TABLE + " h on a."
		+ IAufgabenTableNames.COL_HEFT_ID + " = h." + IHefteTableNames.COL_ID;

	/** */
	public static final String SELECT_MAX_KEY_STMT = "select max(" + IAufgabenTableNames.COL_SCHLUESSEL + ") as "
		+ IAufgabenTableNames.COL_SCHLUESSEL + " from " + IAufgabenTableNames.TABLE;

	/** */
	public static final String SELECT_ID_BY_SCHLUESSEL_STMT = "select " + IAufgabenTableNames.COL_ID + " from "
		+ IAufgabenTableNames.TABLE + " where " + IAufgabenTableNames.COL_SCHLUESSEL + " = ?";

	/** */
	public static final String INSERT_STMT = "insert into " + IAufgabenTableNames.TABLE + " ("
		+ IAufgabenTableNames.COL_SCHLUESSEL + ", " + IAufgabenTableNames.COL_THEMA + ", "
		+ IAufgabenTableNames.COL_STUFE + ", " + IAufgabenTableNames.COL_JAHRESZEIT + ", "
		+ IAufgabenTableNames.COL_ART + ", " + IAufgabenTableNames.COL_ZWECK + ", " + IAufgabenTableNames.COL_TITEL
		+ ", " + IAufgabenTableNames.COL_HEFT_ID + ", " + IAufgabenTableNames.COL_QUELLE_ID + ", "
		+ IAufgabenTableNames.COL_BESCHREIBUNG + ", " + IAufgabenTableNames.COL_ABGELAUFEN_SERIE + ", "
		+ IAufgabenTableNames.COL_VERZEICHNIS + ", " + IAufgabenTableNames.COL_ID
		+ ") values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	/** */
	public static final String UPDATE_STMT = "update " + IAufgabenTableNames.TABLE + " set "
		+ IAufgabenTableNames.COL_SCHLUESSEL + " = ?, " + IAufgabenTableNames.COL_THEMA + " = ?, "
		+ IAufgabenTableNames.COL_STUFE + " = ?, " + IAufgabenTableNames.COL_JAHRESZEIT + " = ?, "
		+ IAufgabenTableNames.COL_ART + " = ?, " + IAufgabenTableNames.COL_ZWECK + " = ?, "
		+ IAufgabenTableNames.COL_TITEL + " = ?, " + IAufgabenTableNames.COL_HEFT_ID + " = ?, "
		+ IAufgabenTableNames.COL_QUELLE_ID + " = ?, " + IAufgabenTableNames.COL_BESCHREIBUNG + " = ?, "
		+ IAufgabenTableNames.COL_ABGELAUFEN_SERIE + " = ?, " + IAufgabenTableNames.COL_VERZEICHNIS + " = ? "
		+ "where " + IAufgabenTableNames.COL_ID + " = ?";

	/** */
	public static final String DELETE_STMT = "delete from " + IAufgabenTableNames.TABLE + " where "
		+ IAufgabenTableNames.COL_ID + " = ?";

	/**
	 * select i.aufgabe_id as aufgabe_id from matheag.serienitems i, matheag.serien s where i.serie_id = s.id and
	 * i.aufgabe_id = 811 and s.runde >= 5 and i.aufgabe_id not in (select ai.aufgabe_id as aufgabe_id from
	 * matheag.arbeitsblattitems ai, matheag.arbeitsblaetter a where ai.arbblatt_id = a.id and ai.aufgabe_id = 811 and
	 * a.sperrend = 1)
	 */
	public static final String SELECT_ID_AUFGABE_SERIE = "select i.aufgabe_id as aufgabe_id from serienitems i, serien s where i.serie_id = s.id and i.aufgabe_id = ? and s.runde >= ? and i.aufgabe_id not in (select ai.aufgabe_id as aufgabe_id from arbeitsblattitems ai, arbeitsblaetter a where ai.arbblatt_id = a.id and ai.aufgabe_id = ? and a.sperrend = ? )";

	/** */
	public static final String SELECT_ANZAHL_MINIKAENGURUITEM = "select count(" + IMinikaenguruitemsTableNames.COL_ID
		+ ") as anzahl from " + IMinikaenguruitemsTableNames.TABLE + " where "
		+ IMinikaenguruitemsTableNames.COL_AUFGABE_ID + " = ?";

	/** */
	public static final String SELECT_ANZAHL_ARBEITSBLAETTER = "select count(i." + IArbeitsblattitemsTableNames.COL_ID
		+ ") from " + IArbeitsblattitemsTableNames.TABLE + " i, " + IArbeitsblaetterTableNames.TABLE + " a where a."
		+ IArbeitsblaetterTableNames.COL_ID + " = i." + IArbeitsblattitemsTableNames.COL_ARBEITSBLATT_ID + " and a."
		+ IArbeitsblaetterTableNames.COL_SPERREND + " = ? and i." + IArbeitsblattitemsTableNames.COL_AUFGABE_ID
		+ " = ?";

	public static final String FIND_SERIE_ID_BY_AUFGABE = "select i.serie_id from serienitems i where i.aufgabe_id = ?";

	public static final String CHANGE_ZU_SCHLECHT = "update aufgaben set " + IAufgabenTableNames.COL_ABGELAUFEN_SERIE
		+ " = ? where id = ?";

}
