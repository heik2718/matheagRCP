/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.mysql.IJdbcTemplateWrapper;
import de.egladil.mathejungalt.domain.medien.Buch;
import de.egladil.mathejungalt.domain.medien.Zeitschrift;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.domain.quellen.AbstractQuelle;
import de.egladil.mathejungalt.domain.quellen.Buchquelle;
import de.egladil.mathejungalt.domain.quellen.Kindquelle;
import de.egladil.mathejungalt.domain.quellen.MitgliederQuelleItem;
import de.egladil.mathejungalt.domain.quellen.MitgliedergruppenQuelle;
import de.egladil.mathejungalt.domain.quellen.NullQuelle;
import de.egladil.mathejungalt.domain.quellen.Zeitschriftquelle;
import de.egladil.mathejungalt.domain.types.EnumTypes.Quellenart;
import de.egladil.mathejungalt.persistence.api.IMedienRepository;
import de.egladil.mathejungalt.persistence.api.IMitgliederRepository;
import de.egladil.mathejungalt.persistence.api.IQuellenRepository;
import de.egladil.mathejungalt.persistence.db.dao.mappers.MitgliederquellenitemRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.QuelleRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.SchluesselRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.sql.IQuellenStmts;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMitgliederquellenitemsTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IQuellenTableNames;
import de.egladil.mathejungalt.persistence.db.dao.typehandler.QuellenArtTypeHandler;

/**
 * @author Winkelv
 */
public class QuellenDao extends AbstractDao implements IQuellenRepository {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(QuellenDao.class);

	/** */
	private Map<Long, AbstractQuelle> quellenIdMap;

	/** */
	private Map<String, AbstractQuelle> quellenSchluesselMap;

	/** */
	private IMitgliederRepository mitgliederRepository;

	/** */
	private IMedienRepository medienRepository;

	/**
	 * @param pJdbcTemplateWrapper
	 */
	public QuellenDao(IJdbcTemplateWrapper pJdbcTemplateWrapper, IdGenerator idGenerator) {
		super(pJdbcTemplateWrapper, idGenerator);
		quellenIdMap = new HashMap<Long, AbstractQuelle>();
		quellenSchluesselMap = new HashMap<String, AbstractQuelle>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.quellen.IQuellenRepository#findById(java.lang.Long)
	 */
	@Override
	public AbstractQuelle findById(Long pId) {
		checkQuellenMapInizialized();
		return quellenIdMap.get(pId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.quellen.IQuellenRepository#findBySchluessel(java.lang.String)
	 */
	@Override
	public AbstractQuelle findBySchluessel(String pSchluessel) {
		checkQuellenMapInizialized();
		return quellenSchluesselMap.get(pSchluessel);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.core.persistenceapi.quellen.IQuellenRepository#completeQuelle(de.egladil.mathe.core.domain.
	 * quellen. AbstractQuelle)
	 */
	@Override
	public void completeQuelle(final AbstractQuelle pQuelle) {
		if (pQuelle == null) {
			throw new MatheJungAltException("null an completeQuelle uebergeben");
		}
		if (pQuelle.isCompletelyLoaded() || pQuelle.isNew())
			return;
		final QuelleRowMapper quelleRowMapper = new QuelleRowMapper(mitgliederRepository, medienRepository, pQuelle);
		final MitgliederquellenitemRowMapper itemRowMapper = new MitgliederquellenitemRowMapper(mitgliederRepository);
		final Object[] args = new Object[] { pQuelle.getId() };
		switch (pQuelle.getArt()) {
		case K:
			getJdbcTemplate().query(IQuellenStmts.SELECT_K_QUELLE_BY_ID_STMT, quelleRowMapper, args);
			break;
		case B:
			getJdbcTemplate().query(IQuellenStmts.SELECT_B_QUELLE_BY_ID_STMT, quelleRowMapper, args);
			break;
		case Z:
			getJdbcTemplate().query(IQuellenStmts.SELECT_Z_QUELLE_BY_ID_STMT, quelleRowMapper, args);
			break;
		case G:
			List<MitgliederQuelleItem> trefferliste = getJdbcTemplate().query(IQuellenStmts.SELECT_G_QUELLE_BY_ID_STMT,
				itemRowMapper, args);
			for (MitgliederQuelleItem item : trefferliste) {
				item.setQuelle((MitgliedergruppenQuelle) pQuelle);
				((MitgliedergruppenQuelle) pQuelle).addItem(item);
			}
			break;
		default:
			getJdbcTemplate().query(IQuellenStmts.SELECT_N_QUELLE_BY_ID_STMT, quelleRowMapper, args);
			break;
		}
		pQuelle.setCompletelyLoaded(true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.persistenceapi.quellen.IQuellenRepository#findAllByBuch(de.egladil.mathe.core.domain.medien
	 * .Buch)
	 */
	@Override
	public List<Buchquelle> findAllByBuch(Buch pBuch) {
		checkBuchParam(pBuch);
		final QuelleRowMapper rowMapper = new QuelleRowMapper(medienRepository);
		final Object[] args = new Object[] { pBuch.getId() };
		List<AbstractQuelle> liste = getJdbcTemplate().query(IQuellenStmts.SELECT_B_QUELLE_BY_BUCH_STMT, rowMapper,
			args);
		List<Buchquelle> erg = new ArrayList<Buchquelle>();
		checkQuellenMapInizialized();
		for (AbstractQuelle q : liste) {
			addToCache(q);
			q.setCompletelyLoaded(true);
			Buchquelle bq = (Buchquelle) q;
			erg.add(bq);
		}
		return erg;
	}

	/**
	 * @param pBuch
	 */
	private void checkBuchParam(Buch pBuch) {
		if (pBuch == null || pBuch.isNew())
			throw new MatheJungAltException("pBuch ist null oder noch nicht persistent.");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.persistenceapi.quellen.IQuellenRepository#findAllByMitglied(de.egladil.mathe.core.domain
	 * .mitglieder .Mitglied)
	 */
	@Override
	public List<Kindquelle> findAllByMitglied(Mitglied pMitglied) {
		checkMitgliedParam(pMitglied);
		final QuelleRowMapper rowMapper = new QuelleRowMapper(mitgliederRepository);
		final Object[] args = new Object[] { pMitglied.getId() };
		List<AbstractQuelle> liste = getJdbcTemplate().query(IQuellenStmts.SELECT_K_QUELLE_BY_KIND_STMT, rowMapper,
			args);
		List<Kindquelle> erg = new ArrayList<Kindquelle>();
		checkQuellenMapInizialized();
		for (AbstractQuelle q : liste) {
			addToCache(q);
			q.setCompletelyLoaded(true);
			Kindquelle kq = (Kindquelle) q;
			erg.add(kq);
		}
		return erg;
	}

	/**
	 * @param pMitglied
	 */
	private void checkMitgliedParam(Mitglied pMitglied) {
		if (pMitglied == null || pMitglied.isNew())
			throw new MatheJungAltException("pMitglied ist null oder noch nicht persistent.");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.quellen.IQuellenRepository#findNullQuelle()
	 */
	@Override
	public NullQuelle findNullQuelle() {
		final QuelleRowMapper rowMapper = new QuelleRowMapper();
		Quellenart art = Quellenart.N;
		Object[] args = new Object[] { new QuellenArtTypeHandler().getSqlValue(art) };
		List<AbstractQuelle> liste = getJdbcTemplate().query(IQuellenStmts.SELECT_N_QUELLE, rowMapper, args);
		if (liste.size() != 1)
			throw new MatheJungAltException("Es gibt keine oder mehr als eine Quelle mit der Art N in der DB!");
		checkQuellenMapInizialized();
		NullQuelle nq = (NullQuelle) liste.get(0);
		addToCache(nq);
		nq.setCompletelyLoaded(true);
		return nq;
	}

	/**
	 *
	 */
	private void checkQuellenMapInizialized() {
		if (quellenIdMap == null) {
			quellenIdMap = new HashMap<Long, AbstractQuelle>();
		}
		if (quellenSchluesselMap == null) {
			quellenSchluesselMap = new HashMap<String, AbstractQuelle>();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.persistenceapi.quellen.IQuellenRepository#findAllByZeitschrift(de.egladil.mathe.core.domain
	 * .medien. Zeitschrift)
	 */
	@Override
	public List<Zeitschriftquelle> findAllByZeitschrift(Zeitschrift pZeitschrift) {
		checkZeitschriftParam(pZeitschrift);
		final QuelleRowMapper rowMapper = new QuelleRowMapper(medienRepository);
		final Object[] args = new Object[] { pZeitschrift.getId() };
		List<AbstractQuelle> liste = getJdbcTemplate().query(IQuellenStmts.SELECT_Z_QUELLE_BY_ZEITSCHR_STMT, rowMapper,
			args);
		List<Zeitschriftquelle> erg = new ArrayList<Zeitschriftquelle>();
		checkQuellenMapInizialized();
		for (AbstractQuelle q : liste) {
			addToCache(q);
			q.setCompletelyLoaded(true);
			Zeitschriftquelle zq = (Zeitschriftquelle) q;
			erg.add(zq);
		}
		return erg;
	}

	/**
	 * @param pZeitschrift
	 */
	private void checkZeitschriftParam(Zeitschrift pZeitschrift) {
		if (pZeitschrift == null || pZeitschrift.isNew())
			throw new MatheJungAltException("pZeitschrift ist null oder noch nicht persistent.");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.persistenceapi.quellen.IQuellenRepository#findBuchquelle(de.egladil.mathe.core.domain.medien
	 * .Buch, int)
	 */
	@Override
	public Buchquelle findBuchquelle(Buch pBuch, int pSeite) {
		checkBuchParam(pBuch);
		final QuelleRowMapper rowMapper = new QuelleRowMapper(medienRepository);
		Object[] args = new Object[] { pBuch.getId(), pSeite };
		List<AbstractQuelle> liste = getJdbcTemplate().query(IQuellenStmts.SELECT_B_QUELLE_BY_BUCH_SEITE_STMT,
			rowMapper, args);
		if (liste.size() == 0)
			return null;
		if (liste.size() > 1)
			throw new MatheJungAltException("Es gibt mehr als eine Buchquelle mit " + pBuch + " und Seite " + pSeite
				+ " in der DB!");
		checkQuellenMapInizialized();
		Buchquelle bq = (Buchquelle) liste.get(0);
		addToCache(bq);
		bq.setCompletelyLoaded(true);
		return bq;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.core.persistenceapi.quellen.IQuellenRepository#findKindquelle(de.egladil.mathe.core.domain.
	 * mitglieder.Mitglied , int, int)
	 */
	@Override
	public Kindquelle findKindquelle(Mitglied pMitglied, int pJahre, int pKlasse) {
		checkMitgliedParam(pMitglied);
		final QuelleRowMapper rowMapper = new QuelleRowMapper(mitgliederRepository);
		Object[] args = new Object[] { pMitglied.getId(), pJahre, pKlasse };
		List<AbstractQuelle> liste = getJdbcTemplate().query(IQuellenStmts.SELECT_K_QUELLE_BY_KIND_ALTER_KLASSE_STMT,
			rowMapper, args);
		if (liste.size() == 0)
			return null;
		if (liste.size() > 1)
			throw new MatheJungAltException("Es gibt mehr als eine Buchquelle mit " + pMitglied + ", Klasse " + pKlasse
				+ " und Alter " + pJahre + " in der DB!");
		checkQuellenMapInizialized();
		Kindquelle bq = (Kindquelle) liste.get(0);
		addToCache(bq);
		bq.setCompletelyLoaded(true);
		return bq;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.persistenceapi.quellen.IQuellenRepository#findZeitschriftquelle(de.egladil.mathe.core.domain
	 * .medien .Zeitschrift, int, int)
	 */
	@Override
	public Zeitschriftquelle findZeitschriftquelle(Zeitschrift pZeitschrift, int pAusgabe, int pJahrgang) {
		checkZeitschriftParam(pZeitschrift);
		final QuelleRowMapper rowMapper = new QuelleRowMapper(medienRepository);
		Object[] args = new Object[] { pZeitschrift.getId(), pAusgabe, pJahrgang };
		List<AbstractQuelle> liste = getJdbcTemplate().query(IQuellenStmts.SELECT_Z_QUELLE_BY_ZEITSCHR_AUSG_JAHRG_STMT,
			rowMapper, args);
		if (liste.size() == 0) {
			return null;
		}
		if (liste.size() > 1) {
			throw new MatheJungAltException("Es gibt mehr als eine Zeitschriftquelle mit " + pZeitschrift
				+ ", Ausgabe " + pAusgabe + " und Jahrgang " + pJahrgang + " in der DB!");
		}
		checkQuellenMapInizialized();
		Zeitschriftquelle zq = (Zeitschriftquelle) liste.get(0);
		addToCache(zq);
		zq.setCompletelyLoaded(true);
		return zq;
	}

	/**
	 * @param pQuelle
	 */
	private void addToCache(AbstractQuelle pQuelle) {
		checkQuellenMapInizialized();
		quellenIdMap.put(pQuelle.getId(), pQuelle);
		quellenSchluesselMap.put(pQuelle.getSchluessel(), pQuelle);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.quellen.IQuellenRepository#getMaxKey()
	 */
	@Override
	public String getMaxKey() {
		return getJdbcTemplate().queryForObject(IQuellenStmts.SELECT_MAX_KEY_STMT, new SchluesselRowMapper());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.core.persistenceapi.quellen.IQuellenRepository#save(de.egladil.mathe.core.domain.quellen.
	 * AbstractQuelle)
	 */
	@Override
	public void save(AbstractQuelle pQuelle) {
		if (pQuelle == null)
			throw new MatheJungAltException("null an save(AbstractQuelle) uebergeben");
		if (!pQuelle.isNew())
			return;
		Long id = getIdGenerator().nextId(IQuellenTableNames.TABLE);
		String stmt = null;
		Object[] args = null;
		Quellenart art = pQuelle.getArt();
		String artSql = new QuellenArtTypeHandler().getSqlValue(art);
		switch (art) {
		case B:
			stmt = IQuellenStmts.INSERT_BUCHQUELLE_STMT;
			args = new Object[] { pQuelle.getSchluessel(), artSql, ((Buchquelle) pQuelle).getBuch().getId(),
				((Buchquelle) pQuelle).getSeite(), id };
			break;
		case K:
			stmt = IQuellenStmts.INSERT_KINDQUELLE_STMT;
			args = new Object[] { pQuelle.getSchluessel(), artSql, ((Kindquelle) pQuelle).getMitglied().getId(),
				((Kindquelle) pQuelle).getKlasse(), ((Kindquelle) pQuelle).getJahre(), id };
			break;
		case Z:
			stmt = IQuellenStmts.INSERT_ZEITSCHRIFTQUELLE_STMT;
			args = new Object[] { pQuelle.getSchluessel(), artSql,
				((Zeitschriftquelle) pQuelle).getZeitschrift().getId(), ((Zeitschriftquelle) pQuelle).getAusgabe(),
				((Zeitschriftquelle) pQuelle).getJahrgang(), id };
			break;
		case G:
			insertMigliedergruppenquelle((MitgliedergruppenQuelle) pQuelle, id);
			addToCache(pQuelle);
			getIdGenerator().increase(IQuellenTableNames.TABLE);
			return;
		default:
			return;
		}
		try {
			getJdbcTemplate().update(stmt, args);
			pQuelle.setId(id);
			addToCache(pQuelle);
			getIdGenerator().increase(IQuellenTableNames.TABLE);
		} catch (DataAccessException e) {
			LOG.error("Fehler beim Speichern der Quelle: " + logAttributes(pQuelle));
			throw e;
		}
	}

	/**
	 * @param pQuelle
	 * @param pId
	 */
	private void insertMigliedergruppenquelle(final MitgliedergruppenQuelle pQuelle, final Long pId) {
		getTransactionTemplate().execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(TransactionStatus pArg0) {
				getJdbcTemplate().update(IQuellenStmts.INSERT_GRUPPENQUELLE_STMT,
					new Object[] { pQuelle.getSchluessel(), "G", pId });
				pQuelle.setId(pId);

				for (MitgliederQuelleItem item : pQuelle.getItems()) {
					persistMitgliederquelleitem(item);
				}
			}
		});
	}

	/**
	 * @param pItem
	 * @param pQuelle
	 */
	private void persistMitgliederquelleitem(final MitgliederQuelleItem pItem) {
		Long neueId = getIdGenerator().nextId(IMitgliederquellenitemsTableNames.TABLE);
		getJdbcTemplate().update(
			IQuellenStmts.INSERT_GRUPPENQUELLEN_ITEM,
			new Object[] { neueId, pItem.getMitglied().getId(), pItem.getQuelle().getId(), pItem.getKlasse(),
				pItem.getJahre() });
		pItem.setId(neueId);
		getIdGenerator().increase(IMitgliederquellenitemsTableNames.TABLE);
	}

	/**
	 * @param pQuelle
	 * @return
	 */
	private String logAttributes(AbstractQuelle pQuelle) {
		StringBuffer sb = new StringBuffer();
		if (pQuelle.getId() != null) {
			sb.append("[ID = ");
			sb.append(pQuelle.getId());
		}
		sb.append("], [SCHLUESSEL = ");
		sb.append(pQuelle.getSchluessel());
		sb.append("], [ART = ");
		sb.append(pQuelle.getArt().toString());
		if (Quellenart.B.equals(pQuelle.getArt())) {
			sb.append("], [BUCH_ID = ");
			sb.append(((Buchquelle) pQuelle).getBuch().getId());
			sb.append("], [SEITE = ");
			sb.append(((Buchquelle) pQuelle).getSeite());
		}
		if (Quellenart.K.equals(pQuelle.getArt())) {
			sb.append("], [MITGLIED_ID = ");
			sb.append(((Kindquelle) pQuelle).getMitglied().getId());
			sb.append("], [JAHRE = ");
			sb.append(((Kindquelle) pQuelle).getJahre());
			sb.append("], [KLASSE = ");
			sb.append(((Kindquelle) pQuelle).getKlasse());
		}
		if (Quellenart.Z.equals(pQuelle.getArt())) {
			sb.append("], [ZEITSCHIFT_ID = ");
			sb.append(((Zeitschriftquelle) pQuelle).getZeitschrift().getId());
			sb.append("], [JAHRGANG = ");
			sb.append(((Zeitschriftquelle) pQuelle).getJahrgang());
			sb.append("], [AUSGABE = ");
			sb.append(((Zeitschriftquelle) pQuelle).getAusgabe());
		}
		sb.append("]");
		return sb.toString();
	}

	/**
	 * @param pQuelle
	 */
	public void remove(AbstractQuelle pQuelle) {
		if (pQuelle == null || pQuelle.isNew())
			return;
		Object[] args = new Object[] { pQuelle.getId() };
		try {
			getJdbcTemplate().update(IQuellenStmts.DELETE_STMT, args);
			if (quellenIdMap != null) {
				quellenIdMap.remove(pQuelle.getId());
			}
			if (quellenSchluesselMap != null) {
				quellenSchluesselMap.remove(pQuelle.getSchluessel());
			}
		} catch (DataAccessException e) {
			LOG.error("Fehler beim L�schen der Quelle " + logAttributes(pQuelle));
			throw e;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.persistenceapi.quellen.IQuellenRepository#register(de.egladil.mathe.core.domain.quellen
	 * .AbstractQuelle)
	 */
	@Override
	public void register(AbstractQuelle pQuelle) {
		if (pQuelle == null || pQuelle.getId() == null)
			throw new MatheJungAltException("Quelle oder Quelle.ID ist null!!!");
		checkQuellenMapInizialized();
		if (!quellenIdMap.containsKey(pQuelle.getId())) {
			quellenIdMap.put(pQuelle.getId(), pQuelle);
		}
		if (!quellenSchluesselMap.containsKey(pQuelle.getSchluessel())) {
			quellenSchluesselMap.put(pQuelle.getSchluessel(), pQuelle);
		}
	}

	/**
	 * @param pMitgliederRepository the mitgliederRepository to set
	 */
	public void setMitgliederRepository(IMitgliederRepository pMitgliederRepository) {
		mitgliederRepository = pMitgliederRepository;
	}

	/**
	 * @param pMedienRepository the medienRepository to set
	 */
	public void setMedienRepository(IMedienRepository pMedienRepository) {
		medienRepository = pMedienRepository;
	}

}
