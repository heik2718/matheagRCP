/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serienitem;
import de.egladil.mathejungalt.persistence.api.IAufgabenRepository;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.ISerienitemsTableNames;

/**
 * @author winkelv
 */
public class SerienitemRowMapper implements ParameterizedRowMapper<Serienitem> {

	/** */
	private Serie serie;

	/** */
	private IAufgabenRepository aufgabenRepository;

	/**
	 * @param pAufgabenRepository
	 * @param pSerie
	 */
	public SerienitemRowMapper(IAufgabenRepository pAufgabenRepository, Serie pSerie) {
		super();
		aufgabenRepository = pAufgabenRepository;
		serie = pSerie;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public Serienitem mapRow(ResultSet pArg0, int pArg1) throws SQLException {
		Serienitem item = new Serienitem();
		item.setId(pArg0.getLong(ISerienitemsTableNames.COL_ID));
		item.setNummer(pArg0.getString(ISerienitemsTableNames.COL_NUMMER));
		item.setSerie(serie);

		Long aufgId = pArg0.getLong(ISerienitemsTableNames.COL_AUFGABE_ID);
		item.setAufgabe(aufgabenRepository.findById(aufgId));
		serie.addItem(item);

		return item;
	}

}
