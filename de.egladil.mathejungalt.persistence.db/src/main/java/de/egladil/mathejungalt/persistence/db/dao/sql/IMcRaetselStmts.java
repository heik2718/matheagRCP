/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.sql;

import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMcRaetselTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMcRaetselitemsTableNames;

/**
 * @author aheike
 */
public interface IMcRaetselStmts {

	/** */
	public static final String SELECT_STMT = "select " + IMcRaetselTableNames.COL_ID + ", "
		+ IMcRaetselTableNames.COL_AUTOR_ID + ", " + IMcRaetselTableNames.COL_LICENSE_SHORT + ", "
		+ IMcRaetselTableNames.COL_LICENSE_FULL + ", " + IMcRaetselTableNames.COL_SCHLUESSEL + ", "
		+ IMcRaetselTableNames.COL_TITEL + ", " + IMcRaetselTableNames.COL_STUFE + ", "
		+ IMcRaetselTableNames.COL_PUBLISHED + ", " + IMcRaetselTableNames.COL_PRIVAT + " from "
		+ IMcRaetselTableNames.TABLE;

	/** */
	public static final String SELECT_ITEMS_ZU_RAETSEL_STMT = "select " + IMcRaetselitemsTableNames.COL_ID + ", "
		+ IMcRaetselitemsTableNames.COL_MC_AUFGABE_ID + ", " + IMcRaetselitemsTableNames.COL_NUMMER + " from "
		+ IMcRaetselitemsTableNames.TABLE + " where " + IMcRaetselitemsTableNames.COL_RAETSEL_ID + " = ?";

	/** */
	public static final String SELECT_MAX_KEY_STMT = "select max(" + IMcRaetselTableNames.COL_SCHLUESSEL + ") as "
		+ IMcRaetselTableNames.COL_SCHLUESSEL + " from " + IMcRaetselTableNames.TABLE + " where "
		+ IMcRaetselTableNames.COL_SCHLUESSEL + " not like ?";

	/** */
	public static final String SELECT_RAETSELITEM_ID_ZU_RAETSEL_STMT = "select " + IMcRaetselitemsTableNames.COL_ID
		+ " from " + IMcRaetselitemsTableNames.TABLE + " where " + IMcRaetselitemsTableNames.COL_RAETSEL_ID + " = ?";

	/** */
	public static final String INSERT_RAETSEL_STMT = "insert into " + IMcRaetselTableNames.TABLE + " ("
		+ IMcRaetselTableNames.COL_AUTOR_ID + ", " + IMcRaetselTableNames.COL_LICENSE_FULL + ", "
		+ IMcRaetselTableNames.COL_LICENSE_SHORT + ", " + IMcRaetselTableNames.COL_SCHLUESSEL + ", "
		+ IMcRaetselTableNames.COL_TITEL + ", " + IMcRaetselTableNames.COL_STUFE + ", "
		+ IMcRaetselTableNames.COL_PUBLISHED + ", " + IMcRaetselTableNames.COL_PRIVAT + ", "
		+ IMcRaetselTableNames.COL_ID + " ) values (?, ?, ?, ?, ?, ?, ?, ?, ?)";

	/** */
	public static final String UPDATE_RAETSEL_STMT = "update " + IMcRaetselTableNames.TABLE + " set "
		+ IMcRaetselTableNames.COL_AUTOR_ID + " = ?, " + IMcRaetselTableNames.COL_LICENSE_FULL + " = ?, "
		+ IMcRaetselTableNames.COL_LICENSE_SHORT + " = ?, " + IMcRaetselTableNames.COL_SCHLUESSEL + " = ?, "
		+ IMcRaetselTableNames.COL_TITEL + " = ?, " + IMcRaetselTableNames.COL_STUFE + " = ?, "
		+ IMcRaetselTableNames.COL_PUBLISHED + " = ?, " + IMcRaetselTableNames.COL_PRIVAT + " = ? where "
		+ IMcRaetselTableNames.COL_ID + " = ?";

	/** */
	public static final String INSERT_RAETSELITEM_STMT = "insert into " + IMcRaetselitemsTableNames.TABLE + " ("
		+ IMcRaetselitemsTableNames.COL_RAETSEL_ID + ", " + IMcRaetselitemsTableNames.COL_MC_AUFGABE_ID + ", "
		+ IMcRaetselitemsTableNames.COL_NUMMER + ", " + IMcRaetselitemsTableNames.COL_ID + " ) values (?, ?, ?, ?)";

	/** */
	public static final String UPDATE_RAETSELITEM_STMT = "update " + IMcRaetselitemsTableNames.TABLE + " set "
		+ IMcRaetselitemsTableNames.COL_NUMMER + " = ? where " + IMcRaetselitemsTableNames.COL_ID + " = ?";

	public static final String DELETE_RAETSEL_STMT = "delete from " + IMcRaetselTableNames.TABLE + " where "
		+ IMcRaetselTableNames.COL_ID + " = ?";

	public static final String DELETE_RAETSELITEM_STMT = "delete from " + IMcRaetselitemsTableNames.TABLE + " where "
		+ IMcRaetselitemsTableNames.COL_ID + " = ?";

	public static final String PUBLISH_RAETSEL_STMT = "update " + IMcRaetselTableNames.TABLE + " set "
		+ IMcRaetselTableNames.COL_PUBLISHED + " = ? " + " where " + IMcRaetselTableNames.COL_ID + " = ?";

}
