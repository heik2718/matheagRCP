/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.tablenames;

/**
 * @author Winkelv
 */
public interface IDiplomeTableNames extends ICommonColumnNamesOhneSchluessel {

	/** */
	public static final String TABLE = "diplome";

	/** */
	public static final String COL_ERFINDERPUNKTE = "erfinderpunkte";

	/** */
	public static final String COL_DATUM = "datum";

	/** */
	public static final String COL_MITGLIED_ID = "mitglied_id";
}
