/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import de.egladil.mathejungalt.domain.schulen.Land;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.ILaenderTableNames;

/**
 * @author winkelv
 */
public class LandRowMapper implements ParameterizedRowMapper<Land> {

	/**
	 *
	 */
	public LandRowMapper() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public Land mapRow(ResultSet pArg0, int pArg1) throws SQLException {
		Land land = new Land();
		land.setId(pArg0.getLong(ILaenderTableNames.COL_ID));
		land.setSchluessel(pArg0.getString(ILaenderTableNames.COL_SCHLUESSEL));
		land.setBezeichnung(pArg0.getString(ILaenderTableNames.COL_NAME));
		return land;
	}

}
