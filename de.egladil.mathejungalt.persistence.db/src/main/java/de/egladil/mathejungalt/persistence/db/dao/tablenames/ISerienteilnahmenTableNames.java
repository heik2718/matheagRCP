/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.tablenames;

/**
 * @author Winkelv
 */
public interface ISerienteilnahmenTableNames extends ICommonColumnNamesOhneSchluessel {

	/** */
	public static final String TABLE = "serienteilnahmen";

	/** */
	public static final String COL_PUNKTE = "punkte";

	/** */
	public static final String COL_FRUEHSTARTER = "fruehstarter";

	/** */
	public static final String COL_SERIE_ID = "serie_id";

	/** */
	public static final String COL_MITGLIED_ID = "mitglied_id";
}
