/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.tablenames;

/**
 * @author Winkelv
 */
public interface ISchulkontakteTableNames extends ICommonColumnNamesOhneSchluessel {

	/** */
	public static final String TABLE = "schulen_kontakte";

	/** */
	public static final String COL_SCHULE_ID = "schule_id";

	/** */
	public static final String COL_KONTAKT_ID = "kontakt_id";
}
