/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.tablenames;

/**
 * @author Winkelv
 */
public interface IMitgliederTableNames extends IColumnNamesMitSchluessel {

	/** */
	public static final String TABLE = "mitglieder";

	/** */
	public static final String COL_VORNAME = "vorname";

	/** */
	public static final String COL_NACHNAME = "nachname";

	/** */
	public static final String COL_KLASSE = "klasse";

	/** */
	public static final String COL_JAHRE = "jahre";

	/** */
	public static final String COL_ERFINDERPUNKTE = "erfinderpunkte";

	/** */
	public static final String COL_GEBURTSMONAT = "gebmonat";

	/** */
	public static final String COL_GEBURTSJAHR = "gebjahr";

	/** */
	public static final String COL_KONTAKT = "kontakt";

	/** */
	public static final String COL_FOTOTITEL = "fototitel";

	/** */
	public static final String COL_AKTIV = "aktiv";
}
