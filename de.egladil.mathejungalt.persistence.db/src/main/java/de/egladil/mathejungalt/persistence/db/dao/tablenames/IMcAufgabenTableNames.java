/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.tablenames;

/**
 * @author Winkelv
 */
public interface IMcAufgabenTableNames extends IColumnNamesMitSchluessel {

	/** */
	String TABLE = "mc_aufgaben";

	/** */
	String COL_THEMA = "thema";

	/** */
	String COL_STUFE = "stufe";

	/** */
	String COL_PUNKTE = "punkte";

	/** */
	String COL_ART = "art";

	/** */
	String COL_TITEL = "titel";

	/** */
	String COL_ANTWORT1 = "antwort1";

	/** */
	String COL_ANTWORT2 = "antwort2";

	/** */
	String COL_ANTWORT3 = "antwort3";

	/** */
	String COL_ANTWORT4 = "antwort4";

	/** */
	String COL_ANTWORT5 = "antwort5";

	/** */
	String COL_LOESUNG = "loesung";

	/** */
	String COL_BEMERKUNG = "bemerkung";

	/** */
	String COL_VERZEICHNIS = "verzeichnis";

	/** */
	String COL_QUELLE = "quelle";

	String COL_TABELLE_GENERIEREN = "tabelle_generieren";

	String COL_ANZAHL_ANTWORTEN = "anzahl_antworten";

}
