/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.persistence.db.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;
import de.egladil.mathejungalt.domain.schulen.MinikaenguruTeilnahme;
import de.egladil.mathejungalt.domain.schulen.Schule;
import de.egladil.mathejungalt.persistence.api.IMinikaenguruRepository;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMinikaenguruteilnahmenTableNames;

/**
 * <p>
 * NOTE: THIS WIDGET AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 * 
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class MiniTeilnahmenRowMapper implements ParameterizedRowMapper<MinikaenguruTeilnahme> {

	private IMinikaenguruRepository minikaenguruRepository;

	private Schule schule;

	/**
	 * @param pMinikaenguruRepository
	 * @param pSchule
	 */
	public MiniTeilnahmenRowMapper(IMinikaenguruRepository pMinikaenguruRepository, Schule pSchule) {
		super();
		minikaenguruRepository = pMinikaenguruRepository;
		schule = pSchule;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public MinikaenguruTeilnahme mapRow(ResultSet pArg0, int pArg1) throws SQLException {
		MinikaenguruTeilnahme teilnahme = new MinikaenguruTeilnahme();
		teilnahme.setId(pArg0.getLong(IMinikaenguruteilnahmenTableNames.COL_ID));
		int rueckmeldung = pArg0.getInt(IMinikaenguruteilnahmenTableNames.COL_RUECKMELDUNG);
		teilnahme.setRueckmeldung(rueckmeldung > 0);

		Long miniId = pArg0.getLong(IMinikaenguruteilnahmenTableNames.COL_MINIKAENGURU_ID);
		Minikaenguru mini = minikaenguruRepository.findById(miniId);
		teilnahme.setMinikaenguru(mini);

		teilnahme.setSchule(schule);
		schule.addMinikaenguruTeilnahme(teilnahme);

		return teilnahme;
	}
}
