/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.tablenames;

/**
 * @author Winkelv
 */
public interface ISerienitemsTableNames extends ICommonColumnNamesOhneSchluessel {

	/** */
	public static final String TABLE = "serienitems";

	/** */
	public static final String COL_NUMMER = "nummer";

	/** */
	public static final String COL_SERIE_ID = "serie_id";

	/** */
	public static final String COL_AUFGABE_ID = "aufgabe_id";
}
