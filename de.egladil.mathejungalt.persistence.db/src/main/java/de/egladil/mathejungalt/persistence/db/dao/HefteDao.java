/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.mysql.IJdbcTemplateWrapper;
import de.egladil.mathejungalt.domain.aufgaben.Heft;
import de.egladil.mathejungalt.persistence.api.IHefteRepository;
import de.egladil.mathejungalt.persistence.db.dao.mappers.SchluesselRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.sql.IHefteStmts;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IHefteTableNames;

/**
 * @author Winkelv
 */
public class HefteDao extends AbstractDao implements IHefteRepository {

	/** */
	private Map<Long, Heft> hefteMap = new HashMap<Long, Heft>();

	/**
	 * @param pJdbcTemplateWrapper
	 */
	public HefteDao(IJdbcTemplateWrapper pJdbcTemplateWrapper, IdGenerator idGenerator) {
		super(pJdbcTemplateWrapper, idGenerator);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.aufgabensammlungen.IHefteRepository#findAll()
	 */
	@Override
	public List<Heft> findAll() {
		if (hefteMap == null || hefteMap.size() == 0)
			throw new MatheJungAltException("Die Aufgaben sind noch nicht geladen");
		List<Heft> liste = new ArrayList<Heft>();
		Iterator<Long> iter = hefteMap.keySet().iterator();
		while (iter.hasNext())
			liste.add(hefteMap.get(iter.next()));
		return liste;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.aufgabensammlungen.IHefteRepository#findById(java.lang.Long)
	 */
	@Override
	public Heft findById(Long pId) {
		return hefteMap.get(pId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.persistenceapi.aufgabensammlungen.IHefteRepository#getMaxKey()
	 */
	@Override
	public String getMaxKey() {
		return getJdbcTemplate().query(IHefteStmts.SELECT_MAX_KEY_STMT, new SchluesselRowMapper()).get(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.core.persistenceapi.aufgabensammlungen.IHefteRepository#save(de.egladil.mathe.core.domain.
	 * aufgabensammlungen .hefte.Heft)
	 */
	@Override
	public void save(Heft pHeft) {
		if (pHeft == null)
			throw new MatheJungAltException("null an save(Heft) übergeben");
		if (pHeft.isNew())
			persist(pHeft);
		else
			merge(pHeft);
	}

	/**
	 * insert
	 * 
	 * @param pHeft
	 */
	private void persist(Heft pHeft) {
		Long id = getIdGenerator().nextId(IHefteTableNames.TABLE);
		Object[] args = new Object[] { pHeft.getSchluessel(), pHeft.getTitel(), id };
		getJdbcTemplate().update(IHefteStmts.INSERT_STMT, args);
		pHeft.setId(id);
		hefteMap.put(id, pHeft);
		getIdGenerator().increase(IHefteTableNames.TABLE);
	}

	/**
	 * update
	 * 
	 * @param pHeft
	 */
	private void merge(Heft pHeft) {
		Object[] args = new Object[] { pHeft.getSchluessel(), pHeft.getTitel(), pHeft.getId() };
		getJdbcTemplate().update(IHefteStmts.UPDATE_STMT, args);
	}

	/**
	 * @param pHeft
	 */
	public void remove(Heft pHeft) {
		if (pHeft == null || pHeft.isNew())
			return;
		getJdbcTemplate().update(IHefteStmts.DELETE_STMT, new Object[] { pHeft.getId() });
		if (hefteMap != null)
			hefteMap.remove(pHeft.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.core.persistenceapi.aufgabensammlungen.IHefteRepository#register(de.egladil.mathe.core.domain
	 * .aufgabensammlungen .hefte.Heft)
	 */
	@Override
	public void register(Heft pHeft) {
		if (pHeft == null || pHeft.getId() == null)
			throw new MatheJungAltException("Heft oder Heft.id ist null!!!");
		if (!hefteMap.containsKey(pHeft.getId()))
			hefteMap.put(pHeft.getId(), pHeft);
	}

}
