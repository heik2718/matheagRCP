/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

/**
 * @author Winkelv
 */
public class LongRowMapper implements ParameterizedRowMapper<Long> {

	/**
	 * 
	 */
	public LongRowMapper() {
		super();
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public Long mapRow(ResultSet pArg0, int pArg1) throws SQLException {
		return pArg0.getLong(1);
	}
}
