/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblattitem;
import de.egladil.mathejungalt.persistence.api.IAufgabenRepository;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IArbeitsblattitemsTableNames;

/**
 * @author Winkelv
 */
public class ArbeitsblattitemRowMapper implements ParameterizedRowMapper<Arbeitsblattitem> {

	/** */
	private IAufgabenRepository aufgabenRepository;

	/** */
	private Arbeitsblatt arbeitsblatt;

	/**
	 * @param pAufgabenRepository
	 * @param pArbeitsblatt
	 */
	public ArbeitsblattitemRowMapper(IAufgabenRepository pAufgabenRepository, Arbeitsblatt pArbeitsblatt) {
		super();
		arbeitsblatt = pArbeitsblatt;
		aufgabenRepository = pAufgabenRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public Arbeitsblattitem mapRow(ResultSet pArg0, int pArg1) throws SQLException {
		Arbeitsblattitem item = new Arbeitsblattitem();
		item.setArbeitsblatt(arbeitsblatt);
		item.setId(pArg0.getLong(IArbeitsblattitemsTableNames.COL_ID));
		item.setNummer(pArg0.getString(IArbeitsblattitemsTableNames.COL_NUMMER));
		Long aufgId = pArg0.getLong(IArbeitsblattitemsTableNames.COL_AUFGABE_ID);
		item.setAufgabe(aufgabenRepository.findById(aufgId));
		arbeitsblatt.addItem(item);
		return item;
	}

}
