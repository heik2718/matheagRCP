/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.sql;

import de.egladil.mathejungalt.persistence.db.dao.tablenames.ILaenderTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMinikaenguruteilnahmenTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.ISchulenTableNames;

/**
 * @author aheike
 */
public interface ISchulenStmts {

	public static final String SELECT_MAX_KEY_STMT = "select max(" + ISchulenTableNames.COL_SCHLUESSEL + ") as "
		+ ISchulenTableNames.COL_SCHLUESSEL + " from " + ISchulenTableNames.TABLE;

	/** ... from aufgaben a left join quellen q on a.quelle_id = q.id left outer join hefte h on a.heft_id = h.id */
	public static final String SELECT_LAENDER_STMT = "select l." + ILaenderTableNames.COL_ID + ", l."
		+ ILaenderTableNames.COL_SCHLUESSEL + ", l." + ILaenderTableNames.COL_NAME + " from "
		+ ILaenderTableNames.TABLE + " l";

	/** ... from aufgaben a left join quellen q on a.quelle_id = q.id left outer join hefte h on a.heft_id = h.id */
	public static final String SELECT_STMT = "select s." + ISchulenTableNames.COL_ID + ", s."
		+ ISchulenTableNames.COL_SCHLUESSEL + ", s." + ISchulenTableNames.COL_NAME + ", s."
		+ ISchulenTableNames.COL_ANSCHRIFT + ", s." + ISchulenTableNames.COL_TELEFON + ", s."
		+ ISchulenTableNames.COL_BUNDESLAND + " from " + ISchulenTableNames.TABLE + " s";

	/** */
	public static final String INSERT_STMT = "insert into " + ISchulenTableNames.TABLE + " ("
		+ ISchulenTableNames.COL_SCHLUESSEL + ", " + ISchulenTableNames.COL_NAME + ", "
		+ ISchulenTableNames.COL_ANSCHRIFT + ", " + ISchulenTableNames.COL_TELEFON + ", "
		+ ISchulenTableNames.COL_BUNDESLAND + ", " + ISchulenTableNames.COL_ID + ") values (?, ?, ?, ?, ?, ?)";

	/** */
	public static final String UPDATE_STMT = "update " + ISchulenTableNames.TABLE + " set "
		+ ISchulenTableNames.COL_SCHLUESSEL + " = ?, " + ISchulenTableNames.COL_NAME + " = ?, "
		+ ISchulenTableNames.COL_ANSCHRIFT + " = ?, " + ISchulenTableNames.COL_TELEFON + " = ?, "
		+ ISchulenTableNames.COL_BUNDESLAND + " = ? " + "where " + ISchulenTableNames.COL_ID + " = ?";

	/* select t.id, t.schule_id, t.mini_id, t.rueckmeldung from mini_teilnahmen t where t.id = 10 */
	public static final String SELECT_MINI_TEILNAHMEN_ZU_SCHULE = "select t."
		+ IMinikaenguruteilnahmenTableNames.COL_ID + ", t." + IMinikaenguruteilnahmenTableNames.COL_SCHULE_ID + ", t."
		+ IMinikaenguruteilnahmenTableNames.COL_MINIKAENGURU_ID + ", t."
		+ IMinikaenguruteilnahmenTableNames.COL_RUECKMELDUNG + " from " + IMinikaenguruteilnahmenTableNames.TABLE
		+ " t where t." + IMinikaenguruteilnahmenTableNames.COL_SCHULE_ID + " = ?";

	public static final String SELECT_MINI_TEILNAHMEN_ID_ZU_SCHULE = "select t."
		+ IMinikaenguruteilnahmenTableNames.COL_ID + " from " + IMinikaenguruteilnahmenTableNames.TABLE + " t where t."
		+ IMinikaenguruteilnahmenTableNames.COL_SCHULE_ID + " = ?";

	public static final String INSERT_MINI_TEILNAHME = "insert into " + IMinikaenguruteilnahmenTableNames.TABLE + " ("
		+ IMinikaenguruteilnahmenTableNames.COL_SCHULE_ID + ", "
		+ IMinikaenguruteilnahmenTableNames.COL_MINIKAENGURU_ID + ", "
		+ IMinikaenguruteilnahmenTableNames.COL_RUECKMELDUNG + ", " + IMinikaenguruteilnahmenTableNames.COL_ID
		+ ") values (?, ?, ?, ?)";

	public static final String UPDATE_MINI_TEILNAHME = "update " + IMinikaenguruteilnahmenTableNames.TABLE + " set "
		+ IMinikaenguruteilnahmenTableNames.COL_RUECKMELDUNG + " = ? where " + IMinikaenguruteilnahmenTableNames.COL_ID
		+ " = ?";

	public static final String DELETE_MINI_TEILNAHME = "delete from " + IMinikaenguruteilnahmenTableNames.TABLE
		+ " where " + IMinikaenguruteilnahmenTableNames.COL_ID + " = ?";
}
