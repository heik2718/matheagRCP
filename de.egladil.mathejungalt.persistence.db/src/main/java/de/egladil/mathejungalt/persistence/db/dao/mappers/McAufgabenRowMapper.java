/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import de.egladil.mathejungalt.domain.mcraetsel.MCAntwortbuchstaben;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabenarten;
import de.egladil.mathejungalt.domain.mcraetsel.MCThemen;
import de.egladil.mathejungalt.domain.types.MCAufgabenstufen;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMcAufgabenTableNames;

/**
 * @author heike
 */
public class McAufgabenRowMapper implements ParameterizedRowMapper<MCAufgabe> {

	private static final Logger LOG = LoggerFactory.getLogger(McAufgabenRowMapper.class);

	/**
   *
   */
	public McAufgabenRowMapper() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public MCAufgabe mapRow(ResultSet pRs, int pRowNum) throws SQLException {
		MCAufgabe result = new MCAufgabe();
		result.setAntwortA(pRs.getString(IMcAufgabenTableNames.COL_ANTWORT1));
		result.setAntwortB(pRs.getString(IMcAufgabenTableNames.COL_ANTWORT2));
		result.setAntwortC(pRs.getString(IMcAufgabenTableNames.COL_ANTWORT3));
		result.setAntwortD(pRs.getString(IMcAufgabenTableNames.COL_ANTWORT4));
		result.setAntwortE(pRs.getString(IMcAufgabenTableNames.COL_ANTWORT5));
		result.setArt(MCAufgabenarten.valueOf(pRs.getString(IMcAufgabenTableNames.COL_ART)));
		result.setBemerkung(pRs.getString(IMcAufgabenTableNames.COL_BEMERKUNG));
		result.setId(pRs.getLong(IMcAufgabenTableNames.COL_ID));
		result.setLoesungsbuchstabe(MCAntwortbuchstaben.valueOf(pRs.getString(IMcAufgabenTableNames.COL_LOESUNG)));
		result.setPunkte(pRs.getInt(IMcAufgabenTableNames.COL_PUNKTE));
		result.setQuelle(pRs.getString(IMcAufgabenTableNames.COL_QUELLE));
		result.setSchluessel(pRs.getString(IMcAufgabenTableNames.COL_SCHLUESSEL));
		result.setStufe(MCAufgabenstufen.valueOf(pRs.getInt(IMcAufgabenTableNames.COL_STUFE)));
		String tabelleGenerieren = pRs.getString(IMcAufgabenTableNames.COL_TABELLE_GENERIEREN);
		result.setTabelleGenerieren("J".equals(tabelleGenerieren));
		result.setThema(MCThemen.valueOfKurzbezeichnung(pRs.getString(IMcAufgabenTableNames.COL_THEMA)));
		result.setTitel(pRs.getString(IMcAufgabenTableNames.COL_TITEL));
		result.setVerzeichnis(pRs.getString(IMcAufgabenTableNames.COL_VERZEICHNIS));
		result.setAnzahlAntwortvorschlaege(pRs.getInt(IMcAufgabenTableNames.COL_ANZAHL_ANTWORTEN));
		return result;

	}
}
