/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.tablenames;

/**
 * @author Winkelv
 */
public interface IMedienTableNames extends IColumnNamesMitSchluessel {

	/** */
	public static final String TABLE = "medien";

	/** */
	public static final String COL_TITEL = "titel";

	/** */
	public static final String COL_ART = "art";
}
