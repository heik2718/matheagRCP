/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import de.egladil.base.exceptions.MatheJungAltException;

/**
 * @author Winkelv
 */
public class IdGenerator {

	private static final String SELECT_ALL = "select entity, sequence_value from sequence_table";

	private static final String UPDATE_FOR_TABLE = "update sequence_table set sequence_value = ? where entity = ?";

	private static final Logger LOG = LoggerFactory.getLogger(IdGenerator.class);

	private Map<String, Long> idMap;

	/** */
	private SimpleJdbcTemplate jdbcTemplate;

	/**
	 * @author Winkelv
	 */
	private class SequenceTableItem {

		private String entity;

		private Long id;

		/**
		 * @return the entity
		 */
		public String getEntity() {
			return entity;
		}

		/**
		 * @param pEntity the entity to set
		 */
		public void setEntity(String pEntity) {
			entity = pEntity;
		}

		/**
		 * @return the id
		 */
		public Long getId() {
			return id;
		}

		/**
		 * @param pId the id to set
		 */
		public void setId(Long pId) {
			id = pId;
		}
	};

	private class SequenceTableRowMapper implements ParameterizedRowMapper<SequenceTableItem> {

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
		 */
		@Override
		public SequenceTableItem mapRow(ResultSet pArg0, int pArg1) throws SQLException {
			SequenceTableItem item = new SequenceTableItem();
			item.setEntity(pArg0.getString("entity"));
			item.setId(pArg0.getLong("sequence_value"));
			return item;
		}

	};

	/**
	 *
	 */
	public IdGenerator() {
		idMap = new HashMap<String, Long>();
	}

	/**
	 * @param pDataSource
	 */
	public void flush() {
		persist();
	}

	/**
	 *
	 */
	private void persist() {
		Iterator<String> iter = idMap.keySet().iterator();
		while (iter.hasNext()) {
			String entityName = iter.next();
			Long wert = idMap.get(entityName);
			jdbcTemplate.update(UPDATE_FOR_TABLE, new Object[] { wert, entityName });
		}
		if (LOG.isInfoEnabled())
			LOG.info("sequence_table aktualisiert");
	}

	/**
	 * Nächste Id für diese Tabelle.
	 *
	 * @param pTableName
	 * @return
	 */
	public Long nextId(String pTableName) {
		return findIdFor(pTableName);
	}

	/**
	 * Erhöht den Zähler um 1 bei der jeweiligen Klasse
	 *
	 * @param pTableName
	 */
	public void increase(String pTableName) {
		increaseForTable(pTableName);
	}

	/**
	 * Erhöht Eintrag für Entity pTableName um 1
	 *
	 * @param pTableName String der Name der Tabelle, für die eine Nummer gezogen wurde.
	 */
	private void increaseForTable(String pTableName) {
		Long id = idMap.get(pTableName);
		if (id != null) {
			idMap.put(pTableName, id.longValue() + 1);
			Long wert = idMap.get(pTableName);
			jdbcTemplate.update(UPDATE_FOR_TABLE, new Object[] { wert, pTableName });
		}
	}

	/**
	 * Sucht die nächste Id. Falls es keinen Eintrag gibt, wird eine Exception geworfen. Man könnte auch so vorgehen,
	 * dass dann ein entsprechender Eintrag erzeugt wird, aber das wurde Implementierungsfehler in der Persistenzschicht
	 * verstecken.
	 *
	 * @param pTableName
	 * @return Long
	 */
	private Long findIdFor(String pTableName) {
		if (idMap.isEmpty()) {
			init();
		}
		Long id = idMap.get(pTableName);
		if (id != null)
			return id;
		throw new MatheJungAltException(
			"Sie sequence_table ist nicht korrekt konfiguriert. Es gibt keinen Eintrag für " + pTableName);
	}

	/**
	 *
	 */
	private void init() {
		List<SequenceTableItem> entries = jdbcTemplate.query(SELECT_ALL, new SequenceTableRowMapper());
		for (SequenceTableItem item : entries)
			idMap.put(item.getEntity(), item.getId());
		LOG.info("IdGenerator gestartet");
		if (LOG.isDebugEnabled()) {
			StringBuffer sb = new StringBuffer();
			sb.append("sequence_table = ");
			Iterator<String> iter = idMap.keySet().iterator();
			while (iter.hasNext()) {
				String name = iter.next();
				sb.append("[");
				sb.append(name);
				sb.append("=");
				sb.append(idMap.get(name).longValue());
				sb.append("],");
			}
			LOG.debug(sb.toString());
		}
	}

	/**
	 * @param pDataSource
	 */
	public void setDataSource(DataSource pDataSource) {
		jdbcTemplate = new SimpleJdbcTemplate(pDataSource);
	}
}
