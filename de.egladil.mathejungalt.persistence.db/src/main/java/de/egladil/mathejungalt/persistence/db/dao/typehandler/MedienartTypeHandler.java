/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao.typehandler;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.types.EnumTypes.Medienart;

/**
 * @author Winkelv
 */
public class MedienartTypeHandler {

	/**
	 * 
	 */
	public MedienartTypeHandler() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Wandelt String in Medienart um.
	 * 
	 * @param pSqlValue
	 * @return {@link Medienart}
	 * @throws MatheJungAltException
	 */
	public Medienart getDomainValue(final String pSqlValue) throws MatheJungAltException {
		if (pSqlValue == null)
			throw new MatheJungAltException("Parameter darf nicht null sein");
		if ("B".equals(pSqlValue))
			return Medienart.B;
		if ("Z".equals(pSqlValue))
			return Medienart.Z;
		throw new MatheJungAltException("Ungültiger String " + pSqlValue + ": nur B oder Z erlaubt.");
	}

	/**
	 * Wandelt {@link Medienart} in String um.
	 * 
	 * @param pDomainValue {@link Medienart}
	 * @return String
	 * @throws MatheJungAltException
	 */
	public String getSqlValue(Medienart pDomainValue) throws MatheJungAltException {
		if (pDomainValue == null)
			throw new MatheJungAltException("Parameter darf nicht null sein");
		return pDomainValue.toString();
	}

}
