/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao.typehandler;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenart;
import de.egladil.mathejungalt.domain.types.EnumTypes.Medienart;

/**
 * @author Winkelv
 */
public class AufgabenArtTypeHandler {

	/**
	 * 
	 */
	public AufgabenArtTypeHandler() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Wandelt String in Medienart um.
	 * 
	 * @param pSqlValue
	 * @return {@link Medienart}
	 * @throws MatheJungAltException
	 */
	public Aufgabenart getDomainValue(final String pSqlValue) throws MatheJungAltException {
		if (pSqlValue == null)
			throw new MatheJungAltException("Parameter darf nicht null sein");
		if ("E".equals(pSqlValue))
			return Aufgabenart.E;
		if ("N".equals(pSqlValue))
			return Aufgabenart.N;
		if ("Z".equals(pSqlValue))
			return Aufgabenart.Z;
		throw new MatheJungAltException("Ungültiger String " + pSqlValue + ": nur E, N oder Z erlaubt.");
	}

	/**
	 * Wandelt {@link Medienart} in String um.
	 * 
	 * @param pDomainValue {@link Aufgabenart}
	 * @return String
	 * @throws MatheJungAltException
	 */
	public String getSqlValue(Aufgabenart pDomainValue) throws MatheJungAltException {
		if (pDomainValue == null)
			throw new MatheJungAltException("Parameter darf nicht null sein");
		return pDomainValue.toString();
	}

}
