/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao.typehandler;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenart;
import de.egladil.mathejungalt.domain.types.EnumTypes.Medienart;
import de.egladil.mathejungalt.domain.types.EnumTypes.Quellenart;

/**
 * @author Winkelv
 */
public class QuellenArtTypeHandler {

	/**
	 * 
	 */
	public QuellenArtTypeHandler() {

	}

	/**
	 * Wandelt String in Medienart um.
	 * 
	 * @param pSqlValue
	 * @return {@link Medienart}
	 * @throws MatheJungAltException
	 */
	public Quellenart getDomainValue(final String pSqlValue) throws MatheJungAltException {
		if (pSqlValue == null) {
			throw new MatheJungAltException("Parameter darf nicht null sein");
		}
		if ("B".equals(pSqlValue))
			return Quellenart.B;
		if ("G".equals(pSqlValue)) {
			return Quellenart.G;
		}
		if ("K".equals(pSqlValue))
			return Quellenart.K;
		if ("N".equals(pSqlValue))
			return Quellenart.N;
		if ("Z".equals(pSqlValue))
			return Quellenart.Z;
		throw new MatheJungAltException("Ungültiger String " + pSqlValue + ": nur B, K, N oder Z erlaubt.");
	}

	/**
	 * Wandelt {@link Medienart} in String um.
	 * 
	 * @param pDomainValue {@link Aufgabenart}
	 * @return String
	 * @throws MatheJungAltException
	 */
	public String getSqlValue(Quellenart pDomainValue) throws MatheJungAltException {
		if (pDomainValue == null)
			throw new MatheJungAltException("Parameter darf nicht null sein");
		return pDomainValue.toString();
	}

}
