/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import de.egladil.mathejungalt.domain.quellen.MitgliederQuelleItem;
import de.egladil.mathejungalt.persistence.api.IMitgliederRepository;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMitgliederquellenitemsTableNames;

/**
 * @author Winkelv
 */
public class MitgliederquellenitemRowMapper implements ParameterizedRowMapper<MitgliederQuelleItem> {

	/** */
	private IMitgliederRepository mitgliederRepository;

	/**
	 * 
	 */
	public MitgliederquellenitemRowMapper() {
		super();
	}

	/**
	 * @param pMitgliederRepository
	 */
	public MitgliederquellenitemRowMapper(IMitgliederRepository pMitgliederRepository) {
		this();
		mitgliederRepository = pMitgliederRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public MitgliederQuelleItem mapRow(ResultSet pArg0, int pArg1) throws SQLException {
		MitgliederQuelleItem item = new MitgliederQuelleItem();
		item.setId(pArg0.getLong(IMitgliederquellenitemsTableNames.COL_ID));
		item.setJahre(pArg0.getInt(IMitgliederquellenitemsTableNames.COL_JAHRE));
		item.setKlasse(pArg0.getInt(IMitgliederquellenitemsTableNames.COL_KLASSE));
		item.setMitglied(mitgliederRepository.findById(pArg0.getLong(IMitgliederquellenitemsTableNames.COL_MITGLIED_ID)));
		return item;
	}
}
