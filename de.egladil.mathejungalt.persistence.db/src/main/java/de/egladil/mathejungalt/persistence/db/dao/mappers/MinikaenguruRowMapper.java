/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMinikaenguruTableNames;

/**
 * @author Winkelv
 */
public class MinikaenguruRowMapper implements ParameterizedRowMapper<Minikaenguru> {

	/**
	 * 
	 */
	public MinikaenguruRowMapper() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public Minikaenguru mapRow(ResultSet pArg0, int pArg1) throws SQLException {
		Minikaenguru minikaenguru = new Minikaenguru();
		minikaenguru.setId(pArg0.getLong(IMinikaenguruTableNames.COL_ID));
		minikaenguru.setBeendet(pArg0.getInt(IMinikaenguruTableNames.COL_BEENDET));
		minikaenguru.setJahr(pArg0.getInt(IMinikaenguruTableNames.COL_JAHR));
		return minikaenguru;
	}
}
