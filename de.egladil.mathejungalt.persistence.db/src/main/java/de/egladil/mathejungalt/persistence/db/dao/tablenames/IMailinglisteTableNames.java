/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao.tablenames;

/**
 * @author Winkelv
 */
public interface IMailinglisteTableNames extends ICommonColumnNamesOhneSchluessel {

	/** */
	public static final String TABLE = "mailingliste";

	/** */
	public static final String COL_EMAIL = "email";
}
