/**
 * 
 */
package de.egladil.mathejungalt.persistence.db.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguruitem;
import de.egladil.mathejungalt.persistence.api.IAufgabenRepository;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMinikaenguruitemsTableNames;

/**
 * @author winkelv
 */
public class MinikaenguruitemRowMapper implements ParameterizedRowMapper<Minikaenguruitem> {

	/** */
	private Minikaenguru minikaenguru;

	/** */
	private IAufgabenRepository aufgabenRepository;

	/**
	 * @param pAufgabenRepository
	 * @param pMinikaenguru
	 */
	public MinikaenguruitemRowMapper(IAufgabenRepository pAufgabenRepository, Minikaenguru pMinikaenguru) {
		super();
		aufgabenRepository = pAufgabenRepository;
		minikaenguru = pMinikaenguru;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.jdbc.core.simple.ParameterizedRowMapper#mapRow(java.sql.ResultSet, int)
	 */
	@Override
	public Minikaenguruitem mapRow(ResultSet pArg0, int pArg1) throws SQLException {
		Minikaenguruitem item = new Minikaenguruitem();
		item.setId(pArg0.getLong(IMinikaenguruitemsTableNames.COL_ID));
		item.setNummer(pArg0.getString(IMinikaenguruitemsTableNames.COL_NUMMER));
		item.setMinikaenguru(minikaenguru);

		Long aufgId = pArg0.getLong(IMinikaenguruitemsTableNames.COL_AUFGABE_ID);
		item.setAufgabe(aufgabenRepository.findById(aufgId));
		minikaenguru.addItem(item);

		return item;
	}

}
