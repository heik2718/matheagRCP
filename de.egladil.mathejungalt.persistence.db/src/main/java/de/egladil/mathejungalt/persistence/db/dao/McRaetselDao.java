/**
 *
 */
package de.egladil.mathejungalt.persistence.db.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.mysql.IJdbcTemplateWrapper;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.persistence.api.IMcAufgabenRepository;
import de.egladil.mathejungalt.persistence.api.IMcRaetselRepository;
import de.egladil.mathejungalt.persistence.api.IUrheberRepository;
import de.egladil.mathejungalt.persistence.db.dao.mappers.LongRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.McArchivraetselItemRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.McArchivraetselRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.mappers.SchluesselRowMapper;
import de.egladil.mathejungalt.persistence.db.dao.sql.IMcRaetselStmts;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMcRaetselTableNames;
import de.egladil.mathejungalt.persistence.db.dao.tablenames.IMcRaetselitemsTableNames;
import de.egladil.mathejungalt.persistence.db.dao.util.SqlQueryUtil;

/**
 * @author heike
 */
public class McRaetselDao extends AbstractDao implements IMcRaetselRepository {

	private static final Logger LOG = LoggerFactory.getLogger(McRaetselDao.class);

	private IUrheberRepository urheberRepository;

	private IMcAufgabenRepository mcAufgabenRepository;

	private Map<Long, MCArchivraetsel> raetselMap;

	/**
	 * @param pJdbcTemplateWrapper
	 */
	public McRaetselDao(IJdbcTemplateWrapper pJdbcTemplateWrapper, IdGenerator idGenerator) {
		super(pJdbcTemplateWrapper, idGenerator);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IMcRaetselRepository#loadAllRaetsel()
	 */
	@Override
	public List<MCArchivraetsel> loadAllRaetsel() {
		if (raetselMap == null) {
			loadRaetselFromDB();
		}
		List<MCArchivraetsel> result = new ArrayList<>();
		for (MCArchivraetsel r : raetselMap.values()) {
			result.add(r);
		}
		return result;
	}

	private void loadRaetselFromDB() {
		List<MCArchivraetsel> liste = getJdbcTemplate().query(IMcRaetselStmts.SELECT_STMT,
			new McArchivraetselRowMapper(urheberRepository));
		raetselMap = new HashMap<Long, MCArchivraetsel>();
		for (MCArchivraetsel raetsel : liste) {
			raetselMap.put(raetsel.getId(), raetsel);
		}
		LOG.info(liste.size() + " " + IMcRaetselTableNames.TABLE + " geladen");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.persistence.api.IMcRaetselRepository#getMaxKey()
	 */
	@Override
	public String getMaxKey() {
		return getJdbcTemplate().queryForObject(IMcRaetselStmts.SELECT_MAX_KEY_STMT, new SchluesselRowMapper(),
			new Object[] { "%-%" });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IMcRaetselRepository#loadItems(de.egladil.mcmatheraetsel.archivdomain
	 * .archiv. MCArchivraetsel)
	 */
	@Override
	public List<MCArchivraetselItem> loadItems(MCArchivraetsel pRaetsel) {
		if (pRaetsel == null || pRaetsel.getId() == null)
			throw new MatheJungAltException(SerienDao.class.getName()
				+ ".loadItems(): Parameter ist null oder Parameter.ID ist null");
		if (pRaetsel.isItemsLoaded()) {
			return pRaetsel.getItems();
		}
		if (raetselMap == null)
			loadRaetselFromDB();

		if (!raetselMap.containsValue(pRaetsel))
			throw new MatheJungAltException(SerienDao.class.getName()
				+ ".loadItems(): schwerwiegender Programmfehler: es gibt das Raetsel " + pRaetsel.getSchluessel()
				+ " nicht im Repository!");

		LOG.info("Lade items zu " + pRaetsel.getSchluessel());
		McArchivraetselItemRowMapper rowMapper = new McArchivraetselItemRowMapper(mcAufgabenRepository, pRaetsel);
		List<MCArchivraetselItem> itemList = getJdbcTemplate().query(IMcRaetselStmts.SELECT_ITEMS_ZU_RAETSEL_STMT,
			rowMapper, new Object[] { pRaetsel.getId() });
		pRaetsel.setItemsLoaded(true);
		LOG.info(itemList.size() + " " + IMcRaetselitemsTableNames.TABLE + " geladen (" + pRaetsel.getSchluessel()
			+ ")");
		return pRaetsel.getItems();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IMcRaetselRepository#saveRaetsel(de.egladil.mcmatheraetsel.archivdomain
	 * .archiv. MCArchivraetsel)
	 */
	@Override
	public void saveRaetsel(MCArchivraetsel pRaetsel) {
		if (pRaetsel == null) {
			throw new MatheJungAltException("null an saveRaetsel() uebergeben");
		}
		if (pRaetsel.getId() == null) {
			persistRaetsel(pRaetsel);
		} else {
			mergeRaetsel(pRaetsel);
		}
	}

	/**
	 * @param pRaetsel
	 * @return
	 */
	private Map<MCArchivraetselItem, SqlQueryUtil.ACTION> initSerienitemActions(MCArchivraetsel pRaetsel) {
		Map<MCArchivraetselItem, SqlQueryUtil.ACTION> map = new HashMap<MCArchivraetselItem, SqlQueryUtil.ACTION>();
		List<Long> idList = getJdbcTemplate().query(IMcRaetselStmts.SELECT_RAETSELITEM_ID_ZU_RAETSEL_STMT,
			new LongRowMapper(), new Object[] { pRaetsel.getId() });
		List<Long> ratselItems = new ArrayList<Long>();
		// INSERTs und UPDATEs
		for (MCArchivraetselItem item : pRaetsel.getItems()) {
			ratselItems.add(item.getId());
			if (item.isNew()) {
				map.put(item, SqlQueryUtil.ACTION.INSERT);
			} else {
				if (idList.contains(item.getId()))
					map.put(item, SqlQueryUtil.ACTION.UPDATE);

			}
		}
		// Deletes sind die, die nicht mehr in der SerieItemsIDs-Liste sind
		for (Long dbID : idList) {
			if (!ratselItems.contains(dbID)) {
				MCArchivraetselItem tempItem = new MCArchivraetselItem();
				tempItem.setId(dbID);
				map.put(tempItem, SqlQueryUtil.ACTION.DELETE);
			}
		}
		return map;
	}

	/**
	 * @param pRaetsel
	 */
	private void persistRaetsel(final MCArchivraetsel pRaetsel) {
		if (raetselMap == null) {
			loadAllRaetsel();
		}
		final Long raetselId = getIdGenerator().nextId(IMcRaetselTableNames.TABLE);
		getTransactionTemplate().execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(TransactionStatus pArg0) {
				getJdbcTemplate().update(IMcRaetselStmts.INSERT_RAETSEL_STMT, getParams(pRaetsel, raetselId));
				pRaetsel.setId(raetselId);

				for (MCArchivraetselItem item : pRaetsel.getItems()) {
					persistItem(item);
				}

				raetselMap.put(raetselId, pRaetsel);
				getIdGenerator().increase(IMcRaetselTableNames.TABLE);
			}
		});

	}

	private Object[] getParams(MCArchivraetsel pRaetsel, Long pId) {
		Integer published = pRaetsel.isVeroeffentlicht() ? 1 : 0;
		Integer privat = pRaetsel.isPrivat() ? 1 : 0;
		return new Object[] { pRaetsel.getAutor().getId(), pRaetsel.getLicenseFull(), pRaetsel.getLicenseShort(),
			pRaetsel.getSchluessel(), pRaetsel.getTitel(), pRaetsel.getStufe().getStufe(), published, privat, pId };
	}

	/**
	 * Änderung speichern
	 *
	 * @param pItem
	 */
	private void mergeItem(final MCArchivraetselItem pItem) {
		getJdbcTemplate().update(IMcRaetselStmts.UPDATE_RAETSELITEM_STMT,
			new Object[] { pItem.getNummer(), pItem.getId() });
	}

	/**
	 * Neues Serienitem speichern
	 *
	 * @param pItem
	 */
	private void persistItem(final MCArchivraetselItem pItem) {
		Long id = getIdGenerator().nextId(IMcRaetselitemsTableNames.TABLE);
		getJdbcTemplate().update(IMcRaetselStmts.INSERT_RAETSELITEM_STMT,
			new Object[] { pItem.getRaetsel().getId(), pItem.getAufgabe().getId(), pItem.getNummer(), id });
		pItem.setId(id);
		getIdGenerator().increase(IMcRaetselitemsTableNames.TABLE);
	}

	/**
	 * @param pRaetsel
	 */
	private void mergeRaetsel(final MCArchivraetsel pRaetsel) {
		final Map<MCArchivraetselItem, SqlQueryUtil.ACTION> raetselitemsActions = initSerienitemActions(pRaetsel);
		getTransactionTemplate().execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(TransactionStatus pArg0) {
				getJdbcTemplate().update(IMcRaetselStmts.UPDATE_RAETSEL_STMT, getParams(pRaetsel, pRaetsel.getId()));

				for (MCArchivraetselItem item : raetselitemsActions.keySet()) {
					switch (raetselitemsActions.get(item)) {
					case DELETE:
						removeItem(item);
						break;
					case INSERT:
						persistItem(item);
						break;
					case UPDATE:
						mergeItem(item);
						break;
					default:
						break;
					}
				}
			}
		});

	}

	/**
	 * @param pItem
	 */
	private void removeItem(MCArchivraetselItem pItem) {
		if (pItem == null || pItem.isNew()) {
			return;
		}
		getJdbcTemplate().update(IMcRaetselStmts.DELETE_RAETSELITEM_STMT, new Object[] { pItem.getId() });
	}

	/**
	 * used by spring
	 *
	 * @param pAufgabenRepository
	 */
	public void setMcAufgabenRepository(IMcAufgabenRepository pAufgabenRepository) {
		mcAufgabenRepository = pAufgabenRepository;
	}

	/**
	 * used by spring
	 *
	 * @param pUrheberRepository the urheberRepository to set
	 */
	public final void setUrheberRepository(IUrheberRepository pUrheberRepository) {
		urheberRepository = pUrheberRepository;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.persistence.api.IMcRaetselRepository#markAsPublished(de.egladil.mcmatheraetsel.archivdomain
	 * .archiv. MCArchivraetsel)
	 */
	@Override
	public void markAsPublished(MCArchivraetsel pRaetsel) {
		getJdbcTemplate().update(IMcRaetselStmts.PUBLISH_RAETSEL_STMT,
			new Object[] { Integer.valueOf(1), pRaetsel.getId() });
	}
}
