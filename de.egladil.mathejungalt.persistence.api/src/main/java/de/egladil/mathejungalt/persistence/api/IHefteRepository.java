/**
 * 
 */
package de.egladil.mathejungalt.persistence.api;

import java.util.List;

import de.egladil.mathejungalt.domain.aufgaben.Heft;

/**
 * @author heike
 */
public interface IHefteRepository {

	/**
	 * @return {@link List} von {@link Heft} Objekten
	 */
	public abstract List<Heft> findAll();

	/**
	 * @param pId
	 * @return
	 */
	public Heft findById(Long pId);

	/**
	 * Speichert das Heft
	 * 
	 * @param pHeft
	 */
	public abstract void save(Heft pHeft);

	/**
	 * Ermittelt den gr��ten aktuellen fachlichen Schl�ssel
	 * 
	 * @return
	 */
	public abstract String getMaxKey();

	/**
	 * Methode um ein {@link Heft} zu registrieren
	 * 
	 * @param pHeft
	 */
	public void register(Heft pHeft);
}