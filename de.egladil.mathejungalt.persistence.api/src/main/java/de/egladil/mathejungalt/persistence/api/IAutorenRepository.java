/**
 * 
 */
package de.egladil.mathejungalt.persistence.api;

import java.util.List;

import de.egladil.mathejungalt.domain.medien.Autor;
import de.egladil.mathejungalt.domain.medien.Buch;

/**
 * Interface zum Zugriff auf die Persistenzschicht für die Autoren.
 * 
 * @author heike
 */
public interface IAutorenRepository {

	/**
	 * L�d alle Autoren.
	 * 
	 * @return {@link List}
	 */
	public abstract List<Autor> findAllAutoren();

	/**
	 * Speichert einen Autor
	 * 
	 * @param pAutor
	 */
	public abstract void saveAutor(final Autor pAutor);

	/**
	 * Loescht den Autor aus der Datenbank.
	 * 
	 * @param pAutor
	 */
	public abstract void removeAutor(Autor pAutor);

	/**
	 * Ermittelt Anzahl Buecher mit gegebenem Autor.
	 * 
	 * @param pAutor {@link Autor}
	 * @return int
	 */
	public abstract long anzahlBuecher(Autor pAutor);

	/**
	 * Sucht den maximalen fachlichen Schluessel fuer AUTOREN.
	 * 
	 * @return String
	 */
	public abstract String getMaxKey();

	/**
	 * @param pBuch
	 * @return
	 */
	public abstract void loadAutorenZuBuch(Buch pBuch);
}