/**
 *
 */
package de.egladil.mathejungalt.persistence.api;

import java.util.List;

import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serienitem;

/**
 * @author heike
 */
public interface ISerienRepository {

	/**
	 * Sucht Objekt mit gegebenem fachlichen Schl�ssel.
	 *
	 * @param pKey
	 * @return
	 */
	public abstract Serie findByKey(final Integer pKey);

	/**
	 * Sucht Objekt mit gegebenem primaty key
	 *
	 * @param pKey
	 * @return
	 */
	public abstract Serie findById(final Long pId);

	/**
	 * L�d alle Serien.
	 *
	 * @return {@link List} von {@link Serie}
	 */
	public abstract List<Serie> findAllSerien();

	/**
	 * Speichert die gegebene Serie.
	 *
	 * @param pSerie
	 */
	public abstract void saveSerie(Serie pSerie);

	/**
	 * L�d alle {@link Serienitem} dieser Serie.
	 *
	 * @param pSerie {@link Serie} die Serie
	 */
	public abstract void loadSerienitems(final Serie pSerie);

	/**
	 * Sucht das Serieneitem mit dieser Aufgabe
	 *
	 * @param pAufgabe
	 * @return
	 */
	public abstract Serienitem findByAufgabe(Aufgabe pAufgabe);

	/**
	 * Ermittelt den größten aktuellen fachlichen Schlüssel
	 *
	 * @return
	 */
	public abstract Integer getMaxKey();

	/**
	 * Sucht alle Serien zu gegebener Runde.
	 *
	 * @param pRunde
	 * @return {@link List} von {@link Serie}
	 */
	public abstract List<Serie> findByRunde(final int pRunde);

	/**
	 * @param pAufgabenRepository
	 */
	public abstract void setAufgabenRepository(IAufgabenRepository pAufgabenRepository);
}