/**
 *
 */
package de.egladil.mathejungalt.persistence.api;

import java.util.List;
import java.util.Map;

import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgaben.Heft;
import de.egladil.mathejungalt.domain.aufgaben.IAufgabeNames;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguruitem;

/**
 * @author heike
 */
public interface IAufgabenRepository {

	/**
	 * Läd alle Aufgaben.
	 *
	 * @return {@link List} von {@link AbstractAufgabe}
	 */
	public abstract List<Aufgabe> findAll();

	/**
	 * Sucht Aufgaben nach Treffern mit den gegebenen Atrtributen. Das Filterkriterium ist gegeben durch eine Map, deren
	 * keys die im {@link IAufgabeNames} definiert sind. Der zweite Parameter gibt an, ob nach exakten Treffern gesucht
	 * wird (bei false Suche mit like). Der dritte Parameter gibt an, ob die Abfrage mit AND verkn�pft wird (bei false
	 * Suche mit OR)<br>
	 * <br>
	 * Beispiel:<br>
	 * String string = "mini";<br>
	 * params.clear();<br>
	 * params.put(AufgabeNamesInterface.TITEL, "");<br>
	 * params.put(AufgabeNamesInterface.BESCHREIBUNG, string);<br>
	 * List<AbstractAufgabe> liste = aufgabenRepository.findByAttributeMap(params, false, false);
	 *
	 * @param pAttributeMap {@link Map} von String-Objekt-Paaren, die einen Attribut-Filter f�r eine Abfrage bilden.
	 * @param pExact boolean
	 * @param pAnd boolean
	 * @return {@link List} von {@link AbstractAufgabe}
	 */
	public List<Aufgabe> findByAttributeMap(Map<String, Object> pAttributeMap, boolean pExact, final boolean pAnd);

	/**
	 * Ermittelt den aktuell größten Schlüssel.
	 *
	 * @return String
	 */
	String getMaxKey();

	/**
	 * Die Anzahl von sperrenden Arbeitsblättern, die diese Aufgabe enthalten (sollte normalerweise nur 0 oder 1 sein)
	 *
	 * @param pAufgabe
	 * @return
	 */
	int anzahlSperrendeArbeitsblaetterMitAufgabe(Aufgabe pAufgabe);

	/**
	 * Speichert die gegbene Aufgabe.
	 *
	 * @param pAufgabe {@link AbstractAufgabe} de zu speichernde Aufgabe.
	 */
	public abstract void save(final Aufgabe pAufgabe);

	/**
	 * Sucht Aufgabe mit diesem Schluessel
	 *
	 * @param pString
	 * @return
	 */
	public abstract Aufgabe findByKey(String pString);

	/**
	 * Setzt das Flag zur Anzeige, ob eine Aufgabe zu schlecht für eine Serie ist auf sein Gegenteil.
	 *
	 * @param pAufgabe
	 */
	void changeZuSchlechtFuerSerie(Aufgabe pAufgabe);

	/**
	 * Sucht Objekt mit dieser id.
	 *
	 * @param pId
	 * @return
	 */
	public abstract Aufgabe findById(long pId);

	/**
	 * Nachträgliches Laden der Aufgaben zum gegebenen Heft
	 *
	 * @param pHeft
	 */
	public abstract void loadAufgabenZuHeft(final Heft pHeft);

	/**
	 * @param pQuellenRepository the quellenRepository to set
	 */
	public void setQuellenRepository(IQuellenRepository pQuellenRepository);

	/**
	 * @param pHefteRepository the hefteRepository to set
	 */
	public void setHefteRepository(IHefteRepository pHefteRepository);

	/**
	 * Ermittelt die Anzahl der {@link Minikaenguruitem} zu dieser Aufgabe
	 *
	 * @param pAufgabe
	 */
	public int anzahlMinikaenguruMitAufgabe(Aufgabe pAufgabe);

	/**
	 * Ermittelt die SerienIds, in denen diese Aufgabe verwendet wurde.
	 *
	 * @param pAufgabe
	 * @return
	 */
	public List<Long> serienIdMitAufgabe(Aufgabe pAufgabe);
}