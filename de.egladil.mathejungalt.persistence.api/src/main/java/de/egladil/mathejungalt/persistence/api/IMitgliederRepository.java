/**
 *
 */
package de.egladil.mathejungalt.persistence.api;

import java.util.List;
import java.util.Map;

import de.egladil.mathejungalt.domain.mitglieder.Diplom;
import de.egladil.mathejungalt.domain.mitglieder.IMitgliedNames;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.domain.mitglieder.Serienteilnahme;

/**
 * @author heike
 */
public interface IMitgliederRepository {

	/**
	 * Läd alle Mitglieder
	 *
	 * @return {@link List} von {@link Mitglied} Objekten
	 */
	public abstract List<Mitglied> findAll();

	/**
	 * Sucht Mitglieder nach Treffern mit den gegebenen Atrtributen. Das Filterkriterium ist gegeben durch eine Map,
	 * deren keys die im {@link IMitgliedNames} definiert sind. Der zweite Parameter gibt an, ob nach exakten Treffern
	 * gesucht wird (bei false Suche mit like). Der dritte Parameter gibt an, ob die Abfrage mit AND verknäpft wird (bei
	 * false Suche mit OR) <br>
	 * <br>
	 * Beispiel: Suche nach Mitgliedern, die in Klasse 6 sind und irgendwie mit john heiäen.<br>
	 * String string = "john";<br>
	 * params.clear();<br>
	 * params.put(MitgliedNamesInterface.VORNAME, string);<br>
	 * params.put(MitgliedNamesInterface.KLASSE, new Integer(6);<br>
	 * List<Mitglied> liste = mitgliederRepository.findByAttributeMap(params, false, true);
	 *
	 * @param pAttributeMap {@link Map} von String-Objekt-Paaren, die einen Attribut-Filter fär eine Abfrage bilden.
	 * @param pExact boolean
	 * @param pAnd boolean
	 * @return {@link List} von {@link AbstractAufgabe}
	 */
	public List<Mitglied> findByAttributeMap(Map<String, Object> pAttributeMap, boolean pExact, final boolean pAnd);

	/**
	 * Speichert das gegebene Mitglied.
	 *
	 * @param pMitglied
	 */
	public abstract void saveMitglied(Mitglied pMitglied);

	/**
	 * @param pMitglied
	 */
	void changeMitgliedActivation(Mitglied pMitglied);

	/**
	 * @param pI
	 * @return {@link Mitglied}
	 */
	public abstract Mitglied findById(long pId);

	/**
	 * Läd eine Liste vom Mitgliedern, die in der gegebenen Runde Frähstarterpunkte haben
	 *
	 * @param pRunde int die Runde
	 * @return {@link List} von {@link Mitglied}
	 */
	abstract List<Mitglied> findAllWithFruehstarterInRunde(int pRunde);

	/**
	 * Sucht den maximalen fachlichen Schlüssel.
	 *
	 * @return String
	 */
	public abstract String getMaxKey();

	/**
	 * Speichert die gegebene Serienteilnahme.
	 *
	 * @param pSerienteilnahme
	 */
	public abstract void saveSerienteilnahme(Serienteilnahme pSerienteilnahme);

	/**
	 * Speichert das gegebene Diplom
	 *
	 * @param pDiplom
	 */
	public abstract void saveDiplom(Diplom pDiplom);

	/**
	 * Läd alle Serienteilnahmen zu gegebenem {@link Mitglied}
	 *
	 * @param pMitglied das {@link Mitglied}
	 * @return {@link List} von {@link Serienteilnahme}
	 */
	public abstract void loadSerienteilnahmen(final Mitglied pMitglied);

	/**
	 * Läd alle {@link Diplom} e zu gegebenem {@link Mitglied}
	 *
	 * @param pMitglied das {@link Mitglied}
	 * @return {@link List} von {@link Serienteilnahme}
	 */
	public abstract void loadDiplome(final Mitglied pMitglied);

	/**
	 * Läscht die gegebene Serienteilnahme
	 *
	 * @param pTeilnahme
	 */
	public abstract void removeSerienteilnahme(Serienteilnahme pTeilnahme);

	/**
	 * Läscht das gegebene Diplom
	 *
	 * @param pDiplom
	 */
	public abstract void removeDiplom(Diplom pDiplom);

	/**
	 * @param pSerienRepository the serienRepository to set
	 */
	public void setSerienRepository(ISerienRepository pSerienRepository);

}