/**
 *
 */
package de.egladil.mathejungalt.persistence.api;

import java.util.List;

import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.domain.schulen.Schule;

/**
 * @author heike
 */
public interface IKontakteRepository {

	/**
	 * Läd alle Schulen.
	 * 
	 * @return {@link List} von {@link Kontakt}
	 */
	public List<Kontakt> findAllKontakte();

	/**
	 * @param pSchule
	 * @return
	 */
	public List<Kontakt> findKontakteForSchule(Schule pSchule);

	/**
	 * Speichert die gegebene Serie.
	 * 
	 * @param pKontakt
	 */
	public void saveKontakt(Kontakt pKontakt);

	/**
	 * @param pKontakt
	 */
	public void loadSchulkontakte(Kontakt pKontakt);

	/**
	 * Sucht den maximalen fachlichen Schlüssel.
	 * 
	 * @return String
	 */
	public String getMaxKey();

	/**
	 * @param pSchulenRepository the schulenRepository to set
	 */
	public void setSchulenRepository(ISchulenRepository pSchulenRepository);

}