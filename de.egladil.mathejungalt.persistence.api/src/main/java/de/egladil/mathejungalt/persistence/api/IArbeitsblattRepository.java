/**
 * 
 */
package de.egladil.mathejungalt.persistence.api;

import java.util.List;

import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblattitem;

/**
 * @author heike
 */
public interface IArbeitsblattRepository {

	/**
	 * Läd alle {@link Arbeitsblatt}.
	 * 
	 * @return {@link List} von {@link Arbeitsblatt}
	 */
	public abstract List<Arbeitsblatt> findAllArbeitsblaetter();

	/**
	 * Speichert das gegebene {@link Arbeitsblatt}
	 * 
	 * @param pArbeitsblatt der {@link Arbeitsblatt}
	 */
	public abstract void save(Arbeitsblatt pArbeitsblatt);

	/**
	 * Sucht das Arbeitsblattitem mit dieser Aufgabe
	 * 
	 * @param pAufgabe {@link Aufgabe}
	 * @return {@link Arbeitsblattitem}
	 */
	public abstract Arbeitsblattitem findByAufgabe(Aufgabe pAufgabe);

	/**
	 * L�d alle {@link Arbeitsblattitem} dieses Arbeitsblatts nach
	 * 
	 * @param pArbeitsblatt {@link Arbeitsblatt} das Arbeitsblatt
	 */
	public abstract void loadItems(final Arbeitsblatt pArbeitsblatt);

	/**
	 * Mit id suchen
	 * 
	 * @param pId
	 * @return
	 */
	public abstract Arbeitsblatt findById(Long pId);

	/**
	 * Ermittelt den gr��ten aktuellen fachlichen Schl�ssel
	 * 
	 * @return Integer
	 */
	public abstract String getMaxKey();

	/**
	 * @param pAufgabenRepository
	 */
	public void setAufgabenRepository(IAufgabenRepository pAufgabenRepository);

}