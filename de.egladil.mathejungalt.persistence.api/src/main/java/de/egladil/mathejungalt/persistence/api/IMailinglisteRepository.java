/**
 * 
 */
package de.egladil.mathejungalt.persistence.api;

import java.util.List;

import de.egladil.mathejungalt.domain.mitglieder.EmailAdresse;

/**
 * @author winkelv
 */
public interface IMailinglisteRepository {

	/**
	 * Alle persistenten Email-Adressen aus der Mailingliste lesen.
	 * 
	 * @return
	 */
	public abstract List<EmailAdresse> findAll();

	/**
	 * Sucht den E-Mail-Kontakt mit der gegebenen Adresse.
	 * 
	 * @param pAdresse String ein String
	 * @return {@link EmailAdresse} oder null (falls es noch keine gibt)
	 */
	public EmailAdresse findByAdresse(String pAdresse);

	/**
	 * Einfuegen oder aendern.
	 * 
	 * @param pAdresse
	 */
	public abstract void save(EmailAdresse pAdresse);

	/**
	 * Einfuegen oder aendern.
	 * 
	 * @param pAdresse String
	 */
	public abstract void save(String pAdresse);

	/**
	 * Loeschen
	 * 
	 * @param pAdresse
	 */
	public abstract void remove(EmailAdresse pAdresse);

	/**
	 * Loeschen
	 * 
	 * @param pAdresse String
	 */
	public abstract void remove(String pAdresse);
}
