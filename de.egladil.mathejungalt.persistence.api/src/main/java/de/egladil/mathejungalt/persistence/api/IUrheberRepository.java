/**
 *
 */
package de.egladil.mathejungalt.persistence.api;

import java.util.List;

import de.egladil.mathejungalt.domain.mcraetsel.Urheber;

/**
 * Interface zum Zugriff auf die Persistenzschicht für die Urheber.
 *
 * @author heike
 */
public interface IUrheberRepository {

	/**
	 * Läd alle Urheber.
	 *
	 * @return {@link List}
	 */
	List<Urheber> findAllUrheber();

	/**
	 * @param pId
	 * @return
	 */
	Urheber findById(Long pId);
}