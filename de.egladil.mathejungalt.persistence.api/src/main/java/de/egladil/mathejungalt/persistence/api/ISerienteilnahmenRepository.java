/**
 *
 */
package de.egladil.mathejungalt.persistence.api;

import java.util.List;

import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.domain.mitglieder.Serienteilnahme;

/**
 * @author Heike Winkelvoss (www.egladil.de)
 */
public interface ISerienteilnahmenRepository {

	/**
	 * Läd alle Serienteilnahmen zu gegebenem {@link Mitglied}
	 *
	 * @param pMitglied das {@link Mitglied}
	 * @return {@link List} von {@link Serienteilnahme}
	 */
	public abstract List<Serienteilnahme> findAllSerienteilnahmen(final Mitglied pMitglied);

	/**
	 * Sucht die {@link Serienteilnahme} mit diesem {@link Mitglied} und dieser {@link Serie}
	 *
	 * @param pMitglied {@link Mitglied} das Mitglied
	 * @param pSerie {@link Serie} die Serie
	 * @return {@link Serienteilnahme} oder null
	 */
	public abstract Serienteilnahme findSerienteilnahme(final Mitglied pMitglied, final Serie pSerie);

	/**
	 * Speichert die gegebene Serienteilnahme.
	 *
	 * @param pSerienteilnahme
	 */
	public abstract void saveSerienteilnahme(Serienteilnahme pSerienteilnahme);

	/**
	 * L�scht die gegebene Serienteilnahme
	 *
	 * @param pTeilnahme
	 */
	public abstract void removeSerienteilnahme(Serienteilnahme pTeilnahme);
}
