package de.egladil.mathejungalt.persistence.api;

public interface IPersistenceservice {

	/**
	 * @return the aufgabenRepository
	 */
	IAufgabenRepository getAufgabenRepository();

	/**
	 * @return the hefteRepository
	 */
	IHefteRepository getHefteRepository();

	/**
	 * @return the minikaenguruRepository
	 */
	IMinikaenguruRepository getMinikaenguruRepository();

	/**
	 * @return the serienRepository
	 */
	ISerienRepository getSerienRepository();

	/**
	 * @return the medienRepository
	 */
	IMedienRepository getMedienRepository();

	/**
	 * @return the autorenRepository
	 */
	IAutorenRepository getAutorenRepository();

	/**
	 * @return the mitgliederRepository
	 */
	IMitgliederRepository getMitgliederRepository();

	/**
	 * @return the quellenRepository
	 */
	IQuellenRepository getQuellenRepository();

	/**
	 * @return the mailinglisteRepository
	 */
	IMailinglisteRepository getMailinglisteRepository();

	/**
	 * @return
	 */
	IArbeitsblattRepository getArbeitsblattRepository();

	/**
	 * @return
	 */
	ISchulenRepository getSchulenRepository();

	/**
	 * @return
	 */
	IKontakteRepository getKontakteRepository();

	/**
	 * Aufräumarbeiten vor dem Herunterfahren.
	 * 
	 * @return
	 */
	public boolean shutDown();

	/**
	 * Gibt eine Liste von unterstuetzten Protokollen zurueck.
	 * 
	 * @return
	 */
	public String[] getProtokolle();

	/**
	 * Gibt den Namen der Datenbank oder den Pfad des Verzeichnisses, in dem die Domain-Objekte persistiert werden.
	 * 
	 * @return String
	 */
	String getPersistenceLocation();

	/**
	 * @return
	 */
	IUrheberRepository getUrheberRepository();

	/**
	 * @return
	 */
	IMcRaetselRepository getRaetselRepository();

	/**
	 * @return
	 */
	IMcAufgabenRepository getMcAufgabenRepository();

}