/**
 *
 */
package de.egladil.mathejungalt.persistence.api;

import java.util.List;

import de.egladil.mathejungalt.domain.schulen.Land;
import de.egladil.mathejungalt.domain.schulen.Schule;

/**
 * @author heike
 */
public interface ISchulenRepository {

	/**
	 * Läd alle Schulen.
	 *
	 * @return {@link List} von {@link Schule}
	 */
	public List<Schule> findAllSchulen();

	/**
	 * @return
	 */
	public List<Land> findAllLaender();

	/**
	 * Speichert die gegebene Serie.
	 *
	 * @param pSchule
	 */
	public void saveSchule(Schule pSchule);

	/**
	 * @param id
	 * @return
	 */
	public Schule findSchuleById(Long id);

	/**
	 * @param pSchule
	 */
	public void loadMinikaenguruteilnahmen(Schule pSchule);

	/**
	 * @return
	 */
	public List<Land> loadLaender();

	/**
	 * Sucht den maximalen fachlichen Schlüssel.
	 *
	 * @return String
	 */
	public abstract String getMaxKey();

	/**
	 * @param pMinikaenguruRepository the minikaenguruRepository to set
	 */
	public void setMinikaenguruRepository(IMinikaenguruRepository pMinikaenguruRepository);

}