/**
 * 
 */
package de.egladil.mathejungalt.persistence.api;

import java.util.List;

import de.egladil.mathejungalt.domain.mitglieder.Diplom;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;

/**
 * @author heike
 */
public interface IDiplomeRepository {

	/**
	 * Sucht alle Diplome des Mitglieds.
	 * 
	 * @param pMitglied
	 * @return
	 */
	public abstract List<Diplom> findForMitglied(Mitglied pMitglied);

	/**
	 * Ermittelt nur die Anzahl der Diplome dieses Mitglieds.
	 * 
	 * @param pMitgliedMitDiplomen
	 * @return long die gesuchte Anzahl
	 */
	public abstract long anzahlDiplomeZuMitglied(Mitglied pMitgliedMitDiplomen);

	/**
	 * Alle Diplome.
	 * 
	 * @return {@link List} von {@link Diplom} Objekten
	 */
	public abstract List<Diplom> findAll();

	/**
	 * @return long die Anzahl aller Diplome
	 */
	public abstract long anzahlDiplome();

	/**
	 * Speichert das gegebene Diplom
	 * 
	 * @param pDiplom
	 */
	public abstract void save(Diplom pDiplom);
}