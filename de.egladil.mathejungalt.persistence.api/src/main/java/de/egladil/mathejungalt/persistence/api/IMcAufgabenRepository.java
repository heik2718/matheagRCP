package de.egladil.mathejungalt.persistence.api;

import java.util.List;

import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;

public interface IMcAufgabenRepository {

	/**
	 * Läd alle MC-Aufgaben
	 *
	 * @return
	 */
	List<MCAufgabe> findAll();

	/**
	 * Ermittelt den aktuell größten Schlüssel.
	 *
	 * @return String
	 */
	String getMaxKey();

	/**
	 * Speichert die gegbene Aufgabe.
	 *
	 * @param pAufgabe {@link AbstractAufgabe} de zu speichernde Aufgabe.
	 */
	void save(final MCAufgabe pAufgabe);

	/**
	 * @param pId
	 * @return
	 */
	MCAufgabe findById(Long pId);

	/**
	 * @param pAufgabe
	 * @return
	 */
	List<Long> raetselIdMitAufgabe(MCAufgabe pAufgabe);
}
