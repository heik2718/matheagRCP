/**
 *
 */
package de.egladil.mathejungalt.persistence.api;

import java.util.List;

import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;

/**
 * @author heike
 */
public interface IMcRaetselRepository {

	List<MCArchivraetsel> loadAllRaetsel();

	/**
	 * Ermittelt den größten aktuellen fachlichen Schlüssel (Rätselnummer)
	 *
	 * @return
	 */
	String getMaxKey();

	/**
	 * Läd alle Items zu diesem Rätsel.
	 *
	 * @param pRaetsel
	 * @return
	 */
	List<MCArchivraetselItem> loadItems(MCArchivraetsel pRaetsel);

	/**
	 * Speichert das gesamte Rätsel.
	 *
	 * @param pRaetsel
	 */
	void saveRaetsel(MCArchivraetsel pRaetsel);

	/**
	 * @param pRaetsel
	 */
	void markAsPublished(MCArchivraetsel pRaetsel);

	/**
	 * used by spring
	 *
	 * @param pAufgabenRepository
	 */
	public void setMcAufgabenRepository(IMcAufgabenRepository pAufgabenRepository);

	/**
	 * used by spring
	 *
	 * @param pUrheberRepository the urheberRepository to set
	 */
	public void setUrheberRepository(IUrheberRepository pUrheberRepository);
}
