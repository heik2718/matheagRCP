/**
 * 
 */
package de.egladil.mathejungalt.persistence.api;

import java.util.List;

import de.egladil.mathejungalt.domain.medien.Buch;
import de.egladil.mathejungalt.domain.medien.Zeitschrift;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.domain.quellen.AbstractQuelle;
import de.egladil.mathejungalt.domain.quellen.Buchquelle;
import de.egladil.mathejungalt.domain.quellen.Kindquelle;
import de.egladil.mathejungalt.domain.quellen.NullQuelle;
import de.egladil.mathejungalt.domain.quellen.Zeitschriftquelle;

/**
 * Interface zum Zugriff auf die Persistenzschicht für das Quellen-Aggregat.
 * 
 * @author heike
 */
public interface IQuellenRepository {

	/**
	 * Speichert die gegebene Quelle
	 * 
	 * @param pQuelle
	 */
	public abstract void save(AbstractQuelle pQuelle);

	/**
	 * @param pId
	 * @return
	 */
	public abstract AbstractQuelle findById(Long pId);

	/**
	 * Sucht die Quelle, die den gegebenen Schl�ssel hat.
	 * 
	 * @param pSchluessel String
	 * @return {@link AbstractQuelle} oder null, wenn es keine gibt.
	 */
	public AbstractQuelle findBySchluessel(String pSchluessel);

	/**
	 * Sucht Buchquelleneintrag zu gegebenem Buch und gegebener Seite.
	 * 
	 * @param pBuch {@link Buch} das Buch
	 * @param pSeite int die Seitenzahl
	 * @return {@link Buchquelle}
	 */
	public abstract Buchquelle findBuchquelle(final Buch pBuch, final int pSeite);

	/**
	 * Sucht Zeitschriftquelle mit dieser Zeitschrift, Ausgabe, Jahrgang.
	 * 
	 * @param pZeitschrift {@link Zeitschrift}
	 * @param pAusgabe int die Ausgabe
	 * @param pJahrgang int der Jahrgang
	 * @return {@link Zeitschriftquelle}
	 */
	public Zeitschriftquelle findZeitschriftquelle(Zeitschrift pZeitschrift, int pAusgabe, int pJahrgang);

	/**
	 * Sucht alle Buchquellen zu gegebenem Buch.
	 * 
	 * @param pBuch {@link Buch}
	 * @return {@link List} von {@link Buchquelle}
	 */
	public List<Buchquelle> findAllByBuch(final Buch pBuch);

	/**
	 * Sucht alle Quellen zu gegebener Zeitschrift.
	 * 
	 * @param pZeitschrift {@link Zeitschrift}
	 * @return {@link List} von {@link Zeitschriftquelle}
	 */
	public List<Zeitschriftquelle> findAllByZeitschrift(Zeitschrift pZeitschrift);

	/**
	 * Sucht alle Quellen mit diesem Mitglied.
	 * 
	 * @param pMitglied
	 * @return {@link List} von {@link Kindquelle} Objekten.
	 */
	public List<Kindquelle> findAllByMitglied(Mitglied pMitglied);

	/**
	 * @return String den nächsten freien fachlichen Schlüssel.
	 */
	public String getMaxKey();

	/**
	 * Sucht Quelle zu diesem Mitglied mit diesem Alter und dieser Klassenstufe.
	 * 
	 * @param pMitglied
	 * @param pJahre
	 * @param pKlasse
	 * @return {@link Kindquelle}
	 */
	public Kindquelle findKindquelle(Mitglied pMitglied, int pJahre, int pKlasse);

	/**
	 * Methode, um eine {@link AbstractQuelle} zu registrieren, wenn nötig
	 * 
	 * @param pQuelle
	 */
	public void register(AbstractQuelle pQuelle);

	/**
	 * Läd die fehlenden Attribute nach
	 * 
	 * @param pQuelle
	 */
	public abstract void completeQuelle(AbstractQuelle pQuelle);

	/**
	 * @param pMitgliederRepository the mitgliederRepository to set
	 */
	public void setMitgliederRepository(IMitgliederRepository pMitgliederRepository);

	/**
	 * @param pMedienRepository the medienRepository to set
	 */
	public void setMedienRepository(IMedienRepository pMedienRepository);

	/**
	 * Die (einzige) Nullquelle.
	 * 
	 * @return
	 */
	public NullQuelle findNullQuelle();
}
