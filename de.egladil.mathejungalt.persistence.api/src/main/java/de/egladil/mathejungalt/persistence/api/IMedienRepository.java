/**
 * 
 */
package de.egladil.mathejungalt.persistence.api;

import java.util.List;

import de.egladil.mathejungalt.domain.medien.AbstractPrintmedium;
import de.egladil.mathejungalt.domain.medien.Autor;
import de.egladil.mathejungalt.domain.medien.Buch;
import de.egladil.mathejungalt.domain.types.EnumTypes.Medienart;

/**
 * Interface zum Zugriff auf die Persistenzschicht für das Medien-Aggregat.
 * 
 * @author heike
 */
public interface IMedienRepository {

	/**
	 * @return
	 */
	public abstract List<AbstractPrintmedium> findAllMedien();

	/**
	 * Sucht alle Medien der gegebenen Art
	 * 
	 * @param pArt
	 * @return List
	 */
	public abstract List<AbstractPrintmedium> findMedienByArt(final Medienart pArt);

	/**
	 * L�d das Buch mit gegebener Id und seine Autoren.
	 * 
	 * @param pId {@link Long} die Id
	 * @return {@link Buch}
	 */
	public abstract AbstractPrintmedium findMediumById(Long pId);

	/**
	 * Speichert ein Printmedium
	 * 
	 * @param pMedium ein {@link AbstractPrintmedium}
	 */
	public abstract void saveMedium(final AbstractPrintmedium pMedium);

	/**
	 * Sucht Medien nach Titel.
	 * 
	 * @param pTitel der Titel
	 * @return {@link List} von {@link AbstractPrintmedium} mit �hnlichem Titel
	 */
	public abstract List<AbstractPrintmedium> findByTitel(final String pTitel);

	/**
	 * Sucht B�cher mit gegebenem Autor.
	 * 
	 * @param pAutor
	 * @return
	 */
	public abstract List<Buch> findBuecherByAutor(Autor pAutor);

	/**
	 * Sucht B�cher mit Autor dieses oder eines �hnlichen Namens.
	 * 
	 * @param pString
	 * @return
	 */
	public abstract List<Buch> findBuecherByAutorName(String pString);

	/**
	 * L�scht das gegebene Printmedium, sofern es keine Verkn�pfungen mehr hat.
	 * 
	 * @param pZeitschrift
	 */
	public abstract void removeMedium(AbstractPrintmedium pMedium);

	/**
	 * Ermittelt die Anzahl der Quellen zu diesem Printmedium
	 * 
	 * @param pMedium
	 * @return
	 */
	public abstract long anzahlQuellen(AbstractPrintmedium pMedium);

	/**
	 * Sucht den maximalen fachlichen Schl�ssel f�r MEDIEN.
	 * 
	 * @return String
	 */
	public abstract String getMaxKey();

	/**
	 * @param pAutorenRepository the autorenRepository to set
	 */
	public void setAutorenRepository(IAutorenRepository pAutorenRepository);
}