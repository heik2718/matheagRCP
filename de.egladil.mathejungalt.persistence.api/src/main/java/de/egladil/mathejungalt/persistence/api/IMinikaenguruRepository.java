/**
 * 
 */
package de.egladil.mathejungalt.persistence.api;

import java.util.List;

import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguruitem;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.schulen.Kontakt;

/**
 * @author heike
 */
public interface IMinikaenguruRepository {

	/**
	 * L�d alle {@link Minikaenguru} Wettbewerbe.
	 * 
	 * @return {@link List} von {@link Serie}
	 */
	public abstract List<Minikaenguru> findAllWettbewerbe();

	/**
	 * Speichert den gegebenen {@link Minikaenguru} Wettbewerb
	 * 
	 * @param pWettbewerb der {@link Minikaenguru} Wettbewerb.
	 */
	public abstract void save(Minikaenguru pWettbewerb);

	/**
	 * Sucht das Minikaenguruitem mit dieser Aufgabe
	 * 
	 * @param pAufgabe
	 * @return
	 */
	public abstract Minikaenguruitem findByAufgabe(Aufgabe pAufgabe);

	/**
	 * Ermittelt alle {@link Minikaenguruitem} diese Wettbewerbs.
	 * 
	 * @param pWettbewerb {@link Minikaenguru} der Wettbewerb
	 * @return {@link List} von {@link Minikaenguruitem}
	 */
	public abstract void loadItems(final Minikaenguru pWettbewerb);

	/**
	 * Mit id suchen
	 * 
	 * @param pId
	 * @return
	 */
	public abstract Minikaenguru findById(Long pId);

	/**
	 * @param pItem
	 */
	public abstract void saveItem(Minikaenguruitem pItem);

	/**
	 * Ermittelt den größten aktuellen fachlichen Schlüssel
	 * 
	 * @return Integer
	 */
	public abstract Integer getMaxKey();

	/**
	 * @param pAufgabenRepository
	 */
	public void setAufgabenRepository(IAufgabenRepository pAufgabenRepository);

	/**
	 * @param minikaenguru
	 * @param loeschliste
	 * @param zugangsliste
	 */
	public void mailinglisteAktualisieren(Minikaenguru minikaenguru, List<Kontakt> loeschliste,
		List<Kontakt> zugangsliste);

	/**
	 * 
	 * @param pMinikaenguru
	 * @return
	 */
	public List<Kontakt> loadMailingliste(Minikaenguru pMinikaenguru);

}