/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.filters;

import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.jface.viewers.Viewer;

import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * Filter, der alle Objekte durchl�sst, die NICHT das Muster treffen. Wenn das Muster leer ist, werden alle Objekte
 * durchgelassen.
 * 
 * @author Winkelv
 */
public class AufgabenzweckNegativeFilter extends AbstractMatheAGObjectFilter {

	public final static String ALLE = "alle";

	/**
	 * @param pViewer
	 */
	public AufgabenzweckNegativeFilter(StructuredViewer pViewer, IStammdatenservice pStammdatenservice) {
		super(pViewer, pStammdatenservice);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public boolean select(Viewer pViewer, Object pParentElement, Object pElement) {
		if (getPattern() == null || ALLE.equals(getPattern()))
			return true;
		return !getStringMatcher().match(((Aufgabe) pElement).getZweck().toString());
	}
}
