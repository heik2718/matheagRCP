/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * @author winkelv
 */
public class MCRaetselitemDetailsPage extends AbstractDetailsPage {

	/** */
	private MCRaetselitemSimpleAttributePartDataBindingProvider contentProvider;

	/**
	 * @param pMaster
	 * @param pDetailsInput
	 */
	public MCRaetselitemDetailsPage(AbstractMasterDetailsBlock pMaster) {
		super(pMaster);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractMatheAGDetailsPage#getAttributesPart()
	 */
	@Override
	protected MatheAGObjectAttributesPart getAttributesPart() {
		contentProvider = new MCRaetselitemSimpleAttributePartDataBindingProvider();
		return new MatheAGObjectAttributesPart(true, contentProvider);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractDetailsPage#getHeader()
	 */
	@Override
	protected String getHeader() {
		return ITextConstants.SECTION_HEADER_AUFGABENSAMMLUNGITEM_ATTRIBUTES;
	}
}
