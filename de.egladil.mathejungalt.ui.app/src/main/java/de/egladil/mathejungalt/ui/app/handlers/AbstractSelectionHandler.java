/**
 *
 */
package de.egladil.mathejungalt.ui.app.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;

/**
 * Abstrakte Oberklasseür einen Handler, der auf einem {@link AbstractMatheAGObject} agiert.
 *
 * @author aheike
 */
public abstract class AbstractSelectionHandler extends AbstractHandler {

	/** */
	private ISelectionProvider selectionProvider;

	private String parameter;

	/**
	 * @param pSelectionProvider
	 */
	public AbstractSelectionHandler(ISelectionProvider pSelectionProvider) {
		super();
		selectionProvider = pSelectionProvider;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent pArg0) throws ExecutionException {
		ISelection selection = selectionProvider.getSelection();
		if (selection == null || !(selection instanceof IStructuredSelection)) {
			return null;
		}
		String parameterId = getCommandParameterId();
		if (parameterId != null) {
			parameter = pArg0.getParameter(parameterId);
		}
		run();
		return null;
	}

	/**
	 * @return String id des command-Parameters. Gehen davon aus, dass die Commands höchstens einen Parameter brauchen.
	 */
	protected String getCommandParameterId() {
		return null;
	}

	/**
	 * Führt die gewünschte Aktion aus.
	 */
	protected abstract void run();

	/**
	 * @return the selectionProvider
	 */
	protected final ISelectionProvider getSelectionProvider() {
		return selectionProvider;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.AbstractHandler#isEnabled()
	 */
	@Override
	public boolean isEnabled() {
		ISelection selection = selectionProvider.getSelection();
		return selection instanceof IStructuredSelection && !selection.isEmpty();
	}

	/**
	 * Gibt bei einer Mehrfachselektion das erste selektierte Teil zur�ck oder null, falls nichts ausgew�hlt wurde.
	 *
	 * @return {@link AbstractMatheAGObject} oder null
	 */
	protected AbstractMatheAGObject getFirstSelectedObject() {
		ISelection selection = selectionProvider.getSelection();
		if (!(selection instanceof IStructuredSelection) || selection.isEmpty()) {
			return null;
		}
		return (AbstractMatheAGObject) ((IStructuredSelection) selection).getFirstElement();
	}

	/**
	 * @return
	 */
	protected int anzahlSelected() {
		ISelection selection = selectionProvider.getSelection();
		if (!(selection instanceof IStructuredSelection) || selection.isEmpty()) {
			return 0;
		}
		return ((IStructuredSelection) selection).size();
	}

	/**
	 * @return the parameter
	 */
	protected String getParameter() {
		return parameter;
	}
}
