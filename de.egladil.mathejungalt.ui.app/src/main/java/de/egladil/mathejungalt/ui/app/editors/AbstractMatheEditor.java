/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.widgets.FormToolkit;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.AbstractMatheEditorInput;

/**
 * Abstrakte Oberklasse für alle Editoren von {@link AbstractMatheAGObject} Instanzen.
 *
 * @author winkelv
 */
public abstract class AbstractMatheEditor extends FormEditor implements PropertyChangeListener {

	/** */
	private boolean modified;

	/**
	 *
	 */
	public AbstractMatheEditor() {
	}

	/**
	 * @return {@link AbstractMatheAGObject} das Domain-Objekt, das editiert wird.
	 */
	public AbstractMatheAGObject getDomainObject() {
		return ((AbstractMatheEditorInput) getEditorInput()).getDomainObject();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.FormEditor#dispose()
	 */
	@Override
	public void dispose() {
		AbstractMatheAGObject domainObject = getDomainObject();
		if (domainObject != null) {
			domainObject.disposePropertyChangeSupport();
		}
		// falls der Editor geschlossen wird, ohne zu speichern, müssen die Modelländerungen rückgängig gemacht werden.
		if (isDirty()) {
			((AbstractMatheEditorInput) getEditorInput()).undoChanges();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.FormEditor#init(org.eclipse.ui.IEditorSite, org.eclipse.ui.IEditorInput)
	 */
	@Override
	public void init(IEditorSite pSite, IEditorInput pInput) throws PartInitException {
		checkEditorInput(pInput);
		super.init(pSite, pInput);
		AbstractMatheAGObject domainObject = getDomainObject();
		if (domainObject != null)
			domainObject.addPropertyChangeListener(this);
		if (domainObject != null && domainObject.isNew())
			setModified(true);
		updateTitel();
	}

	/**
	 * Aktualisiert den Titel, der in der Editor-Tab angezeigt wird.
	 */
	protected abstract void updateTitel();

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.FormEditor#isDirty()
	 */
	@Override
	public boolean isDirty() {
		return modified;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.EditorPart#doSaveAs()
	 */
	@Override
	public void doSaveAs() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.FormEditor#createToolkit(org.eclipse.swt.widgets.Display)
	 */
	@Override
	protected FormToolkit createToolkit(Display pDisplay) {
		return new FormToolkit(MatheJungAltActivator.getDefault().getFormColors(pDisplay));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent pEvt) {
		if (((AbstractMatheEditorInput) getEditorInput()).isAdjusting()) {
			return;
		}
		if (!interestingPropertyChangeEvent(pEvt)) {
			return;
		}
		if ((pEvt.getOldValue() == null && pEvt.getNewValue() != null)
			|| (pEvt.getNewValue() == null && pEvt.getOldValue() != null)
			|| (!pEvt.getOldValue().equals(pEvt.getNewValue()))) {
			setModified(true);
		}
		if (isTitleChangingEvent(pEvt)) {
			updateTitel();
		}
	}

	/**
	 * Prüft, ob das {@link PropertyChangeEvent} zur Änderung des Editor-Titels führt.
	 *
	 * @param pEvt {@link PropertyChangeEvent} das event
	 * @return boolean
	 */
	protected abstract boolean isTitleChangingEvent(PropertyChangeEvent pEvt);

	/**
	 * Prüft, ob das {@link PropertyChangeEvent} zur Änderung des Editor-Modus (read-write) führt. Im Normalfall nicht.
	 *
	 * @param pEvt {@link PropertyChangeEvent} das event
	 * @return boolean
	 */
	protected boolean isEditorModusChangingEvent(PropertyChangeEvent pEvt) {
		return false;
	}

	/**
	 * Prüft, ob es ein interessantes {@link PropertyChangeEvent} ist
	 *
	 * @param pEvt {@link PropertyChangeEvent}
	 * @return boolean
	 */
	protected abstract boolean interestingPropertyChangeEvent(PropertyChangeEvent pEvt);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.EditorPart#isSaveAsAllowed()
	 */
	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	/**
	 * Prüft, ob das ein gültiger EditorInput ist.
	 *
	 * @param pInput
	 * @throws PartInitException
	 */
	protected abstract void checkEditorInput(IEditorInput pInput) throws PartInitException;

	/**
	 * @return the modified
	 */
	public boolean isModified() {
		return modified;
	}

	/**
	 * @param pModified the modified to set
	 */
	public void setModified(boolean pModified) {
		modified = pModified;
		// muss passieren, damit der save-Button aktiviert wird!!!
		firePropertyChange(IEditorPart.PROP_DIRTY);
	}

	/**
	 * @return the stammdatenservice
	 */
	protected IStammdatenservice getStammdatenservice() {
		IStammdatenservice stammdatenservice = MatheJungAltActivator.getDefault().getStammdatenservice();
		return stammdatenservice;
	}

	public abstract String getId();
}
