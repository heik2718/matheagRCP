/**
 *
 */
package de.egladil.mathejungalt.ui.app.handlers;

/**
 * @author aheike
 */
public interface ICommandIds {

	/** */
	static final String EDIT_OBJECT = "de.egladil.mathejungalt.ui.app.editObject";

	/** */
	static final String GENERIEREN = "de.egladil.mathejungalt.ui.app.generate";

	/** */
	static final String ARCHIVSEITE_GENERIEREN = "de.egladil.mathejungalt.ui.app.serie.generateArchiv";

	/** */
	static final String RAETSELLISTE_GENERIEREN = "de.egladil.mathejungalt.ui.app.mcraetsel.generateRaetselliste";

	static final String HTLATEX_VORBEREITEN = "de.egladil.mathejungalt.ui.app.raetsel.prepareHtlatex";

	static final String HTLATEX_NACHBEREITEN = "de.egladil.mathejungalt.ui.app.raetsel.postProcessHtlatex";

	/** */
	static final String PRUEFEN = "de.egladil.mathejungalt.ui.app.check";

	/** */
	static final String ANLEGEN = "de.egladil.mathejungalt.ui.app.anlegen";

	static final String MAILINGLISTE_PFLEGEN = "de.egladil.mathejungalt.ui.app.minikaenguru.mailingliste.pflegen";

	/** */
	static final String DIPLOM_GEBEN = "de.egladil.mathejungalt.ui.app.mitglied.diplomGeben";

	/** */
	static final String MITGLIED_DEAKTIVIEREN = "de.egladil.mathejungalt.ui.app.mitglied.deaktivieren";

	/** */
	static final String KONTAKT_HINZUFUEGEN = "de.egladil.mathejungalt.ui.app.mitglied.kontaktHinzufuegen";

	/** */
	static final String KONTAKT_ENTFERNEN = "de.egladil.mathejungalt.ui.app.mitglied.kontaktEntfernen";

	/** */
	static final String NUMERIEREN = "de.egladil.mathejungalt.ui.app.numerieren";

	/** */
	static final String PERSISTENZ_ANZEIGEN = "de.egladil.mathejungalt.ui.app.showPersistenceLocation";

	/** */
	static final String BEENDEN = "de.egladil.mathejungalt.ui.app.beenden";

	/** */
	static final String SPERREND_SETZEN = "de.egladil.mathejungalt.ui.app.sperrendSetzen";

	/** */
	static final String FLAG_SCHLECHT_AENDERN = "de.egladil.mathejungalt.ui.app.aufgabe.changeSchlechtFlag";

	/** */
	static final String PUBLISH = "de.egladil.mathejungalt.ui.app.raetsel.publish";

	static final String AUFGABEN_PS_GENERIEREN = "de.egladil.mathejungalt.ui.app.generatePs";

	static final String KAENG_TEMPLATES_GENERIEREN = "de.egladil.mathejungalt.ui.app.generateKaenguruTemplates";

	static final String MAILINKLISTE_MINIKAENGURU_ANZEIGEN = "de.egladil.mathejungalt.ui.app.showMailsMinikaenguru";
}
