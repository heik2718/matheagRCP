/**
 * 
 */
package de.egladil.mathejungalt.ui.app.dialogs;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.SWT;

import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.themes.IImageKeys;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * @author Winkelv
 */
public class ShowNullquelleWizardPage extends AbstractSelectMatheAGObjectWizardPage {

	/** */
	private IStammdatenservice stammdatenservice;

	/**
	 * @param pPageName
	 * @param pTitle
	 * @param pTitleImage
	 * @param pLabelProvider
	 */
	public ShowNullquelleWizardPage(IStammdatenservice pStammdatenservice) {
		super(ITextConstants.QUELLE_SELECT_NULLQUELLE_PAGE_NAME, ITextConstants.QUELLE_SELECT_NULLQUELLE_PAGE_TITLE,
			MatheJungAltActivator.imageDescriptorFromPlugin(MatheJungAltActivator.PLUGIN_ID, IImageKeys.QUELLE_WIZARD),
			new LabelProvider(), SWT.SINGLE);
		setDescription(ITextConstants.QUELLE_SELECT_NULLQUELLE_PAGE_DESCR);
		stammdatenservice = pStammdatenservice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.WizardPage#isPageComplete()
	 */
	@Override
	public boolean isPageComplete() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.dialogs.AbstractSelectMatheAGObjectWizardPage#getListElements()
	 */
	@Override
	protected Object[] getListElements() {
		return new Object[] { stammdatenservice.getNullQuelle() };
	}
}
