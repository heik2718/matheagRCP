/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.editorinputs;

import org.eclipse.core.runtime.Assert;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.ui.app.editors.kopierer.AufgabeKopierer;
import de.egladil.mathejungalt.ui.app.editors.kopierer.IKopierer;

/**
 * @author Winkelv
 */
public class AufgabeEditorInput extends AbstractMatheEditorInput {

	/**
	 * @param pObject
	 */
	public AufgabeEditorInput(AbstractMatheAGObject pObject) {
		super(pObject);
		Assert.isTrue(pObject instanceof Aufgabe, "Das Objekt ist keine Aufgabe!");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.editorinputs.AbstractMatheEditorInput#getKopierer()
	 */
	@Override
	public IKopierer getKopierer() {
		return new AufgabeKopierer();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.editorinputs.AbstractMatheEditorInput#initCache()
	 */
	@Override
	public void initCache() {
		setCache(new Aufgabe());
		applyChanges();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Object getAdapter(Class pAdapter) {
		if (pAdapter.getName().equals(Aufgabe.class.getName()))
			return getDomainObject();
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object pObj) {
		if (pObj == null)
			return false;
		if (!(pObj instanceof AufgabeEditorInput))
			return false;
		AufgabeEditorInput param = (AufgabeEditorInput) pObj;
		return getAdapter(Aufgabe.class).equals(param.getAdapter(Aufgabe.class));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.editorinputs.AbstractMatheEditorInput#getInputObjectType()
	 */
	@Override
	protected String getInputObjectType() {
		return Aufgabe.class.getName();
	}
}
