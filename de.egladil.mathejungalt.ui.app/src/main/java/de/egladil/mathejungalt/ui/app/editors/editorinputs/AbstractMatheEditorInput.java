/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.editorinputs;

import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.ui.app.editors.kopierer.IKopierer;

/**
 * Abstrakte Oberklasse für die EditorInputs.
 * 
 * @author Winkelv
 */
public abstract class AbstractMatheEditorInput implements IEditorInput {

	/** Das {@link AbstractMatheAGObject} das editiert werden soll */
	private AbstractMatheAGObject domainObject;

	/** Ein {@link AbstractMatheAGObject}, das als Cache dient */
	private AbstractMatheAGObject cache;

	/** */
	private IKopierer kopierer;

	/**
	 * 
	 */
	public AbstractMatheEditorInput(final AbstractMatheAGObject pObject) {
		super();
		Assert.isNotNull(pObject);
		domainObject = pObject;
		kopierer = getKopierer();
		initCache();
	}

	/**
	 * @return IKopierer
	 */
	public abstract IKopierer getKopierer();

	/**
	 * Initialisiert den Cache.
	 */
	public abstract void initCache();

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IEditorInput#exists()
	 */
	@Override
	public boolean exists() {
		return domainObject != null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IEditorInput#getImageDescriptor()
	 */
	@Override
	public ImageDescriptor getImageDescriptor() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IEditorInput#getName()
	 */
	@Override
	public String getName() {
		return domainObject.getKey().toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IEditorInput#getPersistable()
	 */
	@Override
	public IPersistableElement getPersistable() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IEditorInput#getToolTipText()
	 */
	@Override
	public String getToolTipText() {
		return getName();
	}

	/**
	 * Macht die Änderungen wieder rückgängig. Die Attribute des cache-Objekts werden in das domain-Objekt kopiert.
	 */
	public void undoChanges() {
		kopierer.copyAttributes(cache, domainObject);
	}

	/**
	 * Überträgt die Änderungen in die Historie. Die Attribute des domain-Objekts werden in das cache-Objekt kopiert.
	 */
	public void applyChanges() {
		kopierer.copyAttributes(domainObject, cache);
	}

	/**
	 * @return the domainObject
	 */
	public AbstractMatheAGObject getDomainObject() {
		return domainObject;
	}

	/**
	 * @return String den Typ des {@link AbstractMatheAGObject}, das zum EditorInput geh�rt. Hier muss der voll
	 * qualifiziere Pfadname angegeben werden. Dieser wird ben�tigt, um den EditorInput mittels der
	 * {@link MatheAGDataBaseElementFactory} zu rekonstruieren.
	 */
	protected abstract String getInputObjectType();

	/**
	 * @param pDomainObject the domainObject to set
	 */
	protected void setDomainObject(AbstractMatheAGObject pDomainObject) {
		domainObject = pDomainObject;
	}

	/**
	 * @return the cache
	 */
	protected AbstractMatheAGObject getCache() {
		return cache;
	}

	/**
	 * @param pCache the cache to set
	 */
	protected void setCache(AbstractMatheAGObject pCache) {
		cache = pCache;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return domainObject.hashCode();
	}

	/**
	 * @return the adjusting
	 */
	public final boolean isAdjusting() {
		return domainObject.isAdjusting();
	}

	/**
	 * @param pAdjusting the adjusting to set
	 */
	public final void setAdjusting(boolean pAdjusting) {
		domainObject.setAdjusting(pAdjusting);
	}
}
