/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.pages;

import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.forms.editor.FormEditor;

import de.egladil.mathejungalt.ui.app.editors.parts.MatheAGObjectAttributesPart;

/**
 * @author aheike
 */
public class AufgabensammlungAttributesFormPage extends AbstractAttributesFormPage {

	/**
	 * @param pEditor
	 * @param pId
	 * @param pTitle
	 */
	public AufgabensammlungAttributesFormPage(FormEditor pEditor, String pId, String pTitle) {
		super(pEditor, pId, pTitle);
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.pages.AbstractAttributesFormPage#getAttributesPart()
	 */
	@Override
	protected MatheAGObjectAttributesPart getAttributesPart() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.pages.AbstractAttributesFormPage#getHeader()
	 */
	@Override
	protected String getHeader() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.pages.AbstractAttributesFormPage#getImage()
	 */
	@Override
	protected Image getImage() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.pages.AbstractAttributesFormPage#getSectioHeader()
	 */
	@Override
	protected String getSectioHeader() {
		// TODO Auto-generated method stub
		return null;
	}
}
