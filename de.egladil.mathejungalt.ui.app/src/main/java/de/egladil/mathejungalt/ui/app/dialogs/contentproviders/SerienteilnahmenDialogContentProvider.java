/**
 * 
 */
package de.egladil.mathejungalt.ui.app.dialogs.contentproviders;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.Assert;

import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.domain.mitglieder.Serienteilnahme;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;

/**
 * @author winkelv
 */
public class SerienteilnahmenDialogContentProvider {

	/** */
	private Mitglied mitglied;

	/** */
	private IStammdatenservice serviceFacade;

	/**
	 * @param pMitglied
	 */
	public SerienteilnahmenDialogContentProvider(Mitglied pMitglied) {
		Assert.isNotNull(pMitglied);
		mitglied = pMitglied;
		serviceFacade = MatheJungAltActivator.getDefault().getStammdatenservice();
	}

	/**
	 * Alle abgeschlossenen Serien, an denen das Mitglied noch nicht teilgenommen hatte.
	 * 
	 * @return {@link Object}[]
	 */
	public Object[] getElements() {
		// TODO das gehört nicht zum Stammdatenservice. Hier Platz für einen extensionPoint lassen!
		Collection<Serie> serien = serviceFacade.getSerien();
		int aktuelleRunde = serviceFacade.aktuelleRunde();

		Collection<Serienteilnahme> teilnahmen = serviceFacade.serienteilnahmenZuMitgliedLaden(mitglied);

		List<Serie> mitgliedSerien = new ArrayList<Serie>();
		Iterator<Serienteilnahme> iter = teilnahmen.iterator();
		while (iter.hasNext())
			mitgliedSerien.add(iter.next().getSerie());

		List<Serie> liste = new ArrayList<Serie>();

		for (Serie serie : serien) {
			if (serie.getBeendet() == 1 && serie.getRunde() == aktuelleRunde && !mitgliedSerien.contains(serie))
				liste.add(serie);
		}
		return liste.toArray();
	}
}
