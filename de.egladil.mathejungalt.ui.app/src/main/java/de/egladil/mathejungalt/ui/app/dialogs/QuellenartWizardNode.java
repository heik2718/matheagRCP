/**
 * 
 */
package de.egladil.mathejungalt.ui.app.dialogs;

import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.IWizardNode;
import org.eclipse.swt.graphics.Point;

import de.egladil.mathejungalt.domain.types.EnumTypes.Quellenart;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Node für die CreateQuelleWizardSelectionPage
 * 
 * @author Winkelv
 */
public class QuellenartWizardNode implements IWizardNode {

	/** */
	private IWizard wizard;

	/** */
	private Quellenart quellenart;

	/**
	 * 
	 */
	public QuellenartWizardNode(Quellenart pQuellenart, CreateQuelleWizardSelectionPage pSelectionPage,
		IStammdatenservice pStammdatenservice) {
		super();
		quellenart = pQuellenart;
		createWizard(pQuellenart, pSelectionPage, pStammdatenservice);
	}

	/**
	 * @param pQuellenart
	 */
	private void createWizard(Quellenart pQuellenart, CreateQuelleWizardSelectionPage pSelectionPage,
		IStammdatenservice pStammdatenservice) {
		switch (pQuellenart) {
		case B:
			wizard = new CreateBuchquelleWizard(pSelectionPage, pStammdatenservice);
			break;
		case G:
			wizard = new CreateGruppenquelleWizard(pSelectionPage, pStammdatenservice);
			break;
		case K:
			wizard = new CreateKindquelleWizard(pSelectionPage, pStammdatenservice);
			break;
		case Z:
			wizard = new CreateZeitschriftquelleWizard(pSelectionPage, pStammdatenservice);
			break;
		default:
			wizard = new CreateNullquelleWizard(pSelectionPage, pStammdatenservice);
			break;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.IWizardNode#dispose()
	 */
	@Override
	public void dispose() {
		// ignore
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.IWizardNode#getExtent()
	 */
	@Override
	public Point getExtent() {
		return new Point(-1, -1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.IWizardNode#getWizard()
	 */
	@Override
	public IWizard getWizard() {
		return wizard;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.IWizardNode#isContentCreated()
	 */
	@Override
	public boolean isContentCreated() {
		// Wichtig, damit f�r die Seiten createControl() aufgerufen wird. Anderenfalls wird der gew�hlte Wizard nicht
		// angezeigt.
		return wizard.getPageCount() > 0;
	}

	@Override
	public String toString() {
		switch (quellenart) {
		case B:
			return ITextConstants.QUELLENART_BUCHQUELLE;
		case G:
			return ITextConstants.QUELLENART_GRUPPENQUELLE;
		case K:
			return ITextConstants.QUELLENART_KINDQUELLE;
		case Z:
			return ITextConstants.QUELLENART_ZEITSCHRIFTQUELLE;
		default:
			return ITextConstants.QUELLENART_NULLQUELLE;
		}
	}

	/**
	 * @return
	 */
	public final Quellenart getQuellenart() {
		return quellenart;
	}
}
