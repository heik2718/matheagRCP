/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.converters;

import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.domain.types.EnumTypes.Quellenart;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Konvertiert eine {@link Quellenart} in einen String.
 * 
 * @author Winkelv
 */
public class QuellenartToStringConverter implements IConverter {

	/**
	 * 
	 */
	public QuellenartToStringConverter() {
		//
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object pFromObject) {
		Quellenart param = (Quellenart) pFromObject;
		switch (param) {
		case B:
			return ITextConstants.QUELLENART_BUCHQUELLE;
		case G:
			return ITextConstants.QUELLENART_GRUPPENQUELLE;
		case K:
			return ITextConstants.QUELLENART_KINDQUELLE;
		case Z:
			return ITextConstants.QUELLENART_ZEITSCHRIFTQUELLE;
		default:
			return ITextConstants.QUELLENART_NULLQUELLE;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getFromType()
	 */
	@Override
	public Object getFromType() {
		return Quellenart.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getToType()
	 */
	@Override
	public Object getToType() {
		return String.class;
	}
}
