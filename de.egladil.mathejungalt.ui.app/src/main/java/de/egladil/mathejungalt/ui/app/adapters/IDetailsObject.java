/**
 *
 */
package de.egladil.mathejungalt.ui.app.adapters;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;

/**
 * Adapter-Interface für ein Details-Objekt (Serienteilnahme, AufgabensammlungItem, Autor...)
 *
 * @author Heike Winkelvoss (www.egladil.de)
 */
public interface IDetailsObject {

	/**
	 * Gibt das Master-Objekt zurueck.
	 *
	 * @return
	 */
	IMasterObject getMasterObject();

	/**
	 * @return das Domain-Object, das dahinter steht.
	 */
	AbstractMatheAGObject getDomainObject();
}
