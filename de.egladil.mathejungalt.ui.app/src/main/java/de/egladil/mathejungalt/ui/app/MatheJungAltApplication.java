package de.egladil.mathejungalt.ui.app;

import java.io.IOException;

import org.eclipse.core.runtime.Platform;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.osgi.service.datalocation.Location;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class controls all aspects of the application's execution
 */
public class MatheJungAltApplication implements IApplication {

	private static final Logger LOG = LoggerFactory.getLogger(MatheJungAltApplication.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.equinox.app.IApplication#start(org.eclipse.equinox.app.IApplicationContext)
	 */
	public Object start(IApplicationContext context) {
		Display display = PlatformUI.createDisplay();
		final Location instanceLocation = Platform.getInstanceLocation();
		try {
			System.err.println("[workspace-Location=" + instanceLocation.getURL().getPath() + "]");

			// Hier wird der Workspace gelockt. Beim parallelen Starten erscheint dann der Message-Dialog
			// und die Anwendung startet nicht erneut
			if (!instanceLocation.lock()) {
				MessageDialog.openWarning(display.getActiveShell(), "Achtung",
					"MatheJungAltApplication kann nur einmal gestartet werden");
				return IApplication.EXIT_OK;
			} else {
				int returnCode = PlatformUI.createAndRunWorkbench(display, new ApplicationWorkbenchAdvisor());
				if (returnCode == PlatformUI.RETURN_RESTART) {
					return IApplication.EXIT_RESTART;
				} else {
					return IApplication.EXIT_OK;
				}
			}
		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
			return IApplication.EXIT_OK;
		} finally {
			// Nicht vergessen, den lock wieder aufzuheben :)
			instanceLocation.release();
			display.dispose();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.equinox.app.IApplication#stop()
	 */
	public void stop() {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		if (workbench == null)
			return;
		final Display display = workbench.getDisplay();
		display.syncExec(new Runnable() {
			public void run() {
				if (!display.isDisposed())
					workbench.close();
			}
		});
	}
}
