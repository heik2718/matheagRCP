/**
 *
 */
package de.egladil.mathejungalt.ui.app.themes;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.FormToolkit;

/**
 * @author winkelv
 */
public final class PartControlsFactory {

	/**
	 *
	 */
	private PartControlsFactory() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Erzeugt das Label für das ID-Feld
	 *
	 * @param pClient {@link Composite}
	 * @param pToolkit {@link FormToolkit}
	 * @return {@link Label}
	 */
	public static Label createIdLabel(final Composite pClient, final FormToolkit pToolkit) {
		return PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_ID);
	}

	/**
	 * Erzeugt das Textfeld für die ID mit horizontalSpan 1
	 *
	 * @param pClient {@link Composite}
	 * @param pToolkit {@link FormToolkit}
	 * @param pWidthHint int ein Breitenhinweis
	 * @return {@link Text}
	 */
	public static Text createIdText(final Composite pClient, final FormToolkit pToolkit, int pWidthHint) {
		return PartControlsFactory.createText(pClient, pToolkit, pWidthHint, false);
	}

	/**
	 * Erzeugt das Textfeld für die ID. Horizontal span ist pHorizonalSpan.
	 *
	 * @param pClient {@link Composite}
	 * @param pToolkit {@link FormToolkit}
	 * @param pWidthHint int ein Breitenhinweis
	 * @param pHorizontalSpan die Anzahl der eingenommenen Spalten
	 * @return {@link Text}
	 */
	public static Text createIdText(final Composite pClient, final FormToolkit pToolkit, int pWidthHint,
		int pHorizontalSpan) {
		return PartControlsFactory.createText(pClient, pToolkit, pWidthHint, false, pHorizontalSpan);
	}

	/**
	 * Erzeugt das Label für das Schlüssel-Feld
	 *
	 * @param pClient {@link Composite}
	 * @param pToolkit {@link FormToolkit}
	 * @return {@link Label}
	 */
	public static Label createSchluesselLabel(final Composite pClient, final FormToolkit pToolkit) {
		return PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_SCHLUESSEL);
	}

	/**
	 * Erzeugt ein Textfeld mit Breitenhinweis und editierbar oder nicht. horizontalSpan ist 1
	 *
	 * @param pClient {@link Composite}
	 * @param pToolkit {@link FormToolkit}
	 * @param pWidthHint int ein Breitenhinweis
	 * @param pEditable boolean ob editierbar oder nicht
	 * @return {@link Text}
	 */
	public static Text createText(final Composite pClient, final FormToolkit pToolkit, int pWidthHint, boolean pEditable) {
		Text text = new Text(pClient, SWT.BORDER);// pToolkit.createText(pClient, "");
		GridData gd = LayoutFactory.getGridData(1, pWidthHint);
		text.setLayoutData(gd);
		text.setEditable(pEditable);
		if (!pEditable) {
			text.setBackground(PlatformUI.getWorkbench().getThemeManager().getCurrentTheme().getColorRegistry()
				.get(IColorKeys.LIGHT_GREY));
		}
		return text;
	}

	/**
	 * Erzeugt ein mehrzeiliges Textfeld mit Breitenhinweis und editierbar oder nicht. horizontalSpan ist 1
	 *
	 * @param pClient {@link Composite}
	 * @param pToolkit {@link FormToolkit}
	 * @param pWidthHint int ein Breitenhinweis
	 * @param pEditable boolean ob editierbar oder nicht
	 * @return {@link Text}
	 */
	public static Text createTextArea(final Composite pClient, final FormToolkit pToolkit, int pWidthHint,
		boolean pEditable) {
		Text text = new Text(pClient, SWT.MULTI | SWT.WRAP | SWT.V_SCROLL | SWT.BORDER); // pToolkit.createText(pClient,
																							// "");

		GridData gd = LayoutFactory.getGridData(1, pWidthHint);
		gd.heightHint = 30;
		text.setLayoutData(gd);
		text.setEditable(pEditable);
		if (!pEditable) {
			text.setBackground(PlatformUI.getWorkbench().getThemeManager().getCurrentTheme().getColorRegistry()
				.get(IColorKeys.LIGHT_GREY));
		}
		return text;
	}

	/**
	 * Erzeugt eine Combo-Box innerhalb des Containers pClient mit den gegebenen Items
	 *
	 * @param pClient
	 * @param pItems
	 * @return
	 */
	public static Control createCombo(final Composite pClient, final FormToolkit pToolkit, final String[] pItems,
		int pWidthHint) {
		final CCombo combo = new CCombo(pClient, SWT.READ_ONLY | SWT.FLAT | SWT.BORDER);
		GridData gd = LayoutFactory.getGridData(1, pWidthHint);
		combo.setLayoutData(gd);
		combo.setItems(pItems);
		pToolkit.adapt(combo, true, false);
		return combo;
	}

	/**
	 * Erzeugt ein Textfeld mit Breitenhinweis und editierbar oder nicht. horizontalSpan ist pHorizontalSpan
	 *
	 * @param pClient {@link Composite}
	 * @param pToolkit {@link FormToolkit}
	 * @param pWidthHint int ein Breitenhinweis
	 * @param pEditable boolean ob editierbar oder nicht
	 * @param pHorizontalSpan int die Anzahl Spalten, die eingenommen werden.
	 * @return {@link Text}
	 */
	public static Text createText(final Composite pClient, final FormToolkit pToolkit, int pWidthHint,
		boolean pEditable, int pHorizontalSpan) {
		Text text = new Text(pClient, SWT.BORDER);// pToolkit.createText(pClient, "");
		GridData gd = LayoutFactory.getGridData(pHorizontalSpan, pWidthHint);
		text.setLayoutData(gd);
		text.setEditable(pEditable);
		return text;
	}

	/**
	 * Erzeugt das Label für das Schlüssel-Feld mit horizontalSpan 1
	 *
	 * @param pClient {@link Composite}
	 * @param pToolkit {@link FormToolkit}
	 * @param pBeschriftung String die Labelbeschriftung
	 * @return {@link Label}
	 */
	public static Label createLabel(final Composite pClient, final FormToolkit pToolkit, final String pBeschriftung) {
		return pToolkit.createLabel(pClient, pBeschriftung);
	}

	/**
	 * Erzeugt das Label f�r das Schl�ssel-Feld mit horizontalSpan 1
	 *
	 * @param pClient {@link Composite}
	 * @param pToolkit {@link FormToolkit}
	 * @param pBeschriftung String die Labelbeschriftung
	 * @return {@link Label}
	 */
	public static Label createMessageLabel(final Composite pClient, final FormToolkit pToolkit, int pHorizontalSpan) {
		Label label = pToolkit.createLabel(pClient, "");
		GridData gd = LayoutFactory.getGridData(pHorizontalSpan);
		label.setLayoutData(gd);
		label.setForeground(PlatformUI.getWorkbench().getThemeManager().getCurrentTheme().getColorRegistry()
			.get(IColorKeys.RED));
		return label;
	}
}
