/**
 *
 */
package de.egladil.mathejungalt.ui.app.handlers;

import java.util.StringTokenizer;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.config.IMatheJungAltConfiguration.RaetsellisteGeneratorModus;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;

/**
 * @author aheike
 */
public class RaetsellisteGenerierenHandler extends AbstractHandler {

	/** */
	private IStammdatenservice stammdatenservice;

	/** */
	private final PathInitializer pathInitializer;

	private class GenerierenParameters {
		private Integer version;

		private Integer privat;

		/**
		 * @param pVersion
		 * @param pPrivat
		 */
		public GenerierenParameters(Integer pVersion, Integer pPrivat) {
			super();
			version = pVersion;
			privat = pPrivat;
		}

		/**
		 * @return the version
		 */
		public final Integer getVersion() {
			return version;
		}

		/**
		 * @return the privat
		 */
		public final Integer getPrivat() {
			return privat;
		}

	};

	/**
	 * @param pStammdatenservice
	 * @param pViewId
	 */
	public RaetsellisteGenerierenHandler(IStammdatenservice pStammdatenservice) {
		super();
		stammdatenservice = pStammdatenservice;
		pathInitializer = new PathInitializer();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent pArg0) throws ExecutionException {
		pathInitializer.initOutdirPath();
		if (pathInitializer.isCanceled()) {
			return null;
		}
		String pathOutDir = pathInitializer.getPathOutDir();
		final RaetsellisteGeneratorModus modus = MatheJungAltActivator.getDefault().getRaetselListeGeneratorModus();
		try {
			GenerierenParameters params = getParams();
			if (params == null || params.getVersion() == null || params.getPrivat() == null) {
				return null;
			}
			stammdatenservice.generateRaetselliste(params.getVersion(), pathOutDir, params.getPrivat() == 1, modus);
			MessageDialog.openInformation(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
				"Generator-Info", "Rätselliste nach " + pathOutDir + " generiert.\nModus=" + modus);
		} catch (MatheJungAltException e) {
			MessageDialog.openError(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
				"Generatorfehler (Modus=" + modus + ")", e.getMessage());
		}
		return null;
	}

	/**
	 * @return
	 */
	private GenerierenParameters getParams() {
		InputDialog dialog = new InputDialog(Display.getCurrent().getActiveShell(), "Rätselliste-Version",
			"Versionsnummer und privat-Flag (0/1) für die Rätselliste (durch ; getrennte Zahlen)", null, null);
		int output = dialog.open();
		if (IDialogConstants.OK_ID == output) {
			String value = dialog.getValue();
			String version = null;
			String privat = null;
			StringTokenizer st = new StringTokenizer(value, ";");
			while (st.hasMoreElements()) {
				version = st.nextToken();
				privat = st.nextToken();
			}
			try {
				Integer versionInt = Integer.valueOf(version);
				Integer privatInt = Integer.valueOf(privat);
				GenerierenParameters result = new GenerierenParameters(versionInt, privatInt);
				return result;
			} catch (NumberFormatException e) {
				return null;
			}
		}
		return null;
	}
}
