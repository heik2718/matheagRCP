/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * @author winkelv
 */
public class MiniteilnahmeDetailsPage extends AbstractDetailsPage {

	/** */
	private MiniteilnahmeSimpleAttributePartDataBindingProvider contentProvider;

	/**
	 * @param pMaster
	 * @param pInput
	 */
	public MiniteilnahmeDetailsPage(AbstractMasterDetailsBlockWithViewerPart pMaster) {
		super(pMaster);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractMatheAGDetailsPage#getAttributesPart()
	 */
	@Override
	protected MatheAGObjectAttributesPart getAttributesPart() {
		contentProvider = new MiniteilnahmeSimpleAttributePartDataBindingProvider();
		return new MatheAGObjectAttributesPart(true, contentProvider);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractDetailsPage#getHeader()
	 */
	@Override
	protected String getHeader() {
		return ITextConstants.SECTION_HEADER_MINITEILNAHME_ATTRIBUTES;
	}
}
