/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.converters;

import java.util.Collection;

import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.domain.mcraetsel.Urheber;
import de.egladil.mathejungalt.domain.types.MCAufgabenstufen;

/**
 * Konvertiert einen String in ein {@link MCAufgabenstufen}
 *
 * @author Winkelv
 */
public class StringToUrheberConverter implements IConverter {

	private Collection<Urheber> autoren;

	/**
	 *
	 */
	public StringToUrheberConverter(Collection<Urheber> pAutoren) {
		autoren = pAutoren;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object pFromObject) {
		String param = (String) pFromObject;
		if (param == null || param.isEmpty()) {
			return null;
		}
		for (Urheber u : autoren) {
			if (u.getName().equals(pFromObject)) {
				return u;
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getFromType()
	 */
	@Override
	public Object getFromType() {
		return String.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getToType()
	 */
	@Override
	public Object getToType() {
		return Urheber.class;
	}
}
