/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.converters;

import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.domain.types.EnumTypes.Thema;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Konvertiert einen String in ein {@link Thema}
 * 
 * @author Winkelv
 */
public class StringToThemaConverter implements IConverter {

	/**
	 * 
	 */
	public StringToThemaConverter() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object pFromObject) {
		String param = (String) pFromObject;
		if (ITextConstants.THEMA_ARITHMETIK.equals(param)) {
			return Thema.A;
		}
		if (ITextConstants.THEMA_GEOMETRIE.equals(param)) {
			return Thema.G;
		}
		if (ITextConstants.THEMA_KOMBINATORIK.equals(param)) {
			return Thema.K;
		}
		if (ITextConstants.THEMA_LOGIK.equals(param)) {
			return Thema.L;
		}
		if (ITextConstants.THEMA_WAHRSCHEINLICHKEIT.equals(param)) {
			return Thema.W;
		}
		return Thema.Z;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getFromType()
	 */
	@Override
	public Object getFromType() {
		return String.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getToType()
	 */
	@Override
	public Object getToType() {
		return Thema.class;
	}
}
