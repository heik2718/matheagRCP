/**
 *
 */
package de.egladil.mathejungalt.ui.app.dialogs.contentproviders;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.core.runtime.Assert;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.ui.app.adapters.IMasterObject;

/**
 * @author winkelv
 * @deprecated ersetzen durch ObjectPickerDialogContentProvider
 */
public class MasterDetailsSelectionDialogContentProvider {

	/** */
	private IMasterObject masterObject;

	/** */
	private final Collection<AbstractMatheAGObject> domainObjects;

	/**
	 * @param pMasterObject {@link IMasterObject}
	 */
	public MasterDetailsSelectionDialogContentProvider(IMasterObject pMasterObject,
		Collection<AbstractMatheAGObject> pDomainObjects) {
		Assert.isNotNull(pMasterObject);
		masterObject = pMasterObject;
		domainObjects = pDomainObjects;
	}

	/**
	 * @return
	 */
	public Object[] getElements() {
		List<AbstractMatheAGObject> elements = new ArrayList<AbstractMatheAGObject>();
		for (AbstractMatheAGObject domainObject : domainObjects) {
			if (masterObject.canAdd(domainObject)) {
				elements.add(domainObject);
			}
		}
		Collections.sort(elements);
		Object[] result = new Object[elements.size()];
		for (int i = 0; i < elements.size(); i++) {
			result[i] = elements.get(i);
		}
		return result;
	}

	/**
	 * Dieser Comparator sortiert die Items absteigend nach Label.
	 * 
	 * @return
	 */
	public Comparator<String> getComparator() {
		return new Comparator<String>() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
			 */
			@Override
			public int compare(String o1, String o2) {
				return o2.compareToIgnoreCase(o1);
			}
		};
	}
}
