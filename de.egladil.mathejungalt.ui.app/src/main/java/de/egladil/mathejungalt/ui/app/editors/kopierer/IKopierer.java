/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.kopierer;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;

/**
 * Kopiert die Attribute gegebener {@link AbstractMatheAGObject}
 * 
 * @author aheike
 */
public interface IKopierer {

	/**
	 * Kopiert die Attribute des Quellobjekts pSource auf das Zielobjekt pTarget
	 * 
	 * @param pSource {@link AbstractMatheAGObject} die Quelle
	 * @param pTarget {@link AbstractMatheAGObject} das Ziel
	 * @throws MatheJungAltException falls die gegebenen Opjekte nicht den geforderten Typ haben.
	 */
	public void copyAttributes(final AbstractMatheAGObject pSource, final AbstractMatheAGObject pTarget)
		throws MatheJungAltException;

}
