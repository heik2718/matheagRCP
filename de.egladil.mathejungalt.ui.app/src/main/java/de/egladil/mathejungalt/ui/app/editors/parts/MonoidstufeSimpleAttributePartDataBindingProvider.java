/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.ui.app.editors.converters.IntegerToMonoidStufetextConverter;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Der Teil, der die speziellen Stufen für Serienaufgaben anzeigt.
 * 
 * @author winkelv
 */
public class MonoidstufeSimpleAttributePartDataBindingProvider extends
	AbstractStufeSimpleAttributePartDataBindingProvider {

	/** */
	private final IConverter converter;

	/**
	 * 
	 */
	public MonoidstufeSimpleAttributePartDataBindingProvider() {
		super();
		converter = new IntegerToMonoidStufetextConverter();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.parts.AbstractStufeSimpleAttributePartDataBindingProvider#getItems()
	 */
	protected String[] getItems() {
		return new String[] { ITextConstants.MONOID_1, ITextConstants.MONOID_2 };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.editors.parts.AbstractStufeSimpleAttributePartDataBindingProvider#getConverter()
	 */
	@Override
	protected IConverter getConverter() {
		return converter;
	}
}
