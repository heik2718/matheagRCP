/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.listeners;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;
import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.domain.schulen.MinikaenguruTeilnahme;
import de.egladil.mathejungalt.domain.schulen.Schule;
import de.egladil.mathejungalt.service.stammdaten.event.ModelChangeEvent;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.adapters.IDetailsObject;
import de.egladil.mathejungalt.ui.app.dialogs.ShuttleDialog;
import de.egladil.mathejungalt.ui.app.editors.parts.AbstractViewerPartWithButtons;
import de.egladil.mathejungalt.ui.app.views.labelproviders.MinikaenguruLabelProvider;

/**
 * @author Winkelv
 */
public class MinikaenguruTeilnahmenViewerListener extends AbstractViewerPartWithButtonsViewerListener {

	private final ILabelProvider shuttleLabelProvider = new ColumnLabelProvider() {

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.jface.viewers.ColumnLabelProvider#getText(java.lang.Object)
		 */
		@Override
		public String getText(Object pElement) {
			if (pElement instanceof Kontakt) {
				return ((Kontakt) pElement).getEmail();
			}
			return super.getText(pElement);
		}

	};

	/**
	 * @param pView
	 */
	public MinikaenguruTeilnahmenViewerListener(AbstractViewerPartWithButtons pView) {
		super(pView);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.listeners.IStructuredViewerPartViewerListener#handleRemove()
	 */
	@Override
	public void handleRemove() {
		if (getEditorInput() == null || getSelectedDetails() == null || getSelectedDetails().size() != 1)
			return;
		Schule schule = (Schule) getEditorInput();
		MinikaenguruTeilnahme teilnahme = (MinikaenguruTeilnahme) getSelectedDetails().get(0);
		getStammdatenservice().minikaenguruTeilnahmeLoeschen(schule, teilnahme.getMinikaenguru());
		if (getView() != null) {
			getView().getViewer().setSelection(null);
			getView().updateButtonAndMenuStatus();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.editors.listeners.AbstractViewerPartWithButtonsViewerListener#isInterestingEvent
	 * (de.egladil. mathejungalt.service.stammdaten.event.ModelChangeEvent)
	 */
	@Override
	protected boolean isInterestingEvent(ModelChangeEvent pEvt) {
		if (ModelChangeEvent.ADD_EVENT.equals(pEvt.getEventType())
			|| ModelChangeEvent.REMOVE_EVENT.equals(pEvt.getEventType())) {
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.editors.listeners.AbstractViewerPartWithButtonsViewerListener#specialAdd(java.
	 * util.List)
	 */
	@Override
	protected void specialAdd(List<AbstractMatheAGObject> selectedObjects) {
		Schule schule = (Schule) getEditorInput();
		MinikaenguruTeilnahme teilnahme = null;
		if (selectedObjects != null) {
			Minikaenguru minikaenguru = (Minikaenguru) selectedObjects.get(0);
			teilnahme = getStammdatenservice().schuleNimmtTeil(schule, minikaenguru);
			Object detail = MatheJungAltActivator.getDefault().getAdapter(teilnahme, IDetailsObject.class);
			if (detail == null) {
				throw new MatheJungAltException("Kein Adapter fuer " + teilnahme.getClass().getName()
					+ " und IDetailsObject gefunden");
			}
			IDetailsObject detailsObject = (IDetailsObject) detail;
			updateDetailsList(detailsObject);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.listeners.AbstractViewerPartWithButtonsViewerListener#getObjectPool()
	 */
	@Override
	public Collection<AbstractMatheAGObject> getObjectPool() {
		Collection<AbstractMatheAGObject> objectPool = new ArrayList<AbstractMatheAGObject>();
		Collection<Minikaenguru> minikaengurus = getStammdatenservice().getMinikaengurus();
		Schule schule = (Schule) getEditorInput();

		Collection<MinikaenguruTeilnahme> teilnahmen = getStammdatenservice().minikaenguruTeilnahmenLaden(schule);

		List<Minikaenguru> schuleMinis = new ArrayList<Minikaenguru>();
		for (MinikaenguruTeilnahme teilnahme : teilnahmen) {
			schuleMinis.add(teilnahme.getMinikaenguru());
		}
		for (Minikaenguru mini : minikaengurus) {
			if (!schuleMinis.contains(mini)) {
				objectPool.add(mini);
			}
		}
		return objectPool;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.editors.listeners.AbstractViewerPartWithButtonsViewerListener#getLabelProvider()
	 */
	@Override
	public ColumnLabelProvider getLabelProvider() {
		return new MinikaenguruLabelProvider();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.editors.listeners.AbstractViewerPartWithButtonsViewerListener#handleEdit(org.eclipse
	 * .jface.viewers .IStructuredSelection)
	 */
	@Override
	public void handleEdit(IStructuredSelection pSelection) {
		if (pSelection.isEmpty()) {
			return;
		}
		Object first = pSelection.getFirstElement();
		if (first instanceof MinikaenguruTeilnahme) {
			MinikaenguruTeilnahme teilnahme = (MinikaenguruTeilnahme) first;
			Schule schule = (Schule) getEditorInput();
			Minikaenguru minikaenguru = teilnahme.getMinikaenguru();
			String shellText = "Mailingliste für Minikänguru " + minikaenguru.getJahr();
			if (!MatheJungAltActivator.getDefault().istAktuellesJahr(minikaenguru.getJahr())) {
				MessageDialog.openWarning(Display.getCurrent().getActiveShell(), shellText,
					"Mailingliste ist eingefroren");
				return;
			}
			List<Kontakt> alleKontakteSchule = getStammdatenservice().findKontakteZuSchule(schule);
			List<Kontakt> zugeordneteKontakte = getStammdatenservice().getKontakteMinikaenguru(teilnahme);
			List<Kontakt> verfuegbareKontakte = getFreieKontakte(alleKontakteSchule, zugeordneteKontakte);
			ShuttleDialog<Kontakt> dlg = new ShuttleDialog<Kontakt>(Display.getCurrent().getActiveShell(), shellText,
				shuttleLabelProvider, verfuegbareKontakte, zugeordneteKontakte);
			int result = dlg.open();
			if (Dialog.OK == result) {
				List<Kontakt> selectedKontakte = dlg.getSelectedObjects();
				if (!selectedKontakte.isEmpty()) {
					getStammdatenservice().mailinglisteAktualisieren(teilnahme, selectedKontakte);
				}
			}
		}
	}

	/**
	 * @param pAlleKontakteSchule
	 * @param pZugeordneteKontakte
	 * @return
	 */
	private List<Kontakt> getFreieKontakte(List<Kontakt> pAlleKontakteSchule, List<Kontakt> pZugeordneteKontakte) {
		List<Kontakt> result = new ArrayList<Kontakt>();
		for (Kontakt k : pAlleKontakteSchule) {
			result.add(k);
		}
		for (Kontakt k : pZugeordneteKontakte) {
			result.remove(k);
		}
		return result;
	}
}
