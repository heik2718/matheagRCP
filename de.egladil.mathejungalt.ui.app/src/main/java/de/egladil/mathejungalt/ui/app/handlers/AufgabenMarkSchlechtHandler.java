/**
 *
 */
package de.egladil.mathejungalt.ui.app.handlers;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * Generieren von LaTex und ps zu den gewaehlten Aufgaben.
 *
 * @author aheike
 */
public class AufgabenMarkSchlechtHandler extends AbstractSelectionHandler {

	private final static Logger LOG = LoggerFactory.getLogger(AufgabenMarkSchlechtHandler.class);

	/** */
	private IStammdatenservice stammdatenservice;

	/**
	 * @param pSelectionProvider
	 */
	public AufgabenMarkSchlechtHandler(ISelectionProvider pSelectionProvider, IStammdatenservice pStammdatenservice) {
		super(pSelectionProvider);
		stammdatenservice = pStammdatenservice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.handlers.AbstractSelectionHandler#run()
	 */
	@Override
	protected void run() {
		AbstractMatheAGObject object = getFirstSelectedObject();
		if (!(object instanceof Aufgabe)) {
			return;
		}
		Aufgabe aufgabe = (Aufgabe) object;
		Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		try {
			stammdatenservice.aufgabeStatusZuSchlechtFuerSerieAendern(aufgabe);

		} catch (MatheJungAltException e) {
			MessageDialog.openError(shell, "Fehler Diplom", "Flag konnte nicht getauscht werden \n" + e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.handlers.AbstractSelectionHandler#isEnabled()
	 */
	@Override
	public boolean isEnabled() {
		if (!super.isEnabled()) {
			return false;
		}
		int anzahl = anzahlSelected();
		if (anzahl != 1) {
			return false;
		}
		AbstractMatheAGObject firstSelectedObject = getFirstSelectedObject();
		if (!(firstSelectedObject instanceof Aufgabe)) {
			return false;
		}
		return true;
	}
}
