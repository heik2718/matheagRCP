/**
 * 
 */
package de.egladil.mathejungalt.ui.app.handlers;

import org.eclipse.core.runtime.Assert;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.AbstractMatheEditorInput;

/**
 * Ausgelagertes Öffnen eines Editors, um es in verschiedenen Actions verf�gbar zu machen
 * 
 * @author winkelv
 */
public class EditObjectHandlerDelegate {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(EditObjectHandlerDelegate.class);

	/** */
	private String editorId;

	/**
	 * @param pEditorId String die ID des zu Öffnenden Editors
	 */
	public EditObjectHandlerDelegate(final String pEditorId) {
		super();
		editorId = pEditorId;
	}

	/**
	 * Führt das Kommando aus.
	 * 
	 * @param pObject
	 */
	public void executeCommand(Object pObject) {
		Assert.isNotNull(editorId, "Editor kann nicht geöffnet werden: editorId ist null!");
		Assert.isNotNull(pObject);
		Assert.isTrue(pObject instanceof AbstractMatheEditorInput, "Das gegebene Objekt ist kein "
			+ AbstractMatheEditorInput.class.getName() + ", sondern " + pObject.getClass().getName());
		final AbstractMatheEditorInput editorInput = (AbstractMatheEditorInput) pObject;
		if (Display.getCurrent() != null && !Display.getCurrent().isDisposed()) {
			new Cursor(Display.getCurrent(), SWT.CURSOR_WAIT);

			Display.getCurrent().asyncExec(new Runnable() {

				@Override
				public void run() {
					IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
					try {
						editorInput.setAdjusting(true);
						page.openEditor(editorInput, editorId);
						editorInput.setAdjusting(false);
					} catch (PartInitException e) {
						String msg = "Der Editor " + editorId + " kann nicht geöffnet werden";
						LOG.error(msg, e);
						throw new MatheJungAltException(msg, e);
					}
				}
			});
		} else {
			LOG.info("Der Editor " + editorId + " kann nicht geöffnet werden: der Display ist nicht mehr aktiv");
		}
	}

	/**
	 * @param pEditorId the editorId to set
	 */
	public void setEditorId(String pEditorId) {
		editorId = pEditorId;
	}
}
