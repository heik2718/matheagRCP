/**
 * 
 */
package de.egladil.mathejungalt.ui.app.handlers;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.mitglieder.Diplom;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * @author aheike
 */
public class DiplomGebenHandler extends AbstractSelectionHandler {

	/** */
	private IStammdatenservice stammdatenservice;

	/**
	 * @param pSelectionProvider
	 */
	public DiplomGebenHandler(ISelectionProvider pSelectionProvider, IStammdatenservice pStammdatenservice) {
		super(pSelectionProvider);
		stammdatenservice = pStammdatenservice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.handlers.AbstractSelectionHandler#run()
	 */
	@Override
	protected void run() {
		AbstractMatheAGObject object = getFirstSelectedObject();
		if (!(object instanceof Mitglied)) {
			return;
		}
		Mitglied mitglied = (Mitglied) object;
		Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		try {
			Diplom diplom = stammdatenservice.diplomGeben(mitglied);
			MessageDialog.openInformation(shell, "Diplom", "Diplom mit " + diplom.getAnzahlErfinderpunkte()
				+ " Erfinderpunt(en) an " + mitglied.getVorname() + " vergeben.");
		} catch (MatheJungAltException e) {
			MessageDialog.openError(shell, "Fehler Diplom",
				"Das Diplom konnte nicht gegeben werden.\n Aktion rueckgaengig gemacht.\n" + e.getMessage());
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.handlers.AbstractSelectionHandler#isEnabled()
	 */
	@Override
	public boolean isEnabled() {
		if (!super.isEnabled()) {
			return false;
		}
		if (!(getFirstSelectedObject() instanceof Mitglied)) {
			return false;
		}
		return stammdatenservice.mitgliedBrauchtDiplom((Mitglied) getFirstSelectedObject());
	}
}
