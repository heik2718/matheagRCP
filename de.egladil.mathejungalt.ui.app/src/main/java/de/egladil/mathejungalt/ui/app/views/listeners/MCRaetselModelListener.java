/**
 *
 */
package de.egladil.mathejungalt.ui.app.views.listeners;

import java.beans.PropertyChangeEvent;

import org.eclipse.jface.viewers.TableViewer;

import de.egladil.mathejungalt.domain.mcraetsel.IMCArchivraetselNames;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.service.stammdaten.event.ModelChangeEvent;

/**
 * @author aheike
 */
public class MCRaetselModelListener extends AbstractModelListener {

	/**
	 * @param pViewer
	 */
	public MCRaetselModelListener(TableViewer pViewer) {
		super(pViewer);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.views.listeners.AbstractModelListener#isInterestingEvent(de.egladil.mathejungalt
	 * .service.stammdaten.event.ModelChangeEvent)
	 */
	@Override
	protected boolean isInterestingEvent(ModelChangeEvent pEvt) {
		if (ModelChangeEvent.ADD_EVENT.equals(pEvt.getEventType()) && pEvt.getNewValue() instanceof MCArchivraetsel) {
			return true;
		}
		if (ModelChangeEvent.REMOVE_EVENT.equals(pEvt.getEventType()) && pEvt.getOldValue() instanceof MCArchivraetsel) {
			return true;
		}
		if (ModelChangeEvent.PROPERTY_CHANGE_EVENT.equals(pEvt.getEventType())
			&& !(pEvt.getSource() instanceof MCArchivraetsel)) {
			return false;
		}
		final String property = pEvt.getProperty();
		return isRelevant(property);
	}

	/**
	 * @param pPropertyName
	 * @return boolean
	 */
	private boolean isRelevant(final String pPropertyName) {
		return IMCArchivraetselNames.PROP_SCHLUESSEL.equals(pPropertyName)
			|| IMCArchivraetselNames.PROP_STUFE.equals(pPropertyName)
			|| IMCArchivraetselNames.PROP_TITEL.equals(pPropertyName)
			|| IMCArchivraetselNames.PROP_VEROEFFENTLICHT.equals(pPropertyName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.ui.app.views.listeners.AbstractModelListener#isInterestingEvent(java.beans.
	 * PropertyChangeEvent)
	 */
	@Override
	protected boolean isInterestingEvent(PropertyChangeEvent pEvt) {
		if (!(pEvt.getSource() instanceof MCArchivraetsel)) {
			return false;
		}
		return isRelevant(pEvt.getPropertyName());
	}
}
