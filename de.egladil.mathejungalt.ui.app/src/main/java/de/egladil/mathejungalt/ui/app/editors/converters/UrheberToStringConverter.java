/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.converters;

import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.domain.mcraetsel.Urheber;
import de.egladil.mathejungalt.domain.types.MCAufgabenstufen;

/**
 * Konvertiert eine {@link MCAufgabenstufen} in einen String.
 *
 * @author Winkelv
 */
public class UrheberToStringConverter implements IConverter {

	/**
	 *
	 */
	public UrheberToStringConverter() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object pFromObject) {
		Urheber param = (Urheber) pFromObject;
		return param.getName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getFromType()
	 */
	@Override
	public Object getFromType() {
		return Urheber.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getToType()
	 */
	@Override
	public Object getToType() {
		return String.class;
	}
}
