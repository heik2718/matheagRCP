/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.comparators;

/**
 * @author Winkelv
 */
public abstract class AbstractViewerComparator implements IViewerComparator {

	/** */
	private int index;

	private String type;

	/**
	 * 
	 */
	public AbstractViewerComparator() {
		super();
	}

	/**
	 * @param pIndex
	 */
	public AbstractViewerComparator(int pIndex) {
		super();
		index = pIndex;
	}

	/**
	 * @param pType
	 */
	public AbstractViewerComparator(String pType) {
		super();
		type = pType;
	}

	/**
	 * @param pIndex
	 * @param pType
	 */
	public AbstractViewerComparator(int pIndex, String pType) {
		super();
		index = pIndex;
		type = pType;
	}

	/**
	 * @return the index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
}
