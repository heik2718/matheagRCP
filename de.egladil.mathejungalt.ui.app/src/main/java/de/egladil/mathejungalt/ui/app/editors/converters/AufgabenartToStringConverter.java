/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.converters;

import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenart;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Konvertiert eine {@link Aufgabenart} in einen String.
 * 
 * @author Winkelv
 */
public class AufgabenartToStringConverter implements IConverter {

	/**
	 * 
	 */
	public AufgabenartToStringConverter() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object pFromObject) {
		Aufgabenart param = (Aufgabenart) pFromObject;
		switch (param) {
		case E:
			return ITextConstants.AUFGABENART_EIGEN;
		case N:
			return ITextConstants.AUFGABENART_NACH;
		default:
			return ITextConstants.AUFGABENART_ZITAT;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getFromType()
	 */
	@Override
	public Object getFromType() {
		return Aufgabenart.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getToType()
	 */
	@Override
	public Object getToType() {
		return String.class;
	}
}
