/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.IMatheAGObjectNames;
import de.egladil.mathejungalt.domain.mitglieder.IMitgliedNames;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;
import de.egladil.mathejungalt.ui.app.themes.PartControlsFactory;

/**
 * @author winkelv
 */
public class MitgliedSimpleAttributePartDataBindingProvider extends AbstractSimpleAttributePartDataBindingProvider {

	/**
	 *
	 */
	public MitgliedSimpleAttributePartDataBindingProvider() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.contentproviders.AbstractAttributePartContentsProvider#anzahlZeilen()
	 */
	@Override
	protected int anzahlZeilen() {
		return 11;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.contentproviders.AbstractAttributePartContentsProvider#initLabelsAndTexts(org.eclipse
	 * .swt.widgets .Composite, org.eclipse.ui.forms.widgets.FormToolkit, boolean)
	 */
	@Override
	protected void initControls(Composite pClient, FormToolkit pToolkit, boolean pEditable,
		final AbstractMatheAGObject pInput) {
		final Control[] controls = getControls();
		Control[] labels = new Control[controls.length];

		int widthHint = 150;
		labels[0] = PartControlsFactory.createIdLabel(pClient, pToolkit);
		controls[0] = PartControlsFactory.createIdText(pClient, pToolkit, widthHint);

		labels[1] = PartControlsFactory.createSchluesselLabel(pClient, pToolkit);
		controls[1] = PartControlsFactory.createText(pClient, pToolkit, widthHint, pEditable);
		((Text) controls[1]).setTextLimit(IMitgliedNames.LENGTH_SCHLUESSEL);

		labels[2] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_VORNAME);
		controls[2] = PartControlsFactory.createText(pClient, pToolkit, widthHint, pEditable);
		((Text) controls[2]).setTextLimit(IMitgliedNames.LENGTH_VORNAME);

		labels[3] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_NACHNAME);
		controls[3] = PartControlsFactory.createText(pClient, pToolkit, widthHint, pEditable);
		((Text) controls[3]).setTextLimit(IMitgliedNames.LENGTH_NACHNAME);

		labels[4] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_ALTER);
		controls[4] = PartControlsFactory.createText(pClient, pToolkit, widthHint, pEditable);
		((Text) controls[4]).setTextLimit(IMitgliedNames.LENGTH_ALTER);

		labels[5] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_KLASSE);
		controls[5] = PartControlsFactory.createText(pClient, pToolkit, widthHint, pEditable);
		((Text) controls[5]).setTextLimit(IMitgliedNames.LENGTH_ALTER);

		labels[6] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_KONTAKT);
		controls[6] = PartControlsFactory.createText(pClient, pToolkit, widthHint, pEditable);
		((Text) controls[6]).setTextLimit(IMitgliedNames.LENGTH_KONTAKT);

		labels[7] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_GEBURTSMONAT);
		controls[7] = PartControlsFactory.createText(pClient, pToolkit, widthHint, pEditable);
		((Text) controls[7]).setTextLimit(IMitgliedNames.LENGTH_GEBURTSMONAT);

		labels[8] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_GEBURTSJAHR);
		controls[8] = PartControlsFactory.createText(pClient, pToolkit, widthHint, pEditable);
		((Text) controls[8]).setTextLimit(IMitgliedNames.LENGTH_GEBURTSJAHR + 1);

		labels[9] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_ERFINDERPUNKTE);
		controls[9] = PartControlsFactory.createText(pClient, pToolkit, widthHint, pEditable);

		labels[10] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_ANZAHL_DIPLOME);
		controls[10] = PartControlsFactory.createText(pClient, pToolkit, widthHint, false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.contentproviders.IAttributePartContentsProvider#refreshDataBindings(de.egladil.mathe
	 * .core.domain .AbstractMatheAGObject)
	 */
	@Override
	public void refreshDataBindings(AbstractMatheAGObject pInput) {
		super.refreshDataBindings(pInput);

		Binding[] bindings = getBindings();
		Control[] controls = getControls();
		DataBindingContext dbc = getDbc();

		IObservableValue observableWidget = SWTObservables.observeText(controls[0], SWT.Modify);
		IObservableValue observableProps = BeansObservables.observeValue(pInput, IMatheAGObjectNames.PROP_ID);
		bindings[0] = dbc.bindValue(observableWidget, observableProps, null, null);

		observableWidget = SWTObservables.observeText(controls[1], SWT.Modify);
		observableProps = BeansObservables.observeValue(pInput, IMatheAGObjectNames.PROP_SCHLUESSEL);
		bindings[1] = dbc.bindValue(observableWidget, observableProps, null, null);

		observableWidget = SWTObservables.observeText(controls[2], SWT.Modify);
		observableProps = BeansObservables.observeValue(pInput, IMitgliedNames.PROP_VORNAME);
		bindings[2] = dbc.bindValue(observableWidget, observableProps, null, null);

		observableWidget = SWTObservables.observeText(controls[3], SWT.Modify);
		observableProps = BeansObservables.observeValue(pInput, IMitgliedNames.PROP_NACHNAME);
		bindings[3] = dbc.bindValue(observableWidget, observableProps, null, null);

		observableWidget = SWTObservables.observeText(controls[4], SWT.Modify);
		observableProps = BeansObservables.observeValue(pInput, IMitgliedNames.PROP_ALTER);
		bindings[4] = dbc.bindValue(observableWidget, observableProps, null, null);

		observableWidget = SWTObservables.observeText(controls[5], SWT.Modify);
		observableProps = BeansObservables.observeValue(pInput, IMitgliedNames.PROP_KLASSE);
		bindings[5] = dbc.bindValue(observableWidget, observableProps, null, null);

		observableWidget = SWTObservables.observeText(controls[6], SWT.Modify);
		observableProps = BeansObservables.observeValue(pInput, IMitgliedNames.PROP_KONTAKT);
		bindings[6] = dbc.bindValue(observableWidget, observableProps, null, null);

		observableWidget = SWTObservables.observeText(controls[7], SWT.Modify);
		observableProps = BeansObservables.observeValue(pInput, IMitgliedNames.PROP_GEBURTSMONAT);
		bindings[7] = dbc.bindValue(observableWidget, observableProps, null, null);

		observableWidget = SWTObservables.observeText(controls[8], SWT.Modify);
		observableProps = BeansObservables.observeValue(pInput, IMitgliedNames.PROP_GEBURTSJAHR);
		bindings[8] = dbc.bindValue(observableWidget, observableProps, null, null);

		observableWidget = SWTObservables.observeText(controls[9], SWT.Modify);
		observableProps = BeansObservables.observeValue(pInput, IMitgliedNames.PROP_ERFINDERPUNKTE);
		bindings[9] = dbc.bindValue(observableWidget, observableProps, null, null);

		observableWidget = SWTObservables.observeText(controls[10], SWT.Modify);
		observableProps = BeansObservables.observeValue(pInput, IMitgliedNames.PROP_ANZAHL_DIPLOME);
		bindings[10] = dbc.bindValue(observableWidget, observableProps, null, null);
	}
}
