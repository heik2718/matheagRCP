/**
 *
 */
package de.egladil.mathejungalt.ui.app.handlers;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * Generieren von LaTex und ps zu den gewaehlten Aufgaben.
 *
 * @author aheike
 */
public class PublishQuizHandler extends AbstractSelectionHandler {

	/** */
	private IStammdatenservice stammdatenservice;

	/**
	 * @param pSelectionProvider
	 */
	public PublishQuizHandler(ISelectionProvider pSelectionProvider, IStammdatenservice pStammdatenservice) {
		super(pSelectionProvider);
		stammdatenservice = pStammdatenservice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.handlers.AbstractSelectionHandler#run()
	 */
	@Override
	protected void run() {
		AbstractMatheAGObject object = getFirstSelectedObject();
		if (!(object instanceof MCArchivraetsel)) {
			return;
		}
		MCArchivraetsel raetsel = (MCArchivraetsel) object;
		Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		try {
			stammdatenservice.publish(raetsel);
		} catch (MatheJungAltException e) {
			MessageDialog.openError(shell, "Fehler publish", "Flag konnte nicht getauscht werden \n" + e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.handlers.AbstractSelectionHandler#isEnabled()
	 */
	@Override
	public boolean isEnabled() {
		if (!super.isEnabled()) {
			return false;
		}
		int anzahl = anzahlSelected();
		if (anzahl != 1) {
			return false;
		}
		AbstractMatheAGObject firstSelectedObject = getFirstSelectedObject();
		if (!(firstSelectedObject instanceof MCArchivraetsel)) {
			return false;
		}
		MCArchivraetsel raetsel = (MCArchivraetsel) firstSelectedObject;
		if (!raetsel.isItemsLoaded()) {
			stammdatenservice.raetselitemsLaden(raetsel);
		}
		return raetsel.canPublish();
	}
}
