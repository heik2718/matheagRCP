package de.egladil.mathejungalt.ui.app;

import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.ICoolBarManager;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.action.ToolBarContributionItem;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.swt.SWT;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.eclipse.ui.actions.ContributionItemFactory;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;

/**
 * An action bar advisor is responsible for creating, adding, and disposing of the actions added to a workbench window.
 * Each window will be populated with new actions.
 */
public class ApplicationActionBarAdvisor extends ActionBarAdvisor {

	private IWorkbenchAction exitAction;

	/** */
	private IWorkbenchAction saveAction;

	/** */
	private IWorkbenchAction aboutAction;

	/** */
	private IWorkbenchAction openPreferencesAction;

	/** */
	private IContributionItem viewsShortList;

	/** */
	private IContributionItem perspShortList;

	public ApplicationActionBarAdvisor(IActionBarConfigurer configurer) {
		super(configurer);
	}

	protected void makeActions(final IWorkbenchWindow window) {
		saveAction = ActionFactory.SAVE.create(window);
		register(saveAction);

		exitAction = ActionFactory.QUIT.create(window);
		register(exitAction);

		aboutAction = ActionFactory.ABOUT.create(window);
		register(aboutAction);

		openPreferencesAction = ActionFactory.PREFERENCES.create(window);
		register(openPreferencesAction);

		viewsShortList = ContributionItemFactory.VIEWS_SHORTLIST.create(window);
		perspShortList = ContributionItemFactory.PERSPECTIVES_SHORTLIST.create(window);
	}

	protected void fillMenuBar(IMenuManager menuBar) {
		MenuManager matheMenu = createFileMenu();
		MenuManager windowMenu = createWindowMenu();
		MenuManager helpMenu = createHelpMenu();

		// Adding menus
		menuBar.add(matheMenu);
		// Add a group marker indicating where action set menus will appear.
		menuBar.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
		menuBar.add(windowMenu);
		menuBar.add(helpMenu);
	}

	/**
	 * @return
	 */
	private MenuManager createFileMenu() {
		MenuManager menu = new MenuManager("&File", IWorkbenchActionConstants.M_FILE);
		menu.add(new GroupMarker(IWorkbenchActionConstants.FILE_START));
		menu.add(saveAction);
		menu.add(new GroupMarker(IWorkbenchActionConstants.FILE_END));
		menu.add(new Separator());
		menu.add(exitAction);
		menu.add(new GroupMarker(IWorkbenchActionConstants.NEW_EXT));
		menu.add(new Separator());
		return menu;
	}

	private MenuManager createWindowMenu() {
		MenuManager windowMenu = new MenuManager("&Window", IWorkbenchActionConstants.M_WINDOW);

		// Submenu zum Öffnen von Perspektiven
		MenuManager perspMenu = new MenuManager("Open &Perspective");
		perspMenu.add(perspShortList);

		// Submenu zum Öffnen von Views
		MenuManager viewsMenu = new MenuManager("Show &View");
		viewsMenu.add(viewsShortList);

		windowMenu.add(perspMenu);
		windowMenu.add(viewsMenu);
		windowMenu.add(new Separator());
		windowMenu.add(openPreferencesAction);
		return windowMenu;
	}

	private MenuManager createHelpMenu() {
		MenuManager helpMenu = new MenuManager("&Help", IWorkbenchActionConstants.M_HELP);
		helpMenu.add(aboutAction);
		helpMenu.add(new Separator("additions"));

		return helpMenu;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.application.ActionBarAdvisor#fillCoolBar(org.eclipse.jface.action.ICoolBarManager)
	 */
	@Override
	protected void fillCoolBar(ICoolBarManager pCoolBar) {
		// MatheGroup
		IToolBarManager matheToolBar = new ToolBarManager(SWT.FLAT | SWT.RIGHT);
		pCoolBar.add(new ToolBarContributionItem(matheToolBar, IWorkbenchActionConstants.M_FILE));
		matheToolBar.add(new Separator(IWorkbenchActionConstants.NEW_GROUP));
		matheToolBar.add(saveAction);
		matheToolBar.add(new Separator(IWorkbenchActionConstants.NEW_GROUP));

		IToolBarManager editToolBar = new ToolBarManager(SWT.FLAT | SWT.RIGHT);
		pCoolBar.add(new ToolBarContributionItem(editToolBar, IWorkbenchActionConstants.M_EDIT));
		editToolBar.add(new Separator(IWorkbenchActionConstants.NEW_GROUP));
	}

}
