/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.listeners;

import java.beans.PropertyChangeEvent;

import org.eclipse.jface.viewers.TableViewer;

import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.IArbeitsblattNames;
import de.egladil.mathejungalt.service.stammdaten.event.ModelChangeEvent;

/**
 * @author aheike
 */
public class ArbeitsblaetterModelListener extends AbstractAufgabensammlungModelListener {

	/**
	 * @param pViewer
	 */
	public ArbeitsblaetterModelListener(TableViewer pViewer) {
		super(pViewer);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.views.listeners.AbstractModelListener#isInterestingEvent(de.egladil.mathejungalt
	 * .service.stammdaten.event.ModelChangeEvent)
	 */
	@Override
	protected boolean isInterestingEvent(ModelChangeEvent pEvt) {
		if (ModelChangeEvent.ADD_EVENT.equals(pEvt.getEventType()) && pEvt.getNewValue() instanceof Arbeitsblatt) {
			return true;
		}
		if (ModelChangeEvent.REMOVE_EVENT.equals(pEvt.getEventType()) && pEvt.getOldValue() instanceof Arbeitsblatt) {
			return true;
		}
		if (ModelChangeEvent.PROPERTY_CHANGE_EVENT.equals(pEvt.getEventType())
			&& !(pEvt.getSource() instanceof Arbeitsblatt)) {
			return false;
		}
		final String property = pEvt.getProperty();
		return isRelevant(property);
	}

	/**
	 * @param pPropertyName
	 * @return boolean
	 */
	private boolean isRelevant(final String pPropertyName) {
		return IArbeitsblattNames.PROP_TITEL.equals(pPropertyName)
			|| (IArbeitsblattNames.PROP_SPERREND.equals(pPropertyName))
			|| IAufgabensammlung.PROP_BEENDET.equals(pPropertyName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.ui.app.views.listeners.AbstractModelListener#isInterestingEvent(java.beans.
	 * PropertyChangeEvent)
	 */
	@Override
	protected boolean isInterestingEvent(PropertyChangeEvent pEvt) {
		if (!(pEvt.getSource() instanceof Arbeitsblatt)) {
			return false;
		}
		return isRelevant(pEvt.getPropertyName());
	}
}
