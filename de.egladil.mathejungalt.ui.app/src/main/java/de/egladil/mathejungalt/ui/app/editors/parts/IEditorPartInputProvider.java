package de.egladil.mathejungalt.ui.app.editors.parts;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;

public interface IEditorPartInputProvider {

	/**
	 * Gibt das gewählte oder mit diesem Part anderweitig verkn�pfte Domain-Objekt zur�ck das zum Editor geh�rt.
	 * 
	 * @return AbstractMatheAGObject
	 */
	public abstract AbstractMatheAGObject getSelectedInput();

}