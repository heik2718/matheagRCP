/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import java.util.List;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.conversion.IConverter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.events.IHyperlinkListener;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Hyperlink;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.IMatheAGObjectNames;
import de.egladil.mathejungalt.domain.schulen.IMinikaenguruTeilnahmeNames;
import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.domain.schulen.MinikaenguruTeilnahme;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.dialogs.MailinglisteDialog;
import de.egladil.mathejungalt.ui.app.editors.listeners.HyperlinkListenerAdapter;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;
import de.egladil.mathejungalt.ui.app.themes.PartControlsFactory;

/**
 * @author winkelv
 */
public class MiniteilnahmeSimpleAttributePartDataBindingProvider extends AbstractSimpleAttributePartDataBindingProvider {

	/** */
	private IHyperlinkListener hyperlinkListener;

	private static final int HYPERLINK_INDEX = 2;

	/** */
	private Control[] labels;

	/**
	 * 
	 */
	public MiniteilnahmeSimpleAttributePartDataBindingProvider() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.contentproviders.AbstractAttributePartContentsProvider#anzahlZeilen()
	 */
	@Override
	protected int anzahlZeilen() {
		return 3;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.contentproviders.AbstractAttributePartContentsProvider#initLabelsAndTexts(org.eclipse
	 * .swt.widgets .Composite, org.eclipse.ui.forms.widgets.FormToolkit, boolean)
	 */
	@Override
	protected void initControls(Composite pClient, FormToolkit pToolkit, boolean pEditable, AbstractMatheAGObject pInput) {
		Control[] controls = getControls();
		labels = new Control[controls.length];

		int widthHint = 150;
		labels[0] = PartControlsFactory.createIdLabel(pClient, pToolkit);
		controls[0] = PartControlsFactory.createIdText(pClient, pToolkit, widthHint);

		labels[1] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_RUECKMELDUNG);
		controls[1] = PartControlsFactory.createText(pClient, pToolkit, widthHint, pEditable);

		labels[2] = pToolkit.createHyperlink(pClient, ITextConstants.LABEL_TEXT_AKTIVE_KONTAKTE, SWT.NULL);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.contentproviders.IAttributePartContentsProvider#refreshDataBindings(de.egladil.mathe
	 * .core.domain .AbstractMatheAGObject)
	 */
	@Override
	public void refreshDataBindings(AbstractMatheAGObject pInput) {
		super.refreshDataBindings(pInput);

		Binding[] bindings = getBindings();
		Control[] controls = getControls();

		IObservableValue observableWidget = SWTObservables.observeText(controls[0], SWT.Modify);
		IObservableValue observableProperty = BeansObservables.observeValue(pInput, IMatheAGObjectNames.PROP_ID);
		bindings[0] = getDbc().bindValue(observableWidget, observableProperty, null, null);

		observableWidget = SWTObservables.observeText(controls[1], SWT.Modify);
		observableProperty = BeansObservables.observeValue(pInput, IMinikaenguruTeilnahmeNames.PROP_RUECKMELDUNG);
		UpdateValueStrategy widgetToModelStrategy = new UpdateValueStrategy();
		widgetToModelStrategy.setConverter(new IConverter() {

			@Override
			public Object getToType() {
				return Boolean.class;
			}

			@Override
			public Object getFromType() {
				return String.class;
			}

			@Override
			public Object convert(Object pArg0) {
				if (pArg0 == null) {
					return false;
				}
				return "j".equalsIgnoreCase((String) pArg0);
			}

		});

		UpdateValueStrategy modelToWidgetStrategy = new UpdateValueStrategy();
		modelToWidgetStrategy.setConverter(new IConverter() {
			@Override
			public Object getToType() {
				return String.class;
			}

			@Override
			public Object getFromType() {
				return Boolean.class;
			}

			@Override
			public Object convert(Object pArg0) {
				if ((Boolean) pArg0) {
					return "j";
				}
				return "n";
			}

		});

		bindings[1] = getDbc().bindValue(observableWidget, observableProperty, widgetToModelStrategy,
			modelToWidgetStrategy);
		refreshHyperlinkListener(pInput);
	}

	/**
	 * Erzeugt einen neuen HyperlinkListerner für den Aufgabe-Hyperlink
	 * 
	 * @param pInput
	 * @return
	 */
	private void refreshHyperlinkListener(AbstractMatheAGObject pInput) {
		if (hyperlinkListener != null) {
			((Hyperlink) labels[HYPERLINK_INDEX]).removeHyperlinkListener(hyperlinkListener);
		}
		final MinikaenguruTeilnahme teilnahme = (MinikaenguruTeilnahme) pInput;
		hyperlinkListener = new HyperlinkListenerAdapter() {

			@Override
			public void linkActivated(HyperlinkEvent pE) {
				String shellTitle = "Mailingliste für Minikänguruteilnahme " + teilnahme.getMinikaenguru().getJahr();
				List<Kontakt> zugeordneteKontakte = MatheJungAltActivator.getDefault().getStammdatenservice()
					.getKontakteMinikaenguru(teilnahme);
				MailinglisteDialog dlg = new MailinglisteDialog(Display.getCurrent().getActiveShell(), shellTitle,
					zugeordneteKontakte);
				dlg.open();
			}
		};
		((Hyperlink) labels[HYPERLINK_INDEX]).addHyperlinkListener(hyperlinkListener);
	}

}
