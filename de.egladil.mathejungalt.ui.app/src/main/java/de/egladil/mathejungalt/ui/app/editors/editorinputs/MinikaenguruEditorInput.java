/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.editorinputs;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;
import de.egladil.mathejungalt.ui.app.editors.kopierer.IKopierer;
import de.egladil.mathejungalt.ui.app.editors.kopierer.MinikaenguruKopierer;

/**
 * @author winkelv
 */
public class MinikaenguruEditorInput extends AbstractAufgabensammlungEditorInput {

	/**
	 * @param pObject
	 */
	public MinikaenguruEditorInput(AbstractMatheAGObject pObject) {
		super(pObject);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.editorinputs.AbstractMatheEditorInput#getKopierer()
	 */
	@Override
	public IKopierer getKopierer() {
		return new MinikaenguruKopierer();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.editorinputs.AbstractAufgabensammlungEditorInput#getCacheTarget()
	 */
	@Override
	public AbstractMatheAGObject getCacheTarget() {
		return new Minikaenguru();
	}
}
