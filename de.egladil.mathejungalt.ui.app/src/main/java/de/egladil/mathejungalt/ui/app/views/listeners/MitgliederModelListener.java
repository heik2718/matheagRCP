/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.listeners;

import java.beans.PropertyChangeEvent;

import org.eclipse.jface.viewers.TableViewer;

import de.egladil.mathejungalt.domain.mitglieder.IMitgliedNames;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.service.stammdaten.event.ModelChangeEvent;

/**
 * @author aheike
 */
public class MitgliederModelListener extends AbstractModelListener {

	/**
	 * @param pViewer
	 */
	public MitgliederModelListener(TableViewer pViewer) {
		super(pViewer);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.views.listeners.AbstractModelListener#isInterestingEvent(de.egladil.mathejungalt
	 * .service.stammdaten.event.ModelChangeEvent)
	 */
	@Override
	protected boolean isInterestingEvent(ModelChangeEvent pEvt) {
		if (ModelChangeEvent.ADD_EVENT.equals(pEvt.getEventType()) && pEvt.getNewValue() instanceof Mitglied) {
			return true;
		}
		if (ModelChangeEvent.REMOVE_EVENT.equals(pEvt.getEventType()) && pEvt.getOldValue() instanceof Mitglied) {
			return true;
		}
		if (ModelChangeEvent.PROPERTY_CHANGE_EVENT.equals(pEvt.getEventType())
			&& !(pEvt.getSource() instanceof Mitglied)) {
			return false;
		}
		final String property = pEvt.getProperty();
		return isRelevant(property);
	}

	/**
	 * @param pPropertyName
	 * @return boolean
	 */
	private boolean isRelevant(final String pPropertyName) {
		return IMitgliedNames.PROP_VORNAME.equals(pPropertyName) || IMitgliedNames.PROP_NACHNAME.equals(pPropertyName)
			|| IMitgliedNames.PROP_ALTER.equals(pPropertyName) || IMitgliedNames.PROP_KLASSE.equals(pPropertyName)
			|| IMitgliedNames.PROP_KONTAKT.equals(pPropertyName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.ui.app.views.listeners.AbstractModelListener#isInterestingEvent(java.beans.
	 * PropertyChangeEvent)
	 */
	@Override
	protected boolean isInterestingEvent(PropertyChangeEvent pEvt) {
		if (!(pEvt.getSource() instanceof Mitglied)) {
			return false;
		}
		return isRelevant(pEvt.getPropertyName());
	}
}
