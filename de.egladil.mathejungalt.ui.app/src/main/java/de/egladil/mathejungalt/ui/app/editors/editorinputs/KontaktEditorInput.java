/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.editorinputs;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.ui.app.editors.kopierer.IKopierer;
import de.egladil.mathejungalt.ui.app.editors.kopierer.KontaktKopierer;

/**
 * @author Winkelv
 */
public class KontaktEditorInput extends AbstractMatheEditorInput {

	/**
	 * @param pObject
	 */
	public KontaktEditorInput(AbstractMatheAGObject pObject) {
		super(pObject);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.editorinputs.AbstractMatheEditorInput#getKopierer()
	 */
	@Override
	public IKopierer getKopierer() {
		return new KontaktKopierer();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.editorinputs.AbstractMatheEditorInput#initCache()
	 */
	@Override
	public void initCache() {
		setCache(new Kontakt());
		applyChanges();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Object getAdapter(Class pAdapter) {
		if (pAdapter.getName().equals(Kontakt.class.getName())) {
			return getDomainObject();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object pObj) {
		if (pObj == null) {
			return false;
		}
		if (!(pObj instanceof KontaktEditorInput)) {
			return false;
		}
		KontaktEditorInput param = (KontaktEditorInput) pObj;
		return getAdapter(Kontakt.class).equals(param.getAdapter(Kontakt.class));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.editorinputs.AbstractMatheEditorInput#getInputObjectType()
	 */
	@Override
	protected String getInputObjectType() {
		return Kontakt.class.getName();
	}
}
