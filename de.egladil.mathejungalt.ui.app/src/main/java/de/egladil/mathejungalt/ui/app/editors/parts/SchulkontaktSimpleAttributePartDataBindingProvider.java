/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.events.IHyperlinkListener;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Hyperlink;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.IMatheAGObjectNames;
import de.egladil.mathejungalt.domain.schulen.ISchuleNames;
import de.egladil.mathejungalt.domain.schulen.Schulkontakt;
import de.egladil.mathejungalt.ui.app.editors.SchuleEditor;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.SchuleEditorInput;
import de.egladil.mathejungalt.ui.app.editors.listeners.HyperlinkListenerAdapter;
import de.egladil.mathejungalt.ui.app.handlers.EditObjectHandlerDelegate;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;
import de.egladil.mathejungalt.ui.app.themes.PartControlsFactory;

/**
 * @author winkelv
 */
public class SchulkontaktSimpleAttributePartDataBindingProvider extends AbstractSimpleAttributePartDataBindingProvider {

	/** */
	private IHyperlinkListener hyperlinkListener;

	private static final int HYPERLINK_INDEX = 1;

	/** */
	private Control[] labels;

	/**
	 * 
	 */
	public SchulkontaktSimpleAttributePartDataBindingProvider() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.contentproviders.AbstractAttributePartContentsProvider#anzahlZeilen()
	 */
	@Override
	protected int anzahlZeilen() {
		return 2;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.contentproviders.AbstractAttributePartContentsProvider#initLabelsAndTexts(org.eclipse
	 * .swt.widgets .Composite, org.eclipse.ui.forms.widgets.FormToolkit, boolean)
	 */
	@Override
	protected void initControls(Composite pClient, FormToolkit pToolkit, boolean pEditable, AbstractMatheAGObject pInput) {
		Control[] controls = getControls();
		labels = new Control[controls.length];

		int widthHint = 300;
		labels[0] = PartControlsFactory.createIdLabel(pClient, pToolkit);
		controls[0] = PartControlsFactory.createIdText(pClient, pToolkit, widthHint);

		labels[1] = pToolkit.createHyperlink(pClient, ITextConstants.LABEL_TEXT_SCHULE, SWT.NULL);
		controls[1] = PartControlsFactory.createText(pClient, pToolkit, widthHint, false);
		((Text) controls[1]).setTextLimit(ISchuleNames.LENGTH_SCHLUESSEL);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.contentproviders.IAttributePartContentsProvider#refreshDataBindings(de.egladil.mathe
	 * .core.domain .AbstractMatheAGObject)
	 */
	@Override
	public void refreshDataBindings(AbstractMatheAGObject pInput) {
		super.refreshDataBindings(pInput);

		Binding[] bindings = getBindings();
		Control[] controls = getControls();

		IObservableValue observableWidget = SWTObservables.observeText(controls[0], SWT.Modify);
		IObservableValue observableProperty = BeansObservables.observeValue(pInput, IMatheAGObjectNames.PROP_ID);
		bindings[0] = getDbc().bindValue(observableWidget, observableProperty, null, null);

		observableWidget = SWTObservables.observeText(controls[1], SWT.Modify);
		observableProperty = BeansObservables.observeValue(((Schulkontakt) pInput).getSchule(),
			IMatheAGObjectNames.PROP_SCHLUESSEL);
		bindings[1] = getDbc().bindValue(observableWidget, observableProperty, null, null);

		refreshHyperlinkListener(pInput);
	}

	/**
	 * Erzeugt einen neuen HyperlinkListerner für den Aufgabe-Hyperlink
	 * 
	 * @param pInput
	 * @return
	 */
	private void refreshHyperlinkListener(AbstractMatheAGObject pInput) {
		if (hyperlinkListener != null) {
			((Hyperlink) labels[HYPERLINK_INDEX]).removeHyperlinkListener(hyperlinkListener);
		}
		final Schulkontakt schulkontakt = (Schulkontakt) pInput;
		hyperlinkListener = new HyperlinkListenerAdapter() {

			private final EditObjectHandlerDelegate editObjectAction = new EditObjectHandlerDelegate(SchuleEditor.ID);

			@Override
			public void linkActivated(HyperlinkEvent pE) {
				SchuleEditorInput editorInput = new SchuleEditorInput(schulkontakt.getSchule());
				editObjectAction.executeCommand(editorInput);
			}
		};
		((Hyperlink) labels[HYPERLINK_INDEX]).addHyperlinkListener(hyperlinkListener);
	}
}
