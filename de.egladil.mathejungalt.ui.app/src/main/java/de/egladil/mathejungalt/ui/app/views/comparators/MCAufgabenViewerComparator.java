/**
 *
 */
package de.egladil.mathejungalt.ui.app.views.comparators;

import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;

/**
 * @author Winkelv
 */
public class MCAufgabenViewerComparator extends AbstractViewerComparator {

	/**
	 * @param pIndex
	 */
	public MCAufgabenViewerComparator(int pIndex) {
		super(pIndex);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Object pO1, Object pO2) {
		switch (getIndex()) {
		case 1: {
			return ((MCAufgabe) pO1).getStufe().getStufe() - ((MCAufgabe) pO2).getStufe().getStufe();
		}
		case 2: {
			return ((MCAufgabe) pO1).getPunkte() - ((MCAufgabe) pO2).getPunkte();
		}
		case 3: {
			return ((MCAufgabe) pO1).getThema().getKurzbezeichnung()
				.compareToIgnoreCase(((MCAufgabe) pO2).getThema().getKurzbezeichnung());
		}
		case 4: {
			return ((MCAufgabe) pO1).getTitel().compareToIgnoreCase(((MCAufgabe) pO2).getTitel());
		}
		default:
			return ((MCAufgabe) pO1).getSchluessel().compareToIgnoreCase(((MCAufgabe) pO2).getSchluessel());
		}
	}
}
