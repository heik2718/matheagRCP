/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.kopierer;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguruitem;

/**
 * @author winkelv
 */
public class MinikaenguruKopierer implements IKopierer {

	/**
	 * 
	 */
	public MinikaenguruKopierer() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.ui.app.editors.kopierer.IKopierer#copyAttributes(de.egladil.mathejungalt.domain.
	 * AbstractMatheAGObject, de.egladil.mathejungalt.domain.AbstractMatheAGObject)
	 */
	@Override
	public void copyAttributes(AbstractMatheAGObject pSource, AbstractMatheAGObject pTarget)
		throws MatheJungAltException {
		if (!(pSource instanceof Minikaenguru) && !(pTarget instanceof Minikaenguru)) {
			throw new MatheJungAltException(
				"Quelle oder Ziel sind keine Minikaenguru: Quelle = " + pSource == null ? "null" : pSource.getClass()
					.getName() + ", Ziel = " + pTarget == null ? "null" : pTarget.getClass().getName());
		}
		Minikaenguru source = (Minikaenguru) pSource;
		Minikaenguru target = (Minikaenguru) pTarget;
		target.setJahr(source.getJahr());
		target.setBeendet(source.getBeendet());
		target.clearItems();
		for (IAufgabensammlungItem item : source.getItems()) {
			Minikaenguruitem targetItem = new Minikaenguruitem();
			copyItemAttributes((Minikaenguruitem) item, targetItem);
			target.addItem(targetItem);
			targetItem.setMinikaenguru(target);
		}
	}

	/**
	 * @param pSource
	 * @param pTarget
	 */
	private void copyItemAttributes(Minikaenguruitem pSource, Minikaenguruitem pTarget) {
		pTarget.setAufgabe(pSource.getAufgabe());
		pTarget.setNummer(pSource.getNummer());
	}
}
