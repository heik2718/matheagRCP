/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.dnd;

import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.swt.dnd.DropTargetEvent;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;

/**
 * In den Aufgabensammlung-Editor kann eine Aufgabe gedropt werden. In diesem Fall wird ein neues Item erzeugt.
 *
 * @author winkelv
 */
public class MCRaetselitemsDropTargetListener extends StructuredViewerDropTargetAdapter {

	/**
	 * @param pTargetObject
	 */
	public MCRaetselitemsDropTargetListener(AbstractMatheAGObject pTargetObject) {
		super(pTargetObject);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.dnd.DropTargetAdapter#drop(org.eclipse.swt.dnd.DropTargetEvent)
	 */
	@Override
	public void drop(DropTargetEvent pEvent) {
		if (LocalSelectionTransfer.getTransfer().isSupportedType(pEvent.currentDataType)) {
			AbstractMatheAGObject domainObject = getDragedObject();
			if (domainObject == null) {
				return;
			}
			MCArchivraetsel raetsel = (MCArchivraetsel) getTargetObject();
			if (raetsel.isVeroeffentlicht()) {
				return;
			}
			if (domainObject instanceof MCAufgabe) {
				MCAufgabe aufgabe = (MCAufgabe) domainObject;
				if (raetsel.canAddAufgabe(aufgabe)) {
					MatheJungAltActivator.getDefault().getStammdatenservice()
						.quizaufgabeZuRaetselHinzufuegen(raetsel, aufgabe);
				}
			}
		}
	}
}
