/**
 * 
 */
package de.egladil.mathejungalt.ui.app.dialogs.labelproviders;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import de.egladil.mathejungalt.domain.medien.Buch;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.themes.IImageKeys;

/**
 * @author Winkelv
 */
public class BuchListLabelProvider extends LabelProvider {

	/**
	 * 
	 */
	public BuchListLabelProvider() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.LabelProvider#getImage(java.lang.Object)
	 */
	@Override
	public Image getImage(Object pElement) {
		return MatheJungAltActivator.getDefault().getImage(IImageKeys.BUCH);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
	 */
	@Override
	public String getText(Object pElement) {
		Buch medium = (Buch) pElement;
		return medium.getTitel();
	}
}
