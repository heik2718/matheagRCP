package de.egladil.mathejungalt.ui.app.views;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.handlers.IHandlerService;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.ui.app.editors.ArbeitsblattEditor;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.AbstractMatheEditorInput;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.ArbeitsblattEditorInput;
import de.egladil.mathejungalt.ui.app.handlers.ICommandIds;
import de.egladil.mathejungalt.ui.app.handlers.ToggleSperrendHandler;
import de.egladil.mathejungalt.ui.app.themes.LayoutFactory;
import de.egladil.mathejungalt.ui.app.themes.TableViewerFactory;
import de.egladil.mathejungalt.ui.app.views.comparators.AbstractViewerComparator;
import de.egladil.mathejungalt.ui.app.views.comparators.AlphabethicComparator;
import de.egladil.mathejungalt.ui.app.views.comparators.IViewerComparator;
import de.egladil.mathejungalt.ui.app.views.comparators.NumericComparator;
import de.egladil.mathejungalt.ui.app.views.contentproviders.AufgabensammlungViewContentProvider;
import de.egladil.mathejungalt.ui.app.views.listeners.AbstractModelListener;
import de.egladil.mathejungalt.ui.app.views.listeners.ArbeitsblaetterModelListener;

/**
 * @author aheike
 */
public class ArbeitsblaetterView extends AbstractAufgabensammlungView {

	/** */
	public static final String ID = "de.egladil.mathejungalt.ui.app.arbeitsblaetterview";

	/** */
	public static final int INDEX_SCHLUESSEL = 0;

	/** */
	public static final int INDEX_TITEL = 1;

	/** */
	public static final int INDEX_SPERREND = 2;

	/** */
	private final String[] columnTitles = new String[] { "Schl\u00FCssel", "Titel", "sperrend" };

	/** */
	private final int[] columnBounds = new int[] { 70, 150, 50 };

	/**
	 * 
	 */
	public ArbeitsblaetterView() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.views.AbstractMatheAGStructuredView#initTableViewer(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected TableViewer initTableViewer(Composite pParent) {
		ITableViewColumn[] viewColumns = new ITableViewColumn[columnTitles.length];
		IViewerComparator[] comparators = getColumnComparators();
		ColumnLabelProvider[] labelProviders = getColumnLableProviders();
		for (int i = 0; i < columnTitles.length; i++) {
			viewColumns[i] = new ViewColumn(columnTitles[i], columnBounds[i], comparators[i], labelProviders[i]);
		}
		return TableViewerFactory.createTableViewer(pParent, new AufgabensammlungViewContentProvider(
			getStammdatenservice()), viewColumns, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#getModelListener(org.eclipse.jface.viewers.TableViewer
	 * )
	 */
	@Override
	protected AbstractModelListener getModelListener(TableViewer pViewer) {
		return new ArbeitsblaetterModelListener(pViewer);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractAufgabensammlungView#createHandlers()
	 */
	@Override
	protected void createHandlers() {
		super.createHandlers();
		IHandlerService handlerService = (IHandlerService) getSite().getService(IHandlerService.class);
		handlerService.activateHandler(ICommandIds.SPERREND_SETZEN, new ToggleSperrendHandler(getViewer(),
			getStammdatenservice()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#getEditorID()
	 */
	@Override
	protected String getEditorID() {
		return ArbeitsblattEditor.ID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#getEditorInput(java.lang.Object)
	 */
	@Override
	protected AbstractMatheEditorInput getEditorInput(Object pObj) {
		if (!(pObj instanceof Arbeitsblatt)) {
			return null;
		}
		Arbeitsblatt arbeitsblatt = (Arbeitsblatt) pObj;
		if (!arbeitsblatt.isItemsLoaded()) {
			getStammdatenservice().aufgabensammlungitemsLaden(arbeitsblatt);
		}
		return new ArbeitsblattEditorInput((AbstractMatheAGObject) pObj);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractAufgabensammlungView#getColumnComparators()
	 */
	protected IViewerComparator[] getColumnComparators() {
		return new AbstractViewerComparator[] {
			new AlphabethicComparator(INDEX_SCHLUESSEL, Arbeitsblatt.class.getName()),
			new AlphabethicComparator(INDEX_TITEL, Arbeitsblatt.class.getName()),
			new NumericComparator(Arbeitsblatt.class.getName()) };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractAufgabensammlungView#getColumnLableProviders()
	 */
	protected ColumnLabelProvider[] getColumnLableProviders() {
		ColumnLabelProvider[] labelProviders = new ColumnLabelProvider[columnTitles.length];
		for (int i = 0; i < labelProviders.length; i++) {
			switch (i) {
			case INDEX_SCHLUESSEL:
				labelProviders[i] = new ColumnLabelProvider() {
					@Override
					public String getText(Object pElement) {
						return ((Arbeitsblatt) pElement).getSchluessel();
					}

					/*
					 * (non-Javadoc)
					 * 
					 * @see org.eclipse.jface.viewers.ColumnLabelProvider#getForeground(java.lang.Object)
					 */
					@Override
					public Color getForeground(Object pElement) {
						return LayoutFactory.getColorByBeendetStatus((IAufgabensammlung) pElement);
					}
				};
				break;
			case INDEX_TITEL:
				labelProviders[i] = new ColumnLabelProvider() {
					@Override
					public String getText(Object pElement) {
						return ((Arbeitsblatt) pElement).getTitel();
					}

					/*
					 * (non-Javadoc)
					 * 
					 * @see org.eclipse.jface.viewers.ColumnLabelProvider#getForeground(java.lang.Object)
					 */
					@Override
					public Color getForeground(Object pElement) {
						return LayoutFactory.getColorByBeendetStatus((IAufgabensammlung) pElement);
					}
				};
				break;
			default:
				labelProviders[i] = new ColumnLabelProvider() {
					@Override
					public String getText(Object pElement) {
						return ((Arbeitsblatt) pElement).getSperrend() + "";
					}

					/*
					 * (non-Javadoc)
					 * 
					 * @see org.eclipse.jface.viewers.ColumnLabelProvider#getForeground(java.lang.Object)
					 */
					@Override
					public Color getForeground(Object pElement) {
						return LayoutFactory.getColorByBeendetStatus((IAufgabensammlung) pElement);
					}
				};
				break;
			}
		}
		return labelProviders;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractAufgabensammlungView#getColumnTitles()
	 */
	protected String[] getColumnTitles() {
		return columnTitles;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractAufgabensammlungView#getColumnBounds()
	 */
	protected int[] getColumnBounds() {
		return columnBounds;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractAufgabensammlungView#getViewID()
	 */
	@Override
	protected String getViewID() {
		return ID;
	}
}
