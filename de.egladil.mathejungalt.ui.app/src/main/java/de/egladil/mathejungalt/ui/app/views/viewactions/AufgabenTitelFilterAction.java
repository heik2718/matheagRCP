/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.viewactions;

import org.eclipse.jface.viewers.StructuredViewer;

import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.views.filters.AbstractMatheAGObjectFilter;
import de.egladil.mathejungalt.ui.app.views.filters.AufgabenTitelFilter;

/**
 * @author Winkelv
 */
public class AufgabenTitelFilterAction extends AbstractTitelFilterAction {

	/**
	 * 
	 */
	public AufgabenTitelFilterAction(final StructuredViewer pViewer, final String pText,
		IStammdatenservice pStammdatenservice) {
		super(pViewer, pText, pStammdatenservice);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.views.actions.AbstractTitelFilterAction#getFilter()
	 */
	@Override
	protected AbstractMatheAGObjectFilter getFilter() {
		return new AufgabenTitelFilter(getViewer(), getStammdatenservice());
	}
}
