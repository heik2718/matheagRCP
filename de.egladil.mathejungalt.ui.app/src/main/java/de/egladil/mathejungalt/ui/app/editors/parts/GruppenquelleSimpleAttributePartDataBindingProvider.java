/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.forms.widgets.FormToolkit;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.quellen.IGruppenquelleNames;
import de.egladil.mathejungalt.domain.quellen.MitgliedergruppenQuelle;
import de.egladil.mathejungalt.ui.app.editors.converters.CollectionToStringConverter;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;
import de.egladil.mathejungalt.ui.app.themes.PartControlsFactory;

/**
 * Der Teil, der die speziellen Attribute der Kindquelle anzeigt.
 * 
 * @author winkelv
 */
public class GruppenquelleSimpleAttributePartDataBindingProvider extends AbstractSimpleAttributePartDataBindingProvider {

	/**
	 * 
	 */
	public GruppenquelleSimpleAttributePartDataBindingProvider() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractSimpleAttributePartDataBindingProvider#anzahlZeilen()
	 */
	@Override
	protected int anzahlZeilen() {
		return 1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.parts.AbstractSimpleAttributePartDataBindingProvider#initControls(org.eclipse.swt
	 * .widgets.Composite , org.eclipse.ui.forms.widgets.FormToolkit, boolean,
	 * de.egladil.mathe.core.domain.AbstractMatheAGObject)
	 */
	@Override
	protected void initControls(Composite pClient, FormToolkit pToolkit, boolean pEditable, AbstractMatheAGObject pInput) {
		final Control[] controls = getControls();
		Control[] labels = new Control[controls.length];

		int widthHint = 250;

		labels[0] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_ANZAHL);
		controls[0] = PartControlsFactory.createText(pClient, pToolkit, widthHint, false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.parts.AbstractSimpleAttributePartDataBindingProvider#refreshDataBindings(de.egladil
	 * .mathe.core .domain.AbstractMatheAGObject)
	 */
	@Override
	public void refreshDataBindings(AbstractMatheAGObject pInput) {
		super.refreshDataBindings(pInput);

		MitgliedergruppenQuelle quelle = (MitgliedergruppenQuelle) ((Aufgabe) pInput).getQuelle();

		Binding[] bindings = getBindings();
		Control[] controls = getControls();

		IObservableValue observableWidget = SWTObservables.observeText(controls[0], SWT.Modify);
		IObservableValue observableProperty = BeansObservables.observeValue(quelle, IGruppenquelleNames.PROP_ITEMS);
		UpdateValueStrategy modelToTargetStrategy = new UpdateValueStrategy();
		modelToTargetStrategy.setConverter(new CollectionToStringConverter());
		bindings[0] = getDbc().bindValue(observableWidget, observableProperty, null, modelToTargetStrategy);
	}
}
