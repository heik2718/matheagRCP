/**
 *
 */
package de.egladil.mathejungalt.ui.app.themes;

/**
 * @author aheike
 */
public interface IImageKeys {

	public static final String AUTOR = "icons/objects/autor.gif";

	public static final String AUFGABE = "icons/objects/aufgabe.gif";

	public static final String SERIE = "icons/objects/serie.gif";

	public static final String RAETSEL = "icons/objects/raetsel.png";

	public static final String SCHULE = "icons/objects/schule.png";

	public static final String KONTAKT = "icons/objects/kontakt.png";

	public static final String MINIKAENGURU = "icons/objects/minikaenguru.gif";

	public static final String SERIEN = "icons/views/serien.gif";

	public static final String LISTITEM = "icons/views/element.png";

	public static final String EXIT = "icons/actions/exit.gif";

	public static final String DIPLOM_GEBEN = "icons/actions/diplom_geben.gif";

	public static final String BUCH = "icons/objects/buch.gif";

	public static final String ZEITSCHRIFT = "icons/objects/zeitschrift.png";

	public static final String QUELLE_16 = "icons/objects/quelle_16.gif";

	public static final String MITGLIED = "icons/objects/mitglied.gif";

	public static final String SORT_ALPHABETHICALLY = "icons/filter_and_sort/alphabetisch.gif";

	public static final String EDIT = "icons/actions/edit.gif";

	public static final String REMOVE = "icons/actions/delete.gif";

	public static final String NEW = "icons/actions/neu.gif";

	public static final String CHECK_GENERATABLE = "icons/actions/check_generatable.gif";

	public static final String GENERATE = "icons/actions/generate_latex.gif";

	public static final String BEENDEN = "icons/actions/beenden.gif";

	public static final String QUELLE_WIZARD = "icons/objects/quelle_32.png";

	public static final String MITGLIED_AKTIV = "icons/icons.filter_and_sort/hide_eigenbau.gif";

	public static final String MITGLIED_NICHT_AKTIV = "icons/icons.filter_and_sort/hide_nachbau.gif";
}
