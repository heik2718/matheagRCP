/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.filters;

import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.jface.viewers.Viewer;

import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * @author Winkelv
 */
public class AufgabenTitelFilter extends AbstractMatheAGObjectFilter {

	/**
	 * @param pViewer
	 */
	public AufgabenTitelFilter(StructuredViewer pViewer, IStammdatenservice pStammdatenservice) {
		super(pViewer, pStammdatenservice);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public boolean select(Viewer pViewer, Object pParentElement, Object pElement) {
		return getStringMatcher().match(((Aufgabe) pElement).getTitel());
	}
}
