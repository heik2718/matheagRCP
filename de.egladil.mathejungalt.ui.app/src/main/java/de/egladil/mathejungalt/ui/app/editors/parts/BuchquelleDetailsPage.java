/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * @author winkelv
 */
public class BuchquelleDetailsPage extends AbstractQuelleDetailsPage {

	/**
	 * @param pMaster
	 */
	public BuchquelleDetailsPage(AbstractMasterDetailsBlock pMaster) {
		super(pMaster);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractDetailsPage#getAttributesPart()
	 */
	@Override
	protected MatheAGObjectAttributesPart getAttributesPart() {
		BuchquelleSimpleAttributePartDataBindingProvider contentsProvider = new BuchquelleSimpleAttributePartDataBindingProvider();
		return new MatheAGObjectAttributesPart(false, contentsProvider);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractDetailsPage#getHeader()
	 */
	@Override
	protected String getHeader() {
		return ITextConstants.SECTION_HEADER_BUCHQUELLE_ATTRIBUTES;
	}
}
