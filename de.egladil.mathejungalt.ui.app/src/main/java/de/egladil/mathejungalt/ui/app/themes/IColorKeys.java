/**
 *
 */
package de.egladil.mathejungalt.ui.app.themes;

import org.eclipse.swt.graphics.RGB;

/**
 * @author Winkelv
 */
public interface IColorKeys {

	/** */
	public static final RGB RGB_ORANGE = new RGB(255, 128, 0);

	/** ein sattes Sonnengelb */
	public static final RGB RGB_YELLOW = new RGB(252, 165, 3);

	/** */
	public static final RGB RGB_BLUE = new RGB(0, 0, 128);

	/** */
	public static final RGB RGB_RED = new RGB(255, 0, 0);

	/** */
	public static final RGB RGB_GREEN = new RGB(0, 128, 0);

	/** */
	public static final RGB RGB_LIGHT_GREY = new RGB(230, 230, 230);

	/** */
	public static final RGB RGB_GREY = new RGB(230, 230, 230);

	/** */
	public static final RGB RGB_WHITE = new RGB(255, 255, 255);

	/** */
	public static final RGB RGB_BLACK = new RGB(0, 0, 0);

	/** */
	public static final String LIGHT_GREY = "de.egladil.mathe.rcp.light_grey";

	/** */
	public static final String ORANGE = "de.egladil.mathe.rcp.orange"; //$NON-NLS-1$

	/** */
	public static final String YELLOW = "de.egladil.mathe.rcp.yellow"; //$NON-NLS-1$

	/** */
	public static final String GREEN = "de.egladil.mathe.rcp.green"; //$NON-NLS-1$

	/** */
	public static final String BLUE = "de.egladil.mathe.rcp.blue"; //$NON-NLS-1$

	/** */
	public static final String WHITE = "de.egladil.mathe.rcp.white"; //$NON-NLS-1$

	/** */
	public static final String RED = "de.egladil.mathe.rcp.red"; //$NON-NLS-1$

	/** */
	public static final String BLACK = "de.egladil.mathe.rcp.black"; //$NON-NLS-1$
}
