/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.viewactions;

import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.viewers.StructuredViewer;

import de.egladil.mathejungalt.ui.app.views.filters.AbstractMatheAGObjectFilter;
import de.egladil.mathejungalt.ui.app.views.filters.AufgabenzweckNegativeFilter;

/**
 * @author winkelv
 */
public abstract class AbstractAusblendenAufgabenzweckAction extends AbstractAusblendenAufgabenAction {

	/**
	 * 
	 */
	public AbstractAusblendenAufgabenzweckAction() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.rcp.views.actions.AusblendenAufgabenAction#getFilter(de.egladil.mathe.rcp.views.
	 * AbstractMatheAGStructuredView )
	 */
	@Override
	protected AbstractMatheAGObjectFilter getFilter(StructuredViewer pViewer) {
		Assert.isNotNull(pViewer, "getFilter(pViews)");
		return new AufgabenzweckNegativeFilter(pViewer, getStammdatenservice());
	}
}
