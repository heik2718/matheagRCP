package de.egladil.mathejungalt.ui.app;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

import de.egladil.mathejungalt.ui.app.views.AufgabenView;
import de.egladil.mathejungalt.ui.app.views.MitgliederView;
import de.egladil.mathejungalt.ui.app.views.SerienView;

/**
 * @author aheike
 */
public class StammdatenPerspective implements IPerspectiveFactory {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IPerspectiveFactory#createInitialLayout(org.eclipse.ui.IPageLayout)
	 */
	public void createInitialLayout(IPageLayout layout) {
		String editorArea = layout.getEditorArea();
		layout.setEditorAreaVisible(true);
		layout.setFixed(false);
		layout.setEditorAreaVisible(true);
		IFolderLayout folder = layout.createFolder("matheagViewsFolder", IPageLayout.LEFT, 0.3f, editorArea);
		folder.addView(AufgabenView.ID);
		folder.addView(MitgliederView.ID);
		folder.addView(SerienView.ID);
	}
}
