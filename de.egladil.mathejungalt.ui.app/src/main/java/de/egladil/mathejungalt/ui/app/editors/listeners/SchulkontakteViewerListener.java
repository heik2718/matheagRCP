/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.listeners;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.domain.schulen.Schule;
import de.egladil.mathejungalt.domain.schulen.Schulkontakt;
import de.egladil.mathejungalt.service.stammdaten.event.ModelChangeEvent;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.adapters.IDetailsObject;
import de.egladil.mathejungalt.ui.app.editors.parts.AbstractViewerPartWithButtons;
import de.egladil.mathejungalt.ui.app.views.labelproviders.SchuleLabelProvider;

/**
 * @author Winkelv
 */
public class SchulkontakteViewerListener extends AbstractViewerPartWithButtonsViewerListener {

	/**
	 * @param pView
	 */
	public SchulkontakteViewerListener(AbstractViewerPartWithButtons pView) {
		super(pView);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.listeners.IStructuredViewerPartViewerListener#handleRemove()
	 */
	@Override
	public void handleRemove() {
		if (getEditorInput() == null || getSelectedDetails() == null || getSelectedDetails().size() != 1)
			return;
		Kontakt kontakt = (Kontakt) getEditorInput();
		Schulkontakt schulkontakt = (Schulkontakt) getSelectedDetails().get(0);
		getStammdatenservice().schulkontaktLoschen(kontakt, schulkontakt.getSchule());
		if (getView() != null) {
			getView().getViewer().setSelection(null);
			getView().updateButtonAndMenuStatus();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.editors.listeners.AbstractViewerPartWithButtonsViewerListener#isInterestingEvent
	 * (de.egladil. mathejungalt.service.stammdaten.event.ModelChangeEvent)
	 */
	@Override
	protected boolean isInterestingEvent(ModelChangeEvent pEvt) {
		if (ModelChangeEvent.ADD_EVENT.equals(pEvt.getEventType())
			|| ModelChangeEvent.REMOVE_EVENT.equals(pEvt.getEventType())) {
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.editors.listeners.AbstractViewerPartWithButtonsViewerListener#specialAdd(java.
	 * util.List)
	 */
	@Override
	protected void specialAdd(List<AbstractMatheAGObject> selectedObjects) {
		Kontakt kontakt = (Kontakt) getEditorInput();
		Schulkontakt schulkontakt = null;
		if (selectedObjects != null) {
			Schule schule = (Schule) selectedObjects.get(0);
			schulkontakt = getStammdatenservice().kontaktZuSchuleHinzufuegen(kontakt, schule);
			Object detail = MatheJungAltActivator.getDefault().getAdapter(schulkontakt, IDetailsObject.class);
			if (detail == null) {
				throw new MatheJungAltException("Kein Adapter fuer " + schulkontakt.getClass().getName()
					+ " und IDetailsObject gefunden");
			}
			IDetailsObject detailsObject = (IDetailsObject) detail;
			updateDetailsList(detailsObject);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.listeners.AbstractViewerPartWithButtonsViewerListener#
	 * getObjectPickerComparator()
	 */
	@Override
	protected ViewerComparator getObjectPickerComparator() {
		return new ViewerComparator() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.jface.viewers.ViewerComparator#compare(org.eclipse.jface.viewers.Viewer,
			 * java.lang.Object, java.lang.Object)
			 */
			@Override
			public int compare(Viewer viewer, Object e1, Object e2) {
				Schule o1 = (Schule) e1;
				Schule o2 = (Schule) e2;
				return o1.getName().compareToIgnoreCase(o2.getName());
			}
		};
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.listeners.AbstractViewerPartWithButtonsViewerListener#getObjectPool()
	 */
	@Override
	public Collection<AbstractMatheAGObject> getObjectPool() {
		Collection<AbstractMatheAGObject> objectPool = new ArrayList<AbstractMatheAGObject>();
		Collection<Schule> schulen = getStammdatenservice().getSchulen();
		Kontakt kontakt = (Kontakt) getEditorInput();

		Collection<Schulkontakt> schulkontakte = getStammdatenservice().schulkontakteLaden(kontakt);

		List<Schule> kontaktSchulen = new ArrayList<Schule>();
		for (Schulkontakt sk : schulkontakte) {
			kontaktSchulen.add(sk.getSchule());
		}
		for (Schule s : schulen) {
			if (!kontaktSchulen.contains(s)) {
				objectPool.add(s);
			}
		}
		return objectPool;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.editors.listeners.AbstractViewerPartWithButtonsViewerListener#getLabelProvider()
	 */
	@Override
	public ColumnLabelProvider getLabelProvider() {
		return new SchuleLabelProvider();
	}
}
