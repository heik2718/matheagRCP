/**
 * 
 */
package de.egladil.mathejungalt.ui.app.handlers;

import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.swt.widgets.Display;

import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;
import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.dialogs.MailinglisteDialog;

/**
 * @author aheike
 */
public class MailinglisteMinikaenguruAnzeigenHandler extends AbstractSelectionHandler {

	/** */
	private IStammdatenservice stammdatenservice;

	/**
	 * @param pSelectionProvider
	 */
	public MailinglisteMinikaenguruAnzeigenHandler(ISelectionProvider pSelectionProvider,
		IStammdatenservice pStammdatenservice) {
		super(pSelectionProvider);
		stammdatenservice = pStammdatenservice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.handlers.AbstractSelectionHandler#run()
	 */
	@Override
	protected void run() {
		Minikaenguru object = (Minikaenguru) getFirstSelectedObject();
		if (!object.isMailinglisteLoaded()) {
			stammdatenservice.mailinglisteLaden(object);
		}
		List<Kontakt> mailingliste = object.getMailingliste();
		String shellTitle = "Mailingliste für Minikänguru " + object.getJahr();
		if (mailingliste.isEmpty()) {
			MessageDialog.openInformation(Display.getCurrent().getActiveShell(), shellTitle,
				"Die Mailingliste ist leer");
		} else {
			MailinglisteDialog dlg = new MailinglisteDialog(Display.getCurrent().getActiveShell(), shellTitle,
				mailingliste);
			dlg.open();
		}
	}
}
