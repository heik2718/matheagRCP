/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import java.util.Date;

import org.apache.commons.lang.time.DateFormatUtils;
import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Hyperlink;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.ISerieNames;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.ui.app.dialogs.DateSelectionDialog;
import de.egladil.mathejungalt.ui.app.editors.converters.DateToStringConverter;
import de.egladil.mathejungalt.ui.app.editors.converters.StringToDateConverter;
import de.egladil.mathejungalt.ui.app.editors.listeners.HyperlinkListenerAdapter;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;
import de.egladil.mathejungalt.ui.app.themes.PartControlsFactory;

/**
 * @author winkelv
 */
public class SerieSimpleAttributePartDataBindingProvider extends
	AbstractAufgabensammlungSimpleAttributePartDataBindingProvider {

	/**
	 *
	 */
	public SerieSimpleAttributePartDataBindingProvider() {
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathe.rcp.editors.contentproviders.AbstractAttributePartContentsProvider#anzahlZeilen()
	 */
	@Override
	protected int anzahlZeilen() {
		return 6;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathe.rcp.editors.parts.AbstractSimpleAttributePartControlsProvider#initControls(org.eclipse.swt.widgets
	 * .Composite , org.eclipse.ui.forms.widgets.FormToolkit, boolean)
	 */
	@Override
	protected void initControls(Composite pClient, FormToolkit pToolkit, boolean pEditable, AbstractMatheAGObject pInput) {
		super.initControls(pClient, pToolkit, pEditable, pInput);
		final Control[] controls = getControls();
		Control[] labels = new Control[controls.length];

		Serie serie = (Serie) pInput;
		boolean editable = serie.getBeendet() == 0;

		int widthHint = 150;
		labels[2] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_RUNDE);
		controls[2] = PartControlsFactory.createText(pClient, pToolkit, widthHint, editable);
		((Text) controls[2]).setTextLimit(ISerieNames.LENGTH_RUNDE);

		labels[3] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_SERIENNUMMER);
		controls[3] = PartControlsFactory.createText(pClient, pToolkit, widthHint, editable);
		((Text) controls[3]).setTextLimit(ISerieNames.LENGTH_NUMMER);

		if (editable) {
			labels[4] = pToolkit.createHyperlink(pClient, ITextConstants.LABEL_TEXT_DATUM, SWT.NULL);
			((Hyperlink) labels[4]).addHyperlinkListener(new HyperlinkListenerAdapter() {
				@Override
				public void linkActivated(HyperlinkEvent pE) {
					DateSelectionDialog dateDialog = new DateSelectionDialog(Display.getCurrent().getActiveShell(),
						null);
					if (dateDialog.open() == IDialogConstants.OK_ID) {
						Date cal = dateDialog.getDate();
						((Text) controls[4]).setText(DateFormatUtils
							.format(cal, ITextConstants.DATE_FORMAT_PATTERNS[0]));
					}
				}
			});
		} else {
			labels[4] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_DATUM);
		}

		controls[4] = PartControlsFactory.createText(pClient, pToolkit, widthHint, editable);

		labels[5] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_MONAT_JAHR);
		controls[5] = PartControlsFactory.createText(pClient, pToolkit, widthHint, editable);
		((Text) controls[5]).setTextLimit(ISerieNames.LENGTH_MONAT_JAHR_TEXT);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathe.rcp.editors.contentproviders.IAttributePartContentsProvider#refreshDataBindings(de.egladil.mathe
	 * .core.domain .AbstractMatheAGObject)
	 */
	@Override
	public void refreshDataBindings(AbstractMatheAGObject pInput) {
		super.refreshDataBindings(pInput);

		Binding[] bindings = getBindings();
		Control[] controls = getControls();

		IObservableValue observableWidget = SWTObservables.observeText(controls[2], SWT.Modify);
		IObservableValue observableProperty = BeansObservables.observeValue(pInput, ISerieNames.PROP_RUNDE);
		bindings[2] = getDbc().bindValue(observableWidget, observableProperty, null, null);

		observableWidget = SWTObservables.observeText(controls[3], SWT.Modify);
		observableProperty = BeansObservables.observeValue(pInput, ISerieNames.PROP_NUMMER);
		bindings[3] = getDbc().bindValue(observableWidget, observableProperty, null, null);

		observableWidget = SWTObservables.observeText(controls[4], SWT.Modify);
		observableProperty = BeansObservables.observeValue(pInput, ISerieNames.PROP_DATUM);

		UpdateValueStrategy targetToModelStrategy = new UpdateValueStrategy();
		targetToModelStrategy.setConverter(new StringToDateConverter());

		UpdateValueStrategy modelToTargetStrategy = new UpdateValueStrategy();
		modelToTargetStrategy.setConverter(new DateToStringConverter());

		bindings[4] = getDbc().bindValue(observableWidget, observableProperty, targetToModelStrategy,
			modelToTargetStrategy);

		observableWidget = SWTObservables.observeText(controls[5], SWT.Modify);
		observableProperty = BeansObservables.observeValue(pInput, ISerieNames.PROP_MONAT_JAHR_TEXT);
		bindings[5] = getDbc().bindValue(observableWidget, observableProperty, null, null);
	}
}
