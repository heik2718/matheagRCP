/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.kopierer;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.mcraetsel.MCAntwortbuchstaben;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;

/**
 * @author aheike
 */
public class MCAufgabeKopierer implements IKopierer {

	/**
	 *
	 */
	public MCAufgabeKopierer() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.ui.app.editors.kopierer.IKopierer#copyAttributes(de.egladil.mathejungalt.domain.
	 * AbstractMatheAGObject, de.egladil.mathejungalt.domain.AbstractMatheAGObject)
	 */
	@Override
	public void copyAttributes(AbstractMatheAGObject pSource, AbstractMatheAGObject pTarget)
		throws MatheJungAltException {
		if (!(pSource instanceof MCAufgabe) && !(pTarget instanceof MCAufgabe)) {
			throw new MatheJungAltException(
				"Quelle oder Ziel sind keine MCAufgabe: Quelle = " + pSource == null ? "null" : pSource.getClass()
					.getName() + ", Ziel = " + pTarget == null ? "null" : pTarget.getClass().getName());
		}
		MCAufgabe source = (MCAufgabe) pSource;
		MCAufgabe target = (MCAufgabe) pTarget;
		target.setArt(source.getArt());
		target.setBemerkung(source.getBemerkung());
		target.setGesperrtFuerRaetsel(source.getGesperrtFuerRaetsel());
		target.setId(source.getId());
		target.setQuelle(source.getQuelle());
		target.setSchluessel(source.getSchluessel());
		target.setStufe(source.getStufe());
		target.setThema(source.getThema());
		target.setTitel(source.getTitel());
		target.setAntwortA(source.getAntwortA());
		target.setAntwortB(source.getAntwortB());
		target.setAntwortC(source.getAntwortC());
		target.setAntwortD(source.getAntwortD());
		target.setAntwortE(source.getAntwortE());
		MCAntwortbuchstaben loesungsbuchstabe = source.getLoesungsbuchstabe();
		if (loesungsbuchstabe != null) {
			target.setLoesungsbuchstabe(source.getLoesungsbuchstabe());
		}
		target.setPunkte(source.getPunkte());
		target.setTabelleGenerieren(source.isTabelleGenerieren());
		target.setVerzeichnis(source.getVerzeichnis());
	}
}
