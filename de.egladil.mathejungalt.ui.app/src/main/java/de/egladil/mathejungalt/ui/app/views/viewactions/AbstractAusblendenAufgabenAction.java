/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.viewactions;

import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.ui.IViewActionDelegate;
import org.eclipse.ui.IViewPart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;
import de.egladil.mathejungalt.ui.app.views.AufgabenView;
import de.egladil.mathejungalt.ui.app.views.filters.AbstractMatheAGObjectFilter;

/**
 * @author winkelv
 */
public abstract class AbstractAusblendenAufgabenAction extends Action implements IViewActionDelegate {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(AbstractAusblendenAufgabenAction.class);

	/** */
	private AbstractMatheAGObjectFilter filter;

	/** */
	private boolean notPushed = true;

	/** */
	private IStammdatenservice stammdatenservice;

	/** */
	private AufgabenView view;

	/**
	 * @param pText
	 */
	public AbstractAusblendenAufgabenAction() {
		//
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IViewActionDelegate#init(org.eclipse.ui.IViewPart)
	 */
	@Override
	public void init(IViewPart pView) {
		Assert.isNotNull(pView, "pView ist null");
		Assert.isTrue(pView.getViewSite().getId().equals(AufgabenView.ID), "Action hängt im falschen View: "
			+ pView.getViewSite().getId() + " statt " + AufgabenView.ID);
		view = (AufgabenView) pView;
		stammdatenservice = view.getStammdatenservice();
		filter = getFilter(view.getViewer());
		if (LOG.isDebugEnabled()) {
			LOG.debug("Action " + this.getClass().getSimpleName() + " erzeugt");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	@Override
	public void run(IAction pAction) {
		if (notPushed) {
			Assert.isNotNull(getSpecialPattern(), "run(): das Pattern darf nicht null sein");
			Assert.isTrue(getSpecialPattern().length() > 0, "run(): das Pattern darf nicht leer sein");
			filter.setPattern(getSpecialPattern());
			notPushed = false;
		} else {
			filter.setPattern(ITextConstants.FILTER_ACTION_ALLE);
			notPushed = true;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IActionDelegate#selectionChanged(org.eclipse.jface.action.IAction,
	 * org.eclipse.jface.viewers.ISelection)
	 */
	@Override
	public void selectionChanged(IAction pAction, ISelection pSelection) {
		// ignore
	}

	/**
	 * @return the specialPattern
	 */
	protected abstract String getSpecialPattern();

	/**
	 * @return
	 */
	protected abstract AbstractMatheAGObjectFilter getFilter(StructuredViewer pView);

	/**
	 * @return the stammdatenservice
	 */
	protected IStammdatenservice getStammdatenservice() {
		return stammdatenservice;
	}
}