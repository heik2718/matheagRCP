/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import java.beans.PropertyChangeEvent;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.FormToolkit;

import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.editors.listeners.AufgabensammlungitemViewerListener;
import de.egladil.mathejungalt.ui.app.themes.IImageKeys;
import de.egladil.mathejungalt.ui.app.themes.TableViewerFactory;
import de.egladil.mathejungalt.ui.app.views.ITableViewColumn;
import de.egladil.mathejungalt.ui.app.views.ViewColumn;
import de.egladil.mathejungalt.ui.app.views.comparators.AbstractViewerComparator;
import de.egladil.mathejungalt.ui.app.views.comparators.AufgabensammlungItemViewerComparator;
import de.egladil.mathejungalt.ui.app.views.comparators.IViewerComparator;
import de.egladil.mathejungalt.ui.app.views.contentproviders.AufgabensammlungitemViewContentProvider;
import de.egladil.mathejungalt.ui.app.views.labelproviders.AufgabensammlungItemColumnLabelProvider;

/**
 * Der Teil des MasterDetailsBlocks, der die AufgabensammlungItems auflistet.
 *
 * @author Heike Winkelvoß
 */
public class AufgabensammlungitemsViewerPartWithButtons extends AbstractViewerPartWithButtons {

	/** */
	private static String[] columnTitles = new String[] { "Schl\u00FCssel", "Nummer", "Stufe" };

	/** */
	private static int[] columnBounds = new int[] { 70, 70, 50 };

	/**
	 * @param pParentPart
	 */
	public AufgabensammlungitemsViewerPartWithButtons(AbstractMasterDetailsBlockWithViewerPart pParentPart) {
		super(pParentPart);
		MatheJungAltActivator.getDefault().getStammdatenservice().addModelChangeListener(pParentPart);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractViewerPartWithButtons#create(org.eclipse.swt.widgets.Composite,
	 * org.eclipse.ui.forms.widgets.FormToolkit)
	 */
	@Override
	public void init(final Composite pParent, final FormToolkit pToolkit) {
		super.init(pParent, pToolkit);
		addStructuredViewerListener(new AufgabensammlungitemViewerListener(this));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.parts.AbstractViewerPartWithButtons#makeActions()
	 */
	@Override
	protected void makeActions() {
		super.makeActions();
		Action generierenAction = new Action() {
			public void run() {
				handleGenerate();
			}
		};
		generierenAction.setText("ps generieren");
		generierenAction.setToolTipText("die gew\u00E4hlten Objekte in PS verwandel.");
		generierenAction.setImageDescriptor(MatheJungAltActivator.getImageDescriptor(IImageKeys.GENERATE));
		addAction(generierenAction);
	}

	/**
	 * Generiert die ps-Dateien.
	 */
	private void handleGenerate() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractViewerPartWithButtons#createTableViewer()
	 */
	@Override
	protected TableViewer createTableViewer(Composite pContainer) {
		ITableViewColumn[] viewColumns = new ITableViewColumn[columnTitles.length];
		IViewerComparator[] comparators = getColumnComparators();
		ColumnLabelProvider[] labelProviders = getColumnLableProviders();
		for (int i = 0; i < columnTitles.length; i++) {
			viewColumns[i] = new ViewColumn(columnTitles[i], columnBounds[i], comparators[i], labelProviders[i]);
		}
		return TableViewerFactory.createTableViewer(pContainer, new AufgabensammlungitemViewContentProvider(
			MatheJungAltActivator.getDefault().getStammdatenservice()), viewColumns, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.rcp.editors.parts.AbstractViewerPartWithButtons#handlePropertyChangeEvent(java.beans.
	 * PropertyChangeEvent)
	 */
	@Override
	public void handlePropertyChangeEvent(PropertyChangeEvent pEvt) {
		String property = pEvt.getPropertyName();
		if (property.equalsIgnoreCase(IAufgabensammlung.PROP_BEENDET)) {
			if (((Integer) pEvt.getOldValue()).intValue() != ((Integer) pEvt.getNewValue()).intValue()) {
				updateButtonAndMenuStatus();
			}
		}
		super.handlePropertyChangeEvent(pEvt);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractViewerPartWithButtons#updateButtonAndMenuStatus()
	 */
	@Override
	public void updateButtonAndMenuStatus() {
		IAufgabensammlung aufgabensammlung = (IAufgabensammlung) getViewer().getInput();
		if (aufgabensammlung.getBeendet() == 0) {
			super.updateButtonAndMenuStatus();
			getActions().get(EDIT_INDEX).setEnabled(false);
		} else {
			for (Button button : getButtons()) {
				button.setEnabled(false);
			}
			for (Action action : getActions()) {
				action.setEnabled(false);
			}
		}
	}

	/**
	 * Erzeugt abhängig vom Spaltenindex einen {@link ColumnLabelProvider}
	 *
	 * @param pInd int der Spaltenindex
	 * @return {@link ColumnLabelProvider}
	 */
	private ColumnLabelProvider[] getColumnLableProviders() {
		return new AufgabensammlungItemColumnLabelProvider[] { new AufgabensammlungItemColumnLabelProvider(0),
			new AufgabensammlungItemColumnLabelProvider(1), new AufgabensammlungItemColumnLabelProvider(2) };
	}

	/**
	 * @return
	 */
	private AbstractViewerComparator[] getColumnComparators() {
		AbstractViewerComparator[] comparators = new AufgabensammlungItemViewerComparator[columnTitles.length];
		for (int i = 0; i < comparators.length; i++) {
			comparators[i] = new AufgabensammlungItemViewerComparator(i);
		}
		return comparators;
	}
}
