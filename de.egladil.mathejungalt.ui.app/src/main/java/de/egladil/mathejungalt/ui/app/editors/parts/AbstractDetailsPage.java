/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import java.beans.PropertyChangeListener;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.IDetailsPage;
import org.eclipse.ui.forms.IFormPart;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;
import de.egladil.mathejungalt.ui.app.themes.LayoutFactory;

/**
 * Abstrakte Oberklasse für Details-Parts in MasterDetailsBlocks in den Editoren
 * 
 * @author winkelv
 */
public abstract class AbstractDetailsPage implements IDetailsPage {

	/** */
	private AbstractMatheAGObject input;

	/** */
	private IManagedForm form;

	/** */
	private AbstractMasterDetailsBlock masterBlock;

	/** */
	private MatheAGObjectAttributesPart attributesPart;

	/**
	 * @param pMaster
	 */
	public AbstractDetailsPage(AbstractMasterDetailsBlock pMaster) {
		masterBlock = pMaster;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.IDetailsPage#createContents(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createContents(Composite pParent) {
		// erstmal das Parent-Layout setzen, damit der Teil auch angezeigt wird
		GridLayout layout = LayoutFactory.getGridLayout(1);
		pParent.setLayout(layout);

		FormToolkit toolkit = form.getToolkit();
		Section section = toolkit.createSection(pParent, Section.DESCRIPTION | Section.TITLE_BAR);
		section.setActiveToggleColor(toolkit.getHyperlinkGroup().getActiveForeground());
		toolkit.createCompositeSeparator(section);

		// Der Behälter für die Teile dieser Section
		Composite client = toolkit.createComposite(section, SWT.WRAP);

		// das Layout für den Behälter
		layout = LayoutFactory.getGridLayout(2);
		client.setLayout(layout);

		// Der Teil mit den Eingabefeldern- hier nicht editierbar
		attributesPart = getAttributesPart();
		input = ((IEditorPartInputProvider) masterBlock).getSelectedInput();
		attributesPart.initialize(client, toolkit, input);

		// Behälter zur section hinzuf�gen
		section.setText(getHeader());
		section.setDescription(getDescription());
		layout = LayoutFactory.getGridLayout(1);
		section.setLayout(layout);
		section.setClient(client);
		section.setExpanded(true);

		toolkit.paintBordersFor(client);
	}

	/**
	 * Eine Beschreibung für den AttributesPart. Diese wird oberhalb des Eingabeteils angezeigt.
	 * 
	 * @return
	 */
	protected String getDescription() {
		if (attributesPart.isEditable()) {
			return ITextConstants.SECTION_DESCR_EDITABLE_ATTRIBUTES;
		} else {
			return "";
		}
	}

	/**
	 * @return String die Seitenüberschrift.
	 */
	protected abstract String getHeader();

	/**
	 * @return {@link MatheAGObjectAttributesPart} der spezielle Teil für die einfachen Attribute
	 */
	protected abstract MatheAGObjectAttributesPart getAttributesPart();

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.IFormPart#commit(boolean)
	 */
	@Override
	public void commit(boolean pOnSave) {
		// wird nicht commited
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.IFormPart#dispose()
	 */
	@Override
	public void dispose() {
		attributesPart.dispose(input);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.IFormPart#initialize(org.eclipse.ui.forms.IManagedForm)
	 */
	@Override
	public void initialize(IManagedForm pForm) {
		form = pForm;
		// if (form.getContainer() instanceof AbstractMasterDetailsBlock){
		// form.setInput(((AbstractMasterDetailsBlock) form.getContainer()).getParentPage().getDomainObject());
		// }
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.IFormPart#isDirty()
	 */
	@Override
	public boolean isDirty() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.IFormPart#isStale()
	 */
	@Override
	public boolean isStale() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.IFormPart#refresh()
	 */
	@Override
	public void refresh() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.IFormPart#setFormInput(java.lang.Object)
	 */
	@Override
	public boolean setFormInput(Object pInput) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.IPartSelectionListener#selectionChanged(org.eclipse.ui.forms.IFormPart,
	 * org.eclipse.jface.viewers.ISelection)
	 */
	@Override
	public void selectionChanged(IFormPart pPart, ISelection pSelection) {
		attributesPart.clearDataBindings();
		if (input != null)
			input.removePropertyChangeListener((PropertyChangeListener) masterBlock);
		IStructuredSelection ssel = (IStructuredSelection) pSelection;
		if (ssel.size() == 1) {
			input = (AbstractMatheAGObject) ssel.getFirstElement();
			input.addPropertyChangeListener((PropertyChangeListener) masterBlock);
			input.addPropertyChangeListener((PropertyChangeListener) masterBlock.getParentPage().getEditor());
		} else
			input = null;
		attributesPart.refreshDataBindings(input);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.IFormPart#setFocus()
	 */
	@Override
	public void setFocus() {
		// leer
	}
}
