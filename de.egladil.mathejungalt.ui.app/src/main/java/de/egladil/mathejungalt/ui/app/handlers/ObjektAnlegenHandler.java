/**
 *
 */
package de.egladil.mathejungalt.ui.app.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;

import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.domain.schulen.Schule;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.editors.ArbeitsblattEditor;
import de.egladil.mathejungalt.ui.app.editors.AufgabeEditor;
import de.egladil.mathejungalt.ui.app.editors.KontaktEditor;
import de.egladil.mathejungalt.ui.app.editors.MCAufgabeEditor;
import de.egladil.mathejungalt.ui.app.editors.MCRaetselEditor;
import de.egladil.mathejungalt.ui.app.editors.MinikaenguruEditor;
import de.egladil.mathejungalt.ui.app.editors.MitgliedEditor;
import de.egladil.mathejungalt.ui.app.editors.SchuleEditor;
import de.egladil.mathejungalt.ui.app.editors.SerieEditor;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.ArbeitsblattEditorInput;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.AufgabeEditorInput;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.KontaktEditorInput;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.MCAufgabeEditorInput;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.MCRaetselEditorInput;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.MinikaenguruEditorInput;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.MitgliedEditorInput;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.SchuleEditorInput;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.SerieEditorInput;
import de.egladil.mathejungalt.ui.app.views.ArbeitsblaetterView;
import de.egladil.mathejungalt.ui.app.views.AufgabenView;
import de.egladil.mathejungalt.ui.app.views.KontakteView;
import de.egladil.mathejungalt.ui.app.views.MCAufgabenView;
import de.egladil.mathejungalt.ui.app.views.MCRaetselView;
import de.egladil.mathejungalt.ui.app.views.MinikaenguruView;
import de.egladil.mathejungalt.ui.app.views.MitgliederView;
import de.egladil.mathejungalt.ui.app.views.SchulenView;
import de.egladil.mathejungalt.ui.app.views.SerienView;

/**
 * @author aheike
 */
public class ObjektAnlegenHandler extends AbstractHandler {

	/** */
	private IStammdatenservice stammdatenservice;

	/** */
	private String viewId;

	/**
	 * @param pStammdatenservice
	 * @param pViewId
	 */
	public ObjektAnlegenHandler(IStammdatenservice pStammdatenservice, String pViewId) {
		super();
		stammdatenservice = pStammdatenservice;
		viewId = pViewId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent pArg0) throws ExecutionException {
		EditObjectHandlerDelegate editObjectHandlerDelegate = null;
		if (AufgabenView.ID.equals(viewId)) {
			Aufgabe aufgabe = stammdatenservice.aufgabeAnlegen();
			editObjectHandlerDelegate = new EditObjectHandlerDelegate(AufgabeEditor.ID);
			editObjectHandlerDelegate.executeCommand(new AufgabeEditorInput(aufgabe));
		}
		if (MCAufgabenView.ID.equals(viewId)) {
			MCAufgabe aufgabe = stammdatenservice.quizaufgabeAnlegen();
			editObjectHandlerDelegate = new EditObjectHandlerDelegate(MCAufgabeEditor.ID);
			editObjectHandlerDelegate.executeCommand(new MCAufgabeEditorInput(aufgabe));
		}
		if (MitgliederView.ID.equals(viewId)) {
			Mitglied mitglied = stammdatenservice.mitgliedAnlegen();
			editObjectHandlerDelegate = new EditObjectHandlerDelegate(MitgliedEditor.ID);
			editObjectHandlerDelegate.executeCommand(new MitgliedEditorInput(mitglied));
		}
		if (SerienView.ID.equals(viewId)) {
			Serie serie = (Serie) stammdatenservice.aufgabensammlungAnlegen(Serie.class);
			editObjectHandlerDelegate = new EditObjectHandlerDelegate(SerieEditor.ID);
			editObjectHandlerDelegate.executeCommand(new SerieEditorInput(serie));
		}
		if (MinikaenguruView.ID.equals(viewId)) {
			Minikaenguru minikaenguru = (Minikaenguru) stammdatenservice.aufgabensammlungAnlegen(Minikaenguru.class);
			editObjectHandlerDelegate = new EditObjectHandlerDelegate(MinikaenguruEditor.ID);
			editObjectHandlerDelegate.executeCommand(new MinikaenguruEditorInput(minikaenguru));
		}
		if (ArbeitsblaetterView.ID.equals(viewId)) {
			Arbeitsblatt arbeitsblatt = (Arbeitsblatt) stammdatenservice.aufgabensammlungAnlegen(Arbeitsblatt.class);
			editObjectHandlerDelegate = new EditObjectHandlerDelegate(ArbeitsblattEditor.ID);
			editObjectHandlerDelegate.executeCommand(new ArbeitsblattEditorInput(arbeitsblatt));
		}
		if (MCRaetselView.ID.equals(viewId)) {
			MCArchivraetsel raetsel = stammdatenservice.raetselAnlegen();
			editObjectHandlerDelegate = new EditObjectHandlerDelegate(MCRaetselEditor.ID);
			editObjectHandlerDelegate.executeCommand(new MCRaetselEditorInput(raetsel));
		}
		if (SchulenView.ID.equals(viewId)) {
			Schule schule = stammdatenservice.schuleAnlegen();
			editObjectHandlerDelegate = new EditObjectHandlerDelegate(SchuleEditor.ID);
			editObjectHandlerDelegate.executeCommand(new SchuleEditorInput(schule));
		}
		if (KontakteView.ID.equals(viewId)) {
			Kontakt kontakt = stammdatenservice.kontaktAnlegen();
			editObjectHandlerDelegate = new EditObjectHandlerDelegate(KontaktEditor.ID);
			editObjectHandlerDelegate.executeCommand(new KontaktEditorInput(kontakt));
		}
		return null;
	}

}
