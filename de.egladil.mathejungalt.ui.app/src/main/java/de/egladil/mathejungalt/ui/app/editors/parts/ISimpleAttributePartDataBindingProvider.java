/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.forms.widgets.FormToolkit;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;

/**
 * Managed das DataBinding für den Teil der einfachen Attribute (keine vom Collection-Typ)
 * 
 * @author winkelv
 */
public interface ISimpleAttributePartDataBindingProvider {

	/**
	 * @param pClient
	 * @param pToolkit
	 * @param pEditable
	 * @param pInput
	 */
	public void init(final Composite pClient, final FormToolkit pToolkit, boolean pEditable,
		AbstractMatheAGObject pInput);

	/**
	 * Aktualisiert das DataBinding.
	 * 
	 * @param pInput {@link AbstractMatheAGObject} das Objekt, mit zu welchem das Binding definiert wird
	 */
	public void refreshDataBindings(final AbstractMatheAGObject pInput);

	/**
	 * @return the texts
	 */
	public Control[] getControls();

	/**
	 * @return the bindings
	 */
	public Binding[] getBindings();

	/**
	 * @return the dbc
	 */
	public DataBindingContext getDbc();

	/**
	 * @param pInput ein {@link AbstractMatheAGObject}
	 */
	public void dispose(AbstractMatheAGObject pInput);
}
