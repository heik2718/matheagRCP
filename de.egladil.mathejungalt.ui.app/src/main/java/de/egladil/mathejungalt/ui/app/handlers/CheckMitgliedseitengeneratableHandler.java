/**
 * 
 */
package de.egladil.mathejungalt.ui.app.handlers;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import de.egladil.base.exceptions.MatheAGObjectNotGeneratableException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * Pr�ft Mitgliederseite auf Generierbarkeit.
 * 
 * @author aheike
 */
public class CheckMitgliedseitengeneratableHandler extends AbstractSelectionHandler {

	/** */
	private IStammdatenservice stammdatenservice;

	/**
	 * @param pSelectionProvider
	 */
	public CheckMitgliedseitengeneratableHandler(ISelectionProvider pSelectionProvider,
		IStammdatenservice pStammdatenservice) {
		super(pSelectionProvider);
		stammdatenservice = pStammdatenservice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.handlers.AbstractSelectionHandler#run()
	 */
	@Override
	protected void run() {
		AbstractMatheAGObject object = getFirstSelectedObject();
		if (!(object instanceof Mitglied)) {
			return;
		}
		final Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		try {
			stammdatenservice.canGenerate(object);
			MessageDialog.openInformation(shell, "Info", "Die Mitgliederseiten k\u00F6nnen generiert werden.");
		} catch (MatheAGObjectNotGeneratableException e) {
			MessageDialog.openInformation(shell, "Info", e.getMessage());
		} catch (Exception e) {
			MessageDialog.openError(shell, "Fehler", "Unerwarteter Fehler\n" + e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.handlers.AbstractSelectionHandler#isEnabled()
	 */
	@Override
	public boolean isEnabled() {
		if (!super.isEnabled()) {
			return false;
		}
		return getFirstSelectedObject() instanceof Mitglied;
	}

}
