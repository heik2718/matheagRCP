/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.labelproviders;

import org.eclipse.jface.viewers.ColumnLabelProvider;

/**
 * @author aheike
 */
public class CommonColumnLabelProvider extends ColumnLabelProvider {

	/** */
	private int index;

	/**
	 * 
	 */
	public CommonColumnLabelProvider() {
	}

	/**
	 * @param pIndex
	 */
	public CommonColumnLabelProvider(int pIndex) {
		super();
		index = pIndex;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.CellLabelProvider#getToolTipDisplayDelayTime(java.lang.Object)
	 */
	@Override
	public int getToolTipDisplayDelayTime(Object pObject) {
		return 500;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.CellLabelProvider#getToolTipTimeDisplayed(java.lang.Object)
	 */
	@Override
	public int getToolTipTimeDisplayed(Object pObject) {
		return 3000;
	}

	/**
	 * @return the index
	 */
	protected int getIndex() {
		return index;
	}
}
