/**
 *
 */
package de.egladil.mathejungalt.ui.app.handlers;

import java.io.File;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IEditorRegistry;
import org.eclipse.ui.IPathEditorInput;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.editors.AufgabeLaTeXEditor;
import de.egladil.mathejungalt.ui.app.editors.documentproviders.PathEditorInput;
import de.egladil.mathejungalt.ui.app.preferences.PreferenceConstants;

/**
 * @author Winkelv
 */
public class OpenExternalTextFileHandlerDelegate {

	/**
	 *
	 */
	public OpenExternalTextFileHandlerDelegate() {

	}

	/**
	 * Führt das Kommando aus.
	 *
	 * @param pObject
	 */
	public void executeCommand(Object pObject) {
		Assert.isNotNull(pObject);
		Assert.isTrue(pObject instanceof Aufgabe || pObject instanceof MCAufgabe);
		if (pObject instanceof Aufgabe) {
			Aufgabe aufgabe = (Aufgabe) pObject;
			handleAufgabe(aufgabe);
		} else {
			MCAufgabe aufgabe = (MCAufgabe) pObject;
			handleQuizaufgabe(aufgabe);

		}
	}

	/**
	 * @param pObject
	 */
	private void handleAufgabe(Aufgabe aufgabe) {
		IPathEditorInput[] pathEditorInput = createEditorInput(aufgabe);
		String editorId = getEditorId(aufgabe.getSchluessel());
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		try {
			for (int i = 0; i < pathEditorInput.length; i++) {
				page.openEditor(pathEditorInput[i], editorId);
			}
		} catch (PartInitException e) {
			throw new MatheJungAltException("Der LaTeX-Editor konnte nicht ge�ffnet werden", e);
		}
	}

	/**
	 * @param pObject
	 */
	private void handleQuizaufgabe(MCAufgabe aufgabe) {
		IPathEditorInput[] pathEditorInput = createEditorInput(aufgabe);
		String editorId = getEditorId(aufgabe.getSchluessel());
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		try {
			for (int i = 0; i < pathEditorInput.length; i++) {
				page.openEditor(pathEditorInput[i], editorId);
			}
		} catch (PartInitException e) {
			throw new MatheJungAltException("Der LaTeX-Editor konnte nicht ge�ffnet werden", e);
		}
	}

	/**
	 * @param pAufgabe
	 * @return
	 */
	private String getEditorId(String pSchluessel) {
		IWorkbench workbench = PlatformUI.getWorkbench();
		IEditorRegistry editorRegistry = workbench.getEditorRegistry();
		IEditorDescriptor descriptor = editorRegistry.getDefaultEditor(pSchluessel + ".tex");
		if (descriptor != null) {
			return descriptor.getId();
		}
		return AufgabeLaTeXEditor.ID_AUFGABEN;
	}

	/**
	 * @param pAufgabe
	 * @return
	 */
	public IPathEditorInput[] createEditorInput(Aufgabe pAufgabe) {
		String pathAufgabe = MatheJungAltActivator.getDefault().getPath(PreferenceConstants.P_PATH_AUFGABENARCHIV_DIR)
			+ File.separator + pAufgabe.getVerzeichnis() + File.separator + pAufgabe.getSchluessel() + ".tex";
		String pathLoesung = MatheJungAltActivator.getDefault().getPath(PreferenceConstants.P_PATH_LOESUNGENARCHIV_DIR)
			+ File.separator + pAufgabe.getVerzeichnis() + File.separator + pAufgabe.getSchluessel() + "_l.tex";
		IPath locationAufgabe = new Path(pathAufgabe);
		IPath locationLoesung = new Path(pathLoesung);
		return new PathEditorInput[] { new PathEditorInput(locationAufgabe), new PathEditorInput(locationLoesung) };
	}

	/**
	 * @param pAufgabe
	 * @return
	 */
	public IPathEditorInput[] createEditorInput(MCAufgabe pAufgabe) {
		String pathAufgabe = MatheJungAltActivator.getDefault().getPath(
			PreferenceConstants.P_PATH_QUIZAUFGABENARCHIV_DIR)
			+ File.separator + pAufgabe.getVerzeichnis() + File.separator + "mc_" + pAufgabe.getSchluessel() + ".tex";
		String pathLoesung = MatheJungAltActivator.getDefault().getPath(
			PreferenceConstants.P_PATH_QUIZLOESUNGENARCHIV_DIR)
			+ File.separator + pAufgabe.getVerzeichnis() + File.separator + "mc_" + pAufgabe.getSchluessel() + "_l.tex";
		IPath locationAufgabe = new Path(pathAufgabe);
		IPath locationLoesung = new Path(pathLoesung);
		return new PathEditorInput[] { new PathEditorInput(locationAufgabe), new PathEditorInput(locationLoesung) };
	}
}
