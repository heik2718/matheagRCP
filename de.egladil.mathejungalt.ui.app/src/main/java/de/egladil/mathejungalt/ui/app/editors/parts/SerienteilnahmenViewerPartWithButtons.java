/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.FormToolkit;

import de.egladil.mathejungalt.domain.mitglieder.Serienteilnahme;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.editors.listeners.SerienteilnahmenViewerListener;
import de.egladil.mathejungalt.ui.app.themes.IImageKeys;
import de.egladil.mathejungalt.ui.app.themes.TableViewerFactory;
import de.egladil.mathejungalt.ui.app.views.ITableViewColumn;
import de.egladil.mathejungalt.ui.app.views.ViewColumn;
import de.egladil.mathejungalt.ui.app.views.contentproviders.SerienteilnahmenViewContentProvider;

/**
 * Der Teil des MasterDetailsBlocks, der die Serienteilnahmen auflistet.
 * 
 * @author Heike Winkelvoß
 */
public class SerienteilnahmenViewerPartWithButtons extends AbstractViewerPartWithButtons {

	/** */
	private int[] columnBounds = new int[] { 100 };

	private String[] columnTitles = new String[] { "Serien" };

	/**
	 * @param pParentPart
	 */
	public SerienteilnahmenViewerPartWithButtons(AbstractMasterDetailsBlockWithViewerPart pParentPart) {
		super(pParentPart);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractViewerPartWithButtons#create(org.eclipse.swt.widgets.Composite,
	 * org.eclipse.ui.forms.widgets.FormToolkit)
	 */
	@Override
	public void init(final Composite pParent, final FormToolkit pToolkit) {
		super.init(pParent, pToolkit);
		addStructuredViewerListener(new SerienteilnahmenViewerListener(this));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractViewerPartWithButtons#createTableViewer()
	 */
	@Override
	protected TableViewer createTableViewer(Composite pContainer) {
		ITableViewColumn[] viewColumns = new ITableViewColumn[columnTitles.length];
		ColumnLabelProvider[] labelProviders = getColumnLableProviders();
		for (int i = 0; i < columnTitles.length; i++) {
			viewColumns[i] = new ViewColumn(columnTitles[i], columnBounds[i], labelProviders[i]);
		}
		return TableViewerFactory.createTableViewer(pContainer, new SerienteilnahmenViewContentProvider(
			MatheJungAltActivator.getDefault().getStammdatenservice()), viewColumns, false);
	}

	/**
	 * @return
	 */
	private ColumnLabelProvider[] getColumnLableProviders() {
		ColumnLabelProvider[] labelProviders = new ColumnLabelProvider[columnBounds.length];
		labelProviders[0] = new ColumnLabelProvider() {
			@Override
			public String getText(Object pElement) {
				return "Serie " + ((Serienteilnahme) pElement).getSerie().getNummer().toString();
			}

			@Override
			public Image getImage(Object pElement) {
				return MatheJungAltActivator.getDefault().getImage(IImageKeys.SERIE);
			}
		};
		return labelProviders;
	}
}
