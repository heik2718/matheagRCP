/**
 * 
 */
package de.egladil.mathejungalt.ui.app.adapters;

import java.util.List;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;

/**
 * Adapter-Interface für ein Masterobjekt (Aufgabensammlung, Mitglied, Buch)
 * 
 * @author Heike Winkelvoss (www.egladil.de)
 */
public interface IMasterObject {

	/**
	 * Ein Titel.
	 * 
	 * @return
	 */
	String getTitle();

	/**
	 * @return Die Items, die zu dieser Sammlung gehoeren.
	 */
	List<IDetailsObject> getItems();

	/**
	 * @return boolean. True, falls die Aufgabensammlung geschlossen ist.
	 */
	boolean isClosed();

	/**
	 * @return
	 */
	int size();

	/**
	 * @return
	 */
	boolean isDetailsLoaded();

	/**
	 * @param pDomainObject
	 * @return
	 */
	boolean canAdd(AbstractMatheAGObject pDomainObject);

	/**
	 * @return
	 */
	AbstractMatheAGObject getDomainObject();
}
