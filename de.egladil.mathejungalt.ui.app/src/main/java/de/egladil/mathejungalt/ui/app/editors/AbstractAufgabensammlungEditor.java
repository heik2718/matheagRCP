/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors;

import java.beans.PropertyChangeEvent;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltInconsistentEntityException;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;

/**
 * @author winkelv
 */
public abstract class AbstractAufgabensammlungEditor extends AbstractMatheEditor {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractAufgabensammlungEditor.class);

	/**
	 *
	 */
	public AbstractAufgabensammlungEditor() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.AbstractMatheEditor#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent pEvt) {
		super.propertyChange(pEvt);
		if (IAufgabensammlung.PROP_BEENDET.equals(pEvt.getPropertyName())) {
			setModified(false);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.rcp.editors.AbstractMatheAGEditor#isNotAnInterestingPropertyChangeEvent(java.beans.
	 * PropertyChangeEvent)
	 */
	@Override
	protected boolean interestingPropertyChangeEvent(PropertyChangeEvent pEvt) {
		String prop = pEvt.getPropertyName();
		return IAufgabensammlung.PROP_BEENDET.equals(prop) || IAufgabensammlungItem.PROP_AUFGABE.equals(prop)
			|| IAufgabensammlungItem.PROP_NUMMER.equals(prop);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public void doSave(IProgressMonitor pMonitor) {
		commitPages(true);
		final IAufgabensammlung aufgabensammlung = (IAufgabensammlung) getDomainObject();
		try {
			getStammdatenservice().aufgabensammlungSpeichern(aufgabensammlung);
			setModified(false);
		} catch (MatheJungAltInconsistentEntityException e) {
			setModified(true);
			MessageDialog.openWarning(Display.getCurrent().getActiveShell(), "Warnung", e.getMessage());
		} catch (Exception e) {
			setModified(true);
			String msg = "Die Aufgabensammlung konnte wegen eines MatheAG-Core-Fehlers nicht gespeichert werden:\n"
				+ e.getMessage();
			LOG.error(msg, e);
			MessageDialog.openError(Display.getCurrent().getActiveShell(), "Fehler!", msg);
		}
	}
}
