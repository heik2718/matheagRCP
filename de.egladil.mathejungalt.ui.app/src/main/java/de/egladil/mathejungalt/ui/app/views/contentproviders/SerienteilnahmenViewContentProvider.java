/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.contentproviders;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.mitglieder.IMitgliedNames;
import de.egladil.mathejungalt.domain.mitglieder.ISerienteilnahmeNames;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.domain.mitglieder.Serienteilnahme;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * ContentProvider für die Serienteilnahmen zu einem Mitglied
 * 
 * @author Heike Winkelvoß
 */
public class SerienteilnahmenViewContentProvider extends AbstractMatheObjectsViewContentProvider implements
	IStructuredContentProvider {

	/**
	 * 
	 */
	public SerienteilnahmenViewContentProvider(IStammdatenservice pStammdatenservice) {
		super(pStammdatenservice);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
	 */
	@Override
	public Object[] getElements(Object pInputElement) {
		if (pInputElement == null || !(pInputElement instanceof Mitglied)) {
			return new ArrayList<Serie>().toArray();
		}
		Mitglied mitglied = (Mitglied) pInputElement;
		if (!mitglied.isSerienteilnahmenLoaded()) {
			getStammdatenservice().serienteilnahmenZuMitgliedLaden(mitglied);
		}

		Iterator<Serienteilnahme> iter = mitglied.serienteilnahmenIterator();
		List<Serienteilnahme> serienteilnahmen = new ArrayList<Serienteilnahme>();
		while (iter.hasNext())
			serienteilnahmen.add(iter.next());

		return serienteilnahmen.toArray();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.ui.app.views.contentproviders.AbstractMatheObjectsViewContentProvider#
	 * isInterestingChangeEvent(java.beans.PropertyChangeEvent)
	 */
	@Override
	public boolean isInterestingChangeEvent(String pPropertyName) {
		if (ISerienteilnahmeNames.PROP_FRUEHSTARTER.equals(pPropertyName)
			|| ISerienteilnahmeNames.PROP_PUNKTE.equals(pPropertyName)
			|| IMitgliedNames.PROP_SERIENTEILNAHMEN.equals(pPropertyName)) {
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
	 */
	@Override
	public void dispose() {
		// nix
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public void inputChanged(Viewer pArg0, Object pArg1, Object pArg2) {
		pArg0.refresh();
	}
}
