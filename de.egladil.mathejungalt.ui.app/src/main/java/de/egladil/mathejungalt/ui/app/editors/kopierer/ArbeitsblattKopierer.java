/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.kopierer;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblattitem;

/**
 * @author winkelv
 */
public class ArbeitsblattKopierer implements IKopierer {

	/**
	 * 
	 */
	public ArbeitsblattKopierer() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.ui.app.editors.kopierer.IKopierer#copyAttributes(de.egladil.mathejungalt.domain.
	 * AbstractMatheAGObject, de.egladil.mathejungalt.domain.AbstractMatheAGObject)
	 */
	@Override
	public void copyAttributes(AbstractMatheAGObject pSource, AbstractMatheAGObject pTarget)
		throws MatheJungAltException {
		if (!(pSource instanceof Arbeitsblatt) && !(pTarget instanceof Arbeitsblatt)) {
			throw new MatheJungAltException(
				"Quelle oder Ziel sind keine Arbeitsblatt: Quelle = " + pSource == null ? "null" : pSource.getClass()
					.getName() + ", Ziel = " + pTarget == null ? "null" : pTarget.getClass().getName());
		}
		Arbeitsblatt source = (Arbeitsblatt) pSource;
		Arbeitsblatt target = (Arbeitsblatt) pTarget;
		target.setBeendet(source.getBeendet());
		target.setSchluessel(source.getSchluessel());
		target.setSperrend(source.getSperrend());
		target.setTitel(source.getTitel());
		target.clearItems();
		for (IAufgabensammlungItem item : source.getItems()) {
			Arbeitsblattitem targetItem = new Arbeitsblattitem();
			copyItemAttributes((Arbeitsblattitem) item, targetItem);
			target.addItem(targetItem);
			targetItem.setArbeitsblatt(target);
		}
	}

	/**
	 * @param pSource
	 * @param pTarget
	 */
	private void copyItemAttributes(Arbeitsblattitem pSource, Arbeitsblattitem pTarget) {
		pTarget.setAufgabe(pSource.getAufgabe());
		pTarget.setNummer(pSource.getNummer());
	}
}
