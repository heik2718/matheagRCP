/**
 *
 */
package de.egladil.mathejungalt.ui.app.views;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.part.ViewPart;

import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.editors.dnd.StructuredViewerDragSourceAdapter;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.AbstractMatheEditorInput;
import de.egladil.mathejungalt.ui.app.handlers.EditObjectHandler;
import de.egladil.mathejungalt.ui.app.handlers.EditObjectHandlerDelegate;
import de.egladil.mathejungalt.ui.app.handlers.ICommandIds;
import de.egladil.mathejungalt.ui.app.views.listeners.AbstractModelListener;

/**
 * @author aheike
 */
public abstract class AbstractMatheJungAltView extends ViewPart {

	/** */
	private TableViewer viewer;

	/** */
	private Action doubleClickAction;

	/** */
	private EditObjectHandlerDelegate editObjectHandlerDelegate;

	/** */
	private AbstractModelListener modelListener;

	/**
	 *
	 */
	public AbstractMatheJungAltView() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
	 */
	public void createPartControl(Composite parent) {
		viewer = initTableViewer(parent);
		addDragSupport();
		makeActions();
		hookContextMenu();
		hookDoubleClickAction();
		hookPullDownMenu();
		createHandlers();

		ColumnViewerToolTipSupport.enableFor(viewer);
		viewer.setInput(getViewSite());

		modelListener = getModelListener(viewer);
		IStammdatenservice stammdatenservice = MatheJungAltActivator.getDefault().getStammdatenservice();
		stammdatenservice.addModelChangeListener(modelListener);

		// Propagiert SelectionEvent an das System
		getSite().setSelectionProvider(viewer);
	}

	/**
	 * @param pViewer
	 * @return {@link AbstractModelListener}
	 */
	protected abstract AbstractModelListener getModelListener(TableViewer pViewer);

	/**
	 * Implementiert die doubleClickAction zum Editieren eines Objekts. Unterklassen koennen weitere Actions
	 * instanziieren.
	 */
	protected void makeActions() {
		doubleClickAction = new Action() {
			public void run() {
				if (!(viewer.getSelection() instanceof IStructuredSelection) || viewer.getSelection().isEmpty()) {
					return;
				}
				AbstractMatheEditorInput editorInput = getEditorInput(((IStructuredSelection) viewer.getSelection())
					.getFirstElement());
				if (editorInput != null) {
					editObjectHandlerDelegate.executeCommand(editorInput);
					setFocus();
				}
			}
		};
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	public void setFocus() {
		if (viewer != null) {
			viewer.getControl().setFocus();
		}
	}

	/**
	 * Gibt den relevanten {@link AbstractMatheEditorInput} zurueck.
	 *
	 * @param pObj {@link Object} das zu editierende Objekt.
	 * @return {@link AbstractMatheEditorInput} oder null;
	 */
	protected abstract AbstractMatheEditorInput getEditorInput(Object pObj);

	/**
	 * Anker zum Einbinden eines PullDown-Menues. Leere Implementierung. Unterklassen koennen diese Methode
	 * ueberschreiben, wenn ein PullDownMenu benoetigt wird.
	 */
	protected void hookPullDownMenu() {
	}

	/**
	 * Erzeugen programmatisch die ben�tigten Handler. Unterklassen koennen weitere Handler registrieren.
	 */
	protected void createHandlers() {
		IHandlerService handlerService = (IHandlerService) getSite().getService(IHandlerService.class);
		if (editObjectHandlerDelegate == null) {
			editObjectHandlerDelegate = new EditObjectHandlerDelegate(getEditorID());
			handlerService.activateHandler(ICommandIds.EDIT_OBJECT, new EditObjectHandler(viewer,
				editObjectHandlerDelegate));
		}
	}

	/**
	 * @return String die spezielle ID
	 */
	protected abstract String getEditorID();

	/**
	 *
	 */
	private void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				doubleClickAction.run();
			}
		});
	}

	/**
	 *
	 */
	private void hookContextMenu() {
		MenuManager menuMgr = new MenuManager("#PopupMenu");
		menuMgr.setRemoveAllWhenShown(true);
		Menu menu = menuMgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuMgr, viewer);
	}

	/**
	 * Fügt in speziellen Füllen DragSupport hinzu
	 */
	protected void addDragSupport() {
		// wird DragSource
		int operations = DND.DROP_COPY | DND.DROP_DEFAULT;
		DragSource dragSource = new DragSource(viewer.getControl(), operations);

		Transfer[] types = new Transfer[] { LocalSelectionTransfer.getTransfer() };
		dragSource.setTransfer(types);

		dragSource.addDragListener(new StructuredViewerDragSourceAdapter(viewer));
	}

	/**
	 * @param pParent
	 * @return
	 */
	protected abstract TableViewer initTableViewer(Composite pParent);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#dispose()
	 */
	@Override
	public void dispose() {
		IStammdatenservice stammdatenservice = MatheJungAltActivator.getDefault().getStammdatenservice();
		stammdatenservice.removeModelChangeListener(modelListener);
		super.dispose();
	}

	/**
	 * @return the stammdatenservice
	 */
	public IStammdatenservice getStammdatenservice() {
		IStammdatenservice stammdatenservice = MatheJungAltActivator.getDefault().getStammdatenservice();
		return stammdatenservice;
	}

	/**
	 * @return the viewer
	 */
	public TableViewer getViewer() {
		return viewer;
	}

}
