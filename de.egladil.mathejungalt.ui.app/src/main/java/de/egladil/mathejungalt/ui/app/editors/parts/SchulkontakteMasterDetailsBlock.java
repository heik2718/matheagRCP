/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import org.eclipse.ui.forms.DetailsPart;
import org.eclipse.ui.forms.IDetailsPage;

import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.domain.schulen.Schulkontakt;
import de.egladil.mathejungalt.ui.app.editors.dnd.KontakteDropTargetListener;
import de.egladil.mathejungalt.ui.app.editors.pages.KontaktAttributesFormPage;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * @author Heike Winkelvoß
 */
public class SchulkontakteMasterDetailsBlock extends AbstractMasterDetailsBlockWithViewerPart {

	/** */
	private AbstractDetailsPage detailsPage;

	/**
	 * @param pParentPage
	 */
	public SchulkontakteMasterDetailsBlock(KontaktAttributesFormPage pParentPage) {
		super(pParentPage);
		detailsPage = new SchulkontaktDetailsPage(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractMasterDetailsBlock#getSectionDescription()
	 */
	@Override
	protected String getSectionDescription() {
		return ITextConstants.SECTION_DESCR_SCHULKONTAKEVIEWER;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractMasterDetailsBlock#getWidthHint()
	 */
	@Override
	protected int getWidthHint() {
		return 300;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractMasterDetailsBlock#getSectionHeader()
	 */
	@Override
	protected String getSectionHeader() {
		return ITextConstants.SECTION_HEADER_KONTAKT_SCHULE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractMasterDetailsBlock#initViewerPart()
	 */
	@Override
	protected void hookViewerPart() {
		setViewerPart(new SchulkontakteViewerPartWithButtons(this));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.MasterDetailsBlock#registerPages(org.eclipse.ui.forms.DetailsPart)
	 */
	@Override
	protected void registerPages(DetailsPart pDetailsPart) {
		pDetailsPart.setPageProvider(this);
		pDetailsPart.registerPage(Schulkontakt.class, detailsPage);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.IDetailsPageProvider#getPage(java.lang.Object)
	 */
	@Override
	public IDetailsPage getPage(Object pKey) {
		if (Schulkontakt.class.equals(pKey)) {
			return detailsPage;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.IDetailsPageProvider#getPageKey(java.lang.Object)
	 */
	@Override
	public Object getPageKey(Object pObject) {
		if (pObject instanceof Schulkontakt) {
			return Schulkontakt.class;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.editors.parts.AbstractMasterDetailsBlockWithViewerPart#hookDropTargetListener()
	 */
	@Override
	protected void hookDropTargetListener() {
		Kontakt kontakt = (Kontakt) getParentPage().getDomainObject();
		setDropTargetListener(new KontakteDropTargetListener(kontakt));
	}
}
