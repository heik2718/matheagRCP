/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.ui.app.views;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.handlers.IHandlerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mathejungalt.domain.schulen.Schule;
import de.egladil.mathejungalt.ui.app.editors.SchuleEditor;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.AbstractMatheEditorInput;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.SchuleEditorInput;
import de.egladil.mathejungalt.ui.app.handlers.ICommandIds;
import de.egladil.mathejungalt.ui.app.handlers.ObjektAnlegenHandler;
import de.egladil.mathejungalt.ui.app.themes.TableViewerFactory;
import de.egladil.mathejungalt.ui.app.views.comparators.AbstractViewerComparator;
import de.egladil.mathejungalt.ui.app.views.comparators.IViewerComparator;
import de.egladil.mathejungalt.ui.app.views.comparators.SchulenViewerComparator;
import de.egladil.mathejungalt.ui.app.views.contentproviders.SchulenViewContentProvider;
import de.egladil.mathejungalt.ui.app.views.labelproviders.CommonColumnLabelProvider;
import de.egladil.mathejungalt.ui.app.views.labelproviders.SchulenLabelProvider;
import de.egladil.mathejungalt.ui.app.views.listeners.AbstractModelListener;
import de.egladil.mathejungalt.ui.app.views.listeners.SchulenModelListener;

/**
 * <p>
 * NOTE: THIS WIDGET AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class SchulenView extends AbstractMatheJungAltView {

	private static final Logger LOG = LoggerFactory.getLogger(SchulenView.class);

	/** */
	public static final String ID = "de.egladil.mathejungalt.ui.app.views.SchulenView";

	/** */
	private String[] columnTitles = new String[] { "Schl\u00FCssel", "Name", "Bundesland" };

	/** */
	private int[] columnBounds = new int[] { 50, 300, 50 };

	/**
   *
   */
	public SchulenView() {
		System.out.println("SchulenView");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#getModelListener(org.eclipse.jface.viewers.TableViewer
	 * )
	 */
	@Override
	protected AbstractModelListener getModelListener(TableViewer pViewer) {
		return new SchulenModelListener(pViewer);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#getEditorInput(java.lang.Object)
	 */
	@Override
	protected AbstractMatheEditorInput getEditorInput(Object pObj) {
		if (!(pObj instanceof Schule)) {
			return null;
		}
		Schule schule = (Schule) pObj;
		if (!schule.isMiniTeilnahmenLoaded()) {
			getStammdatenservice().minikaenguruTeilnahmenLaden(schule);
		}
		return new SchuleEditorInput(schule);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#getEditorID()
	 */
	@Override
	protected String getEditorID() {
		return SchuleEditor.ID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#initTableViewer(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected TableViewer initTableViewer(Composite pParent) {
		ITableViewColumn[] viewColumns = new ITableViewColumn[columnTitles.length];
		IViewerComparator[] comparators = getColumnComparators();
		ColumnLabelProvider[] labelProviders = getColumnLableProviders();
		for (int i = 0; i < columnTitles.length; i++) {
			viewColumns[i] = new ViewColumn(columnTitles[i], columnBounds[i], comparators[i], labelProviders[i]);
		}
		return TableViewerFactory.createTableViewer(pParent, new SchulenViewContentProvider(getStammdatenservice()),
			viewColumns, true);
	}

	/**
	 * Erzeugt abhängig vom Spaltenindex einen {@link ColumnLabelProvider}
	 *
	 * @param pInd int der Spaltenindex
	 * @return {@link ColumnLabelProvider}
	 */
	private ColumnLabelProvider[] getColumnLableProviders() {
		CommonColumnLabelProvider[] labelProviders = new CommonColumnLabelProvider[columnTitles.length];

		for (int i = 0; i < labelProviders.length; i++) {
			labelProviders[i] = new SchulenLabelProvider(i);
		}
		return labelProviders;
	}

	/**
	 * @return
	 */
	private AbstractViewerComparator[] getColumnComparators() {
		AbstractViewerComparator[] comparators = new SchulenViewerComparator[columnTitles.length];
		for (int i = 0; i < comparators.length; i++) {
			comparators[i] = new SchulenViewerComparator(i);
		}
		return comparators;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#createHandlers()
	 */
	@Override
	protected void createHandlers() {
		super.createHandlers();
		IHandlerService handlerService = (IHandlerService) getSite().getService(IHandlerService.class);
		handlerService.activateHandler(ICommandIds.ANLEGEN, new ObjektAnlegenHandler(getStammdatenservice(), ID));
	}
}
