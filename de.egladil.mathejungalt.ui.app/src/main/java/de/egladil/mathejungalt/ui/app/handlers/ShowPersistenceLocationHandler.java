/**
 * 
 */
package de.egladil.mathejungalt.ui.app.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.PlatformUI;

import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;

/**
 * @author aheike
 */
public class ShowPersistenceLocationHandler extends AbstractHandler {

	/**
	 * 
	 */
	public ShowPersistenceLocationHandler() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent pArg0) throws ExecutionException {
		IStammdatenservice stammdatenservice = MatheJungAltActivator.getDefault().getStammdatenservice();
		String db = stammdatenservice.getPersistenceLocation(IStammdatenservice.PROTOKOLL_DB + ":");
		MessageDialog.openInformation(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
			"Persistenz-Location", "DB = " + db);
		return null;
	}
}
