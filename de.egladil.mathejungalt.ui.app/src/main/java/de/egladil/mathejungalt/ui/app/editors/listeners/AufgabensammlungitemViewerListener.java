/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.listeners;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.jface.viewers.ColumnLabelProvider;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serienitem;
import de.egladil.mathejungalt.service.stammdaten.event.ModelChangeEvent;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.adapters.IDetailsObject;
import de.egladil.mathejungalt.ui.app.editors.parts.AbstractViewerPartWithButtons;
import de.egladil.mathejungalt.ui.app.views.labelproviders.AufgabenLabelProvider;

/**
 * Listener für den TableViewer mit den {@link IAufgabensammlungItem}.
 *
 * @author Winkelv
 */
public class AufgabensammlungitemViewerListener extends AbstractViewerPartWithButtonsViewerListener {

	private AufgabenLabelProvider labelProvider = new AufgabenLabelProvider(0);

	/**
	 * @param pView
	 */
	public AufgabensammlungitemViewerListener(AbstractViewerPartWithButtons pView) {
		super(pView);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.listeners.AbstractViewerPartWithButtonsViewerListener#getObjectPool()
	 */
	@Override
	public Collection<AbstractMatheAGObject> getObjectPool() {
		Collection<AbstractMatheAGObject> objectPool = new ArrayList<AbstractMatheAGObject>();
		Collection<Aufgabe> aufgaben = getStammdatenservice().getAufgaben();
		for (Aufgabe aufgabe : aufgaben) {
			objectPool.add(aufgabe);
		}
		return objectPool;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.listeners.IStructuredViewerPartViewerListener#handleRemove()
	 */
	@Override
	public void handleRemove() {
		if (getEditorInput() == null || getSelectedDetails() == null || getSelectedDetails().size() != 1) {
			return;
		}
		Object selected = getSelectedDetails().get(0);
		IAufgabensammlungItem item = null;
		if (selected instanceof IAufgabensammlungItem) {
			item = (IAufgabensammlungItem) selected;
		}
		if (selected instanceof IDetailsObject) {
			item = (IAufgabensammlungItem) ((IDetailsObject) selected).getDomainObject();
		}
		if (item == null) {
			throw new MatheJungAltException(
				"Fehler in der Anwendung: das gewählte Objekt hat nicht den richtigen Typ: typ = "
					+ selected.getClass().getName());
		}
		getStammdatenservice().aufgabeAusSammlungEntfernen(item);
		if (getView() != null) {
			getView().getViewer().refresh();
			getView().updateButtonAndMenuStatus();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.editors.listeners.AbstractViewerPartWithButtonsViewerListener#specialAdd(java.
	 * util.List)
	 */
	@Override
	protected void specialAdd(List<AbstractMatheAGObject> selectedObjects) {
		Serie serie = (Serie) getEditorInput();
		Serienitem item = null;
		if (selectedObjects != null) {
			for (int i = 0; i < selectedObjects.size(); i++) {
				if (i == 0) {
					item = (Serienitem) getStammdatenservice().aufgabeZuSammlungHinzufuegen(serie,
						(Aufgabe) selectedObjects.get(i));
				} else {
					getStammdatenservice().aufgabeZuSammlungHinzufuegen(serie, (Aufgabe) selectedObjects.get(i));
				}
			}
		}
		if (item != null) {
			Object object = MatheJungAltActivator.getDefault().getAdapter(item, IDetailsObject.class);
			if (object == null) {
				throw new MatheJungAltException("Kein Adapter fuer " + item.getClass().getName()
					+ " und IDetailsObject gefunden");
			}
			IDetailsObject detailsObject = (IDetailsObject) object;
			updateDetailsList(detailsObject);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.editors.listeners.AbstractViewerPartWithButtonsViewerListener#isInterestingEvent
	 * (de.egladil.mathejungalt.service.stammdaten.event.ModelChangeEvent)
	 */
	@Override
	protected boolean isInterestingEvent(ModelChangeEvent pEvt) {
		if (ModelChangeEvent.ADD_EVENT.equals(pEvt.getEventType())
			|| ModelChangeEvent.REMOVE_EVENT.equals(pEvt.getEventType())) {
			return pEvt.getNewValue() instanceof IAufgabensammlungItem
				|| pEvt.getOldValue() instanceof IAufgabensammlungItem;
		}
		if (ModelChangeEvent.PROPERTY_CHANGE_EVENT.equals(pEvt.getEventType())
			&& !(pEvt.getSource() instanceof IAufgabensammlungItem)) {
			return false;
		}
		final String property = pEvt.getProperty();
		return isRelevant(property);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.editors.listeners.AbstractViewerPartWithButtonsViewerListener#isInterestingEvent
	 * (java.beans.PropertyChangeEvent)
	 */
	@Override
	protected boolean isInterestingEvent(PropertyChangeEvent pEvt) {
		if (!(pEvt.getSource() instanceof IAufgabensammlungItem)) {
			return false;
		}
		return isRelevant(pEvt.getPropertyName());
	}

	/**
	 * @return {@link ColumnLabelProvider} der LabelProvider.
	 */
	public ColumnLabelProvider getLabelProvider() {
		return labelProvider;
	}

	/**
	 * @param pPropertyName
	 * @return boolean
	 */
	private boolean isRelevant(final String pPropertyName) {
		return IAufgabensammlungItem.PROP_NUMMER.equals(pPropertyName);
	}
}
