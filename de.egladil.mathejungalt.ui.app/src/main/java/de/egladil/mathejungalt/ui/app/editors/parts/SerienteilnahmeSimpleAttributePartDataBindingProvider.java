/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.forms.widgets.FormToolkit;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.IMatheAGObjectNames;
import de.egladil.mathejungalt.domain.mitglieder.ISerienteilnahmeNames;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;
import de.egladil.mathejungalt.ui.app.themes.PartControlsFactory;

/**
 * @author winkelv
 */
public class SerienteilnahmeSimpleAttributePartDataBindingProvider extends
	AbstractSimpleAttributePartDataBindingProvider {

	/**
	 * 
	 */
	public SerienteilnahmeSimpleAttributePartDataBindingProvider() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.contentproviders.AbstractAttributePartContentsProvider#anzahlZeilen()
	 */
	@Override
	protected int anzahlZeilen() {
		return 3;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.contentproviders.AbstractAttributePartContentsProvider#initLabelsAndTexts(org.eclipse
	 * .swt.widgets .Composite, org.eclipse.ui.forms.widgets.FormToolkit, boolean)
	 */
	@Override
	protected void initControls(Composite pClient, FormToolkit pToolkit, boolean pEditable, AbstractMatheAGObject pInput) {
		Control[] controls = getControls();
		Control[] labels = new Control[controls.length];

		int widthHint = 150;
		labels[0] = PartControlsFactory.createIdLabel(pClient, pToolkit);
		controls[0] = PartControlsFactory.createIdText(pClient, pToolkit, widthHint);

		labels[1] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_PUNKTE);
		controls[1] = PartControlsFactory.createText(pClient, pToolkit, widthHint, pEditable);

		labels[2] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_FRUEHSTARTER);
		controls[2] = PartControlsFactory.createText(pClient, pToolkit, widthHint, pEditable);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.contentproviders.IAttributePartContentsProvider#refreshDataBindings(de.egladil.mathe
	 * .core.domain .AbstractMatheAGObject)
	 */
	@Override
	public void refreshDataBindings(AbstractMatheAGObject pInput) {
		super.refreshDataBindings(pInput);

		Binding[] bindings = getBindings();
		Control[] controls = getControls();

		IObservableValue observableWidget = SWTObservables.observeText(controls[0], SWT.Modify);
		IObservableValue observableProperty = BeansObservables.observeValue(pInput, IMatheAGObjectNames.PROP_ID);
		bindings[0] = getDbc().bindValue(observableWidget, observableProperty, null, null);

		observableWidget = SWTObservables.observeText(controls[1], SWT.Modify);
		observableProperty = BeansObservables.observeValue(pInput, ISerienteilnahmeNames.PROP_PUNKTE);
		bindings[1] = getDbc().bindValue(observableWidget, observableProperty, null, null);

		observableWidget = SWTObservables.observeText(controls[2], SWT.Modify);
		observableProperty = BeansObservables.observeValue(pInput, ISerienteilnahmeNames.PROP_FRUEHSTARTER);
		bindings[2] = getDbc().bindValue(observableWidget, observableProperty, null, null);
	}
}
