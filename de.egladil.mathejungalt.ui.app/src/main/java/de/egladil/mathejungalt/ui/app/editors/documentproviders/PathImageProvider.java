/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.documentproviders;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Image;

/**
 * @author winkelv
 */
public class PathImageProvider implements ImageProvider {

	/** */
	private String path;

	/**
	 * 
	 */
	public PathImageProvider(String pPath) {
		path = pPath;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.images.providers.ImageProvider#disposeImage(org.eclipse.swt.graphics.Image)
	 */
	@Override
	public void disposeImage(Image pImage) {
		if (pImage != null) {
			pImage.dispose();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.images.providers.ImageProvider#getImage(org.eclipse.swt.graphics.Device)
	 */
	@Override
	public Image getImage(Device pTarget) {
		FileInputStream stream = null;
		try {
			File file = new File(path);
			if (!file.isFile()) {
				return null;
			}
			stream = new FileInputStream(file);
			return new Image(pTarget, stream);
		} catch (FileNotFoundException e) {
			return null;
		} finally {
			if (stream != null) {
				try {
					stream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
