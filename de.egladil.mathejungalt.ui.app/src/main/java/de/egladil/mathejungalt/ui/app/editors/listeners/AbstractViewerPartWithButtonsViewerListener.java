/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.listeners;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.service.stammdaten.event.ModelChangeEvent;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.adapters.IDetailsObject;
import de.egladil.mathejungalt.ui.app.adapters.IMasterObject;
import de.egladil.mathejungalt.ui.app.dialogs.ObjectPickerDialog;
import de.egladil.mathejungalt.ui.app.dialogs.contentproviders.ObjectPickerDialogContentProvider;
import de.egladil.mathejungalt.ui.app.editors.parts.AbstractViewerPartWithButtons;
import de.egladil.mathejungalt.ui.app.themes.ButtonFactory;

/**
 * @author Winkelv
 */
public abstract class AbstractViewerPartWithButtonsViewerListener implements IStructuredViewerPartViewerListener {

	/** */
	private AbstractViewerPartWithButtons view;

	/** */
	private List<IDetailsObject> selectedDetails;

	/** */
	private IStammdatenservice stammdatenservice;

	private ObjectPickerDialogContentProvider neuerContentProvider;

	/**
	 * @param pView
	 */
	public AbstractViewerPartWithButtonsViewerListener(AbstractViewerPartWithButtons pView) {
		super();
		view = pView;
		selectedDetails = new ArrayList<IDetailsObject>();
		stammdatenservice = MatheJungAltActivator.getDefault().getStammdatenservice();
		stammdatenservice.addModelChangeListener(this);

		AbstractMatheAGObject editorInput = getEditorInput();
		Object object = MatheJungAltActivator.getDefault().getAdapter(editorInput, IMasterObject.class);
		if (object == null) {
			throw new MatheJungAltException("Kein Adapter fuer " + editorInput.getClass().getName()
				+ " und IMasterObject gefunden");
		}
		IMasterObject masterObject = (IMasterObject) object;
		neuerContentProvider = new ObjectPickerDialogContentProvider(masterObject, getObjectPool());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
	 */
	@Override
	public void widgetDefaultSelected(SelectionEvent pE) {
		// ignore

	}

	/**
	 * Ermittelt die speziellen Domain-Objekte, aus denen die Details gebildet werden können.
	 *
	 * @return Collection<AbstractMatheAGObject>
	 */
	public abstract Collection<AbstractMatheAGObject> getObjectPool();

	/**
	 * @return {@link ColumnLabelProvider} der LabelProvider.
	 */
	public abstract ColumnLabelProvider getLabelProvider();

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
	 */
	@Override
	public void widgetSelected(SelectionEvent pE) {
		if (!(pE.getSource() instanceof Button))
			return;
		Button source = (Button) pE.getSource();
		Object data = source.getData();
		if (data.equals(ButtonFactory.ADD)) {
			handleAdd();
			return;
		}
		if (data.equals(ButtonFactory.REMOVE)) {
			handleRemove();
		}
	}

	/**
	 * @param selectedObjects
	 */
	protected abstract void specialAdd(List<AbstractMatheAGObject> selectedObjects);

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.rcp.editors.listeners.IStructuredViewerPartViewerListener#handleEdit(org.eclipse.jface.viewers
	 * . IStructuredSelection)
	 */
	@Override
	public void handleEdit(IStructuredSelection pSelection) {
		// standardmäßig nichts tun
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IDoubleClickListener#doubleClick(org.eclipse.jface.viewers.DoubleClickEvent)
	 */
	@Override
	public void doubleClick(DoubleClickEvent pEvent) {
		// im Normalfall ignorieren
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent pEvt) {
		if (!isInterestingEvent(pEvt)) {
			return;
		}
		// Verhinderung des Flackereffekts bei großen Tabellen.
		final TableViewer tableViewer = (TableViewer) view.getViewer();
		tableViewer.getTable().setRedraw(false);
		try {
			// Falls ein Objekt gelöscht wird, dann müssen zugehörige geöffnete Editoren geschlossen werden
		} finally {
			tableViewer.getTable().setRedraw(true);
			tableViewer.refresh();
		}
	}

	/**
	 * Standardimplementierung gibt false zurück.
	 *
	 * @param pEvt
	 * @return
	 */
	protected boolean isInterestingEvent(PropertyChangeEvent pEvt) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.event.IModelChangeListener#modelChanged(de.egladil.mathejungalt.service
	 * .stammdaten.event.ModelChangeEvent)
	 */
	@Override
	public void modelChanged(ModelChangeEvent pEvt) {
		// hier nochmal schauen, ob das Event überhaupt interessiert
		if (!isInterestingEvent(pEvt)) {
			return;
		}
		// Verhinderung des Flackereffekts bei großen Tabellen.
		final TableViewer tableViewer = (TableViewer) view.getViewer();
		tableViewer.getTable().setRedraw(false);
		try {
			if (ModelChangeEvent.ADD_EVENT.equals(pEvt.getEventType())) {
				tableViewer.setSelection(new StructuredSelection(pEvt.getNewValue()), true);
			}
			// Noch nicht fertig: Falls ein Objekt gelöscht wird, dann müssen zugehörige geöffnete Editoren geschlossen
			// werden
			if (ModelChangeEvent.REMOVE_EVENT.equals(pEvt.getEventType())) {
				tableViewer.setSelection(null);
			}
		} finally {
			tableViewer.getTable().setRedraw(true);
			tableViewer.refresh();
		}
	}

	/**
	 * Standardimplementierung gibt false zurück.
	 *
	 * @param pEvt
	 * @return
	 */
	protected abstract boolean isInterestingEvent(ModelChangeEvent pEvt);

	/**
	 * @return the editorInput
	 */
	protected AbstractMatheAGObject getEditorInput() {
		return (AbstractMatheAGObject) view.getViewer().getInput();
	}

	/**
	 * @return the view
	 */
	protected AbstractViewerPartWithButtons getView() {
		return view;
	}

	/**
	 * @return the selectedDetails
	 */
	protected List<IDetailsObject> getSelectedDetails() {
		return selectedDetails;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent
	 * )
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void selectionChanged(SelectionChangedEvent pEvent) {
		if (pEvent.getSelection().isEmpty())
			return;
		StructuredSelection selection = (StructuredSelection) pEvent.getSelection();
		selectedDetails.clear();
		selectedDetails.addAll(selection.toList());
		if (getView() != null) {
			getView().updateButtonAndMenuStatus();
		}
	}

	/**
	 * @return the stammdatenservice
	 */
	protected IStammdatenservice getStammdatenservice() {
		return stammdatenservice;
	}

	/**
	 * @return
	 */
	protected ObjectPickerDialog getObjectPicker() {
		ObjectPickerDialogContentProvider contentProvider = this.getNeuerContentProvider();
		ColumnLabelProvider labelProvider = this.getLabelProvider();
		ViewerComparator comparator = this.getObjectPickerComparator();
		ObjectPickerDialog dialog = new ObjectPickerDialog(Display.getCurrent().getActiveShell(), contentProvider,
			labelProvider, comparator);
		return dialog;
	}

	/**
	 * Dieser Comparator sortiert die Elemente absteigend.
	 *
	 * @return
	 */
	protected ViewerComparator getObjectPickerComparator() {
		return new ViewerComparator() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.jface.viewers.ViewerComparator#compare(org.eclipse.jface.viewers.Viewer,
			 * java.lang.Object, java.lang.Object)
			 */
			@Override
			public int compare(Viewer viewer, Object e1, Object e2) {
				AbstractMatheAGObject o1 = (AbstractMatheAGObject) e1;
				AbstractMatheAGObject o2 = (AbstractMatheAGObject) e2;
				return o2.getId().intValue() - o1.getId().intValue();
			}
		};
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.listeners.IStructuredViewerPartViewerListener#handleAdd()
	 */
	@Override
	public void handleAdd() {
		ObjectPickerDialog dialog = getObjectPicker();
		if (dialog.open() == Dialog.OK) {
			List<AbstractMatheAGObject> selectedObjects = dialog.getSelectedObjects();
			if (selectedObjects != null && !selectedObjects.isEmpty()) {
				specialAdd(selectedObjects);
			}
		}
	}

	/**
	 * @param detailsObject
	 */
	protected void updateDetailsList(IDetailsObject detailsObject) {
		getSelectedDetails().add(detailsObject);
		if (getView() != null) {
			getView().getViewer().refresh();
			getView().getViewer().setSelection(new StructuredSelection(detailsObject.getDomainObject()));
			getView().updateButtonAndMenuStatus();
		}
	}

	/**
	 * @return the neuerContentProvider
	 */
	protected final ObjectPickerDialogContentProvider getNeuerContentProvider() {
		return neuerContentProvider;
	}
}