/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

/**
 * @author aheike
 */
public class ZweckMinikaenguruDetailsPage extends AbstractDetailsPage {

	/**
	 * @param pMaster
	 */
	public ZweckMinikaenguruDetailsPage(AbstractMasterDetailsBlock pMaster) {
		super(pMaster);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.parts.AbstractDetailsPage#getAttributesPart()
	 */
	@Override
	protected MatheAGObjectAttributesPart getAttributesPart() {
		MinikaengurustufeSimpleAttributePartDataBindingProvider contentsProvider = new MinikaengurustufeSimpleAttributePartDataBindingProvider();
		return new MatheAGObjectAttributesPart(false, contentsProvider);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.parts.AbstractDetailsPage#getHeader()
	 */
	@Override
	protected String getHeader() {
		return "Stufe";
	}
}
