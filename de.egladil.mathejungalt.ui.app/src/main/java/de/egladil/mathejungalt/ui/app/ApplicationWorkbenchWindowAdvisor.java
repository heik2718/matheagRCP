package de.egladil.mathejungalt.ui.app;

import org.eclipse.jface.resource.ColorRegistry;
import org.eclipse.swt.graphics.Point;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mathejungalt.ui.app.themes.IColorKeys;

public class ApplicationWorkbenchWindowAdvisor extends WorkbenchWindowAdvisor {

	private static final Logger LOG = LoggerFactory.getLogger(ApplicationWorkbenchWindowAdvisor.class);

	public ApplicationWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
		super(configurer);
	}

	public ActionBarAdvisor createActionBarAdvisor(IActionBarConfigurer configurer) {
		return new ApplicationActionBarAdvisor(configurer);
	}

	public void preWindowOpen() {
		IWorkbenchWindowConfigurer configurer = getWindowConfigurer();
		configurer.setInitialSize(new Point(1400, 750));
		configurer.setShowCoolBar(true);
		configurer.setShowStatusLine(true);
		configurer.setTitle("Mathe f\u00FCr jung und alt");
		configurer.setShowPerspectiveBar(true);
		configurer.setShowFastViewBars(true);
		initColors();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.application.WorkbenchWindowAdvisor#postWindowOpen()
	 */
	@Override
	public void postWindowOpen() {
		// IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		// try {
		// page.showView(AufgabenView.ID);
		// page.showView(MitgliederView.ID);
		// page.showView(SerienView.ID);
		// } catch (PartInitException e) {
		// LOG.error(e.getMessage(), e);
		// }
		super.postWindowOpen();
	}

	/**
	 *
	 */
	private void initColors() {
		ColorRegistry reg = PlatformUI.getWorkbench().getThemeManager().getCurrentTheme().getColorRegistry();
		reg.put(IColorKeys.BLUE, IColorKeys.RGB_BLUE);
		reg.put(IColorKeys.RED, IColorKeys.RGB_RED);
		reg.put(IColorKeys.ORANGE, IColorKeys.RGB_ORANGE);
		reg.put(IColorKeys.GREEN, IColorKeys.RGB_GREEN);
		reg.put(IColorKeys.YELLOW, IColorKeys.RGB_YELLOW);
		reg.put(IColorKeys.LIGHT_GREY, IColorKeys.RGB_LIGHT_GREY);
		reg.put(IColorKeys.WHITE, IColorKeys.RGB_WHITE);
		reg.put(IColorKeys.BLACK, IColorKeys.RGB_BLACK);
	}
}
