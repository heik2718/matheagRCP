/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.editorinputs;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.ui.app.editors.kopierer.ArbeitsblattKopierer;
import de.egladil.mathejungalt.ui.app.editors.kopierer.IKopierer;

/**
 * @author winkelv
 */
public class ArbeitsblattEditorInput extends AbstractAufgabensammlungEditorInput {

	/** */
	private ArbeitsblattKopierer arbeitsblattKopierer;

	/**
	 * @param pObject
	 */
	public ArbeitsblattEditorInput(AbstractMatheAGObject pObject) {
		super(pObject);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.editorinputs.AbstractMatheEditorInput#getKopierer()
	 */
	@Override
	public IKopierer getKopierer() {
		if (arbeitsblattKopierer == null) {
			arbeitsblattKopierer = new ArbeitsblattKopierer();
		}
		return arbeitsblattKopierer;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.editorinputs.AbstractAufgabensammlungEditorInput#getCacheTarget()
	 */
	@Override
	public AbstractMatheAGObject getCacheTarget() {
		return new Arbeitsblatt();
	}
}
