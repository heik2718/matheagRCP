/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.ui.app.editors.dnd;

import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.domain.schulen.Schule;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;

/**
 * <p>
 * NOTE: THIS WIDGET AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class KontakteDropTargetListener extends StructuredViewerDropTargetAdapter {

	private static final Logger LOG = LoggerFactory.getLogger(KontakteDropTargetListener.class);

	/**
	 * @param pTargetObject
	 */
	public KontakteDropTargetListener(AbstractMatheAGObject pTargetObject) {
		super(pTargetObject);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.dnd.DropTargetAdapter#drop(org.eclipse.swt.dnd.DropTargetEvent)
	 */
	@Override
	public void drop(DropTargetEvent event) {
		if (LocalSelectionTransfer.getTransfer().isSupportedType(event.currentDataType)) {
			AbstractMatheAGObject domainObject = getDragedObject();
			if (domainObject == null) {
				return;
			}
			Kontakt kontakt = (Kontakt) getTargetObject();
			if (domainObject instanceof Schule) {
				Schule schule = (Schule) domainObject;
				if (kontakt.canAdd(schule)) {
					MatheJungAltActivator.getDefault().getStammdatenservice()
						.kontaktZuSchuleHinzufuegen(kontakt, schule);
				}
			}
		}
	}
}
