/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.converters;

import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenzweck;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Konvertiert einen {@link Aufgabenzweck} in einen String.
 * 
 * @author Winkelv
 */
public class AufgabenzweckToStringConverter implements IConverter {

	/**
	 * 
	 */
	public AufgabenzweckToStringConverter() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object pFromObject) {
		Aufgabenzweck param = (Aufgabenzweck) pFromObject;
		switch (param) {
		case S:
			return ITextConstants.ZWECK_SERIE;
		case K:
			return ITextConstants.ZWECK_MINIKAENGURU;
		case M:
			return ITextConstants.ZWECK_MONOID;
		default:
			throw new IllegalArgumentException("Falscher Aufgabenzweck " + param);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getFromType()
	 */
	@Override
	public Object getFromType() {
		return Aufgabenzweck.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getToType()
	 */
	@Override
	public Object getToType() {
		return String.class;
	}
}
