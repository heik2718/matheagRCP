package de.egladil.mathejungalt.ui.app.dialogs;

import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.ui.app.themes.TableViewerFactory;
import de.egladil.mathejungalt.ui.app.views.ITableViewColumn;
import de.egladil.mathejungalt.ui.app.views.KontakteView;
import de.egladil.mathejungalt.ui.app.views.ViewColumn;
import de.egladil.mathejungalt.ui.app.views.comparators.IViewerComparator;

public class MailinglisteDialog extends Dialog {

	private static final int[] columnBounds = new int[] { 60, 200, 300 };

	private TableViewer availableObjectsViewer;

	private List<Kontakt> availableObjects;

	private final String shellText;

	/**
	 * Create the dialog.
	 * 
	 * @param parentShell
	 */
	public MailinglisteDialog(Shell parentShell, String shellText, List<Kontakt> availableObjects) {
		super(parentShell);
		this.shellText = shellText;
		this.availableObjects = availableObjects;
	}

	/**
	 * Create contents of the dialog.
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);
		area.setLayout(new GridLayout(1, false));

		Composite infoComposite = new Composite(area, SWT.NONE);
		GridLayout gridLayout = new GridLayout(1, false);
		gridLayout.horizontalSpacing = 2;
		gridLayout.verticalSpacing = 2;
		gridLayout.marginWidth = 0;
		gridLayout.marginHeight = 0;
		infoComposite.setLayout(gridLayout);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		infoComposite.setLayoutData(gridData);

		ITableViewColumn[] viewColumns = new ITableViewColumn[KontakteView.columnTitles.length];
		IViewerComparator[] comparators = KontakteView.getColumnComparators();
		ColumnLabelProvider[] labelProviders = KontakteView.getColumnLableProviders();
		final String[] columnTitles = KontakteView.columnTitles;
		for (int i = 0; i < columnTitles.length; i++) {
			viewColumns[i] = new ViewColumn(columnTitles[i], columnBounds[i], comparators[i], labelProviders[i]);
		}
		availableObjectsViewer = TableViewerFactory.createTableViewer(infoComposite, new ArrayContentProvider(),
			viewColumns, true);
		{
			GridData gd = new GridData(SWT.LEFT, SWT.TOP, true, true);
			gd.horizontalAlignment = SWT.FILL;
			gd.verticalAlignment = SWT.FILL;
			gd.widthHint = 570;
			gd.heightHint = 450;
			availableObjectsViewer.getControl().setLayoutData(gd);
		}
		availableObjectsViewer.setInput(availableObjects);

		Label lblAnzahl = new Label(infoComposite, SWT.NONE);
		lblAnzahl.setText("Anzahl: " + availableObjects.size());
		{
			GridData gd = new GridData(SWT.LEFT, SWT.TOP, true, false);
			gd.horizontalAlignment = SWT.FILL;
			gd.heightHint = 50;
			lblAnzahl.setLayoutData(gd);
		}
		return area;
	}

	/**
	 * Create contents of the button bar.
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(800, 600);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell pNewShell) {
		pNewShell.setText(shellText);
		super.configureShell(pNewShell);
	}

}
