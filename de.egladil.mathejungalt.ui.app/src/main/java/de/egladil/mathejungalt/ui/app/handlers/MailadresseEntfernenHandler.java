/**
 * 
 */
package de.egladil.mathejungalt.ui.app.handlers;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * @author aheike
 */
public class MailadresseEntfernenHandler extends AbstractSelectionHandler {

	/** */
	private IStammdatenservice stammdatenservice;

	/**
	 * @param pSelectionProvider
	 */
	public MailadresseEntfernenHandler(ISelectionProvider pSelectionProvider, IStammdatenservice pStammdatenservice) {
		super(pSelectionProvider);
		stammdatenservice = pStammdatenservice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.handlers.AbstractSelectionHandler#run()
	 */
	@Override
	protected void run() {
		AbstractMatheAGObject object = getFirstSelectedObject();
		if (!(object instanceof Mitglied)) {
			return;
		}
		Mitglied mitglied = (Mitglied) object;
		Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		try {
			stammdatenservice.mailadresseAusMailinglisteEntfernen(mitglied.getKontakt());
			MessageDialog.openInformation(shell, "Mailingliste", "Kontakt '" + mitglied.getKontakt()
				+ "' aus Mailingliste entfernt.");
		} catch (MatheJungAltException e) {
			MessageDialog.openError(shell, "Fehler Diplom", "Kontakt '" + mitglied.getKontakt()
				+ "' konnte nicht aus Mailingliste entfernt werden \n" + e.getMessage());
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.handlers.AbstractSelectionHandler#isEnabled()
	 */
	@Override
	public boolean isEnabled() {
		if (!super.isEnabled()) {
			return false;
		}
		if (!(getFirstSelectedObject() instanceof Mitglied)) {
			return false;
		}
		Mitglied mitglied = (Mitglied) getFirstSelectedObject();
		if (mitglied.getKontakt().indexOf('@') < 0) {
			return false;
		}
		return stammdatenservice.mailadresseInMailinglisteEnthalten(mitglied.getKontakt());
	}
}
