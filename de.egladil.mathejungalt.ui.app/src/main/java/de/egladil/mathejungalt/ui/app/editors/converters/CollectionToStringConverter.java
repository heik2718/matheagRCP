/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.converters;

import java.util.Collection;

import org.eclipse.core.databinding.conversion.IConverter;

/**
 * @author aheike
 */
public class CollectionToStringConverter implements IConverter {

	/**
	 * 
	 */
	public CollectionToStringConverter() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object convert(Object pArg0) {
		return "" + ((Collection) pArg0).size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getFromType()
	 */
	@Override
	public Object getFromType() {
		return Collection.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getToType()
	 */
	@Override
	public Object getToType() {
		return String.class;
	}

}
