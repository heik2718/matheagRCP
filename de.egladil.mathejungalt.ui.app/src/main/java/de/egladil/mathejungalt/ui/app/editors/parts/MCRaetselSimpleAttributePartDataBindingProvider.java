/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.IMatheAGObjectNames;
import de.egladil.mathejungalt.domain.mcraetsel.IMCArchivraetselNames;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.Urheber;
import de.egladil.mathejungalt.domain.types.MCAufgabenstufen;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.editors.converters.MCAufgabenstufeToStringConverter;
import de.egladil.mathejungalt.ui.app.editors.converters.StringToMCAufgabenstufeConverter;
import de.egladil.mathejungalt.ui.app.editors.converters.StringToUrheberConverter;
import de.egladil.mathejungalt.ui.app.editors.converters.UrheberToStringConverter;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;
import de.egladil.mathejungalt.ui.app.themes.PartControlsFactory;

/**
 * Die Änderungsmöglichkeiten sind für gesperrte Aufgaben eingeschränkt da die generierten Dateien nach der Verwendung
 * der Aufgaben unverndert bleiben sollen.
 *
 * @author winkelv
 */
public class MCRaetselSimpleAttributePartDataBindingProvider extends AbstractSimpleAttributePartDataBindingProvider {

	/** */
	private MCArchivraetsel raetsel;

	private Collection<Urheber> autoren = MatheJungAltActivator.getDefault().getStammdatenservice().getQuizAutoren();

	/**
	 *
	 */
	public MCRaetselSimpleAttributePartDataBindingProvider() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.contentproviders.AbstractAttributePartContentsProvider#anzahlZeilen()
	 */
	@Override
	protected int anzahlZeilen() {
		return 8;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.contentproviders.AbstractAttributePartContentsProvider#initLabelsAndTexts(org.eclipse
	 * .swt.widgets .Composite, org.eclipse.ui.forms.widgets.FormToolkit, boolean)
	 */
	@Override
	protected void initControls(Composite pClient, FormToolkit pToolkit, boolean pEditable,
		final AbstractMatheAGObject pInput) {
		final Control[] controls = getControls();
		Control[] labels = new Control[controls.length];

		raetsel = (MCArchivraetsel) pInput;
		final boolean editable = !raetsel.isVeroeffentlicht();

		int widthHint = 150;
		// ID
		labels[0] = PartControlsFactory.createIdLabel(pClient, pToolkit);
		controls[0] = PartControlsFactory.createIdText(pClient, pToolkit, widthHint);

		// Schluessel
		labels[1] = PartControlsFactory.createSchluesselLabel(pClient, pToolkit);
		controls[1] = PartControlsFactory.createText(pClient, pToolkit, widthHint, editable);
		((Text) controls[1]).setTextLimit(IMCArchivraetselNames.LENGTH_SCHLUESSEL);

		String[] items = null;
		// Stufe
		labels[2] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_STUFE);
		if (editable) {
			items = MCAufgabenstufen.toLabels();
			controls[2] = PartControlsFactory.createCombo(pClient, pToolkit, items, widthHint);
		} else {
			controls[2] = PartControlsFactory.createText(pClient, pToolkit, widthHint, false);
		}

		// Titel
		labels[3] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_TITEL);
		controls[3] = PartControlsFactory.createText(pClient, pToolkit, widthHint, editable);

		// Autor
		labels[4] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_AUTOR);
		if (editable) {
			List<String> names = new ArrayList<String>();
			for (Urheber urheber : autoren) {
				names.add(urheber.getName());
			}
			items = names.toArray(new String[0]);
			controls[4] = PartControlsFactory.createCombo(pClient, pToolkit, items, widthHint);
		} else {
			controls[4] = PartControlsFactory.createText(pClient, pToolkit, widthHint, false);
		}

		labels[5] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_LICENSE_SHORT);
		controls[5] = PartControlsFactory.createText(pClient, pToolkit, widthHint, editable);

		labels[6] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_LICENSE_SHORT);
		controls[6] = PartControlsFactory.createText(pClient, pToolkit, widthHint, editable);

		// privat
		PartControlsFactory.createLabel(pClient, pToolkit, "");
		controls[7] = new Button(pClient, SWT.CHECK);
		((Button) controls[7]).setText(ITextConstants.LABEL_TEXT_PRIVAT);
		((Button) controls[7]).setEnabled(editable);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.contentproviders.IAttributePartContentsProvider#refreshDataBindings(de.egladil.mathe
	 * .core.domain .AbstractMatheAGObject)
	 */
	@Override
	public void refreshDataBindings(AbstractMatheAGObject pInput) {
		super.refreshDataBindings(pInput);

		Control[] controls = getControls();
		DataBindingContext dbc = getDbc();

		IObservableValue observableWidget = SWTObservables.observeText(controls[0], SWT.Modify);
		IObservableValue observableProps = BeansObservables.observeValue(pInput, IMatheAGObjectNames.PROP_ID);
		dbc.bindValue(observableWidget, observableProps, null, null);

		observableWidget = SWTObservables.observeText(controls[1], SWT.Modify);
		observableProps = BeansObservables.observeValue(pInput, IMatheAGObjectNames.PROP_SCHLUESSEL);
		dbc.bindValue(observableWidget, observableProps, null, null);

		// Stufe
		if (controls[2] instanceof Text) {
			observableWidget = SWTObservables.observeText(controls[2], SWT.Modify);
		} else {
			observableWidget = SWTObservables.observeText(controls[2]);
		}
		observableProps = BeansObservables.observeValue(pInput, IMCArchivraetselNames.PROP_STUFE);
		UpdateValueStrategy targetToModelStrategy = new UpdateValueStrategy();
		targetToModelStrategy.setConverter(new StringToMCAufgabenstufeConverter());
		UpdateValueStrategy modelToTargetStrategy = new UpdateValueStrategy();
		modelToTargetStrategy.setConverter(new MCAufgabenstufeToStringConverter());
		getDbc().bindValue(observableWidget, observableProps, targetToModelStrategy, modelToTargetStrategy);

		{
			// Titel
			observableWidget = SWTObservables.observeText(controls[3], SWT.Modify);
			observableProps = BeansObservables.observeValue(pInput, IMCArchivraetselNames.PROP_TITEL);
			dbc.bindValue(observableWidget, observableProps);
		}

		{
			// Autor
			if (controls[2] instanceof Text) {
				observableWidget = SWTObservables.observeText(controls[4], SWT.Modify);
			} else {
				observableWidget = SWTObservables.observeText(controls[4]);
			}
			observableProps = BeansObservables.observeValue(pInput, IMCArchivraetselNames.PROP_AUTOR);
			targetToModelStrategy = new UpdateValueStrategy();
			targetToModelStrategy.setConverter(new StringToUrheberConverter(autoren));
			modelToTargetStrategy = new UpdateValueStrategy();
			modelToTargetStrategy.setConverter(new UrheberToStringConverter());
			getDbc().bindValue(observableWidget, observableProps, targetToModelStrategy, modelToTargetStrategy);
		}

		{
			// License Short
			observableWidget = SWTObservables.observeText(controls[5], SWT.Modify);
			observableProps = BeansObservables.observeValue(pInput, IMCArchivraetselNames.PROP_LICENSE_SHORT);
			dbc.bindValue(observableWidget, observableProps);

			// License Full
			observableWidget = SWTObservables.observeText(controls[6], SWT.Modify);
			observableProps = BeansObservables.observeValue(pInput, IMCArchivraetselNames.PROP_LICENSE_FULL);
			dbc.bindValue(observableWidget, observableProps);
		}

		{
			observableWidget = SWTObservables.observeSelection(controls[7]);
			observableProps = BeansObservables.observeValue(pInput, IMCArchivraetselNames.PROP_PRIVAT);
			dbc.bindValue(observableWidget, observableProps);
		}
	}
}
