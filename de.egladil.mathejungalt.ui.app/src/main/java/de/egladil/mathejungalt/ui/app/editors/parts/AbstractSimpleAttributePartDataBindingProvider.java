/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.core.databinding.AggregateValidationStatus;
import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.FormToolkit;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.themes.IColorKeys;
import de.egladil.mathejungalt.ui.app.themes.PartControlsFactory;

/**
 * Abstrakte Oberklasse, die die Implementierung für die Standarfälle beinhaltet. Standard ist ein zweispaltiges Control
 * mit Labels und Text-Inputs.
 * 
 * @author winkelv
 */
public abstract class AbstractSimpleAttributePartDataBindingProvider implements
	ISimpleAttributePartDataBindingProvider, PropertyChangeListener {

	/** */
	private Control[] texts;

	/** */
	private Binding[] bindings;

	/** */
	private DataBindingContext dbc;

	/** */
	private Label errorLabel;

	/**
	 * 
	 */
	public AbstractSimpleAttributePartDataBindingProvider() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.contentproviders.IAttributePartContentsProvider#init(org.eclipse.swt.widgets.Composite
	 * , org.eclipse.ui.forms.widgets.FormToolkit, boolean)
	 */
	@Override
	public void init(Composite pClient, FormToolkit pToolkit, boolean pEditable, AbstractMatheAGObject pInput) {
		texts = new Control[anzahlZeilen()];
		bindings = new Binding[anzahlZeilen()];
		initControls(pClient, pToolkit, pEditable, pInput);
		errorLabel = PartControlsFactory.createMessageLabel(pClient, pToolkit, 2);
		pInput.addPropertyChangeListener(this);
		pInput.addPropertyChangeListener(MatheJungAltActivator.getDefault().getStammdatenservice());
	}

	/**
	 * @param pClient
	 * @param pToolkit
	 * @param pEditable
	 * @param pInput
	 */
	protected abstract void initControls(Composite pClient, FormToolkit pToolkit, boolean pEditable,
		AbstractMatheAGObject pInput);

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent pEvt) {
		// die Standardimplementierung ist leer.
	}

	/**
	 * @return int die Anzahl der Zeilen.
	 */
	protected abstract int anzahlZeilen();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.contentproviders.IAttributePartContentsProvider#getBindings()
	 */
	@Override
	public Binding[] getBindings() {
		return bindings;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.contentproviders.IAttributePartContentsProvider#getDbc()
	 */
	@Override
	public DataBindingContext getDbc() {
		return dbc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.contentproviders.IAttributePartContentsProvider#refreshDataBindings(de.egladil.mathe
	 * .core.domain .AbstractMatheAGObject)
	 */
	@Override
	public void refreshDataBindings(AbstractMatheAGObject pInput) {
		if (pInput == null) {
			return;
		}

		dbc = new DataBindingContext();
		bindings = new Binding[anzahlZeilen()];

		dbc.bindValue(SWTObservables.observeText(errorLabel), new AggregateValidationStatus(dbc.getBindings(),
			AggregateValidationStatus.MAX_SEVERITY), null, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.rcp.editors.parts.ISimpleAttributePartControlsProvider#dispose(de.egladil.mathe.core.domain.
	 * AbstractMatheAGObject)
	 */
	@Override
	public void dispose(AbstractMatheAGObject pInput) {
		if (pInput != null) {
			pInput.removePropertyChangeListener(this);
			pInput.removePropertyChangeListener(MatheJungAltActivator.getDefault().getStammdatenservice());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.ISimpleAttributePartDataBindingProvider#getTexts()
	 */
	@Override
	public Control[] getControls() {
		return texts;
	}

	/**
	 * Ändert die Editierbarkeit der Eingabefelder.
	 * 
	 * @param pEditable
	 */
	protected void refreshEditableStatus(boolean pEditable) {
		Control[] controls = getControls();

		Color background = PlatformUI.getWorkbench().getThemeManager().getCurrentTheme().getColorRegistry()
			.get(IColorKeys.WHITE);
		if (!pEditable) {
			background = PlatformUI.getWorkbench().getThemeManager().getCurrentTheme().getColorRegistry()
				.get(IColorKeys.LIGHT_GREY);
		}

		for (int i = 1; i < controls.length - 1; i++) {
			((Text) controls[i]).setEditable(pEditable);
			controls[i].setBackground(background);
			controls[i].redraw();
		}
	}
}
