/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.IMatheAGObjectNames;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgaben.IAufgabeNames;
import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenart;
import de.egladil.mathejungalt.domain.types.EnumTypes.Quellenart;
import de.egladil.mathejungalt.ui.app.editors.converters.AufgabenartToStringConverter;
import de.egladil.mathejungalt.ui.app.editors.converters.JahreszeitToStringConverter;
import de.egladil.mathejungalt.ui.app.editors.converters.StringToAufgabenartConverter;
import de.egladil.mathejungalt.ui.app.editors.converters.StringToJahreszeitConverter;
import de.egladil.mathejungalt.ui.app.editors.converters.StringToThemaConverter;
import de.egladil.mathejungalt.ui.app.editors.converters.ThemaToStringConverter;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;
import de.egladil.mathejungalt.ui.app.themes.PartControlsFactory;

/**
 * Die Änderungsmöglichkeiten sind für gesperrte Aufgaben eingeschränkt da die generierten Dateien nach der Verwendung
 * der Aufgaben unverndert bleiben sollen.
 *
 * @author winkelv
 */
public class AufgabeSimpleAttributePartDataBindingProvider extends AbstractSimpleAttributePartDataBindingProvider {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(AufgabeSimpleAttributePartDataBindingProvider.class);

	/** */
	private Aufgabe aufgabe;

	/**
	 *
	 */
	public AufgabeSimpleAttributePartDataBindingProvider() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.contentproviders.AbstractAttributePartContentsProvider#anzahlZeilen()
	 */
	@Override
	protected int anzahlZeilen() {
		return 7;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.contentproviders.AbstractAttributePartContentsProvider#initLabelsAndTexts(org.eclipse
	 * .swt.widgets .Composite, org.eclipse.ui.forms.widgets.FormToolkit, boolean)
	 */
	@Override
	protected void initControls(Composite pClient, FormToolkit pToolkit, boolean pEditable,
		final AbstractMatheAGObject pInput) {
		final Control[] controls = getControls();
		Control[] labels = new Control[controls.length];

		aufgabe = (Aufgabe) pInput;
		final boolean editable = aufgabe.getAnzahlSperrendeReferenzen() == 0;

		int widthHint = 150;
		labels[0] = PartControlsFactory.createIdLabel(pClient, pToolkit);
		controls[0] = PartControlsFactory.createIdText(pClient, pToolkit, widthHint);

		labels[1] = PartControlsFactory.createSchluesselLabel(pClient, pToolkit);
		controls[1] = PartControlsFactory.createText(pClient, pToolkit, widthHint, editable);
		((Text) controls[1]).setTextLimit(IAufgabeNames.LENGTH_SCHLUESSEL);

		String[] items = null;
		labels[2] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_ART);
		if (editable) {
			items = new String[] { ITextConstants.AUFGABENART_EIGEN, ITextConstants.AUFGABENART_NACH,
				ITextConstants.AUFGABENART_ZITAT };
			controls[2] = PartControlsFactory.createCombo(pClient, pToolkit, items, widthHint);
		} else {
			controls[2] = PartControlsFactory.createText(pClient, pToolkit, widthHint, false);
		}

		labels[3] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_THEMA);
		if (editable) {
			items = new String[] { ITextConstants.THEMA_ARITHMETIK, ITextConstants.THEMA_GEOMETRIE,
				ITextConstants.THEMA_LOGIK, ITextConstants.THEMA_KOMBINATORIK, ITextConstants.THEMA_WAHRSCHEINLICHKEIT,
				ITextConstants.THEMA_ZAHLENTHEORIE };
			controls[3] = PartControlsFactory.createCombo(pClient, pToolkit, items, widthHint);
		} else {
			controls[3] = PartControlsFactory.createText(pClient, pToolkit, widthHint, false);
		}

		labels[4] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_JAHRESZEIT);
		if (editable) {
			items = new String[] { ITextConstants.JAHRESZEIT_IMMER, ITextConstants.JAHRESZEIT_FRUEH,
				ITextConstants.JAHRESZEIT_KARNEVAL, ITextConstants.JAHRESZEIT_OSTERN,
				ITextConstants.JAHRESZEIT_SOMMERR, ITextConstants.JAHRESZEIT_HERBST, ITextConstants.JAHRESZEIT_WINTER,
				ITextConstants.JAHRESZEIT_XMAS };
			controls[4] = PartControlsFactory.createCombo(pClient, pToolkit, items, widthHint);
		} else {
			controls[4] = PartControlsFactory.createText(pClient, pToolkit, widthHint, false);
		}

		labels[5] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_TITEL);
		controls[5] = PartControlsFactory.createText(pClient, pToolkit, widthHint, pEditable);
		((Text) controls[5]).setTextLimit(IAufgabeNames.LENGTH_TITEL);

		labels[6] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_BESCHREIBUNG);
		controls[6] = PartControlsFactory.createTextArea(pClient, pToolkit, widthHint, pEditable);
		((Text) controls[6]).setTextLimit(IAufgabeNames.LENGTH_BESCHREIBUNG);

		String msg = "";
		if (aufgabe.getQuelle().getArt().equals(Quellenart.N) && !(Aufgabenart.E.equals(aufgabe.getArt()))
			|| !(aufgabe.getQuelle().getArt().equals(Quellenart.N)) && Aufgabenart.E.equals(aufgabe.getArt())) {
			msg = "Die Aufgabenart muss noch ge\u00E4ndert werden!";
			LOG.info(msg);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.contentproviders.IAttributePartContentsProvider#refreshDataBindings(de.egladil.mathe
	 * .core.domain .AbstractMatheAGObject)
	 */
	@Override
	public void refreshDataBindings(AbstractMatheAGObject pInput) {
		super.refreshDataBindings(pInput);

		Binding[] bindings = getBindings();
		Control[] controls = getControls();
		DataBindingContext dbc = getDbc();

		IObservableValue observableWidget = SWTObservables.observeText(controls[0], SWT.Modify);
		IObservableValue observableProps = BeansObservables.observeValue(pInput, IMatheAGObjectNames.PROP_ID);
		bindings[0] = dbc.bindValue(observableWidget, observableProps, null, null);

		observableWidget = SWTObservables.observeText(controls[1], SWT.Modify);
		observableProps = BeansObservables.observeValue(pInput, IMatheAGObjectNames.PROP_SCHLUESSEL);
		bindings[1] = dbc.bindValue(observableWidget, observableProps, null, null);

		if (controls[2] instanceof Text) {
			observableWidget = SWTObservables.observeText(controls[2], SWT.Modify);
		} else {
			observableWidget = SWTObservables.observeText(controls[2]);
		}
		observableProps = BeansObservables.observeValue(pInput, IAufgabeNames.PROP_ART);
		UpdateValueStrategy targetToModelStrategy = new UpdateValueStrategy();
		targetToModelStrategy.setConverter(new StringToAufgabenartConverter());
		UpdateValueStrategy modelToTargetStrategy = new UpdateValueStrategy();
		modelToTargetStrategy.setConverter(new AufgabenartToStringConverter());
		bindings[2] = getDbc().bindValue(observableWidget, observableProps, targetToModelStrategy,
			modelToTargetStrategy);

		if (controls[3] instanceof Text) {
			observableWidget = SWTObservables.observeText(controls[3], SWT.Modify);
		} else {
			observableWidget = SWTObservables.observeText(controls[3]);
		}
		observableProps = BeansObservables.observeValue(pInput, IAufgabeNames.PROP_THEMA);
		targetToModelStrategy = new UpdateValueStrategy();
		targetToModelStrategy.setConverter(new StringToThemaConverter());
		modelToTargetStrategy = new UpdateValueStrategy();
		modelToTargetStrategy.setConverter(new ThemaToStringConverter());
		bindings[3] = dbc.bindValue(observableWidget, observableProps, targetToModelStrategy, modelToTargetStrategy);

		if (controls[4] instanceof Text) {
			observableWidget = SWTObservables.observeText(controls[4], SWT.Modify);
		} else {
			observableWidget = SWTObservables.observeText(controls[4]);
		}
		observableProps = BeansObservables.observeValue(pInput, IAufgabeNames.PROP_JAHRESZEIT);
		targetToModelStrategy = new UpdateValueStrategy();
		targetToModelStrategy.setConverter(new StringToJahreszeitConverter());
		modelToTargetStrategy = new UpdateValueStrategy();
		modelToTargetStrategy.setConverter(new JahreszeitToStringConverter());
		bindings[4] = dbc.bindValue(observableWidget, observableProps, targetToModelStrategy, modelToTargetStrategy);

		observableWidget = SWTObservables.observeText(controls[5], SWT.Modify);
		observableProps = BeansObservables.observeValue(pInput, IAufgabeNames.PROP_TITEL);
		bindings[5] = dbc.bindValue(observableWidget, observableProps, null, null);

		observableWidget = SWTObservables.observeText(controls[6], SWT.Modify);
		observableProps = BeansObservables.observeValue(pInput, IAufgabeNames.PROP_BESCHREIBUNG);
		bindings[6] = dbc.bindValue(observableWidget, observableProps, null, null);
	}
}
