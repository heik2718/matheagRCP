/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.handlers.IHandlerService;

import de.egladil.mathejungalt.domain.schulen.MinikaenguruTeilnahme;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.editors.listeners.MinikaenguruTeilnahmenViewerListener;
import de.egladil.mathejungalt.ui.app.handlers.ICommandIds;
import de.egladil.mathejungalt.ui.app.handlers.MinikaenguruMailsPflegenHandler;
import de.egladil.mathejungalt.ui.app.themes.IImageKeys;
import de.egladil.mathejungalt.ui.app.themes.TableViewerFactory;
import de.egladil.mathejungalt.ui.app.views.ITableViewColumn;
import de.egladil.mathejungalt.ui.app.views.ViewColumn;
import de.egladil.mathejungalt.ui.app.views.contentproviders.MiniteilnahmenViewContentProvider;

/**
 * Der Teil des MasterDetailsBlocks, der die Minikaenguruteilnahmen auflistet.
 * 
 * @author Heike Winkelvoß
 */
public class MiniteilnahmenViewerPartWithButtons extends AbstractViewerPartWithButtons {

	/** */
	private int[] columnBounds = new int[] { 100 };

	private String[] columnTitles = new String[] { "Minik\u00e4nguru" };

	/**
	 * @param pParentPart
	 */
	public MiniteilnahmenViewerPartWithButtons(AbstractMasterDetailsBlockWithViewerPart pParentPart) {
		super(pParentPart);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractViewerPartWithButtons#create(org.eclipse.swt.widgets.Composite,
	 * org.eclipse.ui.forms.widgets.FormToolkit)
	 */
	@Override
	public void init(final Composite pParent, final FormToolkit pToolkit) {
		super.init(pParent, pToolkit);
		addStructuredViewerListener(new MinikaenguruTeilnahmenViewerListener(this));
		activateHandlers();
	}

	/**
   * 
   */
	private void activateHandlers() {
		IHandlerService handlerService = (IHandlerService) PlatformUI.getWorkbench().getService(IHandlerService.class);
		handlerService.activateHandler(ICommandIds.MAILINGLISTE_PFLEGEN, new MinikaenguruMailsPflegenHandler(
			getViewer()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractViewerPartWithButtons#createTableViewer()
	 */
	@Override
	protected TableViewer createTableViewer(Composite pContainer) {
		ITableViewColumn[] viewColumns = new ITableViewColumn[columnTitles.length];
		ColumnLabelProvider[] labelProviders = getColumnLableProviders();
		for (int i = 0; i < columnTitles.length; i++) {
			viewColumns[i] = new ViewColumn(columnTitles[i], columnBounds[i], labelProviders[i]);
		}
		return TableViewerFactory.createTableViewer(pContainer, new MiniteilnahmenViewContentProvider(
			MatheJungAltActivator.getDefault().getStammdatenservice()), viewColumns, false);
	}

	/**
	 * @return
	 */
	private ColumnLabelProvider[] getColumnLableProviders() {
		ColumnLabelProvider[] labelProviders = new ColumnLabelProvider[columnBounds.length];
		labelProviders[0] = new ColumnLabelProvider() {
			@Override
			public String getText(Object pElement) {
				return ((MinikaenguruTeilnahme) pElement).getMinikaenguru().getJahr().toString();
			}

			@Override
			public Image getImage(Object pElement) {
				return MatheJungAltActivator.getDefault().getImage(IImageKeys.MINIKAENGURU);
			}
		};
		return labelProviders;
	}
}
