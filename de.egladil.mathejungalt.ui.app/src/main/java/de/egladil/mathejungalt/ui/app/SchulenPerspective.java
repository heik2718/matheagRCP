package de.egladil.mathejungalt.ui.app;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

import de.egladil.mathejungalt.ui.app.views.KontakteView;
import de.egladil.mathejungalt.ui.app.views.MinikaenguruView;
import de.egladil.mathejungalt.ui.app.views.SchulenView;

/**
 * @author aheike
 */
public class SchulenPerspective implements IPerspectiveFactory {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IPerspectiveFactory#createInitialLayout(org.eclipse.ui.IPageLayout)
	 */
	public void createInitialLayout(IPageLayout layout) {
		String editorArea = layout.getEditorArea();
		layout.setEditorAreaVisible(true);
		layout.setFixed(false);
		layout.setEditorAreaVisible(true);
		IFolderLayout folder = layout.createFolder("schulenViewsFolder", IPageLayout.LEFT, 0.3f, editorArea);
		folder.addView(SchulenView.ID);
		folder.addView(KontakteView.ID);
		folder.addView(MinikaenguruView.ID);
	}
}
