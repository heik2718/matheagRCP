/**
 * 
 */
package de.egladil.mathejungalt.ui.app.dialogs;

import java.util.ArrayList;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardSelectionPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;

import de.egladil.mathejungalt.domain.quellen.AbstractQuelle;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.dialogs.contentproviders.QuellenWizardsTableContentProvider;
import de.egladil.mathejungalt.ui.app.themes.IImageKeys;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * {@link WizardSelectionPage} zur erzeugung einer neuen Quelle. Hier werden die verf�gbaren Quellenarten mit ihrem
 * jeweiligen Wizard hinterlegt.
 * 
 * @author Winkelv
 */
public class CreateQuelleWizardSelectionPage extends WizardSelectionPage {

	/** */
	private AbstractQuelle quelle;

	private IStammdatenservice stammdatenservice;

	/**
	 * @param pPageName
	 */
	public CreateQuelleWizardSelectionPage(IStammdatenservice pStammdatenservice) {
		super(ITextConstants.QUELLE_WIZARD_SELECTION_PAGE_NAME);
		setTitle(ITextConstants.QUELLE_WIZARD_SELECTION_PAGE_TITLE);
		setImageDescriptor(MatheJungAltActivator.imageDescriptorFromPlugin(MatheJungAltActivator.PLUGIN_ID,
			IImageKeys.QUELLE_WIZARD));
		stammdatenservice = pStammdatenservice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createControl(Composite pParent) {
		Composite container = new Composite(pParent, SWT.RESIZE);
		container.setLayout(new GridLayout(1, false));
		Font wizardFond = pParent.getFont();

		final Label label = new Label(container, SWT.NONE);
		label.setFont(wizardFond);
		label.setText(ITextConstants.QUELLE_WIZARD_SELECTION_PAGE_DESCR);

		Group group = new Group(container, SWT.NONE);
		group.setLayout(new GridLayout(1, false));
		group.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		final Label availableWizadrsLabel = new Label(container, SWT.NONE);
		availableWizadrsLabel.setFont(wizardFond);
		availableWizadrsLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));

		final Table table = new Table(group, SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		table.setFont(wizardFond);
		table.setLinesVisible(false);
		table.setHeaderVisible(false);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		TableViewer viewer = new TableViewer(table);
		viewer.setContentProvider(new QuellenWizardsTableContentProvider(this, stammdatenservice));
		viewer.setLabelProvider(new LabelProvider() {
			@Override
			public Image getImage(Object pElement) {
				if (pElement instanceof QuellenartWizardNode) {
					QuellenartWizardNode node = (QuellenartWizardNode) pElement;
					switch (node.getQuellenart()) {
					case K:
						return MatheJungAltActivator.getDefault().getImage(IImageKeys.MITGLIED);
					case G:
						return MatheJungAltActivator.getDefault().getImage(IImageKeys.MITGLIED);
					case B:
						return MatheJungAltActivator.getDefault().getImage(IImageKeys.BUCH);
					case Z:
						return MatheJungAltActivator.getDefault().getImage(IImageKeys.ZEITSCHRIFT);
					default:
						return MatheJungAltActivator.getDefault().getImage(IImageKeys.QUELLE_16);
					}
				}
				return super.getImage(pElement);
			}
		});

		viewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent pEvent) {
				if (!(pEvent.getSelection() instanceof IStructuredSelection)) {
					setSelectedNode(null);
					return;
				}
				IStructuredSelection sel = (IStructuredSelection) pEvent.getSelection();
				if (sel.isEmpty()) {
					setSelectedNode(null);
				} else {
					QuellenartWizardNode node = (QuellenartWizardNode) sel.getFirstElement();
					setDescription(node.toString());
					setSelectedNode(node);
				}
			}
		});

		viewer.getTable().addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetDefaultSelected(SelectionEvent pE) {
				if (canFlipToNextPage()) {
					IWizardPage nextPage = getNextPage();
					if (nextPage != null) {
						getContainer().showPage(nextPage);
					}
				}
			}
		});

		viewer.setInput(new ArrayList<QuellenartWizardNode>());
		setControl(container);
	}

	/**
	 * @return
	 */
	public final AbstractQuelle getQuelle() {
		return quelle;
	}

	/**
	 * @param pQuelle the quelle to set
	 */
	public final void setQuelle(AbstractQuelle pQuelle) {
		quelle = pQuelle;
	}
}
