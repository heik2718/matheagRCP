/**
 * 
 */
package de.egladil.mathejungalt.ui.app.dialogs;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;

import de.egladil.mathejungalt.domain.medien.Buch;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.dialogs.contentproviders.SerienteilnahmenDialogContentProvider;
import de.egladil.mathejungalt.ui.app.themes.IImageKeys;

/**
 * Dialog zur Auswahl von Autoren.
 * 
 * @author winkelv
 */
public class SerieSelectionDialog extends ElementListSelectionDialog {

	/**
	 * �ffnet Serienliste, die alle Serien enthält, an denen das Mitglied noch nicht beteiligt war.
	 * 
	 * @param pParent
	 * @param pLabelProvider
	 * @param pBuch {@link Buch} ein Buch
	 */
	public SerieSelectionDialog(Shell pParent, ILabelProvider pLabelProvider, final Mitglied pMitglied) {
		super(pParent, pLabelProvider);
		createContents(pMitglied);
	}

	/**
	 * @param pMitglied
	 */
	private void createContents(final Mitglied pMitglied) {
		SerienteilnahmenDialogContentProvider contentsProvider = new SerienteilnahmenDialogContentProvider(pMitglied);
		setElements(contentsProvider.getElements());
		setMultipleSelection(false);
		setIgnoreCase(false);
		setImage(MatheJungAltActivator.getDefault().getImage(IImageKeys.SERIEN));
		setAllowDuplicates(false);
		setMessage("Serie w�hlen");
		setTitle("Serienliste");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.dialogs.ElementListSelectionDialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite pParent) {
		Control control = super.createDialogArea(pParent);
		getShell().setText("ShellText");
		return control;
	}
}
