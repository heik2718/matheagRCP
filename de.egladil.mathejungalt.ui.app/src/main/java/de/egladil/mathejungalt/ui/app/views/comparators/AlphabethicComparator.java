/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.comparators;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.AssertionFailedException;

import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.ui.app.views.ArbeitsblaetterView;

/**
 * @author Winkelv
 */
public class AlphabethicComparator extends AbstractViewerComparator {

	/**
	 * @param pIndex
	 */
	public AlphabethicComparator() {
		super();
	}

	/**
	 * @param pIndex
	 * @param pType
	 */
	public AlphabethicComparator(int pIndex, String pType) {
		super(pIndex, pType);
	}

	/**
	 * @param pIndex
	 */
	public AlphabethicComparator(int pIndex) {
		super(pIndex);
	}

	/**
	 * @param pType
	 */
	public AlphabethicComparator(String pType) {
		super(pType);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Object pO1, Object pO2) {
		if (getType() == null) {
			if (pO1 == null && pO2 == null) {
				return 0;
			}
			if (pO1 == null && pO2 != null) {
				return -1;
			}
			if (pO1 != null && pO2 == null) {
				return 1;
			}
			return pO1.toString().compareToIgnoreCase(pO2.toString());
		}
		if (Arbeitsblatt.class.getName().equals(getType())) {
			Assert.isTrue(getIndex() >= 0, "Wenn der type gesetzt ist, muss auch der Index gesetzt sein");
			switch (getIndex()) {
			case ArbeitsblaetterView.INDEX_SCHLUESSEL:
				return ((Arbeitsblatt) pO1).getSchluessel().compareTo(((Arbeitsblatt) pO2).getSchluessel());
			default:
				return ((Arbeitsblatt) pO1).getTitel().compareToIgnoreCase(((Arbeitsblatt) pO2).getTitel());
			}
		}
		throw new AssertionFailedException("assertion failed: Der type ist nicht gesetzt"); //$NON-NLS-1$
	}
}
