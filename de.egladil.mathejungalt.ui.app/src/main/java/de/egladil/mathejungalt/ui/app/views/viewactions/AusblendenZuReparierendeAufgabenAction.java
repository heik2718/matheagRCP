/**
 *
 */
package de.egladil.mathejungalt.ui.app.views.viewactions;

import org.eclipse.jface.viewers.StructuredViewer;

import de.egladil.mathejungalt.ui.app.views.filters.AbstractMatheAGObjectFilter;
import de.egladil.mathejungalt.ui.app.views.filters.AufgabeSchlechtNegativeFilter;

/**
 * @author winkelv
 */
public class AusblendenZuReparierendeAufgabenAction extends AbstractAusblendenAufgabenAction {

	public final static String ID = "de.egladil.mathejungalt.ui.app.views.viewactions.AusblendenZuReparierendeAufgabenAction";

	/**
	 *
	 */
	public AusblendenZuReparierendeAufgabenAction() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.rcp.views.actions.AbstractAusblendenAufgabenAction#getFilter(de.egladil.mathe.rcp.views.
	 * AbstractMatheAGStructuredView)
	 */
	@Override
	protected AbstractMatheAGObjectFilter getFilter(StructuredViewer pView) {
		return new AufgabeSchlechtNegativeFilter(pView, getStammdatenservice());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.views.actions.AbstractAusblendenAufgabenAction#getSpecialPattern()
	 */
	@Override
	protected String getSpecialPattern() {
		return "J";
	}
}
