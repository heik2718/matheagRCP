/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.documentproviders;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableContext;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentListener;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPathEditorInput;
import org.eclipse.ui.texteditor.AbstractDocumentProvider;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.ui.app.editors.AufgabeLaTeXEditor;

/**
 * @author Winkelv
 */
public class SimpleDocumentProvider extends AbstractDocumentProvider implements IDocumentListener {

	/** */
	private AufgabeLaTeXEditor editor;

	/** */
	private IDocument document;

	/**
	 * 
	 */
	public SimpleDocumentProvider(AufgabeLaTeXEditor pEditor) {
		super();
		editor = pEditor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.texteditor.AbstractDocumentProvider#createAnnotationModel(java.lang.Object)
	 */
	@Override
	protected IAnnotationModel createAnnotationModel(Object pElement) throws CoreException {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.texteditor.AbstractDocumentProvider#createDocument(java.lang.Object)
	 */
	@Override
	protected IDocument createDocument(Object pElement) throws CoreException {
		if (pElement instanceof IEditorInput) {
			document = new Document();
			if (setDocumentContent((IEditorInput) pElement)) {
				setupDocument();
			}
			return document;
		}
		return null;
	}

	/**
	 * 
	 */
	protected void setupDocument() {
		document.addDocumentListener(this);
	}

	/**
	 * @param pDocument
	 * @param pInput
	 * @return
	 * @throws CoreException
	 */
	private boolean setDocumentContent(IEditorInput pInput) throws CoreException {
		Reader reader = null;
		try {
			if (pInput instanceof IPathEditorInput) {
				reader = new FileReader(((IPathEditorInput) pInput).getPath().toFile());
			} else {
				return false;
			}
		} catch (FileNotFoundException e) {
			// falls es die Datei nicht gibt, wird ein leeres Dokument angelegt und sp�ter gespeichert
			return true;
		}
		try {
			setDocumentContent(reader);
			return true;
		} catch (IOException e) {
			throw new MatheJungAltException("error when reading file", e);
		}
	}

	/**
	 * @param pDocument
	 * @param pReader
	 */
	private void setDocumentContent(Reader pReader) throws IOException {
		Reader in = new BufferedReader(pReader);
		try {
			StringBuffer buffer = new StringBuffer(512);
			char[] readBuffer = new char[512];
			int n = in.read(readBuffer);
			while (n > 0) {
				buffer.append(readBuffer, 0, n);
				n = in.read(readBuffer);
			}
			document.set(buffer.toString());
		} finally {
			in.close();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.texteditor.AbstractDocumentProvider#doSaveDocument(org.eclipse.core.runtime.IProgressMonitor,
	 * java.lang.Object, org.eclipse.jface.text.IDocument, boolean)
	 */
	@Override
	protected void doSaveDocument(IProgressMonitor pMonitor, Object pElement, IDocument pDocument, boolean pOverwrite)
		throws CoreException {
		if (pElement instanceof IPathEditorInput) {
			IPathEditorInput pathEditorInput = (IPathEditorInput) pElement;
			IPath path = pathEditorInput.getPath();
			File file = path.toFile();
			try {
				file.createNewFile();
				if (file.exists()) {
					if (file.canWrite()) {
						Writer writer = new FileWriter(file);
						writeDocumentContent(pDocument, writer, pMonitor);
					} else {
						throw new MatheJungAltException("cannot overwrite file.");
					}
				} else {
					throw new MatheJungAltException("unable to create writable file");
				}
			} catch (IOException e) {
				throw new MatheJungAltException("error when saving file", e);
			}
		}

	}

	/**
	 * @param pDocument
	 * @param pWriter
	 * @param pMonitor
	 * @throws IOException
	 */
	private void writeDocumentContent(IDocument pDocument, Writer pWriter, IProgressMonitor pMonitor)
		throws IOException {
		Writer out = new BufferedWriter(pWriter);
		try {
			out.write(pDocument.get());
		} finally {
			out.close();
			editor.setModified(false);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.texteditor.AbstractDocumentProvider#getOperationRunner(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	protected IRunnableContext getOperationRunner(IProgressMonitor pMonitor) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.texteditor.AbstractDocumentProvider#isModifiable(java.lang.Object)
	 */
	@Override
	public boolean isModifiable(Object pElement) {
		if (pElement instanceof IPathEditorInput) {
			IPathEditorInput pathEditorInput = (IPathEditorInput) pElement;
			File file = pathEditorInput.getPath().toFile();
			return file.canWrite() || !file.exists();
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.texteditor.AbstractDocumentProvider#isReadOnly(java.lang.Object)
	 */
	@Override
	public boolean isReadOnly(Object pElement) {
		return !isModifiable(pElement);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.texteditor.AbstractDocumentProvider#isStateValidated(java.lang.Object)
	 */
	@Override
	public boolean isStateValidated(Object pElement) {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.text.IDocumentListener#documentAboutToBeChanged(org.eclipse.jface.text.DocumentEvent)
	 */
	@Override
	public void documentAboutToBeChanged(DocumentEvent pEvent) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.text.IDocumentListener#documentChanged(org.eclipse.jface.text.DocumentEvent)
	 */
	@Override
	public void documentChanged(DocumentEvent pEvent) {
		editor.setModified(true);
	}
}
