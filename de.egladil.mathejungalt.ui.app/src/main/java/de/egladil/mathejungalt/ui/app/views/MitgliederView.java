package de.egladil.mathejungalt.ui.app.views;

import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.IHandlerService;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.editors.MitgliedEditor;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.AbstractMatheEditorInput;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.MitgliedEditorInput;
import de.egladil.mathejungalt.ui.app.handlers.CheckMitgliedseitengeneratableHandler;
import de.egladil.mathejungalt.ui.app.handlers.DiplomGebenHandler;
import de.egladil.mathejungalt.ui.app.handlers.GenerierenHandler;
import de.egladil.mathejungalt.ui.app.handlers.ICommandIds;
import de.egladil.mathejungalt.ui.app.handlers.MailadresseEntfernenHandler;
import de.egladil.mathejungalt.ui.app.handlers.MailadresseHinzufuegenHandler;
import de.egladil.mathejungalt.ui.app.handlers.MitgliedDeaktivierenHandler;
import de.egladil.mathejungalt.ui.app.handlers.ObjektAnlegenHandler;
import de.egladil.mathejungalt.ui.app.themes.IImageKeys;
import de.egladil.mathejungalt.ui.app.themes.TableViewerFactory;
import de.egladil.mathejungalt.ui.app.views.comparators.AbstractViewerComparator;
import de.egladil.mathejungalt.ui.app.views.comparators.IViewerComparator;
import de.egladil.mathejungalt.ui.app.views.comparators.MitgliederViewerComparator;
import de.egladil.mathejungalt.ui.app.views.contentproviders.MitgliederViewContentProvider;
import de.egladil.mathejungalt.ui.app.views.labelproviders.CommonColumnLabelProvider;
import de.egladil.mathejungalt.ui.app.views.labelproviders.MitgliederLabelProvider;
import de.egladil.mathejungalt.ui.app.views.listeners.AbstractModelListener;
import de.egladil.mathejungalt.ui.app.views.listeners.MitgliederModelListener;

/**
 * @author aheike
 */
public class MitgliederView extends AbstractMatheJungAltView {

	/** */
	public static final String ID = "de.egladil.mathejungalt.ui.app.mitgliederview";

	/** */
	private String[] columnTitles = new String[] { "Vorname", "Nachname", "Klasse", "Alter", "Kontakt" };

	/** */
	private int[] columnBounds = new int[] { 80, 80, 40, 40, 200 };

	private class MitgliedTableLabelProvider extends LabelProvider implements ITableLabelProvider {

		/*
		 * (non-Javadoc)
		 *
		 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object, int)
		 */
		@Override
		public Image getColumnImage(final Object pElement, final int pColumnIndex) {
			final ImageRegistry imageRegistry = MatheJungAltActivator.getDefault().getImageRegistry();
			if (pColumnIndex == 0 && (pElement instanceof Mitglied)) {
				return ((Mitglied) pElement).isAktiv() ? imageRegistry.get(IImageKeys.MITGLIED_AKTIV) : imageRegistry
					.get(IImageKeys.MITGLIED_NICHT_AKTIV);
			}
			return PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJ_ELEMENT);
		}

		/*
		 * (non-Javadoc)
		 *
		 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object, int)
		 */
		@Override
		public String getColumnText(final Object pElement, final int pColumnIndex) {
			// TODO Auto-generated method stub
			return null;
		}

	};

	/**
	 * Erzeugen programmatisch die benötigten Handler. Diese sind aktiv nur wenn ein Vergleichsobjekt oder ein
	 * Ausschnitt ausgewählt wurde.
	 */
	@Override
	protected void createHandlers() {
		super.createHandlers();
		final IHandlerService handlerService = (IHandlerService) getSite().getService(IHandlerService.class);
		handlerService.activateHandler(ICommandIds.GENERIEREN, new GenerierenHandler(getViewer(),
			getStammdatenservice()));
		handlerService.activateHandler(ICommandIds.PRUEFEN, new CheckMitgliedseitengeneratableHandler(getViewer(),
			getStammdatenservice()));
		handlerService.activateHandler(ICommandIds.ANLEGEN, new ObjektAnlegenHandler(getStammdatenservice(), ID));
		handlerService.activateHandler(ICommandIds.DIPLOM_GEBEN, new DiplomGebenHandler(getViewer(),
			getStammdatenservice()));
		handlerService.activateHandler(ICommandIds.KONTAKT_HINZUFUEGEN, new MailadresseHinzufuegenHandler(getViewer(),
			getStammdatenservice()));
		handlerService.activateHandler(ICommandIds.KONTAKT_ENTFERNEN, new MailadresseEntfernenHandler(getViewer(),
			getStammdatenservice()));
		handlerService.activateHandler(ICommandIds.MITGLIED_DEAKTIVIEREN, new MitgliedDeaktivierenHandler(getViewer(),
			getStammdatenservice()));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#initTableViewer(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected TableViewer initTableViewer(final Composite pParent) {
		final ITableViewColumn[] viewColumns = new ITableViewColumn[columnTitles.length];
		final IViewerComparator[] comparators = getColumnComparators();
		final ColumnLabelProvider[] labelProviders = getColumnLableProviders();
		for (int i = 0; i < columnTitles.length; i++) {
			viewColumns[i] = new ViewColumn(columnTitles[i], columnBounds[i], comparators[i], labelProviders[i]);
		}
		return TableViewerFactory.createTableViewer(pParent, new MitgliederViewContentProvider(getStammdatenservice()),
			viewColumns, true);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#getEditorID()
	 */
	@Override
	protected String getEditorID() {
		return MitgliedEditor.ID;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#getEditorInput(java.lang.Object)
	 */
	@Override
	protected AbstractMatheEditorInput getEditorInput(final Object pObj) {
		if (!(pObj instanceof Mitglied)) {
			return null;
		}
		return new MitgliedEditorInput((AbstractMatheAGObject) pObj);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#getModelListener(org.eclipse.jface.viewers.TableViewer
	 * )
	 */
	@Override
	protected AbstractModelListener getModelListener(final TableViewer pViewer) {
		return new MitgliederModelListener(pViewer);
	}

	/**
	 * Erzeugt abhängig vom Spaltenindex einen {@link ColumnLabelProvider}
	 *
	 * @param pInd int der Spaltenindex
	 * @return {@link ColumnLabelProvider}
	 */
	private ColumnLabelProvider[] getColumnLableProviders() {
		final CommonColumnLabelProvider[] labelProviders = new CommonColumnLabelProvider[columnTitles.length];

		for (int i = 0; i < labelProviders.length; i++) {
			labelProviders[i] = new MitgliederLabelProvider(i);
		}
		return labelProviders;
	}

	/**
	 * @return
	 */
	private AbstractViewerComparator[] getColumnComparators() {
		final AbstractViewerComparator[] comparators = new MitgliederViewerComparator[columnTitles.length];
		for (int i = 0; i < comparators.length; i++) {
			comparators[i] = new MitgliederViewerComparator(i);
		}
		return comparators;
	}
}