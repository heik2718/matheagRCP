/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.listeners;

import java.beans.PropertyChangeEvent;

import org.eclipse.jface.viewers.TableViewer;

import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.IMinikaenguruNames;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.ISerieNames;
import de.egladil.mathejungalt.service.stammdaten.event.ModelChangeEvent;

/**
 * @author aheike
 */
public class MinikaenguruModelListener extends AbstractAufgabensammlungModelListener {

	/**
	 * @param pViewer
	 */
	public MinikaenguruModelListener(TableViewer pViewer) {
		super(pViewer);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.views.listeners.AbstractModelListener#isInterestingEvent(de.egladil.mathejungalt
	 * .service.stammdaten.event.ModelChangeEvent)
	 */
	@Override
	protected boolean isInterestingEvent(ModelChangeEvent pEvt) {
		if (ModelChangeEvent.ADD_EVENT.equals(pEvt.getEventType()) && pEvt.getNewValue() instanceof Minikaenguru) {
			return true;
		}
		if (ModelChangeEvent.REMOVE_EVENT.equals(pEvt.getEventType()) && pEvt.getOldValue() instanceof Minikaenguru) {
			return true;
		}
		if (ModelChangeEvent.PROPERTY_CHANGE_EVENT.equals(pEvt.getEventType())
			&& !(pEvt.getSource() instanceof Minikaenguru)) {
			return false;
		}
		final String property = pEvt.getProperty();
		return isRelevant(property);
	}

	/**
	 * @param pPropertyName
	 * @return boolean
	 */
	private boolean isRelevant(final String pPropertyName) {
		return IMinikaenguruNames.PROP_JAHR.equals(pPropertyName)
			|| IMinikaenguruNames.PROP_BEENDET.equals(pPropertyName)
			|| ISerieNames.PROP_MONAT_JAHR_TEXT.equals(pPropertyName)
			|| IAufgabensammlung.PROP_BEENDET.equals(pPropertyName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.ui.app.views.listeners.AbstractModelListener#isInterestingEvent(java.beans.
	 * PropertyChangeEvent)
	 */
	@Override
	protected boolean isInterestingEvent(PropertyChangeEvent pEvt) {
		if (!(pEvt.getSource() instanceof Minikaenguru)) {
			return false;
		}
		return isRelevant(pEvt.getPropertyName());
	}
}
