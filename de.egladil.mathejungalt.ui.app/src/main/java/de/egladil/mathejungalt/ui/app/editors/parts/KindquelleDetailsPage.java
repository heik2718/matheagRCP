/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import de.egladil.mathejungalt.domain.quellen.Kindquelle;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Die Detailspage für die Attribute der {@link Kindquelle}
 * 
 * @author winkelv
 */
public class KindquelleDetailsPage extends AbstractQuelleDetailsPage {

	/**
	 * @param pMaster
	 */
	public KindquelleDetailsPage(AbstractMasterDetailsBlock pMaster) {
		super(pMaster);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractDetailsPage#getAttributesPart()
	 */
	@Override
	protected MatheAGObjectAttributesPart getAttributesPart() {
		KindquelleSimpleAttributePartDataBindingProvider contentsProvider = new KindquelleSimpleAttributePartDataBindingProvider();
		return new MatheAGObjectAttributesPart(false, contentsProvider);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractDetailsPage#getHeader()
	 */
	@Override
	protected String getHeader() {
		return ITextConstants.SECTION_HEADER_KINDQUELLE_ATTRIBUTES;
	}
}
