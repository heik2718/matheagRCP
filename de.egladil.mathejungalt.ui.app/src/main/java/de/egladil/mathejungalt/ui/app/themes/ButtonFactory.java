/**
 * 
 */
package de.egladil.mathejungalt.ui.app.themes;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.widgets.FormToolkit;

/**
 * @author winkelv
 */
public final class ButtonFactory {

	public static String ADD = "add";

	public static String REMOVE = "remove";

	public static String UP = "up";

	public static String DOWN = "down";

	public static String PROPERTIES = "properties";

	public static String SAVE = "save";

	public static String CANCEL = "cancel";

	public static String UNDO = "undo";

	/**
	 * 
	 */
	private ButtonFactory() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Erzeugt einen Push-Button entweder mit Hilfe des {@link FormToolkit}, falls das nicht null ist oder mittels
	 * Konstruktor. Die Buttons sind mit Layout-Informationen versehen.
	 * 
	 * @param pParent {@link Composite} der Container
	 * @param pToolkit {@link FormToolkit} das FormToolkit
	 * @param pButtonLabel String der Text auf dem Button
	 * @param pCommand String ein Command-Code zur sp�teren Identifizierung. Hierzu die {@link ButtonFactory}-
	 * Konstanten verwenden.
	 * @return
	 */
	public static Button createButton(final Composite pParent, final FormToolkit pToolkit, final String pButtonLabel,
		final String pCommand) {
		Button button;
		if (pToolkit != null)
			button = pToolkit.createButton(pParent, pButtonLabel, SWT.PUSH);
		else {
			button = new Button(pParent, SWT.PUSH);
			button.setText(pButtonLabel);
		}
		GridData gd = new GridData(GridData.FILL_HORIZONTAL | GridData.VERTICAL_ALIGN_BEGINNING);
		button.setLayoutData(gd);
		button.setData(pCommand);
		return button;
	}
}
