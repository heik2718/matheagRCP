/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.handlers.IHandlerService;

import de.egladil.mathejungalt.ui.app.handlers.BeendenHandler;
import de.egladil.mathejungalt.ui.app.handlers.CheckAufgabensammlungGeneratableHandler;
import de.egladil.mathejungalt.ui.app.handlers.GenerierenHandler;
import de.egladil.mathejungalt.ui.app.handlers.ICommandIds;
import de.egladil.mathejungalt.ui.app.handlers.NumerierenHandler;
import de.egladil.mathejungalt.ui.app.handlers.ObjektAnlegenHandler;
import de.egladil.mathejungalt.ui.app.themes.TableViewerFactory;
import de.egladil.mathejungalt.ui.app.views.comparators.IViewerComparator;
import de.egladil.mathejungalt.ui.app.views.contentproviders.AufgabensammlungViewContentProvider;

/**
 * @author aheike
 */
public abstract class AbstractAufgabensammlungView extends AbstractMatheJungAltView {

	/**
	 * 
	 */
	public AbstractAufgabensammlungView() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#initTableViewer(org.eclipse.swt.widgets.Composite)
	 */
	protected TableViewer initTableViewer(Composite pParent) {
		String[] columnTitles = getColumnTitles();
		int[] columnBounds = getColumnBounds();
		ITableViewColumn[] viewColumns = new ITableViewColumn[columnTitles.length];
		IViewerComparator[] comparators = getColumnComparators();
		ColumnLabelProvider[] labelProviders = getColumnLableProviders();
		for (int i = 0; i < columnTitles.length; i++) {
			viewColumns[i] = new ViewColumn(columnTitles[i], columnBounds[i], comparators[i], labelProviders[i]);
		}
		return TableViewerFactory.createTableViewer(pParent, new AufgabensammlungViewContentProvider(
			getStammdatenservice()), viewColumns, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#createHandlers()
	 */
	@Override
	protected void createHandlers() {
		super.createHandlers();
		IHandlerService handlerService = (IHandlerService) getSite().getService(IHandlerService.class);
		handlerService.activateHandler(ICommandIds.GENERIEREN, new GenerierenHandler(getViewer(),
			getStammdatenservice()));
		handlerService.activateHandler(ICommandIds.PRUEFEN, new CheckAufgabensammlungGeneratableHandler(getViewer(),
			getStammdatenservice()));
		handlerService.activateHandler(ICommandIds.NUMERIEREN, new NumerierenHandler(getViewer(),
			getStammdatenservice()));
		handlerService.activateHandler(ICommandIds.BEENDEN, new BeendenHandler(getViewer(), getStammdatenservice()));
		handlerService.activateHandler(ICommandIds.ANLEGEN, new ObjektAnlegenHandler(getStammdatenservice(),
			getViewID()));
	}

	/**
	 * @return
	 */
	protected abstract IViewerComparator[] getColumnComparators();

	/**
	 * Erzeugt abhängig vom Spaltenindex einen {@link ColumnLabelProvider}
	 * 
	 * @param pInd int der Spaltenindex
	 * @return {@link ColumnLabelProvider}
	 */
	protected abstract ColumnLabelProvider[] getColumnLableProviders();

	/**
	 * @return
	 */
	protected abstract String[] getColumnTitles();

	/**
	 * @return
	 */
	protected abstract int[] getColumnBounds();

	/**
	 * @return
	 */
	protected abstract String getViewID();

}
