/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.conversion.IConverter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.IMatheAGObjectNames;
import de.egladil.mathejungalt.domain.schulen.ISchuleNames;
import de.egladil.mathejungalt.domain.schulen.Land;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;
import de.egladil.mathejungalt.ui.app.themes.PartControlsFactory;

/**
 * @author winkelv
 */
public class SchuleSimpleAttributePartDataBindingProvider extends AbstractSimpleAttributePartDataBindingProvider {

	private static final Land NULL_BUNDESLAND = Land.createNullLand();

	private IStammdatenservice stammdatenService;

	private List<Land> laender;

	/**
	 *
	 */
	public SchuleSimpleAttributePartDataBindingProvider() {
	}

	private void checkStammdatenService() {
		if (stammdatenService == null) {
			stammdatenService = MatheJungAltActivator.getDefault().getStammdatenservice();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.contentproviders.AbstractAttributePartContentsProvider#anzahlZeilen()
	 */
	@Override
	protected int anzahlZeilen() {
		return 6;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.contentproviders.AbstractAttributePartContentsProvider#initLabelsAndTexts(org.eclipse
	 * .swt.widgets .Composite, org.eclipse.ui.forms.widgets.FormToolkit, boolean)
	 */
	@Override
	protected void initControls(Composite pClient, FormToolkit pToolkit, boolean pEditable,
		final AbstractMatheAGObject pInput) {
		final Control[] controls = getControls();
		Control[] labels = new Control[controls.length];

		int widthHint = 150;
		labels[0] = PartControlsFactory.createIdLabel(pClient, pToolkit);
		controls[0] = PartControlsFactory.createIdText(pClient, pToolkit, widthHint);

		labels[1] = PartControlsFactory.createSchluesselLabel(pClient, pToolkit);
		controls[1] = PartControlsFactory.createText(pClient, pToolkit, widthHint, pEditable);
		((Text) controls[1]).setTextLimit(ISchuleNames.LENGTH_SCHLUESSEL);

		labels[2] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_NAME);
		controls[2] = PartControlsFactory.createText(pClient, pToolkit, widthHint, pEditable);
		((Text) controls[2]).setTextLimit(ISchuleNames.LENGTH_NAME);

		labels[3] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_ANSCHRIFT);
		controls[3] = PartControlsFactory.createText(pClient, pToolkit, widthHint, pEditable);
		((Text) controls[3]).setTextLimit(ISchuleNames.LENGTH_ANSCHRIFT);

		labels[4] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_BUNDESLAND);

		ComboViewer cv = new ComboViewer(pClient);
		checkStammdatenService();
		laender = stammdatenService.getLaender();
		Collections.sort(laender);

		cv.setContentProvider(new ArrayContentProvider() {

			@Override
			public Object[] getElements(Object pInputElement) {
				Object[] elements = new Object[laender.size() + 1];
				elements[0] = NULL_BUNDESLAND;
				for (int i = 1; i <= laender.size(); i++) {
					elements[i] = laender.get(i - 1);
				}
				return elements;
			}
		});
		cv.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object pElement) {
				if (pElement instanceof Land) {
					return ((Land) pElement).getLabel();
				}
				return super.getText(pElement);
			}
		});
		cv.setInput(laender.toArray());

		controls[4] = cv.getControl();

		labels[5] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_TELEFON);
		controls[5] = PartControlsFactory.createText(pClient, pToolkit, widthHint, pEditable);
		((Text) controls[5]).setTextLimit(ISchuleNames.LENGTH_TELEFONNUMMER);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.contentproviders.IAttributePartContentsProvider#refreshDataBindings(de.egladil.mathe
	 * .core.domain .AbstractMatheAGObject)
	 */
	@Override
	public void refreshDataBindings(AbstractMatheAGObject pInput) {
		super.refreshDataBindings(pInput);

		Binding[] bindings = getBindings();
		Control[] controls = getControls();
		DataBindingContext dbc = getDbc();

		IObservableValue observableWidget = SWTObservables.observeText(controls[0], SWT.Modify);
		IObservableValue observableProps = BeansObservables.observeValue(pInput, IMatheAGObjectNames.PROP_ID);
		bindings[0] = dbc.bindValue(observableWidget, observableProps, null, null);

		observableWidget = SWTObservables.observeText(controls[1], SWT.Modify);
		observableProps = BeansObservables.observeValue(pInput, IMatheAGObjectNames.PROP_SCHLUESSEL);
		bindings[1] = dbc.bindValue(observableWidget, observableProps, null, null);

		observableWidget = SWTObservables.observeText(controls[2], SWT.Modify);
		observableProps = BeansObservables.observeValue(pInput, ISchuleNames.PROP_NAME);
		bindings[2] = dbc.bindValue(observableWidget, observableProps, null, null);

		observableWidget = SWTObservables.observeText(controls[3], SWT.Modify);
		observableProps = BeansObservables.observeValue(pInput, ISchuleNames.PROP_ANSCHRIFT);
		bindings[3] = dbc.bindValue(observableWidget, observableProps, null, null);

		observableWidget = SWTObservables.observeSelection(controls[4]);
		observableProps = BeansObservables.observeValue(pInput, ISchuleNames.PROP_LAND);

		UpdateValueStrategy targetToModelStrategy = new UpdateValueStrategy();
		targetToModelStrategy.setConverter(new IConverter() {

			@Override
			public Object getToType() {
				return Land.class;
			}

			@Override
			public Object getFromType() {
				return String.class;
			}

			@Override
			public Object convert(Object pFromObject) {
				if (pFromObject == null) {
					return null;
				}
				String str = (String) pFromObject;
				for (Land bl : stammdatenService.getLaender()) {
					if (bl.getLabel().equals(str)) {
						return bl;
					}
				}
				return null;
			}
		});

		UpdateValueStrategy modelToTargetStrategy = new UpdateValueStrategy();
		modelToTargetStrategy.setConverter(new IConverter() {

			@Override
			public Object getToType() {
				return String.class;
			}

			@Override
			public Object getFromType() {
				return Land.class;
			}

			@Override
			public Object convert(Object pFromObject) {
				if (pFromObject == null || !(pFromObject instanceof Land)) {
					return NULL_BUNDESLAND;
				}
				Land bl = (Land) pFromObject;
				return bl.getLabel();
			}
		});

		bindings[4] = dbc.bindValue(observableWidget, observableProps, targetToModelStrategy, modelToTargetStrategy);

		observableWidget = SWTObservables.observeText(controls[5], SWT.Modify);
		observableProps = BeansObservables.observeValue(pInput, ISchuleNames.PROP_TELEFONNUMMER);
		bindings[5] = dbc.bindValue(observableWidget, observableProps, null, null);
	}
}
