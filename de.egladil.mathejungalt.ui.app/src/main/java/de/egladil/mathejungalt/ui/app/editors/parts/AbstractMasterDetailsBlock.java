/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.IDetailsPageProvider;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.MasterDetailsBlock;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.services.IDisposable;

import de.egladil.mathejungalt.service.stammdaten.event.IModelChangeListener;
import de.egladil.mathejungalt.service.stammdaten.event.ModelChangeEvent;
import de.egladil.mathejungalt.ui.app.editors.pages.AbstractAttributesFormPage;
import de.egladil.mathejungalt.ui.app.themes.LayoutFactory;

/**
 * @author winkelv
 */
public abstract class AbstractMasterDetailsBlock extends MasterDetailsBlock implements IDisposable,
	PropertyChangeListener, IDetailsPageProvider, IEditorPartInputProvider, IModelChangeListener {

	/** */
	private AbstractAttributesFormPage parentPage;

	/**
	 *
	 */
	public AbstractMasterDetailsBlock(AbstractAttributesFormPage pParentPage) {
		parentPage = pParentPage;
		pParentPage.getEditorInput();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.MasterDetailsBlock#createContent(org.eclipse.ui.forms.IManagedForm)
	 */
	@Override
	public void createContent(IManagedForm pManagedForm) {
		super.createContent(pManagedForm);
		sashForm.setWeights(new int[] { 40, 60 });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.MasterDetailsBlock#createMasterPart(org.eclipse.ui.forms.IManagedForm,
	 * org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void createMasterPart(IManagedForm pManagedForm, Composite pParent) {
		FormToolkit toolkit = pManagedForm.getToolkit();
		// Hier darf nicht die ScrolledForm in pManagedForm als Parent übergeben werden, sonst wird die Section weit
		// nach unten
		// geklatscht
		Section section = toolkit.createSection(pParent, Section.DESCRIPTION | Section.TITLE_BAR);
		section.setActiveToggleColor(toolkit.getHyperlinkGroup().getActiveForeground());
		toolkit.createCompositeSeparator(section);

		// der Container für den StructuredViewerPart mit Buttons
		Composite client = toolkit.createComposite(section);
		client.setLayout(LayoutFactory.createSectionClientGridLayout(false, 1));
		GridData gd = new GridData(GridData.FILL_BOTH);
		gd.widthHint = getWidthHint();
		client.setLayoutData(gd);

		// section anreichern und Behälter hinzufügen
		section.setText(getSectionHeader());
		section.setDescription(getSectionDescription());
		section.setClient(client);
		section.setExpanded(true);

		gd = LayoutFactory.getGridData(1);
		section.setLayoutData(gd);

		// Hier z.B. den ViewerPart einhängen
		hookSpecialParts(pManagedForm, section, client);

		createContextMenuActions();

		// Borders zeichnen
		toolkit.paintBordersFor(client);
	}

	/**
	 * Dient zum Einfügen der speziellen Teile dieses Editors. Das kann z.B. ein ViewerPart sein oder ein Teil, in dem
	 * einfache Domainobjektattribute angezeicgt werden.
	 *
	 * @param pManagedForm {@link IManagedForm} die ManagedForm
	 * @param pSection {@link Section} eine Section.
	 * @param pComposite {@link Composite} der Container in den der zusätzliche Teil eingehängt wird.
	 */
	protected abstract void hookSpecialParts(final IManagedForm pManagedForm, Section pSection, Composite pComposite);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.event.IModelChangeListener#modelChanged(de.egladil.mathejungalt.service
	 * .stammdaten .event.ModelChangeEvent)
	 */
	@Override
	public void modelChanged(ModelChangeEvent pEvt) {
		// Standardimplementierung ist leer
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.MasterDetailsBlock#createToolBarActions(org.eclipse.ui.forms.IManagedForm)
	 */
	@Override
	protected void createToolBarActions(IManagedForm pManagedForm) {
		// gibt Standardmäßig keine
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.services.IDisposable#dispose()
	 */
	@Override
	public void dispose() {
		// leere Standardimplementierung
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent pEvt) {
		// Standardimplementierung ist leer
	}

	/**
	 * @return String die Section-Überschrift
	 */
	protected abstract String getSectionHeader();

	/**
	 * @return String die Section-Beschreibung
	 */
	protected abstract String getSectionDescription();

	/**
	 * Defaultvorschlag für die Breite des Viewers. Default ist 100, kann aber überschrieben werden.
	 *
	 * @return
	 */
	protected int getWidthHint() {
		return 300;
	}

	/**
	 * Erzeugt das Kontextmenü im ViewerPart
	 */
	protected void createContextMenuActions() {
		// normalerweise nix
	}

	/**
	 * @return the parentPage
	 */
	public AbstractAttributesFormPage getParentPage() {
		return parentPage;
	}

}
