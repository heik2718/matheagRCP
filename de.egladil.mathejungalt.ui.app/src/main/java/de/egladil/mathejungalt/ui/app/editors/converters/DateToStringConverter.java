/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.converters;

import java.util.Date;

import org.apache.commons.lang.time.DateFormatUtils;
import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Konvertiert ein Datum in einen String
 *
 * @author Winkelv
 */
public class DateToStringConverter implements IConverter {

	/**
	 *
	 */
	public DateToStringConverter() {
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object pFromObject) {
		Date date = (Date) pFromObject;
		return DateFormatUtils.format(date, ITextConstants.DATE_FORMAT_PATTERNS[0]);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.core.databinding.conversion.IConverter#getFromType()
	 */
	@Override
	public Object getFromType() {
		return Date.class;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.core.databinding.conversion.IConverter#getToType()
	 */
	@Override
	public Object getToType() {
		return String.class;
	}
}
