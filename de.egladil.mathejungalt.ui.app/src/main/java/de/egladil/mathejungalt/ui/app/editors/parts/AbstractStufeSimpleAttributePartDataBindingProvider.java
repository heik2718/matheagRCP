/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.conversion.IConverter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgaben.IAufgabeNames;
import de.egladil.mathejungalt.ui.app.editors.converters.StufetextToIntegerConverter;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;
import de.egladil.mathejungalt.ui.app.themes.PartControlsFactory;

/**
 * Der Teil, der die speziellen Stufen für Serienaufgaben anzeigt.
 *
 * @author winkelv
 */
public abstract class AbstractStufeSimpleAttributePartDataBindingProvider extends
	AbstractSimpleAttributePartDataBindingProvider {

	/**
	 *
	 */
	public AbstractStufeSimpleAttributePartDataBindingProvider() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractSimpleAttributePartDataBindingProvider#anzahlZeilen()
	 */
	@Override
	protected int anzahlZeilen() {
		return 1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.parts.AbstractSimpleAttributePartDataBindingProvider#initControls(org.eclipse.swt
	 * .widgets.Composite , org.eclipse.ui.forms.widgets.FormToolkit, boolean,
	 * de.egladil.mathe.core.domain.AbstractMatheAGObject)
	 */
	@Override
	protected void initControls(Composite pClient, FormToolkit pToolkit, boolean pEditable, AbstractMatheAGObject pInput) {
		final Control[] controls = getControls();
		Control[] labels = new Control[controls.length];
		Aufgabe aufgabe = (Aufgabe) pInput;
		boolean editable = aufgabe.getAnzahlSperrendeReferenzen() == 0;
		int widthHint = 250;

		labels[0] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_STUFE);
		if (editable) {
			String[] items = getItems();
			controls[0] = PartControlsFactory.createCombo(pClient, pToolkit, items, widthHint);
		} else {
			controls[0] = PartControlsFactory.createText(pClient, pToolkit, widthHint, false);
		}
	}

	/**
	 * @return
	 */
	protected abstract String[] getItems();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.parts.AbstractSimpleAttributePartDataBindingProvider#refreshDataBindings(de.egladil
	 * .mathe.core .domain.AbstractMatheAGObject)
	 */
	@Override
	public void refreshDataBindings(AbstractMatheAGObject pInput) {
		if (!(pInput instanceof Aufgabe)) {
			return;
		}
		super.refreshDataBindings(pInput);
		Binding[] bindings = getBindings();
		Control[] controls = getControls();
		DataBindingContext dbc = getDbc();
		IObservableValue observableWidget = null;
		IObservableValue observableProps = null;

		if (controls[0] instanceof Text) {
			observableWidget = SWTObservables.observeText(controls[0], SWT.Modify);
		} else {
			observableWidget = SWTObservables.observeText(controls[0]);
		}
		observableProps = BeansObservables.observeValue(pInput, IAufgabeNames.PROP_STUFE);
		UpdateValueStrategy targetToModelStrategy = new UpdateValueStrategy();
		targetToModelStrategy.setConverter(new StufetextToIntegerConverter());
		UpdateValueStrategy modelToTargetStrategy = new UpdateValueStrategy();
		modelToTargetStrategy.setConverter(getConverter());
		bindings[0] = dbc.bindValue(observableWidget, observableProps, targetToModelStrategy, modelToTargetStrategy);
	}

	/**
	 * @return
	 */
	protected abstract IConverter getConverter();
}
