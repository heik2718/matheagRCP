/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.listeners;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.jface.viewers.ColumnLabelProvider;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.domain.mitglieder.Serienteilnahme;
import de.egladil.mathejungalt.service.stammdaten.event.ModelChangeEvent;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.adapters.IDetailsObject;
import de.egladil.mathejungalt.ui.app.editors.parts.AbstractViewerPartWithButtons;
import de.egladil.mathejungalt.ui.app.views.labelproviders.SerienLabelProvider;

/**
 * @author Winkelv
 */
public class SerienteilnahmenViewerListener extends AbstractViewerPartWithButtonsViewerListener {

	/**
	 * @param pView
	 */
	public SerienteilnahmenViewerListener(AbstractViewerPartWithButtons pView) {
		super(pView);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.listeners.IStructuredViewerPartViewerListener#handleRemove()
	 */
	@Override
	public void handleRemove() {
		if (getEditorInput() == null || getSelectedDetails() == null || getSelectedDetails().size() != 1)
			return;
		Mitglied mitglied = (Mitglied) getEditorInput();
		Serienteilnahme teilnahme = (Serienteilnahme) getSelectedDetails().get(0);
		getStammdatenservice().removeTeilnahme(mitglied, teilnahme.getSerie());
		if (getView() != null) {
			getView().getViewer().setSelection(null);
			getView().updateButtonAndMenuStatus();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.editors.listeners.AbstractViewerPartWithButtonsViewerListener#isInterestingEvent
	 * (de.egladil. mathejungalt.service.stammdaten.event.ModelChangeEvent)
	 */
	@Override
	protected boolean isInterestingEvent(ModelChangeEvent pEvt) {
		if (ModelChangeEvent.ADD_EVENT.equals(pEvt.getEventType())
			|| ModelChangeEvent.REMOVE_EVENT.equals(pEvt.getEventType())) {
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.editors.listeners.AbstractViewerPartWithButtonsViewerListener#specialAdd(java.
	 * util.List)
	 */
	@Override
	protected void specialAdd(List<AbstractMatheAGObject> selectedObjects) {
		Mitglied mitglied = (Mitglied) getEditorInput();
		Serienteilnahme teilnahme = null;
		if (selectedObjects != null) {
			Serie serie = (Serie) selectedObjects.get(0);
			getStammdatenservice().mitgliedNimmtTeil(mitglied, serie);
			teilnahme = getStammdatenservice().findSerienteilnahme(mitglied, serie);
			Object detail = MatheJungAltActivator.getDefault().getAdapter(teilnahme, IDetailsObject.class);
			if (detail == null) {
				throw new MatheJungAltException("Kein Adapter fuer " + teilnahme.getClass().getName()
					+ " und IDetailsObject gefunden");
			}
			IDetailsObject detailsObject = (IDetailsObject) detail;
			updateDetailsList(detailsObject);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.listeners.AbstractViewerPartWithButtonsViewerListener#getObjectPool()
	 */
	@Override
	public Collection<AbstractMatheAGObject> getObjectPool() {
		Collection<AbstractMatheAGObject> objectPool = new ArrayList<AbstractMatheAGObject>();
		Collection<Serie> serien = getStammdatenservice().getSerien();
		Mitglied mitglied = (Mitglied) getEditorInput();
		int aktuelleRunde = getStammdatenservice().aktuelleRunde();

		Collection<Serienteilnahme> teilnahmen = getStammdatenservice().serienteilnahmenZuMitgliedLaden(mitglied);

		List<Serie> mitgliedSerien = new ArrayList<Serie>();
		Iterator<Serienteilnahme> iter = teilnahmen.iterator();
		while (iter.hasNext())
			mitgliedSerien.add(iter.next().getSerie());

		for (Serie serie : serien) {
			if (serie.getBeendet() == 1 && serie.getRunde() == aktuelleRunde && !mitgliedSerien.contains(serie))
				objectPool.add(serie);
		}
		return objectPool;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.editors.listeners.AbstractViewerPartWithButtonsViewerListener#getLabelProvider()
	 */
	@Override
	public ColumnLabelProvider getLabelProvider() {
		return new SerienLabelProvider();
	}
}
