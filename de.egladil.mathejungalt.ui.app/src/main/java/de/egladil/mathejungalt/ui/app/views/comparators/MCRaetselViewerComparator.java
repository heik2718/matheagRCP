/**
 *
 */
package de.egladil.mathejungalt.ui.app.views.comparators;

import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.ui.app.views.MCRaetselView;

/**
 * @author Winkelv
 */
public class MCRaetselViewerComparator extends AbstractViewerComparator {

	/**
	 * @param pIndex
	 */
	public MCRaetselViewerComparator(int pIndex) {
		super(pIndex);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Object pO1, Object pO2) {
		if (pO1 instanceof String && pO2 instanceof String) {
			return pO1.toString().compareToIgnoreCase(pO2.toString());
		}
		MCArchivraetsel raetsel1 = (MCArchivraetsel) pO1;
		MCArchivraetsel raetsel2 = (MCArchivraetsel) pO2;
		switch (getIndex()) {
		case MCRaetselView.INDEX_SCHLUESSEL:
			return raetsel1.getSchluessel().compareTo(raetsel2.getSchluessel());
		case MCRaetselView.INDEX_STUFE:
			return raetsel1.getStufe().getStufe() - raetsel2.getStufe().getStufe();
		case MCRaetselView.INDEX_TITEL:
			return raetsel1.getTitel().compareToIgnoreCase(raetsel2.getTitel());
		case MCRaetselView.INDEX_PRIVAT:
			String privat1 = raetsel1.isPrivat() ? "J" : "N";
			String privat2 = raetsel2.isPrivat() ? "J" : "N";
			return privat1.compareTo(privat2);
		case MCRaetselView.INDEX_PUBLISHED:
			String published1 = raetsel1.isVeroeffentlicht() ? "J" : "N";
			String published2 = raetsel2.isVeroeffentlicht() ? "J" : "N";
			return published1.compareTo(published2);

		default:
			return raetsel1.compareTo(raetsel2);
		}
	}
}
