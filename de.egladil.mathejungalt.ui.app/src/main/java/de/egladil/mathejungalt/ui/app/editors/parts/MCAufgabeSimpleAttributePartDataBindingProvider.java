/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.IMatheAGObjectNames;
import de.egladil.mathejungalt.domain.mcraetsel.IMCAufgabeNames;
import de.egladil.mathejungalt.domain.mcraetsel.MCAntwortbuchstaben;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabenarten;
import de.egladil.mathejungalt.domain.mcraetsel.MCThemen;
import de.egladil.mathejungalt.domain.types.MCAufgabenstufen;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.editors.converters.MCAntwortbuchstabenToStringConverter;
import de.egladil.mathejungalt.ui.app.editors.converters.MCAufgabenartToStringConverter;
import de.egladil.mathejungalt.ui.app.editors.converters.MCAufgabenstufeToStringConverter;
import de.egladil.mathejungalt.ui.app.editors.converters.MCThemaToStringConverter;
import de.egladil.mathejungalt.ui.app.editors.converters.StringToMCAntwortbuchstabenConverter;
import de.egladil.mathejungalt.ui.app.editors.converters.StringToMCAufgabenartConverter;
import de.egladil.mathejungalt.ui.app.editors.converters.StringToMCAufgabenstufeConverter;
import de.egladil.mathejungalt.ui.app.editors.converters.StringToMCThemaConverter;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;
import de.egladil.mathejungalt.ui.app.themes.PartControlsFactory;

/**
 * Die Änderungsmöglichkeiten sind für gesperrte Aufgaben eingeschränkt da die generierten Dateien nach der Verwendung
 * der Aufgaben unverndert bleiben sollen.
 *
 * @author winkelv
 */
public class MCAufgabeSimpleAttributePartDataBindingProvider extends AbstractSimpleAttributePartDataBindingProvider {

	/** */
	private MCAufgabe aufgabe;

	private Button mitTabelle;

	/**
	 *
	 */
	public MCAufgabeSimpleAttributePartDataBindingProvider() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.contentproviders.AbstractAttributePartContentsProvider#anzahlZeilen()
	 */
	@Override
	protected int anzahlZeilen() {
		return 16;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.contentproviders.AbstractAttributePartContentsProvider#initLabelsAndTexts(org.eclipse
	 * .swt.widgets .Composite, org.eclipse.ui.forms.widgets.FormToolkit, boolean)
	 */
	@Override
	protected void initControls(Composite pClient, FormToolkit pToolkit, boolean pEditable,
		final AbstractMatheAGObject pInput) {
		final Control[] controls = getControls();
		Control[] labels = new Control[controls.length];

		aufgabe = (MCAufgabe) pInput;
		final boolean editable = MCAufgabe.NOT_LOCKED.equals(aufgabe.getGesperrtFuerRaetsel());

		int widthHint = 150;
		// ID
		labels[0] = PartControlsFactory.createIdLabel(pClient, pToolkit);
		controls[0] = PartControlsFactory.createIdText(pClient, pToolkit, widthHint);

		// Schluessel
		labels[1] = PartControlsFactory.createSchluesselLabel(pClient, pToolkit);
		controls[1] = PartControlsFactory.createText(pClient, pToolkit, widthHint, editable);
		((Text) controls[1]).setTextLimit(IMCAufgabeNames.LENGTH_SCHLUESSEL);

		// Art
		String[] items = null;
		labels[2] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_ART);
		if (editable) {
			items = new String[] { MCAufgabenarten.E.getLabel(), MCAufgabenarten.N.getLabel(),
				MCAufgabenarten.Z.getLabel() };
			controls[2] = PartControlsFactory.createCombo(pClient, pToolkit, items, widthHint);
		} else {
			controls[2] = PartControlsFactory.createText(pClient, pToolkit, widthHint, false);
		}

		// Thema
		labels[3] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_THEMA);
		if (editable) {
			items = MCThemen.toLabels();
			controls[3] = PartControlsFactory.createCombo(pClient, pToolkit, items, widthHint);
		} else {
			controls[3] = PartControlsFactory.createText(pClient, pToolkit, widthHint, false);
		}

		// Stufe
		labels[4] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_STUFE);
		if (editable) {
			items = MCAufgabenstufen.toLabels();
			controls[4] = PartControlsFactory.createCombo(pClient, pToolkit, items, widthHint);
		} else {
			controls[4] = PartControlsFactory.createText(pClient, pToolkit, widthHint, false);
		}

		// mit Tabelle
		PartControlsFactory.createLabel(pClient, pToolkit, "");
		mitTabelle = new Button(pClient, SWT.CHECK);
		mitTabelle.setText("mit Tabelle");
		mitTabelle.setEnabled(editable);

		// Antworten
		labels[5] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_ANTWORT_A);
		controls[5] = PartControlsFactory.createText(pClient, pToolkit, widthHint, editable);

		labels[6] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_ANTWORT_B);
		controls[6] = PartControlsFactory.createText(pClient, pToolkit, widthHint, editable);

		labels[7] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_ANTWORT_C);
		controls[7] = PartControlsFactory.createText(pClient, pToolkit, widthHint, editable);

		labels[8] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_ANTWORT_D);
		controls[8] = PartControlsFactory.createText(pClient, pToolkit, widthHint, editable);

		labels[9] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_ANTWORT_E);
		controls[9] = PartControlsFactory.createText(pClient, pToolkit, widthHint, editable);

		// Lösungsbuchstabe
		labels[10] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_LOESUNGSBUCHSTABE);
		if (editable) {
			items = new String[] { MCAntwortbuchstaben.A.toString(), MCAntwortbuchstaben.B.toString(),
				MCAntwortbuchstaben.C.toString(), MCAntwortbuchstaben.D.toString(), MCAntwortbuchstaben.E.toString() };
			controls[10] = PartControlsFactory.createCombo(pClient, pToolkit, items, widthHint);
		} else {
			controls[10] = PartControlsFactory.createText(pClient, pToolkit, widthHint, false);
		}

		// Punkte
		labels[11] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_PUNKTE);
		controls[11] = PartControlsFactory.createText(pClient, pToolkit, widthHint, editable);

		// Quelle
		labels[12] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_QUELLE);
		controls[12] = PartControlsFactory.createText(pClient, pToolkit, widthHint, editable);

		// Titel
		labels[13] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_TITEL);
		controls[13] = PartControlsFactory.createText(pClient, pToolkit, widthHint, true);
		((Text) controls[13]).setTextLimit(IMCAufgabeNames.LENGTH_TITEL);

		// Beschreibung
		labels[14] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_BESCHREIBUNG);
		controls[14] = PartControlsFactory.createTextArea(pClient, pToolkit, widthHint, true);
		((Text) controls[14]).setTextLimit(IMCAufgabeNames.LENGTH_BEMERKUNG);

		// anzahl Antworten
		labels[15] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_ANZAHL_ANTWORTEN);
		controls[15] = PartControlsFactory.createText(pClient, pToolkit, widthHint, editable);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.contentproviders.IAttributePartContentsProvider#refreshDataBindings(de.egladil.mathe
	 * .core.domain .AbstractMatheAGObject)
	 */
	@Override
	public void refreshDataBindings(AbstractMatheAGObject pInput) {
		super.refreshDataBindings(pInput);

		Control[] controls = getControls();
		DataBindingContext dbc = getDbc();

		IObservableValue observableWidget = SWTObservables.observeText(controls[0], SWT.Modify);
		IObservableValue observableProps = BeansObservables.observeValue(pInput, IMatheAGObjectNames.PROP_ID);
		dbc.bindValue(observableWidget, observableProps, null, null);

		observableWidget = SWTObservables.observeText(controls[1], SWT.Modify);
		observableProps = BeansObservables.observeValue(pInput, IMatheAGObjectNames.PROP_SCHLUESSEL);
		dbc.bindValue(observableWidget, observableProps, null, null);

		if (controls[2] instanceof Text) {
			observableWidget = SWTObservables.observeText(controls[2], SWT.Modify);
		} else {
			observableWidget = SWTObservables.observeText(controls[2]);
		}
		observableProps = BeansObservables.observeValue(pInput, IMCAufgabeNames.PROP_ART);
		UpdateValueStrategy targetToModelStrategy = new UpdateValueStrategy();
		targetToModelStrategy.setConverter(new StringToMCAufgabenartConverter());
		UpdateValueStrategy modelToTargetStrategy = new UpdateValueStrategy();
		modelToTargetStrategy.setConverter(new MCAufgabenartToStringConverter());
		getDbc().bindValue(observableWidget, observableProps, targetToModelStrategy, modelToTargetStrategy);

		if (controls[3] instanceof Text) {
			observableWidget = SWTObservables.observeText(controls[3], SWT.Modify);
		} else {
			observableWidget = SWTObservables.observeText(controls[3]);
		}
		observableProps = BeansObservables.observeValue(pInput, IMCAufgabeNames.PROP_THEMA);
		targetToModelStrategy = new UpdateValueStrategy();
		targetToModelStrategy.setConverter(new StringToMCThemaConverter());
		modelToTargetStrategy = new UpdateValueStrategy();
		modelToTargetStrategy.setConverter(new MCThemaToStringConverter());
		dbc.bindValue(observableWidget, observableProps, targetToModelStrategy, modelToTargetStrategy);

		if (controls[4] instanceof Text) {
			observableWidget = SWTObservables.observeText(controls[4], SWT.Modify);
		} else {
			observableWidget = SWTObservables.observeText(controls[4]);
		}
		observableProps = BeansObservables.observeValue(pInput, IMCAufgabeNames.PROP_STUFE);
		targetToModelStrategy = new UpdateValueStrategy();
		targetToModelStrategy.setConverter(new StringToMCAufgabenstufeConverter());
		modelToTargetStrategy = new UpdateValueStrategy();
		modelToTargetStrategy.setConverter(new MCAufgabenstufeToStringConverter());
		dbc.bindValue(observableWidget, observableProps, targetToModelStrategy, modelToTargetStrategy);

		// checkbox
		observableWidget = SWTObservables.observeSelection(mitTabelle);
		observableProps = BeansObservables.observeValue(pInput, IMCAufgabeNames.PROP_TABELLE_GENERIEREN);
		dbc.bindValue(observableWidget, observableProps);

		{
			// A
			observableWidget = SWTObservables.observeText(controls[5], SWT.Modify);
			observableProps = BeansObservables.observeValue(pInput, IMCAufgabeNames.PROP_ANTWORT_A);
			dbc.bindValue(observableWidget, observableProps);

			observableWidget = SWTObservables.observeEnabled(controls[5]);
			observableProps = BeansObservables.observeValue(pInput, IMCAufgabeNames.PROP_TABELLE_GENERIEREN);
			dbc.bindValue(observableWidget, observableProps);

			observableWidget = SWTObservables.observeEditable(controls[5]);
			observableProps = BeansObservables.observeValue(pInput, IMCAufgabeNames.PROP_TABELLE_GENERIEREN);
			dbc.bindValue(observableWidget, observableProps);

			// B
			observableWidget = SWTObservables.observeText(controls[6], SWT.Modify);
			observableProps = BeansObservables.observeValue(pInput, IMCAufgabeNames.PROP_ANTWORT_B);
			dbc.bindValue(observableWidget, observableProps);

			observableWidget = SWTObservables.observeEnabled(controls[6]);
			observableProps = BeansObservables.observeValue(pInput, IMCAufgabeNames.PROP_TABELLE_GENERIEREN);
			dbc.bindValue(observableWidget, observableProps);

			observableWidget = SWTObservables.observeEditable(controls[6]);
			observableProps = BeansObservables.observeValue(pInput, IMCAufgabeNames.PROP_TABELLE_GENERIEREN);
			dbc.bindValue(observableWidget, observableProps);

			// C
			observableWidget = SWTObservables.observeText(controls[7], SWT.Modify);
			observableProps = BeansObservables.observeValue(pInput, IMCAufgabeNames.PROP_ANTWORT_C);
			dbc.bindValue(observableWidget, observableProps);

			observableWidget = SWTObservables.observeEnabled(controls[7]);
			observableProps = BeansObservables.observeValue(pInput, IMCAufgabeNames.PROP_TABELLE_GENERIEREN);
			dbc.bindValue(observableWidget, observableProps);

			observableWidget = SWTObservables.observeEditable(controls[7]);
			observableProps = BeansObservables.observeValue(pInput, IMCAufgabeNames.PROP_TABELLE_GENERIEREN);
			dbc.bindValue(observableWidget, observableProps);

			// D
			observableWidget = SWTObservables.observeText(controls[8], SWT.Modify);
			observableProps = BeansObservables.observeValue(pInput, IMCAufgabeNames.PROP_ANTWORT_D);
			dbc.bindValue(observableWidget, observableProps);

			observableWidget = SWTObservables.observeEnabled(controls[8]);
			observableProps = BeansObservables.observeValue(pInput, IMCAufgabeNames.PROP_TABELLE_GENERIEREN);
			dbc.bindValue(observableWidget, observableProps);

			observableWidget = SWTObservables.observeEditable(controls[8]);
			observableProps = BeansObservables.observeValue(pInput, IMCAufgabeNames.PROP_TABELLE_GENERIEREN);
			dbc.bindValue(observableWidget, observableProps);

			// E
			observableWidget = SWTObservables.observeText(controls[9], SWT.Modify);
			observableProps = BeansObservables.observeValue(pInput, IMCAufgabeNames.PROP_ANTWORT_E);
			dbc.bindValue(observableWidget, observableProps);

			observableWidget = SWTObservables.observeEnabled(controls[9]);
			observableProps = BeansObservables.observeValue(pInput, IMCAufgabeNames.PROP_TABELLE_GENERIEREN);
			dbc.bindValue(observableWidget, observableProps);

			observableWidget = SWTObservables.observeEditable(controls[9]);
			observableProps = BeansObservables.observeValue(pInput, IMCAufgabeNames.PROP_TABELLE_GENERIEREN);
			dbc.bindValue(observableWidget, observableProps);
		}

		// Lösungsbuchstabe
		if (controls[10] instanceof Text) {
			observableWidget = SWTObservables.observeText(controls[10], SWT.Modify);
		} else {
			observableWidget = SWTObservables.observeText(controls[10]);
		}
		observableProps = BeansObservables.observeValue(pInput, IMCAufgabeNames.PROP_LOESUNGSBUCHSTABE);
		targetToModelStrategy = new UpdateValueStrategy();
		targetToModelStrategy.setConverter(new StringToMCAntwortbuchstabenConverter());
		modelToTargetStrategy = new UpdateValueStrategy();
		modelToTargetStrategy.setConverter(new MCAntwortbuchstabenToStringConverter());
		dbc.bindValue(observableWidget, observableProps, targetToModelStrategy, modelToTargetStrategy);

		// Punkte
		observableWidget = SWTObservables.observeText(controls[11], SWT.Modify);
		observableProps = BeansObservables.observeValue(pInput, IMCAufgabeNames.PROP_PUNKTE);
		targetToModelStrategy = new UpdateValueStrategy();
		targetToModelStrategy.setBeforeSetValidator(new IValidator() {
			@Override
			public IStatus validate(Object pArg0) {
				if (pArg0 == null) {
					return new Status(IStatus.OK, MatheJungAltActivator.PLUGIN_ID, "");
				}
				try {
					int value = (int) pArg0;
					if (value != 3 && value != 4 && value != 5) {
						return new Status(IStatus.ERROR, MatheJungAltActivator.PLUGIN_ID, "Nur 3, 4 oder 5 erlaubt.");
					}
					return new Status(IStatus.OK, MatheJungAltActivator.PLUGIN_ID, "");
				} catch (NumberFormatException e) {
					return new Status(IStatus.ERROR, MatheJungAltActivator.PLUGIN_ID, "Nur Zahlen erlaubt.");
				}
			}
		});
		dbc.bindValue(observableWidget, observableProps, targetToModelStrategy, null);

		observableWidget = SWTObservables.observeText(controls[12], SWT.Modify);
		observableProps = BeansObservables.observeValue(pInput, IMCAufgabeNames.PROP_QUELLE);
		dbc.bindValue(observableWidget, observableProps);

		observableWidget = SWTObservables.observeText(controls[13], SWT.Modify);
		observableProps = BeansObservables.observeValue(pInput, IMCAufgabeNames.PROP_TITEL);
		dbc.bindValue(observableWidget, observableProps);

		observableWidget = SWTObservables.observeText(controls[14], SWT.Modify);
		observableProps = BeansObservables.observeValue(pInput, IMCAufgabeNames.PROP_BEMERKUNG);
		dbc.bindValue(observableWidget, observableProps);

		// anzahl Antworten
		observableWidget = SWTObservables.observeText(controls[15], SWT.Modify);
		observableProps = BeansObservables.observeValue(pInput, IMCAufgabeNames.PROP_ANZAHL_ANTWORTVOESCHLAEGE);
		targetToModelStrategy = new UpdateValueStrategy();
		targetToModelStrategy.setBeforeSetValidator(new IValidator() {
			@Override
			public IStatus validate(Object pArg0) {
				if (pArg0 == null) {
					return new Status(IStatus.OK, MatheJungAltActivator.PLUGIN_ID, "");
				}
				try {
					int value = (int) pArg0;
					if (value <= 1 || value > 5) {
						return new Status(IStatus.ERROR, MatheJungAltActivator.PLUGIN_ID, "Nur 2, 3, 4 oder 5 erlaubt.");
					}
					return new Status(IStatus.OK, MatheJungAltActivator.PLUGIN_ID, "");
				} catch (NumberFormatException e) {
					return new Status(IStatus.ERROR, MatheJungAltActivator.PLUGIN_ID, "Nur Zahlen erlaubt.");
				}
			}
		});
		dbc.bindValue(observableWidget, observableProps, targetToModelStrategy, null);
	}
}
