package de.egladil.mathejungalt.ui.app;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

import de.egladil.mathejungalt.ui.app.views.ArbeitsblaetterView;
import de.egladil.mathejungalt.ui.app.views.AufgabenView;
import de.egladil.mathejungalt.ui.app.views.MinikaenguruView;

/**
 * @author aheike
 */
public class AufgabensammlungenPerspective implements IPerspectiveFactory {

	public static final String ID = "de.egladil.mathejungalt.ui.app.aufgabensammlungenPerspective";

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IPerspectiveFactory#createInitialLayout(org.eclipse.ui.IPageLayout)
	 */
	public void createInitialLayout(IPageLayout layout) {
		String editorArea = layout.getEditorArea();
		layout.setEditorAreaVisible(true);
		layout.setFixed(false);
		layout.setEditorAreaVisible(true);
		IFolderLayout folder = layout.createFolder("aufgabenammlungenViewsFolder", IPageLayout.LEFT, 0.4f, editorArea);
		folder.addView(AufgabenView.ID);
		folder.addView(MinikaenguruView.ID);
		folder.addView(ArbeitsblaetterView.ID);
	}
}
