/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.dnd;

import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.swt.dnd.DropTargetEvent;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;

/**
 * In den Aufgabensammlung-Editor kann eine Aufgabe gedropt werden. In diesem Fall wird ein neues Item erzeugt.
 * 
 * @author winkelv
 */
public class AufgabensammlungitemsDropTargetListener extends StructuredViewerDropTargetAdapter {

	/**
	 * @param pTargetObject
	 */
	public AufgabensammlungitemsDropTargetListener(AbstractMatheAGObject pTargetObject) {
		super(pTargetObject);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.dnd.DropTargetAdapter#drop(org.eclipse.swt.dnd.DropTargetEvent)
	 */
	@Override
	public void drop(DropTargetEvent pEvent) {
		if (LocalSelectionTransfer.getTransfer().isSupportedType(pEvent.currentDataType)) {
			AbstractMatheAGObject domainObject = getDragedObject();
			if (domainObject == null) {
				return;
			}
			IAufgabensammlung aufgbensammlung = (IAufgabensammlung) getTargetObject();
			if (aufgbensammlung.getBeendet() == 1) {
				return;
			}
			if (domainObject instanceof Aufgabe) {
				Aufgabe aufgabe = (Aufgabe) domainObject;
				if (aufgbensammlung.canAdd(aufgabe)) {
					MatheJungAltActivator.getDefault().getStammdatenservice()
						.aufgabeZuSammlungHinzufuegen(aufgbensammlung, aufgabe);
				}
			}
		}
	}
}
