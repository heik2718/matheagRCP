/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors;

import java.beans.PropertyChangeEvent;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.PartInitException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.exceptions.MatheJungAltInconsistentEntityException;
import de.egladil.mathejungalt.domain.IMatheAGObjectNames;
import de.egladil.mathejungalt.domain.mitglieder.IMitgliedNames;
import de.egladil.mathejungalt.domain.mitglieder.ISerienteilnahmeNames;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.MitgliedEditorInput;
import de.egladil.mathejungalt.ui.app.editors.pages.MitgliedAttributesFormPage;

/**
 * @author Winkelv
 */
public class MitgliedEditor extends AbstractMatheEditor {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(MitgliedEditor.class);

	/** */
	public static final String ID = "de.egladil.mathejungalt.ui.app.editors.MitgliedEditor";

	/**
	 *
	 */
	public MitgliedEditor() {
		super();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.AbstractMatheEditor#checkEditorInput(org.eclipse.ui.IEditorInput)
	 */
	@Override
	protected void checkEditorInput(IEditorInput pInput) throws PartInitException {
		if (!(pInput instanceof MitgliedEditorInput)) {
			throw new PartInitException("Ungültiger Input: muss MitgliedEditorInput sein!");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.AbstractMatheEditor#interestingPropertyChangeEvent(java.beans.PropertyChangeEvent)
	 */
	@Override
	protected boolean interestingPropertyChangeEvent(PropertyChangeEvent pEvt) {
		String property = pEvt.getPropertyName();
		Assert.isNotNull(property);
		if (property.equals(IMatheAGObjectNames.PROP_SCHLUESSEL))
			return true;
		if (property.equals(IMatheAGObjectNames.PROP_ID))
			return true;
		if (property.equals(IMitgliedNames.PROP_SERIENTEILNAHMEN))
			return true;
		if (property.equals(IMitgliedNames.PROP_ALTER))
			return true;
		if (property.equals(IMitgliedNames.PROP_ANZAHL_DIPLOME))
			return true;
		if (property.equals(IMitgliedNames.PROP_ERFINDERPUNKTE))
			return true;
		if (property.equals(IMitgliedNames.PROP_FOTOTITEL))
			return true;
		if (property.equals(IMitgliedNames.PROP_GEBURTSJAHR))
			return true;
		if (property.equals(IMitgliedNames.PROP_GEBURTSMONAT))
			return true;
		if (property.equals(IMitgliedNames.PROP_KLASSE))
			return true;
		if (property.equals(IMitgliedNames.PROP_KONTAKT))
			return true;
		if (property.equals(IMitgliedNames.PROP_NACHNAME))
			return true;
		if (property.equals(IMitgliedNames.PROP_VORNAME))
			return true;
		if (property.equals(ISerienteilnahmeNames.PROP_PUNKTE))
			return true;
		if (property.equals(ISerienteilnahmeNames.PROP_FRUEHSTARTER))
			return true;
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.AbstractMatheEditor#isTitleChangingEvent(java.beans.PropertyChangeEvent)
	 */
	@Override
	protected boolean isTitleChangingEvent(PropertyChangeEvent pEvt) {
		String property = pEvt.getPropertyName();
		Assert.isNotNull(property);
		if (property.equals(IMitgliedNames.PROP_VORNAME)) {
			return true;
		}
		if (property.equals(IMitgliedNames.PROP_KLASSE)) {
			return true;
		}
		if (property.equals(IMitgliedNames.PROP_ALTER)) {
			return true;
		}
		if (property.equals(IMatheAGObjectNames.PROP_ID)) {
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.AbstractMatheEditor#updateTitel()
	 */
	@Override
	protected void updateTitel() {
		Mitglied mitglied = (Mitglied) getDomainObject();
		if (mitglied != null && !mitglied.isNew() && mitglied.getVorname().length() > 0) {
			setPartName(mitglied.getVorname() + "(" + mitglied.getAlter().toString() + ","
				+ mitglied.getKlasse().toString() + ")");
		} else {
			setPartName("neues Mitglied");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.FormEditor#addPages()
	 */
	@Override
	protected void addPages() {
		try {
			int index = addPage(new MitgliedAttributesFormPage(this, getClass().getName(), "Mitglied"));
			setPageText(index, "Mitglied");
		} catch (PartInitException e) {
			String msg = "Fehler beim Hinzuf�gen von Seiten zum " + getClass().getName();
			LOG.error(msg);
			throw new MatheJungAltException(msg, e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public void doSave(IProgressMonitor pMonitor) {
		commitPages(true);
		Mitglied mitglied = (Mitglied) getDomainObject();
		try {
			getStammdatenservice().mitgliedSpeichern(mitglied);
			((MitgliedEditorInput) getEditorInput()).applyChanges();
			setModified(false);
		} catch (MatheJungAltInconsistentEntityException e) {
			setModified(true);
			MessageDialog.openWarning(Display.getCurrent().getActiveShell(), "Warnung", e.getMessage());
		} catch (Exception e) {
			setModified(true);
			String msg = "Das Mitglied konnte wegen eines MatheAG-Core-Fehlers nicht gespeichert werden:\n"
				+ e.getMessage();
			LOG.error(msg, e);
			MessageDialog.openError(Display.getCurrent().getActiveShell(), "Fehler!", msg);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.AbstractMatheEditor#getId()
	 */
	@Override
	public String getId() {
		return ID;
	}
}
