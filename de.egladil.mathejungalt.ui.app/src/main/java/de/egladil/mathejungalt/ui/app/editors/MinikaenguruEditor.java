/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors;

import java.beans.PropertyChangeEvent;

import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.PartInitException;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.IMinikaenguruNames;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.MinikaenguruEditorInput;
import de.egladil.mathejungalt.ui.app.editors.pages.MinikaenguruAttributesFormPage;

/**
 * @author winkelv
 */
public class MinikaenguruEditor extends AbstractAufgabensammlungEditor {

	/** */
	public static final String ID = MinikaenguruEditor.class.getName();

	/**
	 *
	 */
	public MinikaenguruEditor() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.AbstractMatheAGEditor#checkEditorInput(org.eclipse.ui.IEditorInput)
	 */
	@Override
	protected void checkEditorInput(IEditorInput pInput) throws PartInitException {
		if (!(pInput instanceof MinikaenguruEditorInput))
			throw new PartInitException("Ungültiger Input: muss MinikaenguruEditorInput sein!");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.AbstractMatheAGEditor#isAnEditorTitleChangingEvent(java.beans.PropertyChangeEvent)
	 */
	@Override
	protected boolean isTitleChangingEvent(PropertyChangeEvent pEvt) {
		String property = pEvt.getPropertyName();
		return property.equals(IMinikaenguruNames.PROP_JAHR);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.rcp.editors.AbstractMatheAGEditor#isNotAnInterestingPropertyChangeEvent(java.beans.
	 * PropertyChangeEvent)
	 */
	@Override
	protected boolean interestingPropertyChangeEvent(PropertyChangeEvent pEvt) {
		if (super.interestingPropertyChangeEvent(pEvt)) {
			return true;
		}
		String prop = pEvt.getPropertyName();
		return IMinikaenguruNames.PROP_JAHR.equals(prop) || IMinikaenguruNames.PROP_MINKAENGURUITEMS.equals(prop);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.AbstractMatheAGEditor#updateTitel()
	 */
	@Override
	protected void updateTitel() {
		Minikaenguru minikaenguru = (Minikaenguru) getDomainObject();
		setPartName(minikaenguru.getJahr() == null ? "neuer Minik\u00E4nguru" : "Minik\u00E4nguru "
			+ minikaenguru.getJahr());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.FormEditor#addPages()
	 */
	@Override
	protected void addPages() {
		try {
			int index = addPage(new MinikaenguruAttributesFormPage(this, getClass().getName(), "Minik\u00E4nguru"));
			setPageText(index, "Minik\u00E4nguru");
		} catch (PartInitException e) {
			throw new MatheJungAltException(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.AbstractMatheEditor#getId()
	 */
	@Override
	public String getId() {
		return ID;
	}
}
