/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.listeners;

import java.beans.PropertyChangeListener;

import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.events.SelectionListener;

import de.egladil.mathejungalt.service.stammdaten.event.IModelChangeListener;

/**
 * Fasst alle Listener-Typen f+r den ViewerPart eines StructuredViewerListeners zusammen.
 * 
 * @author Winkelv
 */
public interface IStructuredViewerPartViewerListener extends SelectionListener, ISelectionChangedListener,
	IDoubleClickListener, PropertyChangeListener, IModelChangeListener {

	/**
	 * Die "Zur-Liste-Hinzufügen-Aktion"
	 */
	public void handleAdd();

	/**
	 * Die Editor-öffnen-Aktion
	 * 
	 * @param pSelection {@link IStructuredSelection} das ausgewählte Element
	 */
	public void handleEdit(IStructuredSelection pSelection);

	/**
	 * Entfernt das gewählte Element aus der Liste
	 */
	public void handleRemove();
}
