/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IPathEditorInput;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.editor.IFormPage;
import org.eclipse.ui.texteditor.AbstractTextEditor;

import de.egladil.mathejungalt.ui.app.editors.documentproviders.SimpleDocumentProvider;

/**
 * Text-Editor f�r das zugeh�rige LaTeX-File
 * 
 * @author Winkelv
 */
public class AufgabeLaTeXEditor extends AbstractTextEditor implements IFormPage {

	/** */
	public static final String ID_AUFGABEN = AufgabeLaTeXEditor.class.getName() + "_aufgaben"; // $NON-NLS-1$

	/** */
	public static final String ID_LOESUNGEN = AufgabeLaTeXEditor.class.getName() + "loesungen"; // $NON-NLS-1$

	/** */
	private FormEditor formEditor;

	/** */
	private String pageId;

	/** */
	private int index;

	/** */
	private Control control;

	/** */
	private boolean modified;

	/**
	 * 
	 */
	public AufgabeLaTeXEditor() {
		super();
		setKeyBindingScopes(new String[] { "org.eclipse.ui.textEditorScope" }); // $NON-NLS-1$
		internalInit();
	}

	/**
	 * @param pFormEditor
	 * @param pPageId
	 */
	public AufgabeLaTeXEditor(FormEditor pFormEditor, String pPageId, IPathEditorInput pInput) {
		this();
		formEditor = pFormEditor;
		pageId = pPageId;
		// wichtig, damit der Editor seinen Input auch bekommt, wenn er nur als Teil eines Multi-Page-Editors verwendet
		// wird
		setSite(pFormEditor.getSite());
		setInput(pInput);
	}

	/**
	 * 
	 */
	private void internalInit() {
		configureInsertMode(SMART_INSERT, false);
		setDocumentProvider(new SimpleDocumentProvider(this));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.IFormPage#canLeaveThePage()
	 */
	@Override
	public boolean canLeaveThePage() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.IFormPage#getEditor()
	 */
	@Override
	public FormEditor getEditor() {
		return formEditor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.IFormPage#getId()
	 */
	@Override
	public String getId() {
		return pageId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.IFormPage#getIndex()
	 */
	@Override
	public int getIndex() {
		return index;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.IFormPage#getManagedForm()
	 */
	@Override
	public IManagedForm getManagedForm() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.texteditor.AbstractTextEditor#createPartControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createPartControl(Composite pParent) {
		super.createPartControl(pParent);
		Control[] children = pParent.getChildren();
		control = children[children.length - 1];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.IFormPage#getPartControl()
	 */
	@Override
	public Control getPartControl() {
		return control;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.IFormPage#initialize(org.eclipse.ui.forms.editor.FormEditor)
	 */
	@Override
	public void initialize(FormEditor pEditor) {
		formEditor = pEditor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.IFormPage#isActive()
	 */
	@Override
	public boolean isActive() {
		return this.equals(formEditor.getActivePageInstance());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.IFormPage#isEditor()
	 */
	@Override
	public boolean isEditor() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.IFormPage#selectReveal(java.lang.Object)
	 */
	@Override
	public boolean selectReveal(Object pObject) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.IFormPage#setActive(boolean)
	 */
	@Override
	public void setActive(boolean pActive) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.IFormPage#setIndex(int)
	 */
	@Override
	public void setIndex(int pIndex) {
		index = pIndex;
	}

	/**
	 * @return the inputChanged
	 */
	public final boolean isModified() {
		return modified;
	}

	/**
	 * @param pModified the modified to set
	 */
	public final void setModified(boolean pModified) {
		modified = pModified;
		((AbstractMatheEditor) formEditor).setModified(modified);
	}
}
