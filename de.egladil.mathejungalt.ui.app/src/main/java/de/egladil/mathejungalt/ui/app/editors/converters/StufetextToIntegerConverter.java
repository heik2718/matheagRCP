/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.converters;

import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Konvertiert einen Stufenstring in einen Integer
 * 
 * @author Winkelv
 */
public class StufetextToIntegerConverter implements IConverter {

	/**
	 * 
	 */
	public StufetextToIntegerConverter() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object pFromObject) {
		String param = (String) pFromObject;
		if (ITextConstants.STUFE_1.equals(param)) {
			return new Integer(1);
		}
		if (ITextConstants.STUFE_2.equals(param)) {
			return new Integer(2);
		}
		if (ITextConstants.STUFE_3.equals(param)) {
			return new Integer(3);
		}
		if (ITextConstants.STUFE_4.equals(param)) {
			return new Integer(4);
		}
		if (ITextConstants.STUFE_5.equals(param)) {
			return new Integer(5);
		}
		if (ITextConstants.STUFE_6.equals(param)) {
			return new Integer(6);
		}
		if (ITextConstants.PUNKTE_3.equals(param)) {
			return new Integer(3);
		}
		if (ITextConstants.PUNKTE_4.equals(param)) {
			return new Integer(4);
		}
		if (ITextConstants.PUNKTE_5.equals(param)) {
			return new Integer(5);
		}
		if (ITextConstants.MONOID_1.equals(param)) {
			return new Integer(1);
		}
		if (ITextConstants.MONOID_2.equals(param)) {
			return new Integer(2);
		}
		throw new IllegalArgumentException("unbekannter Text " + param);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getFromType()
	 */
	@Override
	public Object getFromType() {
		return String.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getToType()
	 */
	@Override
	public Object getToType() {
		return Integer.class;
	}
}
