/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.viewactions;

import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.viewers.StructuredViewer;

import de.egladil.mathejungalt.ui.app.views.filters.AbstractMatheAGObjectFilter;
import de.egladil.mathejungalt.ui.app.views.filters.AufgabenartNegativeFilter;

/**
 * @author winkelv
 */
public abstract class AbstractAusblendenAufgabenartAction extends AbstractAusblendenAufgabenAction {

	/**
	 * 
	 */
	public AbstractAusblendenAufgabenartAction() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.rcp.views.actions.AusblendenAufgabenAction#getFilter(de.egladil.mathe.rcp.views.
	 * AbstractMatheAGStructuredView )
	 */
	@Override
	protected AbstractMatheAGObjectFilter getFilter(StructuredViewer pView) {
		Assert.isNotNull(pView, "getFilter(pViews)");
		return new AufgabenartNegativeFilter(pView, getStammdatenservice());
	}

}
