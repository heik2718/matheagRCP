/**
 * 
 */
package de.egladil.mathejungalt.ui.app.dialogs;

import org.eclipse.jface.wizard.Wizard;

import de.egladil.mathejungalt.domain.medien.Buch;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * @author Winkelv
 */
public class CreateBuchquelleWizard extends Wizard {

	/** */
	private SelectBuchWizardPage selectBuchPage;

	/** */
	private CreateQuelleWizardSelectionPage selectionPage;

	/** */
	private IStammdatenservice stammdatenservice;

	/**
	 * 
	 */
	public CreateBuchquelleWizard(CreateQuelleWizardSelectionPage pSelectionPage, IStammdatenservice pStammdatenservice) {
		super();
		setWindowTitle(ITextConstants.QUELLE_BUCH_WIZARD_TITLE);
		selectionPage = pSelectionPage;
		stammdatenservice = pStammdatenservice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		Buch buch = (Buch) selectBuchPage.getFirstSelectedDomainObject();
		if (buch == null) {
			return false;
		}
		selectionPage.setQuelle(stammdatenservice.buchquelleFindenOderAnlegen(buch, selectBuchPage.getSeite()));
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#addPages()
	 */
	@Override
	public void addPages() {
		selectBuchPage = new SelectBuchWizardPage();
		addPage(selectBuchPage);
	}
}
