/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors;

import java.beans.PropertyChangeEvent;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.PartInitException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.exceptions.MatheJungAltInconsistentEntityException;
import de.egladil.mathejungalt.domain.mcraetsel.IMCArchivraetselNames;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.MCRaetselEditorInput;
import de.egladil.mathejungalt.ui.app.editors.pages.MCRaetselAttributesFormPage;

/**
 * @author winkelv
 */
public class MCRaetselEditor extends AbstractMatheEditor {

	/** */
	public static final String ID = MCRaetselEditor.class.getName();

	private static final Logger LOG = LoggerFactory.getLogger(MCRaetselEditor.class);

	/**
	 *
	 */
	public MCRaetselEditor() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.AbstractMatheAGEditor#checkEditorInput(org.eclipse.ui.IEditorInput)
	 */
	@Override
	protected void checkEditorInput(IEditorInput pInput) throws PartInitException {
		if (!(pInput instanceof MCRaetselEditorInput)) {
			throw new PartInitException("Ungültiger Input: muss MCRaetselEditorInput sein!");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.AbstractMatheAGEditor#isAnEditorTitleChangingEvent(java.beans.PropertyChangeEvent)
	 */
	@Override
	protected boolean isTitleChangingEvent(PropertyChangeEvent pEvt) {
		String property = pEvt.getPropertyName();
		Assert.isNotNull(property);
		if (IMCArchivraetselNames.PROP_SCHLUESSEL.equals(property)) {
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.rcp.editors.AbstractMatheAGEditor#isNotAnInterestingPropertyChangeEvent(java.beans.
	 * PropertyChangeEvent)
	 */
	@Override
	protected boolean interestingPropertyChangeEvent(PropertyChangeEvent pEvt) {
		String prop = pEvt.getPropertyName();
		return IMCArchivraetselNames.PROP_AUTOR.equals(prop) || IMCArchivraetselNames.PROP_ITEMS.equals(prop)
			|| IMCArchivraetselNames.PROP_LICENSE_FULL.equals(prop)
			|| IMCArchivraetselNames.PROP_LICENSE_SHORT.equals(prop) || IMCArchivraetselNames.PROP_STUFE.equals(prop)
			|| IMCArchivraetselNames.PROP_TITEL.equals(prop) || IMCArchivraetselNames.PROP_PRIVAT.equals(prop)
			|| IMCArchivraetselNames.PROP_VEROEFFENTLICHT.equals(prop) || IMCArchivraetselNames.PROP_ID.equals(prop)
			|| MCArchivraetselItem.PROP_NUMMER.equals(prop) || IMCArchivraetselNames.PROP_SCHLUESSEL.equals(prop);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.AbstractMatheAGEditor#updateTitel()
	 */
	@Override
	protected void updateTitel() {
		MCArchivraetsel serie = (MCArchivraetsel) getDomainObject();
		setPartName(serie.getSchluessel() == null ? "neues Quiz" : "Quiz " + serie.getSchluessel());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.FormEditor#addPages()
	 */
	@Override
	protected void addPages() {
		try {
			int index = addPage(new MCRaetselAttributesFormPage(this, getClass().getName(), "Quiz"));
			setPageText(index, "Quiz");
		} catch (PartInitException e) {
			throw new MatheJungAltException(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.AbstractMatheEditor#getId()
	 */
	@Override
	public String getId() {
		return ID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public void doSave(IProgressMonitor pArg0) {
		commitPages(true);
		MCArchivraetsel raetsel = (MCArchivraetsel) getDomainObject();
		try {
			getStammdatenservice().raetselSpeichern(raetsel);
			((MCRaetselEditorInput) getEditorInput()).applyChanges();
			setModified(false);
		} catch (MatheJungAltInconsistentEntityException e) {
			setModified(true);
			MessageDialog.openWarning(Display.getCurrent().getActiveShell(), "Warnung", e.getMessage());
		} catch (Exception e) {
			setModified(true);
			String msg = "Das Quiz konnte wegen eines MatheAG-Core-Fehlers nicht gespeichert werden:\n"
				+ e.getMessage();
			LOG.error(msg, e);
			MessageDialog.openError(Display.getCurrent().getActiveShell(), "Fehler!", msg);
		}
	}
}
