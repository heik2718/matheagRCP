/**
 *
 */
package de.egladil.mathejungalt.ui.app.views.contentproviders;

import java.util.ArrayList;

import org.eclipse.jface.viewers.IStructuredContentProvider;

import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * @author Heike Winkelvoß
 */
public class MCRaetselitemViewContentProvider extends AbstractMatheObjectsViewContentProvider implements
	IStructuredContentProvider {

	/**
	 * @param pStammdatenservice
	 */
	public MCRaetselitemViewContentProvider(IStammdatenservice pStammdatenservice) {
		super(pStammdatenservice);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
	 */
	@Override
	public Object[] getElements(Object pInputElement) {
		if (pInputElement == null || !(pInputElement instanceof MCArchivraetsel))
			return new ArrayList<Serie>().toArray();
		MCArchivraetsel raetsel = (MCArchivraetsel) pInputElement;
		if (!raetsel.isNew() && !raetsel.isItemsLoaded()) {
			getStammdatenservice().raetselitemsLaden(raetsel);
		}
		return raetsel.getItems().toArray();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
	 */
	@Override
	public void dispose() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.rcp.contentproviders.AbstractSerienViewContentProvider#isInterestingChangeEvent(java.beans.
	 * PropertyChangeEvent )
	 */
	@Override
	public boolean isInterestingChangeEvent(String pPropertyName) {
		if (MCArchivraetselItem.PROP_NUMMER.equals(pPropertyName)
			|| MCArchivraetselItem.PROP_AUFGABE.equals(pPropertyName)) {
			return true;
		}
		return false;
	}
}
