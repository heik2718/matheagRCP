/**
 *
 */
package de.egladil.mathejungalt.ui.app.handlers;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * @author aheike
 */
public class MailadresseHinzufuegenHandler extends AbstractSelectionHandler {

	/** */
	private IStammdatenservice stammdatenservice;

	/**
	 * @param pSelectionProvider
	 */
	public MailadresseHinzufuegenHandler(final ISelectionProvider pSelectionProvider, final IStammdatenservice pStammdatenservice) {
		super(pSelectionProvider);
		stammdatenservice = pStammdatenservice;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.ui.app.handlers.AbstractSelectionHandler#run()
	 */
	@Override
	protected void run() {
		final AbstractMatheAGObject object = getFirstSelectedObject();
		if (!(object instanceof Mitglied)) {
			return;
		}
		final Mitglied mitglied = (Mitglied) object;
		final Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		try {
			stammdatenservice.mailadresseZurMailinglisteHinzufuegen(mitglied.getKontakt());
			MessageDialog.openInformation(shell, "Mailingliste", "Kontakt '" + mitglied.getKontakt()
				+ "' zur Mailingliste hinzugefuegt.");
		} catch (final MatheJungAltException e) {
			MessageDialog.openError(shell, "Fehler Mailadresse", "Kontakt '" + mitglied.getKontakt()
				+ "' konnte nicht zur Mailingliste hinzugefuegt werden \n" + e.getMessage());
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.ui.app.handlers.AbstractSelectionHandler#isEnabled()
	 */
	@Override
	public boolean isEnabled() {
		if (!super.isEnabled()) {
			return false;
		}
		if (!(getFirstSelectedObject() instanceof Mitglied)) {
			return false;
		}
		final Mitglied mitglied = (Mitglied) getFirstSelectedObject();
		if (mitglied.getKontakt().indexOf('@') < 0) {
			return false;
		}
		return !stammdatenservice.mailadresseInMailinglisteEnthalten(mitglied.getKontakt());
	}
}
