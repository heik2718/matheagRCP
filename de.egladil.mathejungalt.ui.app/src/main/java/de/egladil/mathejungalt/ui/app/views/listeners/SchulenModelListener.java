/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.listeners;

import java.beans.PropertyChangeEvent;

import org.eclipse.jface.viewers.TableViewer;

import de.egladil.mathejungalt.domain.IMatheAGObjectNames;
import de.egladil.mathejungalt.domain.schulen.ISchuleNames;
import de.egladil.mathejungalt.domain.schulen.Schule;
import de.egladil.mathejungalt.service.stammdaten.event.ModelChangeEvent;

/**
 * @author aheike
 */
public class SchulenModelListener extends AbstractModelListener {

	/**
	 * @param pViewer
	 */
	public SchulenModelListener(TableViewer pViewer) {
		super(pViewer);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.views.listeners.AbstractModelListener#isInterestingEvent(de.egladil.mathejungalt
	 * .service.stammdaten.event.ModelChangeEvent)
	 */
	@Override
	protected boolean isInterestingEvent(ModelChangeEvent pEvt) {
		if (ModelChangeEvent.ADD_EVENT.equals(pEvt.getEventType()) && pEvt.getNewValue() instanceof Schule) {
			return true;
		}
		if (ModelChangeEvent.REMOVE_EVENT.equals(pEvt.getEventType()) && pEvt.getOldValue() instanceof Schule) {
			return true;
		}
		if (ModelChangeEvent.PROPERTY_CHANGE_EVENT.equals(pEvt.getEventType()) && !(pEvt.getSource() instanceof Schule)) {
			return false;
		}
		final String property = pEvt.getProperty();
		return isRelevant(property);
	}

	/**
	 * @param property
	 * @return
	 */
	private boolean isRelevant(final String property) {
		return IMatheAGObjectNames.PROP_SCHLUESSEL.equals(property) || ISchuleNames.PROP_ANSCHRIFT.equals(property)
			|| ISchuleNames.PROP_MINI_TEILNAHMEN.equals(property) || ISchuleNames.PROP_LAND.equals(property)
			|| ISchuleNames.PROP_NAME.equals(property) || ISchuleNames.PROP_TELEFONNUMMER.equals(property);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.ui.app.views.listeners.AbstractModelListener#isInterestingEvent(java.beans.
	 * PropertyChangeEvent)
	 */
	@Override
	protected boolean isInterestingEvent(PropertyChangeEvent pEvt) {
		if (!(pEvt.getSource() instanceof Schule)) {
			return false;
		}
		return isRelevant(pEvt.getPropertyName());
	}
}
