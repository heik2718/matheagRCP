/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.converters;

import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenzweck;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Konvertiert einen String in einen {@link Aufgabenzweck}
 * 
 * @author Winkelv
 */
public class StringToAufgabenzweckConverter implements IConverter {

	/**
	 * 
	 */
	public StringToAufgabenzweckConverter() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object pFromObject) {
		String param = (String) pFromObject;
		if (ITextConstants.ZWECK_SERIE.equals(param)) {
			return Aufgabenzweck.S;
		}
		if (ITextConstants.ZWECK_MINIKAENGURU.equals(param)) {
			return Aufgabenzweck.K;
		}
		if (ITextConstants.ZWECK_MONOID.equals(param)) {
			return Aufgabenzweck.M;
		}
		throw new IllegalArgumentException("unbekannter Aufgabenzweck " + param);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getFromType()
	 */
	@Override
	public Object getFromType() {
		return String.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getToType()
	 */
	@Override
	public Object getToType() {
		return Aufgabenzweck.class;
	}
}
