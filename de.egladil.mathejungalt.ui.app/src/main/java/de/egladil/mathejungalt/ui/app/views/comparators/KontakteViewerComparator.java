/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.comparators;

import de.egladil.mathejungalt.domain.schulen.Kontakt;

/**
 * @author Winkelv
 */
public class KontakteViewerComparator extends AbstractViewerComparator {

	/**
	 * @param pIndex
	 */
	public KontakteViewerComparator(int pIndex) {
		super(pIndex);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Object pO1, Object pO2) {
		switch (getIndex()) {
		case 0:
			return ((Kontakt) pO1).getSchluessel().compareTo(((Kontakt) pO2).getSchluessel());
		case 1:
			return ((Kontakt) pO1).getName().compareToIgnoreCase(((Kontakt) pO2).getName());
		case 2:
			return ((Kontakt) pO1).getEmail().compareToIgnoreCase(((Kontakt) pO2).getEmail());
		default:
			Kontakt k1 = (Kontakt) pO1;
			Kontakt k2 = (Kontakt) pO2;
			if (k1.isAbo() == k2.isAbo()) {
				return 0;
			}
			if (!k1.isAbo() && k2.isAbo()) {
				return -1;
			}
			return 1;
		}
	}
}
