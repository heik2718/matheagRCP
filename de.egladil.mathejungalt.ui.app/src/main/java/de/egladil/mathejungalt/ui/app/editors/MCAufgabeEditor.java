/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors;

import java.beans.PropertyChangeEvent;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPathEditorInput;
import org.eclipse.ui.PartInitException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.exceptions.MatheJungAltInconsistentEntityException;
import de.egladil.mathejungalt.domain.mcraetsel.IMCAufgabeNames;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.MCAufgabeEditorInput;
import de.egladil.mathejungalt.ui.app.editors.pages.AufgabePreviewFormPage;
import de.egladil.mathejungalt.ui.app.editors.pages.MCAufgabeAttributesFormPage;
import de.egladil.mathejungalt.ui.app.handlers.OpenExternalTextFileHandlerDelegate;

/**
 * @author Winkelv
 */
public class MCAufgabeEditor extends AbstractMatheEditor {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(MCAufgabeEditor.class);

	/** */
	public static final String ID = "de.egladil.mathejungalt.ui.app.editors.MCAufgabeEditor";

	/**
	 *
	 */
	public MCAufgabeEditor() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.AbstractMatheEditor#checkEditorInput(org.eclipse.ui.IEditorInput)
	 */
	@Override
	protected void checkEditorInput(IEditorInput pInput) throws PartInitException {
		if (!(pInput instanceof MCAufgabeEditorInput)) {
			throw new PartInitException("Ungültiger Input: muss MCAufgabeEditorInput sein!");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.AbstractMatheEditor#interestingPropertyChangeEvent(java.beans.PropertyChangeEvent)
	 */
	@Override
	protected boolean interestingPropertyChangeEvent(PropertyChangeEvent pEvt) {
		String property = pEvt.getPropertyName();
		Assert.isNotNull(property);
		if (IMCAufgabeNames.PROP_SCHLUESSEL.equals(property)) {
			return true;
		}
		if (IMCAufgabeNames.PROP_ART.equals(property)) {
			return true;
		}
		if (IMCAufgabeNames.PROP_ANTWORT_A.equals(property)) {
			return true;
		}
		if (IMCAufgabeNames.PROP_ANTWORT_B.equals(property)) {
			return true;
		}
		if (IMCAufgabeNames.PROP_ANTWORT_C.equals(property)) {
			return true;
		}
		if (IMCAufgabeNames.PROP_ANTWORT_D.equals(property)) {
			return true;
		}
		if (IMCAufgabeNames.PROP_ANTWORT_E.equals(property)) {
			return true;
		}
		if (IMCAufgabeNames.PROP_BEMERKUNG.equals(property)) {
			return true;
		}
		if (IMCAufgabeNames.PROP_GESPERRT.equals(property)) {
			return false;
		}
		if (IMCAufgabeNames.PROP_PUNKTE.equals(property)) {
			return true;
		}
		if (IMCAufgabeNames.PROP_QUELLE.equals(property)) {
			return true;
		}
		if (IMCAufgabeNames.PROP_TABELLE_GENERIEREN.equals(property)) {
			return true;
		}
		if (IMCAufgabeNames.PROP_STUFE.equals(property)) {
			return true;
		}
		if (IMCAufgabeNames.PROP_THEMA.equals(property)) {
			return true;
		}
		if (IMCAufgabeNames.PROP_TITEL.equals(property)) {
			return true;
		}
		if (IMCAufgabeNames.PROP_VERZEICHNIS.equals(property)) {
			return true;
		}
		if (IMCAufgabeNames.PROP_ID.equals(property)) {
			return true;
		}
		if (IMCAufgabeNames.PROP_LOESUNGSBUCHSTABE.equals(property)) {
			return true;
		}
		if (IMCAufgabeNames.PROP_ANZAHL_ANTWORTVOESCHLAEGE.equals(property)) {
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.AbstractMatheEditor#isTitleChangingEvent(java.beans.PropertyChangeEvent)
	 */
	@Override
	protected boolean isTitleChangingEvent(PropertyChangeEvent pEvt) {
		String property = pEvt.getPropertyName();
		Assert.isNotNull(property);
		if (IMCAufgabeNames.PROP_SCHLUESSEL.equals(property)) {
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.AbstractMatheEditor#updateTitel()
	 */
	@Override
	protected void updateTitel() {
		MCAufgabe aufgabe = (MCAufgabe) getDomainObject();
		if (aufgabe != null) {
			setPartName("MC-" + aufgabe.getSchluessel());
		} else {
			setPartName("neue Quizaufgabe");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.FormEditor#addPages()
	 */
	@Override
	protected void addPages() {
		try {
			final MCAufgabeAttributesFormPage attributesFormPage = new MCAufgabeAttributesFormPage(this, getClass()
				.getName(), "Quizaufgabe");
			int index = addPage(attributesFormPage);
			setPageText(index, "Aufgabeninfos");
			IPathEditorInput[] inputs = new OpenExternalTextFileHandlerDelegate()
				.createEditorInput((MCAufgabe) getDomainObject());
			final AufgabeLaTeXEditor aufgabeLaTeXPage = new AufgabeLaTeXEditor(this, AufgabeLaTeXEditor.ID_AUFGABEN,
				inputs[0]);
			index = addPage(aufgabeLaTeXPage);
			setPageText(index, "LaTeX Aufgabe");
			final AufgabeLaTeXEditor loesungLaTeXPage = new AufgabeLaTeXEditor(this, AufgabeLaTeXEditor.ID_LOESUNGEN,
				inputs[1]);
			index = addPage(loesungLaTeXPage);
			setPageText(index, "LaTeX L\u00F6sung");
			final AufgabePreviewFormPage aufgabePreview = new AufgabePreviewFormPage(this, true);
			index = addPage(aufgabePreview);
			setPageText(index, "png Aufgabe");
			final AufgabePreviewFormPage loesungPreview = new AufgabePreviewFormPage(this, false);
			index = addPage(loesungPreview);
			setPageText(index, "png L\u00F6sung");
		} catch (PartInitException e) {
			String msg = "Fehler beim Hinzufügen von Seiten zum " + getClass().getName();
			LOG.error(msg);
			throw new MatheJungAltException(msg, e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public void doSave(IProgressMonitor pMonitor) {
		commitPages(true);
		MCAufgabe aufgabe = (MCAufgabe) getDomainObject();
		try {
			getStammdatenservice().quizAufgabeSpeichern(aufgabe);
			((MCAufgabeEditorInput) getEditorInput()).applyChanges();
			setModified(false);
		} catch (MatheJungAltInconsistentEntityException e) {
			setModified(true);
			MessageDialog.openWarning(Display.getCurrent().getActiveShell(), "Warnung", e.getMessage());
		} catch (Exception e) {
			setModified(true);
			String msg = "Die Quizaufgabe konnte wegen eines MatheAG-Core-Fehlers nicht gespeichert werden:\n"
				+ e.getMessage();
			LOG.error(msg, e);
			MessageDialog.openError(Display.getCurrent().getActiveShell(), "Fehler!", msg);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.AbstractMatheEditor#getId()
	 */
	@Override
	public String getId() {
		return ID;
	}
}
