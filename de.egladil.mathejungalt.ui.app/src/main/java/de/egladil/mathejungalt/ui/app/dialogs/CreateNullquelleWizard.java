/**
 * 
 */
package de.egladil.mathejungalt.ui.app.dialogs;

import org.eclipse.jface.wizard.Wizard;

import de.egladil.mathejungalt.domain.quellen.NullQuelle;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * @author Winkelv
 */
public class CreateNullquelleWizard extends Wizard {

	/** */
	private ShowNullquelleWizardPage selectWizardPage;

	/** */
	private CreateQuelleWizardSelectionPage selectionPage;

	/** */
	private IStammdatenservice stammdatenservice;

	/**
	 * 
	 */
	public CreateNullquelleWizard(CreateQuelleWizardSelectionPage pSelectionPage, IStammdatenservice pStammdatenservice) {
		super();
		setWindowTitle(ITextConstants.QUELLE_NULL_WIZARD_TITLE);
		selectionPage = pSelectionPage;
		stammdatenservice = pStammdatenservice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		NullQuelle selectedObject = (NullQuelle) selectWizardPage.getFirstSelectedDomainObject();
		if (selectedObject == null) {
			return false;
		}
		selectionPage.setQuelle(selectedObject);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#addPages()
	 */
	@Override
	public void addPages() {
		selectWizardPage = new ShowNullquelleWizardPage(stammdatenservice);
		addPage(selectWizardPage);
	}
}
