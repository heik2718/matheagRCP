/**
 * 
 */
package de.egladil.mathejungalt.ui.app.dialogs;

import org.eclipse.jface.wizard.Wizard;

import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * @author Winkelv
 */
public class CreateKindquelleWizard extends Wizard {

	/** */
	private SelectMitgliedWizardPage selectMitgliedWizardPage;

	/** */
	private CreateQuelleWizardSelectionPage selectionPage;

	/** */
	private IStammdatenservice stammdatenservice;

	/**
	 * 
	 */
	public CreateKindquelleWizard(CreateQuelleWizardSelectionPage pSelectionPage, IStammdatenservice pStammdatenservice) {
		super();
		setWindowTitle(ITextConstants.QUELLE_KIND_WIZARD_TITLE);
		selectionPage = pSelectionPage;
		stammdatenservice = pStammdatenservice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		Mitglied selectedObject = (Mitglied) selectMitgliedWizardPage.getFirstSelectedDomainObject();
		if (selectedObject == null) {
			return false;
		}
		selectionPage.setQuelle(stammdatenservice.kindquelleFindenOderAnlegen(selectedObject,
			selectedObject.getAlter(), selectedObject.getKlasse()));
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#addPages()
	 */
	@Override
	public void addPages() {
		selectMitgliedWizardPage = new SelectMitgliedWizardPage();
		addPage(selectMitgliedWizardPage);
	}
}
