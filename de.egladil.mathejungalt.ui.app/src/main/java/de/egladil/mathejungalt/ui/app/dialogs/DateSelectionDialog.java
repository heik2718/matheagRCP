/**
 *
 */
package de.egladil.mathejungalt.ui.app.dialogs;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.eclipse.jface.dialogs.TrayDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Shell;

import de.egladil.base.exceptions.EgladilUIException;

/**
 * @author winkelv
 */
public class DateSelectionDialog extends TrayDialog {

	private Date date;

	private DateTime dateTimeControl;

	/**
	 * @param pParentShell
	 * @param pCalendar ein {@link Calendar} oder null. Bei null wird das aktuelle Datum verwendet.
	 */
	public DateSelectionDialog(Shell pParentShell, Date pCalendar) {
		super(pParentShell);
		if (pCalendar != null) {
			date = pCalendar;
		} else {
			date = new Date();
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.jface.dialogs.TitleAreaDialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true));

		dateTimeControl = new DateTime(container, SWT.CALENDAR);
		final GridData gd = new GridData(SWT.CENTER, SWT.CENTER, true, true);
		dateTimeControl.setLayoutData(gd);

		dateTimeControl.setDate(Integer.valueOf(new SimpleDateFormat("yyyy").format(date)),
			Integer.valueOf(new SimpleDateFormat("MM").format(date)),
			Integer.valueOf(new SimpleDateFormat("dd").format(date)));

		return container;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.jface.dialogs.TitleAreaDialog#getInitialSize()
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(300, 250);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	public void okPressed() {
		int year = dateTimeControl.getYear();
		int month = dateTimeControl.getMonth();
		int day = dateTimeControl.getDay();

		Date d;
		try {
			d = new SimpleDateFormat("ddMMyyyy").parse("" + day + month + year);
		} catch (ParseException e) {
			throw new EgladilUIException(e.getMessage());
		}

		date.setTime(d.getTime());

		super.okPressed();
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}
}
