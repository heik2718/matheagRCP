package de.egladil.mathejungalt.ui.app.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public class PreferenceConstants {

	public static final String P_PATH_AUFGABENARCHIV_DIR = "aufgabenarchivDir";

	public static final String P_PATH_LOESUNGENARCHIV_DIR = "loesungenarchivDir";

	public static final String P_PATH_GENERATOR_OUT_DIR = "generatorOutDir";

	public static final String P_PATH_PNG_DIR = "pngDir";

	public static final String P_PATH_QUIZAUFGABENARCHIV_DIR = "quizaufgabenarchivDir";

	public static final String P_PATH_QUIZLOESUNGENARCHIV_DIR = "quizloesungenarchivDir";

	public static final String P_PATH_QUIZ_PNG_DIR = "quizPngDir";

	public static final String P_RAETSEL_GENERATOR_MODUS = "raetselGeneratorModus";

	public static final String P_STUFE_6_SPERREN = "stufe6MaximalEinmal";

	public static final String P_AKTUELLES_JAHR_MINIKAENGURU = "aktuellesJahrMini";

	public static final String PATH_ROOT_DIR = "C:/work/knobelarchiv_2/";

	public static final String PATH_WORKDIR = PATH_ROOT_DIR + "latex/";

	public static final String DEFAULT_AUFGABENARCHIV = PATH_WORKDIR + "aufgabenarchiv";

	public static final String DEFAULT_LOESUNGSARCHIV = PATH_WORKDIR + "loesungsarchiv";

	public static final String DEFAULT_GENERATED = PATH_WORKDIR + "tempdir";

	public static final String DEFAULT_MC_AUFGABENARCHIV = PATH_WORKDIR + "mcaufgabenarchiv";

	public static final String DEFAULT_MC_LOESUNGSARCHIV = PATH_WORKDIR + "mcloesungsarchiv";

	public static final String DEFAULT_PNGS = PATH_ROOT_DIR + "pngs";

	public static final String DEFAULT_QUIZPNGS = PATH_ROOT_DIR + "quizpngs";

	public static final String DEFAULT_RAETSEL_GENERATOR_MODUS = "WEB";
}
