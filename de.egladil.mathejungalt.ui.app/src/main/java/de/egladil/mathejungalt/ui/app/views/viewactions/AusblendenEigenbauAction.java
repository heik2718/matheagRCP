/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.viewactions;

import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenart;

/**
 * Blendet die Aufgaben aus, die Zitate sind
 * 
 * @author Winkelv
 */
public class AusblendenEigenbauAction extends AbstractAusblendenAufgabenartAction {

	/**
	 * 
	 */
	public AusblendenEigenbauAction() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.views.actions.AusblendenAufgabenAction#getSpecialPattern()
	 */
	@Override
	protected String getSpecialPattern() {
		return Aufgabenart.E.toString();
	}

}
