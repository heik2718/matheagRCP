/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.converters;

import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Konvertiert einen Integer in eine Stufe-Text
 * 
 * @author Winkelv
 */
public class IntegerToSerieStufetextConverter implements IConverter {

	/**
	 * 
	 */
	public IntegerToSerieStufetextConverter() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object pFromObject) {
		int param = ((Integer) pFromObject).intValue();
		switch (param) {
		case 1:
			return ITextConstants.STUFE_1;
		case 2:
			return ITextConstants.STUFE_2;
		case 3:
			return ITextConstants.STUFE_3;
		case 4:
			return ITextConstants.STUFE_4;
		case 5:
			return ITextConstants.STUFE_5;
		case 6:
			return ITextConstants.STUFE_6;
		default:
			return "falsche Serie-Stufe " + param;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getFromType()
	 */
	@Override
	public Object getFromType() {
		return Integer.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getToType()
	 */
	@Override
	public Object getToType() {
		return String.class;
	}
}
