/**
 *
 */
package de.egladil.mathejungalt.ui.app.views.labelproviders;

import de.egladil.mathejungalt.domain.schulen.Schule;

/**
 * @author aheike
 */
public class SchulenLabelProvider extends CommonColumnLabelProvider {

	private static final String NULL_BUNDESLAND = "";

	/**
	 *
	 */
	public SchulenLabelProvider(int pIndex) {
		super(pIndex);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ColumnLabelProvider#getText(java.lang.Object)
	 */
	@Override
	public String getText(Object pElement) {
		switch (getIndex()) {
		case 0:
			return ((Schule) pElement).getSchluessel();
		case 1:
			return ((Schule) pElement).getName();
		default:
			Schule schule = (Schule) pElement;
			return schule.getLand() == null ? NULL_BUNDESLAND : schule.getLand().getSchluessel() + " - "
				+ schule.getLand().getBezeichnung();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.CellLabelProvider#getToolTipText(java.lang.Object)
	 */
	@Override
	public String getToolTipText(Object pElement) {
		final Schule schule = (Schule) pElement;
		String suffix = schule.getAnschrift() == null ? "" : schule.getAnschrift();
		return schule.getId() == null ? suffix : schule.getId().toString() + ":" + suffix;
	}

}
