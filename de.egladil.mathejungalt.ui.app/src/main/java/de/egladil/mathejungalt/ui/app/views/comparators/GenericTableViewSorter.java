/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.comparators;

import java.util.Comparator;

import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.TableColumn;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.ui.app.views.ITableViewColumn;

/**
 * Generischer Sortierer f�r einen TableViewer. Mit diesem Sortierer k�nnen Tabellenspalten durch Anklicken des Headers
 * alphanumerisch oder sonstwie auf- und absteigend sortiert werden.
 * 
 * @author Winkelv
 */
public class GenericTableViewSorter extends ViewerSorter {

	/** */
	private TableViewer viewer;

	/**  */
	private SortInfo[] sortInfos;

	/**
	 * Value-Objekt zur Parameterübergabe an die SortiererFactory.
	 * 
	 * @author Winkelv
	 */
	private class SortInfo {

		/** */
		int columnIndex;

		/** */
		IViewerComparator comparator;

		/** */
		boolean descending;

		/**
		 * 
		 */
		public SortInfo() {
		}
	}

	/**
	 * Die Sortierinfo wird anhand des {@link TableViewerColumn}- Arrays und eines {@link Comparator}- Arrays erzeugt.
	 * 
	 * @param pViewer {@link TableViewer} der Viewer
	 * @param pColumns Array von {@link TableColumn}
	 * @param pComparators Array von {@link Comparator}
	 */
	public GenericTableViewSorter(final TableViewer pViewer, TableViewerColumn[] pColumns,
		AbstractViewerComparator[] pComparators) {
		Assert.isTrue(pColumns.length == pComparators.length, "pColumns und pComparators sind verschieden lang.");
		viewer = pViewer;
		sortInfos = new SortInfo[pColumns.length];
		for (int i = 0; i < pColumns.length; i++) {
			sortInfos[i] = new SortInfo();
			sortInfos[i].columnIndex = i;
			sortInfos[i].comparator = pComparators[i];
			sortInfos[i].descending = false;
			createSelectionListener(pColumns[i], sortInfos[i]);
		}
	}

	/**
	 * @param pViewer
	 * @param pTableColumns
	 * @param pViewColumns
	 */
	public GenericTableViewSorter(TableViewer pViewer, TableViewerColumn[] pTableColumns,
		ITableViewColumn[] pViewColumns) {
		Assert.isTrue(pTableColumns.length == pViewColumns.length,
			"pTableColumns und pViewColumns sind verschieden lang.");
		viewer = pViewer;
		sortInfos = new SortInfo[pTableColumns.length];
		for (int i = 0; i < pTableColumns.length; i++) {
			sortInfos[i] = new SortInfo();
			sortInfos[i].columnIndex = i;
			sortInfos[i].comparator = pViewColumns[i].getComparator();
			sortInfos[i].descending = false;
			createSelectionListener(pTableColumns[i], sortInfos[i]);
		}
	}

	/**
	 * Registriert SelectionListener ans der gegebenen Spalte.
	 * 
	 * @param pTableColumn {@link TableColumn} die Spalte
	 * @param pSortInfo {@link SortInfo} die gew�nschte Sortierung.
	 */
	private void createSelectionListener(final TableViewerColumn pTableColumn, final SortInfo pSortInfo) {
		pTableColumn.getColumn().addSelectionListener(new SelectionAdapter() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent pE) {
				swappSortInfoUpOrSwitchSortingOrder(pSortInfo);
			}
		});

	}

	/**
	 * Die SortInfo pSortInfo wird innerhalb der sortInfos ganz nach oben getauscht, falls sie noch nicht oben steht.
	 * Falls pSortInfo bereits erstes Arrayelement ist, wird die Sortierrichtung dieses Elements getauscht.
	 * 
	 * @param pSortInfo
	 */
	protected void swappSortInfoUpOrSwitchSortingOrder(SortInfo pSortInfo) {
		if (pSortInfo == sortInfos[0])
			pSortInfo.descending = !pSortInfo.descending;
		else {
			for (int i = 0; i < sortInfos.length; i++) {
				if (pSortInfo == sortInfos[i]) {
					/*
					 * Dieser Aufruf zieht die gew�nschte SortInfo aus dem Stapel. Dadurch rutschen alle dar�ber
					 * liegenden SortInfos nach unten und der Platz 0 wird frei.
					 */
					System.arraycopy(sortInfos, 0, sortInfos, 1, i);
					// Die gew�nschte SortInfo wird auf Platz 0 gesetzt, also ganz nach oben.
					sortInfos[0] = pSortInfo;
					pSortInfo.descending = false;
					break;
				}
			}
		}
		viewer.refresh();
	}

	/**
	 * Die Sortierung erfolgt sequentiell in der Reihenfolge des {@link SortInfo}- Arrays. Mittels eines
	 * Sch�ttelalgorithmus ist daf�r gesorgt, dass die gew�nschte {@link SortInfo} als erste steht und alle anderen nach
	 * aufsteigendem Index folgen.
	 */
	@Override
	public int compare(Viewer pViewer, Object pE1, Object pE2) {
		for (int i = 0; i < sortInfos.length; i++) {
			int result = sortInfos[i].comparator.compare((AbstractMatheAGObject) pE1, (AbstractMatheAGObject) pE2);
			if (result != 0) {
				if (sortInfos[i].descending)
					return -result;
				return result;
			}
		}
		return 0;
	}
}
