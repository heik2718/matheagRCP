/**
 * 
 */
package de.egladil.mathejungalt.ui.app.dialogs.labelproviders;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.themes.IImageKeys;

/**
 * @author Winkelv
 */
public class MitgliedListLabelProvider extends LabelProvider {

	/**
	 * 
	 */
	public MitgliedListLabelProvider() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.LabelProvider#getImage(java.lang.Object)
	 */
	@Override
	public Image getImage(Object pElement) {
		return MatheJungAltActivator.getDefault().getImage(IImageKeys.MITGLIED);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
	 */
	@Override
	public String getText(Object pElement) {
		Mitglied mitglied = (Mitglied) pElement;
		StringBuffer sb = new StringBuffer(150);
		sb.append(mitglied.getVorname());
		sb.append("-");
		if (mitglied.getNachname() != null) {
			sb.append(mitglied.getNachname());
		}
		sb.append("-");
		sb.append(mitglied.getKlasse());
		sb.append("-");
		sb.append(mitglied.getAlter());
		sb.append("-");
		sb.append(mitglied.getSchluessel());
		return sb.toString();
	}

}
