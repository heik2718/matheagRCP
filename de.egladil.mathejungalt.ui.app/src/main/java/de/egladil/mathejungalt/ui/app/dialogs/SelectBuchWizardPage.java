/**
 * 
 */
package de.egladil.mathejungalt.ui.app.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import de.egladil.mathejungalt.domain.medien.Buch;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.dialogs.labelproviders.BuchListLabelProvider;
import de.egladil.mathejungalt.ui.app.themes.IImageKeys;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * @author Winkelv
 */
public class SelectBuchWizardPage extends AbstractSelectMatheAGObjectWizardPage {

	/** */
	private Text seiteText;

	/** */
	private Integer seite;

	/**
	 * @param pPageName
	 */
	public SelectBuchWizardPage() {
		super(ITextConstants.QUELLE_SELECT_BUCH_PAGE_NAME, ITextConstants.QUELLE_SELECT_BUCH_PAGE_TITLE, null,
			new BuchListLabelProvider(), SWT.SINGLE);
		setDescription(ITextConstants.QUELLE_SELECT_BUCH_PAGE_DESCR);
		setImageDescriptor(MatheJungAltActivator.imageDescriptorFromPlugin(MatheJungAltActivator.PLUGIN_ID,
			IImageKeys.QUELLE_WIZARD));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.dialogs.AbstractSelectMatheAGObjectWizardPage#hookAdditionalControls(org.eclipse.swt.widgets
	 * .Composite)
	 */
	protected void hookAdditionalControls(Composite pParent) {
		Composite container = new Composite(pParent, SWT.NULL);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		container.setLayout(gridLayout);
		setControl(container);

		final Label seiteLabel = new Label(container, SWT.NONE);
		final GridData gd2 = new GridData();
		gd2.horizontalSpan = 1;
		seiteLabel.setLayoutData(gd2);
		seiteLabel.setText(ITextConstants.LABEL_TEXT_SEITE);

		seiteText = new Text(container, SWT.BORDER);
		seiteText.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		seiteText.setFont(pParent.getFont());
		seiteText.setToolTipText(ITextConstants.QUELLE_SELECT_SEITE_TOOLTIP);
		GridData data = new GridData();
		data.grabExcessVerticalSpace = false;
		data.grabExcessHorizontalSpace = true;
		data.horizontalAlignment = GridData.FILL;
		data.verticalAlignment = GridData.BEGINNING;
		seiteText.setLayoutData(data);
		ModifyListener listener = new ModifyListener() {

			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.ModifyListener#modifyText(org.eclipse.swt.events.ModifyEvent)
			 */
			@Override
			public void modifyText(ModifyEvent pE) {
				validateInput();
			}

		};
		seiteText.addModifyListener(listener);
		seiteText.setText("");
	}

	/**
	 * 
	 */
	private void validateInput() {
		try {
			seite = Integer.parseInt(seiteText.getText());
			setPageComplete(true);
		} catch (NumberFormatException e) {
			seite = null;
			setPageComplete(false);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.dialogs.AbstractSelectMatheAGObjectWizardPage#getListElements()
	 */
	@Override
	protected Object[] getListElements() {
		return MatheJungAltActivator.getDefault().getTableViewerContentManager().getElements(Buch.class.getName());
	}

	/**
	 * @return the seite
	 */
	public final Integer getSeite() {
		return seite;
	}
}
