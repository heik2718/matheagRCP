/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.IArbeitsblattNames;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;
import de.egladil.mathejungalt.ui.app.themes.PartControlsFactory;

/**
 * @author winkelv
 */
public class ArbeitsblattSimpleAttributePartDataBindingProvider extends
	AbstractAufgabensammlungSimpleAttributePartDataBindingProvider {

	/**
	 * 
	 */
	public ArbeitsblattSimpleAttributePartDataBindingProvider() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.contentproviders.AbstractAttributePartContentsProvider#anzahlZeilen()
	 */
	@Override
	protected int anzahlZeilen() {
		return 4;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.parts.AbstractSimpleAttributePartControlsProvider#initControls(org.eclipse.swt.widgets
	 * .Composite , org.eclipse.ui.forms.widgets.FormToolkit, boolean)
	 */
	@Override
	protected void initControls(Composite pClient, FormToolkit pToolkit, boolean pEditable, AbstractMatheAGObject pInput) {
		super.initControls(pClient, pToolkit, pEditable, pInput);
		final Control[] controls = getControls();
		Control[] labels = new Control[controls.length];

		boolean editable = ((Arbeitsblatt) pInput).getBeendet() == 0;

		int widthHint = 150;
		labels[2] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_SCHLUESSEL);
		controls[2] = PartControlsFactory.createText(pClient, pToolkit, widthHint, editable);
		((Text) controls[2]).setTextLimit(IArbeitsblattNames.LENGTH_SCHLUESSEL);

		labels[3] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_TITEL);
		controls[3] = PartControlsFactory.createText(pClient, pToolkit, widthHint, editable);
		((Text) controls[3]).setTextLimit(IArbeitsblattNames.LENGTH_TITEL);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.contentproviders.IAttributePartContentsProvider#refreshDataBindings(de.egladil.mathe
	 * .core.domain .AbstractMatheAGObject)
	 */
	@Override
	public void refreshDataBindings(AbstractMatheAGObject pInput) {
		super.refreshDataBindings(pInput);

		Binding[] bindings = getBindings();
		Control[] controls = getControls();

		IObservableValue observableWidget = SWTObservables.observeText(controls[2], SWT.Modify);
		IObservableValue observableProperty = BeansObservables.observeValue(pInput, IArbeitsblattNames.PROP_SCHLUESSEL);
		bindings[2] = getDbc().bindValue(observableWidget, observableProperty, null, null);

		observableWidget = SWTObservables.observeText(controls[3], SWT.Modify);
		observableProperty = BeansObservables.observeValue(pInput, IArbeitsblattNames.PROP_TITEL);
		bindings[3] = getDbc().bindValue(observableWidget, observableProperty, null, null);
	}
}
