/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.ui.app.views;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.handlers.IHandlerService;

import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.ui.app.editors.KontaktEditor;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.AbstractMatheEditorInput;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.KontaktEditorInput;
import de.egladil.mathejungalt.ui.app.handlers.ICommandIds;
import de.egladil.mathejungalt.ui.app.handlers.ObjektAnlegenHandler;
import de.egladil.mathejungalt.ui.app.themes.TableViewerFactory;
import de.egladil.mathejungalt.ui.app.views.comparators.AbstractViewerComparator;
import de.egladil.mathejungalt.ui.app.views.comparators.IViewerComparator;
import de.egladil.mathejungalt.ui.app.views.comparators.KontakteViewerComparator;
import de.egladil.mathejungalt.ui.app.views.contentproviders.KontakteViewContentProvider;
import de.egladil.mathejungalt.ui.app.views.labelproviders.CommonColumnLabelProvider;
import de.egladil.mathejungalt.ui.app.views.labelproviders.KontakteLabelProvider;
import de.egladil.mathejungalt.ui.app.views.listeners.AbstractModelListener;
import de.egladil.mathejungalt.ui.app.views.listeners.KontakteModelListener;

/**
 * <p>
 * NOTE: THIS WIDGET AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 * 
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class KontakteView extends AbstractMatheJungAltView {

	/** */
	public static final String ID = "de.egladil.mathejungalt.ui.app.views.KontakteView";

	/** */
	public static final String[] columnTitles = new String[] { "Schl\u00fcssel", "Name", "Bundesland" };

	/** */
	private static final int[] columnBounds = new int[] { 60, 150, 200 };

	/**
   * 
   */
	public KontakteView() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#getModelListener(org.eclipse.jface.viewers.TableViewer
	 * )
	 */
	@Override
	protected AbstractModelListener getModelListener(TableViewer pViewer) {
		return new KontakteModelListener(pViewer);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#getEditorInput(java.lang.Object)
	 */
	@Override
	protected AbstractMatheEditorInput getEditorInput(Object pObj) {
		if (!(pObj instanceof Kontakt)) {
			return null;
		}
		Kontakt kontakt = (Kontakt) pObj;
		if (!kontakt.isSchulkontakteGeladen()) {
			getStammdatenservice().schulkontakteLaden(kontakt);
		}
		return new KontaktEditorInput(kontakt);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#getEditorID()
	 */
	@Override
	protected String getEditorID() {
		return KontaktEditor.ID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#initTableViewer(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected TableViewer initTableViewer(Composite pParent) {
		ITableViewColumn[] viewColumns = new ITableViewColumn[columnTitles.length];
		IViewerComparator[] comparators = getColumnComparators();
		ColumnLabelProvider[] labelProviders = getColumnLableProviders();
		for (int i = 0; i < columnTitles.length; i++) {
			viewColumns[i] = new ViewColumn(columnTitles[i], columnBounds[i], comparators[i], labelProviders[i]);
		}
		return TableViewerFactory.createTableViewer(pParent, new KontakteViewContentProvider(getStammdatenservice()),
			viewColumns, true);
	}

	/**
	 * Erzeugt abhängig vom Spaltenindex einen {@link ColumnLabelProvider}
	 * 
	 * @param pInd int der Spaltenindex
	 * @return {@link ColumnLabelProvider}
	 */
	public static ColumnLabelProvider[] getColumnLableProviders() {
		CommonColumnLabelProvider[] labelProviders = new CommonColumnLabelProvider[columnTitles.length];

		for (int i = 0; i < labelProviders.length; i++) {
			labelProviders[i] = new KontakteLabelProvider(i);
		}
		return labelProviders;
	}

	/**
	 * @return
	 */
	public static AbstractViewerComparator[] getColumnComparators() {
		AbstractViewerComparator[] comparators = new KontakteViewerComparator[columnTitles.length];
		for (int i = 0; i < comparators.length; i++) {
			comparators[i] = new KontakteViewerComparator(i);
		}
		return comparators;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#createHandlers()
	 */
	@Override
	protected void createHandlers() {
		super.createHandlers();
		IHandlerService handlerService = (IHandlerService) getSite().getService(IHandlerService.class);
		handlerService.activateHandler(ICommandIds.ANLEGEN, new ObjektAnlegenHandler(getStammdatenservice(), ID));
	}
}
