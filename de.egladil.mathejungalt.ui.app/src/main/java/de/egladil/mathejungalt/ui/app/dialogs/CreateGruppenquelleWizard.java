/**
 * 
 */
package de.egladil.mathejungalt.ui.app.dialogs;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.eclipse.jface.wizard.Wizard;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * @author Winkelv
 */
public class CreateGruppenquelleWizard extends Wizard {

	/** */
	private SelectMitgliederWizardPage selectMitgliederWizardPage;

	/** */
	private CreateQuelleWizardSelectionPage selectionPage;

	/** */
	private IStammdatenservice stammdatenservice;

	/**
	 * 
	 */
	public CreateGruppenquelleWizard(CreateQuelleWizardSelectionPage pSelectionPage,
		IStammdatenservice pStammdatenservice) {
		super();
		setWindowTitle(ITextConstants.QUELLE_GRUPPE_WIZARD_TITLE);
		selectionPage = pSelectionPage;
		stammdatenservice = pStammdatenservice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		List<AbstractMatheAGObject> selectedObjects = selectMitgliederWizardPage.getSelectedDomainObjects();
		if (selectedObjects == null || selectedObjects.size() < 2) {
			return false;
		}
		Collection<Mitglied> mitglieder = new HashSet<Mitglied>();
		for (AbstractMatheAGObject obj : selectedObjects) {
			if (obj instanceof Mitglied) {
				mitglieder.add((Mitglied) obj);
			}
		}
		selectionPage.setQuelle(stammdatenservice.gruppenquelleAnlegen(mitglieder));
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#addPages()
	 */
	@Override
	public void addPages() {
		selectMitgliederWizardPage = new SelectMitgliederWizardPage();
		addPage(selectMitgliederWizardPage);
	}
}
