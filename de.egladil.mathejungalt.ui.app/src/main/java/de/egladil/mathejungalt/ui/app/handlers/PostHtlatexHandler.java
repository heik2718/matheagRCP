/**
 *
 */
package de.egladil.mathejungalt.ui.app.handlers;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.ui.PlatformUI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mathejungalt.config.IMatheJungAltConfiguration.RaetsellisteGeneratorModus;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.preferences.PreferenceConstants;

/**
 * Bereitet das ausführen von htlatex für ein gewähltes MCArchivrätsel vor.
 *
 * @author aheike
 */
public class PostHtlatexHandler extends AbstractSelectionHandler {

	private static final Logger LOG = LoggerFactory.getLogger(PostHtlatexHandler.class);

	/** */
	private IStammdatenservice stammdatenservice;

	private String errm = "";

	private IStatus status = null;

	/**
	 * @param pSelectionProvider
	 */
	public PostHtlatexHandler(ISelectionProvider pSelectionProvider, IStammdatenservice pStammdatenservice) {
		super(pSelectionProvider);
		stammdatenservice = pStammdatenservice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.handlers.AbstractSelectionHandler#run()
	 */
	@Override
	protected void run() {
		final AbstractMatheAGObject object = getFirstSelectedObject();
		if (!(object instanceof MCArchivraetsel)) {
			return;
		}
		status = null;
		final MCArchivraetsel raetsel = (MCArchivraetsel) object;
		try {
			PlatformUI.getWorkbench().getProgressService().busyCursorWhile(new IRunnableWithProgress() {
				@Override
				public void run(IProgressMonitor pMonitor) throws InvocationTargetException, InterruptedException {
					try {
						pMonitor.beginTask("Generiere Dateien", 100);
						RaetsellisteGeneratorModus modus = MatheJungAltActivator.getDefault()
							.getRaetselListeGeneratorModus();
						status = stammdatenservice.processPostHtlatexTasks(raetsel, MatheJungAltActivator.getDefault()
							.getPath(PreferenceConstants.P_PATH_GENERATOR_OUT_DIR), modus, pMonitor);
						pMonitor.worked(100);
					} catch (Exception e) {
						errm = "Beim Generieren der Sourcen ist ein Fehler aufgetreten:\n" + e.getMessage();
						status = new Status(IStatus.ERROR, IStammdatenservice.PLUGIN_ID, errm);
						LOG.error(e.getMessage(), e);
					} finally {
						pMonitor.done();
					}
				}
			});
		} catch (InvocationTargetException e1) {
			status = new Status(IStatus.ERROR, IStammdatenservice.PLUGIN_ID, errm);
			errm = e1.getMessage();
		} catch (InterruptedException e1) {
			// ignore
		}
		if (status != null && !status.isOK()) {
			StringBuffer sb = new StringBuffer(status.getMessage());
			if (status.isMultiStatus()) {
				for (IStatus s : ((MultiStatus) status).getChildren()) {
					sb.append("\n");
					sb.append(s.getMessage());
				}
			}
			MessageDialog.openError(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Generatorfehler",
				sb.toString());
		} else {
			MessageDialog.openInformation(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Info",
				"Die Dateien wurden in das konfigurierte Temp-Verzeichnis generiert.");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.handlers.AbstractSelectionHandler#isEnabled()
	 */
	@Override
	public boolean isEnabled() {
		if (!super.isEnabled()) {
			return false;
		}
		AbstractMatheAGObject firstSelectedObject = getFirstSelectedObject();
		if (!(firstSelectedObject instanceof MCArchivraetsel)) {
			return false;
		}
		final String pathWorkDir = MatheJungAltActivator.getDefault().getPath(
			PreferenceConstants.P_PATH_GENERATOR_OUT_DIR);
		final boolean doit = stammdatenservice.canRunPostHtlatexProcessor((MCArchivraetsel) firstSelectedObject,
			pathWorkDir);
		return doit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.handlers.AbstractSelectionHandler#getCommandParameterId()
	 */
	@Override
	protected String getCommandParameterId() {
		return "de.egladil.mathejungalt.ui.app.generieren.objectType";
	}
}
