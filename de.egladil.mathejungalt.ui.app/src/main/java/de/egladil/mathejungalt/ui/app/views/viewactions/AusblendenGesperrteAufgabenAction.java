/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.viewactions;

import org.eclipse.jface.viewers.StructuredViewer;

import de.egladil.mathejungalt.ui.app.views.filters.AbstractMatheAGObjectFilter;
import de.egladil.mathejungalt.ui.app.views.filters.AufgabeFuerWettbewerbGesperrtNegativeFilter;

/**
 * @author winkelv
 */
public class AusblendenGesperrteAufgabenAction extends AbstractAusblendenAufgabenAction {

	public final static String ID = "de.egladil.mathejungalt.ui.app.views.viewactions.AusblendenGesperrteAufgabenAction";

	/**
	 * 
	 */
	public AusblendenGesperrteAufgabenAction() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.rcp.views.actions.AbstractAusblendenAufgabenAction#getFilter(de.egladil.mathe.rcp.views.
	 * AbstractMatheAGStructuredView)
	 */
	@Override
	protected AbstractMatheAGObjectFilter getFilter(StructuredViewer pView) {
		return new AufgabeFuerWettbewerbGesperrtNegativeFilter(pView, getStammdatenservice());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.views.actions.AbstractAusblendenAufgabenAction#getSpecialPattern()
	 */
	@Override
	protected String getSpecialPattern() {
		return "J";
	}
}
