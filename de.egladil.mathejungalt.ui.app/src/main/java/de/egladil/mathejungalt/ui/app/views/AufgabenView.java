package de.egladil.mathejungalt.ui.app.views;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.handlers.IHandlerService;

import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgaben.IAufgabeNames;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.IArbeitsblattNames;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.IMinikaenguruNames;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.ISerieNames;
import de.egladil.mathejungalt.service.stammdaten.event.IModelChangeListener;
import de.egladil.mathejungalt.service.stammdaten.event.ModelChangeEvent;
import de.egladil.mathejungalt.ui.app.editors.AufgabeEditor;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.AbstractMatheEditorInput;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.AufgabeEditorInput;
import de.egladil.mathejungalt.ui.app.handlers.AufgabenMarkSchlechtHandler;
import de.egladil.mathejungalt.ui.app.handlers.GenerierenHandler;
import de.egladil.mathejungalt.ui.app.handlers.ICommandIds;
import de.egladil.mathejungalt.ui.app.handlers.ObjektAnlegenHandler;
import de.egladil.mathejungalt.ui.app.themes.TableViewerFactory;
import de.egladil.mathejungalt.ui.app.views.comparators.AbstractViewerComparator;
import de.egladil.mathejungalt.ui.app.views.comparators.AufgabenViewerComparator;
import de.egladil.mathejungalt.ui.app.views.comparators.IViewerComparator;
import de.egladil.mathejungalt.ui.app.views.contentproviders.AufgabenViewContentProvider;
import de.egladil.mathejungalt.ui.app.views.labelproviders.AufgabenLabelProvider;
import de.egladil.mathejungalt.ui.app.views.labelproviders.CommonColumnLabelProvider;
import de.egladil.mathejungalt.ui.app.views.listeners.AbstractModelListener;
import de.egladil.mathejungalt.ui.app.views.listeners.AufgabenModelListener;
import de.egladil.mathejungalt.ui.app.views.viewactions.AufgabenTitelFilterAction;

/**
 * @author aheike
 */
public class AufgabenView extends AbstractMatheJungAltView implements IModelChangeListener {

	/** */
	public static final String ID = "de.egladil.mathejungalt.ui.app.aufgabenview";

	/** */
	private String[] columnTitles = new String[] { "Schl\u00FCssel", "Zweck", "Stufe", "Thema", "Titel" };

	/** */
	private int[] columnBounds = new int[] { 60, 50, 50, 50, 200 };

	/** */
	private AufgabenTitelFilterAction titelFilterAction;

	/**
	 * Erzeugen programmatisch die benötigten Handler. Diese sind aktiv nur wenn ein Vergleichsobjekt oder ein
	 * Ausschnitt ausgewählt wurde.
	 */
	protected void createHandlers() {
		super.createHandlers();
		IHandlerService handlerService = (IHandlerService) getSite().getService(IHandlerService.class);
		handlerService.activateHandler(ICommandIds.ANLEGEN, new ObjektAnlegenHandler(getStammdatenservice(), ID));
		handlerService.activateHandler(ICommandIds.FLAG_SCHLECHT_AENDERN, new AufgabenMarkSchlechtHandler(getViewer(),
			getStammdatenservice()));
		handlerService.activateHandler(ICommandIds.GENERIEREN, new GenerierenHandler(getViewer(),
			getStammdatenservice()));
		getStammdatenservice().addModelChangeListener(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#getModelListener(org.eclipse.jface.viewers.TableViewer
	 * )
	 */
	@Override
	protected AbstractModelListener getModelListener(TableViewer pViewer) {
		return new AufgabenModelListener(pViewer);
	}

	/**
	 * @param pParent
	 * @return
	 */
	protected TableViewer initTableViewer(Composite pParent) {
		ITableViewColumn[] viewColumns = new ITableViewColumn[columnTitles.length];
		IViewerComparator[] comparators = getColumnComparators();
		ColumnLabelProvider[] labelProviders = getColumnLableProviders();
		for (int i = 0; i < columnTitles.length; i++) {
			viewColumns[i] = new ViewColumn(columnTitles[i], columnBounds[i], comparators[i], labelProviders[i]);
		}
		return TableViewerFactory.createTableViewer(pParent, new AufgabenViewContentProvider(getStammdatenservice()),
			viewColumns, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#hookPullDownMenu()
	 */
	protected void hookPullDownMenu() {
		IMenuManager menu = getViewSite().getActionBars().getMenuManager();
		titelFilterAction = new AufgabenTitelFilterAction(getViewer(), "Filtern...", getStammdatenservice());
		menu.add(titelFilterAction);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#getEditorInput(java.lang.Object)
	 */
	@Override
	protected AbstractMatheEditorInput getEditorInput(Object pObj) {
		if (!(pObj instanceof Aufgabe)) {
			return null;
		}
		Aufgabe aufgabe = (Aufgabe) pObj;
		if (!aufgabe.getQuelle().isCompletelyLoaded()) {
			getStammdatenservice().quelleZuAufgabeLaden(aufgabe);
		}
		return new AufgabeEditorInput(aufgabe);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#getViewID()
	 */
	@Override
	protected String getEditorID() {
		return AufgabeEditor.ID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#isInterestingPropertyChangeEvent(de.egladil.
	 * mathejungalt.service.stammdaten.event.ModelChangeEvent)
	 */
	protected boolean isInterestingPropertyChangeEvent(ModelChangeEvent pEvt) {
		final String property = pEvt.getProperty();
		return pEvt.getNewValue() instanceof Aufgabe
			|| pEvt.getOldValue() instanceof Aufgabe
			|| pEvt.getSource() instanceof Aufgabe
			&& (IAufgabeNames.PROP_SCHLUESSEL.equals(property) || IAufgabeNames.PROP_ZWECK.equals(property)
				|| IAufgabeNames.PROP_STUFE.equals(property) || IAufgabeNames.PROP_THEMA.equals(property)
				|| IAufgabeNames.PROP_TITEL.equals(property) || ISerieNames.PROP_SERIENITEMS.equals(property)
				|| IMinikaenguruNames.PROP_MINKAENGURUITEMS.equals(property) || IArbeitsblattNames.PROP_ITEMS
					.equals(property));
	}

	/**
	 * Erzeugt abhängig vom Spaltenindex einen {@link ColumnLabelProvider}
	 *
	 * @param pInd int der Spaltenindex
	 * @return {@link ColumnLabelProvider}
	 */
	private ColumnLabelProvider[] getColumnLableProviders() {
		CommonColumnLabelProvider[] labelProviders = new CommonColumnLabelProvider[columnTitles.length];

		for (int i = 0; i < labelProviders.length; i++) {
			labelProviders[i] = new AufgabenLabelProvider(i);
		}
		return labelProviders;
	}

	/**
	 * @return
	 */
	private AbstractViewerComparator[] getColumnComparators() {
		AbstractViewerComparator[] comparators = new AufgabenViewerComparator[columnTitles.length];
		for (int i = 0; i < comparators.length; i++) {
			comparators[i] = new AufgabenViewerComparator(i);
		}
		return comparators;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.event.IModelChangeListener#modelChanged(de.egladil.mathejungalt.service
	 * .stammdaten.event.ModelChangeEvent)
	 */
	@Override
	public void modelChanged(ModelChangeEvent pEvt) {
		if (pEvt.getSource() instanceof Aufgabe) {
			getViewer().refresh(true);
		}
	}
}