/**
 *
 */
package de.egladil.mathejungalt.ui.app.views.labelproviders;

import de.egladil.mathejungalt.domain.schulen.Kontakt;

/**
 * @author aheike
 */
public class KontakteLabelProvider extends CommonColumnLabelProvider {

	/**
	 *
	 */
	public KontakteLabelProvider(int pIndex) {
		super(pIndex);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ColumnLabelProvider#getText(java.lang.Object)
	 */
	@Override
	public String getText(Object pElement) {
		switch (getIndex()) {
		case 0:
			return ((Kontakt) pElement).getSchluessel();
		case 1:
			return ((Kontakt) pElement).getName();
		case 2:
			return ((Kontakt) pElement).getEmail();
		default:
			return ((Kontakt) pElement).isAbo() ? "J" : "N";
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.CellLabelProvider#getToolTipText(java.lang.Object)
	 */
	@Override
	public String getToolTipText(Object pElement) {
		final Kontakt kontakt = (Kontakt) pElement;
		return kontakt.getId() == null ? "" : kontakt.getId().toString();
	}
}
