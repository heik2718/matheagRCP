/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.viewactions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.swt.widgets.Shell;

import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.views.filters.AbstractMatheAGObjectFilter;

/**
 * @author Winkelv
 */
public abstract class AbstractTitelFilterAction extends Action {

	/** */
	private final Shell shell;

	/** */
	private StructuredViewer viewer;

	/** */
	private IStammdatenservice stammdatenservice;

	/**
	 * 
	 */
	public AbstractTitelFilterAction(final StructuredViewer pViewer, final String pText,
		IStammdatenservice pStammdatenservice) {
		super(pText);
		shell = pViewer.getControl().getShell();
		viewer = pViewer;
		stammdatenservice = pStammdatenservice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		AbstractMatheAGObjectFilter filter = getFilter();
		InputDialog dialog = new InputDialog(shell, "Titel-Filter", "Hier ein Muster f\u00FCr einen Titel eintragen",
			filter.getPattern(), null);
		if (dialog.open() == InputDialog.OK) {
			filter.setPattern(dialog.getValue().trim(), true, false);
		}
	}

	/**
	 * @return
	 */
	protected abstract AbstractMatheAGObjectFilter getFilter();

	/**
	 * @return the viewer
	 */
	protected StructuredViewer getViewer() {
		return viewer;
	}

	/**
	 * @return the stammdatenservice
	 */
	protected IStammdatenservice getStammdatenservice() {
		return stammdatenservice;
	}
}
