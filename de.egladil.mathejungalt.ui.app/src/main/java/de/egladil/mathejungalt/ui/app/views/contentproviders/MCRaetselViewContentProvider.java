/**
 *
 */
package de.egladil.mathejungalt.ui.app.views.contentproviders;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

import de.egladil.mathejungalt.domain.mcraetsel.IMCArchivraetselNames;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * @author aheike
 */
public class MCRaetselViewContentProvider extends AbstractMatheObjectsViewContentProvider implements
	IStructuredContentProvider {

	/**
	 * @param pStammdatenservice
	 */
	public MCRaetselViewContentProvider(IStammdatenservice pStammdatenservice) {
		super(pStammdatenservice);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
	 */
	@Override
	public Object[] getElements(Object pArg0) {
		return getStammdatenservice().getRaetsel().toArray();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
	 */
	@Override
	public void dispose() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public void inputChanged(Viewer pArg0, Object pArg1, Object pArg2) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.ui.app.views.contentproviders.AbstractMatheObjectsViewContentProvider#
	 * isInterestingChangeEvent(java.beans.PropertyChangeEvent)
	 */
	@Override
	public boolean isInterestingChangeEvent(String pPropertyName) {
		return IMCArchivraetselNames.PROP_STUFE.equals(pPropertyName)
			|| IMCArchivraetselNames.PROP_SCHLUESSEL.equals(pPropertyName)
			|| IMCArchivraetselNames.PROP_TITEL.equals(pPropertyName);
	}

}
