/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views;

import org.eclipse.jface.viewers.ColumnLabelProvider;

import de.egladil.mathejungalt.ui.app.views.comparators.AlphabethicComparator;
import de.egladil.mathejungalt.ui.app.views.comparators.IViewerComparator;

/**
 * @author aheike
 */
public class ViewColumn implements ITableViewColumn {

	/** */
	private String title;

	/** */
	private int bound;

	/** */
	private IViewerComparator comparator;

	/** */
	private ColumnLabelProvider labelProvider;

	/**
	 * @param pTitle String die Spaltenüberschrift.
	 * @param pBound int die Spaltenbreite genommen.
	 * @param pLabelProvider {@link ColumnLabelProvider} der LabelProvider
	 */
	public ViewColumn(String pTitle, int pBound, ColumnLabelProvider pLabelProvider) {
		super();
		bound = pBound;
		comparator = new AlphabethicComparator();
		labelProvider = pLabelProvider;
		title = pTitle;
	}

	/**
	 * @param pTitle String die Spaltenüberschrift.
	 * @param pBound int die Spaltenbreite
	 * @param pComparator {@link IViewerComparator} darf null sein. In diesem Fall wird ein
	 * {@link AlphabethicComparator} genommen.
	 * @param pLabelProvider {@link ColumnLabelProvider} der LabelProvider
	 */
	public ViewColumn(String pTitle, int pBound, IViewerComparator pComparator, ColumnLabelProvider pLabelProvider) {
		super();
		bound = pBound;
		comparator = pComparator != null ? pComparator : new AlphabethicComparator();
		labelProvider = pLabelProvider;
		title = pTitle;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.ITableViewColumn#getBound()
	 */
	@Override
	public int getBound() {
		return bound;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.ITableViewColumn#getComparator()
	 */
	@Override
	public IViewerComparator getComparator() {
		return comparator;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.ITableViewColumn#getLabelProvider()
	 */
	@Override
	public ColumnLabelProvider getLabelProvider() {
		return labelProvider;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.ITableViewColumn#getTitle()
	 */
	@Override
	public String getTitle() {
		return title;
	}

}
