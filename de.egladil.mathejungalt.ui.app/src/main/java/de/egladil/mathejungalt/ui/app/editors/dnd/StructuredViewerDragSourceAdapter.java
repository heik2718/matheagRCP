/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.dnd;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.swt.dnd.DragSourceEvent;

/**
 * @author winkelv
 */
public class StructuredViewerDragSourceAdapter extends AbstractDragSourceAdapter {

	/** */
	private StructuredViewer viewer;

	/**
	 * @param pViewer
	 */
	public StructuredViewerDragSourceAdapter(StructuredViewer pViewer) {
		super();
		viewer = pViewer;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.dnd.AbstractDragSourceAdapter#getSelection()
	 */
	@Override
	protected ISelection getSelection() {
		return viewer.getSelection();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.dnd.AbstractDragSourceAdapter#dragFinished(org.eclipse.swt.dnd.DragSourceEvent)
	 */
	@Override
	public void dragFinished(DragSourceEvent pEvent) {
		viewer.refresh();
	}
}
