/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.IMinikaenguruNames;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;
import de.egladil.mathejungalt.ui.app.themes.PartControlsFactory;

/**
 * @author winkelv
 */
public class MinikaenguruSimpleAttributePartDataBindingProvider extends
	AbstractAufgabensammlungSimpleAttributePartDataBindingProvider {

	/**
	 * 
	 */
	public MinikaenguruSimpleAttributePartDataBindingProvider() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.contentproviders.AbstractAttributePartContentsProvider#anzahlZeilen()
	 */
	@Override
	protected int anzahlZeilen() {
		return 3;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.parts.AbstractSimpleAttributePartControlsProvider#initControls(org.eclipse.swt.widgets
	 * .Composite , org.eclipse.ui.forms.widgets.FormToolkit, boolean)
	 */
	@Override
	protected void initControls(Composite pClient, FormToolkit pToolkit, boolean pEditable, AbstractMatheAGObject pInput) {
		super.initControls(pClient, pToolkit, pEditable, pInput);
		final Control[] controls = getControls();
		Control[] labels = new Control[controls.length];

		Minikaenguru minikaenguru = (Minikaenguru) pInput;
		boolean editable = minikaenguru.getBeendet() == 0;

		int widthHint = 150;
		labels[2] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_JAHR);
		controls[2] = PartControlsFactory.createText(pClient, pToolkit, widthHint, editable);
		((Text) controls[2]).setTextLimit(IMinikaenguruNames.LENGTH_KEY);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.contentproviders.IAttributePartContentsProvider#refreshDataBindings(de.egladil.mathe
	 * .core.domain .AbstractMatheAGObject)
	 */
	@Override
	public void refreshDataBindings(AbstractMatheAGObject pInput) {
		super.refreshDataBindings(pInput);

		Binding[] bindings = getBindings();
		Control[] controls = getControls();

		IObservableValue observableWidget = SWTObservables.observeText(controls[2], SWT.Modify);
		IObservableValue observableProperty = BeansObservables.observeValue(pInput, IMinikaenguruNames.PROP_JAHR);
		bindings[2] = getDbc().bindValue(observableWidget, observableProperty, null, null);
	}
}
