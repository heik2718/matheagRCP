/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.editorinputs;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;

/**
 * @author winkelv
 */
public abstract class AbstractAufgabensammlungEditorInput extends AbstractMatheEditorInput {

	/**
	 * @param pObject
	 */
	public AbstractAufgabensammlungEditorInput(AbstractMatheAGObject pObject) {
		super(pObject);
	}

	/**
	 * @return
	 */
	public abstract AbstractMatheAGObject getCacheTarget();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.editorinputs.AbstractMatheEditorInput#initCache()
	 */
	@Override
	public void initCache() {
		setCache(getCacheTarget());
		applyChanges();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Object getAdapter(Class pAdapter) {
		if (pAdapter.getName().equals(IAufgabensammlung.class.getName())) {
			return getDomainObject();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object pObj) {
		if (pObj == null)
			return false;
		if (!(pObj instanceof AbstractAufgabensammlungEditorInput)) {
			return false;
		}
		AbstractAufgabensammlungEditorInput param = (AbstractAufgabensammlungEditorInput) pObj;
		return getAdapter(IAufgabensammlung.class).equals(param.getAdapter(IAufgabensammlung.class));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.editorinputs.AbstractMatheEditorInput#getInputObjectType()
	 */
	@Override
	protected String getInputObjectType() {
		return IAufgabensammlung.class.getName();
	}
}
