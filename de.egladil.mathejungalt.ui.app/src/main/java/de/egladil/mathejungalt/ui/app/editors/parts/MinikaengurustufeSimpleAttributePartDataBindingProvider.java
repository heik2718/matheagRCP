/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.ui.app.editors.converters.IntegerToMinikaenguruStufetextConverter;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Der Teil, der die speziellen Stufen für Serienaufgaben anzeigt.
 * 
 * @author winkelv
 */
public class MinikaengurustufeSimpleAttributePartDataBindingProvider extends
	AbstractStufeSimpleAttributePartDataBindingProvider {

	/** */
	private final IConverter converter;

	/**
	 * 
	 */
	public MinikaengurustufeSimpleAttributePartDataBindingProvider() {
		super();
		converter = new IntegerToMinikaenguruStufetextConverter();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.parts.AbstractStufeSimpleAttributePartDataBindingProvider#getItems()
	 */
	protected String[] getItems() {
		return new String[] { ITextConstants.PUNKTE_3, ITextConstants.PUNKTE_4, ITextConstants.PUNKTE_5 };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.editors.parts.AbstractStufeSimpleAttributePartDataBindingProvider#getConverter()
	 */
	@Override
	protected IConverter getConverter() {
		return converter;
	}
}
