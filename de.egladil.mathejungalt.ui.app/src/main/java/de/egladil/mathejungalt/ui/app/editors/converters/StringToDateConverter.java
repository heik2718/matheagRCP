/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.converters;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Konvertiert einen String in ein Datum.
 *
 * @author Winkelv
 */
public class StringToDateConverter implements IConverter {

	/**
	 *
	 */
	public StringToDateConverter() {
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object pFromObject) {
		try {
			String from = (String) pFromObject;
			return DateUtils.parseDate(from, ITextConstants.DATE_FORMAT_PATTERNS);
		} catch (ParseException e) {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.core.databinding.conversion.IConverter#getFromType()
	 */
	@Override
	public Object getFromType() {
		return String.class;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.core.databinding.conversion.IConverter#getToType()
	 */
	@Override
	public Object getToType() {
		return Date.class;
	}
}
