/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.pages;

import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormEditor;

import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.editors.parts.AufgabensammlungitemsMasterDetailsBlock;
import de.egladil.mathejungalt.ui.app.editors.parts.MatheAGObjectAttributesPart;
import de.egladil.mathejungalt.ui.app.editors.parts.SerieSimpleAttributePartDataBindingProvider;
import de.egladil.mathejungalt.ui.app.themes.IImageKeys;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * @author winkelv
 */
public class SerieAttributesFormPage extends AbstractAttributesFormPage {

	/** */
	private AufgabensammlungitemsMasterDetailsBlock serienitemsBlock;

	/**
	 * @param pEditor
	 * @param pId
	 * @param pTitle
	 */
	public SerieAttributesFormPage(FormEditor pEditor, String pId, String pTitle) {
		super(pEditor, pId, pTitle);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.pages.AbstractAttributesFormPage#getAttributesPart()
	 */
	@Override
	protected MatheAGObjectAttributesPart getAttributesPart() {
		return new MatheAGObjectAttributesPart(true, new SerieSimpleAttributePartDataBindingProvider());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.pages.AbstractAttributesFormPage#getHeader()
	 */
	@Override
	protected String getHeader() {
		return ITextConstants.SERIE_EDITOR_HEADER;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.pages.AbstractAttributesFormPage#getSectioHeader()
	 */
	@Override
	protected String getSectioHeader() {
		return ITextConstants.SECTION_HEADER_AUFGABENSAMMLUNG_ATTRIBUTES;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.pages.AbstractAttributesFormPage#hookAdditionalParts(org.eclipse.ui.forms.IManagedForm
	 * )
	 */
	@Override
	protected void hookAdditionalParts(IManagedForm pManagedForm) {
		serienitemsBlock = new AufgabensammlungitemsMasterDetailsBlock(this);
		serienitemsBlock.createContent(pManagedForm);
		addAdditionalPart(serienitemsBlock);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.pages.AbstractAttributesFormPage#getImage()
	 */
	@Override
	protected Image getImage() {
		return MatheJungAltActivator.getDefault().getImage(IImageKeys.SERIE);
	}
}
