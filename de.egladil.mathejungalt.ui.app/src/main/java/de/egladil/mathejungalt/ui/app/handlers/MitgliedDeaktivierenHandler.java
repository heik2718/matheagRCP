//=====================================================
// Projekt: de.egladil.mathejungalt.ui.app
// (c) Heike Winkelvoß
//=====================================================

package de.egladil.mathejungalt.ui.app.handlers;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * MitgliedDeaktivierenHandler
 */
public class MitgliedDeaktivierenHandler extends AbstractSelectionHandler {

	/** */
	private IStammdatenservice stammdatenservice;

	public MitgliedDeaktivierenHandler(final ISelectionProvider pSelectionProvider, final IStammdatenservice pStammdatenservice) {
		super(pSelectionProvider);
		this.stammdatenservice = pStammdatenservice;
	}

	@Override
	protected void run() {

		final AbstractMatheAGObject object = getFirstSelectedObject();
		if (!(object instanceof Mitglied)) {
			return;
		}
		final Mitglied mitglied = (Mitglied) object;
		if (!mitglied.isAktiv()) {
			return;
		}
		mitglied.setAktiv(0);
		final Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();

		try {
			stammdatenservice.mitgliedSpeichern(mitglied);
		} catch (final MatheJungAltException e) {
			MessageDialog.openError(shell, "Fehler beim Deaktivieren",
				mitglied.getKontakt() + "' konnte nicht deaktiviert werden \n" + e.getMessage());
		}
	}

	@Override
	public boolean isEnabled() {
		if (!super.isEnabled()) {
			return false;
		}
		if (!(getFirstSelectedObject() instanceof Mitglied)) {
			return false;
		}
		final Mitglied mitglied = (Mitglied) getFirstSelectedObject();
		return mitglied.isAktiv();
	}

}
