/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.kopierer;

import java.util.Iterator;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.mitglieder.Diplom;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.domain.mitglieder.Serienteilnahme;

/**
 * @author aheike
 */
public class MitgliedKopierer implements IKopierer {

	/**
	 * 
	 */
	public MitgliedKopierer() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.ui.app.editors.kopierer.IKopierer#copyAttributes(de.egladil.mathejungalt.domain.
	 * AbstractMatheAGObject, de.egladil.mathejungalt.domain.AbstractMatheAGObject)
	 */
	@Override
	public void copyAttributes(AbstractMatheAGObject pSource, AbstractMatheAGObject pTarget)
		throws MatheJungAltException {
		if (!(pSource instanceof Mitglied) && !(pTarget instanceof Mitglied)) {
			throw new MatheJungAltException(
				"Quelle oder Ziel sind keine Mitglieder: Quelle = " + pSource == null ? "null" : pSource.getClass()
					.getName() + ", Ziel = " + pTarget == null ? "null" : pTarget.getClass().getName());
		}
		Mitglied source = (Mitglied) pSource;
		Mitglied target = (Mitglied) pTarget;
		target.setAlter(source.getAlter());
		target.setErfinderpunkte(source.getErfinderpunkte());
		target.setFototitel(source.getFototitel());
		target.setGeburtsjahr(source.getGeburtsjahr());
		target.setGeburtsmonat(source.getGeburtsmonat());
		target.setKlasse(source.getKlasse());
		target.setKontakt(source.getKontakt());
		target.setNachname(source.getNachname());
		target.setVorname(source.getVorname());
		target.setSchluessel(source.getSchluessel());

		target.initDiplome();
		target.clearDiplome();
		Iterator<Diplom> iter1 = source.diplomeIterator();
		while (iter1.hasNext()) {
			target.addDiplom(iter1.next());
		}

		target.initSerienteilnahmen();
		target.clearSerienteilnahmen();
		Iterator<Serienteilnahme> iter2 = source.serienteilnahmenIterator();
		while (iter2.hasNext()) {
			target.addSerienteilname(iter2.next());
		}
	}

}
