/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.converters;

import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;

/**
 * Konvertiert einen Schluessel in eine {@link Aufgabe}.
 *
 * @author Winkelv
 */
public class SchluesselToAufgabeConverter implements IConverter {

	/**
	 *
	 */
	public SchluesselToAufgabeConverter() {
		//
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object pFromObject) {
		return MatheJungAltActivator.getDefault().getStammdatenservice().aufgabeZuSchluessel(pFromObject.toString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getFromType()
	 */
	@Override
	public Object getFromType() {
		return String.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getToType()
	 */
	@Override
	public Object getToType() {
		return Aufgabe.class;
	}
}
