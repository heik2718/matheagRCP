/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.converters;

import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Konvertiert einen Integer in eine Stufe-Text
 * 
 * @author Winkelv
 */
public class IntegerToMonoidStufetextConverter implements IConverter {

	/**
	 * 
	 */
	public IntegerToMonoidStufetextConverter() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object pFromObject) {
		int param = ((Integer) pFromObject).intValue();
		switch (param) {
		case 1:
			return ITextConstants.MONOID_1;
		case 2:
			return ITextConstants.MONOID_2;
		default:
			return "falsche Monoid-Stufe" + param;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getFromType()
	 */
	@Override
	public Object getFromType() {
		return Integer.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getToType()
	 */
	@Override
	public Object getToType() {
		return String.class;
	}
}
