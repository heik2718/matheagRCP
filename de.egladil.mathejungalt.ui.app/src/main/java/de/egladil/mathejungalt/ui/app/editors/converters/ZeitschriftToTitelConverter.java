/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.converters;

import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.domain.medien.Zeitschrift;

/**
 * Wandelt eine Buchquelle zu dem dahinterliegenden Buch um
 * 
 * @author Winkelv
 */
public class ZeitschriftToTitelConverter implements IConverter {

	/**
	 * 
	 */
	public ZeitschriftToTitelConverter() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object pFromObject) {
		if (pFromObject instanceof Zeitschrift) {
			return ((Zeitschrift) pFromObject).getTitel();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getFromType()
	 */
	@Override
	public Object getFromType() {
		return Zeitschrift.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getToType()
	 */
	@Override
	public Object getToType() {
		return String.class;
	}

}
