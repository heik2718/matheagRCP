/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.pages;

import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormEditor;

import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.editors.parts.ArbeitsblattSimpleAttributePartDataBindingProvider;
import de.egladil.mathejungalt.ui.app.editors.parts.AufgabensammlungitemsMasterDetailsBlock;
import de.egladil.mathejungalt.ui.app.editors.parts.MatheAGObjectAttributesPart;
import de.egladil.mathejungalt.ui.app.themes.IImageKeys;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * @author winkelv
 */
public class ArbeitsblattAttributesFormPage extends AbstractAttributesFormPage {

	/** */
	private AufgabensammlungitemsMasterDetailsBlock aufgabensammlungitemsBlock;

	/**
	 * @param pEditor
	 * @param pId
	 * @param pTitle
	 */
	public ArbeitsblattAttributesFormPage(FormEditor pEditor, String pId, String pTitle) {
		super(pEditor, pId, pTitle);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.pages.AbstractAttributesFormPage#getAttributesPart()
	 */
	@Override
	protected MatheAGObjectAttributesPart getAttributesPart() {
		return new MatheAGObjectAttributesPart(true, new ArbeitsblattSimpleAttributePartDataBindingProvider());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.pages.AbstractAttributesFormPage#getHeader()
	 */
	@Override
	protected String getHeader() {
		return ITextConstants.ARBEITSBLATT_EDITOR_HEADER;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.pages.AbstractAttributesFormPage#getSectioHeader()
	 */
	@Override
	protected String getSectioHeader() {
		return ITextConstants.SECTION_HEADER_AUFGABENSAMMLUNG_ATTRIBUTES;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.pages.AbstractAttributesFormPage#hookAdditionalParts(org.eclipse.ui.forms.IManagedForm
	 * )
	 */
	@Override
	protected void hookAdditionalParts(IManagedForm pManagedForm) {
		aufgabensammlungitemsBlock = new AufgabensammlungitemsMasterDetailsBlock(this);
		aufgabensammlungitemsBlock.createContent(pManagedForm);
		addAdditionalPart(aufgabensammlungitemsBlock);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.pages.AbstractAttributesFormPage#getImage()
	 */
	@Override
	protected Image getImage() {
		return MatheJungAltActivator.getDefault().getImage(IImageKeys.MINIKAENGURU);
	}
}
