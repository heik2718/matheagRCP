/**
 *
 */
package de.egladil.mathejungalt.ui.app.views.comparators;

import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;

/**
 * @author Winkelv
 */
public class MCRaetselitemViewerComparator extends AbstractViewerComparator {

	/**
	 * @param pIndex
	 */
	public MCRaetselitemViewerComparator(int pIndex) {
		super(pIndex);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Object pO1, Object pO2) {
		MCArchivraetselItem item1 = (MCArchivraetselItem) pO1;
		MCArchivraetselItem item2 = (MCArchivraetselItem) pO2;
		switch (getIndex()) {
		case 1: {
			return item1.compareTo(item2);
		}
		case 2: {
			return item1.getAufgabe().getPunkte() - item2.getAufgabe().getPunkte();
		}
		default:
			return item1.getAufgabe().getSchluessel().compareToIgnoreCase(item2.getAufgabe().getSchluessel());
		}
	}
}
