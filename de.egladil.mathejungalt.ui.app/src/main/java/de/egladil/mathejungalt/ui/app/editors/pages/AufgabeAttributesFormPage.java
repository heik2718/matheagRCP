/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.pages;

import java.beans.PropertyChangeEvent;

import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormEditor;

import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.editors.documentproviders.ImageProvider;
import de.egladil.mathejungalt.ui.app.editors.documentproviders.ImageViewer;
import de.egladil.mathejungalt.ui.app.editors.parts.AufgabeSimpleAttributePartDataBindingProvider;
import de.egladil.mathejungalt.ui.app.editors.parts.AufgabenzweckStufeMasterDetailsBlock;
import de.egladil.mathejungalt.ui.app.editors.parts.MatheAGObjectAttributesPart;
import de.egladil.mathejungalt.ui.app.editors.parts.QuelleMasterDetailsBlock;
import de.egladil.mathejungalt.ui.app.themes.IImageKeys;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Die AttributesFormPage verhält sich wie der MasterPart in einem MasterDetailsBlock: je nach Quellenart wir ein
 * eigener DetailsPart angezeigt
 * 
 * @author winkelv
 */
public class AufgabeAttributesFormPage extends AbstractAttributesFormPage {

	/** */
	private QuelleMasterDetailsBlock quellePart;

	/** */
	private AufgabenzweckStufeMasterDetailsBlock zweckStufePart;

	/** */
	private ImageViewer imageViewer;

	/** */
	private ImageProvider imageProvider;

	/** */
	private Image image;

	/**
	 * @param pEditor
	 * @param pId
	 * @param pTitle
	 */
	public AufgabeAttributesFormPage(FormEditor pEditor, String pId, String pTitle) {
		super(pEditor, pId, pTitle);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.pages.AbstractAttributesFormPage#hookAdditionalParts(org.eclipse.ui.forms.IManagedForm
	 * )
	 */
	@Override
	protected void hookAdditionalParts(IManagedForm pManagedForm) {
		zweckStufePart = new AufgabenzweckStufeMasterDetailsBlock(this);
		zweckStufePart.createContent(pManagedForm);
		addAdditionalPart(zweckStufePart);
		// hier explizit die Selection setzen. Das ist notwendig weil sonst kein ChangeEvent geworfen wird und die
		// Details-Seiten nicht angezeigt werden.
		zweckStufePart.setSelection(new StructuredSelection(getDomainObject()));

		quellePart = new QuelleMasterDetailsBlock(this);
		quellePart.createContent(pManagedForm);
		addAdditionalPart(quellePart);
		// hier explizit die Selection setzen. Das ist notwendig weil sonst kein ChangeEvent geworfen wird und die
		// Details-Seiten nicht angezeigt werden.
		quellePart.setSelection(new StructuredSelection(getDomainObject()));

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.pages.AbstractAttributesFormPage#getAttributesPart()
	 */
	@Override
	protected MatheAGObjectAttributesPart getAttributesPart() {
		return new MatheAGObjectAttributesPart(true, new AufgabeSimpleAttributePartDataBindingProvider());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.pages.AbstractAttributesFormPage#getHeader()
	 */
	@Override
	protected String getHeader() {
		return ITextConstants.AUFGABE_EDITOR_HEADER;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.pages.AbstractAttributesFormPage#getSectioHeader()
	 */
	@Override
	protected String getSectioHeader() {
		return ITextConstants.SECTION_HEADER_AUFGABE_ATTRIBUTES;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.pages.AbstractAttributesFormPage#getImage()
	 */
	@Override
	protected Image getImage() {
		return MatheJungAltActivator.getDefault().getImage(IImageKeys.AUFGABE);
	}

	@Override
	public void propertyChange(PropertyChangeEvent pEvt) {
		super.propertyChange(pEvt);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.pages.AbstractAttributesFormPage#dispose()
	 */
	@Override
	public void dispose() {
		if (imageProvider != null) {
			imageProvider.disposeImage(image);
		}
		if (!imageViewer.isDisposed())
			imageViewer.dispose();
		super.dispose();
	}
}
