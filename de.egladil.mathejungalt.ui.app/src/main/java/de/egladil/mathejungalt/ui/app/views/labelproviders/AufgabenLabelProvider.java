/**
 *
 */
package de.egladil.mathejungalt.ui.app.views.labelproviders;

import org.eclipse.swt.graphics.Color;

import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.ui.app.themes.LayoutFactory;

/**
 * @author aheike
 */
public class AufgabenLabelProvider extends CommonColumnLabelProvider {

	/**
	 *
	 */
	public AufgabenLabelProvider(int pIndex) {
		super(pIndex);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ColumnLabelProvider#getText(java.lang.Object)
	 */
	@Override
	public String getText(Object pElement) {
		final Aufgabe aufgabe = (Aufgabe) pElement;
		switch (getIndex()) {
		case 0:
			String schluessel = aufgabe.getSchluessel().toString();
			if (Aufgabe.LOCKED.equals(aufgabe.getZuSchlechtFuerSerie())) {
				schluessel += "*";
			}
			return schluessel;
		case 1:
			return aufgabe.getZweck().toString();
		case 2:
			return aufgabe.getStufe().toString();
		case 3:
			return aufgabe.getThema().toString();
		default:
			return aufgabe.getTitel().toString();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.CellLabelProvider#getToolTipText(java.lang.Object)
	 */
	@Override
	public String getToolTipText(Object pElement) {
		return ((Aufgabe) pElement).getBeschreibung();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ColumnLabelProvider#getForeground(java.lang.Object)
	 */
	@Override
	public Color getForeground(Object pElement) {
		return LayoutFactory.getColorByAufgbe((Aufgabe) pElement);
	}
}
