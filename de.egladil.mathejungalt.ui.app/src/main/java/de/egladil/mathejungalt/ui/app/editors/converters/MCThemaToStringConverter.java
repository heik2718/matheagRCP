/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.converters;

import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.domain.mcraetsel.MCThemen;

/**
 * Konvertiert ein {@link MCThemen} in einen String.
 *
 * @author Winkelv
 */
public class MCThemaToStringConverter implements IConverter {

	/**
	 *
	 */
	public MCThemaToStringConverter() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object pFromObject) {
		MCThemen param = (MCThemen) pFromObject;
		return param == null ? "" : param.getLabel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getFromType()
	 */
	@Override
	public Object getFromType() {
		return MCThemen.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getToType()
	 */
	@Override
	public Object getToType() {
		return String.class;
	}
}
