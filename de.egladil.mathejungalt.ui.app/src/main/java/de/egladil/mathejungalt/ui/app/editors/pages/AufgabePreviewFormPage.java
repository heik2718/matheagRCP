/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.pages;

import java.io.File;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;

import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.editors.AbstractMatheEditor;
import de.egladil.mathejungalt.ui.app.editors.AufgabeEditor;
import de.egladil.mathejungalt.ui.app.editors.documentproviders.ImageProvider;
import de.egladil.mathejungalt.ui.app.editors.documentproviders.ImageViewer;
import de.egladil.mathejungalt.ui.app.editors.documentproviders.PathImageProvider;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.AufgabeEditorInput;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.MCAufgabeEditorInput;
import de.egladil.mathejungalt.ui.app.preferences.PreferenceConstants;
import de.egladil.mathejungalt.ui.app.themes.IColorKeys;
import de.egladil.mathejungalt.ui.app.themes.LayoutFactory;

/**
 * @author winkelv
 */
public class AufgabePreviewFormPage extends FormPage {

	private static final String ID_AUFGABE = AufgabePreviewFormPage.class.getName() + "_aufgabe";

	private static final String ID_LOESUNG = AufgabePreviewFormPage.class.getName() + "_loesung";

	private static final String TITEL_AUFGABE = "png Aufgabe";

	private static final String TITEL_LOESUNG = "png L\u00F6sung";

	private final boolean quizaufgabe;

	private ImageProvider imageProvider;

	private Image image;

	/** */
	private ImageViewer imageViewer;

	/** */
	private boolean showAufgabe;

	/**
	 * @param pEditor
	 * @param pId
	 * @param pTitle
	 */
	public AufgabePreviewFormPage(FormEditor pEditor, boolean pShowAufgabe) {
		super(pEditor, pShowAufgabe ? ID_AUFGABE : ID_LOESUNG, pShowAufgabe ? TITEL_AUFGABE : TITEL_LOESUNG);
		showAufgabe = pShowAufgabe;
		if (((AbstractMatheEditor) pEditor).getId().equals(AufgabeEditor.ID)) {
			quizaufgabe = false;
		} else {
			quizaufgabe = true;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.FormPage#createFormContent(org.eclipse.ui.forms.IManagedForm)
	 */
	@Override
	protected void createFormContent(IManagedForm pManagedForm) {
		final ScrolledForm form = pManagedForm.getForm();
		FormToolkit formToolkit = pManagedForm.getToolkit();
		Layout layout = LayoutFactory.createFormPaneGridLayout(true, 1);
		form.getBody().setLayout(layout);
		form.getBody().setLayoutData(new GridData(GridData.FILL_BOTH));
		String path = getPath();

		// Der Behälter für die Teile dieser Section
		Composite editorArea = formToolkit.createComposite(form.getBody(), SWT.WRAP);// formToolkit.createComposite(
		// section,
		// SWT.WRAP);
		editorArea.setLayout(new GridLayout(1, false));
		GridData gd = new GridData(SWT.CENTER, SWT.CENTER, true, true);
		gd.widthHint = 600;
		gd.heightHint = 300;

		editorArea.setLayoutData(gd);

		imageViewer = new ImageViewer(editorArea, SWT.NONE);
		imageProvider = new PathImageProvider(path);
		image = imageProvider.getImage(imageViewer.getDisplay());
		if (image != null) {
			imageViewer.setImage(image);
		}
		imageViewer.setBackground(PlatformUI.getWorkbench().getThemeManager().getCurrentTheme().getColorRegistry()
			.get(IColorKeys.WHITE));

	}

	private String getPath() {
		String path = null;
		if (quizaufgabe) {
			MCAufgabe aufgabe = (MCAufgabe) ((MCAufgabeEditorInput) getEditorInput()).getDomainObject();
			path = MatheJungAltActivator.getDefault().getPath(PreferenceConstants.P_PATH_QUIZ_PNG_DIR) + File.separator
				+ aufgabe.getVerzeichnis() + File.separator + "prev_" + aufgabe.getSchluessel();
		} else {
			AufgabeEditorInput input = (AufgabeEditorInput) getEditorInput();
			Aufgabe aufgabe = (Aufgabe) input.getDomainObject();
			path = MatheJungAltActivator.getDefault().getPath(PreferenceConstants.P_PATH_PNG_DIR) + File.separator
				+ aufgabe.getVerzeichnis() + File.separator + aufgabe.getSchluessel();

		}
		if (showAufgabe) {
			path += ".png";
		} else {
			path += "_l.png";
		}
		return path;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.FormPage#isDirty()
	 */
	@Override
	public boolean isDirty() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.FormPage#dispose()
	 */
	@Override
	public void dispose() {
		if (imageProvider != null) {
			imageProvider.disposeImage(image);
		}
		if (!imageViewer.isDisposed())
			imageViewer.dispose();
	}
}
