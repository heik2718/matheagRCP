package de.egladil.mathejungalt.ui.app.dialogs;

import java.util.Iterator;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;

public class ShuttleDialog<T extends AbstractMatheAGObject> extends Dialog implements SelectionListener,
	ISelectionChangedListener {

	private TableViewer availableObjectsViewer;

	private final ILabelProvider labelProvider;

	private List<T> availableObjects;

	private Button addButton;

	private Button removeButton;

	private Button removeAllButton;

	private TableViewer selectedObjectsViewer;

	private List<T> selectedObjects;

	private final String shellText;

	/**
	 * Create the dialog.
	 * 
	 * @param parentShell
	 */
	public ShuttleDialog(Shell parentShell, String shellText, ILabelProvider labelProvider, List<T> availableObjects,
		List<T> selectedObjects) {
		super(parentShell);
		this.shellText = shellText;
		this.labelProvider = labelProvider;
		this.availableObjects = availableObjects;
		this.selectedObjects = selectedObjects;
	}

	/**
	 * Create contents of the dialog.
	 * 
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);
		area.setLayout(new GridLayout(1, false));

		Composite infoComposite = new Composite(area, SWT.NONE);
		GridLayout gridLayout = new GridLayout(3, false);
		gridLayout.horizontalSpacing = 2;
		gridLayout.verticalSpacing = 2;
		gridLayout.marginWidth = 0;
		gridLayout.marginHeight = 0;
		infoComposite.setLayout(gridLayout);
		GridData gridData = new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1);
		infoComposite.setLayoutData(gridData);

		availableObjectsViewer = createTableViewer(infoComposite);
		availableObjectsViewer.setInput(availableObjects);
		availableObjectsViewer.getControl().addMouseListener(new MouseAdapter() {

			@Override
			public void mouseDoubleClick(MouseEvent pE) {
				ISelection sel = availableObjectsViewer.getSelection();
				if (sel != null && !sel.isEmpty() && (sel instanceof IStructuredSelection)) {
					shuttleRight();
				}
			}

		});

		Composite buttonComposite = new Composite(infoComposite, SWT.NONE);
		buttonComposite.setLayout(new GridLayout(1, true));
		{
			GridData gd = new GridData(SWT.CENTER, SWT.TOP, true, true);
			gd.horizontalAlignment = SWT.FILL;
			gd.verticalAlignment = SWT.FILL;
			gd.widthHint = 30;
			gd.heightHint = 200;
			buttonComposite.setLayoutData(gd);
		}

		addButton = createButton(buttonComposite, ">");
		removeButton = createButton(buttonComposite, "<");
		removeAllButton = createButton(buttonComposite, "<<");
		removeAllButton.setEnabled(!selectedObjects.isEmpty());

		selectedObjectsViewer = createTableViewer(infoComposite);
		selectedObjectsViewer.setInput(selectedObjects);
		selectedObjectsViewer.getControl().addMouseListener(new MouseAdapter() {

			@Override
			public void mouseDoubleClick(MouseEvent pE) {
				ISelection sel = availableObjectsViewer.getSelection();
				if (sel != null && !sel.isEmpty() && (sel instanceof IStructuredSelection)) {
					shuttleLeft();
				}
			}

		});

		return area;
	}

	private TableViewer createTableViewer(Composite parent) {
		TableViewer viewer = new TableViewer(parent, SWT.H_SCROLL | SWT.V_SCROLL | SWT.MULTI | SWT.FULL_SELECTION
			| SWT.BORDER);
		viewer.setContentProvider(new ArrayContentProvider());
		viewer.setLabelProvider(labelProvider);
		{
			GridData gd = new GridData(SWT.LEFT, SWT.TOP, true, true);
			gd.horizontalAlignment = SWT.FILL;
			gd.verticalAlignment = SWT.FILL;
			gd.widthHint = 150;
			gd.heightHint = 200;
			viewer.getControl().setLayoutData(gd);
		}
		viewer.addSelectionChangedListener(this);
		return viewer;
	}

	/**
	 * @param parent
	 * @return
	 */
	private Button createButton(Composite parent, String text) {
		Button btn = new Button(parent, SWT.NONE);
		btn.setText(text);

		GridData gd = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd.grabExcessHorizontalSpace = true;
		btn.setLayoutData(gd);

		btn.addSelectionListener(this);
		btn.setEnabled(false);
		return btn;
	}

	/**
	 * Create contents of the button bar.
	 * 
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(450, 300);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell pNewShell) {
		pNewShell.setText(shellText);
		super.configureShell(pNewShell);
	}

	/**
	 * @return the selectedObjects
	 */
	public final List<T> getSelectedObjects() {
		return selectedObjects;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
	 */
	@Override
	public void widgetSelected(SelectionEvent pE) {
		if (pE.widget.equals(addButton)) {
			shuttleRight();
		}
		if (pE.widget.equals(removeButton)) {
			shuttleLeft();
		}
		if (pE.widget.equals(removeAllButton)) {
			shuttleAllLeft();
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void shuttleRight() {
		deactivateViewers();
		IStructuredSelection sel = (IStructuredSelection) availableObjectsViewer.getSelection();
		Iterator iter = sel.iterator();
		while (iter.hasNext()) {
			T object = (T) iter.next();
			if (!selectedObjects.contains(object)) {
				selectedObjects.add(object);
				availableObjects.remove(object);
			}
		}
		activateViewers();
		resetButtonStatus();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void shuttleLeft() {
		deactivateViewers();
		IStructuredSelection sel = (IStructuredSelection) selectedObjectsViewer.getSelection();
		Iterator iter = sel.iterator();
		while (iter.hasNext()) {
			T object = (T) iter.next();
			selectedObjects.remove(object);
			availableObjects.add(object);
		}
		activateViewers();
		resetButtonStatus();
	}

	private void shuttleAllLeft() {
		deactivateViewers();
		for (T object : selectedObjects) {
			availableObjects.add(object);
		}
		selectedObjects.clear();
		activateViewers();
		resetButtonStatus();
	}

	private void deactivateViewers() {
		availableObjectsViewer.getControl().setRedraw(false);
		selectedObjectsViewer.getControl().setRedraw(false);
	}

	private void activateViewers() {
		availableObjectsViewer.getControl().setRedraw(true);
		selectedObjectsViewer.getControl().setRedraw(true);
		availableObjectsViewer.setInput(availableObjects);
		selectedObjectsViewer.setInput(selectedObjects);
		availableObjectsViewer.getControl().redraw();
		selectedObjectsViewer.getControl().redraw();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.events.SelectionListener#widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent)
	 */
	@Override
	public void widgetDefaultSelected(SelectionEvent pE) {
		// nüscht
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent
	 * )
	 */
	@Override
	public void selectionChanged(SelectionChangedEvent pEvent) {
		if (pEvent.getSource().equals(availableObjectsViewer)) {
			addButton.setEnabled(!pEvent.getSelection().isEmpty());
		}
		if (pEvent.getSource().equals(selectedObjectsViewer)) {
			removeButton.setEnabled(!pEvent.getSelection().isEmpty());
		}

	}

	private void resetButtonStatus() {
		addButton.setEnabled(false);
		removeButton.setEnabled(false);
		removeAllButton.setEnabled(!selectedObjects.isEmpty());
	}
}
