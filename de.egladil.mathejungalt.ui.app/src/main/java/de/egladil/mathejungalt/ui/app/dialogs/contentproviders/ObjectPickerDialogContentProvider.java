/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.ui.app.dialogs.contentproviders;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.ui.app.adapters.IMasterObject;

/**
 * <p>
 * NOTE: THIS WIDGET AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class ObjectPickerDialogContentProvider implements IStructuredContentProvider {

	private static final Logger LOG = LoggerFactory.getLogger(ObjectPickerDialogContentProvider.class);

	/** */
	private final IMasterObject masterObject;

	/** */
	private final Collection<AbstractMatheAGObject> domainObjects;

	/**
	 * @param masterObject
	 * @param domainObjects
	 */
	public ObjectPickerDialogContentProvider(IMasterObject masterObject, Collection<AbstractMatheAGObject> domainObjects) {
		this.masterObject = masterObject;
		this.domainObjects = domainObjects;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
	 */
	@Override
	public void dispose() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public void inputChanged(Viewer arg0, Object arg1, Object arg2) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
	 */
	@Override
	public Object[] getElements(Object arg0) {
		List<AbstractMatheAGObject> elements = new ArrayList<AbstractMatheAGObject>();
		for (AbstractMatheAGObject domainObject : domainObjects) {
			if (masterObject.canAdd(domainObject)) {
				elements.add(domainObject);
			}
		}
		Collections.sort(elements);
		Object[] result = new Object[elements.size()];
		for (int i = 0; i < elements.size(); i++) {
			result[i] = elements.get(i);
		}
		return result;
	}
}
