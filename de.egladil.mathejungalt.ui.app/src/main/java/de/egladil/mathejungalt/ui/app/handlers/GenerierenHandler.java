/**
 *
 */
package de.egladil.mathejungalt.ui.app.handlers;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.PlatformUI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguruitem;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serienitem;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.preferences.PreferenceConstants;

/**
 * @author aheike
 */
public class GenerierenHandler extends AbstractSelectionHandler {

	private static final Logger LOG = LoggerFactory.getLogger(GenerierenHandler.class);

	/** */
	private IStammdatenservice stammdatenservice;

	private Map<String, AbstractMatheAGObject> markierteObjekte;

	private String errm = "";

	private IStatus status = null;

	/**
	 * @param pSelectionProvider
	 */
	public GenerierenHandler(ISelectionProvider pSelectionProvider, IStammdatenservice pStammdatenservice) {
		super(pSelectionProvider);
		stammdatenservice = pStammdatenservice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.handlers.AbstractSelectionHandler#run()
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected void run() {
		final AbstractMatheAGObject object = getFirstSelectedObject();
		status = null;
		@SuppressWarnings("rawtypes")
		final Class type = getType();
		if (object instanceof IAufgabensammlung) {
			markierteObjekte = null;
		} else {
			markierteObjekte = getObjectMap(type);
		}
		try {
			PlatformUI.getWorkbench().getProgressService().busyCursorWhile(new IRunnableWithProgress() {
				@Override
				public void run(IProgressMonitor pMonitor) throws InvocationTargetException, InterruptedException {
					try {
						pMonitor.beginTask("Generiere Dateien", 100);
						if (object instanceof IAufgabensammlung) {
							stammdatenservice.generate((IAufgabensammlung) object, pMonitor);
						} else {
							status = stammdatenservice.generate(getParameter(), markierteObjekte, MatheJungAltActivator
								.getDefault().getPath(PreferenceConstants.P_PATH_GENERATOR_OUT_DIR),
								MatheJungAltActivator.getDefault().getRaetselListeGeneratorModus(), pMonitor);
						}
						pMonitor.worked(100);
					} catch (Exception e) {
						errm = "Beim Generieren der Sourcen ist ein Fehler aufgetreten:\n" + e.getMessage();
						status = new Status(IStatus.ERROR, IStammdatenservice.PLUGIN_ID, errm);
						LOG.error(e.getMessage(), e);
					} finally {
						pMonitor.done();
					}
				}
			});
		} catch (InvocationTargetException e1) {
			status = new Status(IStatus.ERROR, IStammdatenservice.PLUGIN_ID, errm);
			errm = e1.getMessage();
		} catch (InterruptedException e1) {
			// ignore
		}
		if (status != null && !status.isOK()) {
			StringBuffer sb = new StringBuffer(status.getMessage());
			if (status.isMultiStatus()) {
				for (IStatus s : ((MultiStatus) status).getChildren()) {
					sb.append("\n");
					sb.append(s.getMessage());
				}
			}
			MessageDialog.openError(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Generatorfehler",
				sb.toString());
		} else {
			MessageDialog.openInformation(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Info",
				"Die Dateien wurden in das konfigurierte Temp-Verzeichnis generiert.");
		}
	}

	@SuppressWarnings("rawtypes")
	private Class getType() {
		final String parameter = this.getParameter();
		if (Mitglied.class.getName().equals(parameter)) {
			return Mitglied.class;
		}
		if (Aufgabe.class.getName().equals(parameter)) {
			return Aufgabe.class;
		}
		if (MCAufgabe.class.getName().equals(parameter)) {
			return MCAufgabe.class;
		}
		if (MCArchivraetsel.class.getName().equals(parameter)) {
			return MCArchivraetsel.class;
		}
		if (Serienitem.class.equals(parameter)) {
			return Serienitem.class;
		}
		if (Minikaenguruitem.class.getName().equals(parameter)) {
			return Minikaenguruitem.class;
		}
		return null;
	}

	/**
	 * @return
	 */
	private <T extends AbstractMatheAGObject> Map<String, T> getObjectMap(Class<T> pType) {
		final Map<String, T> result = new HashMap<String, T>();
		IStructuredSelection ssel = (IStructuredSelection) getSelectionProvider().getSelection();
		@SuppressWarnings("unchecked")
		Iterator<Object> iter = ssel.iterator();
		while (iter.hasNext()) {
			Object element = iter.next();
			AbstractMatheAGObject object = null;
			String nummer = null;
			if (element instanceof IAufgabensammlungItem) {
				object = ((IAufgabensammlungItem) element).getAufgabe();
				nummer = ((IAufgabensammlungItem) element).getNummer();
				if (nummer != null && nummer.isEmpty()) {
					nummer = null;
				}
			} else {
				object = pType.cast(element);
				nummer = object.getKey().toString();
			}
			if (nummer != null && object != null) {
				result.put(nummer, pType.cast(object));
			}
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.handlers.AbstractSelectionHandler#isEnabled()
	 */
	@Override
	public boolean isEnabled() {
		if (!super.isEnabled()) {
			return false;
		}
		AbstractMatheAGObject firstSelectedObject = getFirstSelectedObject();
		if (!(firstSelectedObject instanceof Mitglied) && !(firstSelectedObject instanceof Aufgabe)
			&& !(firstSelectedObject instanceof MCAufgabe) && !(firstSelectedObject instanceof IAufgabensammlung)
			&& !(firstSelectedObject instanceof MCArchivraetsel)) {
			return false;
		}
		// final boolean doit = firstSelectedObject instanceof Aufgabe || firstSelectedObject instanceof MCAufgabe
		// || firstSelectedObject instanceof Mitglied && stammdatenservice.canGenerate(firstSelectedObject)
		// || firstSelectedObject instanceof IAufgabensammlung
		// && stammdatenservice.canGenerate((AbstractMatheAGObject) firstSelectedObject);
		final boolean doit = stammdatenservice.canGenerate((AbstractMatheAGObject) firstSelectedObject);
		return doit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.handlers.AbstractSelectionHandler#getCommandParameterId()
	 */
	@Override
	protected String getCommandParameterId() {
		return "de.egladil.mathejungalt.ui.app.generieren.objectType";
	}
}
