/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * @author winkelv
 */
public class SerienteilnahmeDetailsPage extends AbstractDetailsPage {

	/** */
	private SerienteilnahmeSimpleAttributePartDataBindingProvider contentProvider;

	/**
	 * @param pMaster
	 * @param pInput
	 */
	public SerienteilnahmeDetailsPage(AbstractMasterDetailsBlockWithViewerPart pMaster) {
		super(pMaster);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractMatheAGDetailsPage#getAttributesPart()
	 */
	@Override
	protected MatheAGObjectAttributesPart getAttributesPart() {
		contentProvider = new SerienteilnahmeSimpleAttributePartDataBindingProvider();
		return new MatheAGObjectAttributesPart(true, contentProvider);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractDetailsPage#getHeader()
	 */
	@Override
	protected String getHeader() {
		return ITextConstants.SECTION_HEADER_SERIENTEILNAHME_ATTRIBUTES;
	}
}
