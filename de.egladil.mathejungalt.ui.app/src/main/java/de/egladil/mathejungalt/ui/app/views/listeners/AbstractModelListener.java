/**
 *
 */
package de.egladil.mathejungalt.ui.app.views.listeners;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;

import de.egladil.mathejungalt.service.stammdaten.event.IModelChangeListener;
import de.egladil.mathejungalt.service.stammdaten.event.ModelChangeEvent;

/**
 * @author aheike
 */
public abstract class AbstractModelListener implements PropertyChangeListener, IModelChangeListener,
	ISelectionChangedListener {

	/** */
	private final TableViewer viewer;

	/**
	 * @param pViewer
	 */
	public AbstractModelListener(TableViewer pViewer) {
		super();
		viewer = pViewer;
		viewer.addSelectionChangedListener(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent pEvt) {
		if (!isInterestingEvent(pEvt)) {
			return;
		}
		// Verhinderung des Flackereffekts bei großen Tabellen.
		viewer.getTable().setRedraw(false);
		try {
			// Falls ein Objekt gelöscht wird, dann müssen zugehörige geöffnete Editoren geschlossen werden
		} finally {
			viewer.getTable().setRedraw(true);
			viewer.refresh();
		}
	}

	/**
	 * @param pEvt
	 * @return
	 */
	protected boolean isInterestingEvent(PropertyChangeEvent pEvt) {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.event.IModelChangeListener#modelChanged(de.egladil.mathejungalt.service
	 * .stammdaten.event.ModelChangeEvent)
	 */
	@Override
	public void modelChanged(ModelChangeEvent pEvt) {
		// hier nochmal schauen, ob das Event überhaupt interessiert
		if (!isInterestingEvent(pEvt)) {
			return;
		}
		// Verhinderung des Flackereffekts bei großen Tabellen.
		viewer.getTable().setRedraw(false);
		try {
			if (ModelChangeEvent.ADD_EVENT.equals(pEvt.getEventType())) {
				viewer.refresh();
				viewer.setSelection(new StructuredSelection(pEvt.getNewValue()), true);
			}
			// Noch nicht fertig: Falls ein Objekt gelöscht wird, dann müssen zugehörige geöffnete Editoren geschlossen
			// werden
			if (ModelChangeEvent.REMOVE_EVENT.equals(pEvt.getEventType())) {
				viewer.setSelection(null);
			}
		} finally {
			viewer.getTable().setRedraw(true);
			viewer.refresh();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent
	 * )
	 */
	@Override
	public void selectionChanged(SelectionChangedEvent pEvent) {
		// Standardimplementierung ist leer
	}

	/**
	 * @param pEvt
	 * @return
	 */
	protected boolean isInterestingEvent(ModelChangeEvent pEvt) {
		return true;
	}
}
