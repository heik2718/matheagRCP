/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.converters;

import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.domain.types.MCAufgabenstufen;

/**
 * Konvertiert einen String in ein {@link MCAufgabenstufen}
 *
 * @author Winkelv
 */
public class StringToMCAufgabenstufeConverter implements IConverter {

	/**
	 *
	 */
	public StringToMCAufgabenstufeConverter() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object pFromObject) {
		String param = (String) pFromObject;
		return param == null || param.isEmpty() ? null : MCAufgabenstufen.getByLabel(param);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getFromType()
	 */
	@Override
	public Object getFromType() {
		return String.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getToType()
	 */
	@Override
	public Object getToType() {
		return MCAufgabenstufen.class;
	}
}
