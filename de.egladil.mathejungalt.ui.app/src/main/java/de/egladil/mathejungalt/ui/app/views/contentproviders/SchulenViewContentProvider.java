/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.contentproviders;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

import de.egladil.mathejungalt.domain.schulen.ISchuleNames;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * @author winkelv
 */
public class SchulenViewContentProvider extends AbstractMatheObjectsViewContentProvider implements
	IStructuredContentProvider {

	/**
	 * @param pStammdatenservice
	 */
	public SchulenViewContentProvider(IStammdatenservice pStammdatenservice) {
		super(pStammdatenservice);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
	 */
	@Override
	public Object[] getElements(Object pArg0) {
		return getStammdatenservice().getSchulen().toArray();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
	 */
	@Override
	public void dispose() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.ui.app.views.contentproviders.AbstractMatheObjectsViewContentProvider#
	 * isInterestingChangeEvent(java.beans.PropertyChangeEvent)
	 */
	@Override
	public boolean isInterestingChangeEvent(String pPropertyName) {
		return ISchuleNames.PROP_NAME.equals(pPropertyName) || ISchuleNames.PROP_ANSCHRIFT.equals(pPropertyName)
			|| ISchuleNames.PROP_TELEFONNUMMER.equals(pPropertyName)
			|| ISchuleNames.PROP_TELEFONNUMMER.equals(pPropertyName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public void inputChanged(Viewer pArg0, Object pArg1, Object pArg2) {

	}

}
