/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Abstrakte Oberklasse für alle QuelleDetailsPages, die gemeinsame Implementierungen für einige Methoden zur Verf�gung
 * stellt.
 * 
 * @author winkelv
 */
public abstract class AbstractQuelleDetailsPage extends AbstractDetailsPage {

	/**
	 * @param pMaster
	 */
	public AbstractQuelleDetailsPage(AbstractMasterDetailsBlock pMaster) {
		super(pMaster);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractDetailsPage#getDescription()
	 */
	@Override
	protected String getDescription() {
		return ITextConstants.LEERZEILE + ITextConstants.LEERZEILE;
	}
}
