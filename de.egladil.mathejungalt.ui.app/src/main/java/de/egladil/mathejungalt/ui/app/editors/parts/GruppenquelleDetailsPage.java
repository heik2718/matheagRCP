/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import de.egladil.mathejungalt.domain.quellen.Kindquelle;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Die Detailspage für die Attribute der {@link Kindquelle}
 * 
 * @author winkelv
 */
public class GruppenquelleDetailsPage extends AbstractQuelleDetailsPage {

	/**
	 * @param pMaster
	 */
	public GruppenquelleDetailsPage(AbstractMasterDetailsBlock pMaster) {
		super(pMaster);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractDetailsPage#getAttributesPart()
	 */
	@Override
	protected MatheAGObjectAttributesPart getAttributesPart() {
		GruppenquelleSimpleAttributePartDataBindingProvider contentsProvider = new GruppenquelleSimpleAttributePartDataBindingProvider();
		return new MatheAGObjectAttributesPart(false, contentsProvider);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractDetailsPage#getHeader()
	 */
	@Override
	protected String getHeader() {
		return ITextConstants.SECTION_HEADER_GRUPPENQUELLE_ATTRIBUTES;
	}
}
