/**
 *
 */
package de.egladil.mathejungalt.ui.app.handlers;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.PlatformUI;

import de.egladil.mathejungalt.domain.quellen.AbstractQuelle;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.dialogs.CreateQuelleWizard;

/**
 * Behandelt das Ereignis, mit dem eine neue Quelle erzeugt oder eine bereits vorhandene gefunden werden kann.
 *
 * @author Winkelv
 */
public class SelectQuelleHandler {

	/** */
	private AbstractQuelle quelle;

	/** */
	private IStammdatenservice stammdatenservice;

	/**
	 *
	 */
	public SelectQuelleHandler(AbstractQuelle pQuelle, IStammdatenservice pStammdatenservice) {
		stammdatenservice = pStammdatenservice;
		if (pQuelle != null) {
			quelle = pQuelle;
		} else {
			quelle = stammdatenservice.getNullQuelle();
		}
	}

	/**
   *
   */
	public AbstractQuelle execute() {
		CreateQuelleWizard wizard = new CreateQuelleWizard(stammdatenservice);
		WizardDialog dialog = new WizardDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), wizard);
		dialog.setMinimumPageSize(300, 400);
		int outcome = dialog.open();
		if (outcome == IDialogConstants.OK_ID) {
			return wizard.getQuelle();
		} else {
			return quelle;
		}
	}
}
