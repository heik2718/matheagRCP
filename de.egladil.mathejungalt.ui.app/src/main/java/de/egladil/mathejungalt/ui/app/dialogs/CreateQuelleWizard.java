/**
 * 
 */
package de.egladil.mathejungalt.ui.app.dialogs;

import org.eclipse.jface.wizard.Wizard;

import de.egladil.mathejungalt.domain.quellen.AbstractQuelle;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * @author Winkelv
 */
public class CreateQuelleWizard extends Wizard {

	/** */
	private CreateQuelleWizardSelectionPage wizardSelectionPage;

	/** */
	private IStammdatenservice stammdatenservice;

	/** */
	private AbstractQuelle quelle;

	/**
	 * 
	 */
	public CreateQuelleWizard(IStammdatenservice pStammdatenservice) {
		stammdatenservice = pStammdatenservice;
		setForcePreviousAndNextButtons(true);
		setWindowTitle(ITextConstants.QUELLE_WIZARD_SELECTION_PAGE_TITLE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		quelle = wizardSelectionPage.getQuelle();
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#addPages()
	 */
	@Override
	public void addPages() {
		wizardSelectionPage = new CreateQuelleWizardSelectionPage(stammdatenservice);
		addPage(wizardSelectionPage);
	}

	/**
	 * @return
	 */
	public final AbstractQuelle getQuelle() {
		return quelle;
	}

}
