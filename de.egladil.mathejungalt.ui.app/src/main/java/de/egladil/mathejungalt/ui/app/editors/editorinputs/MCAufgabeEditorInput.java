/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.editorinputs;

import org.eclipse.core.runtime.Assert;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.ui.app.editors.kopierer.IKopierer;
import de.egladil.mathejungalt.ui.app.editors.kopierer.MCAufgabeKopierer;

/**
 * @author Winkelv
 */
public class MCAufgabeEditorInput extends AbstractMatheEditorInput {

	/**
	 * @param pObject
	 */
	public MCAufgabeEditorInput(AbstractMatheAGObject pObject) {
		super(pObject);
		Assert.isTrue(pObject instanceof MCAufgabe, "Das Objekt ist keine MCAufgabe!");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.editorinputs.AbstractMatheEditorInput#getKopierer()
	 */
	@Override
	public IKopierer getKopierer() {
		return new MCAufgabeKopierer();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.editorinputs.AbstractMatheEditorInput#initCache()
	 */
	@Override
	public void initCache() {
		final MCAufgabe aufgabe = new MCAufgabe();
		aufgabe.setGesperrtFuerRaetsel(AbstractMatheAGObject.NOT_LOCKED);
		setCache(aufgabe);
		applyChanges();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Object getAdapter(Class pAdapter) {
		if (pAdapter.getName().equals(MCAufgabe.class.getName()))
			return getDomainObject();
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object pObj) {
		if (pObj == null)
			return false;
		if (!(pObj instanceof MCAufgabeEditorInput))
			return false;
		MCAufgabeEditorInput param = (MCAufgabeEditorInput) pObj;
		Object adapter1 = getAdapter(MCAufgabe.class);
		Object adapter2 = param.getAdapter(MCAufgabe.class);
		return adapter1.equals(adapter2);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.editorinputs.AbstractMatheEditorInput#getInputObjectType()
	 */
	@Override
	protected String getInputObjectType() {
		return MCAufgabe.class.getName();
	}
}
