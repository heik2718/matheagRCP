/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.comparators;

import java.util.Comparator;

import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblattitem;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;

/**
 * @author Winkelv
 */
public class AufgabensammlungItemViewerComparator extends AbstractViewerComparator {

	/**
	 * @param pIndex
	 */
	public AufgabensammlungItemViewerComparator(int pIndex) {
		super(pIndex);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Object pO1, Object pO2) {
		IAufgabensammlungItem item1 = (IAufgabensammlungItem) pO1;
		IAufgabensammlungItem item2 = (IAufgabensammlungItem) pO2;
		switch (getIndex()) {
		case 1: {
			if (item1 instanceof Arbeitsblattitem && item2 instanceof Arbeitsblattitem) {
				return compareArbeitsblattitems((Arbeitsblattitem) item1, (Arbeitsblattitem) item2);
			}
			return (item1.getNummer().compareToIgnoreCase(item2.getNummer()));
		}
		case 2: {
			return item1.getAufgabe().getStufe() - item2.getAufgabe().getStufe();
		}
		default:
			return item1.getAufgabe().getSchluessel().compareToIgnoreCase(item2.getAufgabe().getSchluessel());
		}
	}

	/**
	 * Arbeitsblattitems werden manchmal anders als alphabethisch sortiert.
	 * 
	 * @param pItem1
	 * @param pItem2
	 * @return
	 */
	private int compareArbeitsblattitems(Arbeitsblattitem pItem1, Arbeitsblattitem pItem2) {
		Comparator<IAufgabensammlungItem> comparator = MatheJungAltActivator.getDefault().getStammdatenservice()
			.getArbeitsblattItemNummerComparator();
		if (comparator == null) {
			return (pItem1.getNummer().compareToIgnoreCase(pItem1.getNummer()));
		}
		return comparator.compare(pItem1, pItem2);
	}
}
