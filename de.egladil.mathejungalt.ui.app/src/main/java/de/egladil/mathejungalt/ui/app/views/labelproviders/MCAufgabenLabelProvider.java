/**
 *
 */
package de.egladil.mathejungalt.ui.app.views.labelproviders;

import org.eclipse.swt.graphics.Color;

import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.ui.app.themes.LayoutFactory;

/**
 * @author aheike
 */
public class MCAufgabenLabelProvider extends CommonColumnLabelProvider {

	/**
	 *
	 */
	public MCAufgabenLabelProvider(int pIndex) {
		super(pIndex);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ColumnLabelProvider#getText(java.lang.Object)
	 */
	@Override
	public String getText(Object pElement) {
		final MCAufgabe aufgabe = (MCAufgabe) pElement;
		switch (getIndex()) {
		case 0:
			String schluessel = aufgabe.getSchluessel().toString();
			return schluessel;
		case 1:
			return aufgabe.getStufe() == null ? "" : aufgabe.getStufe().getStufe() + "";
		case 2:
			return aufgabe.getPunkte() == null ? "" : aufgabe.getPunkte().toString();
		case 3:
			return aufgabe.getThema() == null ? "" : aufgabe.getThema().getKurzbezeichnung();
		default:
			return aufgabe.getTitel();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.CellLabelProvider#getToolTipText(java.lang.Object)
	 */
	@Override
	public String getToolTipText(Object pElement) {
		return ((MCAufgabe) pElement).getBemerkung();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ColumnLabelProvider#getForeground(java.lang.Object)
	 */
	@Override
	public Color getForeground(Object pElement) {
		return LayoutFactory.getColorByMCAufgbe((MCAufgabe) pElement);
	}
}
