/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

/**
 * @author aheike
 */
public class ZweckMonoidDetailsPage extends AbstractDetailsPage {

	/**
	 * @param pMaster
	 */
	public ZweckMonoidDetailsPage(AbstractMasterDetailsBlock pMaster) {
		super(pMaster);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.parts.AbstractDetailsPage#getAttributesPart()
	 */
	@Override
	protected MatheAGObjectAttributesPart getAttributesPart() {
		MonoidstufeSimpleAttributePartDataBindingProvider contentsProvider = new MonoidstufeSimpleAttributePartDataBindingProvider();
		return new MatheAGObjectAttributesPart(false, contentsProvider);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.parts.AbstractDetailsPage#getHeader()
	 */
	@Override
	protected String getHeader() {
		return "Stufe";
	}
}
