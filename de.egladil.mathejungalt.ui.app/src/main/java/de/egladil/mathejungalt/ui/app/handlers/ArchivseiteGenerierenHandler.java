/**
 *
 */
package de.egladil.mathejungalt.ui.app.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.PlatformUI;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * @author aheike
 */
public class ArchivseiteGenerierenHandler extends AbstractHandler {

	/** */
	private IStammdatenservice stammdatenservice;

	/** */
	private final PathInitializer pathInitializer;

	/**
	 * @param pStammdatenservice
	 * @param pViewId
	 */
	public ArchivseiteGenerierenHandler(IStammdatenservice pStammdatenservice) {
		super();
		stammdatenservice = pStammdatenservice;
		pathInitializer = new PathInitializer();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent pArg0) throws ExecutionException {
		pathInitializer.initOutdirPath();
		if (pathInitializer.isCanceled()) {
			return null;
		}
		String pathOutDir = pathInitializer.getPathOutDir();
		try {
			stammdatenservice.generateMatheAGArchivseite(pathOutDir);
			MessageDialog.openInformation(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
				"Generator-Info", "Archivseite nach " + pathOutDir + " generiert");
		} catch (MatheJungAltException e) {
			MessageDialog.openError(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Generatorfehler",
				e.getMessage());
		}
		return null;
	}
}
