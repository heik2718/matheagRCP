/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.kopierer;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;

/**
 * @author aheike
 */
public class AufgabeKopierer implements IKopierer {

	/**
	 *
	 */
	public AufgabeKopierer() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.ui.app.editors.kopierer.IKopierer#copyAttributes(de.egladil.mathejungalt.domain.
	 * AbstractMatheAGObject, de.egladil.mathejungalt.domain.AbstractMatheAGObject)
	 */
	@Override
	public void copyAttributes(AbstractMatheAGObject pSource, AbstractMatheAGObject pTarget)
		throws MatheJungAltException {
		if (!(pSource instanceof Aufgabe) && !(pTarget instanceof Aufgabe)) {
			throw new MatheJungAltException(
				"Quelle oder Ziel sind keine Aufgaben: Quelle = " + pSource == null ? "null" : pSource.getClass()
					.getName() + ", Ziel = " + pTarget == null ? "null" : pTarget.getClass().getName());
		}
		Aufgabe source = (Aufgabe) pSource;
		Aufgabe target = (Aufgabe) pTarget;
		target.setArt(source.getArt());
		target.setBeschreibung(source.getBeschreibung());
		target.setGesperrtFuerWettbewerb(source.getGesperrtFuerWettbewerb());
		target.setGesperrtFuerArbeitsblatt(source.getGesperrtFuerArbeitsblatt());
		target.setHeft(source.getHeft());
		target.setId(source.getId());
		target.setJahreszeit(source.getJahreszeit());
		target.setQuelle(source.getQuelle());
		target.setSchluessel(source.getSchluessel());
		target.setStufe(source.getStufe());
		target.setThema(source.getThema());
		target.setTitel(source.getTitel());
		target.setVerzeichnis(source.getVerzeichnis());
	}
}
