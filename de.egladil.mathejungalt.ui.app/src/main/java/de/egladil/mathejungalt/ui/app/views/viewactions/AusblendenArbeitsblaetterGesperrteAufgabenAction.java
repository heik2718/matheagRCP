/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.viewactions;

import org.eclipse.jface.viewers.StructuredViewer;

import de.egladil.mathejungalt.ui.app.views.filters.AbstractMatheAGObjectFilter;
import de.egladil.mathejungalt.ui.app.views.filters.AufgabeFuerArbeitsblattGesperrtNegativeFilter;

/**
 * Blendet alle Aufgaben aus, die für sperrende Arbeitsblaetter gesperrt sind. Das sind die, die in Wettbewerb genutzt
 * wurden, aber in keinem sperrenden Arbeitsblatt.
 * 
 * @author winkelv
 */
public class AusblendenArbeitsblaetterGesperrteAufgabenAction extends AbstractAusblendenAufgabenAction {

	public final static String ID = "de.egladil.mathejungalt.ui.app.views.viewactions.AusblendenArbeitsblaetterGesperrteAufgabenAction";

	/**
	 * 
	 */
	public AusblendenArbeitsblaetterGesperrteAufgabenAction() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.rcp.views.actions.AbstractAusblendenAufgabenAction#getFilter(de.egladil.mathe.rcp.views.
	 * AbstractMatheAGStructuredView)
	 */
	@Override
	protected AbstractMatheAGObjectFilter getFilter(StructuredViewer pView) {
		return new AufgabeFuerArbeitsblattGesperrtNegativeFilter(pView, getStammdatenservice());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.views.actions.AbstractAusblendenAufgabenAction#getSpecialPattern()
	 */
	@Override
	protected String getSpecialPattern() {
		return "J";
	}
}
