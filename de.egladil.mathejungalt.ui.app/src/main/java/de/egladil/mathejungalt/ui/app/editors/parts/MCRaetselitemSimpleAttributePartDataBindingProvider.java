/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.events.IHyperlinkListener;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Hyperlink;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.IMatheAGObjectNames;
import de.egladil.mathejungalt.domain.mcraetsel.IMCAufgabeNames;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.ui.app.editors.MCAufgabeEditor;
import de.egladil.mathejungalt.ui.app.editors.converters.MCAufgabeToSchluesselConverter;
import de.egladil.mathejungalt.ui.app.editors.converters.SchluesselToMCAufgabeConverter;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.MCAufgabeEditorInput;
import de.egladil.mathejungalt.ui.app.editors.listeners.HyperlinkListenerAdapter;
import de.egladil.mathejungalt.ui.app.handlers.EditObjectHandlerDelegate;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;
import de.egladil.mathejungalt.ui.app.themes.PartControlsFactory;

/**
 * @author winkelv
 */
public class MCRaetselitemSimpleAttributePartDataBindingProvider extends AbstractSimpleAttributePartDataBindingProvider {

	/** */
	private IHyperlinkListener hyperlinkListener;

	/** */
	private Control[] labels;

	/** */
	private static final int HYPERLINK_INDEX = 1;

	/**
	 *
	 */
	public MCRaetselitemSimpleAttributePartDataBindingProvider() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.contentproviders.AbstractAttributePartContentsProvider#anzahlZeilen()
	 */
	@Override
	protected int anzahlZeilen() {
		return 3;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.contentproviders.AbstractAttributePartContentsProvider#initLabelsAndTexts(org.eclipse
	 * .swt.widgets .Composite, org.eclipse.ui.forms.widgets.FormToolkit, boolean)
	 */
	@Override
	protected void initControls(Composite pClient, FormToolkit pToolkit, boolean pEditable, AbstractMatheAGObject pInput) {
		final MCArchivraetselItem raetselitem = (MCArchivraetselItem) pInput;
		boolean editable = !raetselitem.getRaetsel().isVeroeffentlicht();

		final Control[] controls = getControls();
		labels = new Control[controls.length];

		int widthHint = 150;
		labels[0] = PartControlsFactory.createIdLabel(pClient, pToolkit);
		controls[0] = PartControlsFactory.createIdText(pClient, pToolkit, widthHint);

		labels[1] = pToolkit.createHyperlink(pClient, ITextConstants.LABEL_TEXT_AUFGABE, SWT.NULL);

		controls[1] = PartControlsFactory.createText(pClient, pToolkit, widthHint, false);
		((Text) controls[1]).setTextLimit(IMCAufgabeNames.LENGTH_SCHLUESSEL);

		labels[2] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_NUMMER);
		controls[2] = PartControlsFactory.createText(pClient, pToolkit, widthHint, editable);
		((Text) controls[2]).setTextLimit(2);

		refreshHyperlinkListener(pInput);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.contentproviders.IAttributePartContentsProvider#refreshDataBindings(de.egladil.mathe
	 * .core.domain .AbstractMatheAGObject)
	 */
	@Override
	public void refreshDataBindings(AbstractMatheAGObject pInput) {
		super.refreshDataBindings(pInput);

		Binding[] bindings = getBindings();
		Control[] controls = getControls();

		IObservableValue observableWidget = SWTObservables.observeText(controls[0], SWT.Modify);
		IObservableValue observableProperty = BeansObservables.observeValue(pInput, IMatheAGObjectNames.PROP_ID);
		bindings[0] = getDbc().bindValue(observableWidget, observableProperty, null, null);

		observableWidget = SWTObservables.observeText(controls[1], SWT.Modify);
		observableProperty = BeansObservables.observeValue(pInput, MCArchivraetselItem.PROP_AUFGABE);
		UpdateValueStrategy targetToModelStrategy = new UpdateValueStrategy();
		targetToModelStrategy.setConverter(new SchluesselToMCAufgabeConverter());
		UpdateValueStrategy modelToTargetStrategy = new UpdateValueStrategy();
		modelToTargetStrategy.setConverter(new MCAufgabeToSchluesselConverter());
		bindings[1] = getDbc().bindValue(observableWidget, observableProperty, targetToModelStrategy,
			modelToTargetStrategy);

		observableWidget = SWTObservables.observeText(controls[2], SWT.Modify);
		observableProperty = BeansObservables.observeValue(pInput, MCArchivraetselItem.PROP_NUMMER);
		bindings[2] = getDbc().bindValue(observableWidget, observableProperty, null, null);

		refreshHyperlinkListener(pInput);
	}

	/**
	 * Erzeugt einen neuen HyperlinkListerner für den Aufgabe-Hyperlink
	 *
	 * @param pInput
	 * @return
	 */
	private void refreshHyperlinkListener(AbstractMatheAGObject pInput) {
		if (hyperlinkListener != null) {
			((Hyperlink) labels[HYPERLINK_INDEX]).removeHyperlinkListener(hyperlinkListener);
		}
		final MCArchivraetselItem raetselItem = (MCArchivraetselItem) pInput;
		hyperlinkListener = new HyperlinkListenerAdapter() {

			private final EditObjectHandlerDelegate editObjectAction = new EditObjectHandlerDelegate(MCAufgabeEditor.ID);

			@Override
			public void linkActivated(HyperlinkEvent pE) {
				MCAufgabeEditorInput editorInput = new MCAufgabeEditorInput(raetselItem.getAufgabe());
				editObjectAction.executeCommand(editorInput);
			}
		};
		((Hyperlink) labels[HYPERLINK_INDEX]).addHyperlinkListener(hyperlinkListener);
	}
}
