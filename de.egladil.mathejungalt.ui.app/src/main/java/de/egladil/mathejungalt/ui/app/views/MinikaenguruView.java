package de.egladil.mathejungalt.ui.app.views;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.handlers.IHandlerService;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;
import de.egladil.mathejungalt.ui.app.editors.MinikaenguruEditor;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.AbstractMatheEditorInput;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.MinikaenguruEditorInput;
import de.egladil.mathejungalt.ui.app.handlers.ICommandIds;
import de.egladil.mathejungalt.ui.app.handlers.MailinglisteMinikaenguruAnzeigenHandler;
import de.egladil.mathejungalt.ui.app.themes.LayoutFactory;
import de.egladil.mathejungalt.ui.app.themes.TableViewerFactory;
import de.egladil.mathejungalt.ui.app.views.comparators.AbstractViewerComparator;
import de.egladil.mathejungalt.ui.app.views.comparators.IViewerComparator;
import de.egladil.mathejungalt.ui.app.views.comparators.NumericComparator;
import de.egladil.mathejungalt.ui.app.views.contentproviders.AufgabensammlungViewContentProvider;
import de.egladil.mathejungalt.ui.app.views.listeners.AbstractModelListener;
import de.egladil.mathejungalt.ui.app.views.listeners.MinikaenguruModelListener;

/**
 * @author aheike
 */
public class MinikaenguruView extends AbstractAufgabensammlungView {

	/** */
	public static final String ID = "de.egladil.mathejungalt.ui.app.minikaenguruview";

	/** */
	public static final int INDEX_JAHR = 0;

	/** */
	private final String[] columnTitles = new String[] { "Jahr" };

	/** */
	private final int[] columnBounds = new int[] { 80 };

	/**
	 * 
	 */
	public MinikaenguruView() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.views.AbstractMatheAGStructuredView#initTableViewer(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected TableViewer initTableViewer(Composite pParent) {
		ITableViewColumn[] viewColumns = new ITableViewColumn[columnTitles.length];
		IViewerComparator[] comparators = getColumnComparators();
		ColumnLabelProvider[] labelProviders = getColumnLableProviders();
		for (int i = 0; i < columnTitles.length; i++) {
			viewColumns[i] = new ViewColumn(columnTitles[i], columnBounds[i], comparators[i], labelProviders[i]);
		}
		return TableViewerFactory.createTableViewer(pParent, new AufgabensammlungViewContentProvider(
			getStammdatenservice()), viewColumns, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#getModelListener(org.eclipse.jface.viewers.TableViewer
	 * )
	 */
	@Override
	protected AbstractModelListener getModelListener(TableViewer pViewer) {
		return new MinikaenguruModelListener(pViewer);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#getEditorID()
	 */
	@Override
	protected String getEditorID() {
		return MinikaenguruEditor.ID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#getEditorInput(java.lang.Object)
	 */
	@Override
	protected AbstractMatheEditorInput getEditorInput(Object pObj) {
		if (!(pObj instanceof Minikaenguru)) {
			return null;
		}
		Minikaenguru mini = (Minikaenguru) pObj;
		if (!mini.isItemsLoaded()) {
			getStammdatenservice().aufgabensammlungitemsLaden(mini);
		}
		return new MinikaenguruEditorInput((AbstractMatheAGObject) pObj);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractAufgabensammlungView#getColumnComparators()
	 */
	protected IViewerComparator[] getColumnComparators() {
		return new AbstractViewerComparator[] { new NumericComparator(Minikaenguru.class.getName()) };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractAufgabensammlungView#getColumnLableProviders()
	 */
	protected ColumnLabelProvider[] getColumnLableProviders() {
		return new ColumnLabelProvider[] { new ColumnLabelProvider() {
			@Override
			public String getText(Object pElement) {
				return ((Minikaenguru) pElement).getJahr().toString();
			}

			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.jface.viewers.ColumnLabelProvider#getForeground(java.lang.Object)
			 */
			@Override
			public Color getForeground(Object pElement) {
				return LayoutFactory.getColorByBeendetStatus((IAufgabensammlung) pElement);
			}
		} };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractAufgabensammlungView#getColumnTitles()
	 */
	protected String[] getColumnTitles() {
		return columnTitles;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractAufgabensammlungView#getColumnBounds()
	 */
	protected int[] getColumnBounds() {
		return columnBounds;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractAufgabensammlungView#getViewID()
	 */
	@Override
	protected String getViewID() {
		return ID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractAufgabensammlungView#createHandlers()
	 */
	@Override
	protected void createHandlers() {
		super.createHandlers();
		IHandlerService handlerService = (IHandlerService) getSite().getService(IHandlerService.class);
		handlerService.activateHandler(ICommandIds.MAILINKLISTE_MINIKAENGURU_ANZEIGEN,
			new MailinglisteMinikaenguruAnzeigenHandler(getViewer(), getStammdatenservice()));
	}
}
