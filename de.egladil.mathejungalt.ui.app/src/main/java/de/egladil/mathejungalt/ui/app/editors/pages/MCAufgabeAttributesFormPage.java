/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.pages;

import java.beans.PropertyChangeEvent;

import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormEditor;

import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.editors.documentproviders.ImageProvider;
import de.egladil.mathejungalt.ui.app.editors.documentproviders.ImageViewer;
import de.egladil.mathejungalt.ui.app.editors.parts.MCAufgabeSimpleAttributePartDataBindingProvider;
import de.egladil.mathejungalt.ui.app.editors.parts.MatheAGObjectAttributesPart;
import de.egladil.mathejungalt.ui.app.themes.IImageKeys;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Die AttributesFormPage verhält sich wie der MasterPart in einem MasterDetailsBlock: je nach Quellenart wir ein
 * eigener DetailsPart angezeigt
 *
 * @author winkelv
 */
public class MCAufgabeAttributesFormPage extends AbstractAttributesFormPage {

	/** */
	private ImageViewer imageViewer;

	/** */
	private ImageProvider imageProvider;

	/** */
	private Image image;

	/**
	 * @param pEditor
	 * @param pId
	 * @param pTitle
	 */
	public MCAufgabeAttributesFormPage(FormEditor pEditor, String pId, String pTitle) {
		super(pEditor, pId, pTitle);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.pages.AbstractAttributesFormPage#hookAdditionalParts(org.eclipse.ui.forms.IManagedForm
	 * )
	 */
	@Override
	protected void hookAdditionalParts(IManagedForm pManagedForm) {
		// keine
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.pages.AbstractAttributesFormPage#getAttributesPart()
	 */
	@Override
	protected MatheAGObjectAttributesPart getAttributesPart() {
		return new MatheAGObjectAttributesPart(true, new MCAufgabeSimpleAttributePartDataBindingProvider());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.pages.AbstractAttributesFormPage#getHeader()
	 */
	@Override
	protected String getHeader() {
		return ITextConstants.MCAUFGABE_EDITOR_HEADER;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.pages.AbstractAttributesFormPage#getSectioHeader()
	 */
	@Override
	protected String getSectioHeader() {
		return ITextConstants.SECTION_HEADER_AUFGABE_ATTRIBUTES;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.pages.AbstractAttributesFormPage#getImage()
	 */
	@Override
	protected Image getImage() {
		return MatheJungAltActivator.getDefault().getImage(IImageKeys.AUFGABE);
	}

	@Override
	public void propertyChange(PropertyChangeEvent pEvt) {
		super.propertyChange(pEvt);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.pages.AbstractAttributesFormPage#dispose()
	 */
	@Override
	public void dispose() {
		if (imageProvider != null) {
			imageProvider.disposeImage(image);
		}
		// if (!imageViewer.isDisposed()){
		// imageViewer.dispose();
		// }
		super.dispose();
	}
}
