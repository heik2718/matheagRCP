/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.converters;

import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.domain.types.EnumTypes.Thema;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Konvertiert ein {@link Thema} in einen String.
 * 
 * @author Winkelv
 */
public class ThemaToStringConverter implements IConverter {

	/**
	 * 
	 */
	public ThemaToStringConverter() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object pFromObject) {
		Thema param = (Thema) pFromObject;
		switch (param) {
		case A:
			return ITextConstants.THEMA_ARITHMETIK;
		case G:
			return ITextConstants.THEMA_GEOMETRIE;
		case K:
			return ITextConstants.THEMA_KOMBINATORIK;
		case L:
			return ITextConstants.THEMA_LOGIK;
		case W:
			return ITextConstants.THEMA_WAHRSCHEINLICHKEIT;
		default:
			return ITextConstants.THEMA_ZAHLENTHEORIE;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getFromType()
	 */
	@Override
	public Object getFromType() {
		return Thema.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getToType()
	 */
	@Override
	public Object getToType() {
		return String.class;
	}
}
