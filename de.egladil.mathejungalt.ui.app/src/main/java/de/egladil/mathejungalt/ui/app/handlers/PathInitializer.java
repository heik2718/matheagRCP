/**
 * 
 */
package de.egladil.mathejungalt.ui.app.handlers;

import java.io.File;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.ui.PlatformUI;

import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.preferences.PreferenceConstants;

/**
 * @author aheike
 */
public class PathInitializer {

	/** */
	private String pathOutDir;

	/** */
	private boolean canceled = false;

	/**
	 * 
	 */
	public PathInitializer() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	protected void initOutdirPath() {
		initPath();
		MessageDialog dialog = new MessageDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
			"Verzeichnis Ergebnisdateien (Modus=" + MatheJungAltActivator.getDefault().getRaetselListeGeneratorModus()
				+ ")", null, "Sollen die Ergebnisdateien in das Verzeichnis\n" + pathOutDir + "\ngeschrieben werden?",
			SWT.NULL, new String[] { "Ja", "Nein", "Abbrechen" }, 0);
		int ok = dialog.open();
		if (ok == 2 || ok == -1) {
			canceled = true;
			return;
		}
		if (ok != MessageDialog.OK) {
			DirectoryDialog dirDialog = new DirectoryDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getShell());
			String path = dirDialog.open();
			if (path == null) {
				canceled = true;
				return;
			}
			if (new File(path).isDirectory()) {
				pathOutDir = path;
			}
		}
	}

	/**
	 * 
	 */
	private void initPath() {
		if (pathOutDir == null) {
			pathOutDir = MatheJungAltActivator.getDefault().getPath(PreferenceConstants.P_PATH_GENERATOR_OUT_DIR);
		}
	}

	/**
	 * @return the pathOutDir
	 */
	protected String getPathOutDir() {
		return pathOutDir;
	}

	/**
	 * @return the canceled
	 */
	protected boolean isCanceled() {
		return canceled;
	}

}
