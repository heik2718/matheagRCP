/**
 *
 */
package de.egladil.mathejungalt.ui.app.dialogs.contentproviders;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.IWizardNode;

import de.egladil.mathejungalt.domain.quellen.AbstractQuelle;
import de.egladil.mathejungalt.domain.types.EnumTypes.Quellenart;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.dialogs.CreateQuelleWizardSelectionPage;
import de.egladil.mathejungalt.ui.app.dialogs.QuellenartWizardNode;

/**
 * Stellt die {@link IWizardNode} - Objekte f�r die QuellenWizardSelectionPage zur Verf�gung.
 *
 * @author Winkelv
 */
public class QuellenWizardsTableContentProvider implements IStructuredContentProvider {

	/** */
	private IStammdatenservice stammdatenservice;

	/**
	 * wird benötigt, um in die Wizards eine Referenz zu injezieren, der die erzeugte {@link AbstractQuelle} übergeben
	 * werden kann
	 */
	private CreateQuelleWizardSelectionPage selectionPage;

	/**
	 *
	 */
	public QuellenWizardsTableContentProvider(CreateQuelleWizardSelectionPage pSelectionPage,
		IStammdatenservice pStammdatenservice) {
		selectionPage = pSelectionPage;
		stammdatenservice = pStammdatenservice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
	 */
	@Override
	public Object[] getElements(Object pInputElement) {
		QuellenartWizardNode[] nodes = new QuellenartWizardNode[5];
		nodes[0] = new QuellenartWizardNode(Quellenart.B, selectionPage, stammdatenservice);
		nodes[1] = new QuellenartWizardNode(Quellenart.G, selectionPage, stammdatenservice);
		nodes[2] = new QuellenartWizardNode(Quellenart.K, selectionPage, stammdatenservice);
		nodes[3] = new QuellenartWizardNode(Quellenart.Z, selectionPage, stammdatenservice);
		nodes[4] = new QuellenartWizardNode(Quellenart.N, selectionPage, stammdatenservice);
		return nodes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
	 */
	@Override
	public void dispose() {
		// nix

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public void inputChanged(Viewer pViewer, Object pOldInput, Object pNewInput) {
		// nix
	}

}
