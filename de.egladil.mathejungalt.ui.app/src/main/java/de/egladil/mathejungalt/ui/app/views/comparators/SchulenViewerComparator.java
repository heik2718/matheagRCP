/**
 *
 */
package de.egladil.mathejungalt.ui.app.views.comparators;

import de.egladil.mathejungalt.domain.schulen.Schule;

/**
 * @author Winkelv
 */
public class SchulenViewerComparator extends AbstractViewerComparator {

	/**
	 * @param pIndex
	 */
	public SchulenViewerComparator(int pIndex) {
		super(pIndex);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Object pO1, Object pO2) {
		switch (getIndex()) {
		case 0:
			return ((Schule) pO1).getSchluessel().compareTo(((Schule) pO2).getSchluessel());
		case 1:
			return ((Schule) pO1).getName().compareToIgnoreCase(((Schule) pO2).getName());
		default:
			return ((Schule) pO1).getLand().getSchluessel()
				.compareToIgnoreCase(((Schule) pO2).getLand().getSchluessel());
		}
	}
}
