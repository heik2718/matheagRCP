/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.forms.widgets.FormToolkit;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.quellen.IZeitschriftquelleNames;
import de.egladil.mathejungalt.domain.quellen.Zeitschriftquelle;
import de.egladil.mathejungalt.ui.app.editors.converters.ZeitschriftToTitelConverter;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;
import de.egladil.mathejungalt.ui.app.themes.PartControlsFactory;

/**
 * Der Teil, der die speziellen Attribute der Zeitschriftquelle anzeigt.
 * 
 * @author winkelv
 */
public class ZeitschriftquelleSimpleAttributePartDataBindingProvider extends
	AbstractSimpleAttributePartDataBindingProvider {

	/**
	 * 
	 */
	public ZeitschriftquelleSimpleAttributePartDataBindingProvider() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractSimpleAttributePartDataBindingProvider#anzahlZeilen()
	 */
	@Override
	protected int anzahlZeilen() {
		return 3;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.parts.AbstractSimpleAttributePartDataBindingProvider#initControls(org.eclipse.swt
	 * .widgets.Composite , org.eclipse.ui.forms.widgets.FormToolkit, boolean,
	 * de.egladil.mathe.core.domain.AbstractMatheAGObject)
	 */
	@Override
	protected void initControls(Composite pClient, FormToolkit pToolkit, boolean pEditable, AbstractMatheAGObject pInput) {
		final Control[] controls = getControls();
		Control[] labels = new Control[controls.length];

		int widthHint = 250;

		labels[0] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_ZEITSCHRIFT);
		controls[0] = PartControlsFactory.createText(pClient, pToolkit, widthHint, false);

		labels[1] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_AUSGABE);
		controls[1] = PartControlsFactory.createText(pClient, pToolkit, widthHint, false);

		labels[2] = PartControlsFactory.createLabel(pClient, pToolkit, ITextConstants.LABEL_TEXT_JAHRGANG);
		controls[2] = PartControlsFactory.createText(pClient, pToolkit, widthHint, false);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.parts.AbstractSimpleAttributePartDataBindingProvider#refreshDataBindings(de.egladil
	 * .mathe.core .domain.AbstractMatheAGObject)
	 */
	@Override
	public void refreshDataBindings(AbstractMatheAGObject pInput) {
		if (!(pInput instanceof Aufgabe)) {
			return;
		}
		super.refreshDataBindings(pInput);

		Zeitschriftquelle quelle = (Zeitschriftquelle) ((Aufgabe) pInput).getQuelle();

		Binding[] bindings = getBindings();
		Control[] controls = getControls();

		IObservableValue observableWidget = SWTObservables.observeText(controls[0], SWT.Modify);
		IObservableValue observableProperty = BeansObservables.observeValue(quelle,
			IZeitschriftquelleNames.PROP_ZEITSCHRIFT);
		UpdateValueStrategy modelToTargetStrategy = new UpdateValueStrategy();
		modelToTargetStrategy.setConverter(new ZeitschriftToTitelConverter());
		bindings[0] = getDbc().bindValue(observableWidget, observableProperty, null, modelToTargetStrategy);

		observableWidget = SWTObservables.observeText(controls[1], SWT.Modify);
		observableProperty = BeansObservables.observeValue(quelle, IZeitschriftquelleNames.PROP_AUSGABE);
		bindings[1] = getDbc().bindValue(observableWidget, observableProperty, null, null);

		observableWidget = SWTObservables.observeText(controls[2], SWT.Modify);
		observableProperty = BeansObservables.observeValue(quelle, IZeitschriftquelleNames.PROP_JAHRGANG);
		bindings[2] = getDbc().bindValue(observableWidget, observableProperty, null, null);
	}
}
