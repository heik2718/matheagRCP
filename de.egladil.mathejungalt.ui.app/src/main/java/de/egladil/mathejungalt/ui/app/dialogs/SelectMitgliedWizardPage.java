/**
 * 
 */
package de.egladil.mathejungalt.ui.app.dialogs;

import org.eclipse.swt.SWT;

import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.dialogs.labelproviders.MitgliedListLabelProvider;
import de.egladil.mathejungalt.ui.app.themes.IImageKeys;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * @author Winkelv
 */
public class SelectMitgliedWizardPage extends AbstractSelectMatheAGObjectWizardPage {

	/**
	 * @param pPageName
	 */
	public SelectMitgliedWizardPage() {
		super(ITextConstants.QUELLE_SELECT_MITGLIED_PAGE_NAME, ITextConstants.QUELLE_SELECT_MITGLIED_PAGE_TITLE,
			MatheJungAltActivator.imageDescriptorFromPlugin(MatheJungAltActivator.PLUGIN_ID, IImageKeys.QUELLE_WIZARD),
			new MitgliedListLabelProvider(), SWT.SINGLE);
		setDescription(ITextConstants.QUELLE_SELECT_MITGLIED_PAGE_DESCR);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.dialogs.AbstractSelectMatheAGObjectWizardPage#getListElements()
	 */
	@Override
	protected Object[] getListElements() {
		return MatheJungAltActivator.getDefault().getTableViewerContentManager().getElements(Mitglied.class.getName());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.dialogs.AbstractSelectMatheAGObjectWizardPage#handleDefaultSelected()
	 */
	@Override
	protected void handleDefaultSelected() {
		super.handleDefaultSelected();
		if (getFirstSelectedDomainObject() != null) {
			setPageComplete(true);
		} else {
			setPageComplete(false);
		}
	}
}
