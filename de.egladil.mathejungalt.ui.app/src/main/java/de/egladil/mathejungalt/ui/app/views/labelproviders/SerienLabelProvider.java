/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.labelproviders;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;

import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.themes.IImageKeys;
import de.egladil.mathejungalt.ui.app.views.AutorenView;

/**
 * LabelProvider für den {@link AutorenView}
 * 
 * @author aheike
 */
public class SerienLabelProvider extends ColumnLabelProvider implements ITableLabelProvider {

	/**
	 * 
	 */
	public SerienLabelProvider() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object, int)
	 */
	@Override
	public Image getColumnImage(Object pArg0, int pArg1) {
		return getImage(pArg0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object, int)
	 */
	@Override
	public String getColumnText(Object pArg0, int pArg1) {
		return getText(pArg0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.LabelProvider#getImage(java.lang.Object)
	 */
	@Override
	public Image getImage(Object pElement) {
		ImageDescriptor imDes = MatheJungAltActivator.getImageDescriptor(IImageKeys.SERIE);
		return imDes.createImage();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
	 */
	@Override
	public String getText(Object pElement) {
		Serie serie = (Serie) pElement;
		return "Serie " + serie.getNummer();
	}
}
