/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.converters;

import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.domain.mitglieder.Mitglied;

/**
 * Wandelt eine Buchquelle zu dem dahinterliegenden Buch um
 * 
 * @author Winkelv
 */
public class MitgliedToNameConverter implements IConverter {

	/**
	 * 
	 */
	public MitgliedToNameConverter() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object pFromObject) {
		if (pFromObject instanceof Mitglied) {
			final Mitglied mitglied = (Mitglied) pFromObject;
			String str = mitglied.getVorname();
			if (mitglied.getNachname() != null) {
				str += " " + mitglied.getNachname();
			}
			return str;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getFromType()
	 */
	@Override
	public Object getFromType() {
		return Mitglied.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getToType()
	 */
	@Override
	public Object getToType() {
		return String.class;
	}

}
