/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.ui.app.editors.kopierer;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.domain.schulen.Schulkontakt;

/**
 * <p>
 * NOTE: THIS WIDGET AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 * 
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class KontaktKopierer implements IKopierer {

	/**
   * 
   */
	public KontaktKopierer() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.kopierer.IKopierer#copyAttributes(de.egladil.mathejungalt.domain.
	 * AbstractMatheAGObject, de.egladil.mathejungalt.domain.AbstractMatheAGObject)
	 */
	@Override
	public void copyAttributes(AbstractMatheAGObject pSource, AbstractMatheAGObject pTarget)
		throws MatheJungAltException {
		if (!(pSource instanceof Kontakt) && !(pTarget instanceof Kontakt)) {
			throw new MatheJungAltException(
				"Quelle oder Ziel sind keine Kontakte: Quelle = " + pSource == null ? "null" : pSource.getClass()
					.getName() + ", Ziel = " + pTarget == null ? "null" : pTarget.getClass().getName());
		}
		Kontakt source = (Kontakt) pSource;
		Kontakt target = (Kontakt) pTarget;

		target.setAbo(source.isAbo());
		target.setEmail(source.getEmail());
		target.setName(source.getName());

		for (Schulkontakt schulkontakt : source.getSchulkontakte()) {
			target.addSchulkontakt(schulkontakt);
		}

	}
}
