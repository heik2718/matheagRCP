/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.ui.app.editors.converters.IntegerToSerieStufetextConverter;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Der Teil, der die speziellen Stufen für Serienaufgaben anzeigt.
 * 
 * @author winkelv
 */
public class SerienstufeSimpleAttributePartDataBindingProvider extends
	AbstractStufeSimpleAttributePartDataBindingProvider {

	/** */
	private final IConverter converter;

	/**
	 * 
	 */
	public SerienstufeSimpleAttributePartDataBindingProvider() {
		super();
		converter = new IntegerToSerieStufetextConverter();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.parts.AbstractStufeSimpleAttributePartDataBindingProvider#getItems()
	 */
	protected String[] getItems() {
		return new String[] { ITextConstants.STUFE_1, ITextConstants.STUFE_2, ITextConstants.STUFE_3,
			ITextConstants.STUFE_4, ITextConstants.STUFE_5, ITextConstants.STUFE_6 };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.editors.parts.AbstractStufeSimpleAttributePartDataBindingProvider#getConverter()
	 */
	@Override
	protected IConverter getConverter() {
		return converter;
	}
}
