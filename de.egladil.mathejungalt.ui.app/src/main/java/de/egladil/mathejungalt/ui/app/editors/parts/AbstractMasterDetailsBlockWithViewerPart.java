/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.dnd.DropTargetListener;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.SectionPart;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.services.IDisposable;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.ui.app.editors.pages.AbstractAttributesFormPage;

/**
 * Abstrakte Oberklasse für Master-Details-Teile mit ViewerParts in den Editoren.
 * 
 * @author winkelv
 */
public abstract class AbstractMasterDetailsBlockWithViewerPart extends AbstractMasterDetailsBlock implements
	IDisposable, IEditorPartInputProvider, PropertyChangeListener {

	/** */
	private AbstractViewerPartWithButtons viewerPart;

	/** */
	private DropTarget target;

	/** */
	private DropTargetListener dropTargetListener;

	/**
	 * @param pParentPage
	 */
	public AbstractMasterDetailsBlockWithViewerPart(AbstractAttributesFormPage pParentPage) {
		super(pParentPage);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.parts.AbstractMasterDetailsBlock#hookSpecialParts(org.eclipse.ui.forms.IManagedForm,
	 * org.eclipse.ui.forms.widgets.Section, org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void hookSpecialParts(final IManagedForm pManagedForm, Section pSection, Composite pComposite) {
		FormToolkit toolkit = pManagedForm.getToolkit();
		hookViewerPart();
		viewerPart.init(pComposite, toolkit);

		final SectionPart spart = new SectionPart(pSection);
		pManagedForm.addPart(spart);

		// Hier beim Viewer des ViewerParts einen ISelectionChangedListener registrieren, der das event weiterwirft
		viewerPart.getViewer().addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(SelectionChangedEvent event) {
				pManagedForm.fireSelectionChanged(spart, event.getSelection());
			}
		});

		addDropSupport();
	}

	/**
	 * Fügt Dropsupport hinzu
	 */
	private void addDropSupport() {
		// mache das EditorArea zum DropTarget
		int operations = DND.DROP_COPY | DND.DROP_DEFAULT;
		target = new DropTarget(viewerPart.getViewer().getControl(), operations);
		Transfer[] transferTypes = new Transfer[] { LocalSelectionTransfer.getTransfer() };
		target.setTransfer(transferTypes);

		hookDropTargetListener();

		if (dropTargetListener != null) {
			target.addDropListener(dropTargetListener);
		}
	}

	/**
	 * Als Standard- {@link DropTargetListener} wird ein Adapter mit Implementierungen verwendet, der nur COPY-Drops
	 * anzeigt. Unterklassen sollten hier einen Adapter verwenden, der die Methode drop() �berschreibt, um ihre
	 * Spezielle drop-Aktion zu beenden.
	 */
	protected void hookDropTargetListener() {
		dropTargetListener = null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractMasterDetailsBlock#createContextMenuActions()
	 */
	@Override
	protected void createContextMenuActions() {
		MenuManager popupMenuManager = new MenuManager();
		IMenuListener listener = new IMenuListener() {
			public void menuAboutToShow(IMenuManager mng) {
				viewerPart.fillContextMenu(mng);
			}
		};
		popupMenuManager.addMenuListener(listener);
		popupMenuManager.setRemoveAllWhenShown(true);
		Control control = viewerPart.getViewer().getControl();
		Menu menu = popupMenuManager.createContextMenu(control);
		control.setMenu(menu);
	}

	/**
	 * Instanziiert den speziellen ViewerPart
	 */
	protected abstract void hookViewerPart();

	/**
	 * @return the viewerPart
	 */
	public AbstractViewerPartWithButtons getViewerPart() {
		return viewerPart;
	}

	/**
	 * @param pViewerPart the viewerPart to set
	 */
	public void setViewerPart(AbstractViewerPartWithButtons pViewerPart) {
		viewerPart = pViewerPart;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent pEvt) {
		viewerPart.handlePropertyChangeEvent(pEvt);
	}

	/**
	 * geordnetes Freigeben
	 */
	public void dispose() {
		// viewerPart.dispose();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.IEditorPartInputProvider#getSelectedInput()
	 */
	public AbstractMatheAGObject getSelectedInput() {
		return (AbstractMatheAGObject) ((IStructuredSelection) viewerPart.getViewer().getSelection()).getFirstElement();
	}

	/**
	 * @param pDropTargetListener the dropTargetListener to set
	 */
	protected final void setDropTargetListener(DropTargetListener pDropTargetListener) {
		dropTargetListener = pDropTargetListener;
	}
}
