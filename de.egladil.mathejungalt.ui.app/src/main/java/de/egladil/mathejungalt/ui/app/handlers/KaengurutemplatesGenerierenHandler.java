/**
 *
 */
package de.egladil.mathejungalt.ui.app.handlers;

import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.domain.types.MCAufgabenstufen;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.dialogs.KaengurutemplatesGenerierenDialog;
import de.egladil.mathejungalt.ui.app.views.MCAufgabenView;

/**
 * @author heike
 */
public class KaengurutemplatesGenerierenHandler extends AbstractHandler {

	private static final Logger LOG = LoggerFactory.getLogger(KaengurutemplatesGenerierenHandler.class);

	private final IStammdatenservice stammdatenservice;

	private final MCAufgabenView view;

	/**
	 * @param pStammdatenservice
	 */
	public KaengurutemplatesGenerierenHandler(IStammdatenservice pStammdatenservice, MCAufgabenView pView) {
		stammdatenservice = pStammdatenservice;
		view = pView;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent pArg0) throws ExecutionException {
		KaengurutemplatesGenerierenDialog dialog = new KaengurutemplatesGenerierenDialog(Display.getDefault()
			.getActiveShell());
		int result = dialog.open();
		if (result == IDialogConstants.OK_ID) {
			String schluessel = dialog.getSchluessel();
			int anzahl = dialog.getAnzahl();
			MCAufgabenstufen stufe = dialog.getStufe();
			try {
				List<MCAufgabe> aufgaben = stammdatenservice
					.kaenguruAufgabentemplatesAnlegen(schluessel, anzahl, stufe);
				MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "Info", aufgaben.size()
					+ " Templates generiert");
				view.refresh();
			} catch (Exception e) {
				MessageDialog.openError(Display.getCurrent().getActiveShell(), "Fehler", "Fehler beim Generieren:\n"
					+ e.getMessage());
			}
		}
		return null;
	}
}
