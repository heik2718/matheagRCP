/**
 *
 */
package de.egladil.mathejungalt.ui.app.preferences;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import de.egladil.mathejungalt.config.IMatheJungAltConfiguration;
import de.egladil.mathejungalt.config.IMatheJungAltConfiguration.RaetsellisteGeneratorModus;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;

/**
 * @author aheike
 */
public class MatheJungAltPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	/** */
	private DirectoryFieldEditor aufgabenarchivPathEditor;

	/** */
	private DirectoryFieldEditor loesungsarchivPathEditor;

	/** */
	private DirectoryFieldEditor pngsPathEditor;

	/** */
	private DirectoryFieldEditor generatedPathEditor;

	private ComboFieldEditor raetselGeneratorModusEditor;

	private BooleanFieldEditor stufe6DauerhaftSperrenEditor;

	private IntegerFieldEditor aktuellesMinikaenguruJahrEditor;

	/**
	 *
	 */
	public MatheJungAltPreferencePage() {
		super(GRID);
		setPreferenceStore(MatheJungAltActivator.getDefault().getPreferenceStore());
		setDescription("Pfade f\u00FCr MatheJungAlt");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	@Override
	public void init(IWorkbench pArg0) {
		setPreferenceStore(MatheJungAltActivator.getDefault().getPreferenceStore());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors()
	 */
	@Override
	protected void createFieldEditors() {
		aufgabenarchivPathEditor = new DirectoryFieldEditor(PreferenceConstants.P_PATH_AUFGABENARCHIV_DIR,
			"Aufgabenarchiv", getFieldEditorParent());
		addField(aufgabenarchivPathEditor);

		loesungsarchivPathEditor = new DirectoryFieldEditor(PreferenceConstants.P_PATH_LOESUNGENARCHIV_DIR,
			"L\u00F6sungsarchiv", getFieldEditorParent());
		addField(loesungsarchivPathEditor);

		pngsPathEditor = new DirectoryFieldEditor(PreferenceConstants.P_PATH_PNG_DIR, "PNG-Verzeichnis",
			getFieldEditorParent());
		addField(pngsPathEditor);

		generatedPathEditor = new DirectoryFieldEditor(PreferenceConstants.P_PATH_GENERATOR_OUT_DIR,
			"generierte Dateien", getFieldEditorParent());
		addField(generatedPathEditor);

		final RaetsellisteGeneratorModus[] alleModus = IMatheJungAltConfiguration.RaetsellisteGeneratorModus.values();
		final int anzahl = alleModus.length;
		final String[][] contents = new String[anzahl][1];
		for (int i = 0; i < anzahl; i++) {
			RaetsellisteGeneratorModus modus = alleModus[i];
			contents[i] = new String[] { modus.toString(), modus.toString() };
		}
		raetselGeneratorModusEditor = new ComboFieldEditor(PreferenceConstants.P_RAETSEL_GENERATOR_MODUS,
			"Rätsel generieren für", contents, getFieldEditorParent());
		addField(raetselGeneratorModusEditor);

		stufe6DauerhaftSperrenEditor = new BooleanFieldEditor(PreferenceConstants.P_STUFE_6_SPERREN,
			"Stufe 6 höchstens einmal", getFieldEditorParent());
		addField(stufe6DauerhaftSperrenEditor);

		aktuellesMinikaenguruJahrEditor = new IntegerFieldEditor(PreferenceConstants.P_AKTUELLES_JAHR_MINIKAENGURU,
			"aktuelles Minikängurujahr", getFieldEditorParent());
		addField(aktuellesMinikaenguruJahrEditor);

	}

	// /*
	// * (non-Javadoc)
	// * @see
	// org.eclipse.jface.preference.FieldEditorPreferencePage#propertyChange(org.eclipse.jface.util.PropertyChangeEvent)
	// */
	// @Override
	// public void propertyChange(PropertyChangeEvent pEvent) {
	// super.propertyChange(pEvent);
	// final String property = pEvent.getProperty();
	// final Object source = pEvent.getSource();
	// if (property.equals(FieldEditor.VALUE)) {
	// if (source == aufgabenarchivPathEditor) {
	// checkState();
	// }
	// if (source == loesungsarchivPathEditor) {
	// checkState();
	// }
	// if (source == pngsPathEditor) {
	// checkState();
	// }
	// if (source == generatedPathEditor) {
	// checkState();
	// }
	// }
	// if (property.equals(ComboFieldEditor.VALUE)){
	// if (source == raetselGeneratorModusEditor){
	// checkState();
	// MatheJungAltActivator.getDefault().updateRaetselListeGeneratorModus();
	// }
	// }
	// }
}
