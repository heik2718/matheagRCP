/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.DetailsPart;
import org.eclipse.ui.forms.IDetailsPage;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.SectionPart;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Hyperlink;
import org.eclipse.ui.forms.widgets.Section;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.IMatheAGObjectNames;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgaben.IAufgabeNames;
import de.egladil.mathejungalt.domain.quellen.AbstractQuelle;
import de.egladil.mathejungalt.domain.quellen.Buchquelle;
import de.egladil.mathejungalt.domain.quellen.IQuelleNames;
import de.egladil.mathejungalt.domain.quellen.Kindquelle;
import de.egladil.mathejungalt.domain.quellen.MitgliedergruppenQuelle;
import de.egladil.mathejungalt.domain.quellen.Zeitschriftquelle;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.editors.converters.QuellenartToStringConverter;
import de.egladil.mathejungalt.ui.app.editors.converters.StringToQuellenartConverter;
import de.egladil.mathejungalt.ui.app.editors.listeners.HyperlinkListenerAdapter;
import de.egladil.mathejungalt.ui.app.editors.pages.AbstractAttributesFormPage;
import de.egladil.mathejungalt.ui.app.handlers.SelectQuelleHandler;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;
import de.egladil.mathejungalt.ui.app.themes.LayoutFactory;
import de.egladil.mathejungalt.ui.app.themes.PartControlsFactory;

/**
 * Der QuelleMasterDetailsPart ist ein {@link ISelectionProvider}, um das Anzeigen der speziellen DetailsParts zu
 * triggern. In der üblichen Implementierung enthält der MasterBlock einen Viewer, der selbst ein ISelectionProvider
 * ist. Bei MasterBlocks, die keinen solchen Viewer enthalten, muss ein anderer {@link ISelectionProvider} zur Verf�gung
 * gestellt werden, damit sich der DetailsPart aktualisiert.
 * 
 * @author winkelv
 */
public class QuelleMasterDetailsBlock extends AbstractMasterDetailsBlock implements ISelectionProvider {

	/** */
	private Text[] texts;

	/** */
	private int widthHint = 100;

	/** */
	// private AbstractQuelle quelle;
	private Aufgabe aufgabe;

	/** */
	private List<ISelectionChangedListener> selectionChangedListeners = new ArrayList<ISelectionChangedListener>();

	@SuppressWarnings("unchecked")
	private Map<Class, IDetailsPage> detailsPageMap = new HashMap<Class, IDetailsPage>();

	/** */
	private IManagedForm managedForm;

	/** */
	private SectionPart sectionPart;

	/**
	 * @param pParentPage
	 */
	public QuelleMasterDetailsBlock(AbstractAttributesFormPage pParentPage) {
		super(pParentPage);
		detailsPageMap.put(Buchquelle.class, new BuchquelleDetailsPage(this));
		detailsPageMap.put(MitgliedergruppenQuelle.class, new GruppenquelleDetailsPage(this));
		detailsPageMap.put(Kindquelle.class, new KindquelleDetailsPage(this));
		detailsPageMap.put(Zeitschriftquelle.class, new ZeitschriftquelleDetailsPage(this));
	}

	/**
	 * @param pInput
	 */
	public void refreshDataBindings(AbstractMatheAGObject pInput) {
		if (!(pInput instanceof Aufgabe)) {
			return;
		}
		Binding[] bindings = new Binding[3];
		DataBindingContext dbc = new DataBindingContext();

		aufgabe = (Aufgabe) pInput;
		final AbstractQuelle quelle = aufgabe.getQuelle();

		IObservableValue observableWidget = SWTObservables.observeText(texts[0], SWT.Modify);
		IObservableValue observableProps = BeansObservables.observeValue(quelle, IMatheAGObjectNames.PROP_ID);
		bindings[0] = dbc.bindValue(observableWidget, observableProps, null, null);

		observableWidget = SWTObservables.observeText(texts[1], SWT.Modify);
		observableProps = BeansObservables.observeValue(quelle, IMatheAGObjectNames.PROP_SCHLUESSEL);
		bindings[1] = dbc.bindValue(observableWidget, observableProps, null, null);

		observableWidget = SWTObservables.observeText(texts[2], SWT.Modify);
		observableProps = BeansObservables.observeValue(quelle, IQuelleNames.PROP_ART);
		UpdateValueStrategy targetToModelStrategy = new UpdateValueStrategy();
		targetToModelStrategy.setConverter(new StringToQuellenartConverter());
		UpdateValueStrategy modelToTargetStrategy = new UpdateValueStrategy();
		modelToTargetStrategy.setConverter(new QuellenartToStringConverter());
		bindings[2] = dbc.bindValue(observableWidget, observableProps, targetToModelStrategy, modelToTargetStrategy);

		aufgabe.removePropertyChangeListener(this);
		aufgabe.addPropertyChangeListener(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.MasterDetailsBlock#registerPages(org.eclipse.ui.forms.DetailsPart)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected void registerPages(DetailsPart pDetailsPart) {
		pDetailsPart.setPageProvider(this);
		Iterator<Class> iter = detailsPageMap.keySet().iterator();
		while (iter.hasNext()) {
			Class clazz = iter.next();
			pDetailsPart.registerPage(clazz, detailsPageMap.get(clazz));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.IDetailsPageProvider#getPage(java.lang.Object)
	 */
	@Override
	public IDetailsPage getPage(Object pKey) {
		return detailsPageMap.get(pKey);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.IDetailsPageProvider#getPageKey(java.lang.Object)
	 */
	@Override
	public Object getPageKey(Object pObject) {
		if (!(pObject instanceof Aufgabe)) {
			return null;
		}
		AbstractQuelle quelle = ((Aufgabe) pObject).getQuelle();
		if (quelle instanceof Buchquelle) {
			return Buchquelle.class;
		}
		if (quelle instanceof Kindquelle) {
			return Kindquelle.class;
		}
		if (quelle instanceof Zeitschriftquelle) {
			return Zeitschriftquelle.class;
		}
		if (quelle instanceof MitgliedergruppenQuelle) {
			return MitgliedergruppenQuelle.class;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractMasterDetailsBlock#getSectionDescription()
	 */
	@Override
	protected String getSectionDescription() {
		return ITextConstants.SECTION_DESCR_QUELLEN_ATTRIBUTES;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractMasterDetailsBlock#getSectionHeader()
	 */
	@Override
	protected String getSectionHeader() {
		return ITextConstants.SECTION_HEADER_QUELLE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.parts.AbstractMasterDetailsBlock#hookSpecialParts(org.eclipse.ui.forms.IManagedForm,
	 * org.eclipse.ui.forms.widgets.Section, org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void hookSpecialParts(IManagedForm pManagedForm, Section pSection, Composite pComposite) {
		FormToolkit toolkit = pManagedForm.getToolkit();
		managedForm = pManagedForm;
		sectionPart = new SectionPart(pSection);
		managedForm.addPart(sectionPart);
		pComposite.setLayout(LayoutFactory.createSectionClientGridLayout(false, 2));

		texts = new Text[3];
		Control[] labels = new Control[3];

		labels[0] = PartControlsFactory.createIdLabel(pComposite, toolkit);
		texts[0] = PartControlsFactory.createIdText(pComposite, toolkit, widthHint);

		labels[1] = PartControlsFactory.createSchluesselLabel(pComposite, toolkit);
		texts[1] = PartControlsFactory.createText(pComposite, toolkit, widthHint, true);
		texts[1].setTextLimit(IQuelleNames.LENGTH_SCHLUESSEL);

		labels[2] = PartControlsFactory.createLabel(pComposite, toolkit, ITextConstants.LABEL_TEXT_ART);
		texts[2] = PartControlsFactory.createText(pComposite, toolkit, widthHint, false);

		aufgabe = (Aufgabe) getParentPage().getDomainObject();
		aufgabe.addPropertyChangeListener(this);

		// Falls die Aufgabe noch nicht gesperrt ist, soll ein Hyperlink zum Ändern der Quelle erscheinen
		if (MatheJungAltActivator.getDefault().getStammdatenservice().aufgabeEditierbar(aufgabe)) {
			Hyperlink link = toolkit.createHyperlink(pComposite, ITextConstants.LINK_TEXT_QUELLE_AENDERN, SWT.NULL);
			GridData gd = new GridData();
			gd.horizontalSpan = 2;
			link.setLayoutData(gd);

			link.addHyperlinkListener(new HyperlinkListenerAdapter() {
				@Override
				public void linkActivated(HyperlinkEvent pE) {
					AbstractQuelle quelle = handleQuellelinkActivated();
					if (quelle != null) {
						aufgabe.setQuelle(quelle);
					}
				}

				/**
				 * @return
				 */
				private AbstractQuelle handleQuellelinkActivated() {
					SelectQuelleHandler delegate = new SelectQuelleHandler(aufgabe.getQuelle(), MatheJungAltActivator
						.getDefault().getStammdatenservice());
					return delegate.execute();
				}
			});
		}
		// registriere einen SelectionChangeListener, der bewirkt dass das Anzeigen der DetailsSeite getriggert wird.
		addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent pEvent) {
				if (!(pEvent.getSource() instanceof QuelleMasterDetailsBlock)) {
					return;
				}
				managedForm.fireSelectionChanged(sectionPart, pEvent.getSelection());
			}
		});
		refreshDataBindings(aufgabe);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.IEditorPartInputProvider#getSelectedInput()
	 */
	@Override
	public AbstractMatheAGObject getSelectedInput() {
		return aufgabe;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.jface.viewers.ISelectionProvider#addSelectionChangedListener(org.eclipse.jface.viewers.
	 * ISelectionChangedListener)
	 */
	@Override
	public void addSelectionChangedListener(ISelectionChangedListener pListener) {
		if (selectionChangedListeners.contains(pListener)) {
			return;
		}
		selectionChangedListeners.add(pListener);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ISelectionProvider#getSelection()
	 */
	@Override
	public ISelection getSelection() {
		return new StructuredSelection(aufgabe);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.jface.viewers.ISelectionProvider#removeSelectionChangedListener(org.eclipse.jface.viewers.
	 * ISelectionChangedListener )
	 */
	@Override
	public void removeSelectionChangedListener(ISelectionChangedListener pListener) {
		selectionChangedListeners.remove(pListener);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ISelectionProvider#setSelection(org.eclipse.jface.viewers.ISelection)
	 */
	@Override
	public void setSelection(ISelection pSelection) {
		if (!(pSelection instanceof IStructuredSelection) || ((IStructuredSelection) pSelection).isEmpty()) {
			return;
		}
		Assert.isTrue(((IStructuredSelection) pSelection).getFirstElement() instanceof Aufgabe,
			"Nur " + Aufgabe.class.getName() + "-Objekte erlaubt!");
		aufgabe = (Aufgabe) ((IStructuredSelection) pSelection).getFirstElement();

		for (ISelectionChangedListener listener : selectionChangedListeners) {
			listener.selectionChanged(new SelectionChangedEvent(this, pSelection));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractMasterDetailsBlock#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent pEvt) {
		if (pEvt.getPropertyName().equals(IAufgabeNames.PROP_QUELLE)) {
			// setSelection(new StructuredSelection(pEvt.getNewValue()));
			managedForm.fireSelectionChanged(sectionPart, new StructuredSelection(pEvt.getSource()));
			refreshDataBindings((AbstractMatheAGObject) pEvt.getSource());
		}
	}
}
