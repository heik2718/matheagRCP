package de.egladil.mathejungalt.ui.app;

import java.util.Calendar;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.PlatformObject;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.FormColors;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.utils.OsgiServiceLocator;
import de.egladil.mathejungalt.config.IMatheJungAltConfiguration.RaetsellisteGeneratorModus;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.preferences.PreferenceConstants;
import de.egladil.mathejungalt.ui.app.themes.IImageKeys;

/**
 * The activator class controls the plug-in life cycle
 */
public class MatheJungAltActivator extends AbstractUIPlugin implements IPropertyChangeListener {

	// The plug-in ID
	public static final String PLUGIN_ID = "de.egladil.mathejungalt.ui.app";

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(MatheJungAltActivator.class);

	// The shared instance
	private static MatheJungAltActivator plugin;

	/** */
	private FormColors formColors;

	/** */
	private TableViewerContentManager tableViewerContentManager;

	private IStammdatenservice stammdatenservice;

	/**
	 * The constructor
	 */
	public MatheJungAltActivator() {
		IPreferenceStore prefStore = getPreferenceStore();
		prefStore.addPropertyChangeListener(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		tableViewerContentManager = new TableViewerContentManager();
		LOG.info("UI- Activator");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static MatheJungAltActivator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in relative path
	 * 
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		ImageDescriptor descriptor = JFaceResources.getImageRegistry().getDescriptor(path);
		if (descriptor == null) {
			descriptor = imageDescriptorFromPlugin(PLUGIN_ID, path);
			JFaceResources.getImageRegistry().put(path, descriptor);
		}
		return descriptor;
	}

	/**
	 * @param display
	 * @return
	 */
	public FormColors getFormColors(Display display) {
		if (formColors == null) {
			formColors = new FormColors(display);
			formColors.markShared();
		}
		return formColors;
	}

	/**
	 * @param pPath
	 * @return
	 */
	public Image getImage(String pPath) {
		Image image = JFaceResources.getImage(pPath);
		if (image == null) {
			getImageDescriptor(pPath);
			image = JFaceResources.getImage(pPath);
		}
		return image;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#initializeImageRegistry(org.eclipse.jface.resource.ImageRegistry)
	 */
	@Override
	protected void initializeImageRegistry(ImageRegistry pReg) {
		super.initializeImageRegistry(pReg);
		Bundle bundle = Platform.getBundle(PLUGIN_ID);
		{
			final String key = IImageKeys.AUFGABE;
			ImageDescriptor imageDescr = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path(key), null));
			pReg.put(key, imageDescr);
		}
		{
			final String key = IImageKeys.AUTOR;
			ImageDescriptor imageDescr = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path(key), null));
			pReg.put(key, imageDescr);
		}
		{
			final String key = IImageKeys.BEENDEN;
			ImageDescriptor imageDescr = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path(key), null));
			pReg.put(key, imageDescr);
		}
		{
			final String key = IImageKeys.BUCH;
			ImageDescriptor imageDescr = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path(key), null));
			pReg.put(key, imageDescr);
		}
		{
			final String key = IImageKeys.CHECK_GENERATABLE;
			ImageDescriptor imageDescr = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path(key), null));
			pReg.put(key, imageDescr);
		}
		{
			final String key = IImageKeys.DIPLOM_GEBEN;
			ImageDescriptor imageDescr = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path(key), null));
			pReg.put(key, imageDescr);
		}
		{
			final String key = IImageKeys.EDIT;
			ImageDescriptor imageDescr = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path(key), null));
			pReg.put(key, imageDescr);
		}
		{
			final String key = IImageKeys.EXIT;
			ImageDescriptor imageDescr = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path(key), null));
			pReg.put(key, imageDescr);
		}
		{
			final String key = IImageKeys.GENERATE;
			ImageDescriptor imageDescr = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path(key), null));
			pReg.put(key, imageDescr);
		}
		{
			final String key = IImageKeys.LISTITEM;
			ImageDescriptor imageDescr = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path(key), null));
			pReg.put(key, imageDescr);
		}
		{
			final String key = IImageKeys.MINIKAENGURU;
			ImageDescriptor imageDescr = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path(key), null));
			pReg.put(key, imageDescr);
		}
		{
			final String key = IImageKeys.MITGLIED;
			ImageDescriptor imageDescr = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path(key), null));
			pReg.put(key, imageDescr);
		}
		{
			final String key = IImageKeys.MITGLIED_AKTIV;
			ImageDescriptor imageDescr = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path(key), null));
			pReg.put(key, imageDescr);
		}
		{
			final String key = IImageKeys.MITGLIED_NICHT_AKTIV;
			ImageDescriptor imageDescr = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path(key), null));
			pReg.put(key, imageDescr);
		}
		{
			final String key = IImageKeys.NEW;
			ImageDescriptor imageDescr = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path(key), null));
			pReg.put(key, imageDescr);
		}
		{
			final String key = IImageKeys.QUELLE_16;
			ImageDescriptor imageDescr = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path(key), null));
			pReg.put(key, imageDescr);
		}
		{
			final String key = IImageKeys.QUELLE_WIZARD;
			ImageDescriptor imageDescr = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path(key), null));
			pReg.put(key, imageDescr);
		}
		{
			final String key = IImageKeys.REMOVE;
			ImageDescriptor imageDescr = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path(key), null));
			pReg.put(key, imageDescr);
		}
		{
			final String key = IImageKeys.SERIE;
			ImageDescriptor imageDescr = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path(key), null));
			pReg.put(key, imageDescr);
		}
		{
			final String key = IImageKeys.RAETSEL;
			ImageDescriptor imageDescr = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path(key), null));
			pReg.put(key, imageDescr);
		}
		{
			final String key = IImageKeys.SERIEN;
			ImageDescriptor imageDescr = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path(key), null));
			pReg.put(key, imageDescr);
		}
		{
			final String key = IImageKeys.SORT_ALPHABETHICALLY;
			ImageDescriptor imageDescr = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path(key), null));
			pReg.put(key, imageDescr);
		}
		{
			final String key = IImageKeys.ZEITSCHRIFT;
			ImageDescriptor imageDescr = ImageDescriptor.createFromURL(FileLocator.find(bundle, new Path(key), null));
			pReg.put(key, imageDescr);
		}
	}

	/**
	 * @return the tableViewerContentManager
	 */
	public final TableViewerContentManager getTableViewerContentManager() {
		return tableViewerContentManager;
	}

	/**
	 * @return the stammdatenservice
	 */
	public IStammdatenservice getStammdatenservice() {
		if (stammdatenservice == null) {
			stammdatenservice = new OsgiServiceLocator().getService(IStammdatenservice.class);
			Boolean stufe6 = getPreferenceStore().getBoolean(PreferenceConstants.P_STUFE_6_SPERREN);
			stammdatenservice.setStufe6MaximalEinmal(stufe6.booleanValue());
		}
		return stammdatenservice;
	}

	/**
	 * Liest aus den Preferences den Pfad mit dem gegebenen Key. Als Key können alle Konstanten in
	 * {@link PreferenceConstants} verwendet werden.
	 * 
	 * @param pKey String der Key (aus {@link PreferenceConstants})
	 * @return String ein Pfad zu einem Verzeichnis: entweder das konfigurierte oder das User-Home-Verzeichnis, wenn
	 * keins konfiguriert ist.
	 */
	public String getPath(String pKey) {
		IPreferenceStore store = getPreferenceStore();
		String value = store.getString(pKey);
		if (value == null || value.length() == 0) {
			value = store.getDefaultString(pKey);
		}
		return value;
	}

	/**
	 * @param pSrcObject
	 * @param pAdapterType
	 * @return Object vom Typ pAdapterType oder null
	 */
	@SuppressWarnings("rawtypes")
	public Object getAdapter(Object pSrcObject, Class pAdapterType) {
		Assert.isNotNull(pAdapterType);
		if (pSrcObject == null) {
			return null;
		}
		if (pAdapterType.isInstance(pSrcObject)) {
			return pSrcObject;
		}
		if (pSrcObject instanceof IAdaptable) {
			Object result = ((IAdaptable) pSrcObject).getAdapter(pAdapterType);
			Assert.isTrue(pAdapterType.isInstance(result));
			return result;
		}
		if (!(pSrcObject instanceof PlatformObject)) {
			return Platform.getAdapterManager().getAdapter(pSrcObject, pAdapterType);
		}
		return null;
	}

	/**
	 * @return the raetselListeGeneratorModus
	 */
	public final RaetsellisteGeneratorModus getRaetselListeGeneratorModus() {
		IPreferenceStore store = getPreferenceStore();
		String value = store.getString(PreferenceConstants.P_RAETSEL_GENERATOR_MODUS);
		if (value == null || value.length() == 0) {
			value = store.getDefaultString(PreferenceConstants.P_RAETSEL_GENERATOR_MODUS);
		}
		return RaetsellisteGeneratorModus.valueOf(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.util.IPropertyChangeListener#propertyChange(org.eclipse.jface.util.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent pEvent) {
		if (PreferenceConstants.P_STUFE_6_SPERREN.equals(pEvent.getProperty())) {
			Boolean neuerWert = (Boolean) pEvent.getNewValue();
			getStammdatenservice().setStufe6MaximalEinmal(neuerWert.booleanValue());
		}
	}

	/**
	 * Ermittelt, ob das gegebene Jahr das aktuelle Jahr ist.
	 * 
	 * @param jahr
	 * @return
	 */
	public boolean istAktuellesJahr(int jahr) {
		Integer aktuellesKaengurujahr = getPreferenceStore().getInt(PreferenceConstants.P_AKTUELLES_JAHR_MINIKAENGURU);
		if (aktuellesKaengurujahr == null) {
			Calendar cal = Calendar.getInstance();
			return cal.get(Calendar.YEAR) == jahr;
		}
		return aktuellesKaengurujahr.intValue() == jahr;
	}
}
