/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.contentproviders;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.PlatformUI;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.editors.AbstractMatheEditor;

/**
 * Abstrakte Oberklasse für die Viewer ContentProvieder für die Domain-Objekt-Views.
 * 
 * @author Winkelv
 */
public abstract class AbstractMatheObjectsViewContentProvider implements IStructuredContentProviderWithTableViewer,
	PropertyChangeListener {

	/** */
	private IStammdatenservice stammdatenservice;

	/** */
	private TableViewer viewer;

	/**
	 * 
	 */
	public AbstractMatheObjectsViewContentProvider(IStammdatenservice pStammdatenservice) {
		stammdatenservice = pStammdatenservice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public void inputChanged(Viewer pViewer, Object pOldInput, Object pNewInput) {
		viewer = (TableViewer) pViewer;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent pEvt) {
		if (!isInterestingChangeEvent(pEvt.getPropertyName())) {
			return;
		}
		// Verhinderung des Flackereffekts bei großen Tabellen.
		viewer.getTable().setRedraw(false);
		try {
			// Falls ein Objekt gelöscht wird, dann müssen zugehörige geöffnete Editoren geschlossen werden
		} finally {
			viewer.getTable().setRedraw(true);
			viewer.refresh();
		}
	}

	/**
	 * Schließt geöffnete Editoren zu diesem {@link AbstractMatheAGObject}, falls vorhanden.
	 * 
	 * @param pEditorId String die ID des interessanten Editors.
	 * @param pDomainObject {@link AbstractMatheAGObject} das Domain-Objekt, f�r das evtl. ein Editor ge�ffnet ist.
	 */
	protected void closeOpenEditor(final String[] pEditorIds, AbstractMatheAGObject pDomainObject) {
		Assert.isNotNull(pDomainObject);
		IEditorReference[] activeEditors = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
			.getEditorReferences();
		for (int i = 0; i < activeEditors.length; i++) {
			for (int j = 0; j < pEditorIds.length; j++) {
				if (activeEditors[i].getId().equals(pEditorIds[j])) {
					AbstractMatheEditor editor = (AbstractMatheEditor) activeEditors[i].getEditor(false);
					if (editor.getDomainObject().equals(pDomainObject)) {
						editor.setModified(false);
						editor.close(false);
					}
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.contentproviders.IStructuredContentProviderWithTableViewer#setViewer(org.eclipse.jface.viewers
	 * .TableViewer )
	 */
	public void setViewer(TableViewer pViewer) {
		viewer = pViewer;
		viewer.setContentProvider(this);
	}

	/**
	 * Standardimplementierung gibt einfach leeres StringArray zur�ck.
	 * 
	 * @return String die IDs der zugeh�rigen Editors.
	 */
	protected String[] getRelatedEditorIds() {
		return new String[] {};
	}

	/**
	 * Prüft, ob es sich um ein interessantes {@link PropertyChangeEvent} handelt.
	 * 
	 * @param pPropertyName
	 * @return boolean
	 */
	public abstract boolean isInterestingChangeEvent(String pPropertyName);

	/**
	 * @return the stammdatenservice
	 */
	protected IStammdatenservice getStammdatenservice() {
		return stammdatenservice;
	}
}
