/**
 * 
 */
package de.egladil.mathejungalt.ui.app.dialogs;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.FilteredList;
import org.eclipse.ui.dialogs.ISelectionStatusValidator;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;

/**
 * @author Winkelv
 */
public abstract class AbstractSelectMatheAGObjectWizardPage extends WizardPage {

	/** */
	private LabelProvider labelProvider;

	/** */
	private Text filterText;

	/** */
	private Label messageLabel;

	/** */
	private FilteredList filteredList;

	/** */
	private String filterString = null;

	/** */
	private int width = 60;

	/** */
	private int height = 40;

	/** */
	private String emptyListMessage = "Die Liste ist leer"; //$NON-NLS-1$

	/** */
	private String emptySelectionMessage = "Es ist kein Mitglied ausgewählt"; //$NON-NLS-1$

	/** */
	private ISelectionStatusValidator selectionValidator;

	/** */
	private List<AbstractMatheAGObject> selectedDomainObjects = new ArrayList<AbstractMatheAGObject>();

	/** */
	private Composite dialogArea;

	/** */
	private Label selectedObjectLabel;

	/** */
	private final int selectionStyle;

	/**
	 * @param pPageName
	 * @param pTitle
	 * @param pTitleImage
	 * @param pLabelProvider
	 * @param pSelectionStyle
	 */
	protected AbstractSelectMatheAGObjectWizardPage(String pPageName, String pTitle, ImageDescriptor pTitleImage,
		LabelProvider pLabelProvider, int pSelectionStyle) {
		super(pPageName, pTitle, pTitleImage);
		selectionStyle = pSelectionStyle;
		labelProvider = pLabelProvider;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createControl(Composite pParent) {
		dialogArea = createPageArea(pParent);
		createMessageArea();
		createFilterText();
		createFilteredList();
		filteredList.setElements(getListElements());
		createInputArea();
		setControl(dialogArea);
	}

	/**
	 * Fügt ggf. zusätzliche Controls in das input area ein. Die Standardimplementierung ist leer.
	 * 
	 * @param pParent {@link Composite} ein Container
	 */
	protected void hookAdditionalControls(Composite pParent) {
		// leere Standardimplementierung
	}

	/**
	 * Der Teil, in dem man die Attribute eingeben kann
	 */
	private void createInputArea() {
		Group entryGroup = new Group(dialogArea, SWT.NONE);
		entryGroup.setText("Quellenattribute");

		GridLayout entryLayout = new GridLayout(2, false);
		entryGroup.setLayout(entryLayout);
		final GridData entryGroupLayoutData = new GridData(SWT.FILL, SWT.FILL, true, true);
		entryGroupLayoutData.widthHint = convertWidthInCharsToPixels(width);
		entryGroupLayoutData.heightHint = convertHeightInCharsToPixels(height);
		entryGroup.setLayoutData(entryGroupLayoutData);

		selectedObjectLabel = new Label(entryGroup, SWT.NONE);
		if (!(selectedDomainObjects.isEmpty())) {
			selectedObjectLabel.setText(selectedDomainObjects.get(0).toString());
		}
		GridData data = new GridData();
		data.grabExcessVerticalSpace = false;
		data.grabExcessHorizontalSpace = true;
		data.horizontalAlignment = GridData.FILL;
		data.verticalAlignment = GridData.BEGINNING;
		data.horizontalSpan = 2;
		selectedObjectLabel.setLayoutData(data);

		hookAdditionalControls(entryGroup);
	}

	/**
	 * @return Object[] Array von Objekten, die durch die {@link FilteredList} dargestellt werden.
	 */
	protected abstract Object[] getListElements();

	/**
	 * Erzeugt ein Textfeld, in das ein Filtertext eingegeben werden kann, um die Liste der angezeigten Mitglieder
	 * einzuschränken
	 * 
	 * @param pDialogArea
	 */
	protected void createFilterText() {
		filterText = new Text(dialogArea, SWT.BORDER);

		GridData data = new GridData();
		data.grabExcessVerticalSpace = false;
		data.grabExcessHorizontalSpace = true;
		data.horizontalAlignment = GridData.FILL;
		data.verticalAlignment = GridData.BEGINNING;
		filterText.setLayoutData(data);
		filterText.setFont(dialogArea.getFont());

		filterText.setText((filterString == null ? "" : filterString)); //$NON-NLS-1$

		Listener listener = new Listener() {
			public void handleEvent(Event e) {
				filteredList.setFilter(filterText.getText());
			}
		};
		filterText.addListener(SWT.Modify, listener);

		filterText.addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent e) {
				if (e.keyCode == SWT.ARROW_DOWN) {
					filteredList.setFocus();
				}
			}

			public void keyReleased(KeyEvent e) {
			}
		});
	}

	/**
	 * Erzeugt einen Teil, in dem eine Message angezeigt wird.
	 * 
	 * @param pDialogArea
	 */
	protected void createMessageArea() {
		messageLabel = new Label(dialogArea, SWT.NONE);
		messageLabel.setText("Liste durch Filtern einschr\u00E4nken:");
		messageLabel.setFont(dialogArea.getFont());
		GridData data = new GridData();
		data.grabExcessVerticalSpace = false;
		data.grabExcessHorizontalSpace = true;
		data.horizontalAlignment = GridData.FILL;
		data.verticalAlignment = GridData.BEGINNING;
		messageLabel.setLayoutData(data);
	}

	/**
	 * Erzeugt den Container, der die Elemente des Dialogs aufnimmt.
	 * 
	 * @param pParent
	 * @return
	 */
	protected Composite createPageArea(Composite pParent) {
		Composite composite = new Composite(pParent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.marginHeight = convertVerticalDLUsToPixels(IDialogConstants.VERTICAL_MARGIN);
		layout.marginWidth = convertHorizontalDLUsToPixels(IDialogConstants.HORIZONTAL_MARGIN);
		layout.verticalSpacing = convertVerticalDLUsToPixels(IDialogConstants.VERTICAL_SPACING);
		layout.horizontalSpacing = convertHorizontalDLUsToPixels(IDialogConstants.HORIZONTAL_SPACING);
		composite.setLayout(layout);
		composite.setLayoutData(new GridData(GridData.FILL_BOTH));
		return composite;
	}

	/**
	 * @param pParent
	 */
	protected void createFilteredList() {
		int flags = selectionStyle | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL;

		FilteredList list = new FilteredList(dialogArea, flags, labelProvider, true, false, true);

		GridData data = new GridData();
		data.widthHint = convertWidthInCharsToPixels(width);
		data.heightHint = convertHeightInCharsToPixels(height);
		data.grabExcessVerticalSpace = true;
		data.grabExcessHorizontalSpace = true;
		data.horizontalAlignment = GridData.FILL;
		data.verticalAlignment = GridData.FILL;
		list.setLayoutData(data);
		list.setFont(dialogArea.getFont());
		list.setFilter((filterString == null ? "" : filterString)); //$NON-NLS-1$		

		list.addSelectionListener(new SelectionListener() {
			public void widgetDefaultSelected(SelectionEvent e) {
				handleDefaultSelected();
			}

			public void widgetSelected(SelectionEvent e) {
				handleDefaultSelected();
			}
		});

		filteredList = list;
	}

	/**
	 * Default selection ist normalerweise das DoubleClick-Event auf einem Item. Dann ist die WizardSeite fertig.
	 */
	protected void handleDefaultSelected() {
		if (validateCurrentSelection()) {
			Object[] selected = getSelectedElements();
			selectedDomainObjects.clear();
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < selected.length; i++) {
				if ((selected[i] instanceof AbstractMatheAGObject) && !selectedDomainObjects.contains(selected[i])) {
					selectedDomainObjects.add((AbstractMatheAGObject) selected[i]);
					sb.append(selected[i].toString());
					sb.append(";");
				}
			}
			selectedObjectLabel.setText(sb.toString());
		}
	}

	/**
	 * Validates the current selection and updates the status line accordingly.
	 * 
	 * @return boolean <code>true</code> if the current selection is valid.
	 */
	protected boolean validateCurrentSelection() {
		Assert.isNotNull(filteredList);

		IStatus status;
		Object[] elements = getSelectedElements();

		if (elements.length > 0) {
			if (selectionValidator != null) {
				status = selectionValidator.validate(elements);
			} else {
				status = new Status(IStatus.OK, PlatformUI.PLUGIN_ID, IStatus.OK, "", //$NON-NLS-1$
					null);
			}
		} else {
			if (filteredList.isEmpty()) {
				status = new Status(IStatus.ERROR, PlatformUI.PLUGIN_ID, IStatus.ERROR, emptyListMessage, null);
			} else {
				status = new Status(IStatus.ERROR, PlatformUI.PLUGIN_ID, IStatus.ERROR, emptySelectionMessage, null);
			}
		}

		return status.isOK();
	}

	/**
	 * Returns an array of the currently selected elements. To be called within or after open().
	 * 
	 * @return returns an array of the currently selected elements.
	 */
	protected Object[] getSelectedElements() {
		Assert.isNotNull(filteredList);
		return filteredList.getSelection();
	}

	/**
	 * @return the selectedDomainObject
	 */
	public AbstractMatheAGObject getFirstSelectedDomainObject() {
		return selectedDomainObjects.isEmpty() ? null : selectedDomainObjects.get(0);
	}

	/**
	 * @return the dialogArea
	 */
	protected final Composite getDialogArea() {
		return dialogArea;
	}

	/**
	 * @return the selectedDomainObjects
	 */
	public List<AbstractMatheAGObject> getSelectedDomainObjects() {
		return selectedDomainObjects;
	}
}
