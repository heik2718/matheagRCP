/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.IFormColors;
import org.eclipse.ui.forms.widgets.FormToolkit;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.service.stammdaten.event.ModelChangeEvent;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.adapters.IMasterObject;
import de.egladil.mathejungalt.ui.app.editors.listeners.IStructuredViewerPartViewerListener;
import de.egladil.mathejungalt.ui.app.themes.ButtonFactory;
import de.egladil.mathejungalt.ui.app.themes.IImageKeys;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;
import de.egladil.mathejungalt.ui.app.themes.LayoutFactory;
import de.egladil.mathejungalt.ui.app.views.comparators.AlphabethicComparator;
import de.egladil.mathejungalt.ui.app.views.comparators.IViewerComparator;
import de.egladil.mathejungalt.ui.app.views.contentproviders.AbstractMatheObjectsViewContentProvider;

/**
 * Der Teil des MasterDetailsBlocks, der die Items auflistet.
 * 
 * @author Heike Winkelvoß
 */
public abstract class AbstractViewerPartWithButtons {

	/** */
	protected static final int ADD_INDEX = 0;

	/** */
	protected static final int REMOVE_INDEX = 1;

	/** */
	protected static final int EDIT_INDEX = 2;

	/** */
	protected static final int UP_INDEX = 3;

	/** */
	protected static final int DOWN_INDEX = 4;

	/** */
	private List<Action> actions;

	/** */
	private TableViewer viewer;

	/** */
	private Point minSize;

	/** */
	private Button[] buttons;

	/** */
	private Composite buttonContainer;

	/** */
	private Label countLabel;

	/** */
	private AbstractMatheAGObject editedObject;

	/** */
	private List<IStructuredViewerPartViewerListener> structuredViewerListeners;

	/**
	 * @param pParent
	 */
	public AbstractViewerPartWithButtons(AbstractMasterDetailsBlockWithViewerPart pParent) {
		Assert.isNotNull(pParent.getParentPage().getDomainObject());
		minSize = new Point(200, 300);
		structuredViewerListeners = new ArrayList<IStructuredViewerPartViewerListener>();
		actions = new ArrayList<Action>();
		editedObject = pParent.getParentPage().getDomainObject();
		editedObject.addPropertyChangeListener(pParent);
	}

	/**
	 * Erzeugt innerhalb des Containers pParent mit Hilfe des {@link FormToolkit} pToolkit den {@link StructuredViewer},
	 * die Buttons und das Anzahl-Label.
	 * 
	 * @param pParent
	 * @param pToolkit
	 */
	public void init(final Composite pParent, final FormToolkit pToolkit) {
		// Erzeugen der Section mit Überschrift (zum auf-und zuklappen)
		// client container
		Composite client = pToolkit.createComposite(pParent, SWT.WRAP);
		GridLayout layout = LayoutFactory.getGridLayout(2);
		client.setLayout(layout);

		initViewer(client, pToolkit);

		createButtons(client, pToolkit);
		makeActions();
		updateButtonAndMenuStatus();
		updateLabel();
	}

	/**
	 * Es wird ein TableViewer erzeugt, der gleich sofort Layout-Informationen bekommt.
	 * 
	 * @param pContainer
	 * @param pToolkit
	 */
	private void initViewer(Composite pContainer, FormToolkit pToolkit) {
		viewer = createTableViewer(pContainer);
		viewer.setInput(editedObject);

		Control control = viewer.getControl();
		GridData gd = new GridData(GridData.FILL_BOTH);
		gd.horizontalSpan = 1;
		control.setLayoutData(gd);
		applyMinimumSizeToViewer();

		pToolkit.paintBordersFor(pContainer);
	}

	/**
	 * @return {@link TableViewer} der spezielle TableViewer
	 */
	protected abstract TableViewer createTableViewer(Composite pContainer);

	/**
	 * Die Standardsortierung ist alphabetisch ohne Beachtung der Gro�-Kleinschreibung. Unterklassen sollten sie
	 * �berschreiben, wenn die Items anders sortiert werden sollen.
	 * 
	 * @return ViewerComparator
	 */
	protected IViewerComparator getViewerComparator() {
		return new AlphabethicComparator();
	}

	/**
	 * Die Standardsortierung ist alphabetisch ohne Beachtung der Gro�-Kleinschreibung. Unterklassen sollten sie
	 * �berscreiben, wenn die Items anders sortiert werden sollen.
	 * 
	 * @return ViewerComparator
	 */
	protected IViewerComparator[] getViewerComparators() {
		return new IViewerComparator[] { new AlphabethicComparator() };
	}

	/**
	 * Erzeugt die allgemeinen Actions.
	 */
	protected void makeActions() {
		Action addAction = new Action() {
			public void run() {
				BusyIndicator.showWhile(((TableViewer) viewer).getTable().getDisplay(), new Runnable() {
					public void run() {
						handleAdd();
					}
				});
			}
		};
		addAction.setText("hinzuf\u00FCgen");
		addAction.setToolTipText("ein neues Objekt hinzuf\u00FCgen");
		addAction.setImageDescriptor(MatheJungAltActivator.getImageDescriptor(IImageKeys.NEW));
		actions.add(addAction);

		Action removeAction = new Action() {
			public void run() {
				BusyIndicator.showWhile(((TableViewer) viewer).getTable().getDisplay(), new Runnable() {
					public void run() {
						handleRemove();
					}
				});
			}
		};
		removeAction.setText("entfernen");
		removeAction.setToolTipText("das gew\u00E4hlte Objekt aus der Liste entfernen");
		removeAction.setImageDescriptor(MatheJungAltActivator.getImageDescriptor(IImageKeys.REMOVE));
		actions.add(removeAction);

		Action editAction = new Action() {
			public void run() {
				BusyIndicator.showWhile(((TableViewer) viewer).getTable().getDisplay(), new Runnable() {
					public void run() {
						handleEdit();
					}
				});
			}
		};
		editAction.setText("\u00E4ndern");
		editAction.setToolTipText("Editor zum \u00C4ndern \u00D6ffnen");
		editAction.setImageDescriptor(MatheJungAltActivator.getImageDescriptor(IImageKeys.EDIT));
		actions.add(editAction);
	}

	/**
	 * Ruft die add-Action der registrierten Listener auf.
	 */
	private void handleAdd() {
		for (IStructuredViewerPartViewerListener listener : structuredViewerListeners) {
			listener.handleAdd();
		}
	}

	/**
	 * Ruft die edit-Action der registrierten Listener auf.
	 */
	private void handleEdit() {
		ISelection selection = viewer.getSelection();
		if (!(selection instanceof IStructuredSelection))
			return;
		for (IStructuredViewerPartViewerListener listener : structuredViewerListeners)
			listener.handleEdit((IStructuredSelection) selection);
	}

	/**
	 * Ruft die remove-Action der registrierten Listener auf.
	 */
	private void handleRemove() {
		for (IStructuredViewerPartViewerListener listener : structuredViewerListeners)
			listener.handleRemove();
	}

	/**
	 * Hängt die Actions in den {@link IMenuManager} ein.
	 * 
	 * @param pManager {@link IMenuManager}
	 */
	protected void fillContextMenu(IMenuManager pManager) {
		for (Action action : actions) {
			pManager.add(action);
		}
	}

	/**
	 * Erzeugt Container für die Buttons und zun�chst 2 Buttons. Sollte später abstrakt werden
	 * 
	 * @param pContainer
	 * @param pToolkit
	 * @param pListener {@link BuchAutorenViewerListener}
	 */
	private void createButtons(final Composite pContainer, final FormToolkit pToolkit) {
		buttonContainer = createButtonContainer(pContainer, pToolkit);
		buttons = new Button[2];
		buttons[ADD_INDEX] = ButtonFactory.createButton(buttonContainer, pToolkit, ITextConstants.BUTTON_TEXT_ADD,
			ButtonFactory.ADD);
		buttons[REMOVE_INDEX] = ButtonFactory.createButton(buttonContainer, pToolkit,
			ITextConstants.BUTTON_TEXT_REMOVE, ButtonFactory.REMOVE);
		Composite comp = pToolkit.createComposite(buttonContainer);
		GridLayout layout = new GridLayout();
		layout.marginWidth = layout.marginHeight = 0;
		comp.setLayout(layout);
		comp.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_END | GridData.FILL_BOTH));
		countLabel = pToolkit.createLabel(comp, ""); //$NON-NLS-1$
		countLabel.setForeground(pToolkit.getColors().getColor(IFormColors.TITLE));
		countLabel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
	}

	/**
	 * Erzeugt den Container für die Buttons innerhalb des gegebenen pParen- Containers.
	 * 
	 * @param pParent
	 * @param pToolkit
	 * @return {@link Composite}
	 */
	private Composite createButtonContainer(Composite pParent, FormToolkit pToolkit) {
		Composite container;
		if (pToolkit == null)
			container = new Composite(pParent, SWT.NULL);
		else
			container = pToolkit.createComposite(pParent);
		GridLayout layout = new GridLayout();
		layout.marginWidth = layout.marginHeight = 0;
		container.setLayout(layout);
		GridData gd = new GridData(GridData.FILL_VERTICAL);
		container.setLayoutData(gd);
		return container;
	}

	/**
	 * Falls der Viewer schon da ist, wird versucht, die Gr��e zu setzen.
	 */
	private void applyMinimumSizeToViewer() {
		if (minSize != null) {
			GridData gd = (GridData) viewer.getControl().getLayoutData();
			gd.widthHint = minSize.x;
			gd.heightHint = minSize.y;
		}
	}

	/**
	 * Behandelt das Event
	 * 
	 * @param pEvt
	 */
	public void handlePropertyChangeEvent(PropertyChangeEvent pEvt) {
		AbstractMatheObjectsViewContentProvider contentProvider = (AbstractMatheObjectsViewContentProvider) viewer
			.getContentProvider();

		String propertyName = pEvt.getPropertyName();
		if (contentProvider.isInterestingChangeEvent(propertyName)) {
			viewer.refresh();
			updateLabel();
		}
	}

	/**
	 * Behandelt das Event
	 * 
	 * @param pEvt
	 */
	public void handleModelChangeEvent(ModelChangeEvent pEvt) {
		AbstractMatheObjectsViewContentProvider contentProvider = (AbstractMatheObjectsViewContentProvider) viewer
			.getContentProvider();
		if (contentProvider.isInterestingChangeEvent(pEvt.getProperty())) {
			viewer.refresh();
			updateLabel();
		}
	}

	/**
	 * ändert die Aktivierung der Buttons in Abhängigkeit von der Auswahl.
	 */
	public void updateButtonAndMenuStatus() {
		buttons[ADD_INDEX].setEnabled(true);
		actions.get(ADD_INDEX).setEnabled(true);
		if (viewer.getSelection() == null) {
			buttons[REMOVE_INDEX].setEnabled(false);
			return;
		}
		Object[] selected = ((IStructuredSelection) viewer.getSelection()).toArray();
		if (selected == null || selected.length != 1) {
			buttons[REMOVE_INDEX].setEnabled(false);
			actions.get(EDIT_INDEX).setEnabled(false);
			actions.get(REMOVE_INDEX).setEnabled(false);
		} else {
			buttons[REMOVE_INDEX].setEnabled(true);
			actions.get(EDIT_INDEX).setEnabled(true);
			actions.get(REMOVE_INDEX).setEnabled(true);
		}
	}

	/**
	 *
	 */
	private void updateLabel() {
		if (countLabel != null && !countLabel.isDisposed()) {
			IMasterObject masterObject = adapt(editedObject);
			countLabel.setText("" + masterObject.getItems().size());
		}
	}

	/**
	 * @param pObj
	 * @return
	 */
	private IMasterObject adapt(Object pObj) {
		Object result = MatheJungAltActivator.getDefault().getAdapter(pObj, IMasterObject.class);
		Assert.isNotNull(result);
		return (IMasterObject) result;
	}

	/**
	 * @return the viewer
	 */
	public StructuredViewer getViewer() {
		return viewer;
	}

	/**
	 * @param pListener
	 */
	public void addStructuredViewerListener(IStructuredViewerPartViewerListener pListener) {
		if (structuredViewerListeners.contains(pListener))
			return;
		structuredViewerListeners.add(pListener);
		viewer.addSelectionChangedListener(pListener);
		viewer.addDoubleClickListener(pListener);
		for (int i = 0; i < buttons.length; i++) {
			buttons[i].addSelectionListener(pListener);
		}
	}

	/**
	 * @param pListener
	 */
	public void removeStructuredViewerListener(IStructuredViewerPartViewerListener pListener) {
		viewer.removeSelectionChangedListener(pListener);
		viewer.removeDoubleClickListener(pListener);
		for (int i = 0; i < buttons.length; i++) {
			buttons[i].removeSelectionListener(pListener);
		}
		structuredViewerListeners.remove(pListener);
	}

	/**
	 * geordnetes Freigeben
	 */
	public void dispose() {
		for (IStructuredViewerPartViewerListener listener : structuredViewerListeners) {
			removeStructuredViewerListener(listener);
		}
	}

	/**
	 * Fügt der verwalteten Liste von Actions eine weitere hinzu
	 * 
	 * @param pAction
	 */
	protected void addAction(Action pAction) {
		if (actions.contains(pAction)) {
			return;
		}
		actions.add(pAction);
	}

	/**
	 * @return the actions
	 */
	protected List<Action> getActions() {
		return actions;
	}

	/**
	 * @return the buttons
	 */
	protected Button[] getButtons() {
		return buttons;
	}
}
