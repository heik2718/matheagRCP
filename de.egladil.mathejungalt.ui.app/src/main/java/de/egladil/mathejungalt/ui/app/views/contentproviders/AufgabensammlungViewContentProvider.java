/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.contentproviders;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.IViewSite;

import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.views.ArbeitsblaetterView;
import de.egladil.mathejungalt.ui.app.views.AutorenView;
import de.egladil.mathejungalt.ui.app.views.MinikaenguruView;
import de.egladil.mathejungalt.ui.app.views.SerienView;

/**
 * ContentProvider für den {@link AutorenView}
 * 
 * @author aheike
 */
public class AufgabensammlungViewContentProvider implements IStructuredContentProvider {

	/** */
	private IStammdatenservice stammdatenservice;

	/**
	 * 
	 */
	public AufgabensammlungViewContentProvider(IStammdatenservice pStammdatenservice) {
		super();
		stammdatenservice = pStammdatenservice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
	 */
	@Override
	public Object[] getElements(Object pArg0) {
		if (pArg0 instanceof IViewSite) {
			IViewSite site = (IViewSite) pArg0;
			if (site.getId().equals(SerienView.ID)) {
				return stammdatenservice.getSerien().toArray();
			}
			if (site.getId().equals(MinikaenguruView.ID)) {
				return stammdatenservice.getMinikaengurus().toArray();
			}
			if (site.getId().equals(ArbeitsblaetterView.ID)) {
				return stammdatenservice.getArbeitsblaetter().toArray();
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
	 */
	@Override
	public void dispose() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public void inputChanged(Viewer pArg0, Object pArg1, Object pArg2) {
	}
}
