/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.converters;

import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.domain.types.EnumTypes.Quellenart;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Konvertiert einen Quellenart-Text in eine {@link Quellenart}
 * 
 * @author Winkelv
 */
public class StringToQuellenartConverter implements IConverter {

	/**
	 * 
	 */
	public StringToQuellenartConverter() {
		//
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object pFromObject) {
		String param = (String) pFromObject;
		if (ITextConstants.QUELLENART_BUCHQUELLE.equals(param)) {
			return Quellenart.B;
		}
		if (ITextConstants.QUELLENART_KINDQUELLE.equals(param)) {
			return Quellenart.K;
		}
		if (ITextConstants.QUELLENART_ZEITSCHRIFTQUELLE.equals(param)) {
			return Quellenart.Z;
		}
		return Quellenart.N;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getFromType()
	 */
	@Override
	public Object getFromType() {
		return String.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getToType()
	 */
	@Override
	public Object getToType() {
		return Quellenart.class;
	}
}
