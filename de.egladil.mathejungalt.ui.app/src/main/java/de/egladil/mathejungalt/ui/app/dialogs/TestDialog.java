package de.egladil.mathejungalt.ui.app.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestDialog extends Dialog {

	private static final Logger LOG = LoggerFactory.getLogger(TestDialog.class);

	/**
	 * Create the dialog.
	 *
	 * @param parentShell
	 */
	public TestDialog(Shell parentShell) {
		super(parentShell);
	}

	/**
	 * Create contents of the dialog.
	 *
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);
		area.setLayout(new GridLayout(1, false));

		Composite infoComposite = new Composite(area, SWT.NONE);
		infoComposite.setLayout(new GridLayout(2, true));
		infoComposite.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		{
			Label label = new Label(infoComposite, SWT.NONE);
			label.setText("Hallo Spielername");
		}

		new Label(infoComposite, SWT.NONE);

		{
			Label label = new Label(infoComposite, SWT.NONE);
			label.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
			label.setText("Spiel:");
		}

		{
			Label label = new Label(infoComposite, SWT.NONE);
			label.setText("Name des Spiels");
		}

		{
			Label label = new Label(infoComposite, SWT.NONE);
			label.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
			label.setText("Punkte:");
		}

		{
			Label label = new Label(infoComposite, SWT.NONE);
			label.setText("55,25 von 75");
		}

		{
			Label label = new Label(infoComposite, SWT.NONE);
			label.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
			label.setText("verbrauchte Zeit:");
		}

		{
			Label label = new Label(infoComposite, SWT.NONE);
			label.setText("45 Minuten");
		}

		TableViewer viewer = new TableViewer(area, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION);
		viewer.getTable().setHeaderVisible(true);
		viewer.getTable().setLinesVisible(true);

		return area;
	}

	/**
	 * Create contents of the button bar.
	 *
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(450, 300);
	}
}
