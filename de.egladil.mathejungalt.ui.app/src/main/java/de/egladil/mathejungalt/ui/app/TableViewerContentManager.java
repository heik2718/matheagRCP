/**
 * 
 */
package de.egladil.mathejungalt.ui.app;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.egladil.mathejungalt.domain.medien.AbstractPrintmedium;
import de.egladil.mathejungalt.domain.medien.Buch;
import de.egladil.mathejungalt.domain.medien.Zeitschrift;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.domain.types.EnumTypes.Medienart;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * Dies ist ein Cache f�r Arrays von Domain-Objekten, die in WizardPages angezeigt werden. Er soll die Anzeige einer
 * Wizard-Page beschleunigen.
 * 
 * @author Winkelv
 */
public class TableViewerContentManager {

	/** */
	private Map<String, Object[]> objectsMap = new HashMap<String, Object[]>();

	/** */
	private List<String> classNames = new ArrayList<String>();

	/**
	 * 
	 */
	public TableViewerContentManager() {
		classNames.add(Mitglied.class.getName());
		classNames.add(Buch.class.getName());
		classNames.add(Zeitschrift.class.getName());
	}

	/**
	 * @param pClassName String ein eindeutiger Name, der als Schl�ssel zum speziellen Array dient.
	 * @return
	 */
	public Object[] getElements(String pClassName) {
		if (!canHandle(pClassName)) {
			return new Object[0];
		}
		Object[] elements = objectsMap.get(pClassName);
		if (elements == null) {
			initForKey(pClassName);
		}
		return objectsMap.get(pClassName);
	}

	/**
	 * @param pClassName
	 */
	private void initForKey(String pClassName) {
		final IStammdatenservice stammdatenservice = MatheJungAltActivator.getDefault().getStammdatenservice();
		if (Mitglied.class.getName().equals(pClassName)) {
			objectsMap.put(pClassName, stammdatenservice.getMitglieder().toArray());
		}
		if (Buch.class.getName().equals(pClassName)) {
			Collection<AbstractPrintmedium> alle = stammdatenservice.getMedien();
			List<Buch> buecher = new ArrayList<Buch>();
			for (AbstractPrintmedium medium : alle) {
				if (Medienart.B.equals(medium.getArt())) {
					buecher.add((Buch) medium);
				}
			}
			objectsMap.put(pClassName, buecher.toArray());
		}
		if (Zeitschrift.class.getName().equals(pClassName)) {
			Collection<AbstractPrintmedium> alle = stammdatenservice.getMedien();
			List<Zeitschrift> zeitschriften = new ArrayList<Zeitschrift>();
			for (AbstractPrintmedium medium : alle) {
				if (Medienart.Z.equals(medium.getArt())) {
					zeitschriften.add((Zeitschrift) medium);
				}
			}
			objectsMap.put(pClassName, zeitschriften.toArray());
		}
	}

	/**
	 * @param pClassName
	 * @return
	 */
	public boolean canHandle(String pClassName) {
		return classNames.contains(pClassName);
	}
}
