/**
 * 
 */
package de.egladil.mathejungalt.ui.app.handlers;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import de.egladil.base.exceptions.MatheAGObjectNotGeneratableException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * Pr�ft Mitgliederseite auf Generierbarkeit.
 * 
 * @author aheike
 */
public class CheckAufgabensammlungGeneratableHandler extends AbstractSelectionHandler {

	/** */
	private IStammdatenservice stammdatenservice;

	/**
	 * @param pSelectionProvider
	 */
	public CheckAufgabensammlungGeneratableHandler(ISelectionProvider pSelectionProvider,
		IStammdatenservice pStammdatenservice) {
		super(pSelectionProvider);
		stammdatenservice = pStammdatenservice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.handlers.AbstractSelectionHandler#run()
	 */
	@Override
	protected void run() {
		AbstractMatheAGObject object = getFirstSelectedObject();
		if (!(object instanceof IAufgabensammlung)) {
			return;
		}
		final Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		try {
			stammdatenservice.canGenerate((AbstractMatheAGObject) object);
			MessageDialog.openInformation(shell, "Info", "Die Seiten k\u00F6nnen generiert werden.");
		} catch (MatheAGObjectNotGeneratableException e) {
			MessageDialog.openInformation(shell, "Info", e.getMessage());
		} catch (Exception e) {
			MessageDialog.openError(shell, "Fehler", "Unerwarteter Fehler\n" + e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.handlers.AbstractSelectionHandler#isEnabled()
	 */
	@Override
	public boolean isEnabled() {
		if (!super.isEnabled()) {
			return false;
		}
		return getFirstSelectedObject() instanceof IAufgabensammlung;
	}

}
