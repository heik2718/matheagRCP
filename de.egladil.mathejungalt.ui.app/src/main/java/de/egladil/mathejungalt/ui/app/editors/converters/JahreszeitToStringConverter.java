/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.converters;

import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.domain.types.EnumTypes.Jahreszeit;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Konvertiert einen {@link Jahreszeit} in einen String.
 * 
 * @author Winkelv
 */
public class JahreszeitToStringConverter implements IConverter {

	/**
	 * 
	 */
	public JahreszeitToStringConverter() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object pFromObject) {
		Jahreszeit param = (Jahreszeit) pFromObject;
		switch (param) {
		case F:
			return ITextConstants.JAHRESZEIT_FRUEH;
		case H:
			return ITextConstants.JAHRESZEIT_HERBST;
		case I:
			return ITextConstants.JAHRESZEIT_IMMER;
		case K:
			return ITextConstants.JAHRESZEIT_KARNEVAL;
		case O:
			return ITextConstants.JAHRESZEIT_OSTERN;
		case S:
			return ITextConstants.JAHRESZEIT_SOMMERR;
		case W:
			return ITextConstants.JAHRESZEIT_WINTER;
		default:
			return ITextConstants.JAHRESZEIT_XMAS;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getFromType()
	 */
	@Override
	public Object getFromType() {
		return Jahreszeit.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getToType()
	 */
	@Override
	public Object getToType() {
		return String.class;
	}
}
