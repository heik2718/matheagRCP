/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.kopierer;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;

/**
 * @author winkelv
 */
public class MCRaetselKopierer implements IKopierer {

	/**
	 *
	 */
	public MCRaetselKopierer() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.ui.app.editors.kopierer.IKopierer#copyAttributes(de.egladil.mathejungalt.domain.
	 * AbstractMatheAGObject, de.egladil.mathejungalt.domain.AbstractMatheAGObject)
	 */
	@Override
	public void copyAttributes(AbstractMatheAGObject pSource, AbstractMatheAGObject pTarget)
		throws MatheJungAltException {
		if (!(pSource instanceof MCArchivraetsel) && !(pTarget instanceof MCArchivraetsel)) {
			throw new MatheJungAltException(
				"Quelle oder Ziel sind keine MCArchivraetsel: Quelle = " + pSource == null ? "null" : pSource
					.getClass().getName() + ", Ziel = " + pTarget == null ? "null" : pTarget.getClass().getName());
		}
		MCArchivraetsel source = (MCArchivraetsel) pSource;
		MCArchivraetsel target = (MCArchivraetsel) pTarget;
		target.setAutor(source.getAutor());
		target.setLicenseFull(source.getLicenseFull());
		target.setLicenseShort(source.getLicenseShort());
		target.setSchluessel(source.getSchluessel());
		target.setStufe(source.getStufe());
		target.setTitel(source.getTitel());
		target.setVeroeffentlicht(source.isVeroeffentlicht());
		target.clearItems();
		for (MCArchivraetselItem item : source.getItems()) {
			MCArchivraetselItem targetItem = new MCArchivraetselItem();
			copyItemAttributes((MCArchivraetselItem) item, targetItem);
			target.addItem(targetItem);
			targetItem.setRaetsel(target);
		}
	}

	/**
	 * @param pSource
	 * @param pTarget
	 */
	private void copyItemAttributes(MCArchivraetselItem pSource, MCArchivraetselItem pTarget) {
		pTarget.setAufgabe(pSource.getAufgabe());
		pTarget.setNummer(pSource.getNummer());
	}
}
