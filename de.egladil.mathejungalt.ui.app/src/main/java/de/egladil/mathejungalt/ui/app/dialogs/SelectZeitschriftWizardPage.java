/**
 * 
 */
package de.egladil.mathejungalt.ui.app.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import de.egladil.mathejungalt.domain.medien.Zeitschrift;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.dialogs.labelproviders.ZeitschriftListLabelProvider;
import de.egladil.mathejungalt.ui.app.themes.IImageKeys;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * @author Winkelv
 */
public class SelectZeitschriftWizardPage extends AbstractSelectMatheAGObjectWizardPage {

	/** */
	private Text ausgabeText;

	/** */
	private Integer ausgabe;

	/** */
	private Text jahrgangText;

	/** */
	private Integer jahrgang;

	/**
	 * @param pPageName
	 */
	public SelectZeitschriftWizardPage() {
		super(ITextConstants.QUELLE_SELECT_ZEITSCHRIFT_PAGE_NAME, ITextConstants.QUELLE_SELECT_ZEITSCHRIFT_PAGE_TITLE,
			MatheJungAltActivator.imageDescriptorFromPlugin(MatheJungAltActivator.PLUGIN_ID, IImageKeys.QUELLE_WIZARD),
			new ZeitschriftListLabelProvider(), SWT.SINGLE);
		setDescription(ITextConstants.QUELLE_SELECT_ZEITSCHRIFT_PAGE_DESCR);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.dialogs.AbstractSelectMatheAGObjectWizardPage#hookAdditionalControls(org.eclipse.swt.widgets
	 * .Composite)
	 */
	protected void hookAdditionalControls(Composite pParent) {
		Composite container = new Composite(pParent, SWT.NULL);
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 2;
		container.setLayout(gridLayout);
		setControl(container);

		final Label ausgabeLabel = new Label(container, SWT.NONE);
		final GridData gd2 = new GridData();
		gd2.horizontalSpan = 1;
		ausgabeLabel.setLayoutData(gd2);
		ausgabeLabel.setText(ITextConstants.LABEL_TEXT_AUSGABE);

		ausgabeText = new Text(container, SWT.BORDER);
		ausgabeText.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		ausgabeText.setFont(pParent.getFont());
		ausgabeText.setToolTipText(ITextConstants.QUELLE_SELECT_AUSGABE_TOOLTIP);
		GridData data = new GridData();
		data.grabExcessVerticalSpace = false;
		data.grabExcessHorizontalSpace = true;
		data.horizontalAlignment = GridData.FILL;
		data.verticalAlignment = GridData.BEGINNING;
		ausgabeText.setLayoutData(data);
		ModifyListener listener = new ModifyListener() {

			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.ModifyListener#modifyText(org.eclipse.swt.events.ModifyEvent)
			 */
			@Override
			public void modifyText(ModifyEvent pE) {
				validateInput();
			}

		};

		final Label jahrgangLabel = new Label(container, SWT.NONE);
		jahrgangLabel.setLayoutData(gd2);
		jahrgangLabel.setText(ITextConstants.LABEL_TEXT_JAHRGANG);

		jahrgangText = new Text(container, SWT.BORDER);
		jahrgangText.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		jahrgangText.setFont(pParent.getFont());
		jahrgangText.setToolTipText(ITextConstants.QUELLE_SELECT_JAHRGANG_TOOLTIP);
		jahrgangText.setLayoutData(data);

		ausgabeText.addModifyListener(listener);
		jahrgangText.addModifyListener(listener);
		ausgabeText.setText("");
		jahrgangText.setText("");
	}

	/**
	 * 
	 */
	private void validateInput() {
		try {
			ausgabe = Integer.parseInt(ausgabeText.getText());
		} catch (NumberFormatException e) {
			ausgabe = null;
		}
		try {
			jahrgang = Integer.parseInt(jahrgangText.getText());
		} catch (NumberFormatException e) {
			jahrgang = null;
		}
		if (ausgabe != null && jahrgang != null) {
			setPageComplete(true);
		} else {
			setPageComplete(false);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.dialogs.AbstractSelectMatheAGObjectWizardPage#getListElements()
	 */
	@Override
	protected Object[] getListElements() {
		return MatheJungAltActivator.getDefault().getTableViewerContentManager()
			.getElements(Zeitschrift.class.getName());
	}

	/**
	 * @return the ausgabe
	 */
	public final Integer getAusgabe() {
		return ausgabe;
	}

	/**
	 * @return the jahrgang
	 */
	public final Integer getJahrgang() {
		return jahrgang;
	}
}
