/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors;

import java.beans.PropertyChangeEvent;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPathEditorInput;
import org.eclipse.ui.PartInitException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.exceptions.MatheJungAltInconsistentEntityException;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgaben.IAufgabeNames;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.AufgabeEditorInput;
import de.egladil.mathejungalt.ui.app.editors.pages.AufgabeAttributesFormPage;
import de.egladil.mathejungalt.ui.app.editors.pages.AufgabePreviewFormPage;
import de.egladil.mathejungalt.ui.app.handlers.OpenExternalTextFileHandlerDelegate;

/**
 * @author Winkelv
 */
public class AufgabeEditor extends AbstractMatheEditor {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(AufgabeEditor.class);

	/** */
	public static final String ID = "de.egladil.mathejungalt.ui.app.editors.AufgabeEditor";

	/**
	 *
	 */
	public AufgabeEditor() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.AbstractMatheEditor#checkEditorInput(org.eclipse.ui.IEditorInput)
	 */
	@Override
	protected void checkEditorInput(IEditorInput pInput) throws PartInitException {
		if (!(pInput instanceof AufgabeEditorInput)) {
			throw new PartInitException("Ungültiger Input: muss AufgabeEditorInput sein!");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.AbstractMatheEditor#interestingPropertyChangeEvent(java.beans.PropertyChangeEvent)
	 */
	@Override
	protected boolean interestingPropertyChangeEvent(PropertyChangeEvent pEvt) {
		String property = pEvt.getPropertyName();
		Assert.isNotNull(property);
		if (IAufgabeNames.PROP_SCHLUESSEL.equals(property)) {
			return true;
		}
		if (IAufgabeNames.PROP_ART.equals(property)) {
			return true;
		}
		if (IAufgabeNames.PROP_BESCHREIBUNG.equals(property)) {
			return true;
		}
		if (IAufgabeNames.PROP_HEFT.equals(property)) {
			return true;
		}
		if (IAufgabeNames.PROP_JAHRESZEIT.equals(property)) {
			return true;
		}
		if (IAufgabeNames.PROP_QUELLE.equals(property)) {
			return true;
		}
		if (IAufgabeNames.PROP_STUFE.equals(property)) {
			return true;
		}
		if (IAufgabeNames.PROP_THEMA.equals(property)) {
			return true;
		}
		if (IAufgabeNames.PROP_TITEL.equals(property)) {
			return true;
		}
		if (IAufgabeNames.PROP_ZWECK.equals(property)) {
			return true;
		}
		if (IAufgabeNames.PROP_ID.equals(property)) {
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.AbstractMatheEditor#isTitleChangingEvent(java.beans.PropertyChangeEvent)
	 */
	@Override
	protected boolean isTitleChangingEvent(PropertyChangeEvent pEvt) {
		String property = pEvt.getPropertyName();
		Assert.isNotNull(property);
		if (IAufgabeNames.PROP_SCHLUESSEL.equals(property)) {
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.AbstractMatheEditor#updateTitel()
	 */
	@Override
	protected void updateTitel() {
		Aufgabe aufgabe = (Aufgabe) getDomainObject();
		if (aufgabe != null) {
			setPartName(aufgabe.getSchluessel());
		} else {
			setPartName("neue Aufgabe");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.FormEditor#addPages()
	 */
	@Override
	protected void addPages() {
		try {
			final AufgabeAttributesFormPage attributesFormPage = new AufgabeAttributesFormPage(this, getClass()
				.getName(), "Aufgabe");
			int index = addPage(attributesFormPage);
			setPageText(index, "Aufgabeninfos");
			IPathEditorInput[] inputs = new OpenExternalTextFileHandlerDelegate()
				.createEditorInput((Aufgabe) getDomainObject());
			final AufgabeLaTeXEditor aufgabeLaTeXPage = new AufgabeLaTeXEditor(this, AufgabeLaTeXEditor.ID_AUFGABEN,
				inputs[0]);
			index = addPage(aufgabeLaTeXPage);
			setPageText(index, "LaTeX Aufgabe");
			final AufgabeLaTeXEditor loesungLaTeXPage = new AufgabeLaTeXEditor(this, AufgabeLaTeXEditor.ID_LOESUNGEN,
				inputs[1]);
			index = addPage(loesungLaTeXPage);
			setPageText(index, "LaTeX L\u00F6sung");
			final AufgabePreviewFormPage aufgabePreview = new AufgabePreviewFormPage(this, true);
			index = addPage(aufgabePreview);
			setPageText(index, "png Aufgabe");
			final AufgabePreviewFormPage loesungPreview = new AufgabePreviewFormPage(this, false);
			index = addPage(loesungPreview);
			setPageText(index, "png L\u00F6sung");
		} catch (PartInitException e) {
			String msg = "Fehler beim Hinzufügen von Seiten zum " + getClass().getName();
			LOG.error(msg);
			throw new MatheJungAltException(msg, e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public void doSave(IProgressMonitor pMonitor) {
		commitPages(true);
		Aufgabe aufgabe = (Aufgabe) getDomainObject();
		try {
			getStammdatenservice().aufgabeSpeichern(aufgabe);
			((AufgabeEditorInput) getEditorInput()).applyChanges();
			setModified(false);
		} catch (MatheJungAltInconsistentEntityException e) {
			setModified(true);
			MessageDialog.openWarning(Display.getCurrent().getActiveShell(), "Warnung", e.getMessage());
		} catch (Exception e) {
			setModified(true);
			String msg = "Die Aufgabe konnte wegen eines MatheAG-Core-Fehlers nicht gespeichert werden:\n"
				+ e.getMessage();
			LOG.error(msg, e);
			MessageDialog.openError(Display.getCurrent().getActiveShell(), "Fehler!", msg);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.AbstractMatheEditor#getId()
	 */
	@Override
	public String getId() {
		return ID;
	}
}
