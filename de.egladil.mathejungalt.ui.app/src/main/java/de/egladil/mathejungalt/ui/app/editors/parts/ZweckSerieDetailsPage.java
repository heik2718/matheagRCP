/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

/**
 * @author aheike
 */
public class ZweckSerieDetailsPage extends AbstractDetailsPage {

	/**
	 * @param pMaster
	 */
	public ZweckSerieDetailsPage(AbstractMasterDetailsBlock pMaster) {
		super(pMaster);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.parts.AbstractDetailsPage#getAttributesPart()
	 */
	@Override
	protected MatheAGObjectAttributesPart getAttributesPart() {
		SerienstufeSimpleAttributePartDataBindingProvider contentsProvider = new SerienstufeSimpleAttributePartDataBindingProvider();
		return new MatheAGObjectAttributesPart(false, contentsProvider);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.parts.AbstractDetailsPage#getHeader()
	 */
	@Override
	protected String getHeader() {
		return "Stufe";
	}
}
