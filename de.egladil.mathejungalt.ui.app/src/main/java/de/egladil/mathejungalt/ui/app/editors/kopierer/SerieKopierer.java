/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.kopierer;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serienitem;

/**
 * @author winkelv
 */
public class SerieKopierer implements IKopierer {

	/**
	 * 
	 */
	public SerieKopierer() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.ui.app.editors.kopierer.IKopierer#copyAttributes(de.egladil.mathejungalt.domain.
	 * AbstractMatheAGObject, de.egladil.mathejungalt.domain.AbstractMatheAGObject)
	 */
	@Override
	public void copyAttributes(AbstractMatheAGObject pSource, AbstractMatheAGObject pTarget)
		throws MatheJungAltException {
		if (!(pSource instanceof Serie) && !(pTarget instanceof Serie)) {
			throw new MatheJungAltException("Quelle oder Ziel sind keine Serien: Quelle = " + pSource == null ? "null"
				: pSource.getClass().getName() + ", Ziel = " + pTarget == null ? "null" : pTarget.getClass().getName());
		}
		Serie source = (Serie) pSource;
		Serie target = (Serie) pTarget;
		target.setBeendet(source.getBeendet());
		target.setDatum(source.getDatum());
		target.setMonatJahrText(source.getMonatJahrText());
		target.setRunde(source.getRunde());
		target.setNummer(source.getNummer());
		target.clearSerienitems();
		for (IAufgabensammlungItem item : source.getItems()) {
			Serienitem targetItem = new Serienitem();
			copyItemAttributes((Serienitem) item, targetItem);
			target.addItem(targetItem);
			targetItem.setSerie(target);
		}
	}

	/**
	 * @param pSource
	 * @param pTarget
	 */
	private void copyItemAttributes(Serienitem pSource, Serienitem pTarget) {
		pTarget.setAufgabe(pSource.getAufgabe());
		pTarget.setNummer(pSource.getNummer());
	}
}
