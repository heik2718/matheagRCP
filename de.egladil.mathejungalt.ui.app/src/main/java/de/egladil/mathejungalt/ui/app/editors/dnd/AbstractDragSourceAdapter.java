/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.dnd;

import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.DragSourceListener;

import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.domain.medien.Autor;
import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.domain.schulen.Schule;

/**
 * @author Winkelv
 */
public abstract class AbstractDragSourceAdapter implements DragSourceListener {

	/**
	 *
	 */
	public AbstractDragSourceAdapter() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.dnd.DragSourceListener#dragFinished(org.eclipse.swt.dnd.DragSourceEvent)
	 */
	@Override
	public void dragFinished(DragSourceEvent pEvent) {
		// nix
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.dnd.DragSourceListener#dragSetData(org.eclipse.swt.dnd.DragSourceEvent)
	 */
	@Override
	public void dragSetData(DragSourceEvent pEvent) {
		if (!shallDo(pEvent)) {
			return;
		}
		LocalSelectionTransfer.getTransfer().setSelection(getSelection());
		pEvent.data = LocalSelectionTransfer.getTransfer();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.dnd.DragSourceListener#dragStart(org.eclipse.swt.dnd.DragSourceEvent)
	 */
	@Override
	public void dragStart(DragSourceEvent pEvent) {
		// Only start the drag if there is actually selected an item
		pEvent.doit = false;
		final ISelection selection = getSelection();
		if (selection instanceof IStructuredSelection && !selection.isEmpty()) {
			Object selectedObject = ((IStructuredSelection) selection).getFirstElement();
			if (selectedObject instanceof Aufgabe || selectedObject instanceof Autor
				|| selectedObject instanceof MCAufgabe || selectedObject instanceof Schule
				|| selectedObject instanceof Kontakt) {
				pEvent.doit = true;
			}
		}
	}

	/**
	 * @param pEvent
	 * @return
	 */
	private boolean shallDo(DragSourceEvent pEvent) {
		boolean supported = LocalSelectionTransfer.getTransfer().isSupportedType(pEvent.dataType);
		return supported;
	}

	/**
	 * @return {@link ISelection} enthält das DomainObjekt, das mittels drag und drop kopiert oder einem
	 * Container-Objekt hinzugefügt werden soll.
	 */
	protected abstract ISelection getSelection();

}
