/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.contentproviders;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

import de.egladil.mathejungalt.domain.mitglieder.IMitgliedNames;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * @author winkelv
 */
public class MitgliederViewContentProvider extends AbstractMatheObjectsViewContentProvider implements
	IStructuredContentProvider {

	/**
	 * @param pStammdatenservice
	 */
	public MitgliederViewContentProvider(IStammdatenservice pStammdatenservice) {
		super(pStammdatenservice);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
	 */
	@Override
	public Object[] getElements(Object pArg0) {
		return getStammdatenservice().getMitglieder().toArray();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
	 */
	@Override
	public void dispose() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.ui.app.views.contentproviders.AbstractMatheObjectsViewContentProvider#
	 * isInterestingChangeEvent(java.beans.PropertyChangeEvent)
	 */
	@Override
	public boolean isInterestingChangeEvent(String pPropertyName) {
		return IMitgliedNames.PROP_ALTER.equals(pPropertyName) || IMitgliedNames.PROP_KLASSE.equals(pPropertyName)
			|| IMitgliedNames.PROP_KONTAKT.equals(pPropertyName) || IMitgliedNames.PROP_NACHNAME.equals(pPropertyName)
			|| IMitgliedNames.PROP_VORNAME.equals(pPropertyName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public void inputChanged(Viewer pArg0, Object pArg1, Object pArg2) {

	}

}
