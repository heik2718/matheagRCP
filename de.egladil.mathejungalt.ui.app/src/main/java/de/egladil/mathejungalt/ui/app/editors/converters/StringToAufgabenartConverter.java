/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.converters;

import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenart;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Konvertiert einen Aufgabenart-Text in eine {@link Aufgabenart}
 * 
 * @author Winkelv
 */
public class StringToAufgabenartConverter implements IConverter {

	/**
	 * 
	 */
	public StringToAufgabenartConverter() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object pFromObject) {
		String param = (String) pFromObject;
		if (ITextConstants.AUFGABENART_EIGEN.equals(param)) {
			return Aufgabenart.E;
		}
		if (ITextConstants.AUFGABENART_NACH.equals(param)) {
			return Aufgabenart.N;
		}
		return Aufgabenart.Z;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getFromType()
	 */
	@Override
	public Object getFromType() {
		return String.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getToType()
	 */
	@Override
	public Object getToType() {
		return Aufgabenart.class;
	}
}
