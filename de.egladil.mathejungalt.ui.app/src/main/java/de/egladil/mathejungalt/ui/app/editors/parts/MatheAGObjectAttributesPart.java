/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.forms.widgets.FormToolkit;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.medien.Autor;

/**
 * Teil einer FormPage zum editieren von einfachen Attributen.
 * 
 * @author Winkelv
 */
public class MatheAGObjectAttributesPart {

	/** */
	private boolean editable;

	/** */
	private ISimpleAttributePartDataBindingProvider attributePartControlsProvider;

	/**
	 * @param pEditable boolean editierbar (true/false). Falls false, dann sind die Textfelder nicht editierbar
	 * @param pControlsProvider {@link ISimpleAttributePartDataBindingProvider} der Teil für die einfachen Attribute.
	 */
	public MatheAGObjectAttributesPart(boolean pEditable, ISimpleAttributePartDataBindingProvider pControlsProvider) {
		super();
		editable = pEditable;
		attributePartControlsProvider = pControlsProvider;
	}

	/**
	 * Initialisiert die Labels und Texteingabefelder.
	 * 
	 * @param pClient {@link Composite} der Container, in dem die Eingabefelder sitzen
	 * @param pFormToolkit {@link FormToolkit} das zum Zeichnen dient.
	 * @param pInput {@link AbstractMatheAGObject}
	 */
	public void initialize(Composite pClient, FormToolkit pFormToolkit, AbstractMatheAGObject pInput) {
		attributePartControlsProvider.init(pClient, pFormToolkit, editable, pInput);
	}

	/**
	 * @param pInput ein {@link Autor}
	 */
	public void refreshDataBindings(final AbstractMatheAGObject pInput) {
		attributePartControlsProvider.refreshDataBindings(pInput);
	}

	/**
	 * Gibt das DataBinding frei.
	 */
	public void dispose(AbstractMatheAGObject pInput) {
		clearDataBindings();
		attributePartControlsProvider.dispose(pInput);
	}

	/**
	 * @return the editable
	 */
	protected boolean isEditable() {
		return editable;
	}

	/**
	 * Default-Implementierung zum Setzen des Fokus. Standardmäßig wird der Fokus auf das dritte Eingabefeld gesetzt,
	 * sofern vorhanden, falls nicht aufs zweite.
	 */
	public void setFocus() {
		Control[] controls = attributePartControlsProvider.getControls();
		if (controls.length > 2 && controls[2] != null)
			controls[2].setFocus();
		else
			controls[1].setFocus();
	}

	/**
	 *
	 */
	public void clearDataBindings() {
		DataBindingContext dbc = attributePartControlsProvider.getDbc();
		Binding[] bindings = attributePartControlsProvider.getBindings();
		if (dbc == null || bindings == null)
			return;
		for (int i = 0; i < bindings.length; i++) {
			if (bindings[i] != null) {
				dbc.removeBinding(bindings[i]);
				bindings[i].dispose();
			}
		}
		dbc.dispose();
	}
}
