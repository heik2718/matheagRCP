/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.converters;

import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.domain.mcraetsel.MCAntwortbuchstaben;

/**
 * Konvertiert ein {@link MCAntwortbuchstaben} in einen String.
 *
 * @author Winkelv
 */
public class MCAntwortbuchstabenToStringConverter implements IConverter {

	/**
	 *
	 */
	public MCAntwortbuchstabenToStringConverter() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object pFromObject) {
		MCAntwortbuchstaben param = (MCAntwortbuchstaben) pFromObject;
		return param == null ? "" : param.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getFromType()
	 */
	@Override
	public Object getFromType() {
		return MCAntwortbuchstaben.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getToType()
	 */
	@Override
	public Object getToType() {
		return String.class;
	}
}
