/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.editorinputs;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.ui.app.editors.kopierer.IKopierer;
import de.egladil.mathejungalt.ui.app.editors.kopierer.MCRaetselKopierer;

/**
 * @author winkelv
 */
public class MCRaetselEditorInput extends AbstractMatheEditorInput {

	/**
	 * @param pObject
	 */
	public MCRaetselEditorInput(AbstractMatheAGObject pObject) {
		super(pObject);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.editorinputs.AbstractMatheEditorInput#getKopierer()
	 */
	@Override
	public IKopierer getKopierer() {
		return new MCRaetselKopierer();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Object getAdapter(Class pArg0) {
		final String name = pArg0.getName();
		if (name.equals(MCArchivraetsel.class.getName())) {
			return getDomainObject();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.editorinputs.AbstractMatheEditorInput#initCache()
	 */
	@Override
	public void initCache() {
		setCache(new MCArchivraetsel());
		applyChanges();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.editorinputs.AbstractMatheEditorInput#getInputObjectType()
	 */
	@Override
	protected String getInputObjectType() {
		return MCArchivraetsel.class.getName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object pObj) {
		if (pObj == null)
			return false;
		if (!(pObj instanceof MCRaetselEditorInput)) {
			return false;
		}
		MCRaetselEditorInput param = (MCRaetselEditorInput) pObj;
		final Object adapter1 = getAdapter(MCArchivraetsel.class);
		final Object adapter2 = param.getAdapter(MCArchivraetsel.class);
		return adapter1.equals(adapter2);
	}

}
