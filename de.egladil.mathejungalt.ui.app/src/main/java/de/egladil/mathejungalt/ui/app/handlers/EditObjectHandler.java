/**
 *
 */
package de.egladil.mathejungalt.ui.app.handlers;

import org.eclipse.jface.viewers.ISelectionProvider;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.AbstractMatheEditorInput;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.AufgabeEditorInput;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.MCAufgabeEditorInput;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.MitgliedEditorInput;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.SerieEditorInput;

/**
 * Öffnet den passenden Editor.
 *
 * @author aheike
 */
public class EditObjectHandler extends AbstractSelectionHandler {

	/** */
	private final EditObjectHandlerDelegate delegate;

	/**
	 * @param pSelectionProvider
	 */
	public EditObjectHandler(ISelectionProvider pSelectionProvider, EditObjectHandlerDelegate pDelegate) {
		super(pSelectionProvider);
		delegate = pDelegate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.handlers.AbstractSelectionHandler#run()
	 */
	@Override
	protected void run() {
		AbstractMatheAGObject object = getFirstSelectedObject();
		AbstractMatheEditorInput editorInput = null;
		if (object instanceof Aufgabe) {
			editorInput = new AufgabeEditorInput(object);
		}
		if (object instanceof MCAufgabe) {
			editorInput = new MCAufgabeEditorInput(object);
		}
		if (object instanceof Mitglied) {
			editorInput = new MitgliedEditorInput(object);
		}
		if (object instanceof Serie) {
			editorInput = new SerieEditorInput(object);
		}
		if (editorInput != null) {
			delegate.executeCommand(editorInput);
		}
	}
}
