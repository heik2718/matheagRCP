package de.egladil.mathejungalt.ui.app;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

import de.egladil.mathejungalt.ui.app.views.MCAufgabenView;
import de.egladil.mathejungalt.ui.app.views.MCRaetselView;

/**
 * @author aheike
 */
public class QuizarchivPerspective implements IPerspectiveFactory {

	public static final String ID = "de.egladil.mathejungalt.ui.app.quizarchivPerspective";

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IPerspectiveFactory#createInitialLayout(org.eclipse.ui.IPageLayout)
	 */
	public void createInitialLayout(IPageLayout layout) {
		String editorArea = layout.getEditorArea();
		layout.setEditorAreaVisible(true);
		layout.setFixed(false);
		layout.setEditorAreaVisible(true);
		IFolderLayout folder = layout.createFolder("quizarchivViewsFolder", IPageLayout.LEFT, 0.3f, editorArea);
		folder.addView(MCAufgabenView.ID);
		folder.addView(MCRaetselView.ID);
	}
}
