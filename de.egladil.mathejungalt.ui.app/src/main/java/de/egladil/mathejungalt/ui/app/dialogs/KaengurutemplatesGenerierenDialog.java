package de.egladil.mathejungalt.ui.app.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mathejungalt.domain.types.MCAufgabenstufen;

public class KaengurutemplatesGenerierenDialog extends Dialog {

	private static final Logger LOG = LoggerFactory.getLogger(KaengurutemplatesGenerierenDialog.class);

	private String schluessel;

	private String anzahl;

	private Text schluesselText;

	private Text anzahlText;

	private ComboViewer stufeViewer;

	private MCAufgabenstufen stufe;

	/**
	 * Create the dialog.
	 *
	 * @param parentShell
	 */
	public KaengurutemplatesGenerierenDialog(Shell parentShell) {
		super(parentShell);
	}

	/**
	 * Create contents of the dialog.
	 *
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout(2, false));

		Label lblStufe = new Label(container, SWT.NONE);
		lblStufe.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblStufe.setText("Stufe:");

		stufeViewer = new ComboViewer(container, SWT.BORDER);
		Combo combo = stufeViewer.getCombo();
		combo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		stufeViewer.setContentProvider(new ArrayContentProvider());
		stufeViewer.setLabelProvider(new LabelProvider() {

			@Override
			public String getText(Object pElement) {
				if (pElement instanceof MCAufgabenstufen) {
					MCAufgabenstufen stufe = (MCAufgabenstufen) pElement;
					return stufe.getLabel();
				}
				return super.getText(pElement);
			}

		});
		stufeViewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent pArg0) {
				ISelection sel = pArg0.getSelection();
				if (sel == null || sel.isEmpty()) {
					stufe = null;
				} else {
					stufe = (MCAufgabenstufen) ((IStructuredSelection) sel).getFirstElement();
				}
			}
		});

		Label lblSchlssel = new Label(container, SWT.NONE);
		lblSchlssel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblSchlssel.setText("Schlüssel:");

		schluesselText = new Text(container, SWT.BORDER);
		schluesselText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		schluesselText.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent pArg0) {
				schluessel = schluesselText.getText();

			}
		});

		Label lblAufgaben = new Label(container, SWT.NONE);
		lblAufgaben.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblAufgaben.setText("Aufgaben:");

		anzahlText = new Text(container, SWT.BORDER);
		anzahlText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		anzahlText.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent pArg0) {
				anzahl = anzahlText.getText();

			}
		});

		stufeViewer.setInput(MCAufgabenstufen.values());
		return container;
	}

	/**
	 * Create contents of the button bar.
	 *
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		Button btnOK = createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		btnOK.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent pE) {
				close();
			}

		});
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(450, 300);
	}

	public String getSchluessel() {
		return schluessel.trim();
	}

	public int getAnzahl() {
		return Integer.valueOf(anzahl.trim());
	}

	public MCAufgabenstufen getStufe() {
		return stufe;
	}

}
