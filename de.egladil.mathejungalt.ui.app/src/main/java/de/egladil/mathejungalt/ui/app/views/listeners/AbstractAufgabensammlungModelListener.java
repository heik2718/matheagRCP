/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.listeners;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;

import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;

/**
 * @author aheike
 */
public abstract class AbstractAufgabensammlungModelListener extends AbstractModelListener implements
	ISelectionChangedListener {

	/** */
	private IAufgabensammlung aufgabensammlung;

	/**
	 * @param pViewer
	 */
	public AbstractAufgabensammlungModelListener(TableViewer pViewer) {
		super(pViewer);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.views.listeners.AbstractModelListener#selectionChanged(org.eclipse.jface.viewers
	 * .SelectionChangedEvent)
	 */
	@Override
	public void selectionChanged(SelectionChangedEvent pEvent) {
		ISelection sel = pEvent.getSelection();
		if (!(sel instanceof IStructuredSelection) || sel.isEmpty()) {
			return;
		}
		Object selectedObject = ((IStructuredSelection) sel).getFirstElement();
		if (!(selectedObject instanceof IAufgabensammlung)) {
			return;
		}
		if (aufgabensammlung != null) {
			aufgabensammlung.removePropertyChangeListener(this);
		}
		aufgabensammlung = (IAufgabensammlung) selectedObject;
		if (aufgabensammlung != null) {
			aufgabensammlung.addPropertyChangeListener(this);
		}
	}

}
