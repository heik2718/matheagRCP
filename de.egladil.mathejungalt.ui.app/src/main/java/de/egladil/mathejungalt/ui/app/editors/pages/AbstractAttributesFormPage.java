/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.pages;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.MasterDetailsBlock;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.services.IDisposable;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.ui.app.editors.dnd.EditorAreaDragSourceAdapter;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.AbstractMatheEditorInput;
import de.egladil.mathejungalt.ui.app.editors.parts.MatheAGObjectAttributesPart;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;
import de.egladil.mathejungalt.ui.app.themes.LayoutFactory;

/**
 * Abstrakte Oberklasse für FormPages zum editieren der Attribute von {@link AbstractMatheAGObject} Instanzen. Er
 * enthält standardmäßig einen {@link MatheAGObjectAttributesPart} zum editieren der Attribute und kann mittels einer
 * Hakenmethode um weitere Teile (z.B. MasterDetailsBlocks) angereichert werden.
 *
 * @author winkelv
 */
public abstract class AbstractAttributesFormPage extends FormPage implements PropertyChangeListener {

	/** */
	private MatheAGObjectAttributesPart attributesPart;

	/** */
	private List<IDisposable> additionalParts;

	/** */
	private Composite editorArea;

	/**
	 * @param pEditor
	 * @param pId
	 * @param pTitle
	 */
	public AbstractAttributesFormPage(FormEditor pEditor, String pId, String pTitle) {
		super(pEditor, pId, pTitle);
		additionalParts = new ArrayList<IDisposable>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.FormPage#createFormContent(org.eclipse.ui.forms.IManagedForm)
	 */
	@Override
	protected void createFormContent(final IManagedForm pManagedForm) {
		final ScrolledForm form = pManagedForm.getForm();
		Image image = getImage();
		if (image != null)
			form.setImage(image);

		form.setText(getHeader());
		FormToolkit formToolkit = pManagedForm.getToolkit();

		Section section = formToolkit.createSection(form.getBody(), Section.TWISTIE | Section.DESCRIPTION
			| Section.TITLE_BAR);
		section.setActiveToggleColor(formToolkit.getHyperlinkGroup().getActiveForeground());
		formToolkit.createCompositeSeparator(section);

		// Der Behälter für die Teile dieser Section
		editorArea = formToolkit.createComposite(section, SWT.WRAP);

		// das Layout für den Behälter: es werden 2 Spalten verwendet: eine für Label oder Hyperlink, die zweite für
		// Text-Inputs
		GridLayout layout = LayoutFactory.getGridLayout(2);
		editorArea.setLayout(layout);

		// section anreichern und Behülter hinzufügen
		section.setText(getSectioHeader());
		section.setDescription(ITextConstants.SECTION_DESCR_EDITABLE_ATTRIBUTES);
		section.setClient(editorArea);
		section.setExpanded(true);
		section.addExpansionListener(new ExpansionAdapter() {
			public void expansionStateChanged(ExpansionEvent e) {
				form.reflow(false);
			}
		});

		GridData gd = LayoutFactory.getGridData(1);
		section.setLayoutData(gd);

		// hier den Teil mit den TextInputs einhängen
		attributesPart = getAttributesPart();
		attributesPart.initialize(editorArea, formToolkit, getDomainObject());
		attributesPart.refreshDataBindings(getDomainObject());

		// ggf. weitere Teile einhängen
		hookAdditionalParts(pManagedForm);

		layout = LayoutFactory.createFormPaneGridLayout(true, 1);
		form.getBody().setLayout(layout);

		// createSectionToolbar(section, formToolkit);
		// updateActionStatus();

		addDragSupport();

		formToolkit.paintBordersFor(editorArea);
		getDomainObject().addPropertyChangeListener(this);

	}

	/**
	 * Eine Methode zum Einhängen weiterer Teile in den Editor, z.B. eines {@link MasterDetailsBlock} in die
	 * {@link FormPage}
	 *
	 * @param pManagedForm {@link IManagedForm}
	 */
	protected void hookAdditionalParts(IManagedForm pManagedForm) {
		// default-Implementierung ist leer
	}

	/**
	 * @return {@link AbstractMatheAGObject} das editierte {@link AbstractMatheAGObject}
	 */
	public AbstractMatheAGObject getDomainObject() {
		return ((AbstractMatheEditorInput) getEditorInput()).getDomainObject();
	}

	/**
	 * @return {@link MatheAGObjectAttributesPart} die spezielle Implementierung, die hier verwendet werden soll.
	 */
	protected abstract MatheAGObjectAttributesPart getAttributesPart();

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.FormPage#setFocus()
	 */
	@Override
	public void setFocus() {
		if (attributesPart != null)
			attributesPart.setFocus();
	}

	/**
	 * @return String die Seitenüberschrift.
	 */
	protected abstract String getHeader();

	/**
	 * @return String die Sectionüberschrift.
	 */
	protected abstract String getSectioHeader();

	/**
	 * @return {@link Image} das Seitenbild zum Aufhübschen
	 */
	protected abstract Image getImage();

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.FormPage#dispose()
	 */
	@Override
	public void dispose() {
		// if (getDomainObject() != null) {
		// getDomainObject().removePropertyChangeListener(this);
		// }
		// attributesPart.dispose(getDomainObject());
		// for (IDisposable disp : additionalParts) {
		// disp.dispose();
		// }
		super.dispose();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	@Override
	public void propertyChange(PropertyChangeEvent pEvt) {
		// updateActionStatus();
	}

	/**
	 * Fügt in speziellen Fällen DragSupport hinzu
	 */
	protected void addDragSupport() {
		// wird DragSource
		int operations = DND.DROP_COPY | DND.DROP_DEFAULT;
		DragSource dragSource = new DragSource(editorArea, operations);

		Transfer[] types = new Transfer[] { LocalSelectionTransfer.getTransfer() };
		dragSource.setTransfer(types);

		dragSource.addDragListener(new EditorAreaDragSourceAdapter(this));
	}

	/**
	 * @param pDisposable
	 */
	protected void addAdditionalPart(IDisposable pDisposable) {
		if (additionalParts.contains(pDisposable))
			return;
		additionalParts.add(pDisposable);
	}

	/**
	 * @return the editorArea
	 */
	public Composite getEditorArea() {
		return editorArea;
	}

	/**
	 * @return the additionalParts
	 */
	protected List<IDisposable> getAdditionalParts() {
		return additionalParts;
	}
}
