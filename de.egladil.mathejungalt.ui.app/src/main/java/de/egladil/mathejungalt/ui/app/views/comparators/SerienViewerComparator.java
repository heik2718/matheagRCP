/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.comparators;

import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.ui.app.views.SerienView;

/**
 * @author Winkelv
 */
public class SerienViewerComparator extends AbstractViewerComparator {

	/**
	 * @param pIndex
	 */
	public SerienViewerComparator(int pIndex) {
		super(pIndex);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Object pO1, Object pO2) {
		if (pO1 instanceof String && pO2 instanceof String) {
			return pO1.toString().compareToIgnoreCase(pO2.toString());
		}
		switch (getIndex()) {
		case SerienView.INDEX_NUMMER:
			return ((Serie) pO1).getNummer().intValue() - ((Serie) pO2).getNummer().intValue();
		case SerienView.INDEX_RUNDE:
			return ((Serie) pO1).getRunde().intValue() - ((Serie) pO2).getRunde().intValue();
		case SerienView.INDEX_MONAT_JAHR:
			return ((Serie) pO1).getMonatJahrText().compareToIgnoreCase(((Serie) pO2).getMonatJahrText());
		default:
			return ((Serie) pO1).compareTo((Serie) pO2);
		}
	}
}
