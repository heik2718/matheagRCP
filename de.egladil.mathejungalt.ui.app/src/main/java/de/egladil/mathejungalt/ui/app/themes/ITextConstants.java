/**
 *
 */
package de.egladil.mathejungalt.ui.app.themes;

/**
 * @author winkelv
 */
public interface ITextConstants {

	final static String LEERZEILE = "\n"; //$NON-NLS-1$

	final static String BUCH_EDITOR_HEADER = "Buch anlegen oder \u00E4ndern"; //$NON-NLS-1$

	final static String MITGLIED_EDITOR_HEADER = "Mitglied anlegen oder \u00E4ndern"; //$NON-NLS-1$

	final static String SCHULE_EDITOR_HEADER = "Schule anlegen oder \u00E4ndern"; //$NON-NLS-1$

	final static String KONTAKT_EDITOR_HEADER = "Kontakt anlegen oder \u00E4ndern"; //$NON-NLS-1$

	final static String SERIE_EDITOR_HEADER = "Serie anlegen oder \u00E4ndern"; //$NON-NLS-1$

	final static String MINIKAENGURU_EDITOR_HEADER = "Minik\u00E4nguru anlegen oder \u00E4ndern"; //$NON-NLS-1$

	final static String ARBEITSBLATT_EDITOR_HEADER = "Arbeitsblatt anlegen oder \u00E4ndern"; //$NON-NLS-1$

	final static String AUFGABE_EDITOR_HEADER = "Aufgabe anlegen oder \u00E4ndern"; //$NON-NLS-1$

	final static String MCAUFGABE_EDITOR_HEADER = "Quizaufgabe anlegen oder \u00E4ndern"; //$NON-NLS-1$

	final static String MCARCHIVRAETSEL_EDITOR_HEADER = "Quiz anlegen oder \u00E4ndern"; //$NON-NLS-1$

	final static String AUTOR_EDITOR_HEADER = "Autor anlegen oder \u00E4ndern"; //$NON-NLS-1$

	final static String ZEITSCHRIFT_EDITOR_HEADER = "Zeitschrift anlegen oder \u00E4ndern"; //$NON-NLS-1$

	final static String SECTION_HEADER_BUCH_ATTRIBUTES = "Buchattribute"; //$NON-NLS-1$

	final static String SECTION_HEADER_AUFGABE_ATTRIBUTES = "Aufgabenattribute"; //$NON-NLS-1$

	final static String SECTION_HEADER_MITGLIED_ATTRIBUTES = "Mitgliedattribute"; //$NON-NLS-1$

	final static String SECTION_HEADER_AUFGABENSAMMLUNG_ATTRIBUTES = "Aufgabensammlungattribute"; //$NON-NLS-1$

	final static String SECTION_HEADER_QUIZ_ATTRIBUTES = "Quizattribute"; //$NON-NLS-1$

	final static String SECTION_HEADER_AUTOR_ATTRIBUTES = "Autorattribute"; //$NON-NLS-1$

	final static String SECTION_HEADER_BUCHQUELLE_ATTRIBUTES = "Buchquellenattribute"; //$NON-NLS-1$

	final static String SECTION_HEADER_KINDQUELLE_ATTRIBUTES = "Kindquellenattribute"; //$NON-NLS-1$

	final static String SECTION_HEADER_GRUPPENQUELLE_ATTRIBUTES = "Gruppenquellenattribute"; //$NON-NLS-1$

	final static String SECTION_HEADER_ZEITSCHIFTQUELLE_ATTRIBUTES = "Zeitschriftquellenattribute"; //$NON-NLS-1$

	final static String SECTION_HEADER_QUELLE_ATTRIBUTES = "Quellenattribute"; //$NON-NLS-1$

	final static String SECTION_HEADER_SERIENTEILNAHME_ATTRIBUTES = "Serienteilnahmenattribute"; //$NON-NLS-1$

	final static String SECTION_HEADER_MINITEILNAHME_ATTRIBUTES = "Minik\u00e4nguruteilnahmenattribute"; //$NON-NLS-1$

	final static String SECTION_HEADER_AUFGABENSAMMLUNGITEM_ATTRIBUTES = "Itemattribute"; //$NON-NLS-1$

	final static String SECTION_HEADER_ZEITSCHRIFT_ATTRIBUTES = "Zeitschriftattribute"; //$NON-NLS-1$

	final static String SECTION_HEADER_BUCH_AUTOREN = "Autoren"; //$NON-NLS-1$

	final static String SECTION_HEADER_MITGLIED_SERIE = "Serienteilnahmen"; //$NON-NLS-1$

	final static String SECTION_HEADER_SCHULE_MINI = "Minik\u00e4nguruteilnahmen"; //$NON-NLS-1$

	final static String SECTION_HEADER_KONTAKT_SCHULE = "Schulkontakte"; //$NON-NLS-1$

	final static String SECTION_HEADER_AUFGABENSAMMLUNG_AUFGABE = "Aufgaben"; //$NON-NLS-1$

	final static String SECTION_HEADER_QUELLE = "Quelle"; //$NON-NLS-1$

	final static String SECTION_HEADER_MITGLIED_DIPLOM = "Diplome"; //$NON-NLS-1$

	final static String SECTION_DESCR_EDITABLE_ATTRIBUTES = "Pflichtattribute haben ein Sternchen. Grau hinterlegte Attribute k\u00F6nnen nicht ge\u00E4ndert werden."; //$NON-NLS-1$

	final static String SECTION_DESCR_QUELLEN_ATTRIBUTES = "Quellenattribute werden nicht ge\u00E4ndert. Es kann eine neue Quelle zugeordnet werden."; //$NON-NLS-1$

	final static String SECTION_DESCR_OPEN_NEW_EDITOR = "zum \u00C4ndern der Attribute den Editor \u00F6ffnen"; //$NON-NLS-1$

	final static String SECTION_DESCR_BUCH_AUTORENVIEWER = "Autoren hinzuf\u00FCgen oder entfernen"; //$NON-NLS-1$

	final static String SECTION_DESCR_SERIENTEILNAHMENVIEWER = "Serienteilnahmen hinzuf\u00FCgen oder entfernen"; //$NON-NLS-1$

	final static String SECTION_DESCR_MINITEILNAHMENVIEWER = "Minikänguruteilnahmen hinzuf\u00FCgen oder entfernen"; //$NON-NLS-1$

	final static String SECTION_DESCR_SCHULKONTAKEVIEWER = "Schule hinzuf\u00FCgen oder entfernen"; //$NON-NLS-1$

	final static String SECTION_DESCR_AUFGABENSAMMLUNGITEMSVIEWER = "Aufgaben hinzuf\u00FCgen oder entfernen"; //$NON-NLS-1$

	final static String LABEL_TEXT_ID = "Id:"; //$NON-NLS-1$

	final static String LABEL_TEXT_SCHLUESSEL = "Schl\u00FCssel*:"; //$NON-NLS-1$

	final static String LABEL_TEXT_TITEL = "Titel*:"; //$NON-NLS-1$

	final static String LABEL_TEXT_BESCHREIBUNG = "Beschreibung:"; //$NON-NLS-1$

	final static String LABEL_TEXT_ANTWORT_A = "Antwort A:"; //$NON-NLS-1$

	final static String LABEL_TEXT_ANTWORT_B = "Antwort B:"; //$NON-NLS-1$

	final static String LABEL_TEXT_ANTWORT_C = "Antwort C:"; //$NON-NLS-1$

	final static String LABEL_TEXT_ANTWORT_D = "Antwort D:"; //$NON-NLS-1$

	final static String LABEL_TEXT_ANTWORT_E = "Antwort E:"; //$NON-NLS-1$

	final static String LABEL_TEXT_ANZAHL_ANTWORTEN = "Anzahl Antwortvorschl\u00e4ge"; //$NON-NLS-1$

	final static String LABEL_TEXT_LOESUNGSBUCHSTABE = "L\u00f6sungsbuchstabe*:"; //$NON-NLS-1$

	final static String LABEL_TEXT_LICENSE_SHORT = "Lizenz (kurz):"; //$NON-NLS-1$

	final static String LABEL_TEXT_LICENSE_FULL = "Lizenz (komplett):"; //$NON-NLS-1$

	final static String LABEL_TEXT_AUTOR = "Autor:"; //$NON-NLS-1$

	final static String LABEL_TEXT_PUNKTE = "Punkte*:"; //$NON-NLS-1$ 

	final static String LABEL_TEXT_FRUEHSTARTER = "Fr\u00FChstarter:"; //$NON-NLS-1$

	final static String LABEL_TEXT_RUECKMELDUNG = "R\u00fcckmeldung:"; //$NON-NLS-1$

	final static String LABEL_TEXT_DATUM = "Datum*:"; //$NON-NLS-1$

	final static String LABEL_TEXT_ALTER = "Alter*:"; //$NON-NLS-1$

	final static String LABEL_TEXT_ERFINDERPUNKTE = "Erfinderpunkte:"; //$NON-NLS-1$

	final static String LABEL_TEXT_AUFGABE = "Aufgabe:"; //$NON-NLS-1$

	final static String LABEL_TEXT_ANZAHL_ITEMS = "Anzahl Aufgaben:"; //$NON-NLS-1$

	final static String LABEL_TEXT_ANZAHL_DIPLOME = "Anzahl Diplome:"; //$NON-NLS-1$

	final static String LABEL_TEXT_MONAT_JAHR = "Monat-Jahr-Text*:"; //$NON-NLS-1$

	final static String LABEL_TEXT_BEENDET = "beendet:"; //$NON-NLS-1$

	final static String LABEL_TEXT_FOTOTITEL = "Fototitel:"; //$NON-NLS-1$

	final static String LABEL_TEXT_GEBURTSJAHR = "Geburtsjahr:"; //$NON-NLS-1$

	final static String LABEL_TEXT_GEBURTSMONAT = "Geburtsmonat:"; //$NON-NLS-1$

	final static String LABEL_TEXT_KLASSE = "Klasse*:"; //$NON-NLS-1$

	final static String LABEL_TEXT_KONTAKT = "Kontakt*:"; //$NON-NLS-1$

	final static String LABEL_TEXT_NACHNAME = "Nachname:"; //$NON-NLS-1$

	final static String LABEL_TEXT_ANSCHRIFT = "Anschrift*:"; //$NON-NLS-1$

	final static String LABEL_TEXT_EMAIL = "Email*:"; //$NON-NLS-1$

	final static String LABEL_TEXT_BUNDESLAND = "Bundesland*:"; //$NON-NLS-1$

	final static String LABEL_TEXT_ABO = "Abo:"; //$NON-NLS-1$

	final static String LABEL_TEXT_TELEFON = "Telefon:"; //$NON-NLS-1$

	final static String LABEL_TEXT_VORNAME = "Vorname*:"; //$NON-NLS-1$

	final static String LABEL_TEXT_NAME = "Name*:"; //$NON-NLS-1$

	final static String LABEL_TEXT_SERIENNUMMER = "Nummer*:"; //$NON-NLS-1$

	final static String LABEL_TEXT_RUNDE = "Runde*:"; //$NON-NLS-1$

	final static String LABEL_TEXT_NUMMER = "Nummer:"; //$NON-NLS-1$

	final static String BUTTON_TEXT_ADD = "ADD"; //$NON-NLS-1$

	final static String BUTTON_TEXT_REMOVE = "REMOVE"; //$NON-NLS-1$

	final static String LINK_TEXT_QUELLE_AENDERN = "\u00E4ndern"; //$NON-NLS-1$

	final static String FILTER_ACTION_ALLE = "alle"; //$NON-NLS-1$

	final static String LABEL_TEXT_ZWECK = "Zweck*:"; //$NON-NLS-1$

	final static String LABEL_TEXT_ART = "Art*:"; //$NON-NLS-1$

	final static String LABEL_TEXT_THEMA = "Thema*:"; //$NON-NLS-1$

	final static String LABEL_TEXT_STUFE = "Stufe*:"; //$NON-NLS-1$

	final static String LABEL_TEXT_JAHRESZEIT = "Jahreszeit:"; //$NON-NLS-1$

	final static String LABEL_TEXT_QUELLE = "Quelle*:"; //$NON-NLS-1$

	final static String LABEL_TEXT_QUELLENART = "Quellenart:"; //$NON-NLS-1$

	final static String LABEL_TEXT_BUCH = "Buch"; //$NON-NLS-1$

	final static String LABEL_TEXT_ZEITSCHRIFT = "Zeitschrift:"; //$NON-NLS-1$

	final static String LABEL_TEXT_MITGLIED = "Mitglied:"; //$NON-NLS-1$

	final static String LABEL_TEXT_ANZAHL = "Anzahl Mitglieder:"; //$NON-NLS-1$

	final static String LABEL_TEXT_SEITE = "Seite:"; //$NON-NLS-1$

	final static String LABEL_TEXT_AUSGABE = "Ausgabe:"; //$NON-NLS-1$

	final static String LABEL_TEXT_JAHRGANG = "Jahrgang:"; //$NON-NLS-1$

	final static String LABEL_TEXT_JAHR = "Jahr*:"; //$NON-NLS-1$

	final static String LABEL_TEXT_KLASSE_Q = "Klasse:"; //$NON-NLS-1$

	final static String LABEL_TEXT_ALTER_Q = "Alter:"; //$NON-NLS-1$

	final static String QUELLE_WIZARD_SELECTION_PAGE_NAME = "CreateQuelleWizardSelectionPage"; //$NON-NLS-1$

	final static String QUELLE_WIZARD_SELECTION_PAGE_TITLE = "Aufgabenquelle"; //$NON-NLS-1$

	final static String QUELLE_WIZARD_SELECTION_PAGE_DESCR = "Bitte w\u00E4hlen Sie die gew\u00FCnschte Quellenart aus."; //$NON-NLS-1$

	final static String QUELLE_BUCH_WIZARD_TITLE = "Buchquelle"; //$NON-NLS-1$

	final static String QUELLE_KIND_WIZARD_TITLE = "Kindquelle"; //$NON-NLS-1$

	final static String QUELLE_GRUPPE_WIZARD_TITLE = "Gruppenquelle"; //$NON-NLS-1$

	final static String QUELLE_NULL_WIZARD_TITLE = "Nullquelle"; //$NON-NLS-1$

	final static String QUELLE_ZEITSCHRIFT_WIZARD_TITLE = "Zeitschriftquelle"; //$NON-NLS-1$

	final static String QUELLE_WIZARD_DESCRIPTION = "Eine Quelle ausw\u00E4hlen oder anlegen."; //$NON-NLS-1$

	final static String QUELLE_SELECT_ART_PAGE_TITLE = "Quellenart w\u00E4hlen."; //$NON-NLS-1$

	final static String QUELLE_SELECT_MITGLIED_PAGE_NAME = "selectMitglied"; //$NON-NLS-1$

	final static String QUELLE_SELECT_MITGLIED_PAGE_TITLE = "Mitgliedquelle"; //$NON-NLS-1$

	final static String QUELLE_SELECT_MITGLIED_PAGE_DESCR = "Eine Mitgliedquelle wird nach Wahl eines Mitglieds erzeugt."; //$NON-NLS-1$

	final static String QUELLE_SELECT_NULLQUELLE_PAGE_NAME = "selectNullquelle"; //$NON-NLS-1$

	final static String QUELLE_SELECT_NULLQUELLE_PAGE_TITLE = "Nullquelle"; //$NON-NLS-1$

	final static String QUELLE_SELECT_NULLQUELLE_PAGE_DESCR = "Es gibt nur eine Nullquelle und das bin ich :)"; //$NON-NLS-1$

	final static String QUELLE_SELECT_BUCH_PAGE_NAME = "selectBuch"; //$NON-NLS-1$

	final static String QUELLE_SELECT_BUCH_PAGE_TITLE = "Buchquelle"; //$NON-NLS-1$

	final static String QUELLE_SELECT_BUCH_PAGE_DESCR = "Eine Buchquelle besteht aus einem Buch UND einer Seite"; //$NON-NLS-1$

	final static String QUELLE_SELECT_ZEITSCHRIFT_PAGE_NAME = "selectZeitschrift"; //$NON-NLS-1$

	final static String QUELLE_SELECT_ZEITSCHRIFT_PAGE_TITLE = "Zeitschriftquelle"; //$NON-NLS-1$

	final static String QUELLE_SELECT_ZEITSCHRIFT_PAGE_DESCR = "Eine Zeitschriftquelle besteht aus einer Zeitschrift, einer Ausgabe und einem Jahrgang."; //$NON-NLS-1$

	final static String QUELLE_SELECT_SEITE_PAGE_NAME = "selectSeite"; //$NON-NLS-1$

	final static String QUELLE_SELECT_SEITE_PAGE_TITLE = "Seite w\u00E4hlen"; //$NON-NLS-1$

	final static String QUELLE_SELECT_AUSGABE_TOOLTIP = "Hier die Ausgabe (Zahl!) eingeben."; //$NON-NLS-1$

	final static String QUELLE_SELECT_JAHRGANG_TOOLTIP = "Hier den Jahrgang (Zahl!) eingeben."; //$NON-NLS-1$

	final static String QUELLE_SELECT_SEITE_TOOLTIP = "Hier die Seite (Zahl!) eingeben."; //$NON-NLS-1$

	/** */
	final static String ZWECK_SERIE = "Serie"; //$NON-NLS-1$

	/** */
	final static String ZWECK_MINIKAENGURU = "Minik\u00E4nguru"; //$NON-NLS-1$

	/** */
	final static String ZWECK_MONOID = "Monoid"; //$NON-NLS-1$

	/** */
	final static String MEDIENART_ZEITSCHRIFT = "Zeitschrift"; //$NON-NLS-1$

	/** */
	final static String MEDIENART_BUCH = "Buch"; //$NON-NLS-1$

	/** */
	final static String QUELLENART_BUCHQUELLE = "Buchquelle"; //$NON-NLS-1$

	/** */
	final static String QUELLENART_GRUPPENQUELLE = "Gruppenquelle"; //$NON-NLS-1$

	/** */
	final static String QUELLENART_KINDQUELLE = "Kindquelle"; //$NON-NLS-1$

	/** */
	final static String QUELLENART_NULLQUELLE = "Nullquelle"; //$NON-NLS-1$

	/** */
	final static String QUELLENART_ZEITSCHRIFTQUELLE = "Zeitschriftquelle"; //$NON-NLS-1$

	/** */
	final static String THEMA_ARITHMETIK = "Arithmetik"; //$NON-NLS-1$

	/** */
	final static String THEMA_GEOMETRIE = "Geometrie"; //$NON-NLS-1$

	/** */
	final static String THEMA_LOGIK = "Logik"; //$NON-NLS-1$

	/** */
	final static String THEMA_KOMBINATORIK = "Kombinatorik"; //$NON-NLS-1$

	/** */
	final static String THEMA_WAHRSCHEINLICHKEIT = "Wahrscheinlichkeit"; //$NON-NLS-1$

	/** */
	final static String THEMA_PHYSIK = "Physik"; //$NON-NLS-1$

	/** */
	final static String THEMA_ZAHLENTHEORIE = "Zahlentheorie"; //$NON-NLS-1$

	/** */
	final static String AUFGABENART_EIGEN = "Eigenbau"; //$NON-NLS-1$

	/** */
	final static String AUFGABENART_NACH = "Nachbau"; //$NON-NLS-1$

	/** */
	final static String AUFGABENART_ZITAT = "Zitat"; //$NON-NLS-1$

	/** */
	final static String JAHRESZEIT_IMMER = "immer"; //$NON-NLS-1$

	/** */
	final static String JAHRESZEIT_FRUEH = "Fr\u00FChling"; //$NON-NLS-1$

	/** */
	final static String JAHRESZEIT_KARNEVAL = "Fasching"; //$NON-NLS-1$

	/** */
	final static String JAHRESZEIT_OSTERN = "Ostern"; //$NON-NLS-1$

	/** */
	final static String JAHRESZEIT_SOMMERR = "Sommer"; //$NON-NLS-1$

	/** */
	final static String JAHRESZEIT_HERBST = "Herbst"; //$NON-NLS-1$

	/** */
	final static String JAHRESZEIT_WINTER = "Winter"; //$NON-NLS-1$

	/** */
	final static String JAHRESZEIT_XMAS = "Weihnachten"; //$NON-NLS-1$

	/** */
	final static String STUFE_1 = "Vorschule"; //$NON-NLS-1$

	/** */
	final static String STUFE_2 = "Klassen 1 und 2"; //$NON-NLS-1$

	/** */
	final static String STUFE_3 = "Klassen 3 und 4"; //$NON-NLS-1$

	/** */
	final static String STUFE_4 = "Klassen 5 und 6"; //$NON-NLS-1$

	/** */
	final static String STUFE_5 = "Klassen 7 und 8"; //$NON-NLS-1$

	/** */
	final static String STUFE_6 = "Klassen 9 bis 13"; //$NON-NLS-1$

	/** */
	final static String PUNKTE_3 = "3 Punkte"; //$NON-NLS-1$

	/** */
	final static String PUNKTE_4 = "4 Punkte"; //$NON-NLS-1$

	/** */
	final static String PUNKTE_5 = "5 Punkte"; //$NON-NLS-1$

	/** */
	final static String MONOID_1 = "Mathies"; //$NON-NLS-1$

	/** */
	final static String MONOID_2 = "neue Aufgaben"; //$NON-NLS-1$

	/** */
	final static String GROUP_QUELLE_MASTER_LABEL = "Quellenattribute"; //$NON-NLS-1$

	/** */
	final static String[] DATE_FORMAT_PATTERNS = new String[] { "dd.MM.yyyy", "ddMMyyyy" };

	final static String LABEL_TEXT_PRIVAT = "privat";

	static final String SECTION_HEADER_SCHULE_ATTRIBUTES = "Schulattribute";

	static final String SECTION_HEADER_KONTAKT_ATTRIBUTES = "Kontaktattribute";

	static final String LABEL_TEXT_SCHULE = "Schule:";

	static final String LABEL_TEXT_AKTIVE_KONTAKTE = "aktive Kontakte";

}
