/**
 *
 */
package de.egladil.mathejungalt.ui.app.dialogs;

import org.eclipse.jface.wizard.Wizard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.medien.Zeitschrift;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * @author Winkelv
 */
public class CreateZeitschriftquelleWizard extends Wizard {

	private static final Logger LOG = LoggerFactory.getLogger(CreateZeitschriftquelleWizard.class);

	/** */
	private SelectZeitschriftWizardPage selectZeitschriftPage;

	/** */
	private CreateQuelleWizardSelectionPage selectionPage;

	/** */
	private IStammdatenservice stammdatenservice;

	/**
	 *
	 */
	public CreateZeitschriftquelleWizard(CreateQuelleWizardSelectionPage pSelectionPage,
		IStammdatenservice pStammdatenservice) {
		super();
		setWindowTitle(ITextConstants.QUELLE_ZEITSCHRIFT_WIZARD_TITLE);
		selectionPage = pSelectionPage;
		stammdatenservice = pStammdatenservice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#addPages()
	 */
	@Override
	public void addPages() {
		selectZeitschriftPage = new SelectZeitschriftWizardPage();
		addPage(selectZeitschriftPage);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		Zeitschrift zeitschrift = (Zeitschrift) selectZeitschriftPage.getFirstSelectedDomainObject();
		if (zeitschrift == null) {
			return false;
		}
		try {
			selectionPage.setQuelle(stammdatenservice.zeitschriftquelleFindenOderAnlegen(zeitschrift,
				selectZeitschriftPage.getAusgabe(), selectZeitschriftPage.getJahrgang()));
			return true;
		} catch (MatheJungAltException e) {
			LOG.error(e.getMessage());
			return false;
		}
	}
}
