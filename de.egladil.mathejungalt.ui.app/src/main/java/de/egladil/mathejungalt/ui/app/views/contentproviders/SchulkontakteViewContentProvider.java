/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.contentproviders;

import java.util.ArrayList;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

import de.egladil.mathejungalt.domain.schulen.IKontaktNames;
import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.editors.listeners.SchulkontakteViewerListener;

/**
 * ContentProvider für die Schulen zu einem Kontakt
 * 
 * @author Heike Winkelvoß
 */
public class SchulkontakteViewContentProvider extends AbstractMatheObjectsViewContentProvider implements
	IStructuredContentProvider {

	/**
	 * 
	 */
	public SchulkontakteViewContentProvider(IStammdatenservice pStammdatenservice) {
		super(pStammdatenservice);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
	 */
	@Override
	public Object[] getElements(Object pInputElement) {
		if (pInputElement == null || !(pInputElement instanceof Kontakt)) {
			return new ArrayList<SchulkontakteViewerListener>().toArray();
		}
		Kontakt kontakt = (Kontakt) pInputElement;
		if (!kontakt.isSchulkontakteGeladen()) {
			getStammdatenservice().schulkontakteLaden(kontakt);
		}
		return kontakt.getSchulkontakte().toArray();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.ui.app.views.contentproviders.AbstractMatheObjectsViewContentProvider#
	 * isInterestingChangeEvent(java.beans.PropertyChangeEvent)
	 */
	@Override
	public boolean isInterestingChangeEvent(String pPropertyName) {
		if (IKontaktNames.PROP_SCHULKONTAKTE.equals(pPropertyName)) {
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
	 */
	@Override
	public void dispose() {
		// nix
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public void inputChanged(Viewer pArg0, Object pArg1, Object pArg2) {
		pArg0.refresh();
	}
}
