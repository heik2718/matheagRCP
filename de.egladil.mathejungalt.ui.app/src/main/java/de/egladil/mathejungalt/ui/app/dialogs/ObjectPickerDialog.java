package de.egladil.mathejungalt.ui.app.dialogs;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;

public class ObjectPickerDialog extends Dialog {

	private Table table;

	private final IContentProvider contentProvider;

	private final ILabelProvider labelProvider;

	private ViewerComparator comparator;

	private Button okButton;

	private TableViewer tableViewer;

	private List<AbstractMatheAGObject> selectedObjects = new ArrayList<AbstractMatheAGObject>();

	/**
	 * Create the dialog.
	 *
	 * @param parentShell
	 */
	public ObjectPickerDialog(Shell parentShell, IContentProvider contentProvider, ILabelProvider labelProvider) {
		super(parentShell);
		this.contentProvider = contentProvider;
		this.labelProvider = labelProvider;
	}

	/**
	 * @param parentShell
	 * @param contentProvider
	 * @param labelProvider
	 * @param comparator
	 */
	public ObjectPickerDialog(Shell parentShell, IContentProvider contentProvider, ILabelProvider labelProvider,
		ViewerComparator comparator) {
		this(parentShell, contentProvider, labelProvider);
		this.comparator = comparator;
	}

	/**
	 * Create contents of the dialog.
	 *
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout(1, false));

		tableViewer = new TableViewer(container, SWT.BORDER | SWT.FULL_SELECTION | SWT.SINGLE);
		table = tableViewer.getTable();
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));

		tableViewer.setContentProvider(contentProvider);
		tableViewer.setLabelProvider(labelProvider);
		if (comparator != null) {
			tableViewer.setComparator(comparator);
		}
		tableViewer.setInput(new Object());

		tableViewer.addDoubleClickListener(new IDoubleClickListener() {
			@Override
			public void doubleClick(DoubleClickEvent arg0) {
				tableViewer.setSelection(arg0.getSelection());
				ObjectPickerDialog.this.setReturnCode(OK);
				ObjectPickerDialog.this.close();
			}
		});

		tableViewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent arg0) {
				selectedObjects.clear();
				ISelection sel = tableViewer.getSelection();
				if (sel != null && !sel.isEmpty()) {
					selectedObjects.add((AbstractMatheAGObject) ((IStructuredSelection) sel).getFirstElement());
				}
				okButton.setEnabled(!selectedObjects.isEmpty());
			}
		});

		return container;
	}

	/**
	 * Create contents of the button bar.
	 *
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		okButton = createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, false);
		okButton.addSelectionListener(new SelectionAdapter() {

			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent e) {
				ISelection sel = tableViewer.getSelection();
				if (sel != null && !sel.isEmpty()) {
					IStructuredSelection ssel = (IStructuredSelection) sel;
					@SuppressWarnings("unchecked")
					Iterator<AbstractMatheAGObject> iter = ssel.iterator();
					while (iter.hasNext()) {
						AbstractMatheAGObject o = iter.next();
						if (!selectedObjects.contains(o)) {
							selectedObjects.add(o);
						}
					}
				}
				super.widgetSelected(e);
			}
		});
		okButton.setEnabled(tableViewer.getSelection() != null && !tableViewer.getSelection().isEmpty());
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, true);
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(340, 460);
	}

	/**
	 * @return the selectedObjects
	 */
	public final List<AbstractMatheAGObject> getSelectedObjects() {
		return selectedObjects;
	}
}
