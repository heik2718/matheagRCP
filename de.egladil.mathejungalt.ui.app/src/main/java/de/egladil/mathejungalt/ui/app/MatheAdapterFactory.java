/**
 *
 */
package de.egladil.mathejungalt.ui.app;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IAdapterFactory;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serienitem;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.domain.mitglieder.Serienteilnahme;
import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.domain.schulen.MinikaenguruTeilnahme;
import de.egladil.mathejungalt.domain.schulen.Schule;
import de.egladil.mathejungalt.domain.schulen.Schulkontakt;
import de.egladil.mathejungalt.ui.app.adapters.IDetailsObject;
import de.egladil.mathejungalt.ui.app.adapters.IMasterObject;

/**
 * @author Winkelv
 */
public class MatheAdapterFactory implements IAdapterFactory {

	private class DetailsObjectAdapter implements IDetailsObject {

		private final AbstractMatheAGObject domainObject;

		/**
		 * @param pDomainObject
		 */
		public DetailsObjectAdapter(AbstractMatheAGObject pDomainObject) {
			super();
			domainObject = pDomainObject;
		}

		@Override
		public AbstractMatheAGObject getDomainObject() {
			return domainObject;
		}

		@Override
		public IMasterObject getMasterObject() {
			if (domainObject instanceof Serienteilnahme) {
				Object object = MatheJungAltActivator.getDefault().getAdapter(
					((Serienteilnahme) domainObject).getMitglied(), IMasterObject.class);
				if (object != null) {
					return (IMasterObject) object;
				}
			}
			if (domainObject instanceof Serienitem) {
				Object object = MatheJungAltActivator.getDefault().getAdapter(((Serienitem) domainObject).getSerie(),
					IMasterObject.class);
				if (object != null) {
					return (IMasterObject) object;
				}
			}
			if (domainObject instanceof MCArchivraetselItem) {
				Object object = MatheJungAltActivator.getDefault().getAdapter(
					((MCArchivraetselItem) domainObject).getRaetsel(), IMasterObject.class);
				if (object != null) {
					return (IMasterObject) object;
				}
			}
			if (domainObject instanceof Schulkontakt) {
				Object object = MatheJungAltActivator.getDefault().getAdapter(
					((Schulkontakt) domainObject).getSchule(), IMasterObject.class);
				if (object != null) {
					return (IMasterObject) object;
				}
			}
			if (domainObject instanceof MinikaenguruTeilnahme) {
				Object object = MatheJungAltActivator.getDefault().getAdapter(
					((MinikaenguruTeilnahme) domainObject).getMinikaenguru(), IMasterObject.class);
				if (object != null) {
					return (IMasterObject) object;
				}
			}
			return null;
		}
	}

	private class MasterObjectAdapter implements IMasterObject {

		/** */
		private final AbstractMatheAGObject domainObject;

		/**
		 * @param pDomainObject
		 */
		public MasterObjectAdapter(AbstractMatheAGObject pDomainObject) {
			super();
			domainObject = pDomainObject;
		}

		@Override
		public AbstractMatheAGObject getDomainObject() {
			return domainObject;
		}

		@Override
		public boolean canAdd(AbstractMatheAGObject pDomainObject) {
			if (domainObject instanceof Mitglied && pDomainObject instanceof Serie) {
				Serie serie = (Serie) pDomainObject;
				if (serie.getBeendet() == 0) {
					return false;
				}
				Mitglied mitglied = (Mitglied) domainObject;
				for (Serienteilnahme teilnahme : mitglied.getSerienteilnahmen()) {
					if (serie.equals(teilnahme.getSerie())) {
						return false;
					}
				}
				return true;
			}
			if (pDomainObject instanceof Aufgabe) {
				Aufgabe aufgabe = (Aufgabe) pDomainObject;
				if (domainObject instanceof IAufgabensammlung) {
					return ((IAufgabensammlung) domainObject).canAdd(aufgabe);
				}
			}
			if (pDomainObject instanceof MCAufgabe) {
				MCAufgabe aufgabe = (MCAufgabe) pDomainObject;
				if (domainObject instanceof MCArchivraetsel) {
					return ((MCArchivraetsel) domainObject).canAddAufgabe(aufgabe);
				}
			}
			if (pDomainObject instanceof Schule) {
				Schule schule = (Schule) pDomainObject;
				if (domainObject instanceof Kontakt) {
					Kontakt kontakt = (Kontakt) domainObject;
					for (Schulkontakt sk : kontakt.getSchulkontakte()) {
						if (sk.getSchule().equals(schule)) {
							return false;
						}
					}
				}
				return true;
			}
			if (pDomainObject instanceof Minikaenguru) {
				Minikaenguru minikaenguru = (Minikaenguru) pDomainObject;
				if (domainObject instanceof Schule) {
					// if (!MatheJungAltActivator.getDefault().istAktuellesJahr(minikaenguru.getJahr())) {
					// return false;
					// }
					Schule schule = (Schule) domainObject;
					for (MinikaenguruTeilnahme teilnahme : schule.getMinikaenguruTeilnahmen()) {
						if (teilnahme.getMinikaenguru().equals(minikaenguru)) {
							return false;
						}
					}
				}
				return true;
			}
			return false;
		}

		@Override
		public List<IDetailsObject> getItems() {
			List<IDetailsObject> items = new ArrayList<IDetailsObject>();
			if (domainObject instanceof Mitglied) {
				Mitglied mitglied = (Mitglied) domainObject;
				for (Serienteilnahme teilnahme : mitglied.getSerienteilnahmen()) {
					Object obj = MatheJungAltActivator.getDefault().getAdapter(teilnahme, IDetailsObject.class);
					if (obj != null) {
						items.add((IDetailsObject) obj);
					}
				}
			}
			if (domainObject instanceof Schule) {
				Schule schule = (Schule) domainObject;
				for (MinikaenguruTeilnahme teilnahme : schule.getMinikaenguruTeilnahmen()) {
					Object obj = MatheJungAltActivator.getDefault().getAdapter(teilnahme, IDetailsObject.class);
					if (obj != null) {
						items.add((IDetailsObject) obj);
					}
				}
			}
			if (domainObject instanceof Kontakt) {
				Kontakt kontakt = (Kontakt) domainObject;
				for (Schulkontakt schulkontakt : kontakt.getSchulkontakte()) {
					Object obj = MatheJungAltActivator.getDefault().getAdapter(schulkontakt, IDetailsObject.class);
					if (obj != null) {
						items.add((IDetailsObject) obj);
					}
				}
			}
			if (domainObject instanceof IAufgabensammlung) {
				IAufgabensammlung serie = (IAufgabensammlung) domainObject;
				for (IAufgabensammlungItem item : serie.getItems()) {
					Object obj = MatheJungAltActivator.getDefault().getAdapter(item, IDetailsObject.class);
					if (obj != null) {
						items.add((IDetailsObject) obj);
					}
				}
			}
			if (domainObject instanceof MCArchivraetsel) {
				MCArchivraetsel raetsel = (MCArchivraetsel) domainObject;
				for (MCArchivraetselItem item : raetsel.getItems()) {
					Object obj = MatheJungAltActivator.getDefault().getAdapter(item, IDetailsObject.class);
					if (obj != null) {
						items.add((IDetailsObject) obj);
					}
				}
			}
			// if (domainObject instanceof )
			// Collections.sort(items);
			return items;
		}

		@Override
		public String getTitle() {
			return domainObject.toString();
		}

		@Override
		public boolean isClosed() {
			if (domainObject instanceof Serie) {
				return ((Serie) domainObject).getBeendet() == 1;
			}
			if (domainObject instanceof MCArchivraetsel) {
				return ((MCArchivraetsel) domainObject).isVeroeffentlicht();
			}
			return false;
		}

		@Override
		public boolean isDetailsLoaded() {
			if (domainObject instanceof Serie) {
				return ((Serie) domainObject).isItemsLoaded();
			}
			if (domainObject instanceof Mitglied) {
				((Mitglied) domainObject).isSerienteilnahmenLoaded();
			}
			if (domainObject instanceof MCArchivraetsel) {
				return ((MCArchivraetsel) domainObject).isItemsLoaded();
			}
			if (domainObject instanceof Schule) {
				return ((Schule) domainObject).isMiniTeilnahmenLoaded();
			}
			if (domainObject instanceof Kontakt) {
				return ((Kontakt) domainObject).isSchulkontakteGeladen();
			}
			return false;
		}

		@Override
		public int size() {
			if (domainObject instanceof IAufgabensammlung) {
				return ((IAufgabensammlung) domainObject).anzahlAufgaben();
			}
			if (domainObject instanceof Mitglied) {
				((Mitglied) domainObject).gesamtpunkteSerien();
			}
			if (domainObject instanceof MCArchivraetsel) {
				return ((MCArchivraetsel) domainObject).getItems().size();
			}
			if (domainObject instanceof Schule) {
				return ((Schule) domainObject).getMinikaenguruTeilnahmen().size();
			}
			if (domainObject instanceof Kontakt) {
				return ((Kontakt) domainObject).getSchulkontakte().size();
			}
			return 0;
		}
	}

	/**
	 *
	 */
	public MatheAdapterFactory() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.IAdapterFactory#getAdapter(java.lang.Object, java.lang.Class)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Object getAdapter(Object pAdaptableObject, Class pAdapterType) {
		Assert.isNotNull(pAdaptableObject);
		if (IDetailsObject.class.equals(pAdapterType)) {
			return adaptToDetailsObject(pAdaptableObject);
		}
		if (IMasterObject.class.equals(pAdapterType)) {
			return adaptToMasterObject(pAdaptableObject);
		}
		return null;
	}

	/**
	 * @param pAdaptableObject
	 * @return
	 */
	private Object adaptToMasterObject(Object pAdaptableObject) {
		if (pAdaptableObject instanceof Mitglied || pAdaptableObject instanceof Schule
			|| pAdaptableObject instanceof Kontakt || pAdaptableObject instanceof IAufgabensammlung
			|| pAdaptableObject instanceof MCArchivraetsel) {
			return new MasterObjectAdapter((AbstractMatheAGObject) pAdaptableObject);
		}
		return null;
	}

	/**
	 * @param pAdaptableObject
	 * @return
	 */
	private Object adaptToDetailsObject(Object pAdaptableObject) {
		if (pAdaptableObject instanceof Serienteilnahme || pAdaptableObject instanceof IAufgabensammlungItem
			|| pAdaptableObject instanceof MCArchivraetselItem || pAdaptableObject instanceof Schulkontakt
			|| pAdaptableObject instanceof MinikaenguruTeilnahme) {
			return new DetailsObjectAdapter((AbstractMatheAGObject) pAdaptableObject);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.IAdapterFactory#getAdapterList()
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Class[] getAdapterList() {
		return new Class[] { IMasterObject.class, IDetailsObject.class };
	}
}
