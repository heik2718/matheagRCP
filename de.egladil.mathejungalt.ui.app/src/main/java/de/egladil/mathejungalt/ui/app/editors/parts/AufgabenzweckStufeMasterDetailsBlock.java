/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeansObservables;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.DetailsPart;
import org.eclipse.ui.forms.IDetailsPage;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.SectionPart;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgaben.IAufgabeNames;
import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenzweck;
import de.egladil.mathejungalt.ui.app.editors.converters.AufgabenzweckToStringConverter;
import de.egladil.mathejungalt.ui.app.editors.converters.StringToAufgabenzweckConverter;
import de.egladil.mathejungalt.ui.app.editors.pages.AbstractAttributesFormPage;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;
import de.egladil.mathejungalt.ui.app.themes.LayoutFactory;
import de.egladil.mathejungalt.ui.app.themes.PartControlsFactory;

/**
 * @author aheike
 */
public class AufgabenzweckStufeMasterDetailsBlock extends AbstractMasterDetailsBlock implements ISelectionProvider {

	/** */
	private Control[] controls;

	/** */
	private int widthHint = 100;

	/** */
	private Map<Aufgabenzweck, IDetailsPage> detailsPageMap = new HashMap<Aufgabenzweck, IDetailsPage>();

	/** */
	private List<ISelectionChangedListener> selectionChangedListeners = new ArrayList<ISelectionChangedListener>();

	/** */
	private IManagedForm managedForm;

	/** */
	private SectionPart sectionPart;

	/**
	 * @param pParentPage
	 */
	public AufgabenzweckStufeMasterDetailsBlock(AbstractAttributesFormPage pParentPage) {
		super(pParentPage);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.parts.AbstractMasterDetailsBlock#getSectionDescription()
	 */
	@Override
	protected String getSectionDescription() {
		return "Aufgabenzweck w\u00E4hlen";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.parts.AbstractMasterDetailsBlock#getSectionHeader()
	 */
	@Override
	protected String getSectionHeader() {
		return "Zweck";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.editors.parts.AbstractMasterDetailsBlock#hookSpecialParts(org.eclipse.ui.forms
	 * .IManagedForm, org.eclipse.ui.forms.widgets.Section, org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void hookSpecialParts(IManagedForm pManagedForm, Section pSection, Composite pComposite) {
		FormToolkit toolkit = pManagedForm.getToolkit();
		managedForm = pManagedForm;
		sectionPart = new SectionPart(pSection);
		managedForm.addPart(sectionPart);

		final Aufgabe aufgabe = (Aufgabe) getParentPage().getDomainObject();// (Aufgabe) managedForm.getInput();
		aufgabe.addPropertyChangeListener(this);
		boolean editable = aufgabe.getAnzahlSperrendeReferenzen() == 0;

		pComposite.setLayout(LayoutFactory.createSectionClientGridLayout(false, 2));

		controls = new Control[1];
		Control[] labels = new Control[1];

		labels[0] = PartControlsFactory.createLabel(pComposite, toolkit, ITextConstants.LABEL_TEXT_ZWECK);
		if (editable) {
			String[] items = new String[] { ITextConstants.ZWECK_SERIE, ITextConstants.ZWECK_MINIKAENGURU,
				ITextConstants.ZWECK_MONOID };
			controls[0] = PartControlsFactory.createCombo(pComposite, toolkit, items, widthHint);
		} else {
			controls[0] = PartControlsFactory.createText(pComposite, toolkit, widthHint, false);
		}

		// registriere einen SelectionChangeListener, der bewirkt dass das Anzeigen der DetailsSeite getriggert wird.
		addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent pEvent) {
				if (!(pEvent.getSource() instanceof AufgabenzweckStufeMasterDetailsBlock)) {
					return;
				}
				managedForm.fireSelectionChanged(sectionPart, getSelection());
			}
		});
		refreshDataBindings(aufgabe);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.parts.IEditorPartInputProvider#getSelectedInput()
	 */
	@Override
	public AbstractMatheAGObject getSelectedInput() {
		return getParentPage().getDomainObject();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.editors.parts.AbstractMasterDetailsBlock#propertyChange(java.beans.PropertyChangeEvent
	 * )
	 */
	@Override
	public void propertyChange(PropertyChangeEvent pEvt) {
		super.propertyChange(pEvt);
		if (IAufgabeNames.PROP_ZWECK.equals(pEvt.getPropertyName())) {
			managedForm.fireSelectionChanged(sectionPart, getSelection());
		}
	}

	/**
	 * @param pInput
	 */
	public void refreshDataBindings(AbstractMatheAGObject pInput) {
		if (pInput == null || !(pInput instanceof Aufgabe)) {
			return;
		}
		Binding[] bindings = new Binding[1];
		DataBindingContext dbc = new DataBindingContext();

		final Aufgabe aufgabe = (Aufgabe) pInput;

		IObservableValue observableWidget = null;
		if (controls[0] instanceof Text) {
			observableWidget = SWTObservables.observeText(controls[0], SWT.Modify);
		} else {
			observableWidget = SWTObservables.observeText(controls[0]);
		}
		IObservableValue observableProps = BeansObservables.observeValue(aufgabe, IAufgabeNames.PROP_ZWECK);
		UpdateValueStrategy targetToModelStrategy = new UpdateValueStrategy();
		targetToModelStrategy.setConverter(new StringToAufgabenzweckConverter());
		UpdateValueStrategy modelToTargetStrategy = new UpdateValueStrategy();
		modelToTargetStrategy.setConverter(new AufgabenzweckToStringConverter());
		bindings[0] = dbc.bindValue(observableWidget, observableProps, targetToModelStrategy, modelToTargetStrategy);

		aufgabe.removePropertyChangeListener(this);
		aufgabe.addPropertyChangeListener(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.MasterDetailsBlock#registerPages(org.eclipse.ui.forms.DetailsPart)
	 */
	@Override
	protected void registerPages(DetailsPart pDetailsPart) {
		pDetailsPart.setPageProvider(this);
		final ZweckSerieDetailsPage zweckSeriePage = new ZweckSerieDetailsPage(this);
		final ZweckMinikaenguruDetailsPage zweckMinikaenguruPage = new ZweckMinikaenguruDetailsPage(this);
		final ZweckMonoidDetailsPage zweckMonoidPage = new ZweckMonoidDetailsPage(this);

		pDetailsPart.registerPage(Aufgabenzweck.S, zweckSeriePage);
		pDetailsPart.registerPage(Aufgabenzweck.K, zweckMinikaenguruPage);
		pDetailsPart.registerPage(Aufgabenzweck.M, zweckMonoidPage);

		detailsPageMap.put(Aufgabenzweck.S, zweckSeriePage);
		detailsPageMap.put(Aufgabenzweck.K, zweckMinikaenguruPage);
		detailsPageMap.put(Aufgabenzweck.M, zweckMonoidPage);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.IDetailsPageProvider#getPage(java.lang.Object)
	 */
	@Override
	public IDetailsPage getPage(Object pArg0) {
		if (pArg0 instanceof Aufgabenzweck) {
			return detailsPart.getCurrentPage(); // detailsPageMap.get(pArg0);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.IDetailsPageProvider#getPageKey(java.lang.Object)
	 */
	@Override
	public Object getPageKey(Object pArg0) {
		if (!(pArg0 instanceof Aufgabe)) {
			return null;
		}
		return ((Aufgabe) pArg0).getZweck();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.jface.viewers.ISelectionProvider#addSelectionChangedListener(org.eclipse.jface.viewers.
	 * ISelectionChangedListener)
	 */
	@Override
	public void addSelectionChangedListener(ISelectionChangedListener pListener) {
		if (selectionChangedListeners.contains(pListener)) {
			return;
		}
		selectionChangedListeners.add(pListener);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ISelectionProvider#getSelection()
	 */
	@Override
	public ISelection getSelection() {
		// return new StructuredSelection(managedForm.getInput());
		return new StructuredSelection(getSelectedInput());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.eclipse.jface.viewers.ISelectionProvider#removeSelectionChangedListener(org.eclipse.jface.viewers.
	 * ISelectionChangedListener )
	 */
	@Override
	public void removeSelectionChangedListener(ISelectionChangedListener pListener) {
		selectionChangedListeners.remove(pListener);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ISelectionProvider#setSelection(org.eclipse.jface.viewers.ISelection)
	 */
	@Override
	public void setSelection(ISelection pSelection) {
		if (!(pSelection instanceof IStructuredSelection) || ((IStructuredSelection) pSelection).isEmpty()) {
			return;
		}
		Assert.isTrue(((IStructuredSelection) pSelection).getFirstElement() instanceof Aufgabe,
			"Nur " + Aufgabe.class.getName() + "-Objekte erlaubt!");
		for (ISelectionChangedListener listener : selectionChangedListeners) {
			listener.selectionChanged(new SelectionChangedEvent(this, pSelection));
		}
	}
}
