/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.editorinputs;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.schulen.Schule;
import de.egladil.mathejungalt.ui.app.editors.kopierer.IKopierer;
import de.egladil.mathejungalt.ui.app.editors.kopierer.SchuleKopierer;

/**
 * @author Winkelv
 */
public class SchuleEditorInput extends AbstractMatheEditorInput {

	/**
	 * @param pObject
	 */
	public SchuleEditorInput(AbstractMatheAGObject pObject) {
		super(pObject);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.editorinputs.AbstractMatheEditorInput#getKopierer()
	 */
	@Override
	public IKopierer getKopierer() {
		return new SchuleKopierer();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.editorinputs.AbstractMatheEditorInput#initCache()
	 */
	@Override
	public void initCache() {
		setCache(new Schule());
		applyChanges();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.IAdaptable#getAdapter(java.lang.Class)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Object getAdapter(Class pAdapter) {
		if (pAdapter.getName().equals(Schule.class.getName())) {
			return getDomainObject();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object pObj) {
		if (pObj == null) {
			return false;
		}
		if (!(pObj instanceof SchuleEditorInput)) {
			return false;
		}
		SchuleEditorInput param = (SchuleEditorInput) pObj;
		return getAdapter(Schule.class).equals(param.getAdapter(Schule.class));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.editorinputs.AbstractMatheEditorInput#getInputObjectType()
	 */
	@Override
	protected String getInputObjectType() {
		return Schule.class.getName();
	}
}
