/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.pages;

import java.beans.PropertyChangeEvent;

import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormEditor;

import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.editors.parts.MatheAGObjectAttributesPart;
import de.egladil.mathejungalt.ui.app.editors.parts.MiniteilnahmenMasterDetailsBlock;
import de.egladil.mathejungalt.ui.app.editors.parts.SchuleSimpleAttributePartDataBindingProvider;
import de.egladil.mathejungalt.ui.app.themes.IImageKeys;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Die AttributesFormPage verhält sich wie der MasterPart in einem MasterDetailsBlock.
 * 
 * @author winkelv
 */
public class SchuleAttributesFormPage extends AbstractAttributesFormPage {

	/** */
	private MiniteilnahmenMasterDetailsBlock miniteilnahmenBlock;

	/**
	 * @param pEditor
	 * @param pId
	 * @param pTitle
	 */
	public SchuleAttributesFormPage(FormEditor pEditor, String pId, String pTitle) {
		super(pEditor, pId, pTitle);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.pages.AbstractAttributesFormPage#hookAdditionalParts(org.eclipse.ui.forms.IManagedForm
	 * )
	 */
	@Override
	protected void hookAdditionalParts(IManagedForm pManagedForm) {
		miniteilnahmenBlock = new MiniteilnahmenMasterDetailsBlock(this);
		miniteilnahmenBlock.createContent(pManagedForm);
		addAdditionalPart(miniteilnahmenBlock);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.pages.AbstractAttributesFormPage#getAttributesPart()
	 */
	@Override
	protected MatheAGObjectAttributesPart getAttributesPart() {
		return new MatheAGObjectAttributesPart(true, new SchuleSimpleAttributePartDataBindingProvider());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.pages.AbstractAttributesFormPage#getHeader()
	 */
	@Override
	protected String getHeader() {
		return ITextConstants.SCHULE_EDITOR_HEADER;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.pages.AbstractAttributesFormPage#getSectioHeader()
	 */
	@Override
	protected String getSectioHeader() {
		return ITextConstants.SECTION_HEADER_SCHULE_ATTRIBUTES;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.pages.AbstractAttributesFormPage#getImage()
	 */
	@Override
	protected Image getImage() {
		return MatheJungAltActivator.getDefault().getImage(IImageKeys.MITGLIED);
	}

	@Override
	public void propertyChange(PropertyChangeEvent pEvt) {
		super.propertyChange(pEvt);
	}
}
