/**
 *
 */
package de.egladil.mathejungalt.ui.app.themes;

import org.eclipse.jface.resource.ColorRegistry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.SectionPart;

import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.domain.quellen.AbstractQuelle;
import de.egladil.mathejungalt.domain.quellen.Zeitschriftquelle;
import de.egladil.mathejungalt.domain.types.EnumTypes.Quellenart;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;

/**
 * @author winkelv
 */
public final class LayoutFactory {

	// Used in place of 0. If 0 is used, widget borders will appear clipped
	// on some platforms (e.g. Windows XP Classic Theme).
	// Form tool kit requires parent composites containing the widget to have
	// at least 1 pixel border margins in order to paint the flat borders.
	// The form toolkit paints flat borders on a given widget when native
	// borders are not painted by SWT. See FormToolkit#paintBordersFor()
	public static final int DEFAULT_CLEAR_MARGIN = 2;

	// Required to allow space for field decorations
	public static final int CONTROL_HORIZONTAL_INDENT = 3;

	// UI Forms Standards

	// FORM BODY
	public static final int FORM_BODY_MARGIN_TOP = 12;

	public static final int FORM_BODY_MARGIN_BOTTOM = 12;

	public static final int FORM_BODY_MARGIN_LEFT = 6;

	public static final int FORM_BODY_MARGIN_RIGHT = 6;

	public static final int FORM_BODY_HORIZONTAL_SPACING = 20;

	// Should be 20; but, we minus 3 because the section automatically pads the
	// bottom margin by that amount
	public static final int FORM_BODY_VERTICAL_SPACING = 17;

	public static final int FORM_BODY_MARGIN_HEIGHT = 0;

	public static final int FORM_BODY_MARGIN_WIDTH = 0;

	// SECTION CLIENT
	public static final int SECTION_CLIENT_MARGIN_TOP = 5;

	public static final int SECTION_CLIENT_MARGIN_BOTTOM = 5;

	// Should be 6; but, we minus 4 because the section automatically pads the
	// left margin by that amount
	public static final int SECTION_CLIENT_MARGIN_LEFT = 2;

	// Should be 6; but, we minus 4 because the section automatically pads the
	// right margin by that amount
	public static final int SECTION_CLIENT_MARGIN_RIGHT = 2;

	public static final int SECTION_CLIENT_HORIZONTAL_SPACING = 5;

	public static final int SECTION_CLIENT_VERTICAL_SPACING = 5;

	public static final int SECTION_CLIENT_MARGIN_HEIGHT = 0;

	public static final int SECTION_CLIENT_MARGIN_WIDTH = 0;

	public static final int SECTION_HEADER_VERTICAL_SPACING = 6;

	// CLEAR
	public static final int CLEAR_MARGIN_TOP = DEFAULT_CLEAR_MARGIN;

	public static final int CLEAR_MARGIN_BOTTOM = DEFAULT_CLEAR_MARGIN;

	public static final int CLEAR_MARGIN_LEFT = DEFAULT_CLEAR_MARGIN;

	public static final int CLEAR_MARGIN_RIGHT = DEFAULT_CLEAR_MARGIN;

	public static final int CLEAR_HORIZONTAL_SPACING = 0;

	public static final int CLEAR_VERTICAL_SPACING = 0;

	public static final int CLEAR_MARGIN_HEIGHT = 0;

	public static final int CLEAR_MARGIN_WIDTH = 0;

	// FORM PANE
	public static final int FORM_PANE_MARGIN_TOP = 0;

	public static final int FORM_PANE_MARGIN_BOTTOM = 0;

	public static final int FORM_PANE_MARGIN_LEFT = 0;

	public static final int FORM_PANE_MARGIN_RIGHT = 0;

	public static final int FORM_PANE_HORIZONTAL_SPACING = FORM_BODY_HORIZONTAL_SPACING;

	public static final int FORM_PANE_VERTICAL_SPACING = FORM_BODY_VERTICAL_SPACING;

	public static final int FORM_PANE_MARGIN_HEIGHT = 0;

	public static final int FORM_PANE_MARGIN_WIDTH = 0;

	// MASTER DETAILS
	public static final int MASTER_DETAILS_MARGIN_TOP = 0;

	public static final int MASTER_DETAILS_MARGIN_BOTTOM = 0;

	// Used only by masters part. Details part margin dynamically calculated
	public static final int MASTER_DETAILS_MARGIN_LEFT = 0;

	// Used only by details part. Masters part margin dynamically calcualated
	public static final int MASTER_DETAILS_MARGIN_RIGHT = 1;

	public static final int MASTER_DETAILS_HORIZONTAL_SPACING = FORM_BODY_HORIZONTAL_SPACING;

	public static final int MASTER_DETAILS_VERTICAL_SPACING = FORM_BODY_VERTICAL_SPACING;

	public static final int MASTER_DETAILS_MARGIN_HEIGHT = 0;

	public static final int MASTER_DETAILS_MARGIN_WIDTH = 0;

	/**
	 *
	 */
	private LayoutFactory() {
		super();
	}

	/**
	 * Erzeugt neues {@link GridData} f�r ein Element. Das Element wird horizontal und vertikal den Nachbarelementen
	 * angepasst.
	 *
	 * @param pHorSpan - Anzahl der Spalten die diese Zelle �berspannt
	 * @return GridData - in der gew�nschten Breite
	 */
	public static GridData getGridData(int pHorSpan) {
		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.verticalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.horizontalSpan = pHorSpan;

		return gridData;
	}

	/**
	 * Erzeugt neues {@link GridData} f�r ein Element. Das Element wird horizontal und vertikal den Nachbarelementen
	 * angepasst.
	 *
	 * @param pHorSpan - Anzahl der Spalten des GridLayouts
	 * @return GridData - in der gew�nschten Breite
	 */
	public static GridData getGridData(int pHorSpan, int pWidthHint) {
		GridData gridData = getGridData(pHorSpan);
		gridData.widthHint = pWidthHint;

		return gridData;
	}

	/**
	 * Erzuegt {@link GridLayout} f�r ein Element. Alle Spalten des GridLayouts haben die gleiche Breite. Der Abstand zu
	 * anderen Elementen betr�gt jeweils 2.
	 *
	 * @param pNumCols - Anzahl Spalten f�r das Layout
	 * @return GridLayout
	 */
	public static GridLayout getGridLayoutEqualColumns(int pNumCols) {
		GridLayout layout = new GridLayout();
		layout.numColumns = pNumCols;
		layout.makeColumnsEqualWidth = true;
		layout.marginWidth = 2;
		layout.marginHeight = 2;

		return layout;
	}

	/**
	 * Erzuegt {@link GridLayout} f�r ein Element. Der Abstand zu anderen Elementen betr�gt jeweils 2.
	 *
	 * @param pNumCols - Anzahl Spalten f�r das Layout
	 * @return GridLayout
	 */
	public static GridLayout getGridLayout(int pNumCols) {
		GridLayout layout = new GridLayout();
		layout.numColumns = pNumCols;
		// layout.makeColumnsEqualWidth = false;
		layout.marginWidth = 2;
		layout.marginHeight = 2;
		return layout;
	}

	/**
	 * Erzeut ein {@link GridLayout} f�r Untercontainer einer FormPane.
	 *
	 * @param pMakeColumnsEqualWidth
	 * @param pNumColumns
	 * @return {@link GridLayout}
	 */
	public static GridLayout createFormPaneGridLayout(boolean pMakeColumnsEqualWidth, int pNumColumns) {
		GridLayout layout = new GridLayout();

		layout.marginHeight = FORM_PANE_MARGIN_HEIGHT;
		layout.marginWidth = FORM_PANE_MARGIN_WIDTH;

		layout.marginTop = FORM_PANE_MARGIN_TOP;
		layout.marginBottom = FORM_PANE_MARGIN_BOTTOM;
		layout.marginLeft = FORM_PANE_MARGIN_LEFT;
		layout.marginRight = FORM_PANE_MARGIN_RIGHT;

		layout.horizontalSpacing = FORM_PANE_HORIZONTAL_SPACING;
		layout.verticalSpacing = FORM_PANE_VERTICAL_SPACING;

		layout.makeColumnsEqualWidth = pMakeColumnsEqualWidth;
		layout.numColumns = pNumColumns;

		return layout;
	}

	/**
	 * {@link GridLayout} für Teile, die in einem {@link SectionPart} enthalten sind
	 *
	 * @param pMakeColumnsEqualWidth
	 * @param pNumColumns
	 * @return
	 */
	public static GridLayout createSectionClientGridLayout(boolean pMakeColumnsEqualWidth, int pNumColumns) {
		GridLayout layout = new GridLayout();

		layout.marginHeight = SECTION_CLIENT_MARGIN_HEIGHT;
		layout.marginWidth = SECTION_CLIENT_MARGIN_WIDTH;

		layout.marginTop = SECTION_CLIENT_MARGIN_TOP;
		layout.marginBottom = SECTION_CLIENT_MARGIN_BOTTOM;
		layout.marginLeft = SECTION_CLIENT_MARGIN_LEFT;
		layout.marginRight = SECTION_CLIENT_MARGIN_RIGHT;

		layout.horizontalSpacing = SECTION_CLIENT_HORIZONTAL_SPACING;
		layout.verticalSpacing = SECTION_CLIENT_VERTICAL_SPACING;

		layout.makeColumnsEqualWidth = pMakeColumnsEqualWidth;
		layout.numColumns = pNumColumns;

		return layout;
	}

	/**
	 * @param pAufgabe
	 * @return
	 */
	public static Color getColorByAufgbe(Aufgabe pAufgabe) {
		ColorRegistry registry = PlatformUI.getWorkbench().getThemeManager().getCurrentTheme().getColorRegistry();
		if (pAufgabe == null) {
			return registry.get(IColorKeys.BLACK);
		}
		switch (pAufgabe.getArt()) {
		case E:
			return registry.get(IColorKeys.GREEN);
		case N:
			return registry.get(IColorKeys.BLACK);
		case Z:
			AbstractQuelle quelle = pAufgabe.getQuelle();
			if (Quellenart.K.equals(quelle.getArt()) || Quellenart.G.equals(quelle.getArt())) {
				return registry.get(IColorKeys.BLUE);
			}
			if (Quellenart.Z.equals(quelle.getArt())) {
				if (!quelle.isCompletelyLoaded()) {
					MatheJungAltActivator.getDefault().getStammdatenservice().quelleZuAufgabeLaden(pAufgabe);
				}
				if ("alpha".equalsIgnoreCase(((Zeitschriftquelle) quelle).getZeitschrift().getTitel())) {
					return registry.get(IColorKeys.BLUE);
				}
			}
			return registry.get(IColorKeys.RED);
		default:
			return registry.get(IColorKeys.BLACK);
		}
	}

	/**
	 * @param pAufgabe
	 * @return
	 */
	public static Color getColorByMCAufgbe(MCAufgabe pAufgabe) {
		ColorRegistry registry = PlatformUI.getWorkbench().getThemeManager().getCurrentTheme().getColorRegistry();
		if (pAufgabe == null) {
			return registry.get(IColorKeys.BLACK);
		}
		switch (pAufgabe.getArt()) {
		case E:
			return registry.get(IColorKeys.GREEN);
		case N:
			return registry.get(IColorKeys.BLACK);
		case Z:
			String quelle = pAufgabe.getQuelle();
			if (quelle.indexOf("alpha") >= 0) {
				return registry.get(IColorKeys.BLUE);
			}
			return registry.get(IColorKeys.RED);
		default:
			return registry.get(IColorKeys.BLACK);
		}
	}

	/**
	 * @param pAufgabe
	 * @return
	 */
	public static Color getColorByMitglied(Mitglied pMitglied) {
		ColorRegistry registry = PlatformUI.getWorkbench().getThemeManager().getCurrentTheme().getColorRegistry();
		if (pMitglied == null) {
			return registry.get(IColorKeys.BLACK);
		}
		return pMitglied.isAktiv() ? registry.get(IColorKeys.GREEN) : Display.getCurrent().getSystemColor(
			SWT.COLOR_GRAY);
	}

	/**
	 * @param pAufgabensammlung
	 * @return
	 */
	public static Color getColorByBeendetStatus(IAufgabensammlung pAufgabensammlung) {
		ColorRegistry registry = PlatformUI.getWorkbench().getThemeManager().getCurrentTheme().getColorRegistry();
		if (pAufgabensammlung == null) {
			return registry.get(IColorKeys.BLACK);
		}
		switch (pAufgabensammlung.getBeendet()) {
		case 1:
			return registry.get(IColorKeys.RED);
		default:
			return registry.get(IColorKeys.BLACK);
		}
	}

	/**
	 * @param pAufgabensammlung
	 * @return
	 */
	public static Color getColorByPublishedStatus(MCArchivraetsel pAufgabensammlung) {
		ColorRegistry registry = PlatformUI.getWorkbench().getThemeManager().getCurrentTheme().getColorRegistry();
		if (pAufgabensammlung == null) {
			return registry.get(IColorKeys.BLACK);
		}
		return pAufgabensammlung.isVeroeffentlicht() ? registry.get(IColorKeys.RED) : registry.get(IColorKeys.BLACK);
	}
}
