package de.egladil.mathejungalt.ui.app.views;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.handlers.IHandlerService;

import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.IArbeitsblattNames;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.IMinikaenguruNames;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.ISerieNames;
import de.egladil.mathejungalt.domain.mcraetsel.IMCAufgabeNames;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.service.stammdaten.event.IModelChangeListener;
import de.egladil.mathejungalt.service.stammdaten.event.ModelChangeEvent;
import de.egladil.mathejungalt.ui.app.editors.MCAufgabeEditor;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.AbstractMatheEditorInput;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.MCAufgabeEditorInput;
import de.egladil.mathejungalt.ui.app.handlers.AufgabenMarkSchlechtHandler;
import de.egladil.mathejungalt.ui.app.handlers.GenerierenHandler;
import de.egladil.mathejungalt.ui.app.handlers.ICommandIds;
import de.egladil.mathejungalt.ui.app.handlers.KaengurutemplatesGenerierenHandler;
import de.egladil.mathejungalt.ui.app.handlers.ObjektAnlegenHandler;
import de.egladil.mathejungalt.ui.app.themes.TableViewerFactory;
import de.egladil.mathejungalt.ui.app.views.comparators.AbstractViewerComparator;
import de.egladil.mathejungalt.ui.app.views.comparators.IViewerComparator;
import de.egladil.mathejungalt.ui.app.views.comparators.MCAufgabenViewerComparator;
import de.egladil.mathejungalt.ui.app.views.contentproviders.MCAufgabenViewContentProvider;
import de.egladil.mathejungalt.ui.app.views.labelproviders.CommonColumnLabelProvider;
import de.egladil.mathejungalt.ui.app.views.labelproviders.MCAufgabenLabelProvider;
import de.egladil.mathejungalt.ui.app.views.listeners.AbstractModelListener;
import de.egladil.mathejungalt.ui.app.views.listeners.MCAufgabenModelListener;

/**
 * @author aheike
 */
public class MCAufgabenView extends AbstractMatheJungAltView implements IModelChangeListener {

	/** */
	public static final String ID = "de.egladil.mathejungalt.ui.app.mcaufgabenview";

	/** */
	private String[] columnTitles = new String[] { "Schl\u00FCssel", "Stufe", "Punkte", "Thema", "Titel" };

	/** */
	private int[] columnBounds = new int[] { 80, 50, 50, 50, 200 };

	/**
	 * Erzeugen programmatisch die benötigten Handler. Diese sind aktiv nur wenn ein Vergleichsobjekt oder ein
	 * Ausschnitt ausgewählt wurde.
	 */
	protected void createHandlers() {
		super.createHandlers();
		IHandlerService handlerService = (IHandlerService) getSite().getService(IHandlerService.class);
		handlerService.activateHandler(ICommandIds.ANLEGEN, new ObjektAnlegenHandler(getStammdatenservice(), ID));
		handlerService.activateHandler(ICommandIds.GENERIEREN, new GenerierenHandler(getViewer(),
			getStammdatenservice()));
		handlerService.activateHandler(ICommandIds.FLAG_SCHLECHT_AENDERN, new AufgabenMarkSchlechtHandler(getViewer(),
			getStammdatenservice()));
		handlerService.activateHandler(ICommandIds.KAENG_TEMPLATES_GENERIEREN, new KaengurutemplatesGenerierenHandler(
			getStammdatenservice(), this));
		getStammdatenservice().addModelChangeListener(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#getModelListener(org.eclipse.jface.viewers.TableViewer
	 * )
	 */
	@Override
	protected AbstractModelListener getModelListener(TableViewer pViewer) {
		return new MCAufgabenModelListener(pViewer);
	}

	/**
	 * @param pParent
	 * @return
	 */
	protected TableViewer initTableViewer(Composite pParent) {
		ITableViewColumn[] viewColumns = new ITableViewColumn[columnTitles.length];
		IViewerComparator[] comparators = getColumnComparators();
		ColumnLabelProvider[] labelProviders = getColumnLableProviders();
		for (int i = 0; i < columnTitles.length; i++) {
			viewColumns[i] = new ViewColumn(columnTitles[i], columnBounds[i], comparators[i], labelProviders[i]);
		}
		return TableViewerFactory.createTableViewer(pParent, new MCAufgabenViewContentProvider(getStammdatenservice()),
			viewColumns, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#getEditorInput(java.lang.Object)
	 */
	@Override
	protected AbstractMatheEditorInput getEditorInput(Object pObj) {
		if (!(pObj instanceof MCAufgabe)) {
			return null;
		}
		MCAufgabe aufgabe = (MCAufgabe) pObj;
		return new MCAufgabeEditorInput(aufgabe);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#getViewID()
	 */
	@Override
	protected String getEditorID() {
		return MCAufgabeEditor.ID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#isInterestingPropertyChangeEvent(de.egladil.
	 * mathejungalt.service.stammdaten.event.ModelChangeEvent)
	 */
	protected boolean isInterestingPropertyChangeEvent(ModelChangeEvent pEvt) {
		final String property = pEvt.getProperty();
		return pEvt.getNewValue() instanceof MCAufgabe
			|| pEvt.getOldValue() instanceof MCAufgabe
			|| pEvt.getSource() instanceof MCAufgabe
			&& (IMCAufgabeNames.PROP_SCHLUESSEL.equals(property) || IMCAufgabeNames.PROP_ART.equals(property)
				|| IMCAufgabeNames.PROP_BEMERKUNG.equals(property) || IMCAufgabeNames.PROP_GESPERRT.equals(property)
				|| IMCAufgabeNames.PROP_LOESUNGSBUCHSTABE.equals(property)
				|| IMCAufgabeNames.PROP_PUNKTE.equals(property) || IMCAufgabeNames.PROP_QUELLE.equals(property)
				|| IMCAufgabeNames.PROP_VERZEICHNIS.equals(property) || IMCAufgabeNames.PROP_STUFE.equals(property)
				|| IMCAufgabeNames.PROP_THEMA.equals(property) || IMCAufgabeNames.PROP_TITEL.equals(property)
				|| ISerieNames.PROP_SERIENITEMS.equals(property)
				|| IMinikaenguruNames.PROP_MINKAENGURUITEMS.equals(property) || IArbeitsblattNames.PROP_ITEMS
					.equals(property));
	}

	/**
	 * Erzeugt abhängig vom Spaltenindex einen {@link ColumnLabelProvider}
	 *
	 * @param pInd int der Spaltenindex
	 * @return {@link ColumnLabelProvider}
	 */
	private ColumnLabelProvider[] getColumnLableProviders() {
		CommonColumnLabelProvider[] labelProviders = new CommonColumnLabelProvider[columnTitles.length];

		for (int i = 0; i < labelProviders.length; i++) {
			labelProviders[i] = new MCAufgabenLabelProvider(i);
		}
		return labelProviders;
	}

	/**
	 * @return
	 */
	private AbstractViewerComparator[] getColumnComparators() {
		AbstractViewerComparator[] comparators = new MCAufgabenViewerComparator[columnTitles.length];
		for (int i = 0; i < comparators.length; i++) {
			comparators[i] = new MCAufgabenViewerComparator(i);
		}
		return comparators;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.event.IModelChangeListener#modelChanged(de.egladil.mathejungalt.service
	 * .stammdaten .event.ModelChangeEvent)
	 */
	@Override
	public void modelChanged(ModelChangeEvent pEvt) {
		// if (pEvt.getSource() instanceof MCAufgabe || pEvt.getSource() instanceof IStammdatenservice
		// && pEvt.getNewValue() instanceof List) {
		// getViewer().refresh(true);
		// }
		if (pEvt.getSource() instanceof MCAufgabe) {
			getViewer().refresh(true);
		}
	}

	public void refresh() {
		getViewer().refresh(true);
	}
}