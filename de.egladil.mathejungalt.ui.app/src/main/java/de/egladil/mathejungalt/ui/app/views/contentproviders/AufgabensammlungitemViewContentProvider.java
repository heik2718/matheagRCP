/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.contentproviders;

import java.util.ArrayList;

import org.eclipse.jface.viewers.IStructuredContentProvider;

import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.IArbeitsblattNames;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.IMinikaenguruNames;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.ISerieNames;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * @author Heike Winkelvoß
 */
public class AufgabensammlungitemViewContentProvider extends AbstractMatheObjectsViewContentProvider implements
	IStructuredContentProvider {

	/**
	 * @param pStammdatenservice
	 */
	public AufgabensammlungitemViewContentProvider(IStammdatenservice pStammdatenservice) {
		super(pStammdatenservice);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
	 */
	@Override
	public Object[] getElements(Object pInputElement) {
		if (pInputElement == null || !(pInputElement instanceof IAufgabensammlung))
			return new ArrayList<Serie>().toArray();
		IAufgabensammlung aufgabensammlung = (IAufgabensammlung) pInputElement;
		if (!aufgabensammlung.isItemsLoaded()) {
			getStammdatenservice().aufgabensammlungitemsLaden(aufgabensammlung);
		}
		return aufgabensammlung.getItems().toArray();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
	 */
	@Override
	public void dispose() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.rcp.contentproviders.AbstractSerienViewContentProvider#isInterestingChangeEvent(java.beans.
	 * PropertyChangeEvent )
	 */
	@Override
	public boolean isInterestingChangeEvent(String pPropertyName) {
		if (IAufgabensammlungItem.PROP_NUMMER.equals(pPropertyName)
			|| IAufgabensammlungItem.PROP_AUFGABE.equals(pPropertyName)
			|| ISerieNames.PROP_SERIENITEMS.equals(pPropertyName)
			|| IMinikaenguruNames.PROP_MINKAENGURUITEMS.equals(pPropertyName)
			|| IArbeitsblattNames.PROP_ITEMS.equals(pPropertyName)) {
			return true;
		}
		return false;
	}
}
