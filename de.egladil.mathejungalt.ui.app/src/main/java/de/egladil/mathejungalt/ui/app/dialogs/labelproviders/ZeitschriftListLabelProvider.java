/**
 * 
 */
package de.egladil.mathejungalt.ui.app.dialogs.labelproviders;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import de.egladil.mathejungalt.domain.medien.Zeitschrift;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.themes.IImageKeys;

/**
 * @author Winkelv
 */
public class ZeitschriftListLabelProvider extends LabelProvider {

	/**
	 * 
	 */
	public ZeitschriftListLabelProvider() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.LabelProvider#getImage(java.lang.Object)
	 */
	@Override
	public Image getImage(Object pElement) {
		return MatheJungAltActivator.getDefault().getImage(IImageKeys.ZEITSCHRIFT);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.LabelProvider#getText(java.lang.Object)
	 */
	@Override
	public String getText(Object pElement) {
		Zeitschrift medium = (Zeitschrift) pElement;
		return medium.getTitel();
	}
}
