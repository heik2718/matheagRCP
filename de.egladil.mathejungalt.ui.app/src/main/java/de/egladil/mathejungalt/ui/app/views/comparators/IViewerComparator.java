package de.egladil.mathejungalt.ui.app.views.comparators;

import java.util.Comparator;

/**
 * @author aheike
 */
public interface IViewerComparator extends Comparator<Object> {

	/**
	 * @return
	 */
	int getIndex();

}