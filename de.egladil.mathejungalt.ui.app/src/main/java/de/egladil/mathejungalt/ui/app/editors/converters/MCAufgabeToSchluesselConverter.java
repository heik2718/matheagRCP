/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.converters;

import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;

/**
 * Konvertiert eine Aufgabe in einen Schluessel-Wert
 *
 * @author Winkelv
 */
public class MCAufgabeToSchluesselConverter implements IConverter {

	/**
	 *
	 */
	public MCAufgabeToSchluesselConverter() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object pFromObject) {
		MCAufgabe aufgabe = null;
		try {
			aufgabe = (MCAufgabe) pFromObject;
		} catch (ClassCastException e) {
			return null;
		}
		return aufgabe.getSchluessel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getFromType()
	 */
	@Override
	public Object getFromType() {
		return MCAufgabe.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getToType()
	 */
	@Override
	public Object getToType() {
		return String.class;
	}
}
