/**
 *
 */
package de.egladil.mathejungalt.ui.app.views.labelproviders;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.swt.graphics.Color;

import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.ui.app.themes.LayoutFactory;

/**
 * @author winkelv
 */
public class MCRaetselitemColumnLabelProvider extends ColumnLabelProvider {

	/** */
	private static final int INDEX_SCHLUESSEL = 0;

	/** */
	private static final int INDEX_NUMMER = 1;

	/** */
	private int index;

	/**
	 * @param pIndex
	 */
	public MCRaetselitemColumnLabelProvider(int pIndex) {
		super();
		index = pIndex;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ColumnLabelProvider#getForeground(java.lang.Object)
	 */
	@Override
	public Color getForeground(Object pElement) {
		return LayoutFactory.getColorByMCAufgbe(((MCArchivraetselItem) pElement).getAufgabe());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ColumnLabelProvider#getText(java.lang.Object)
	 */
	@Override
	public String getText(Object pElement) {
		MCArchivraetselItem item = (MCArchivraetselItem) pElement;
		switch (index) {
		case INDEX_SCHLUESSEL:
			return item.getAufgabe().getSchluessel();
		case INDEX_NUMMER:
			return item.getNummer();
		default:
			return item.getAufgabe().getPunkte().toString();
		}
	}

}
