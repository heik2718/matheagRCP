/**
 *
 */
package de.egladil.mathejungalt.ui.app.views.labelproviders;

import org.eclipse.swt.graphics.Color;

import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.ui.app.themes.LayoutFactory;

/**
 * @author aheike
 */
public class MitgliederLabelProvider extends CommonColumnLabelProvider {

	/**
	 *
	 */
	public MitgliederLabelProvider(int pIndex) {
		super(pIndex);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ColumnLabelProvider#getForeground(java.lang.Object)
	 */
	@Override
	public Color getForeground(Object pElement) {
		return LayoutFactory.getColorByMitglied((Mitglied) pElement);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ColumnLabelProvider#getText(java.lang.Object)
	 */
	@Override
	public String getText(Object pElement) {
		switch (getIndex()) {
		case 0:
			return ((Mitglied) pElement).getVorname();
		case 1:
			return ((Mitglied) pElement).getNachname();
		case 2:
			return ((Mitglied) pElement).getKlasse().toString();
		case 3:
			return ((Mitglied) pElement).getAlter().toString();
		default:
			return ((Mitglied) pElement).getKontakt();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.CellLabelProvider#getToolTipText(java.lang.Object)
	 */
	@Override
	public String getToolTipText(Object pElement) {
		final Mitglied mitglied = (Mitglied) pElement;
		String suffix = mitglied.getGeburtsmonat() == null ? "" : ", " + mitglied.getGeburtsmonat().toString() + ", "
			+ mitglied.getGeburtsjahr().toString();
		return mitglied.getId() == null ? suffix : mitglied.getId().toString() + suffix;
	}

}
