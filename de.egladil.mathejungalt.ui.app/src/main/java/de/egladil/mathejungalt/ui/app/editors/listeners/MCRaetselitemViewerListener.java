/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.listeners;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.jface.viewers.ColumnLabelProvider;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.mcraetsel.IMCArchivraetselNames;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.service.stammdaten.event.ModelChangeEvent;
import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;
import de.egladil.mathejungalt.ui.app.adapters.IDetailsObject;
import de.egladil.mathejungalt.ui.app.editors.parts.AbstractViewerPartWithButtons;
import de.egladil.mathejungalt.ui.app.views.labelproviders.MCAufgabenLabelProvider;

/**
 * Listener für den TableViewer mit den {@link IAufgabensammlungItem}.
 *
 * @author Winkelv
 */
public class MCRaetselitemViewerListener extends AbstractViewerPartWithButtonsViewerListener {

	private MCAufgabenLabelProvider labelProvider = new MCAufgabenLabelProvider(0);

	/**
	 * @param pView
	 */
	public MCRaetselitemViewerListener(AbstractViewerPartWithButtons pView) {
		super(pView);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.listeners.AbstractViewerPartWithButtonsViewerListener#getObjectPool()
	 */
	@Override
	public Collection<AbstractMatheAGObject> getObjectPool() {
		Collection<AbstractMatheAGObject> objectPool = new ArrayList<AbstractMatheAGObject>();
		Collection<MCAufgabe> aufgaben = getStammdatenservice().getQuizaufgaben();
		for (MCAufgabe aufgabe : aufgaben) {
			objectPool.add(aufgabe);
		}
		return objectPool;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.listeners.IStructuredViewerPartViewerListener#handleRemove()
	 */
	@Override
	public void handleRemove() {
		if (getEditorInput() == null || getSelectedDetails() == null || getSelectedDetails().size() != 1) {
			return;
		}
		Object selected = getSelectedDetails().get(0);
		MCArchivraetselItem item = null;
		if (selected instanceof MCArchivraetselItem) {
			item = (MCArchivraetselItem) selected;
		}
		if (selected instanceof IDetailsObject) {
			item = (MCArchivraetselItem) ((IDetailsObject) selected).getDomainObject();
		}
		if (item == null) {
			throw new MatheJungAltException(
				"Fehler in der Anwendung: das gewählte Objekt hat nicht den richtigen Typ: typ = "
					+ selected.getClass().getName());
		}
		getStammdatenservice().quizaufgabeAusRaetselEntfernen(item);
		if (getView() != null) {
			getView().getViewer().refresh();
			getView().updateButtonAndMenuStatus();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.editors.listeners.AbstractViewerPartWithButtonsViewerListener#isInterestingEvent
	 * (de.egladil.mathejungalt.service.stammdaten.event.ModelChangeEvent)
	 */
	@Override
	protected boolean isInterestingEvent(ModelChangeEvent pEvt) {
		if (ModelChangeEvent.ADD_EVENT.equals(pEvt.getEventType())
			|| ModelChangeEvent.REMOVE_EVENT.equals(pEvt.getEventType())) {
			return pEvt.getNewValue() instanceof MCArchivraetselItem
				|| pEvt.getOldValue() instanceof MCArchivraetselItem;
		}
		if (ModelChangeEvent.PROPERTY_CHANGE_EVENT.equals(pEvt.getEventType())
			&& !(pEvt.getSource() instanceof MCArchivraetselItem || pEvt.getSource() instanceof MCArchivraetsel)) {
			return false;
		}
		final String property = pEvt.getProperty();
		return isRelevant(property);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.editors.listeners.AbstractViewerPartWithButtonsViewerListener#isInterestingEvent
	 * (java.beans.PropertyChangeEvent)
	 */
	@Override
	protected boolean isInterestingEvent(PropertyChangeEvent pEvt) {
		if (!(pEvt.getSource() instanceof MCArchivraetselItem)) {
			return false;
		}
		return isRelevant(pEvt.getPropertyName());
	}

	/**
	 * @return {@link ColumnLabelProvider} der LabelProvider.
	 */
	public ColumnLabelProvider getLabelProvider() {
		return labelProvider;
	}

	/**
	 * @param pPropertyName
	 * @return boolean
	 */
	private boolean isRelevant(final String pPropertyName) {
		return MCArchivraetselItem.PROP_NUMMER.equals(pPropertyName)
			|| IMCArchivraetselNames.PROP_AUTOR.equals(pPropertyName)
			|| IMCArchivraetselNames.PROP_ITEMS.equals(pPropertyName)
			|| IMCArchivraetselNames.PROP_LICENSE_FULL.equals(pPropertyName)
			|| IMCArchivraetselNames.PROP_LICENSE_SHORT.equals(pPropertyName)
			|| IMCArchivraetselNames.PROP_STUFE.equals(pPropertyName)
			|| IMCArchivraetselNames.PROP_TITEL.equals(pPropertyName)
			|| IMCArchivraetselNames.PROP_SCHLUESSEL.equals(pPropertyName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.editors.listeners.AbstractViewerPartWithButtonsViewerListener#specialAdd(java.
	 * util.List)
	 */
	@Override
	protected void specialAdd(List<AbstractMatheAGObject> selectedObjects) {
		MCArchivraetsel serie = (MCArchivraetsel) getEditorInput();
		MCArchivraetselItem item = null;
		if (selectedObjects != null) {
			for (int i = 0; i < selectedObjects.size(); i++) {
				if (i == 0) {
					item = (MCArchivraetselItem) getStammdatenservice().quizaufgabeZuRaetselHinzufuegen(serie,
						(MCAufgabe) selectedObjects.get(i));
				} else {
					getStammdatenservice().quizaufgabeZuRaetselHinzufuegen(serie, (MCAufgabe) selectedObjects.get(i));
				}
			}
		}
		if (item != null) {
			Object object = MatheJungAltActivator.getDefault().getAdapter(item, IDetailsObject.class);
			if (object == null) {
				throw new MatheJungAltException("Kein Adapter fuer " + item.getClass().getName()
					+ " und IDetailsObject gefunden");
			}
			IDetailsObject detailsObject = (IDetailsObject) object;
			updateDetailsList(detailsObject);
		}
	}
}
