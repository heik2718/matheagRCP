/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.comparators;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.AssertionFailedException;

import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;

/**
 * @author Winkelv
 */
public class NumericComparator extends AbstractViewerComparator {

	/**
	 * @param pIndex
	 */
	public NumericComparator() {
		super();
	}

	/**
	 * @param pIndex
	 * @param pType
	 */
	public NumericComparator(int pIndex, String pType) {
		super(pIndex, pType);
	}

	/**
	 * @param pIndex
	 */
	public NumericComparator(int pIndex) {
		super(pIndex);
	}

	/**
	 * @param pType
	 */
	public NumericComparator(String pType) {
		super(pType);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Object pO1, Object pO2) {
		if (getType() == null) {
			Assert.isTrue(pO1 instanceof Integer, "pO1 ist kein Integer");
			Assert.isTrue(pO2 instanceof Integer, "pO2 ist kein Integer");
			return ((Integer) pO1).intValue() - ((Integer) pO2).intValue();
		}
		if (Minikaenguru.class.getName().equals(getType())) {
			return ((Minikaenguru) pO1).getJahr().intValue() - ((Minikaenguru) pO2).getJahr().intValue();
		}
		if (Arbeitsblatt.class.getName().equals(getType())) {
			return ((Arbeitsblatt) pO1).getSperrend() - ((Arbeitsblatt) pO2).getSperrend();
		}
		throw new AssertionFailedException("assertion failed: Der type ist nicht gesetzt"); //$NON-NLS-1$
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.views.sorters.AbstractViewerComparator#getIndex()
	 */
	@Override
	public int getIndex() {
		return 0;
	}
}
