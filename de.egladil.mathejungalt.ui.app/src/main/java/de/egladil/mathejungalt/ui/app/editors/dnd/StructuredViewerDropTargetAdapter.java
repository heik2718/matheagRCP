/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.dnd;

import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;

/**
 * Adapter für Drag-und Drop- Events zwischen Viewern und Editoren. Die Methode drop(DropEvent) muss überschrieben
 * werden, wenn das Droppen eines Objekts eine Aktion auslösen soll. Dieser Adapter hat nur eine leere Implementierung
 * für diese Methode. Mit der Methode doAcceptForDrop(AbstractMatheAGObject) kann kontextabängig unterschieden werden,
 * ob das gezogene Objekt für eine drop-Operation akzeptiert wird.<br>
 * <br>
 * Damit ein Objekt als DragSource erkannt wird, muss die Methode AbstractDragSourceAdapter.dragStart() erweitert
 * werden. Dort wird festgelegt, welche Sourcen DragSourcen sein können.
 *
 * @author winkelv
 */
public class StructuredViewerDropTargetAdapter extends DropTargetAdapter {

	/** */
	private AbstractMatheAGObject targetObject;

	/**
	 * @param pTargetObject {@link AbstractMatheAGObject} das Objekt, das Ziel der dnd-Operation ist.
	 */
	public StructuredViewerDropTargetAdapter(AbstractMatheAGObject pTargetObject) {
		targetObject = pTargetObject;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.dnd.DropTargetAdapter#dragEnter(org.eclipse.swt.dnd.DropTargetEvent)
	 */
	@Override
	public void dragEnter(DropTargetEvent pEvent) {
		retargetEvent(pEvent);
	}

	/**
	 * Es wird immer nur COPY angezeigt.
	 *
	 * @param pEvent
	 */
	private void retargetEvent(DropTargetEvent pEvent) {
		if (!LocalSelectionTransfer.getTransfer().isSupportedType(pEvent.currentDataType)) {
			pEvent.detail = DND.DROP_NONE;
		} else {
			pEvent.detail = DND.DROP_COPY;

		}
		pEvent.feedback = DND.FEEDBACK_NONE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.dnd.DropTargetAdapter#dragOperationChanged(org.eclipse.swt.dnd.DropTargetEvent)
	 */
	@Override
	public void dragOperationChanged(DropTargetEvent pEvent) {
		retargetEvent(pEvent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.dnd.DropTargetAdapter#dragOver(org.eclipse.swt.dnd.DropTargetEvent)
	 */
	@Override
	public void dragOver(DropTargetEvent pEvent) {
		retargetEvent(pEvent);
	}

	/**
	 * List aus dem {@link LocalSelectionTransfer} das {@link AbstractMatheAGObject} heraus, falls vorhanden.
	 *
	 * @return {@link AbstractMatheAGObject} oder null!
	 */
	protected AbstractMatheAGObject getDragedObject() {
		ISelection selection = LocalSelectionTransfer.getTransfer().getSelection();
		if (!(selection instanceof IStructuredSelection)) {
			return null;
		}
		if (((IStructuredSelection) selection).getFirstElement() instanceof AbstractMatheAGObject) {
			return (AbstractMatheAGObject) ((IStructuredSelection) selection).getFirstElement();
		}
		return null;
	}

	/**
	 * @return
	 */
	protected final AbstractMatheAGObject getTargetObject() {
		return targetObject;
	}

}
