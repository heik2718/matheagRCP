package de.egladil.mathejungalt.ui.app.views;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.handlers.IHandlerService;

import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.service.stammdaten.event.IModelChangeListener;
import de.egladil.mathejungalt.service.stammdaten.event.ModelChangeEvent;
import de.egladil.mathejungalt.ui.app.editors.MCRaetselEditor;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.AbstractMatheEditorInput;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.MCRaetselEditorInput;
import de.egladil.mathejungalt.ui.app.handlers.GenerierenHandler;
import de.egladil.mathejungalt.ui.app.handlers.ICommandIds;
import de.egladil.mathejungalt.ui.app.handlers.ObjektAnlegenHandler;
import de.egladil.mathejungalt.ui.app.handlers.PostHtlatexHandler;
import de.egladil.mathejungalt.ui.app.handlers.PrepareHtlatexHandler;
import de.egladil.mathejungalt.ui.app.handlers.PublishQuizHandler;
import de.egladil.mathejungalt.ui.app.handlers.RaetsellisteGenerierenHandler;
import de.egladil.mathejungalt.ui.app.themes.LayoutFactory;
import de.egladil.mathejungalt.ui.app.themes.TableViewerFactory;
import de.egladil.mathejungalt.ui.app.views.comparators.AbstractViewerComparator;
import de.egladil.mathejungalt.ui.app.views.comparators.IViewerComparator;
import de.egladil.mathejungalt.ui.app.views.comparators.MCRaetselViewerComparator;
import de.egladil.mathejungalt.ui.app.views.contentproviders.MCRaetselViewContentProvider;
import de.egladil.mathejungalt.ui.app.views.listeners.AbstractModelListener;
import de.egladil.mathejungalt.ui.app.views.listeners.MCRaetselModelListener;

public class MCRaetselView extends AbstractMatheJungAltView implements IModelChangeListener {

	/** */
	public static final String ID = "de.egladil.mathejungalt.ui.app.raetselview";

	/** */
	public static final int INDEX_SCHLUESSEL = 0;

	/** */
	public static final int INDEX_STUFE = 1;

	/** */
	public static final int INDEX_TITEL = 2;

	/** */
	public static final int INDEX_PRIVAT = 3;

	public static final int INDEX_PUBLISHED = 4;

	/** */
	private final String[] columnTitles = new String[] { "Schl\u00fcssel", "Stufe", "Titel", "privat", "published" };

	/** */
	private final int[] columnBounds = new int[] { 80, 50, 160, 50, 50 };

	/**
	 *
	 */
	public MCRaetselView() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractAufgabensammlungView#createHandlers()
	 */
	@Override
	protected void createHandlers() {
		super.createHandlers();
		IHandlerService handlerService = (IHandlerService) getSite().getService(IHandlerService.class);
		handlerService.activateHandler(ICommandIds.ANLEGEN, new ObjektAnlegenHandler(getStammdatenservice(), ID));
		handlerService
			.activateHandler(ICommandIds.PUBLISH, new PublishQuizHandler(getViewer(), getStammdatenservice()));
		handlerService.activateHandler(ICommandIds.GENERIEREN, new GenerierenHandler(getViewer(),
			getStammdatenservice()));
		handlerService.activateHandler(ICommandIds.RAETSELLISTE_GENERIEREN, new RaetsellisteGenerierenHandler(
			getStammdatenservice()));
		handlerService.activateHandler(ICommandIds.HTLATEX_VORBEREITEN, new PrepareHtlatexHandler(getViewer(),
			getStammdatenservice()));
		handlerService.activateHandler(ICommandIds.HTLATEX_NACHBEREITEN, new PostHtlatexHandler(getViewer(),
			getStammdatenservice()));
		getStammdatenservice().addModelChangeListener(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#getEditorID()
	 */
	@Override
	protected String getEditorID() {
		return MCRaetselEditor.ID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#getEditorInput(java.lang.Object)
	 */
	@Override
	protected AbstractMatheEditorInput getEditorInput(Object pObj) {
		if (!(pObj instanceof MCArchivraetsel)) {
			return null;
		}
		MCArchivraetsel raetsel = (MCArchivraetsel) pObj;
		if (!raetsel.isItemsLoaded()) {
			getStammdatenservice().raetselitemsLaden(raetsel);
		}
		return new MCRaetselEditorInput((MCArchivraetsel) pObj);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#getModelListener(org.eclipse.jface.viewers.TableViewer
	 * )
	 */
	@Override
	protected AbstractModelListener getModelListener(TableViewer pViewer) {
		return new MCRaetselModelListener(pViewer);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractAufgabensammlungView#getColumnComparators()
	 */
	protected IViewerComparator[] getColumnComparators() {
		AbstractViewerComparator[] comparators = new MCRaetselViewerComparator[columnTitles.length];
		for (int i = 0; i < comparators.length; i++) {
			comparators[i] = new MCRaetselViewerComparator(i);
		}
		return comparators;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractAufgabensammlungView#getColumnLableProviders()
	 */
	protected ColumnLabelProvider[] getColumnLableProviders() {
		ColumnLabelProvider[] labelProviders = new ColumnLabelProvider[columnTitles.length];
		for (int i = 0; i < labelProviders.length; i++) {
			switch (i) {
			case MCRaetselView.INDEX_SCHLUESSEL:
				labelProviders[i] = new ColumnLabelProvider() {
					@Override
					public String getText(Object pElement) {
						return ((MCArchivraetsel) pElement).getSchluessel() != null ? ((MCArchivraetsel) pElement)
							.getSchluessel() : "";
					}

					/*
					 * (non-Javadoc)
					 * 
					 * @see org.eclipse.jface.viewers.ColumnLabelProvider#getForeground(java.lang.Object)
					 */
					@Override
					public Color getForeground(Object pElement) {
						return LayoutFactory.getColorByPublishedStatus((MCArchivraetsel) pElement);
					}
				};
				break;
			case MCRaetselView.INDEX_STUFE:
				labelProviders[i] = new ColumnLabelProvider() {
					@Override
					public String getText(Object pElement) {
						return ((MCArchivraetsel) pElement).getStufe() != null ? ((MCArchivraetsel) pElement)
							.getStufe().getStufe() + "" : "";
					}

					/*
					 * (non-Javadoc)
					 * 
					 * @see org.eclipse.jface.viewers.ColumnLabelProvider#getForeground(java.lang.Object)
					 */
					@Override
					public Color getForeground(Object pElement) {
						return LayoutFactory.getColorByPublishedStatus((MCArchivraetsel) pElement);
					}
				};
				break;
			case MCRaetselView.INDEX_PRIVAT:
				labelProviders[i] = new ColumnLabelProvider() {
					@Override
					public String getText(Object pElement) {
						String privat = ((MCArchivraetsel) pElement).isPrivat() ? "J" : "N";
						return privat;
					}

					/*
					 * (non-Javadoc)
					 * 
					 * @see org.eclipse.jface.viewers.ColumnLabelProvider#getForeground(java.lang.Object)
					 */
					@Override
					public Color getForeground(Object pElement) {
						return LayoutFactory.getColorByPublishedStatus((MCArchivraetsel) pElement);
					}
				};
				break;
			case MCRaetselView.INDEX_PUBLISHED:
				labelProviders[i] = new ColumnLabelProvider() {
					@Override
					public String getText(Object pElement) {
						String published = ((MCArchivraetsel) pElement).isVeroeffentlicht() ? "J" : "N";
						return published;
					}

					/*
					 * (non-Javadoc)
					 * 
					 * @see org.eclipse.jface.viewers.ColumnLabelProvider#getForeground(java.lang.Object)
					 */
					@Override
					public Color getForeground(Object pElement) {
						return LayoutFactory.getColorByPublishedStatus((MCArchivraetsel) pElement);
					}
				};
				break;
			default:
				labelProviders[i] = new ColumnLabelProvider() {
					@Override
					public String getText(Object pElement) {
						return ((MCArchivraetsel) pElement).getTitel();
					}

					/*
					 * (non-Javadoc)
					 * 
					 * @see org.eclipse.jface.viewers.ColumnLabelProvider#getForeground(java.lang.Object)
					 */
					@Override
					public Color getForeground(Object pElement) {
						return LayoutFactory.getColorByPublishedStatus((MCArchivraetsel) pElement);
					}
				};
				break;
			}
		}
		return labelProviders;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractAufgabensammlungView#getColumnTitles()
	 */
	protected String[] getColumnTitles() {
		return columnTitles;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractAufgabensammlungView#getColumnBounds()
	 */
	protected int[] getColumnBounds() {
		return columnBounds;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.service.stammdaten.event.IModelChangeListener#modelChanged(de.egladil.mathejungalt.service
	 * .stammdaten .event.ModelChangeEvent)
	 */
	@Override
	public void modelChanged(ModelChangeEvent pEvt) {
		if (pEvt.getSource() instanceof MCArchivraetsel) {
			getViewer().refresh(true);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#initTableViewer(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected TableViewer initTableViewer(Composite pParent) {
		ITableViewColumn[] viewColumns = new ITableViewColumn[columnTitles.length];
		IViewerComparator[] comparators = getColumnComparators();
		ColumnLabelProvider[] labelProviders = getColumnLableProviders();
		for (int i = 0; i < columnTitles.length; i++) {
			viewColumns[i] = new ViewColumn(columnTitles[i], columnBounds[i], comparators[i], labelProviders[i]);
		}
		return TableViewerFactory.createTableViewer(pParent, new MCRaetselViewContentProvider(getStammdatenservice()),
			viewColumns, true);
	}
}
