/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.comparators;

import de.egladil.mathejungalt.domain.mitglieder.Mitglied;

/**
 * @author Winkelv
 */
public class MitgliederViewerComparator extends AbstractViewerComparator {

	/**
	 * @param pIndex
	 */
	public MitgliederViewerComparator(int pIndex) {
		super(pIndex);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Object pO1, Object pO2) {
		switch (getIndex()) {
		case 0: {
			if (((Mitglied) pO1).getVorname() == null && ((Mitglied) pO2).getVorname() == null)
				return 0;
			if (((Mitglied) pO1).getVorname() == null)
				return -1;
			if (((Mitglied) pO2).getVorname() == null)
				return 1;
			return (((Mitglied) pO1).getVorname().compareToIgnoreCase(((Mitglied) pO2).getVorname()));
		}
		case 1: {
			if (((Mitglied) pO1).getNachname() == null && ((Mitglied) pO2).getNachname() == null)
				return 0;
			if (((Mitglied) pO1).getNachname() == null)
				return -1;
			if (((Mitglied) pO2).getNachname() == null)
				return 1;
			return (((Mitglied) pO1).getNachname().compareToIgnoreCase(((Mitglied) pO2).getNachname()));
		}

		case 2: {
			if (((Mitglied) pO1).getKlasse() == null && ((Mitglied) pO2).getKlasse() == null)
				return 0;
			if (((Mitglied) pO1).getKlasse() == null)
				return -1;
			if (((Mitglied) pO2).getKlasse() == null)
				return 1;
			return ((Mitglied) pO1).getKlasse().intValue() - ((Mitglied) pO2).getKlasse().intValue();
		}

		case 3: {
			if (((Mitglied) pO1).getAlter() == null && ((Mitglied) pO2).getAlter() == null)
				return 0;
			if (((Mitglied) pO1).getAlter() == null)
				return -1;
			if (((Mitglied) pO2).getAlter() == null)
				return 1;
			return ((Mitglied) pO1).getAlter().intValue() - ((Mitglied) pO2).getAlter().intValue();
		}
		default:
			if (((Mitglied) pO1).getKontakt() == null && ((Mitglied) pO2).getKontakt() == null)
				return 0;
			if (((Mitglied) pO1).getKontakt() == null)
				return -1;
			if (((Mitglied) pO2).getKontakt() == null)
				return 1;
			return (((Mitglied) pO1).getKontakt().compareToIgnoreCase(((Mitglied) pO2).getKontakt()));
		}
	}
}
