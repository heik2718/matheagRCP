/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors;

import java.beans.PropertyChangeEvent;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.PartInitException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.base.exceptions.MatheJungAltInconsistentEntityException;
import de.egladil.mathejungalt.domain.IMatheAGObjectNames;
import de.egladil.mathejungalt.domain.schulen.IKontaktNames;
import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.KontaktEditorInput;
import de.egladil.mathejungalt.ui.app.editors.pages.KontaktAttributesFormPage;

/**
 * @author Winkelv
 */
public class KontaktEditor extends AbstractMatheEditor {

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(KontaktEditor.class);

	/** */
	public static final String ID = "de.egladil.mathejungalt.ui.app.editors.KontaktEditor";

	/**
	 *
	 */
	public KontaktEditor() {
		super();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.AbstractMatheEditor#checkEditorInput(org.eclipse.ui.IEditorInput)
	 */
	@Override
	protected void checkEditorInput(IEditorInput pInput) throws PartInitException {
		if (!(pInput instanceof KontaktEditorInput)) {
			throw new PartInitException("Ungültiger Input: muss KontaktEditorInput sein!");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.AbstractMatheEditor#interestingPropertyChangeEvent(java.beans.PropertyChangeEvent)
	 */
	@Override
	protected boolean interestingPropertyChangeEvent(PropertyChangeEvent pEvt) {
		String property = pEvt.getPropertyName();
		Assert.isNotNull(property);
		if (property.equals(IMatheAGObjectNames.PROP_SCHLUESSEL))
			return true;
		if (property.equals(IMatheAGObjectNames.PROP_ID))
			return true;
		if (property.equals(IKontaktNames.PROP_NAME))
			return true;
		if (property.equals(IKontaktNames.PROP_EMAIL))
			return true;
		if (property.equals(IKontaktNames.PROP_ABO))
			return true;
		if (property.equals(IKontaktNames.PROP_SCHULKONTAKTE))
			return true;
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.AbstractMatheEditor#isTitleChangingEvent(java.beans.PropertyChangeEvent)
	 */
	@Override
	protected boolean isTitleChangingEvent(PropertyChangeEvent pEvt) {
		String property = pEvt.getPropertyName();
		Assert.isNotNull(property);
		if (property.equals(IKontaktNames.PROP_NAME)) {
			return true;
		}
		if (property.equals(IMatheAGObjectNames.PROP_ID)) {
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.AbstractMatheEditor#updateTitel()
	 */
	@Override
	protected void updateTitel() {
		Kontakt kontakt = (Kontakt) getDomainObject();
		if (kontakt != null && !kontakt.isNew()) {
			setPartName(kontakt.getName());
		} else {
			setPartName("neuer Kontakt");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.FormEditor#addPages()
	 */
	@Override
	protected void addPages() {
		try {
			int index = addPage(new KontaktAttributesFormPage(this, getClass().getName(), "Kontakt"));
			setPageText(index, "Kontakt");
		} catch (PartInitException e) {
			String msg = "Fehler beim Hinzufuegen von Seiten zum " + getClass().getName();
			LOG.error(msg);
			throw new MatheJungAltException(msg, e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.EditorPart#doSave(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	public void doSave(IProgressMonitor pMonitor) {
		commitPages(true);
		Kontakt kontakt = (Kontakt) getDomainObject();
		try {
			getStammdatenservice().kontaktSpeichern(kontakt);
			((KontaktEditorInput) getEditorInput()).applyChanges();
			setModified(false);
		} catch (MatheJungAltInconsistentEntityException e) {
			setModified(true);
			MessageDialog.openWarning(Display.getCurrent().getActiveShell(), "Warnung", e.getMessage());
		} catch (Exception e) {
			setModified(true);
			String msg = "Der Kontakt konnte wegen eines MatheAG-Core-Fehlers nicht gespeichert werden:\n"
				+ e.getMessage();
			LOG.error(msg, e);
			MessageDialog.openError(Display.getCurrent().getActiveShell(), "Fehler!", msg);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.AbstractMatheEditor#getId()
	 */
	@Override
	public String getId() {
		return ID;
	}
}
