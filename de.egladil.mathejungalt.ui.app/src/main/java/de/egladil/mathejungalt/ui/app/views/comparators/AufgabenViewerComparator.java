/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.comparators;

import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;

/**
 * @author Winkelv
 */
public class AufgabenViewerComparator extends AbstractViewerComparator {

	/**
	 * @param pIndex
	 */
	public AufgabenViewerComparator(int pIndex) {
		super(pIndex);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Object pO1, Object pO2) {
		switch (getIndex()) {
		case 1: {
			return ((Aufgabe) pO1).getZweck().toString().compareToIgnoreCase(((Aufgabe) pO2).getZweck().toString());
		}
		case 2: {
			return ((Aufgabe) pO1).getStufe().intValue() - ((Aufgabe) pO2).getStufe().intValue();
		}
		case 3: {
			return ((Aufgabe) pO1).getThema().toString().compareToIgnoreCase(((Aufgabe) pO2).getThema().toString());
		}
		case 4: {
			return ((Aufgabe) pO1).getTitel().compareToIgnoreCase(((Aufgabe) pO2).getTitel());
		}
		default:
			return ((Aufgabe) pO1).getSchluessel().compareToIgnoreCase(((Aufgabe) pO2).getSchluessel());
		}
	}
}
