/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.filters;

import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.jface.viewers.ViewerFilter;

import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * Filter, der mit genauem Treffer selektiert (keine Wildcards und Beachtung der Gro�-Kleinschreibung.
 * 
 * @author Winkelv
 */
public abstract class AbstractMatheAGObjectFilter extends ViewerFilter {

	private final StructuredViewer viewer;

	private String pattern = "";

	private StringMatcher stringMatcher;

	/** */
	private IStammdatenservice stammdatenservice;

	/**
	 * 
	 */
	public AbstractMatheAGObjectFilter(final StructuredViewer pViewer, IStammdatenservice pStammdatenservice) {
		viewer = pViewer;
		stammdatenservice = pStammdatenservice;
	}

	/**
	 * @return the pattern
	 */
	public String getPattern() {
		return pattern;
	}

	/**
	 * Entspricht setPattern(pPattern, false, true), d.h. Texte müssen mit dem Muster exakt Übereinstimmen. Das ist eine
	 * convenience-Methode für den am häufigsten vorkommenden Fall.
	 * 
	 * @param pPattern the pattern to set
	 */
	public void setPattern(String pPattern) {
		setPattern(pPattern, false, true);
	}

	/**
	 * Treffertexte müssen mit dem gegebenen Muster pPattern �bereinstimmen. Das Muster kann wildcards '*' für 0 bis
	 * viele Zeichen und '?' für genau ein Zeichen enthalten. '*' and '?' müssen im Muster maskiert werden, wenn sie
	 * auch auftauchen können, z.B., "\*" steht fuer das Zeichen "*", usw..
	 * 
	 * @param pPattern String das Muster, gegen das der Text geprüft werden soll.
	 * @param pIgnoreCase boolean falls true dann ohne Beachtung von Groß-Kleinschreibung
	 * @param pIgnoreEscapeChars boolean - falls true, werden maskierte Zeichen und ihre Maskierungen ignoriert (also
	 * genaue Treffer der Zeichenkette).
	 */
	public void setPattern(String pPattern, boolean pIgnoreCase, boolean pIgnoreEscapeChars) {
		boolean filtering = stringMatcher != null;
		if (pPattern != null && pPattern.trim().length() > 0) {
			pattern = pPattern;
			stringMatcher = new StringMatcher(pattern, pIgnoreCase, pIgnoreEscapeChars);
			if (!filtering)
				viewer.addFilter(this);
			else
				viewer.refresh();
		} else {
			pattern = "";
			stringMatcher = null;
			if (filtering) {
				viewer.removeFilter(this);
			}
		}

		pattern = pPattern;
	}

	/**
	 * @return the stringMatcher
	 */
	protected StringMatcher getStringMatcher() {
		return stringMatcher;
	}

	/**
	 * @return the stammdatenservice
	 */
	protected IStammdatenservice getStammdatenservice() {
		return stammdatenservice;
	}
}
