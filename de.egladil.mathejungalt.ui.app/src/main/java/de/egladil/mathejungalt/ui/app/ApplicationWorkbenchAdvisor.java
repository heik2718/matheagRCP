package de.egladil.mathejungalt.ui.app;

import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchAdvisor;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;

public class ApplicationWorkbenchAdvisor extends WorkbenchAdvisor {

	private static final String INITIAL_PERSPECTIVE_ID = "de.egladil.mathejungalt.ui.app.stammdatenperspective";

	// private static final String INITIAL_PERSPECTIVE_ID = "de.egladil.mathejungalt.ui.app.quizarchivPerspective";

	public WorkbenchWindowAdvisor createWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
		return new ApplicationWorkbenchWindowAdvisor(configurer);
	}

	public String getInitialWindowPerspectiveId() {
		return INITIAL_PERSPECTIVE_ID;
	}

}
