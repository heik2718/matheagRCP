/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors;

import java.beans.PropertyChangeEvent;

import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.PartInitException;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.ISerieNames;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.SerieEditorInput;
import de.egladil.mathejungalt.ui.app.editors.pages.SerieAttributesFormPage;

/**
 * @author winkelv
 */
public class SerieEditor extends AbstractAufgabensammlungEditor {

	/** */
	public static final String ID = SerieEditor.class.getName();

	/**
	 *
	 */
	public SerieEditor() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.AbstractMatheAGEditor#checkEditorInput(org.eclipse.ui.IEditorInput)
	 */
	@Override
	protected void checkEditorInput(IEditorInput pInput) throws PartInitException {
		if (!(pInput instanceof SerieEditorInput))
			throw new PartInitException("Ungültiger Input: muss SerieEditorInput sein!");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.AbstractMatheAGEditor#isAnEditorTitleChangingEvent(java.beans.PropertyChangeEvent)
	 */
	@Override
	protected boolean isTitleChangingEvent(PropertyChangeEvent pEvt) {
		String property = pEvt.getPropertyName();
		return property.equals(ISerieNames.PROP_NUMMER);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.rcp.editors.AbstractMatheAGEditor#isNotAnInterestingPropertyChangeEvent(java.beans.
	 * PropertyChangeEvent)
	 */
	@Override
	protected boolean interestingPropertyChangeEvent(PropertyChangeEvent pEvt) {
		if (super.interestingPropertyChangeEvent(pEvt)) {
			return true;
		}
		String prop = pEvt.getPropertyName();
		return ISerieNames.PROP_DATUM.equals(prop) || ISerieNames.PROP_MONAT_JAHR_TEXT.equals(prop)
			|| ISerieNames.PROP_NUMMER.equals(prop) || ISerieNames.PROP_RUNDE.equals(prop)
			|| ISerieNames.PROP_SERIENITEMS.equals(prop);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.AbstractMatheAGEditor#updateTitel()
	 */
	@Override
	protected void updateTitel() {
		Serie serie = (Serie) getDomainObject();
		setPartName(serie.getNummer() == null ? "neue Serie" : "Serie " + serie.getNummer());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.FormEditor#addPages()
	 */
	@Override
	protected void addPages() {
		try {
			int index = addPage(new SerieAttributesFormPage(this, getClass().getName(), "Serie"));
			setPageText(index, "Serie");
		} catch (PartInitException e) {
			throw new MatheJungAltException(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.AbstractMatheEditor#getId()
	 */
	@Override
	public String getId() {
		return ID;
	}
}
