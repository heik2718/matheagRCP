/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.converters;

import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.domain.types.EnumTypes.Jahreszeit;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Konvertiert einen String in eine {@link Jahreszeit}
 * 
 * @author Winkelv
 */
public class StringToJahreszeitConverter implements IConverter {

	/**
	 * 
	 */
	public StringToJahreszeitConverter() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object pFromObject) {
		String param = (String) pFromObject;
		if (ITextConstants.JAHRESZEIT_FRUEH.equals(param)) {
			return Jahreszeit.F;
		}
		if (ITextConstants.JAHRESZEIT_HERBST.equals(param)) {
			return Jahreszeit.H;
		}
		if (ITextConstants.JAHRESZEIT_KARNEVAL.equals(param)) {
			return Jahreszeit.K;
		}
		if (ITextConstants.JAHRESZEIT_OSTERN.equals(param)) {
			return Jahreszeit.O;
		}
		if (ITextConstants.JAHRESZEIT_SOMMERR.equals(param)) {
			return Jahreszeit.S;
		}
		if (ITextConstants.JAHRESZEIT_WINTER.equals(param)) {
			return Jahreszeit.W;
		}
		if (ITextConstants.JAHRESZEIT_XMAS.equals(param)) {
			return Jahreszeit.X;
		}
		return Jahreszeit.I;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getFromType()
	 */
	@Override
	public Object getFromType() {
		return String.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getToType()
	 */
	@Override
	public Object getToType() {
		return Jahreszeit.class;
	}
}
