/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors;

import java.beans.PropertyChangeEvent;

import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.PartInitException;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.IArbeitsblattNames;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.ArbeitsblattEditorInput;
import de.egladil.mathejungalt.ui.app.editors.pages.ArbeitsblattAttributesFormPage;

/**
 * @author winkelv
 */
public class ArbeitsblattEditor extends AbstractAufgabensammlungEditor {

	/** */
	public static final String ID = ArbeitsblattEditor.class.getName();

	/**
	 *
	 */
	public ArbeitsblattEditor() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.AbstractMatheAGEditor#checkEditorInput(org.eclipse.ui.IEditorInput)
	 */
	@Override
	protected void checkEditorInput(IEditorInput pInput) throws PartInitException {
		if (!(pInput instanceof ArbeitsblattEditorInput))
			throw new PartInitException("Ungültiger Input: muss ArbeitsblattEditorInput sein!");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathe.rcp.editors.AbstractMatheAGEditor#isAnEditorTitleChangingEvent(java.beans.PropertyChangeEvent)
	 */
	@Override
	protected boolean isTitleChangingEvent(PropertyChangeEvent pEvt) {
		String property = pEvt.getPropertyName();
		return property.equals(IArbeitsblattNames.PROP_TITEL);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathe.rcp.editors.AbstractMatheAGEditor#isNotAnInterestingPropertyChangeEvent(java.beans.
	 * PropertyChangeEvent)
	 */
	@Override
	protected boolean interestingPropertyChangeEvent(PropertyChangeEvent pEvt) {
		if (super.interestingPropertyChangeEvent(pEvt)) {
			return true;
		}
		String prop = pEvt.getPropertyName();
		return IArbeitsblattNames.PROP_TITEL.equals(prop) || IArbeitsblattNames.PROP_ITEMS.equals(prop);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.AbstractMatheAGEditor#updateTitel()
	 */
	@Override
	protected void updateTitel() {
		Arbeitsblatt arbeitsblatt = (Arbeitsblatt) getDomainObject();
		setPartName(arbeitsblatt.getTitel() == null || arbeitsblatt.getTitel().isEmpty() ? "neues Arbeitsblatt"
			: arbeitsblatt.getTitel());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.editor.FormEditor#addPages()
	 */
	@Override
	protected void addPages() {
		try {
			int index = addPage(new ArbeitsblattAttributesFormPage(this, getClass().getName(), "Arbeitsblatt"));
			setPageText(index, "Arbeitsblatt");
		} catch (PartInitException e) {
			throw new MatheJungAltException(e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.AbstractMatheEditor#getId()
	 */
	@Override
	public String getId() {
		return ID;
	}
}
