/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.dnd;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;

import de.egladil.mathejungalt.ui.app.editors.pages.AbstractAttributesFormPage;

/**
 * @author Winkelv
 */
public class EditorAreaDragSourceAdapter extends AbstractDragSourceAdapter {

	/** */
	private AbstractAttributesFormPage formPage;

	/**
	 * 
	 */
	public EditorAreaDragSourceAdapter(AbstractAttributesFormPage pFormPage) {
		formPage = pFormPage;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.dnd.AbstractDragSourceAdapter#getSelection()
	 */
	@Override
	protected ISelection getSelection() {
		return new StructuredSelection(formPage.getDomainObject());
	}

}
