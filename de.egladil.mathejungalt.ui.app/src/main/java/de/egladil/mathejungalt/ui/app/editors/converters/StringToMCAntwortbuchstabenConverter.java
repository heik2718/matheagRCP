/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.converters;

import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.domain.mcraetsel.MCAntwortbuchstaben;

/**
 * Konvertiert einen String in ein {@link MCAntwortbuchstaben}
 *
 * @author Winkelv
 */
public class StringToMCAntwortbuchstabenConverter implements IConverter {

	/**
	 *
	 */
	public StringToMCAntwortbuchstabenConverter() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object pFromObject) {
		String param = (String) pFromObject;
		return param == null || param.isEmpty() ? null : MCAntwortbuchstaben.valueOf(param);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getFromType()
	 */
	@Override
	public Object getFromType() {
		return String.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getToType()
	 */
	@Override
	public Object getToType() {
		return MCAntwortbuchstaben.class;
	}
}
