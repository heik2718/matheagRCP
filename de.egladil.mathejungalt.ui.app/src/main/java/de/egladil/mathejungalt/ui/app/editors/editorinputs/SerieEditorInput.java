/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.editorinputs;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.ui.app.editors.kopierer.IKopierer;
import de.egladil.mathejungalt.ui.app.editors.kopierer.SerieKopierer;

/**
 * @author winkelv
 */
public class SerieEditorInput extends AbstractAufgabensammlungEditorInput {

	/**
	 * @param pObject
	 */
	public SerieEditorInput(AbstractMatheAGObject pObject) {
		super(pObject);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.editorinputs.AbstractMatheEditorInput#getKopierer()
	 */
	@Override
	public IKopierer getKopierer() {
		return new SerieKopierer();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.editors.editorinputs.AbstractAufgabensammlungEditorInput#getCacheTarget()
	 */
	@Override
	public AbstractMatheAGObject getCacheTarget() {
		return new Serie();
	}
}
