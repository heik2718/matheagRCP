/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.comparators;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;

import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;

/**
 * @author winkelv
 */
public class SerienteilnahmenViewerComparator extends ViewerComparator {

	/**
	 * 
	 */
	public SerienteilnahmenViewerComparator() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ViewerComparator#compare(org.eclipse.jface.viewers.Viewer, java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public int compare(Viewer pViewer, Object pE1, Object pE2) {
		if (pE1 instanceof Serie && pE2 instanceof Serie) {
			Serie s1 = (Serie) pE1;
			Serie s2 = (Serie) pE2;
			return s1.getNummer().intValue() - s2.getNummer().intValue();
		}
		return super.compare(pViewer, pE1, pE2);
	}
}
