/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import de.egladil.mathejungalt.domain.quellen.Zeitschriftquelle;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Die Detailspage für die Attribute dser {@link Zeitschriftquelle}
 * 
 * @author winkelv
 */
public class ZeitschriftquelleDetailsPage extends AbstractQuelleDetailsPage {

	/**
	 * @param pMaster
	 */
	public ZeitschriftquelleDetailsPage(AbstractMasterDetailsBlock pMaster) {
		super(pMaster);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractDetailsPage#getAttributesPart()
	 */
	@Override
	protected MatheAGObjectAttributesPart getAttributesPart() {
		ZeitschriftquelleSimpleAttributePartDataBindingProvider contentsProvider = new ZeitschriftquelleSimpleAttributePartDataBindingProvider();
		return new MatheAGObjectAttributesPart(false, contentsProvider);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractDetailsPage#getHeader()
	 */
	@Override
	protected String getHeader() {
		return ITextConstants.SECTION_HEADER_ZEITSCHIFTQUELLE_ATTRIBUTES;
	}
}
