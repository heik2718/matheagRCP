/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.contentproviders;

import java.util.ArrayList;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

import de.egladil.mathejungalt.domain.schulen.IMinikaenguruTeilnahmeNames;
import de.egladil.mathejungalt.domain.schulen.ISchuleNames;
import de.egladil.mathejungalt.domain.schulen.Schule;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.editors.listeners.MinikaenguruTeilnahmenViewerListener;

/**
 * ContentProvider für die Minikaenguruteilnahmen einer Schule
 * 
 * @author Heike Winkelvoß
 */
public class MiniteilnahmenViewContentProvider extends AbstractMatheObjectsViewContentProvider implements
	IStructuredContentProvider {

	/**
	 * 
	 */
	public MiniteilnahmenViewContentProvider(IStammdatenservice pStammdatenservice) {
		super(pStammdatenservice);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
	 */
	@Override
	public Object[] getElements(Object pInputElement) {
		if (pInputElement == null || !(pInputElement instanceof Schule)) {
			return new ArrayList<MinikaenguruTeilnahmenViewerListener>().toArray();
		}
		Schule schule = (Schule) pInputElement;
		if (!schule.isMiniTeilnahmenLoaded()) {
			getStammdatenservice().minikaenguruTeilnahmenLaden(schule);
		}
		return schule.getMinikaenguruTeilnahmen().toArray();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.ui.app.views.contentproviders.AbstractMatheObjectsViewContentProvider#
	 * isInterestingChangeEvent(java.beans.PropertyChangeEvent)
	 */
	@Override
	public boolean isInterestingChangeEvent(String pPropertyName) {
		if (IMinikaenguruTeilnahmeNames.PROP_RUECKMELDUNG.equals(pPropertyName)
			|| ISchuleNames.PROP_MINI_TEILNAHMEN.equals(pPropertyName)) {
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
	 */
	@Override
	public void dispose() {
		// nix
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public void inputChanged(Viewer pArg0, Object pArg1, Object pArg2) {
		pArg0.refresh();
	}
}
