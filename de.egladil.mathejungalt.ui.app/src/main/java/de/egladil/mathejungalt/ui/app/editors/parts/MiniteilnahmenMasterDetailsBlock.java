/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import org.eclipse.ui.forms.DetailsPart;
import org.eclipse.ui.forms.IDetailsPage;

import de.egladil.mathejungalt.domain.schulen.MinikaenguruTeilnahme;
import de.egladil.mathejungalt.ui.app.editors.pages.SchuleAttributesFormPage;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * @author Heike Winkelvoß
 */
public class MiniteilnahmenMasterDetailsBlock extends AbstractMasterDetailsBlockWithViewerPart {

	/** */
	private AbstractDetailsPage detailsPage;

	/**
	 * @param pParentPage
	 */
	public MiniteilnahmenMasterDetailsBlock(SchuleAttributesFormPage pParentPage) {
		super(pParentPage);
		detailsPage = new MiniteilnahmeDetailsPage(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractMasterDetailsBlock#getSectionDescription()
	 */
	@Override
	protected String getSectionDescription() {
		return ITextConstants.SECTION_DESCR_MINITEILNAHMENVIEWER;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractMasterDetailsBlock#getWidthHint()
	 */
	@Override
	protected int getWidthHint() {
		return 150;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractMasterDetailsBlock#getSectionHeader()
	 */
	@Override
	protected String getSectionHeader() {
		return ITextConstants.SECTION_HEADER_SCHULE_MINI;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractMasterDetailsBlock#initViewerPart()
	 */
	@Override
	protected void hookViewerPart() {
		setViewerPart(new MiniteilnahmenViewerPartWithButtons(this));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.MasterDetailsBlock#registerPages(org.eclipse.ui.forms.DetailsPart)
	 */
	@Override
	protected void registerPages(DetailsPart pDetailsPart) {
		pDetailsPart.setPageProvider(this);
		pDetailsPart.registerPage(MinikaenguruTeilnahme.class, detailsPage);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.IDetailsPageProvider#getPage(java.lang.Object)
	 */
	@Override
	public IDetailsPage getPage(Object pKey) {
		if (MinikaenguruTeilnahme.class.equals(pKey)) {
			return detailsPage;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.IDetailsPageProvider#getPageKey(java.lang.Object)
	 */
	@Override
	public Object getPageKey(Object pObject) {
		if (pObject instanceof MinikaenguruTeilnahme) {
			return MinikaenguruTeilnahme.class;
		}
		return null;
	}
}
