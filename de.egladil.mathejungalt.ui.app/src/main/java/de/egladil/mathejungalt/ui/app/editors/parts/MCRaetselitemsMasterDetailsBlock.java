/**
 *
 */
package de.egladil.mathejungalt.ui.app.editors.parts;

import org.eclipse.ui.forms.DetailsPart;
import org.eclipse.ui.forms.IDetailsPage;

import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.ui.app.editors.dnd.MCRaetselitemsDropTargetListener;
import de.egladil.mathejungalt.ui.app.editors.pages.AbstractAttributesFormPage;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * @author Heike Winkelvoß
 */
public class MCRaetselitemsMasterDetailsBlock extends AbstractMasterDetailsBlockWithViewerPart {

	/** */
	private AbstractDetailsPage detailsPage;

	/**
	 * @param pParentPage
	 */
	public MCRaetselitemsMasterDetailsBlock(AbstractAttributesFormPage pParentPage) {
		super(pParentPage);
		detailsPage = new MCRaetselitemDetailsPage(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractMasterDetailsBlock#getSectionDescription()
	 */
	@Override
	protected String getSectionDescription() {
		return ITextConstants.SECTION_DESCR_AUFGABENSAMMLUNGITEMSVIEWER;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractMasterDetailsBlock#getWidthHint()
	 */
	@Override
	protected int getWidthHint() {
		return 150;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractMasterDetailsBlock#getSectionHeader()
	 */
	@Override
	protected String getSectionHeader() {
		return ITextConstants.SECTION_HEADER_AUFGABENSAMMLUNG_AUFGABE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractMasterDetailsBlock#initViewerPart()
	 */
	@Override
	protected void hookViewerPart() {
		setViewerPart(new MCRaetselitemsViewerPartWithButtons(this));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractMasterDetailsBlockWithViewerPart#hookDropTargetListener()
	 */
	@Override
	protected void hookDropTargetListener() {
		MCArchivraetsel raetsel = (MCArchivraetsel) getParentPage().getDomainObject();
		if (!raetsel.isVeroeffentlicht()) {
			setDropTargetListener(new MCRaetselitemsDropTargetListener(raetsel));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.MasterDetailsBlock#registerPages(org.eclipse.ui.forms.DetailsPart)
	 */
	@Override
	protected void registerPages(DetailsPart pDetailsPart) {
		pDetailsPart.setPageProvider(this);
		pDetailsPart.registerPage(IAufgabensammlungItem.class, detailsPage);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.editors.parts.AbstractMasterDetailsBlockWithViewerPart#dispose()
	 */
	@Override
	public void dispose() {
		detailsPage.dispose();
		super.dispose();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.IDetailsPageProvider#getPage(java.lang.Object)
	 */
	@Override
	public IDetailsPage getPage(Object pKey) {
		if (MCArchivraetselItem.class.equals(pKey)) {
			return detailsPage;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.forms.IDetailsPageProvider#getPageKey(java.lang.Object)
	 */
	@Override
	public Object getPageKey(Object pObject) {
		if (pObject instanceof MCArchivraetselItem) {
			return MCArchivraetselItem.class;
		}
		return null;
	}
}
