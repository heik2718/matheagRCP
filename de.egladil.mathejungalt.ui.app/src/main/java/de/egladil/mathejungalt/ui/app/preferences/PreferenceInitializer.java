/**
 *
 */
package de.egladil.mathejungalt.ui.app.preferences;

import java.util.Calendar;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import de.egladil.mathejungalt.ui.app.MatheJungAltActivator;

/**
 * @author aheike
 */
public class PreferenceInitializer extends AbstractPreferenceInitializer {

	/**
	 *
	 */
	public PreferenceInitializer() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	@Override
	public void initializeDefaultPreferences() {
		IPreferenceStore store = MatheJungAltActivator.getDefault().getPreferenceStore();
		store.setDefault(PreferenceConstants.P_PATH_AUFGABENARCHIV_DIR, PreferenceConstants.DEFAULT_AUFGABENARCHIV);
		store.setDefault(PreferenceConstants.P_PATH_LOESUNGENARCHIV_DIR, PreferenceConstants.DEFAULT_LOESUNGSARCHIV);
		store.setDefault(PreferenceConstants.P_PATH_PNG_DIR, PreferenceConstants.DEFAULT_PNGS);
		store.setDefault(PreferenceConstants.P_PATH_GENERATOR_OUT_DIR, PreferenceConstants.DEFAULT_GENERATED);
		store.setDefault(PreferenceConstants.P_PATH_QUIZAUFGABENARCHIV_DIR,
			PreferenceConstants.DEFAULT_MC_AUFGABENARCHIV);
		store.setDefault(PreferenceConstants.P_PATH_QUIZLOESUNGENARCHIV_DIR,
			PreferenceConstants.DEFAULT_MC_LOESUNGSARCHIV);
		store.setDefault(PreferenceConstants.P_PATH_QUIZ_PNG_DIR, PreferenceConstants.DEFAULT_QUIZPNGS);
		store.setDefault(PreferenceConstants.P_RAETSEL_GENERATOR_MODUS,
			PreferenceConstants.DEFAULT_RAETSEL_GENERATOR_MODUS);
		store.setDefault(PreferenceConstants.P_STUFE_6_SPERREN, "false");
		store.setDefault(PreferenceConstants.P_AKTUELLES_JAHR_MINIKAENGURU, aktuellesJahr() + "");
	}

	private int aktuellesJahr() {
		return Calendar.getInstance().get(Calendar.YEAR);
	}
}
