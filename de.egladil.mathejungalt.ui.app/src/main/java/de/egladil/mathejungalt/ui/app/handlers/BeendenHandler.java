/**
 * 
 */
package de.egladil.mathejungalt.ui.app.handlers;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.ui.PlatformUI;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * @author aheike
 */
public class BeendenHandler extends AbstractSelectionHandler {

	/** */
	private IStammdatenservice stammdatenservice;

	/**
	 * @param pSelectionProvider
	 */
	public BeendenHandler(ISelectionProvider pSelectionProvider, IStammdatenservice pStammdatenservice) {
		super(pSelectionProvider);
		stammdatenservice = pStammdatenservice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.handlers.AbstractSelectionHandler#run()
	 */
	@Override
	protected void run() {
		AbstractMatheAGObject object = getFirstSelectedObject();
		try {
			if (object instanceof IAufgabensammlung) {
				final IAufgabensammlung aufgabensammlung = (IAufgabensammlung) object;
				if (IAufgabensammlung.NICHT_BEENDET == aufgabensammlung.getBeendet()) {
					aufgabensammlung.setBeendet(IAufgabensammlung.BEENDET);
				} else {
					aufgabensammlung.setBeendet(IAufgabensammlung.NICHT_BEENDET);
				}
				stammdatenservice.aufgabensammlungSpeichern(aufgabensammlung);
			}
		} catch (Exception e) {
			MessageDialog.openError(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
				"Numerierungsfehler", e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.handlers.AbstractSelectionHandler#isEnabled()
	 */
	@Override
	public boolean isEnabled() {
		if (!super.isEnabled()) {
			return false;
		}
		AbstractMatheAGObject firstSelectedObject = getFirstSelectedObject();
		if (!(firstSelectedObject instanceof IAufgabensammlung)) {
			return false;
		}
		final IAufgabensammlung aufgabensammlung = (IAufgabensammlung) firstSelectedObject;
		return aufgabensammlung.getBeendet() == IAufgabensammlung.BEENDET
			|| aufgabensammlung.getBeendet() != IAufgabensammlung.BEENDET && aufgabensammlung.anzahlAufgaben() > 0;
	}
}
