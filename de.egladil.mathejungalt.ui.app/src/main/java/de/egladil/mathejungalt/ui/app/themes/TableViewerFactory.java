/**
 * 
 */
package de.egladil.mathejungalt.ui.app.themes;

import javax.swing.table.TableColumn;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import de.egladil.mathejungalt.ui.app.views.ITableViewColumn;
import de.egladil.mathejungalt.ui.app.views.comparators.AbstractViewerComparator;
import de.egladil.mathejungalt.ui.app.views.comparators.GenericTableViewSorter;
import de.egladil.mathejungalt.ui.app.views.contentproviders.IStructuredContentProviderWithTableViewer;

/**
 * Eine Factory für einen TableViewer
 * 
 * @author Winkelv
 */
public final class TableViewerFactory {

	/**
	 * 
	 */
	private TableViewerFactory() {
	}

	/**
	 * Erzeugt einen {@link TableViewer} mit Überschriftenzeile, der nach den Überschriftattributen sortiert werden
	 * kann. Auswahlmodus ist Multiselection.
	 * 
	 * @param pParent {@link Composite} der Container (darf nicht null sein)
	 * @param pContentProvider {@link IStructuredContentProviderWithTableViewer} der ContentProvider (darf nicht null
	 * sein)
	 * @param pColumnTitles String[] die Spaltenüberschriften. (darf nicht null sein)
	 * @param pColumnBounds int[] die Spaltenbreitenvorschläge (darf nicht null sein)
	 * @param pComparators {@link AbstractViewerComparator}[] oder null. Falls nicht null, dann kann die Tabelle nach
	 * den Attributen in der Spalte sortiert werden
	 * @param pColumnLabelProviders {@link ColumnLabelProvider}[] die LabelProvider für die einzelnen
	 * {@link TableColumn}s (darf nicht null sein)
	 * @param pHeaderVisible boolean
	 * @return {@link TableViewer}
	 */
	public static TableViewer createTableViewer(Composite pParent, IContentProvider pContentProvider,
		ITableViewColumn[] pViewColumns, boolean pHeaderVisible) {
		TableViewer viewer = new TableViewer(pParent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION);
		viewer.getTable().setHeaderVisible(pHeaderVisible);
		viewer.getTable().setLinesVisible(false);

		TableViewerColumn[] tableViewerColumns = createColumns(viewer, pViewColumns);

		if (pHeaderVisible) {
			createTableSorter(viewer, tableViewerColumns, pViewColumns);
		}
		viewer.setContentProvider(pContentProvider);
		return viewer;
	}

	/**
	 * Erstellt aus den ITableViewColumn und den bounds {@link TableViewerColumn}s für den gegebenen {@link TableViewer}
	 * 
	 * @param pViewer
	 * @param pViewColumns
	 */
	private static TableViewerColumn[] createColumns(TableViewer pViewer, ITableViewColumn[] pViewColumns) {
		TableViewerColumn[] columns = new TableViewerColumn[pViewColumns.length];
		for (int i = 0; i < pViewColumns.length; i++) {
			columns[i] = new TableViewerColumn(pViewer, SWT.LEFT);
			columns[i].getColumn().setText(pViewColumns[i].getTitle());
			columns[i].getColumn().setWidth(pViewColumns[i].getBound());
			columns[i].getColumn().setMoveable(true);
			columns[i].setLabelProvider(pViewColumns[i].getLabelProvider());
		}
		return columns;
	}

	/**
	 * Dem {@link TableViewer} in pView wird eine Sortierung injeziert.
	 * 
	 * @param pViewer
	 */
	private static void createTableSorter(final TableViewer pViewer, TableViewerColumn[] pTableColumns,
		ITableViewColumn[] pViewColumns) {
		GenericTableViewSorter sorter = new GenericTableViewSorter(pViewer, pTableColumns, pViewColumns);
		pViewer.setSorter(sorter);
	}
}
