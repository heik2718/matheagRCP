/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.viewactions;

import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenzweck;

/**
 * Blendet die Aufgaben aus, die Zitate sind
 * 
 * @author Winkelv
 */
public class AusblendenSerienaufgabenAction extends AbstractAusblendenAufgabenzweckAction {

	/**
	 * 
	 */
	public AusblendenSerienaufgabenAction() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.rcp.views.actions.AusblendenAufgabenAction#getSpecialPattern()
	 */
	@Override
	protected String getSpecialPattern() {
		return Aufgabenzweck.S.toString();
	}
}
