/**
 * 
 */
package de.egladil.mathejungalt.ui.app.handlers;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.ui.PlatformUI;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;

/**
 * @author aheike
 */
public class ToggleSperrendHandler extends AbstractSelectionHandler {

	/** */
	private IStammdatenservice stammdatenservice;

	/**
	 * @param pSelectionProvider
	 */
	public ToggleSperrendHandler(ISelectionProvider pSelectionProvider, IStammdatenservice pStammdatenservice) {
		super(pSelectionProvider);
		stammdatenservice = pStammdatenservice;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.handlers.AbstractSelectionHandler#run()
	 */
	@Override
	protected void run() {
		AbstractMatheAGObject object = getFirstSelectedObject();
		try {
			if (object instanceof Arbeitsblatt) {
				Arbeitsblatt arbblatt = (Arbeitsblatt) object;
				if (arbblatt.getSperrend() == Arbeitsblatt.NICHT_SPERREND) {
					arbblatt.setSperrend(Arbeitsblatt.SPERREND);
				} else {
					arbblatt.setSperrend(Arbeitsblatt.NICHT_SPERREND);
				}
				stammdatenservice.aufgabensammlungSpeichern(arbblatt);
				return;
			}
		} catch (Exception e) {
			MessageDialog.openError(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
				"Numerierungsfehler", e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.handlers.AbstractSelectionHandler#isEnabled()
	 */
	@Override
	public boolean isEnabled() {
		if (!super.isEnabled()) {
			return false;
		}
		return getFirstSelectedObject() instanceof Arbeitsblatt;
	}
}
