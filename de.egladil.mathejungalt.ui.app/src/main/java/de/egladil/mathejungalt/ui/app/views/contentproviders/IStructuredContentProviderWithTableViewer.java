/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views.contentproviders;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.TableViewer;

/**
 * Ein {@link IStructuredContentProvider} der zu einem {@link TableViewer} gehört.
 * 
 * @author winkelv
 */
public interface IStructuredContentProviderWithTableViewer extends IStructuredContentProvider {

	/**
	 * Setzt einen {@link TableViewer} der dieses Interface gleichzeitig als ContentProvider hat.
	 * 
	 * @param pViewer the viewer to set
	 */
	public abstract void setViewer(TableViewer pViewer);
}
