package de.egladil.mathejungalt.ui.app.views;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.graphics.Color;
import org.eclipse.ui.handlers.IHandlerService;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.ui.app.editors.SerieEditor;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.AbstractMatheEditorInput;
import de.egladil.mathejungalt.ui.app.editors.editorinputs.SerieEditorInput;
import de.egladil.mathejungalt.ui.app.handlers.ArchivseiteGenerierenHandler;
import de.egladil.mathejungalt.ui.app.handlers.ICommandIds;
import de.egladil.mathejungalt.ui.app.themes.LayoutFactory;
import de.egladil.mathejungalt.ui.app.views.comparators.AbstractViewerComparator;
import de.egladil.mathejungalt.ui.app.views.comparators.IViewerComparator;
import de.egladil.mathejungalt.ui.app.views.comparators.SerienViewerComparator;
import de.egladil.mathejungalt.ui.app.views.listeners.AbstractModelListener;
import de.egladil.mathejungalt.ui.app.views.listeners.SerienModelListener;

public class SerienView extends AbstractAufgabensammlungView {

	/** */
	public static final String ID = "de.egladil.mathejungalt.ui.app.serienview";

	/** */
	public static final int INDEX_NUMMER = 0;

	/** */
	public static final int INDEX_RUNDE = 1;

	/** */
	public static final int INDEX_MONAT_JAHR = 2;

	/** */
	private final String[] columnTitles = new String[] { "Nummer", "Runde", "Monat-Jahr" };

	/** */
	private final int[] columnBounds = new int[] { 80, 80, 160 };

	/**
	 * 
	 */
	public SerienView() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractAufgabensammlungView#createHandlers()
	 */
	@Override
	protected void createHandlers() {
		super.createHandlers();
		IHandlerService handlerService = (IHandlerService) getSite().getService(IHandlerService.class);
		handlerService.activateHandler(ICommandIds.ARCHIVSEITE_GENERIEREN, new ArchivseiteGenerierenHandler(
			getStammdatenservice()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#getEditorID()
	 */
	@Override
	protected String getEditorID() {
		return SerieEditor.ID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#getEditorInput(java.lang.Object)
	 */
	@Override
	protected AbstractMatheEditorInput getEditorInput(Object pObj) {
		if (!(pObj instanceof Serie)) {
			return null;
		}
		Serie serie = (Serie) pObj;
		if (!serie.isItemsLoaded()) {
			getStammdatenservice().aufgabensammlungitemsLaden(serie);
		}
		return new SerieEditorInput((AbstractMatheAGObject) pObj);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.ui.app.views.AbstractMatheJungAltView#getModelListener(org.eclipse.jface.viewers.TableViewer
	 * )
	 */
	@Override
	protected AbstractModelListener getModelListener(TableViewer pViewer) {
		return new SerienModelListener(pViewer);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractAufgabensammlungView#getColumnComparators()
	 */
	protected IViewerComparator[] getColumnComparators() {
		AbstractViewerComparator[] comparators = new SerienViewerComparator[3];
		for (int i = 0; i < comparators.length; i++) {
			comparators[i] = new SerienViewerComparator(i);
		}
		return comparators;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractAufgabensammlungView#getColumnLableProviders()
	 */
	protected ColumnLabelProvider[] getColumnLableProviders() {
		ColumnLabelProvider[] labelProviders = new ColumnLabelProvider[columnTitles.length];
		for (int i = 0; i < labelProviders.length; i++) {
			switch (i) {
			case SerienView.INDEX_NUMMER:
				labelProviders[i] = new ColumnLabelProvider() {
					@Override
					public String getText(Object pElement) {
						return ((Serie) pElement).getNummer() != null ? ((Serie) pElement).getNummer().toString() : "0";
					}

					/*
					 * (non-Javadoc)
					 * 
					 * @see org.eclipse.jface.viewers.ColumnLabelProvider#getForeground(java.lang.Object)
					 */
					@Override
					public Color getForeground(Object pElement) {
						return LayoutFactory.getColorByBeendetStatus((IAufgabensammlung) pElement);
					}
				};
				break;
			case SerienView.INDEX_RUNDE:
				labelProviders[i] = new ColumnLabelProvider() {
					@Override
					public String getText(Object pElement) {
						return ((Serie) pElement).getRunde() != null ? ((Serie) pElement).getRunde().toString() : "0";
					}

					/*
					 * (non-Javadoc)
					 * 
					 * @see org.eclipse.jface.viewers.ColumnLabelProvider#getForeground(java.lang.Object)
					 */
					@Override
					public Color getForeground(Object pElement) {
						return LayoutFactory.getColorByBeendetStatus((IAufgabensammlung) pElement);
					}
				};
				break;
			default:
				labelProviders[i] = new ColumnLabelProvider() {
					@Override
					public String getText(Object pElement) {
						return ((Serie) pElement).getMonatJahrText();
					}

					/*
					 * (non-Javadoc)
					 * 
					 * @see org.eclipse.jface.viewers.ColumnLabelProvider#getForeground(java.lang.Object)
					 */
					@Override
					public Color getForeground(Object pElement) {
						return LayoutFactory.getColorByBeendetStatus((IAufgabensammlung) pElement);
					}
				};
				break;
			}
		}
		return labelProviders;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractAufgabensammlungView#getColumnTitles()
	 */
	protected String[] getColumnTitles() {
		return columnTitles;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractAufgabensammlungView#getColumnBounds()
	 */
	protected int[] getColumnBounds() {
		return columnBounds;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.ui.app.views.AbstractAufgabensammlungView#getViewID()
	 */
	@Override
	protected String getViewID() {
		return ID;
	}
}
