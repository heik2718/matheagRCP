package de.egladil.mathejungalt.ui.app.editors.parts;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestMitRadio extends Composite {

	private static final Logger LOG = LoggerFactory.getLogger(TestMitRadio.class);

	private Text text;

	/**
	 * Create the composite.
	 * 
	 * @param parent
	 * @param style
	 */
	public TestMitRadio(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(2, false));

		Button btnRadioButton = new Button(this, SWT.RADIO);
		btnRadioButton.setText("Radio Button");

		Button btnRadioButton_1 = new Button(this, SWT.RADIO);
		btnRadioButton_1.setText("Radio Button");

		Label lblNewLabel = new Label(this, SWT.NONE);
		lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		lblNewLabel.setText("New Label");

		text = new Text(this, SWT.BORDER);
		text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Label lblNewLabel_1 = new Label(this, SWT.NONE);
		lblNewLabel_1.setText("New Label");

		Button btnCheckButton = new Button(this, SWT.CHECK);
		btnCheckButton.setText("");

	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}
