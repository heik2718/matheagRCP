/**
 *
 */
package de.egladil.mathejungalt.ui.app.views.filters;

import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.jface.viewers.Viewer;

import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;
import de.egladil.mathejungalt.service.stammdaten.IStammdatenservice;
import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Filter, der alle Objekte durchlässt, die NICHT das Muster treffen. Wenn das Muster leer ist, werden alle Objekte
 * durchgelassen.
 *
 * @author Winkelv
 */
public class MCAufgabeFuerRaetselGesperrtNegativeFilter extends AbstractMatheAGObjectFilter {

	/**
	 * @param pViewer
	 */
	public MCAufgabeFuerRaetselGesperrtNegativeFilter(StructuredViewer pViewer, IStammdatenservice pStammdatenservice) {
		super(pViewer, pStammdatenservice);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer, java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public boolean select(Viewer pViewer, Object pParentElement, Object pElement) {
		if (getPattern() == null || ITextConstants.FILTER_ACTION_ALLE.equals(getPattern())) {
			return true;
		}
		String gesperrt = getStammdatenservice().isGesperrtFuerRaetsel((MCAufgabe) pElement);
		return !getStringMatcher().match(gesperrt);
	}
}
