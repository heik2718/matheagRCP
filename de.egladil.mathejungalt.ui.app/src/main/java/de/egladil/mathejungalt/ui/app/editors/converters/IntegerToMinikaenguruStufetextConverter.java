/**
 * 
 */
package de.egladil.mathejungalt.ui.app.editors.converters;

import org.eclipse.core.databinding.conversion.IConverter;

import de.egladil.mathejungalt.ui.app.themes.ITextConstants;

/**
 * Konvertiert einen Integer in eine Stufe-Text
 * 
 * @author Winkelv
 */
public class IntegerToMinikaenguruStufetextConverter implements IConverter {

	/**
	 * 
	 */
	public IntegerToMinikaenguruStufetextConverter() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#convert(java.lang.Object)
	 */
	@Override
	public Object convert(Object pFromObject) {
		int param = ((Integer) pFromObject).intValue();
		switch (param) {
		case 3:
			return ITextConstants.PUNKTE_3;
		case 4:
			return ITextConstants.PUNKTE_4;
		case 5:
			return ITextConstants.PUNKTE_5;
		default:
			return "falsche Minik\u00E4nguru-Stufe" + param;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getFromType()
	 */
	@Override
	public Object getFromType() {
		return Integer.class;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.databinding.conversion.IConverter#getToType()
	 */
	@Override
	public Object getToType() {
		return String.class;
	}
}
