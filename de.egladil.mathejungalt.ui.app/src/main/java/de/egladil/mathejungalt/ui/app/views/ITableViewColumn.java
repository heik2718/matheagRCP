/**
 * 
 */
package de.egladil.mathejungalt.ui.app.views;

import org.eclipse.jface.viewers.ColumnLabelProvider;

import de.egladil.mathejungalt.ui.app.views.comparators.IViewerComparator;

/**
 * Stellt Attribute zur Darstellung einer Spalte eines TableViewers ereit.
 * 
 * @author aheike
 */
public interface ITableViewColumn {

	/**
	 * Titel der Spalte.
	 * 
	 * @return String
	 */
	String getTitle();

	/**
	 * Bevorzugte Spaltenbreite
	 * 
	 * @return int
	 */
	int getBound();

	/**
	 * @return {@link IViewerComparator} zum Sortieren.
	 */
	IViewerComparator getComparator();

	/**
	 * @return {@link ColumnLabelProvider}
	 */
	ColumnLabelProvider getLabelProvider();
}
