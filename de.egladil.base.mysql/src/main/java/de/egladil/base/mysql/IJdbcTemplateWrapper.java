/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.base.mysql;

import javax.sql.DataSource;

import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * @author Heike Winkelvoß (public@egladil.de)
 */
public interface IJdbcTemplateWrapper {

  public static final String JDBC_DB_NAME_KEY = "jdbc.database.name";
  public static final String JDBC_DRIVERCLASSNAME_KEY = "jdbc.driverClassName";
  public static final String JDBC_URL_KEY = "jdbc.url";
  public static final String JDBC_ENV_KEY = "jdbc.env";
  public static final String JDBC_USER_KEY = "jdbc.user";
  public static final String JDBC_PWD_KEY = "jdbc.pwd";
  public static final String JDBC_SHOW_SQL_KEY = "jdbc.showSql";
  public static final String JDBC_GENERATE_DDL_KEY = "jdbc.generateDdl";

  /**
   * Stellt ein JdbcTemplate zur Verfügung
   * 
   * @return
   */
  public SimpleJdbcTemplate getJdbcTemplate();

  /**
   * @return Ausgabe der aktuellen Konfiguration.
   */
  public String printConfiguration();

  /**
   * @return UserName der DB für Info.
   */
  public String getDBUserName();
  
  public String getDBUrl();

  /**
   * @return
   */
  public TransactionTemplate getTransactionTemplate();

  /**
   * @return
   */
  public DataSource getDataSource();

}
