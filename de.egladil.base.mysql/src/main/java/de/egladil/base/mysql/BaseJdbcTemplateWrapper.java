/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.base.mysql;

import java.sql.SQLException;
import java.util.Dictionary;
import java.util.Properties;

import javax.sql.DataSource;

import org.osgi.service.component.ComponentContext;
import org.osgi.service.jdbc.DataSourceFactory;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * <p>
 * NOTE: THIS WIDGET AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 * 
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class BaseJdbcTemplateWrapper implements IJdbcTemplateWrapper {

  /** */
  private SimpleJdbcTemplate jdbcTemplate;
  
  private TransactionTemplate transactionTemplate;

  private DataSource dataSource;  

  private Properties props;

  /**
   *
   */
  public BaseJdbcTemplateWrapper() {

  }

  @SuppressWarnings("rawtypes")
  protected void activate(ComponentContext pComponentContext) {
	// Hier liest die Service-Komponente die Konfiguration, die der Client in den
	// ComponentContext gesetzt hat.
	Dictionary dict = pComponentContext.getProperties();
	// Die URLs können in ein Dictionary gepackt werden, damit sie einfach an den AuthenticationService
	// durchgereicht werden können.
	props = new Properties();
	props.put(DataSourceFactory.JDBC_URL, (String) dict.get(IJdbcTemplateWrapper.JDBC_URL_KEY));
	props.put(DataSourceFactory.JDBC_USER, (String) dict.get(IJdbcTemplateWrapper.JDBC_USER_KEY));
	props.put(DataSourceFactory.JDBC_PASSWORD, (String) dict.get(IJdbcTemplateWrapper.JDBC_PWD_KEY));
	checkConfiguration();

	try {
	  dataSource = new MySQLJDBCDataSourceService().createDataSource(props);
	} catch (SQLException e) {
	  // TODO Auto-generated catch block
	  e.printStackTrace();
	}
  }

  /**
   * Prüft die Pflichtattribute.
   */
  private void checkConfiguration() {
	if (props.get(DataSourceFactory.JDBC_URL) == null) {
	  throw new RuntimeException("Konfigurationsfehler: Konfigurationsparameter [" + IJdbcTemplateWrapper.JDBC_URL_KEY + "] fehlt");
	}
	if (props.get(DataSourceFactory.JDBC_USER) == null) {
	  throw new RuntimeException("Konfigurationsfehler: Konfigurationsparameter [" + IJdbcTemplateWrapper.JDBC_USER_KEY + "] fehlt");
	}
	if (props.get(DataSourceFactory.JDBC_PASSWORD) == null) {
	  throw new RuntimeException("Konfigurationsfehler: Konfigurationsparameter [" + IJdbcTemplateWrapper.JDBC_PWD_KEY + "] fehlt");
	}

  }

  public SimpleJdbcTemplate getJdbcTemplate() {
	if (jdbcTemplate == null) {
	  jdbcTemplate = new SimpleJdbcTemplate(dataSource);
	}
	return jdbcTemplate;
  }

  public String printConfiguration() {
	return "[JDBC_USER=" + props.getProperty(DataSourceFactory.JDBC_USER) + ", JDBC_URL="
		+ props.getProperty(DataSourceFactory.JDBC_URL) + "]";
  }

  public String getDBUserName() {
	return props.getProperty(DataSourceFactory.JDBC_USER);
  }
  
  

  /* (non-Javadoc)
   * @see de.egladil.base.mysql.IJdbcTemplateWrapper#getDBUrl()
   */
  @Override
  public String getDBUrl() {
	return props.getProperty(DataSourceFactory.JDBC_URL);
  }

  /* (non-Javadoc)
   * @see de.egladil.base.mysql.IJdbcTemplateWrapper#getTransactionTemplate()
   */
  @Override
  public TransactionTemplate getTransactionTemplate() {
	if (transactionTemplate == null){
	  DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
	  transactionManager.setDataSource(dataSource);
	  transactionTemplate = new TransactionTemplate();
	  transactionTemplate.setTransactionManager(transactionManager);
	  transactionTemplate.setPropagationBehavior(DefaultTransactionDefinition.PROPAGATION_REQUIRED);
	}
	return transactionTemplate;
  }

  /* (non-Javadoc)
   * @see de.egladil.base.mysql.IJdbcTemplateWrapper#getDataSource()
   */
  @Override
  public DataSource getDataSource() {
	return dataSource;
  }
}
