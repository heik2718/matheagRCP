/**
 * 
 */
package de.egladil.mathejungalt.domain.schulen;

/**
 * This generated interface defines constants for all attributes and associatations in
 * {@link de.egladil.mathejungalt.domain.schulen.Schule}.
 * <p>
 * These constants are useful for example when building criterias.
 * 
 * @author heike
 */
public interface IMinikaenguruTeilnahmeNames {

	public static final String PROP_RUECKMELDUNG = "rueckmeldung";

	public static final String PROP_MINIKAENGURU = "minikaenguru";

	public static final String PROP_SCHULE = "schule";

	public static final String SQL_MINIKAENGURU = "minikaenguru";

	public static final String SQL_SCHULE = "schule";

	public final static int LENGTH_RUECKMELDUNG = 1;

	public final static int LENGTH_ANSCHRIFT = 256;

	public final static int LENGTH_TELEFONNUMMER = 45;

	public final static int LENGTH_BUNDSLAND = 10;
}
