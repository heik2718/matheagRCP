/**
 *
 */
package de.egladil.mathejungalt.domain.aufgaben;

import de.egladil.mathejungalt.domain.IMatheAGObjectNames;

/**
 * This generated interface defines constants for all attributes and associatations in
 * {@link de.egladil.mathe.domain.quelle.Buchquelle}.
 * <p>
 * These constants are useful for example when building criterias.
 *
 * @author heike
 */
public interface IAufgabeNames extends IMatheAGObjectNames {

	public static final String PROP_ART = "art";

	public static final String PROP_ZWECK = "zweck";

	public static final String PROP_THEMA = "thema";

	public static final String PROP_TITEL = "titel";

	public static final String PROP_STUFE = "stufe";

	public static final String PROP_JAHRESZEIT = "jahreszeit";

	public static final String PROP_BESCHREIBUNG = "beschreibung";

	public static final String PROP_HEFT = "heft";

	public static final String PROP_QUELLE = "quelle";

	public static final String PROP_ZU_SCHLECHT = "zuSchlechtFuerSerie";

	public static final int LENGTH_SCHLUESSEL = 5;

	public static final int LENGTH_TITEL = 100;

	public static final int LENGTH_ART = 1;

	public static final int LENGTH_ZWECK = 1;

	public static final int LENGTH_THEMA = 1;

	public static final int LENGTH_STUFE = 1;

	public static final int LENGTH_JAHRESZEIT = 1;

	public static final int LENGTH_BESCHREIBUNG = 256;

	public static final int LENGTH_HEFT = 100;
}
