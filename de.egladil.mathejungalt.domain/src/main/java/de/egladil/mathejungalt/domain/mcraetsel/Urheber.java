/**
 *
 */
package de.egladil.mathejungalt.domain.mcraetsel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;

/**
 * @author heike
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Urheber extends AbstractMatheAGObject {

	/**
   *
   */
	@XmlTransient
	private static final long serialVersionUID = 4542968453670168294L;

	@XmlElement(name = "name")
	private String name = "Heike Winkelvoß";

	@XmlElement(name = "url")
	private String url = "www.egladil.de/mathe/mathehome.html";

	/**
   *
   */
	public Urheber() {
		// placeholder
	}

	/**
	 * @return the name
	 */
	public final String getName() {
		return name;
	}

	/**
	 * @param pName the name to set
	 */
	public final void setName(String pName) {
		name = pName;
	}

	/**
	 * @return the url
	 */
	public final String getUrl() {
		return url;
	}

	/**
	 * @param pUrl the url to set
	 */
	public final void setUrl(String pUrl) {
		url = pUrl;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Urheber [name=");
		builder.append(name);
		builder.append(", url=");
		builder.append(url);
		builder.append("]");
		return builder.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.domain.AbstractMatheAGObject#getKey()
	 */
	@Override
	public Object getKey() {
		return name;
	}
}
