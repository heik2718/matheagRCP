/**
 * 
 */
package de.egladil.mathejungalt.domain.quellen;

/**
 * This generated interface defines constants for all attributes and associatations in
 * {@link de.egladil.mathe.domain.quelle.Buchquelle}.
 * <p>
 * These constants are useful for example when building criterias.
 * 
 * @author heike
 */
public interface IZeitschriftquelleNames extends IQuelleNames {

	public static final String SQL_AUSGABE = "ausgabe";

	public static final String SQL_JAHRGANG = "jahrgang";

	public static final String SQL_ZEITSCHRIFT = "zeitschrift";

	public static final String PROP_AUSGABE = "ausgabe";

	public static final String PROP_JAHRGANG = "jahrgang";

	public static final String PROP_ZEITSCHRIFT = "zeitschrift";
}
