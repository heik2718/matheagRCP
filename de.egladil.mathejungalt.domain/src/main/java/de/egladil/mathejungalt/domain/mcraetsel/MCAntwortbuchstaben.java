/**
 *
 */
package de.egladil.mathejungalt.domain.mcraetsel;

/**
 * @author heike
 */
public enum MCAntwortbuchstaben {

	A("A") {

		@Override
		public int getIndex() {
			return 0;
		}

		@Override
		public Boolean[] getCheckState() {
			return new Boolean[] { true, false, false, false, false, false };
		}

	},

	B("B") {

		@Override
		public int getIndex() {
			return 1;
		}

		@Override
		public Boolean[] getCheckState() {
			return new Boolean[] { false, true, false, false, false, false };
		}

	},

	C("C") {

		@Override
		public int getIndex() {
			return 2;
		}

		@Override
		public Boolean[] getCheckState() {
			return new Boolean[] { false, false, true, false, false, false };
		}

	},

	D("D") {

		@Override
		public int getIndex() {
			return 3;
		}

		@Override
		public Boolean[] getCheckState() {
			return new Boolean[] { false, false, false, true, false, false };
		}

	},

	E("E") {

		@Override
		public int getIndex() {
			return 4;
		}

		@Override
		public Boolean[] getCheckState() {
			return new Boolean[] { false, false, false, false, true, false };
		}

	},

	N("N") {

		@Override
		public int getIndex() {
			return 5;
		}

		@Override
		public Boolean[] getCheckState() {
			return new Boolean[] { false, false, false, false, false, true };
		}

	};

	private final String bezeichnung;

	/**
	 * @param pBezeichnung
	 */
	private MCAntwortbuchstaben(String pBezeichnung) {
		bezeichnung = pBezeichnung;
	}

	/**
	 * Der Index in dem geordneten Array der Antwortvorschläge.
	 * 
	 * @return
	 */
	public abstract int getIndex();

	/**
	 * @return the bezeichnung
	 */
	public final String getBezeichnung() {
		return bezeichnung;
	}

	public static MCAntwortbuchstaben[] permittedValues() {
		return new MCAntwortbuchstaben[] { A, B, C, D, E };
	}

	/**
	 * @param pIndex
	 * @return
	 */
	public static MCAntwortbuchstaben valueOfIndex(int pIndex) {
		switch (pIndex) {
		case 0:
			return A;
		case 1:
			return B;
		case 2:
			return C;
		case 3:
			return D;
		case 4:
			return E;
		default:
			return N;
		}
	}

	public abstract Boolean[] getCheckState();
}
