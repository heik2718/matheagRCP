/**
 * 
 */
package de.egladil.mathejungalt.domain.medien;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.IMatheAGObjectNames;
import de.egladil.mathejungalt.domain.types.EnumTypes.Medienart;

/**
 * Entity - Businesslogik-Verhalten eines AbstractPrintmedium (Abhängigkeiten werden in der Basisklasse implementiert)
 * 
 * @author heike
 */
public abstract class AbstractPrintmedium extends AbstractMatheAGObject {

	/** */
	private static final long serialVersionUID = -6493639413229430782L;

	/** */
	private String titel;

	/** */
	private String schluessel;

	/**
	 * 
	 */
	public AbstractPrintmedium() {
		super();
		titel = "";
	}

	/**
	 * @param pSchluessel
	 */
	public AbstractPrintmedium(String pSchluessel) {
		super();
		schluessel = pSchluessel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return (getKey() + "-" + titel);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.domain.AbstractMatheAGObject#getKey()
	 */
	@Override
	public final Object getKey() {
		return schluessel;
	}

	/**
	 * @return
	 */
	public String getTitel() {
		return titel;
	}

	/**
	 * @param pTitel
	 */
	public void setTitel(String pTitel) {
		firePropertyChange(IMedienNames.PROP_TITEL, titel, titel = pTitel);
	}

	/**
	 * @return
	 */
	public abstract Medienart getArt();

	/**
	 * @return the schluessel
	 */
	public String getSchluessel() {
		return schluessel;
	}

	/**
	 * @param pSchluessel the schluessel to set
	 */
	public void setSchluessel(String pSchluessel) {
		firePropertyChange(IMatheAGObjectNames.PROP_SCHLUESSEL, schluessel, schluessel = pSchluessel);
	}
}
