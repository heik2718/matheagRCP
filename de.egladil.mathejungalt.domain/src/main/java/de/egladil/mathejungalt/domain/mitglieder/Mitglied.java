/**
 *
 */
package de.egladil.mathejungalt.domain.mitglieder;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.IMatheAGObjectNames;

/**
 * @author heike
 */
public class Mitglied extends AbstractMatheAGObject {

	/** */
	private static final long serialVersionUID = -6108951830655463326L;

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(Mitglied.class);

	/** */
	private String schluessel;

	/** */
	private String vorname = "";

	/** */
	private String nachname;

	/** */
	private Integer klasse = -1;

	/** */
	private Integer alter = -1;

	/** */
	private int erfinderpunkte;

	/** */
	private Integer geburtsmonat;

	/** */
	private Integer geburtsjahr;

	/** */
	private String fototitel;

	/** */
	private String kontakt = "";

	private int aktiv = 1;

	/** */
	private List<Serienteilnahme> serienteilnahmen;

	/** */
	private List<Diplom> diplome;

	/** */
	private boolean serienteilnahmenLoaded = false;

	/** */
	private boolean diplomeLoaded = false;

	/** */
	private int anzDiplome = 0;

	/**
	 *
	 */
	public Mitglied() {
		super();
		setSerienteilnahmenLoaded(false);
		setDiplomeLoaded(false);
		serienteilnahmen = new ArrayList<Serienteilnahme>();
		diplome = new ArrayList<Diplom>();
	}

	/**
	 * @return the serienteilnahmen
	 */
	public List<Serienteilnahme> getSerienteilnahmen() {
		return serienteilnahmen;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer();
		sb.append(vorname);
		sb.append("-");
		if (nachname != null) {
			sb.append(nachname);
			sb.append("-");
		}
		sb.append(alter);
		sb.append("-Klasse ");
		sb.append(klasse);
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.domain.AbstractMatheAGObject#getKey()
	 */
	@Override
	public Object getKey() {
		return schluessel;
	}

	/**
	 * @param pSchluessel
	 */
	public void setSchluessel(final String pSchluessel) {
		final String oldKey = schluessel;
		schluessel = pSchluessel;
		firePropertyChange(IMatheAGObjectNames.PROP_SCHLUESSEL, oldKey, schluessel);
	}

	/**
	 * @return the vorname
	 */
	public String getVorname() {
		return vorname;
	}

	/**
	 * @param pVorname the vorname to set
	 */
	public void setVorname(final String pVorname) {
		firePropertyChange(IMitgliedNames.PROP_VORNAME, vorname, vorname = pVorname);
	}

	/**
	 * @return the nachname
	 */
	public String getNachname() {
		return nachname;
	}

	/**
	 * @param pNachname the nachname to set
	 */
	public void setNachname(final String pNachname) {
		firePropertyChange(IMitgliedNames.PROP_NACHNAME, nachname, nachname = pNachname);
	}

	/**
	 * @return the klasse
	 */
	public Integer getKlasse() {
		return klasse;
	}

	/**
	 * @param pKlasse the klasse to set
	 */
	public void setKlasse(final Integer pKlasse) {
		firePropertyChange(IMitgliedNames.PROP_KLASSE, klasse, klasse = pKlasse);
	}

	/**
	 * @return the jahre
	 */
	public Integer getAlter() {
		return alter;
	}

	/**
	 * @param pJahre the jahre to set
	 */
	public void setAlter(final Integer pJahre) {
		firePropertyChange(IMitgliedNames.PROP_ALTER, alter, alter = pJahre);
	}

	/**
	 * @return the erfinderpunkte
	 */
	public int getErfinderpunkte() {
		return erfinderpunkte;
	}

	/**
	 * @param pErfinderpunkte the erfinderpunkte to set
	 */
	public void setErfinderpunkte(final int pErfinderpunkte) {
		if (pErfinderpunkte < erfinderpunkte && anzDiplome > 0) {
			throw new MatheJungAltException(
				"Die Anzahl der Erfinderpunkte darf nicht verkleinert werden, da das Mitglied bereits Diplome hat!");
		}
		firePropertyChange(IMitgliedNames.PROP_ERFINDERPUNKTE, erfinderpunkte, erfinderpunkte = pErfinderpunkte);
	}

	/**
	 * @return the geburtsmonat
	 */
	public Integer getGeburtsmonat() {
		return geburtsmonat;
	}

	/**
	 * @param pGeburtsmonat the geburtsmonat to set
	 */
	public void setGeburtsmonat(final Integer pGeburtsmonat) {
		firePropertyChange(IMitgliedNames.PROP_GEBURTSMONAT, geburtsmonat, geburtsmonat = pGeburtsmonat);
	}

	/**
	 * @return the geburtsjahr
	 */
	public Integer getGeburtsjahr() {
		return geburtsjahr;
	}

	/**
	 * @param pGeburtsjahr the geburtsjahr to set
	 */
	public void setGeburtsjahr(final Integer pGeburtsjahr) {
		firePropertyChange(IMitgliedNames.PROP_GEBURTSJAHR, geburtsjahr, geburtsjahr = pGeburtsjahr);
	}

	/**
	 * @return the fototitel
	 */
	public String getFototitel() {
		return fototitel;
	}

	/**
	 * @param pFototitel the fototitel to set
	 */
	public void setFototitel(final String pFototitel) {
		firePropertyChange(IMitgliedNames.PROP_FOTOTITEL, fototitel, fototitel = pFototitel);
	}

	/**
	 * @return the kontakt
	 */
	public String getKontakt() {
		return kontakt;
	}

	/**
	 * @param pKontakt the kontakt to set
	 */
	public void setKontakt(final String pKontakt) {
		firePropertyChange(IMitgliedNames.PROP_KONTAKT, kontakt, kontakt = pKontakt);
	}

	/**
	 * Fügt die Serienteilnahme zum Mitglied hinzu
	 *
	 * @param pSerienteilnahme
	 */
	public void addSerienteilname(final Serienteilnahme pSerienteilnahme) {
		if (pSerienteilnahme == null) {
			throw new MatheJungAltException("Not allowed to add null Serienteilnahme.");
		}
		if (serienteilnahmen == null) {
			serienteilnahmen = new ArrayList<Serienteilnahme>();
		}
		if (serienteilnahmen.contains(pSerienteilnahme)) {
			return;
		}
		serienteilnahmen.add(pSerienteilnahme);
		if (serienteilnahmenLoaded) {
			fireSerienteilnahmenChanged();
		}
	}

	/**
	 * Handle auf die zugeordneten Serienteilnahmen.
	 *
	 * @return {@link Iterator}
	 */
	public Iterator<Serienteilnahme> serienteilnahmenIterator() {
		LOG.debug("Start serienteilnahmenIterator()");
		if (serienteilnahmen == null)
			initSerienteilnahmen();
		return serienteilnahmen.iterator();
	}

	/**
	 *
	 */
	public void initDiplome() {
		if (!diplomeLoaded)
			diplome = new ArrayList<Diplom>();
	}

	/**
	 *
	 */
	public void clearDiplome() {
		if (diplome != null)
			diplome.clear();
	}

	/**
	 *
	 */
	public void initSerienteilnahmen() {
		if (!isSerienteilnahmenLoaded())
			serienteilnahmen = new ArrayList<Serienteilnahme>();
	}

	/**
	 *
	 */
	public void clearSerienteilnahmen() {
		if (serienteilnahmen != null)
			serienteilnahmen.clear();
	}

	/**
	 * Handle auf die zugeordneten Diplome.
	 *
	 * @return {@link Iterator}
	 */
	public Iterator<Diplom> diplomeIterator() {
		LOG.debug("Start diplomeIterator()");
		if (diplome == null)
			initDiplome();
		return diplome.iterator();
	}

	/**
	 * @param pSerienteilnahme
	 * @return boolean true, falls tats�chlich entfernt wurde, false sonst.
	 */
	public boolean removeSerienteilnahme(final Serienteilnahme pSerienteilnahme) {
		LOG.debug("Start removeSerienteilnahme()");
		final boolean removed = serienteilnahmen.remove(pSerienteilnahme);
		if (removed)
			fireSerienteilnahmenChanged();
		return removed;
	}

	/**
	 * F�gt das Diplom der Diplom-Liste hinzu
	 *
	 * @param pDiplom das {@link Diplom}
	 * @return boolean true, falls das Diplom hizugef�gt wurde, false sonst.
	 */
	public void addDiplom(final Diplom pDiplom) {
		if (pDiplom == null) {
			final String msg = MatheJungAltException.argumentNullMessage("Diplom");
			throw new MatheJungAltException(msg);
		}
		if (diplome == null) {
			diplome = new ArrayList<Diplom>();
		}
		if (!pDiplom.isNew() && diplome.contains(pDiplom)) {
			return;
		}
		diplome.add(pDiplom);
		setAnzDiplome(anzDiplome + 1);
	}

	/**
	 * Entfernt das {@link Diplom} aus der Diplom-Liste.
	 *
	 * @param pDiplom
	 * @return
	 */
	public boolean removeDiplom(final Diplom pDiplom) {
		LOG.debug("Start removeDiplom()");
		final boolean removed = diplome.remove(pDiplom);
		if (removed) {
			setAnzDiplome(anzDiplome - 1);
		}
		return removed;
	}

	/**
	 * @return int Anzahl der Diplome.
	 */
	public int anzahlDiplome() {
		return diplome.size();
	}

	public int gesamtpunkteSerien() {
		int result = 0;
		for (final Serienteilnahme teilnahme : serienteilnahmen) {
			result += teilnahme.getPunkte();
		}
		return result;
	}

	/**
	 * @return the schluessel
	 */
	public String getSchluessel() {
		return schluessel;
	}

	/**
	 *
	 */
	private void fireSerienteilnahmenChanged() {
		firePropertyChange(IMitgliedNames.PROP_SERIENTEILNAHMEN, null, serienteilnahmen.iterator());
	}

	/**
	 * @return the serienteilnahmenLoaded
	 */
	public boolean isSerienteilnahmenLoaded() {
		return serienteilnahmenLoaded;
	}

	/**
	 * @param pSerienteilnahmenLoaded the serienteilnahmenLoaded to set
	 */
	public void setSerienteilnahmenLoaded(final boolean pSerienteilnahmenLoaded) {
		serienteilnahmenLoaded = pSerienteilnahmenLoaded;
	}

	/**
	 * @return the diplome
	 */
	public List<Diplom> getDiplome() {
		return diplome;
	}

	/**
	 * @return the diplomeLoaded
	 */
	public boolean isDiplomeLoaded() {
		return diplomeLoaded;
	}

	/**
	 * @param pDiplomeLoaded the diplomeLoaded to set
	 */
	public void setDiplomeLoaded(final boolean pDiplomeLoaded) {
		diplomeLoaded = pDiplomeLoaded;
	}

	/**
	 * @return the anzDiplome
	 */
	public int getAnzDiplome() {
		return anzDiplome;
	}

	/**
	 * @param pAnzDiplome the anzDiplome to set
	 */
	public void setAnzDiplome(final int pAnzDiplome) {
		firePropertyChange(IMitgliedNames.PROP_ANZAHL_DIPLOME, anzDiplome, anzDiplome = pAnzDiplome);
	}

	/**
	 * @return
	 */
	public boolean isAktiv() {
		return aktiv == 1;
	}

	/**
	 * @param pAktiv the aktiv to set
	 */
	public final void setAktiv(final int pAktiv) {
		aktiv = pAktiv;
	}

	public int getAktiv() {
		return aktiv;
	}
}
