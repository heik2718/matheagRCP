/**
 *
 */
package de.egladil.mathejungalt.domain;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.EgladilBaseException;
import de.egladil.base.exceptions.ServerExceptionResponse;
import de.egladil.mathejungalt.domain.mcraetsel.AndroidRaetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetselItem;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;

/**
 * @author heike
 */
public final class JaxbContextProvider {

	private static final Logger LOG = LoggerFactory.getLogger(JaxbContextProvider.class);

	private static JAXBContext jaxbContext;

	/**
   *
   */
	private JaxbContextProvider() {
	}

	private static void checkContext() {
		if (jaxbContext != null) {
			return;
		}
		try {
			jaxbContext = JAXBContext.newInstance(MCAufgabe.class, MCArchivraetsel.class, MCArchivraetselItem.class,
				ServerExceptionResponse.class, AndroidRaetsel.class);
		} catch (JAXBException e) {
			LOG.error(e.getMessage());
			throw new EgladilBaseException("Testsetting", e);
		}
	}

	/**
	 * @return
	 */
	public static Marshaller getMarshaller() {
		checkContext();
		try {
			return jaxbContext.createMarshaller();
		} catch (JAXBException e) {
			LOG.error(e.getMessage());
			throw new EgladilBaseException("Testsetting", e);
		}
	}

	/**
	 * @return
	 */
	public static Unmarshaller getUnmarshaller() {
		checkContext();
		try {
			return jaxbContext.createUnmarshaller();
		} catch (JAXBException e) {
			LOG.error(e.getMessage());
			throw new EgladilBaseException("Testsetting", e);
		}
	}

	/**
	 * Schreibt den Writer auf die Konsole oder in den Logger, falls dieser nicht null ist.
	 *
	 * @param out
	 * @param pLogger TODO
	 */
	public static void print(StringWriter out, Logger pLogger) {
		// Gib das Resultat auf stdout aus, dabei vergiss Leerzeilen
		BufferedReader buf = new BufferedReader(new StringReader(out.toString()));
		StringBuffer sb = new StringBuffer();
		String line = null;
		try {
			while ((line = buf.readLine()) != null) {
				if (line.trim().equals("")) {
					// leere Zeile -- mach nix.
				} else {
					sb.append(line);
				}
			}
			if (pLogger != null) {
				pLogger.info(sb.toString());
			} else {
				System.out.println(sb.toString());
			}
		} catch (IOException e) {
			LOG.error(e.getMessage());
			throw new EgladilBaseException("Test print", e);
		}
	}
}
