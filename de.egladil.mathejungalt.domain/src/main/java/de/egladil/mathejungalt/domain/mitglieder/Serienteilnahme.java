/**
 * 
 */
package de.egladil.mathejungalt.domain.mitglieder;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.types.MitgliedSerieComposedKey;

/**
 * @author heike
 */
public class Serienteilnahme extends AbstractMatheAGObject {

	/** */
	private static final long serialVersionUID = 3867304153858942801L;

	/** */
	private Double fruehstarter;

	/** */
	private Integer punkte;

	/** */
	private Mitglied mitglied;

	/** */
	private Serie serie;

	/**
	 * 
	 */
	public Serienteilnahme() {
		super();
		fruehstarter = 0.0;
		punkte = 1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.domain.AbstractMatheAGObject#getKey()
	 */
	@Override
	public Object getKey() {
		if (mitglied == null && serie == null) {
			return getId();
		}
		return new MitgliedSerieComposedKey(mitglied, serie);
	}

	/**
	 * @return the fruehstarter
	 */
	public Double getFruehstarter() {
		return fruehstarter;
	}

	/**
	 * @param pFruehstarter the fruehstarter to set
	 */
	public void setFruehstarter(Double pFruehstarter) {
		firePropertyChange(ISerienteilnahmeNames.PROP_FRUEHSTARTER, fruehstarter, fruehstarter = pFruehstarter);
	}

	/**
	 * @return the punkte
	 */
	public Integer getPunkte() {
		return punkte;
	}

	/**
	 * @param pPunkte the punkte to set
	 */
	public void setPunkte(Integer pPunkte) {
		firePropertyChange(ISerienteilnahmeNames.PROP_PUNKTE, punkte, punkte = pPunkte);
	}

	/**
	 * @return the mitglied
	 */
	public Mitglied getMitglied() {
		return mitglied;
	}

	/**
	 * @param pMitglied the mitglied to set
	 */
	public void setMitglied(Mitglied pMitglied) {
		firePropertyChange(ISerienteilnahmeNames.PROP_MITGLIED, mitglied, mitglied = pMitglied);
	}

	/**
	 * @return the serie
	 */
	public Serie getSerie() {
		return serie;
	}

	/**
	 * @param pSerie the serie to set
	 */
	public void setSerie(Serie pSerie) {
		firePropertyChange(ISerienteilnahmeNames.PROP_SERIE, serie, serie = pSerie);
	}
}
