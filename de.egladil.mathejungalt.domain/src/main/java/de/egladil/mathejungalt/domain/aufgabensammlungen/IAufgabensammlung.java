/**
 *
 */
package de.egladil.mathejungalt.domain.aufgabensammlungen;

import java.beans.PropertyChangeListener;
import java.util.List;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;

/**
 * @author aheike
 */
public interface IAufgabensammlung {

	/** */
	public final static String PROP_BEENDET = "beendet";

	/** */
	public final static int BEENDET = 1;

	/** */
	public final static int NICHT_BEENDET = 0;

	/**
	 * @return {@link List} von Items
	 */
	List<IAufgabensammlungItem> getItems();

	/**
	 * @return boolean true, wenn die Items geladen sind, false sonst.
	 */
	boolean isItemsLoaded();

	/**
	 * Gibt die Items einer bestimmten Stufe zurueck.
	 *
	 * @param pStufe int die Stufe
	 * @return {@link List} von Items
	 */
	List<IAufgabensammlungItem> getItems(int pStufe);

	/**
	 * @param pItem
	 * @return boolean true, wenn hinzugefügt wurde.
	 */
	boolean addItem(final IAufgabensammlungItem pItem);

	/**
	 * @param pItem
	 * @return boolean true, wenn entfernt wurde.
	 */
	boolean removeItem(final IAufgabensammlungItem pItem);

	/**
	 * Prüft, ob die gegebene Aufgabe zu der Aufgabensammlung hinzugefuegt werden darf.
	 *
	 * @param pAufgabe {@link Aufgabe}
	 * @return boolean
	 */
	boolean canAdd(Aufgabe pAufgabe);

	/**
	 * @return
	 */
	int anzahlAufgaben();

	/**
	 * Das implementierende Objekt.
	 *
	 * @return
	 */
	AbstractMatheAGObject getDomainObject();

	/**
	 * @return
	 */
	int getBeendet();

	/**
	 * @param pBeendet
	 */
	void setBeendet(int pBeendet);

	/**
	 * Den Listener anmelden
	 *
	 * @param pListener {@link PropertyChangeListener}
	 */
	void addPropertyChangeListener(final PropertyChangeListener pListener);

	/**
	 * Den Listener wieder abmelden.
	 *
	 * @param pListener {@link PropertyChangeListener}
	 */
	void removePropertyChangeListener(final PropertyChangeListener pListener);

	/**
	 * @return
	 */
	boolean isNew();
}
