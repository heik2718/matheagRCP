/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.domain.schulen;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;

/**
 * <p>
 * NOTE: THIS WIDGET AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class MinikaenguruTeilnahme extends AbstractMatheAGObject {

	private static final long serialVersionUID = 8543516756065219827L;

	private Schule schule;

	private Minikaenguru minikaenguru;

	private boolean rueckmeldung;

	/**
   *
   */
	public MinikaenguruTeilnahme() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.domain.AbstractMatheAGObject#getKey()
	 */
	@Override
	public Object getKey() {
		if (schule == null && minikaenguru == null) {
			return getId();
		}
		return new SchuleMinikaenguruComposedKey(schule, minikaenguru);
	}

	/**
	 * @return the schule
	 */
	public final Schule getSchule() {
		return schule;
	}

	/**
	 * @param pSchule the schule to set
	 */
	public final void setSchule(Schule pSchule) {
		schule = pSchule;
	}

	/**
	 * @return the minikaenguru
	 */
	public final Minikaenguru getMinikaenguru() {
		return minikaenguru;
	}

	/**
	 * @param pMinikaenguru the minikaenguru to set
	 */
	public final void setMinikaenguru(Minikaenguru pMinikaenguru) {
		minikaenguru = pMinikaenguru;
	}

	/**
	 * @return the rueckmeldung
	 */
	public final boolean isRueckmeldung() {
		return rueckmeldung;
	}

	/**
	 * @param pRueckmeldung the rueckmeldung to set
	 */
	public final void setRueckmeldung(boolean pRueckmeldung) {
		firePropertyChange(IMinikaenguruTeilnahmeNames.PROP_RUECKMELDUNG, this.rueckmeldung,
			rueckmeldung = pRueckmeldung);
		;
	}

	/**
	 * @param schule
	 * @param minikaenguru
	 * @return
	 */
	public static MinikaenguruTeilnahme create(Schule pSchule, Minikaenguru pMinikaenguru) {
		MinikaenguruTeilnahme teilnahme = new MinikaenguruTeilnahme();
		teilnahme.setSchule(pSchule);
		teilnahme.setMinikaenguru(pMinikaenguru);
		pSchule.addMinikaenguruTeilnahme(teilnahme);
		return teilnahme;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.domain.AbstractMatheAGObject#compareTo(de.egladil.mathejungalt.domain.AbstractMatheAGObject
	 * )
	 */
	@Override
	public int compareTo(AbstractMatheAGObject pO) {
		MinikaenguruTeilnahme other = (MinikaenguruTeilnahme) pO;
		int jahr1 = minikaenguru.getJahr();
		int jahr2 = other.getMinikaenguru().getJahr();
		// return jahr1- jahr2;
		return super.compareTo(pO);
	}
}
