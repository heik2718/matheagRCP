/**
 *
 */
package de.egladil.mathejungalt.domain.mcraetsel;

import de.egladil.mathejungalt.domain.IMatheAGObjectNames;

/**
 * @author heike
 */
public interface IMCArchivraetselNames extends IMatheAGObjectNames {

	String PROP_TITEL = "titel";

	String PROP_STUFE = "stufe";

	String PROP_LICENSE_SHORT = "licenseShort";

	String PROP_LICENSE_FULL = "licenseFull";

	String PROP_AUTOR = "autor";

	String PROP_VEROEFFENTLICHT = "veroeffentlicht";

	String PROP_PRIVAT = "privat";

	String PROP_ITEMS = "items";

	int LENGTH_SCHLUESSEL = 15;

	int LENGTH_SCHLUESSEL_DEFAULT = 5;

}
