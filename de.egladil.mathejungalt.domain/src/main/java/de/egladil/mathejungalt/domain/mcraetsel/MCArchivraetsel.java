/**
 *
 */
package de.egladil.mathejungalt.domain.mcraetsel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.eclipse.core.runtime.Assert;

import de.egladil.base.exceptions.MatheJungAltInconsistentEntityException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.types.MCAufgabenstufen;

/**
 * Das ist ein persistentes MC-Rätsel. Ein Rätsel besteht aus einer durch 3 teilbaren Anzahl vom MCAufgaben.
 *
 * @author heike
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "mcMatheRaetsel")
public class MCArchivraetsel extends AbstractMatheAGObject implements IMatheAGEntity {

	/**
   *
   */
	@XmlTransient
	private static final long serialVersionUID = -4261676848595119981L;

	@XmlElement(name = "schluessel")
	private String schluessel;

	@XmlElement(name = "titel")
	private String titel;

	@XmlElement(name = "stufe")
	private MCAufgabenstufen stufe;

	@XmlElement(name = "licenseShort")
	private String licenseShort = "CC-BY-SA";

	@XmlElement(name = "licenseFull")
	private String licenseFull = "Creative Commons Namensnennung-Weitergabe unter gleichen Bedingungen 3.0 Unported Lizenz (http://creativecommons.org/licenses/by-sa/3.0/)";

	@XmlElement(name = "urheber")
	private Urheber autor = new Urheber();

	@XmlElement(name = "published")
	private boolean veroeffentlicht = false;

	@XmlElement(name = "privat")
	private boolean privat;

	@XmlElement(name = "mcRaetselItem")
	private final List<MCArchivraetselItem> items = new ArrayList<MCArchivraetselItem>();

	@XmlTransient
	private boolean itemsLoaded;

	/**
	 * licenceFull und licenceShort sind bereits initialisiert auf CC-BY-SA. autor ist bereits initialisiert mit Heike
	 * Winkelvoß
	 */
	public MCArchivraetsel() {
	}

	/**
	 * @param pItem
	 */
	public void addItem(MCArchivraetselItem pItem) {
		Assert.isNotNull(pItem, "pItem");
		Assert.isNotNull(pItem.getAufgabe(), "pItem.aufgabe");
		Assert.isNotNull(stufe, "this.stufe");
		Assert.isTrue(stufe.equals(pItem.getAufgabe().getStufe()), "es darf nur eine Aufgabe der Stufe " + stufe
			+ " zugeordnet werden.");
		if (!items.contains(pItem)) {
			items.add(pItem);
			pItem.setRaetsel(this);
			if (itemsLoaded) {
				fireItemsChanged();
			}
		}
	}

	/**
	 *
	 */
	public void fireItemsChanged() {
		firePropertyChange(IMCArchivraetselNames.PROP_ITEMS, null, getItems());
	}

	/**
	 * @param pItem
	 */
	public void removeItem(MCArchivraetselItem pItem) {
		boolean removed = items.remove(pItem);
		if (itemsLoaded && removed) {
			fireItemsChanged();
		}
	}

	/**
	 * @return the name
	 */
	public final String getTitel() {
		return titel;
	}

	/**
	 * @param pName the name to set
	 */
	public final void setTitel(String pName) {
		firePropertyChange(IMCArchivraetselNames.PROP_TITEL, titel, titel = pName);
	}

	/**
	 * @return the stufe
	 */
	public final MCAufgabenstufen getStufe() {
		return stufe;
	}

	/**
	 * @param pStufe the stufe to set
	 */
	public final void setStufe(MCAufgabenstufen pStufe) {
		firePropertyChange(IMCArchivraetselNames.PROP_STUFE, stufe, stufe = pStufe);
	}

	/**
	 * @return the items als unmodifiable List
	 */
	public final List<MCArchivraetselItem> getItems() {
		return Collections.unmodifiableList(items);
	}

	/**
	 * @return
	 */
	public final List<MCArchivraetselItem> getItemsSorted() {
		List<MCArchivraetselItem> result = items;
		Collections.sort(result);
		return Collections.unmodifiableList(result);
	}

	/**
	 * @return the nummer
	 */
	public final String getSchluessel() {
		return schluessel;
	}

	/**
	 * @param pNummer the nummer to set
	 */
	public final void setSchluessel(String pNummer) {
		firePropertyChange(IMCArchivraetselNames.PROP_SCHLUESSEL, schluessel, schluessel = pNummer);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((schluessel == null) ? 0 : schluessel.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MCArchivraetsel other = (MCArchivraetsel) obj;
		if (schluessel == null) {
			if (other.schluessel != null)
				return false;
		} else if (!schluessel.equals(other.schluessel))
			return false;
		return true;
	}

	/**
	 * @return the license
	 */
	public final String getLicenseFull() {
		return licenseFull;
	}

	/**
	 * @param pLicence the licence to set
	 */
	public final void setLicenseFull(String pLicence) {
		firePropertyChange(IMCArchivraetselNames.PROP_LICENSE_FULL, licenseFull, licenseFull = pLicence);
	}

	/**
	 * @return the autor
	 */
	public Urheber getAutor() {
		return autor;
	}

	/**
	 * @param pAutor the autor to set
	 */
	public void setAutor(Urheber pAutor) {
		firePropertyChange(IMCArchivraetselNames.PROP_AUTOR, autor, autor = pAutor);
	}

	/**
	 * @return
	 */
	public int size() {
		return items.size();
	}

	/**
	 * @return
	 */
	public boolean isEmpty() {
		return items.isEmpty();
	}

	/**
	 * @return the licenseShort
	 */
	public String getLicenseShort() {
		return licenseShort;
	}

	/**
	 * @param pLicenseShort the licenseShort to set
	 */
	public void setLicenseShort(String pLicenseShort) {
		firePropertyChange(IMCArchivraetselNames.PROP_LICENSE_SHORT, licenseShort, licenseShort = pLicenseShort);
	}

	/**
	 * @return the itemsLoaded
	 */
	public boolean isItemsLoaded() {
		return itemsLoaded;
	}

	/**
	 * @param pItemsLoaded the itemsLoaded to set
	 */
	public void setItemsLoaded(boolean pItemsLoaded) {
		itemsLoaded = pItemsLoaded;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mcmatheraetsel.archivdomain.archiv.IMatheAGEntity#checkConsistent()
	 */
	@Override
	public void checkConsistent() throws MatheJungAltInconsistentEntityException {
		StringBuffer sb = new StringBuffer();
		boolean consistent = true;
		if (schluessel == null || schluessel.isEmpty() || schluessel.length() > 15) {
			sb.append("schluessel darf nicht null, leer oder  l\u00e4nger als 15 Zeichen sein\n");
			consistent = false;
		}
		if (autor == null) {
			sb.append("autor darf nicht null sein\n");
			consistent = false;
		}
		if (autor.getId() == null) {
			sb.append("autor.id darf nicht null sein\n");
			consistent = false;
		}
		if (licenseFull != null && licenseFull.length() > 255) {
			sb.append("licenseFull darf nicht l\u00e4nger als 255 Zeichen sein\n");
			consistent = false;
		}
		if (licenseShort != null && licenseShort.length() > 100) {
			sb.append("licenseShort darf nicht l\u00e4nger als 100 Zeichen sein\n");
			consistent = false;
		}
		if (titel != null && titel.length() > 100) {
			sb.append("titel darf nicht l\u00e4nger als 100 Zeichen sein\n");
			consistent = false;
		}
		if (!consistent) {
			throw new MatheJungAltInconsistentEntityException("Das R\u00e4tsel kann nicht gespeichert werden:\n\n"
				+ sb.toString());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.domain.AbstractMatheAGObject#getKey()
	 */
	@Override
	public Object getKey() {
		return schluessel;
	}

	/**
   *
   */
	public void clearItems() {
		items.clear();
	}

	/**
	 * @return the veroeffentlicht
	 */
	public boolean isVeroeffentlicht() {
		return veroeffentlicht;
	}

	/**
	 * @param pVeroeffentlicht the veroeffentlicht to set
	 */
	public void setVeroeffentlicht(boolean pVeroeffentlicht) {
		firePropertyChange(IMCArchivraetselNames.PROP_VEROEFFENTLICHT, veroeffentlicht,
			veroeffentlicht = pVeroeffentlicht);
	}

	/**
	 * @param pAufgabe
	 * @return
	 */
	public boolean canAddAufgabe(MCAufgabe pAufgabe) {
		Assert.isNotNull(pAufgabe, "pAufgabe");
		if (veroeffentlicht) {
			return false;
		}
		return stufe != null && stufe.equals(pAufgabe.getStufe());
	}

	/**
	 * @return
	 */
	public boolean canPublish() {
		if (veroeffentlicht) {
			return false;
		}
		return canGenerate();
	}

	/**
	 * @return
	 */
	public boolean canGenerate() {
		if (schluessel == null || schluessel.isEmpty()) {
			return false;
		}
		if (titel == null || titel.isEmpty()) {
			return false;
		}
		if (stufe == null) {
			return false;
		}
		for (MCArchivraetselItem item : items) {
			if (!item.canGenerate()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @param pPathWorkDir
	 * @return
	 */
	public String getPathPrefix(String pPathWorkDir) {
		return pPathWorkDir + "/raetsel_" + schluessel;
	}

	/**
	 * @return the restricted
	 */
	public boolean isPrivat() {
		return privat;
	}

	/**
	 * @param pPrivat the restricted to set
	 */
	public void setPrivat(boolean pPrivat) {
		firePropertyChange(IMCArchivraetselNames.PROP_PRIVAT, privat, privat = pPrivat);
	}
}
