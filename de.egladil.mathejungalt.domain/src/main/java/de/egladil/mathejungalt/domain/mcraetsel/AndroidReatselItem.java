/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.domain.mcraetsel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class AndroidReatselItem {

	@XmlElement(name = "index")
	private int index;

	@XmlElement(name = "schluessel")
	private String schluessel;

	@XmlElement(name = "punkte")
	private int punkte;

	@XmlElement(name = "a")
	private String antwortA;

	@XmlElement(name = "b")
	private String antwortB;

	@XmlElement(name = "c")
	private String antwortC;

	@XmlElement(name = "d")
	private String antwortD;

	@XmlElement(name = "e")
	private String antwortE;

	@XmlElement(name = "loesung")
	private MCAntwortbuchstaben loesungsbuchstabe;

	/**
   *
   */
	public AndroidReatselItem() {

	}

	/**
	 * @param item
	 * @return
	 */
	public static AndroidReatselItem createFromMCArchivRaetselItem(MCArchivraetselItem item, int index) {
		AndroidReatselItem result = new AndroidReatselItem();
		final MCAufgabe aufgabe = item.getAufgabe();
		if (aufgabe.isTabelleGenerieren()) {
			result.setAntwortA(aufgabe.getAntwortA());
			result.setAntwortB(aufgabe.getAntwortB());
			result.setAntwortC(aufgabe.getAntwortC());
			result.setAntwortD(aufgabe.getAntwortD());
			result.setAntwortE(aufgabe.getAntwortE());
		} else {
			result.setAntwortA("(A)");
			result.setAntwortB("(B)");
			switch (aufgabe.getAnzahlAntwortvorschlaege()) {
			case 3:
				result.setAntwortC("(C)");
				break;
			case 4:
				result.setAntwortC("(C)");
				result.setAntwortD("(D)");
				break;
			case 5:
				result.setAntwortC("(C)");
				result.setAntwortD("(D)");
				result.setAntwortE("(E)");
				break;
			default:
				break;
			}
		}
		result.setIndex(index);
		result.setLoesungsbuchstabe(aufgabe.getLoesungsbuchstabe());
		result.setPunkte(aufgabe.getPunkte());
		final String nummer = toSchluessel(item.getNummer());
		result.setSchluessel(nummer);
		return result;
	}

	private static String toSchluessel(String nummer) {
		if (nummer.length() == 2) {
			return nummer;
		}
		return "0" + nummer;
	}

	/**
	 * @param index the index to set
	 */
	void setIndex(int index) {
		this.index = index;
	}

	/**
	 * @param schluessel the schluessel to set
	 */
	void setSchluessel(String schluessel) {
		this.schluessel = schluessel;
	}

	/**
	 * @param punkte the punkte to set
	 */
	void setPunkte(int punkte) {
		this.punkte = punkte;
	}

	/**
	 * @param antwortA the antwortA to set
	 */
	void setAntwortA(String antwortA) {
		this.antwortA = antwortA;
	}

	/**
	 * @param antwortB the antwortB to set
	 */
	void setAntwortB(String antwortB) {
		this.antwortB = antwortB;
	}

	/**
	 * @param antwortC the antwortC to set
	 */
	void setAntwortC(String antwortC) {
		this.antwortC = antwortC;
	}

	/**
	 * @param antwortD the antwortD to set
	 */
	void setAntwortD(String antwortD) {
		this.antwortD = antwortD;
	}

	/**
	 * @param antwortE the antwortE to set
	 */
	void setAntwortE(String antwortE) {
		this.antwortE = antwortE;
	}

	/**
	 * @param loesungsbuchstabe the loesungsbuchstabe to set
	 */
	void setLoesungsbuchstabe(MCAntwortbuchstaben loesungsbuchstabe) {
		this.loesungsbuchstabe = loesungsbuchstabe;
	}
}
