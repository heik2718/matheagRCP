/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.domain.schulen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.Assert;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.IMatheAGObjectNames;

/**
 * <p>
 * NOTE: THIS WIDGET AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class Schule extends AbstractMatheAGObject {

	private static final long serialVersionUID = -574479296153740331L;

	private String schluessel;

	private String name;

	private String anschrift;

	private String telefonnummer;

	private Land land;

	private List<MinikaenguruTeilnahme> minikaenguruTeilnahmen = new ArrayList<MinikaenguruTeilnahme>();

	private boolean miniTeilnahmenLoaded = false;

	/**
   *
   */
	public Schule() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param teilnahme
	 */
	public void addMinikaenguruTeilnahme(MinikaenguruTeilnahme teilnahme) {
		Assert.isNotNull(teilnahme);
		if (minikaenguruTeilnahmen == null) {
			minikaenguruTeilnahmen = new ArrayList<MinikaenguruTeilnahme>();
		}
		if (!teilnahme.isNew() && minikaenguruTeilnahmen.contains(teilnahme)) {
			return;
		}
		boolean added = minikaenguruTeilnahmen.add(teilnahme);
		if (added && !isAdjusting()) {
			firePropertyChange(ISchuleNames.PROP_MINI_TEILNAHMEN, null, minikaenguruTeilnahmen);
		}
	}

	/**
	 * @param teilnahme
	 */
	public boolean removeMinikaenguruTeilnahme(MinikaenguruTeilnahme teilnahme) {
		boolean result = minikaenguruTeilnahmen.remove(teilnahme);
		if (result && !isAdjusting()) {
			firePropertyChange(ISchuleNames.PROP_MINI_TEILNAHMEN, minikaenguruTeilnahmen, null);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.domain.AbstractMatheAGObject#getKey()
	 */
	@Override
	public Object getKey() {
		return schluessel;
	}

	/**
	 * @return the name
	 */
	public final String getName() {
		return name;
	}

	/**
	 * @param pName the name to set
	 */
	public final void setName(String pName) {
		firePropertyChange(ISchuleNames.PROP_NAME, this.name, name = pName);
	}

	/**
	 * @return the anschrift
	 */
	public final String getAnschrift() {
		return anschrift;
	}

	/**
	 * @param pAnschrift the anschrift to set
	 */
	public final void setAnschrift(String pAnschrift) {
		firePropertyChange(ISchuleNames.PROP_ANSCHRIFT, this.anschrift, anschrift = pAnschrift);
	}

	/**
	 * @return the telefonnummer
	 */
	public final String getTelefonnummer() {
		return telefonnummer;
	}

	/**
	 * @param pTelefonnummer the telefonnummer to set
	 */
	public final void setTelefonnummer(String pTelefonnummer) {
		firePropertyChange(ISchuleNames.PROP_TELEFONNUMMER, this.telefonnummer, telefonnummer = pTelefonnummer);
	}

	/**
	 * @return the bundesland
	 */
	public final Land getLand() {
		return land;
	}

	/**
	 * @param pBundesland the bundesland to set
	 */
	public final void setLand(Land pLand) {
		firePropertyChange(ISchuleNames.PROP_LAND, this.land, land = pLand);
	}

	/**
	 * @return the minikaenguruTeilnahmen
	 */
	public final List<MinikaenguruTeilnahme> getMinikaenguruTeilnahmen() {
		return Collections.unmodifiableList(minikaenguruTeilnahmen);
	}

	/**
	 * @return the miniTeilnahmenLoaded
	 */
	public final boolean isMiniTeilnahmenLoaded() {
		return miniTeilnahmenLoaded;
	}

	/**
	 * @param pMiniTeilnahmenLoaded the miniTeilnahmenLoaded to set
	 */
	public final void setMiniTeilnahmenLoaded(boolean pMiniTeilnahmenLoaded) {
		miniTeilnahmenLoaded = pMiniTeilnahmenLoaded;
	}

	/**
	 * @return the schluessel
	 */
	public final String getSchluessel() {
		return schluessel;
	}

	/**
	 * @param pSchluessel the schluessel to set
	 */
	public final void setSchluessel(String pSchluessel) {
		firePropertyChange(IMatheAGObjectNames.PROP_SCHLUESSEL, this.schluessel, schluessel = pSchluessel);
	}
}
