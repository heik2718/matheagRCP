/**
 *
 */
package de.egladil.mathejungalt.domain.types;

import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * @author heike
 */
public class LongDateComposedKey {

	/** */
	private Long id = new Long(0);

	/** */
	private Date datum = new Date();

	/**
	 * @param pId
	 * @param pDatum
	 */
	public LongDateComposedKey(Long pId, Date pDatum) {
		super();
		id = pId;
		datum = pDatum;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object pObj) {
		if (this == pObj)
			return true;
		if (pObj.getClass() != this.getClass())
			return false;
		LongDateComposedKey param = (LongDateComposedKey) pObj;
		EqualsBuilder eb = new EqualsBuilder();
		eb.append(id, param.getId());
		eb.append(datum, param.getDatum());
		return eb.isEquals();

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		HashCodeBuilder hb = new HashCodeBuilder();
		hb.append(id);
		hb.append(datum);
		return hb.toHashCode();
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the datum
	 */
	public Date getDatum() {
		return datum;
	}
}
