/**
 * 
 */
package de.egladil.mathejungalt.domain.aufgaben;

/**
 * This generated interface defines constants for all attributes and associatations in
 * {@link de.egladil.mathe.domain.quelle.Buchquelle}.
 * <p>
 * These constants are useful for example when building criterias.
 * 
 * @author heike
 */
public interface IHeftNames {

	public static final String PROP_BEENDET = "beendet";

	public static final String PROP_TITEL = "titel";

	public static final String PROP_AUFGABEN = "aufgaben";

	public static final int LENGTH_KEY = 2;

	public static final int LENGTH_TITEL = 100;
}
