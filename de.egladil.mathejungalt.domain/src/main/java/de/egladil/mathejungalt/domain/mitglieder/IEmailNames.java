/**
 * 
 */
package de.egladil.mathejungalt.domain.mitglieder;

/**
 * This generated interface defines constants for all attributes and associatations in {@link Mitglied}
 * <p>
 * These constants are useful for example when building criterias.
 * 
 * @author heike
 */
public interface IEmailNames {

	public static final String PROP_EMAIL = "email";
}
