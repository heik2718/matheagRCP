/**
 * 
 */
package de.egladil.mathejungalt.domain.mitglieder;

/**
 * This generated interface defines constants for all attributes and associatations in {@link Mitglied}
 * <p>
 * These constants are useful for example when building criterias.
 * 
 * @author heike
 */
public interface IMitgliedNames {

	public static final String PROP_VORNAME = "vorname";

	public static final String PROP_KONTAKT = "kontakt";

	public static final String PROP_KLASSE = "klasse";

	public static final String PROP_NACHNAME = "nachname";

	public static final String PROP_ALTER = "alter";

	public static final String SQL_ALTER = "jahre";

	public final static String PROP_FOTOTITEL = "fototitel";

	public final static String SQL_GEBURTSMONAT = "gebmonat";

	public final static String SQL_GEBURTSJAHR = "gebjahr";

	public final static String PROP_GEBURTSMONAT = "geburtsmonat";

	public final static String PROP_GEBURTSJAHR = "geburtsjahr";

	public static final String PROP_ERFINDERPUNKTE = "erfinderpunkte";

	public static final String PROP_SERIENTEILNAHMEN = "serienteilnahmen";

	public static final String PROP_DIPLOME = "diplome";

	public static final String PROP_ANZAHL_DIPLOME = "anzDiplome";

	public static final int LENGTH_SCHLUESSEL = 3;

	public static final int LENGTH_VORNAME = 50;

	public static final int LENGTH_KONTAKT = 150;

	public static final int LENGTH_NACHNAME = 50;

	public static final int LENGTH_ALTER = 3;

	public static final int LENGTH_FOTOTITEL = 30;

	public static final int LENGTH_GEBURTSMONAT = 2;

	public static final int LENGTH_GEBURTSJAHR = 4;
}
