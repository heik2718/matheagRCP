/**
 *
 */
package de.egladil.mathejungalt.domain.types;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import de.egladil.mathejungalt.domain.mcraetsel.MCArchivraetsel;
import de.egladil.mathejungalt.domain.mcraetsel.MCAufgabe;

/**
 * @author heike
 */
public class MCAufgabeRaetselComposedKey {

	/** */
	private final MCAufgabe aufgabe;

	/** */
	private final MCArchivraetsel raetsel;

	/**
	 * @param pAufgabe
	 * @param pRaetsel
	 */
	public MCAufgabeRaetselComposedKey(MCAufgabe pAufgabe, MCArchivraetsel pRaetsel) {
		super();
		aufgabe = pAufgabe;
		raetsel = pRaetsel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object pObj) {
		if (this == pObj)
			return true;
		if (!this.getClass().isAssignableFrom(pObj.getClass())) {
			return false;
		}
		MCAufgabeRaetselComposedKey param = (MCAufgabeRaetselComposedKey) pObj;
		EqualsBuilder eb = new EqualsBuilder();
		eb.append(aufgabe, param.getAufgabe());
		eb.append(raetsel, param.getRaetsel());
		return eb.isEquals();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		HashCodeBuilder hb = new HashCodeBuilder();
		hb.append(aufgabe);
		hb.append(raetsel);
		return hb.toHashCode();
	}

	/**
	 * @return the aufgabe
	 */
	public final MCAufgabe getAufgabe() {
		return aufgabe;
	}

	/**
	 * @return the raetsel
	 */
	public final MCArchivraetsel getRaetsel() {
		return raetsel;
	}
}
