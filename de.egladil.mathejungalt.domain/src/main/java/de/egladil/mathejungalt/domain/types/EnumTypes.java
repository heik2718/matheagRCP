/**
 * 
 */
package de.egladil.mathejungalt.domain.types;

/**
 * @author heike
 */
public class EnumTypes {

	/**
	 * Medienarten sind B(uch), Z(eitschrift)
	 * 
	 * @author heike
	 */
	public static enum Medienart {
		B,
		Z
	};

	/**
	 * Quelenarten sind B(uchquelle), K(indquelle), M(itgliederquelle), N(ullquelle), Z(eitschriftquelle)
	 * 
	 * @author heike
	 */
	public static enum Quellenart {
		B,
		G,
		K,
		N,
		Z
	};

	/**
	 * Themen sind A(rithmetik), G(eometrie), L(ogik), K(ombinatorik), W(ahrscheinlichkeit),<br>
	 * Z(ahlentheorie)
	 * 
	 * @author heike
	 */
	public static enum Thema {
		A,
		G,
		L,
		K,
		W,
		Z
	};

	/**
	 * Aufgabenarten sind E(igenbau), N(achbau), Z(itat)
	 * 
	 * @author heike
	 */
	public static enum Aufgabenart {
		E,
		N,
		Z
	};

	/**
	 * Zwecke sind K(aenguru), S(erien), M(onoid)
	 * 
	 * @author heike
	 */
	public static enum Aufgabenzweck {
		K,
		M,
		S
	};

	/**
	 * Jahreszeiten sind I(mmer), F(r�hling), K(arneval), O(stern), S(ommer), H(erbst),<br>
	 * W(inter), X(mas)
	 * 
	 * @author heike
	 */
	public static enum Jahreszeit {
		I,
		F,
		K,
		O,
		S,
		H,
		W,
		X
	};

	/**
	 * 
	 */
	private EnumTypes() {
	}
}
