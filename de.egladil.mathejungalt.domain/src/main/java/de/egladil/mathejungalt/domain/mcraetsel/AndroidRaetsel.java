/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.domain.mcraetsel;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "quiz")
public class AndroidRaetsel {

	@XmlElement(name = "kategorie")
	private String kategorie;

	@XmlElement(name = "nummer")
	private String nummer;

	@XmlElement(name = "item")
	private List<AndroidReatselItem> items;

	/**
   *
   */
	public AndroidRaetsel() {
	}

	/**
	 * @param item
	 */
	void addItem(AndroidReatselItem item) {
		if (items == null) {
			items = new ArrayList<AndroidReatselItem>();
		}
		items.add(item);
	}

	/**
	 * @param raetsel
	 * @return AndroidRaetsel
	 */
	public static AndroidRaetsel createFromMCArchivRaetsel(MCArchivraetsel raetsel) {
		AndroidRaetsel result = new AndroidRaetsel();
		result.setNummer(raetsel.getSchluessel());
		result.setKategorie(raetsel.getStufe().mcAufgabenstufeToAndroidStufeString(raetsel.getTitel()));
		int index = 0;
		for (MCArchivraetselItem archivraetselitem : raetsel.getItemsSorted()) {
			AndroidReatselItem item = AndroidReatselItem.createFromMCArchivRaetselItem(archivraetselitem, index);
			result.addItem(item);
			index++;
		}
		return result;
	}

	/**
	 * @param kategorie the kategorie to set
	 */
	void setKategorie(String kategorie) {
		this.kategorie = kategorie;
	}

	/**
	 * @param nummer the nummer to set
	 */
	void setNummer(String nummer) {
		this.nummer = nummer;
	}
}
