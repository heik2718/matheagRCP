/**
 *
 */
package de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenzweck;

/**
 * Ein Minikänguru-Wettbewerb.
 *
 * @author heike
 */
public class Minikaenguru extends AbstractMatheAGObject implements IAufgabensammlung {

	/** */
	private static final long serialVersionUID = 5308738279830394337L;

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(Minikaenguru.class);

	/** */
	private int beendet;

	/** */
	private Integer jahr;

	/** */
	private List<Minikaenguruitem> minikaenguruitems;

	/** */
	private List<Kontakt> mailingliste = new ArrayList<Kontakt>();

	/** */
	private boolean itemsLoaded = false;

	/** */
	private boolean mailinglisteLoaded = false;

	/**
	 *
	 */
	public Minikaenguru() {
		super();
		minikaenguruitems = new ArrayList<Minikaenguruitem>();
		itemsLoaded = false;
	}

	/**
	 *
	 */
	public void initItems() {
		if (itemsLoaded)
			return;
		minikaenguruitems = new ArrayList<Minikaenguruitem>();
	}

	/**
	 *
	 */
	public void clearItems() {
		if (!itemsLoaded)
			return;
		minikaenguruitems.clear();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung#anzahlAufgaben()
	 */
	public int anzahlAufgaben() {
		if (itemsLoaded) {
			return minikaenguruitems.size();
		}
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.domain.AbstractMatheAGObject#getKey()
	 */
	@Override
	public Object getKey() {
		return jahr;
	}

	/**
	 * @return the beendet
	 */
	public int getBeendet() {
		return beendet;
	}

	/**
	 * @param pBeendet the beendet to set
	 */
	public void setBeendet(int pBeendet) {
		firePropertyChange(IMinikaenguruNames.PROP_BEENDET, beendet, beendet = pBeendet);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung#canAdd(de.egladil.mathejungalt.domain.aufgaben
	 * .Aufgabe)
	 */
	@Override
	public boolean canAdd(Aufgabe pAufgabe) {
		if (beendet == 1) {
			return false;
		}
		if (pAufgabe == null) {
			return false;
		}
		if (Aufgabe.LOCKED.equals(pAufgabe.getGesperrtFuerWettbewerb())) {
			return false;
		}
		return Aufgabenzweck.K.equals(pAufgabe.getZweck());
	}

	/**
	 * @param pKontakt
	 * @return
	 */
	public boolean addKontakt(Kontakt pKontakt) {
		Assert.isTrue(mailinglisteLoaded, "Ablauf: Mailingliste noch nicht geladen");
		boolean added = false;
		if (!mailingliste.contains(pKontakt)) {
			added = mailingliste.add(pKontakt);
		}
		if (added) {
			firePropertyChange(IMinikaenguruNames.PROP_MAILINGLISTE, null, mailingliste);
		}
		return added;
	}

	/**
	 * @param pKontakt
	 * @return
	 */
	public boolean removeKontakt(Kontakt pKontakt) {
		Assert.isTrue(mailinglisteLoaded, "Ablauf: Mailingliste noch nicht geladen");
		boolean removed = mailingliste.remove(pKontakt);
		if (removed) {
			firePropertyChange(IMinikaenguruNames.PROP_MAILINGLISTE, null, mailingliste);
		}
		return removed;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung#addItem(de.egladil.mathejungalt.domain.
	 * aufgabensammlungen.IAufgabensammlungItem)
	 */
	public boolean addItem(final IAufgabensammlungItem pItem) {
		if (pItem == null) {
			String msg = MatheJungAltException.argumentNullMessage("addItem(IAufgabensammlungItem)");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (!(pItem instanceof Minikaenguruitem)) {
			String msg = "addItem(): unzulässiger Typ " + pItem.getClass().getName();
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		boolean added = minikaenguruitems.add((Minikaenguruitem) pItem);
		if (added) {
			fireItemsChanged();
		}
		return added;
	}

	/**
	 *
	 */
	private void fireItemsChanged() {
		firePropertyChange(IMinikaenguruNames.PROP_MINKAENGURUITEMS, null, minikaenguruitems);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung#removeItem(de.egladil.mathejungalt.domain
	 * .aufgabensammlungen.IAufgabensammlungItem)
	 */
	public boolean removeItem(IAufgabensammlungItem pItem) {
		LOG.debug("Start removeItem()");
		boolean removed = minikaenguruitems.remove(pItem);
		if (removed) {
			fireItemsChanged();
		}
		return removed;
	}

	/**
	 * @return the jahr
	 */
	public Integer getJahr() {
		return jahr;
	}

	/**
	 * @param pJahr the jahr to set
	 */
	public void setJahr(Integer pJahr) {
		firePropertyChange(IMinikaenguruNames.PROP_JAHR, jahr, jahr = pJahr);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung#isItemsLoaded()
	 */
	public boolean isItemsLoaded() {
		return itemsLoaded;
	}

	/**
	 * @param pItemsLoaded the itemsLoaded to set
	 */
	public void setItemsLoaded(boolean pItemsLoaded) {
		itemsLoaded = pItemsLoaded;
	}

	/**
	 * @return the minikaenguruitems
	 */
	public List<Minikaenguruitem> getMinikaenguruitems() {
		return minikaenguruitems;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung#getDomainObject()
	 */
	@Override
	public AbstractMatheAGObject getDomainObject() {
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung#getItems()
	 */
	@Override
	public List<IAufgabensammlungItem> getItems() {
		List<IAufgabensammlungItem> items = new ArrayList<IAufgabensammlungItem>();
		for (Minikaenguruitem item : minikaenguruitems) {
			items.add(item);
		}
		return items;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung#getItems(int)
	 */
	@Override
	public List<IAufgabensammlungItem> getItems(int pStufe) {
		List<IAufgabensammlungItem> items = new ArrayList<IAufgabensammlungItem>();
		for (Minikaenguruitem item : minikaenguruitems) {
			if (item.getAufgabe().getStufe() == pStufe) {
				items.add(item);
			}
		}
		return items;
	}

	/**
	 * @return the mailinglisteLoaded
	 */
	public final boolean isMailinglisteLoaded() {
		return mailinglisteLoaded;
	}

	/**
	 * @param pMailinglisteLoaded the mailinglisteLoaded to set
	 */
	public final void setMailinglisteLoaded(boolean pMailinglisteLoaded) {
		mailinglisteLoaded = pMailinglisteLoaded;
	}

	/**
	 * @return the mailingliste unmodifiable!
	 */
	public final List<Kontakt> getMailingliste() {
		return Collections.unmodifiableList(mailingliste);
	}

	/**
   *
   */
	public void clearMailingliste() {
		mailingliste.clear();
		mailinglisteLoaded = false;
	}
}
