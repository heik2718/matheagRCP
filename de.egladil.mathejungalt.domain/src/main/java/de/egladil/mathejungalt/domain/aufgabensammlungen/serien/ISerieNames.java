/**
 * 
 */
package de.egladil.mathejungalt.domain.aufgabensammlungen.serien;

/**
 * This generated interface defines constants for all attributes and associatations in
 * {@link de.egladil.mathe.domain.quelle.Buchquelle}.
 * <p>
 * These constants are useful for example when building criterias.
 * 
 * @author heike
 */
public interface ISerieNames {

	public static final String PROP_NUMMER = "nummer";

	public static final String PROP_RUNDE = "runde";

	public static final String PROP_SERIENITEMS = "serienitems";

	public static final String PROP_BEENDET = "beendet";

	public static final String PROP_DATUM = "datum";

	public static final String PROP_MONAT_JAHR_TEXT = "monatJahrText";

	public final static int LENGTH_BEENDET = 1;

	public final static int LENGTH_DATUM = 10;

	public final static int LENGTH_MONAT_JAHR_TEXT = 15;

	public final static int LENGTH_NUMMER = 3;

	public final static int LENGTH_RUNDE = 2;
}
