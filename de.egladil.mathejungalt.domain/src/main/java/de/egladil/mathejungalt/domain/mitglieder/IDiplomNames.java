/**
 * 
 */
package de.egladil.mathejungalt.domain.mitglieder;

/**
 * This generated interface defines constants for all attributes and associatations in {@link Mitglied}
 * <p>
 * These constants are useful for example when building criterias.
 * 
 * @author heike
 */
public interface IDiplomNames {

	public static final String SQL_MITGLIED = "mitglied";

	public static final String PROP_ANZAHL_ERF = "anzahlErfinderpunkte";

	public static final String PROP_DATUM = "datum";

	public static final String PROP_MITGLIED = "mitglied";
}
