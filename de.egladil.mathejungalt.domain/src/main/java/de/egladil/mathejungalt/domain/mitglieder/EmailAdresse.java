/**
 * 
 */
package de.egladil.mathejungalt.domain.mitglieder;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;

/**
 * @author winkelv
 */
public class EmailAdresse extends AbstractMatheAGObject {

	/** */
	private static final long serialVersionUID = -5671821266687464002L;

	/** */
	private String email;

	/**
	 * 
	 */
	public EmailAdresse() {
		super();
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param pEmail the email to set
	 */
	public void setEmail(String pEmail) {
		firePropertyChange(IEmailNames.PROP_EMAIL, email, email = pEmail);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.domain.AbstractMatheAGObject#getKey()
	 */
	@Override
	public Object getKey() {
		return getId();
	}
}
