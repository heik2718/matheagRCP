/**
 *
 */
package de.egladil.mathejungalt.domain.aufgabensammlungen;

import java.beans.PropertyChangeListener;

import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;

/**
 * Adapter-Interface für eine Aufgabensammlung-Verknüpfung.
 *
 * @author Heike Winkelvoss (www.egladil.de)
 */
public interface IAufgabensammlungItem {

	public static final String PROP_NUMMER = "nummer";

	public final static String PROP_AUFGABE = "aufgabe";

	public final static int LENGTH_NUMMER = 6;

	/**
	 * Den Listener anmelden
	 *
	 * @param pListener {@link PropertyChangeListener}
	 */
	public void addPropertyChangeListener(final PropertyChangeListener pListener);

	/**
	 * Den Listener anmelden
	 *
	 * @param pListener {@link PropertyChangeListener}
	 */
	public void removePropertyChangeListener(final PropertyChangeListener pListener);

	/**
	 * @param pAufgabe
	 */
	void setAufgabe(Aufgabe pAufgabe);

	/**
	 * Gibt die verknüpfte Aufgabe zurück.
	 *
	 * @return
	 */
	Aufgabe getAufgabe();

	/**
	 * @return
	 */
	IAufgabensammlung getAufgabensammlung();

	/**
	 * Eine Nummer
	 *
	 * @return
	 */
	String getNummer();

	/**
	 * @param pNUmmer
	 */
	void setNummer(String pNUmmer);

	/**
	 *
	 * @return
	 */
	Long getId();
}
