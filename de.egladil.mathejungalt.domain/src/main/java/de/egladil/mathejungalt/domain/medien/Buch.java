/**
 * 
 */
package de.egladil.mathejungalt.domain.medien;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.types.EnumTypes.Medienart;

/**
 * Ein Buch mit Titel und Autoren
 * 
 * @author heike
 */
public class Buch extends AbstractPrintmedium {

	/** */
	private static final long serialVersionUID = 1218835870200573217L;

	/** */
	private List<Autor> autoren;

	/** */
	private boolean autorenLoaded = false;

	/**
	 * 
	 */
	public Buch() {
		super();
		autorenLoaded = false;
	}

	/**
	 * @param pAutor
	 */
	public final void addAutor(final Autor pAutor) {
		if (pAutor == null) {
			throw new MatheJungAltException("Not allowed to add null Autor.");
		}
		autoren.add(pAutor);
		if (autorenLoaded) {
			fireAutorChanged();
		}
	}

	/**
	 * Prüft, ob die Autoren bereits geladen sind. Falls nicht, wird eine Exception geworfen.
	 * 
	 * @throws MatheAGDomainRuntimeException
	 */
	private void checkAutorenLoaded() {
		if (!autorenLoaded) {
			throw new MatheJungAltException("Die Autoren zum Buch sind noch nicht geladen.");
		}
	}

	/**
	 * 
	 */
	private void fireAutorChanged() {
		firePropertyChange(IMedienNames.PROP_AUTOREN, null, autoren.iterator());
	}

	/**
	 * @param pAutor
	 */
	public final void removeAutor(final Autor pAutor) {
		if (pAutor == null) {
			throw new IllegalArgumentException("Not allowed to remove null Autor.");
		}
		checkAutorenLoaded();
		boolean removed = autoren.remove(pAutor);
		if (removed)
			fireAutorChanged();
	}

	/**
	 * @param pAutoren
	 */
	public final void removeAutoren(final Collection<Autor> pAutoren) {
		if (pAutoren == null) {
			throw new IllegalArgumentException("Not allowed to remove null Collection<Autor>.");
		}
		checkAutorenLoaded();
		autoren.removeAll(pAutoren);
		fireAutorChanged();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.domain.medien.AbstractPrintmedium#getArt()
	 */
	@Override
	public Medienart getArt() {
		return Medienart.B;
	}

	/**
	 * 
	 */
	public void clearAutoren() {
		if (autorenLoaded)
			autoren.clear();
	}

	/**
	 * Initialisiert die Autoren
	 */
	public void initAutoren() {
		if (autorenLoaded)
			return;
		autoren = new ArrayList<Autor>();
	}

	/**
	 * @return
	 */
	public final int anzahlAutoren() {
		if (autoren == null)
			return 0;

		return autoren.size();
	}

	/**
	 * Achtung: Methode gibt null zur�ck, wenn die Autoren noch nicht geladen wurden.
	 * 
	 * @return the autoren
	 */
	public List<Autor> getAutoren() {
		return autoren;
	}

	/**
	 * @return the autorenLoaded
	 */
	public boolean isAutorenLoaded() {
		return autorenLoaded;
	}

	/**
	 * @param pAutorenLoaded the autorenLoaded to set
	 */
	public void setAutorenLoaded(boolean pAutorenLoaded) {
		autorenLoaded = pAutorenLoaded;
	}
}
