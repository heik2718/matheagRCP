/**
 * 
 */
package de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter;

/**
 * @author Heike Winkelvoss (www.egladil.de)
 */
public interface IArbeitsblattItemNames {

	public static final String PROP_ARBEITSBLATT = "arbeitsblatt";

	public static final int LENGTH_KEY = 4;
}
