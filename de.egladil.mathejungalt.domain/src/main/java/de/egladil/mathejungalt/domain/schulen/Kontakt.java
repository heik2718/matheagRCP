/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.domain.schulen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.Assert;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.IMatheAGObjectNames;

/**
 * <p>
 * NOTE: THIS WIDGET AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class Kontakt extends AbstractMatheAGObject {

	private static final long serialVersionUID = -5918955557273492499L;

	private String schluessel;

	private String name;

	private String email;

	private boolean abo;

	private List<Schulkontakt> schulkontakte = new ArrayList<Schulkontakt>();

	private boolean schulkontakteGeladen = false;

	/**
   *
   */
	public Kontakt() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param kontakt
	 */
	public void addSchulkontakt(Schulkontakt kontakt) {
		Assert.isNotNull(kontakt);
		if (schulkontakte == null) {
			schulkontakte = new ArrayList<Schulkontakt>();
		}
		if (!kontakt.isNew() && schulkontakte.contains(kontakt)) {
			return;
		}
		boolean added = schulkontakte.add(kontakt);
		if (added) {
			firePropertyChange(IKontaktNames.PROP_SCHULKONTAKTE, null, schulkontakte);
		}
	}

	/**
	 * @param kontakt
	 */
	public boolean removeSchulkontakt(Schulkontakt kontakt) {
		boolean removed = schulkontakte.remove(kontakt);
		if (removed) {
			firePropertyChange(IKontaktNames.PROP_SCHULKONTAKTE, schulkontakte, null);
		}
		return removed;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.domain.AbstractMatheAGObject#getKey()
	 */
	@Override
	public Object getKey() {
		return schluessel;
	}

	/**
	 * @return the name
	 */
	public final String getName() {
		return name;
	}

	/**
	 * @param pName the name to set
	 */
	public final void setName(String pName) {
		firePropertyChange(IKontaktNames.PROP_NAME, this.name, name = pName);
		;
	}

	/**
	 * @return the email
	 */
	public final String getEmail() {
		return email;
	}

	/**
	 * @param pEmail the email to set
	 */
	public final void setEmail(String pEmail) {
		firePropertyChange(IKontaktNames.PROP_EMAIL, this.email, email = pEmail);
	}

	/**
	 * @return the abo
	 */
	public final boolean isAbo() {
		return abo;
	}

	/**
	 * @param pAbo the abo to set
	 */
	public final void setAbo(boolean pAbo) {
		firePropertyChange(IKontaktNames.PROP_ABO, this.abo, abo = pAbo);
	}

	/**
	 * @return the schulkontakte
	 */
	public final List<Schulkontakt> getSchulkontakte() {
		return Collections.unmodifiableList(schulkontakte);
	}

	/**
	 * @return the schulkontakteGeladen
	 */
	public final boolean isSchulkontakteGeladen() {
		return schulkontakteGeladen;
	}

	/**
	 * @param pSchulkontakteGeladen the schulkontakteGeladen to set
	 */
	public final void setSchulkontakteGeladen(boolean pSchulkontakteGeladen) {
		schulkontakteGeladen = pSchulkontakteGeladen;
	}

	/**
	 * @return the schluessel
	 */
	public final String getSchluessel() {
		return schluessel;
	}

	/**
	 * @param pSchluessel the schluessel to set
	 */
	public final void setSchluessel(String pSchluessel) {
		firePropertyChange(IMatheAGObjectNames.PROP_SCHLUESSEL, this.schluessel, schluessel = pSchluessel);
	}

	/**
	 * @param schule
	 * @return
	 */
	public boolean canAdd(Schule schule) {
		if (schulkontakteGeladen) {
			for (Schulkontakt k : schulkontakte) {
				if (k.getSchule().equals(schule)) {
					return false;
				}
			}
			return true;
		}
		return false;

	}
}
