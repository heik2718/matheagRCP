/**
 * 
 */
package de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.types.AufgabeArbeitsblattComposedKey;

/**
 * Die Aufgaben des Arbeitsblatts, aufgehuebscht um ein paar Attribute.
 * 
 * @author heike
 */
public class Arbeitsblattitem extends AbstractMatheAGObject implements IAufgabensammlungItem {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1226711776133759078L;

	/** */
	private String nummer;

	/** */
	private Aufgabe aufgabe;

	/** */
	private Arbeitsblatt arbeitsblatt;

	/**
	 * 
	 */
	public Arbeitsblattitem() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.domain.AbstractMatheAGObject#getKey()
	 */
	@Override
	public Object getKey() {
		if (aufgabe == null && arbeitsblatt == null)
			return getId();
		return new AufgabeArbeitsblattComposedKey(aufgabe, arbeitsblatt);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(nummer);
		sb.append("-");
		sb.append(arbeitsblatt.getKey());
		sb.append("-");
		sb.append(aufgabe.getKey());
		return sb.toString();
	}

	/**
	 * @return the nummer
	 */
	public String getNummer() {
		return nummer;
	}

	/**
	 * @param pNummer the nummer to set
	 */
	public void setNummer(String pNummer) {
		firePropertyChange(IAufgabensammlungItem.PROP_NUMMER, nummer, nummer = pNummer);
	}

	/**
	 * @return the aufgabe
	 */
	public Aufgabe getAufgabe() {
		return aufgabe;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem#getAufgabensammlung()
	 */
	@Override
	public IAufgabensammlung getAufgabensammlung() {
		return arbeitsblatt;
	}

	/**
	 * @param pAufgabe the aufgabe to set
	 */
	public void setAufgabe(Aufgabe pAufgabe) {
		firePropertyChange(IAufgabensammlungItem.PROP_AUFGABE, aufgabe, aufgabe = pAufgabe);
	}

	/**
	 * @return the minikaenguru
	 */
	public Arbeitsblatt getArbeitsblatt() {
		return arbeitsblatt;
	}

	/**
	 * @param pArbeitsblatt the minikaenguru to set
	 */
	public void setArbeitsblatt(Arbeitsblatt pArbeitsblatt) {
		firePropertyChange(IArbeitsblattItemNames.PROP_ARBEITSBLATT, arbeitsblatt, arbeitsblatt = pArbeitsblatt);
	}
}
