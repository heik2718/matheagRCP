/**
 * 
 */
package de.egladil.mathejungalt.domain.types;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;

/**
 * @author heike
 */
public class AufgabeArbeitsblattComposedKey {

	/** */
	private Aufgabe aufgabe;

	/** */
	private Arbeitsblatt arbeitsblatt;

	/**
	 * @param pAufgabe
	 * @param pArbeitsblatt
	 */
	public AufgabeArbeitsblattComposedKey(Aufgabe pAufgabe, Arbeitsblatt pArbeitsblatt) {
		super();
		aufgabe = pAufgabe;
		arbeitsblatt = pArbeitsblatt;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object pObj) {
		if (this == pObj)
			return true;
		if (!this.getClass().isAssignableFrom(pObj.getClass())) {
			return false;
		}
		AufgabeArbeitsblattComposedKey param = (AufgabeArbeitsblattComposedKey) pObj;
		EqualsBuilder eb = new EqualsBuilder();
		eb.append(aufgabe, param.getAufgabe());
		eb.append(arbeitsblatt, param.getArbeitsblatt());
		return eb.isEquals();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		HashCodeBuilder hb = new HashCodeBuilder();
		hb.append(aufgabe);
		hb.append(arbeitsblatt);
		return hb.toHashCode();
	}

	/**
	 * @return the aufgabe
	 */
	public Aufgabe getAufgabe() {
		return aufgabe;
	}

	/**
	 * @return the serie
	 */
	public Arbeitsblatt getArbeitsblatt() {
		return arbeitsblatt;
	}
}
