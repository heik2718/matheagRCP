/**
 * 
 */
package de.egladil.mathejungalt.domain.quellen;

/**
 * This generated interface defines constants for all attributes and associatations in
 * {@link de.egladil.mathe.domain.quelle.Buchquelle}.
 * <p>
 * These constants are useful for example when building criterias.
 * 
 * @author heike
 */
public interface IQuelleNames {

	public static final String ID = "id";

	public static final String SCHLUESSEL = "schluessel";

	public static final int LENGTH_SCHLUESSEL = 6;

	public static final int LENGTH_SEITE = 5;

	public static final String PROP_ART = "art";
}
