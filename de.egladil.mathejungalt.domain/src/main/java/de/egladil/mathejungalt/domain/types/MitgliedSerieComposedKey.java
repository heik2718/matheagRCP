/**
 * 
 */
package de.egladil.mathejungalt.domain.types;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;

/**
 * Zusammengesetzter Schluessel fuer Serienteilnahmen.
 * 
 * @author heike
 */
public class MitgliedSerieComposedKey {

	/** */
	private Mitglied mitglied;

	/** */
	private Serie serie;

	/**
	 * @param pMitglied
	 * @param pSerie
	 */
	public MitgliedSerieComposedKey(Mitglied pMitglied, Serie pSerie) {
		super();
		mitglied = pMitglied;
		serie = pSerie;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object pObj) {
		if (this == pObj)
			return true;
		if (!this.getClass().isAssignableFrom(pObj.getClass())) {
			return false;
		}
		MitgliedSerieComposedKey param = (MitgliedSerieComposedKey) pObj;
		EqualsBuilder eb = new EqualsBuilder();
		eb.append(mitglied, param.getMitglied());
		eb.append(serie, param.getSerie());
		return eb.isEquals();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		HashCodeBuilder hb = new HashCodeBuilder();
		hb.append(mitglied);
		hb.append(serie);
		return hb.toHashCode();
	}

	/**
	 * @return the aufgabe
	 */
	public Mitglied getMitglied() {
		return mitglied;
	}

	/**
	 * @return the serie
	 */
	public Serie getSerie() {
		return serie;
	}
}
