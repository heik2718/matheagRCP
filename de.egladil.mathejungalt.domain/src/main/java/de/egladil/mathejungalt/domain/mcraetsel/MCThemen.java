/**
 *
 */
package de.egladil.mathejungalt.domain.mcraetsel;

import java.util.ArrayList;
import java.util.List;

/**
 * @author heike
 */
public enum MCThemen {
	ARITHMETIK("A", "Arithmetik"),

	GEOMETRIE("G", "Geometrie"),

	LOGIK("L", "Logik"),

	KOMBINATORIK("K", "Kombinatorik"),

	ANALYSIS("N", "Analysis"),

	WAHRSCHEINLICHKEITSRECHNUNG("W", "Wahrscheinlichkeitsrechnung"),

	PHYSIK("P", "Physik"),

	ZAHLENTHEORIE("Z", "Zahlentheorie");

	private final String kurzbezeichnung;

	private final String label;

	/**
	 * @param pKurzbezeichnung
	 */
	private MCThemen(String pKurzbezeichnung, String pLabel) {
		kurzbezeichnung = pKurzbezeichnung;
		label = pLabel;
	}

	/**
	 * @return the label
	 */
	public String getKurzbezeichnung() {
		return kurzbezeichnung;
	}

	/**
	 * @return
	 */
	public static String[] toLabels() {
		List<String> liste = new ArrayList<String>(values().length);
		for (MCThemen thema : values()) {
			liste.add(thema.getLabel());
		}
		return liste.toArray(new String[0]);
	}

	/**
	 * @return the label
	 */
	public final String getLabel() {
		return label;
	}

	/**
	 * @param pLabel
	 * @return
	 */
	public static MCThemen valueOfLabel(String pLabel) {
		for (MCThemen thema : values()) {
			if (thema.getLabel().equals(pLabel)) {
				return thema;
			}
		}
		return null;
	}

	/**
	 * @param pLabel
	 * @return
	 */
	public static MCThemen valueOfKurzbezeichnung(String pLabel) {
		for (MCThemen thema : values()) {
			if (thema.getKurzbezeichnung().equals(pLabel)) {
				return thema;
			}
		}
		return null;
	}

}
