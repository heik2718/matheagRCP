/**
 * 
 */
package de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru;

/**
 * @author Heike Winkelvoss (www.egladil.de)
 */
public interface IMinikaenguruNames {

	public static final String PROP_MINKAENGURUITEMS = "minikaenguruitems";

	public static final String PROP_BEENDET = "beendet";

	public static final String PROP_JAHR = "jahr";

	public static final String PROP_MAILINGLISTE = "mailingliste";

	public static final int LENGTH_KEY = 5;

	public static final String SQL_AUFGABE = "aufgabe";

	public static final String SQL_MINIKAENGURU = "minikaenguru";

}
