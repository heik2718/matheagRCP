/**
 * 
 */
package de.egladil.mathejungalt.domain.schulen;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;

/**
 * Zusammengesetzter Schluessel fuer Minikänguruteilnahmen.
 * 
 * @author heike
 */
public class SchuleMinikaenguruComposedKey {

	/** */
	private Schule schule;

	/** */
	private Minikaenguru minikaenguru;

	/**
	 * @param pSchule
	 * @param pMinikaenguru
	 */
	public SchuleMinikaenguruComposedKey(Schule pSchule, Minikaenguru pMinikaenguru) {
		super();
		schule = pSchule;
		minikaenguru = pMinikaenguru;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object pObj) {
		if (this == pObj)
			return true;
		if (!this.getClass().isAssignableFrom(pObj.getClass())) {
			return false;
		}
		SchuleMinikaenguruComposedKey param = (SchuleMinikaenguruComposedKey) pObj;
		EqualsBuilder eb = new EqualsBuilder();
		eb.append(schule, param.getSchule());
		eb.append(minikaenguru, param.getMinikaenguru());
		return eb.isEquals();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		HashCodeBuilder hb = new HashCodeBuilder();
		hb.append(schule);
		hb.append(minikaenguru);
		return hb.toHashCode();
	}

	/**
	 * @return the aufgabe
	 */
	public Schule getSchule() {
		return schule;
	}

	/**
	 * @return the serie
	 */
	public Minikaenguru getMinikaenguru() {
		return minikaenguru;
	}
}
