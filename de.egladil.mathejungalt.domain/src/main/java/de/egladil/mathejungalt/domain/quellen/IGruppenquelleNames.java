/**
 * 
 */
package de.egladil.mathejungalt.domain.quellen;

/**
 * This generated interface defines constants for all attributes and associatations in
 * {@link de.egladil.mathe.domain.quelle.Buchquelle}.
 * <p>
 * These constants are useful for example when building criterias.
 * 
 * @author heike
 */
public interface IGruppenquelleNames extends IQuelleNames {

	public static final String PROP_ITEMS = "items";
}
