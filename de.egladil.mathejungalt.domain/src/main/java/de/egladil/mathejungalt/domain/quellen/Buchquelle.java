/**
 * 
 */
package de.egladil.mathejungalt.domain.quellen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mathejungalt.domain.medien.Buch;
import de.egladil.mathejungalt.domain.types.EnumTypes.Quellenart;

/**
 * @author heike
 */
public class Buchquelle extends AbstractQuelle {

	/** */
	private static final long serialVersionUID = -5076243206674544187L;

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(Buchquelle.class);

	/** */
	private Buch buch;

	/** */
	private Integer seite;

	/**
	 * 
	 */
	public Buchquelle() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.domain.quellen.AbstractQuelle#toString()
	 */
	@Override
	public String toString() {
		if (LOG.isDebugEnabled())
			LOG.debug("Start toString()");
		String string = super.toString();
		if (buch != null) {
			string += "-" + buch.getTitel();
		}
		if (seite != null) {
			string += "(" + seite.intValue() + ")";
		}
		return string;
	}

	/**
	 * @return the buch
	 */
	public Buch getBuch() {
		return buch;
	}

	/**
	 * @param pBuch the buch to set
	 */
	public void setBuch(Buch pBuch) {
		firePropertyChange(IBuchquelleNames.PROP_BUCH, buch, buch = pBuch);
	}

	/**
	 * @return the seite
	 */
	public Integer getSeite() {
		return seite;
	}

	/**
	 * @param pSeite the seite to set
	 */
	public void setSeite(Integer pSeite) {
		firePropertyChange(IBuchquelleNames.PROP_SEITE, seite, seite = pSeite);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.domain.quellen.AbstractQuelle#getArt()
	 */
	@Override
	public Quellenart getArt() {
		return Quellenart.B;
	}
}
