/**
 *
 */
package de.egladil.mathejungalt.domain.mcraetsel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.eclipse.core.runtime.Assert;

import de.egladil.base.exceptions.MatheJungAltInconsistentEntityException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.types.MCAufgabenstufen;

/**
 * Dies sind die Informationen über eine multiple choice Aufgabe, aus denen sich ein LaTeX-Baustein generieren lässt und
 * das die Info für die Bewertung der Antwort enthält.
 *
 * @author heike
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "mcMatheAufgabe")
public class MCAufgabe extends AbstractMatheAGObject implements IMatheAGEntity {

	@XmlTransient
	private static final long serialVersionUID = -1202070357305457667L;

	@XmlTransient
	public static final String DEFAULT_QUELLE = "Heike Winkelvoß";

	@XmlTransient
	private String gesperrtFuerRaetsel;

	@XmlElement(name = "schluessel")
	private String schluessel;

	@XmlElement(name = "thema")
	private MCThemen thema;

	@XmlElement(name = "art")
	private MCAufgabenarten art = MCAufgabenarten.E;

	@XmlElement(name = "stufe")
	private MCAufgabenstufen stufe;

	@XmlElement(name = "titel")
	private String titel;

	@XmlElement(name = "bemerkung")
	private String bemerkung;

	@XmlElement(name = "punkte")
	private Integer punkte;

	@XmlElement(name = "anzahlAntwortvorschlaege")
	private int anzahlAntwortvorschlaege = 5;

	@XmlElement(name = "antwortA")
	private String antwortA;

	@XmlElement(name = "antwortB")
	private String antwortB;

	@XmlElement(name = "antwortC")
	private String antwortC;

	@XmlElement(name = "antwortD")
	private String antwortD;

	@XmlElement(name = "antwortE")
	private String antwortE;

	@XmlElement(name = "loesung")
	private MCAntwortbuchstaben loesungsbuchstabe;

	@XmlElement(name = "tabelleGenerieren")
	private boolean tabelleGenerieren = true;

	@XmlElement(name = "quelle")
	private String quelle = DEFAULT_QUELLE;

	@XmlElement(name = "verzeichnis")
	private String verzeichnis;

	/**
   *
   */
	public MCAufgabe() {
	}

	/**
	 * @param pAntworten
	 */
	public void setAntworten(String[] pAntworten) {
		if (pAntworten == null || pAntworten.length != 5) {
			throw new IllegalArgumentException("pAntworten war null oder length != 5");
		}
		setAntwortA(pAntworten[0]);
		setAntwortB(pAntworten[1]);
		setAntwortC(pAntworten[2]);
		setAntwortD(pAntworten[3]);
		setAntwortE(pAntworten[4]);
	}

	/**
	 * @return the schluessel
	 */
	public final String getSchluessel() {
		return schluessel;
	}

	/**
	 * @param pSchluessel the schluessel to set
	 */
	public final void setSchluessel(String pSchluessel) {
		Object oldValue = schluessel;
		schluessel = pSchluessel;
		firePropertyChange(IMCAufgabeNames.PROP_SCHLUESSEL, oldValue, schluessel);
	}

	/**
	 * @return the thema
	 */
	public final MCThemen getThema() {
		return thema;
	}

	/**
	 * @param pThema the thema to set
	 */
	public final void setThema(MCThemen pThema) {
		Object oldValue = thema;
		thema = pThema;
		firePropertyChange(IMCAufgabeNames.PROP_THEMA, oldValue, thema);
	}

	/**
	 * @return the art
	 */
	public final MCAufgabenarten getArt() {
		return art;
	}

	/**
	 * @param pArt the art to set
	 */
	public final void setArt(MCAufgabenarten pArt) {
		Object oldValue = art;
		art = pArt;
		if (MCAufgabenarten.N.equals(art) || MCAufgabenarten.Z.equals(art)) {
			setQuelle(null);
		} else {
			setQuelle(DEFAULT_QUELLE);
		}
		firePropertyChange(IMCAufgabeNames.PROP_ART, oldValue, art);
	}

	/**
	 * @return the stufe
	 */
	public final MCAufgabenstufen getStufe() {
		return stufe;
	}

	/**
	 * @param pStufe the stufe to set
	 */
	public final void setStufe(MCAufgabenstufen pStufe) {
		Object oldValue = stufe;
		stufe = pStufe;
		firePropertyChange(IMCAufgabeNames.PROP_STUFE, oldValue, stufe);
	}

	/**
	 * @return the titel
	 */
	public final String getTitel() {
		return titel;
	}

	/**
	 * @param pTitel the titel to set
	 */
	public final void setTitel(String pTitel) {
		Object oldValue = titel;
		titel = pTitel;
		firePropertyChange(IMCAufgabeNames.PROP_TITEL, oldValue, titel);
	}

	/**
	 * @return the bemerkung
	 */
	public final String getBemerkung() {
		return bemerkung;
	}

	/**
	 * @param pBemerkung the bemerkung to set
	 */
	public final void setBemerkung(String pBemerkung) {
		Object oldValue = bemerkung;
		bemerkung = pBemerkung;
		firePropertyChange(IMCAufgabeNames.PROP_BEMERKUNG, oldValue, bemerkung);
	}

	/**
	 * @return the punkte
	 */
	public final Integer getPunkte() {
		return punkte;
	}

	/**
	 * @param pPunkte the punkte to set
	 */
	public final void setPunkte(Integer pPunkte) {
		Object oldValue = punkte;
		punkte = pPunkte;
		firePropertyChange(IMCAufgabeNames.PROP_PUNKTE, oldValue, punkte);
	}

	/**
	 * @return the antwortA
	 */
	public final String getAntwortA() {
		return antwortA;
	}

	/**
	 * @param pAntwortA the antwortA to set
	 */
	public final void setAntwortA(String pAntwortA) {
		Object oldValue = antwortA;
		antwortA = pAntwortA;
		firePropertyChange(IMCAufgabeNames.PROP_ANTWORT_A, oldValue, antwortA);
	}

	/**
	 * @return the antwortB
	 */
	public final String getAntwortB() {
		return antwortB;
	}

	/**
	 * @param pAntwortB the antwortB to set
	 */
	public final void setAntwortB(String pAntwortB) {
		Object oldValue = antwortB;
		antwortB = pAntwortB;
		firePropertyChange(IMCAufgabeNames.PROP_ANTWORT_B, oldValue, antwortB);
	}

	/**
	 * @return the antwortC
	 */
	public final String getAntwortC() {
		return antwortC;
	}

	/**
	 * @param pAntwortC the antwortC to set
	 */
	public final void setAntwortC(String pAntwortC) {
		Object oldValue = antwortC;
		antwortC = pAntwortC;
		firePropertyChange(IMCAufgabeNames.PROP_ANTWORT_C, oldValue, antwortC);
	}

	/**
	 * @return the antwortD
	 */
	public final String getAntwortD() {
		return antwortD;
	}

	/**
	 * @param pAntwortD the antwortD to set
	 */
	public final void setAntwortD(String pAntwortD) {
		Object oldValue = antwortD;
		antwortD = pAntwortD;
		firePropertyChange(IMCAufgabeNames.PROP_ANTWORT_D, oldValue, antwortD);
	}

	/**
	 * @return the antwortE
	 */
	public final String getAntwortE() {
		return antwortE;
	}

	/**
	 * @param pAntwortE the antwortE to set
	 */
	public final void setAntwortE(String pAntwortE) {
		Object oldValue = antwortE;
		antwortE = pAntwortE;
		firePropertyChange(IMCAufgabeNames.PROP_ANTWORT_E, oldValue, antwortE);
	}

	/**
	 * @return the loesungsbuchstabe
	 */
	public final MCAntwortbuchstaben getLoesungsbuchstabe() {
		return loesungsbuchstabe;
	}

	/**
	 * @param pLoesungsbuchstabe the loesungsbuchstabe to set
	 */
	public final void setLoesungsbuchstabe(MCAntwortbuchstaben pLoesungsbuchstabe) {
		Assert.isNotNull(pLoesungsbuchstabe);
		Assert.isTrue(MCAntwortbuchstaben.N != pLoesungsbuchstabe, "Es sind nur A-E erlaubt!");
		Object oldValue = loesungsbuchstabe;
		loesungsbuchstabe = pLoesungsbuchstabe;
		firePropertyChange(IMCAufgabeNames.PROP_LOESUNGSBUCHSTABE, oldValue, loesungsbuchstabe);
	}

	/**
	 * @return the quelle
	 */
	public final String getQuelle() {
		return quelle;
	}

	/**
	 * @param pQuelle the quelle to set
	 */
	public final void setQuelle(String pQuelle) {
		Object oldValue = quelle;
		quelle = pQuelle;
		firePropertyChange(IMCAufgabeNames.PROP_QUELLE, oldValue, quelle);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((schluessel == null) ? 0 : schluessel.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MCAufgabe other = (MCAufgabe) obj;
		if (schluessel == null) {
			if (other.schluessel != null)
				return false;
		} else if (!schluessel.equals(other.schluessel))
			return false;
		return true;
	}

	/**
	 * @return the tabelleGenerieren
	 */
	public final boolean isTabelleGenerieren() {
		return tabelleGenerieren;
	}

	/**
	 * @param pTabelleGenerieren the tabelleGenerieren to set
	 */
	public final void setTabelleGenerieren(boolean pTabelleGenerieren) {
		Object oldValue = tabelleGenerieren;
		tabelleGenerieren = pTabelleGenerieren;
		firePropertyChange(IMCAufgabeNames.PROP_TABELLE_GENERIEREN, oldValue, tabelleGenerieren);
	}

	/**
	 * @return the verzeichnis
	 */
	public final String getVerzeichnis() {
		return verzeichnis;
	}

	/**
	 * @param pVerzeichnis the verzeichnis to set
	 */
	public final void setVerzeichnis(String pVerzeichnis) {
		Object oldValue = verzeichnis;
		verzeichnis = pVerzeichnis;
		firePropertyChange(IMCAufgabeNames.PROP_VERZEICHNIS, oldValue, verzeichnis);
	}

	/**
	 * @return the gesperrtFuerRaetsel
	 */
	public final String getGesperrtFuerRaetsel() {
		return gesperrtFuerRaetsel;
	}

	/**
	 * @param pGesperrtFuerRaetsel the gesperrtFuerRaetsel to set
	 */
	public final void setGesperrtFuerRaetsel(String pGesperrtFuerRaetsel) {
		Object oldValue = gesperrtFuerRaetsel;
		gesperrtFuerRaetsel = pGesperrtFuerRaetsel;
		firePropertyChange(IMCAufgabeNames.PROP_GESPERRT, oldValue, gesperrtFuerRaetsel);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mcmatheraetsel.archivdomain.archiv.IMatheAGEntity#checkConsistent()
	 */
	@Override
	public void checkConsistent() throws MatheJungAltInconsistentEntityException {
		StringBuffer sb = new StringBuffer();
		boolean consistent = true;
		if (schluessel == null || schluessel.isEmpty() || schluessel.length() > 15) {
			sb.append("schluessel darf nicht null, leer oder l\u00e4nger als 15 Zeichen sein\n");
			consistent = false;
		}
		if (art == null) {
			sb.append("art darf nicht null sein\n");
			consistent = false;
		}
		if (anzahlAntwortvorschlaege <= 1 || anzahlAntwortvorschlaege > 5) {
			sb.append("Es muss mindestens 2und h\u00f6chstens 5 Antwortvorschl\u00e4ge geben.\n");
		}

		if (tabelleGenerieren) {
			if (antwortA == null || antwortA.isEmpty() || antwortA.length() > 255) {
				sb.append("antwortA darf nicht null, leer oder l\u00e4nger als 255 Zeichen sein\n");
				consistent = false;
			}
			if (antwortB == null || antwortB.isEmpty() || antwortB.length() > 255) {
				sb.append("antwortB darf nicht null, leer oder l\u00e4nger als 255 Zeichen sein\n");
				consistent = false;
			}
			if (anzahlAntwortvorschlaege >= 3) {
				if (antwortC == null || antwortC.isEmpty() || antwortC.length() > 255) {
					sb.append("antwortC darf nicht null, leer oder l\u00e4nger als 255 Zeichen sein\n");
					consistent = false;
				}
				if (anzahlAntwortvorschlaege >= 4) {
					if (antwortD == null || antwortD.isEmpty() || antwortD.length() > 255) {
						sb.append("antwortD darf nicht null, leer oder l\u00e4nger als 255 Zeichen sein\n");
						consistent = false;
					}
				}
				if (anzahlAntwortvorschlaege >= 5) {
					if (antwortE == null || antwortE.isEmpty() || antwortE.length() > 255) {
						sb.append("antwortE darf nicht null, leer oder l\u00e4nger als 255 Zeichen sein\n");
						consistent = false;
					}
				}
			}
		}
		if (bemerkung != null && bemerkung.length() > 255) {
			sb.append("bemerkung darf nicht l\u00e4nger als 255 Zeichen sein\n");
			consistent = false;
		}
		if (loesungsbuchstabe == null) {
			sb.append("loesungsbuchstabe darf nicht null sein\n");
			consistent = false;
		}
		switch (anzahlAntwortvorschlaege) {
		case 2:
			if (MCAntwortbuchstaben.C == loesungsbuchstabe || MCAntwortbuchstaben.D == loesungsbuchstabe
				|| MCAntwortbuchstaben.E == loesungsbuchstabe) {
				sb.append("loesungsbuchstabe darf nur A oder B sein\n");
				consistent = false;
			}
			break;
		case 3:
			if (MCAntwortbuchstaben.D == loesungsbuchstabe || MCAntwortbuchstaben.E == loesungsbuchstabe) {
				sb.append("loesungsbuchstabe darf nur A, B oder C sein\n");
				consistent = false;
			}
			break;
		case 4:
			if (MCAntwortbuchstaben.D == loesungsbuchstabe || MCAntwortbuchstaben.E == loesungsbuchstabe) {
				sb.append("loesungsbuchstabe darf nur A, B, C oder D sein\n");
				consistent = false;
			}
			break;
		default:
			break;
		}
		if (punkte == null) {
			sb.append("punkte darf nicht null sein\n");
			consistent = false;
		}
		if (quelle == null || quelle.isEmpty() || quelle.length() > 255) {
			sb.append("quelle darf nicht null, leer oder l\u00e4nger als 255 Zeichen sein\n");
			consistent = false;
		}
		if (titel == null || quelle.length() > 100) {
			sb.append("titel darf nicht null, leer oder l\u00e4nger als 100 Zeichen sein\n");
			consistent = false;
		}
		if (thema == null) {
			sb.append("thema darf nicht null sein\n");
			consistent = false;
		}
		if (verzeichnis == null || verzeichnis.length() != 3) {
			sb.append("verzeichnis muss genau 3 Zeichen lang sein\n");
			consistent = false;
		}
		if (!consistent) {
			throw new MatheJungAltInconsistentEntityException("Die Quizaufgabe kann nicht gespeichert werden:\n\n"
				+ sb.toString());
		}
	}

	public boolean canGenerate() {
		if (schluessel == null) {
			return false;
		}
		if (verzeichnis == null) {
			return false;
		}
		if (anzahlAntwortvorschlaege <= 1) {
			return false;
		}
		if (tabelleGenerieren) {
			switch (anzahlAntwortvorschlaege) {
			case 2:
				if (antwortA == null || antwortA.isEmpty() || antwortB == null || antwortB.isEmpty()) {
					return false;
				}
				break;
			case 3:
				if (antwortA == null || antwortA.isEmpty() || antwortB == null || antwortB.isEmpty()
					|| antwortC == null || antwortC.isEmpty()) {
					return false;
				}
				break;
			case 4:
				if (antwortA == null || antwortA.isEmpty() || antwortB == null || antwortB.isEmpty()
					|| antwortC == null || antwortC.isEmpty() || antwortD == null || antwortD.isEmpty()) {
					return false;
				}
				break;
			default:
				if (antwortA == null || antwortA.isEmpty() || antwortB == null || antwortB.isEmpty()
					|| antwortC == null || antwortC.isEmpty() || antwortD == null || antwortD.isEmpty()
					|| antwortE == null || antwortE.isEmpty()) {
					return false;
				}
				break;
			}
		}
		if (loesungsbuchstabe == null) {
			return false;
		}
		if (stufe == null) {
			return false;
		}
		if (punkte == null) {
			return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.domain.AbstractMatheAGObject#getKey()
	 */
	@Override
	public Object getKey() {
		return schluessel;
	}

	/**
	 * @param pPathWorkDir
	 * @return
	 */
	public String getPathPrefix(String pPathWorkDir) {
		return pPathWorkDir + "/prev_" + schluessel;
	}

	/**
	 * @return the anzahlAntwortvorschlaege
	 */
	public final int getAnzahlAntwortvorschlaege() {
		return anzahlAntwortvorschlaege;
	}

	/**
	 * @param pAnzahlAntwortvorschlaege the anzahlAntwortvorschlaege to set
	 */
	public final void setAnzahlAntwortvorschlaege(int pAnzahlAntwortvorschlaege) {
		firePropertyChange(IMCAufgabeNames.PROP_ANZAHL_ANTWORTVOESCHLAEGE, anzahlAntwortvorschlaege,
			anzahlAntwortvorschlaege = pAnzahlAntwortvorschlaege);
	}
}
