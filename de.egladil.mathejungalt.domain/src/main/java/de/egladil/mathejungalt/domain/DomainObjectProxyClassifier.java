/**
 *
 */
package de.egladil.mathejungalt.domain;

import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgaben.Heft;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblatt;
import de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter.Arbeitsblattitem;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguruitem;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serienitem;
import de.egladil.mathejungalt.domain.medien.Autor;
import de.egladil.mathejungalt.domain.medien.Buch;
import de.egladil.mathejungalt.domain.medien.Zeitschrift;
import de.egladil.mathejungalt.domain.mitglieder.Diplom;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.domain.mitglieder.Serienteilnahme;
import de.egladil.mathejungalt.domain.quellen.AbstractQuelle;
import de.egladil.mathejungalt.domain.quellen.Buchquelle;
import de.egladil.mathejungalt.domain.quellen.Kindquelle;
import de.egladil.mathejungalt.domain.quellen.MitgliedergruppenQuelle;
import de.egladil.mathejungalt.domain.quellen.NullQuelle;
import de.egladil.mathejungalt.domain.quellen.Zeitschriftquelle;
import de.egladil.mathejungalt.domain.schulen.Kontakt;
import de.egladil.mathejungalt.domain.schulen.Schule;

/**
 * Utilityklasse, Proxies auf {@link AbstractMatheAGObject} pr�ft, ob sie einen bestimmten Typ haben.
 *
 * @author winkelv
 */
public final class DomainObjectProxyClassifier {

	/**
	 *
	 */
	private DomainObjectProxyClassifier() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return
	 */
	public static boolean isAutor(final Object pObj) {
		if (pObj == null)
			return false;
		try {
			@SuppressWarnings("unused")
			Autor param = (Autor) pObj;
			return true;
		} catch (ClassCastException e) {
			return false;
		}
	}

	/**
	 * @return
	 */
	public static boolean isBuch(final Object pObj) {
		if (pObj == null)
			return false;
		try {
			@SuppressWarnings("unused")
			Buch param = (Buch) pObj;
			return true;
		} catch (ClassCastException e) {
			return false;
		}
	}

	/**
	 * @return
	 */
	public static boolean isZeitschrift(final Object pObj) {
		if (pObj == null)
			return false;
		try {
			@SuppressWarnings("unused")
			Zeitschrift param = (Zeitschrift) pObj;
			return true;
		} catch (ClassCastException e) {
			return false;
		}
	}

	/**
	 * @return
	 */
	public static boolean isAufgabe(final Object pObj) {
		try {
			if (pObj == null)
				return false;
			@SuppressWarnings("unused")
			Aufgabe param = (Aufgabe) pObj;
			return true;
		} catch (ClassCastException e) {
			return false;
		}
	}

	/**
	 * @return
	 */
	public static boolean isAufgabensammlungItem(final Object pObj) {
		try {
			if (pObj == null)
				return false;
			@SuppressWarnings("unused")
			IAufgabensammlungItem param = (IAufgabensammlungItem) pObj;
			return true;
		} catch (ClassCastException e) {
			return false;
		}
	}

	/**
	 * @return
	 */
	public static boolean isHeft(final Object pObj) {
		if (pObj == null)
			return false;
		try {
			@SuppressWarnings("unused")
			Heft param = (Heft) pObj;
			return true;
		} catch (ClassCastException e) {
			return false;
		}
	}

	/**
	 * @return
	 */
	public static boolean isMinikaenguru(final Object pObj) {
		if (pObj == null)
			return false;
		try {
			@SuppressWarnings("unused")
			Minikaenguru param = (Minikaenguru) pObj;
			return true;
		} catch (ClassCastException e) {
			return false;
		}
	}

	/**
	 * @return
	 */
	public static boolean isArbeitsblatt(final Object pObj) {
		if (pObj == null)
			return false;
		try {
			@SuppressWarnings("unused")
			Arbeitsblatt param = (Arbeitsblatt) pObj;
			return true;
		} catch (ClassCastException e) {
			return false;
		}
	}

	/**
	 * @return
	 */
	public static boolean isArbeitsblattitem(final Object pObj) {
		if (pObj == null)
			return false;
		try {
			@SuppressWarnings("unused")
			Arbeitsblattitem param = (Arbeitsblattitem) pObj;
			return true;
		} catch (ClassCastException e) {
			return false;
		}
	}

	/**
	 * @return
	 */
	public static boolean isMinikaenguruitem(final Object pObj) {
		if (pObj == null)
			return false;
		try {
			@SuppressWarnings("unused")
			Minikaenguruitem param = (Minikaenguruitem) pObj;
			return true;
		} catch (ClassCastException e) {
			return false;
		}
	}

	/**
	 * @return
	 */
	public static boolean isSerie(final Object pObj) {
		if (pObj == null)
			return false;
		try {
			@SuppressWarnings("unused")
			Serie param = (Serie) pObj;
			return true;
		} catch (ClassCastException e) {
			return false;
		}
	}

	/**
	 * @return
	 */
	public static boolean isSerienitem(final Object pObj) {
		if (pObj == null)
			return false;
		try {
			@SuppressWarnings("unused")
			Serienitem param = (Serienitem) pObj;
			return true;
		} catch (ClassCastException e) {
			return false;
		}
	}

	/**
	 * @return
	 */
	public static boolean isDiplom(final Object pObj) {
		if (pObj == null)
			return false;
		try {
			@SuppressWarnings("unused")
			Diplom param = (Diplom) pObj;
			return true;
		} catch (ClassCastException e) {
			return false;
		}
	}

	/**
	 * @return
	 */
	public static boolean isMitglied(final Object pObj) {
		if (pObj == null)
			return false;
		try {
			@SuppressWarnings("unused")
			Mitglied param = (Mitglied) pObj;
			return true;
		} catch (ClassCastException e) {
			return false;
		}
	}

	/**
	 * @return
	 */
	public static boolean isSerienteilnahme(final Object pObj) {
		if (pObj == null)
			return false;
		try {
			@SuppressWarnings("unused")
			Serienteilnahme param = (Serienteilnahme) pObj;
			return true;
		} catch (ClassCastException e) {
			return false;
		}
	}

	/**
	 * @return
	 */
	public static boolean isBuchquelle(final Object pObj) {
		if (pObj == null)
			return false;
		try {
			@SuppressWarnings("unused")
			Buchquelle param = (Buchquelle) pObj;
			return true;
		} catch (ClassCastException e) {
			return false;
		}
	}

	/**
	 * @return
	 */
	public static boolean isZeitschriftquelle(final Object pObj) {
		if (pObj == null)
			return false;
		try {
			@SuppressWarnings("unused")
			Zeitschriftquelle param = (Zeitschriftquelle) pObj;
			return true;
		} catch (ClassCastException e) {
			return false;
		}
	}

	/**
	 * @return
	 */
	public static boolean isNullquelle(final Object pObj) {
		if (pObj == null)
			return false;
		try {
			@SuppressWarnings("unused")
			NullQuelle param = (NullQuelle) pObj;
			return true;
		} catch (ClassCastException e) {
			return false;
		}
	}

	/**
	 * @return
	 */
	public static boolean isKindquelle(final Object pObj) {
		if (pObj == null)
			return false;
		try {
			@SuppressWarnings("unused")
			Kindquelle param = (Kindquelle) pObj;
			return true;
		} catch (ClassCastException e) {
			return false;
		}
	}

	/**
	 * @return
	 */
	public static boolean isGruppenquelle(final Object pObj) {
		if (pObj == null)
			return false;
		try {
			@SuppressWarnings("unused")
			MitgliedergruppenQuelle param = (MitgliedergruppenQuelle) pObj;
			return true;
		} catch (ClassCastException e) {
			return false;
		}
	}

	/**
	 * @param pObj
	 * @return
	 */
	public static boolean isQuelle(Object pObj) {
		if (pObj == null)
			return false;
		try {
			@SuppressWarnings("unused")
			AbstractQuelle quelle = (AbstractQuelle) pObj;
			return true;
		} catch (ClassCastException e) {
			return false;
		}
	}

	public static boolean isSchule(Object pObj) {
		try {
			if (pObj == null)
				return false;
			@SuppressWarnings("unused")
			Schule param = (Schule) pObj;
			return true;
		} catch (ClassCastException e) {
			return false;
		}
	}

	public static boolean isKontakt(Object pObj) {
		try {
			if (pObj == null)
				return false;
			@SuppressWarnings("unused")
			Kontakt param = (Kontakt) pObj;
			return true;
		} catch (ClassCastException e) {
			return false;
		}
	}
}
