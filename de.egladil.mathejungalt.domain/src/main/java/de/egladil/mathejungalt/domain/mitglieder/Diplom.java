/**
 *
 */
package de.egladil.mathejungalt.domain.mitglieder;

import java.util.Date;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.types.LongDateComposedKey;

/**
 * @author heike
 */
public class Diplom extends AbstractMatheAGObject {

	/** */
	private static final long serialVersionUID = -7193580072415288429L;

	/** */
	private Date datum;

	/** */
	private int anzahlErfinderpunkte;

	/** */
	private Mitglied mitglied;

	/**
	 *
	 */
	public Diplom() {
		super();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.domain.AbstractMatheAGObject#getKey()
	 */
	@Override
	public Object getKey() {
		if (mitglied == null && datum == null)
			return getId();
		return new LongDateComposedKey(mitglied.getId(), datum);
	}

	/**
	 * @return the datum
	 */
	public Date getDatum() {
		return datum;
	}

	/**
	 * @param pDatum the datum to set
	 */
	public void setDatum(Date pDatum) {
		firePropertyChange(IDiplomNames.PROP_DATUM, datum, datum = pDatum);
	}

	/**
	 * @return the anzahlErfinderpunkte
	 */
	public int getAnzahlErfinderpunkte() {
		return anzahlErfinderpunkte;
	}

	/**
	 * @param pAnzahlErfinderpunkte the anzahlErfinderpunkte to set
	 */
	public void setAnzahlErfinderpunkte(int pAnzahlErfinderpunkte) {
		firePropertyChange(IDiplomNames.PROP_ANZAHL_ERF, anzahlErfinderpunkte,
			anzahlErfinderpunkte = pAnzahlErfinderpunkte);
	}

	/**
	 * @return the mitglied
	 */
	public Mitglied getMitglied() {
		return mitglied;
	}

	/**
	 * @param pMitglied the mitglied to set
	 */
	public void setMitglied(Mitglied pMitglied) {
		firePropertyChange(IDiplomNames.PROP_MITGLIED, mitglied, mitglied = pMitglied);
	}
}
