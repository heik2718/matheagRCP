/**
 *
 */
package de.egladil.mathejungalt.domain.mcraetsel;

import de.egladil.mathejungalt.domain.IMatheAGObjectNames;

/**
 * This generated interface defines constants for all attributes and associatations in
 * {@link de.egladil.mathe.domain.quelle.Buchquelle}.
 * <p>
 * These constants are useful for example when building criterias.
 *
 * @author heike
 */
public interface IMCAufgabeNames extends IMatheAGObjectNames {

	String PROP_ANTWORT_A = "antwortA";

	String PROP_ANTWORT_B = "antwortB";

	String PROP_ANTWORT_C = "antwortC";

	String PROP_ANTWORT_D = "antwortD";

	String PROP_ANTWORT_E = "antwortE";

	String PROP_ART = "art";

	String PROP_BEMERKUNG = "bemerkung";

	String PROP_GESPERRT = "gesperrtFuerRaetsel";

	String PROP_LOESUNGSBUCHSTABE = "loesungsbuchstabe";

	String PROP_PUNKTE = "punkte";

	String PROP_QUELLE = "quelle";

	String PROP_STUFE = "stufe";

	String PROP_TABELLE_GENERIEREN = "tabelleGenerieren";

	String PROP_THEMA = "thema";

	String PROP_TITEL = "titel";

	String PROP_VERZEICHNIS = "verzeichnis";

	String PROP_ANZAHL_ANTWORTVOESCHLAEGE = "anzahlAntwortvorschlaege";

	int LENGTH_SCHLUESSEL = 15;

	int LENGTH_SCHLUESSEL_DEFAULT = 5;

	int LENGTH_TITEL = 100;

	int LENGTH_ART = 1;

	int LENGTH_ZWECK = 1;

	int LENGTH_STUFE = 1;

	int LENGTH_VERZEICHNIS = 3;

	int LENGTH_BEMERKUNG = 256;
}
