/**
 * 
 */
package de.egladil.mathejungalt.domain.medien;

/**
 * This generated interface defines constants for all attributes and associatations in
 * {@link de.egladil.mathe.domain.quelle.Buchquelle}.
 * <p>
 * These constants are useful for example when building criterias.
 * 
 * @author heike
 */
public interface IAutorNames {

	public static final String PROP_NAME = "name";

	public static final int LENGTH_SCHLUESSEL = 2;

	public static final int LENGTH_NAME = 100;
}
