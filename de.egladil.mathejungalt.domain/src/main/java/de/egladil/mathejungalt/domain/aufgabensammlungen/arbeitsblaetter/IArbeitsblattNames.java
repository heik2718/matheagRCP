/**
 * 
 */
package de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter;

import de.egladil.mathejungalt.domain.IMatheAGObjectNames;

/**
 * @author Heike Winkelvoss (www.egladil.de)
 */
public interface IArbeitsblattNames extends IMatheAGObjectNames {

	public static final String PROP_ITEMS = "arbeitsblattitems";

	public static final String PROP_TITEL = "titel";

	public static final String PROP_SPERREND = "sperrend";

	public static final String PROP_BEENDET = "beendet";

	public static final int LENGTH_TITEL = 100;

	public static final String SQL_AUFGABE = "aufgabe";

	public static final String SQL_MINIKAENGURU = "minikaenguru";

	public static final int LENGTH_SCHLUESSEL = 4;
}
