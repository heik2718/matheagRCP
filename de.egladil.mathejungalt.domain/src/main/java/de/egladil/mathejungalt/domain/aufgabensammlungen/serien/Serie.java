/**
 *
 */
package de.egladil.mathejungalt.domain.aufgabensammlungen.serien;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.EgladilServerException;
import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenzweck;

/**
 * @author heike
 */
public class Serie extends AbstractMatheAGObject implements IAufgabensammlung {

	/** */
	private static final long serialVersionUID = 6300539344656699794L;

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(Serie.class);

	/** */
	private Integer nummer;

	/** */
	private Integer runde;

	/** */
	private int beendet;

	/** */
	private String monatJahrText;

	/** */
	private Date datum;

	/** */
	private List<Serienitem> serienitems = new ArrayList<Serienitem>();

	/** */
	private boolean itemsLoaded = false;

	/**
	 *
	 */
	public Serie() {
		super();
		beendet = 0;
		itemsLoaded = false;
	}

	/**
	 *
	 */
	public void initItems() {
		if (itemsLoaded) {
			return;
		}
		serienitems = new ArrayList<Serienitem>();
	}

	/**
	 *
	 */
	public void clearSerienitems() {
		if (!itemsLoaded)
			return;
		serienitems.clear();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.domain.AbstractMatheAGObject#getKey()
	 */
	@Override
	public Object getKey() {
		return nummer;
	}

	/**
	 * @return the runde
	 */
	public Integer getRunde() {
		return runde;
	}

	/**
	 * @param pRunde the runde to set
	 */
	public void setRunde(Integer pRunde) {
		firePropertyChange(ISerieNames.PROP_RUNDE, runde, runde = pRunde);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung#getBeendet()
	 */
	public int getBeendet() {
		return beendet;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung#setBeendet(int)
	 */
	public void setBeendet(int pBeendet) {
		firePropertyChange(ISerieNames.PROP_BEENDET, beendet, beendet = pBeendet);
	}

	/**
	 * @return the monatJahrText
	 */
	public String getMonatJahrText() {
		return monatJahrText;
	}

	/**
	 * @param pMonatJahrText the monatJahrText to set
	 */
	public void setMonatJahrText(String pMonatJahrText) {
		firePropertyChange(ISerieNames.PROP_MONAT_JAHR_TEXT, monatJahrText, monatJahrText = pMonatJahrText);
	}

	/**
	 * @return the datum
	 */
	public Date getDatum() {
		return datum;
	}

	/**
	 * @param pDatum the datum to set
	 */
	public void setDatum(Date pDatum) {
		firePropertyChange(ISerieNames.PROP_DATUM, datum, datum = pDatum);
	}

	/**
	 * @param pNummer the key to set
	 */
	public void setNummer(Integer pNummer) {
		firePropertyChange(ISerieNames.PROP_NUMMER, nummer, nummer = pNummer);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung#anzahlAufgaben()
	 */
	public int anzahlAufgaben() {
		if (serienitems == null)
			return 0;
		return serienitems.size();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @seede.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung#addItem(de.egladil.mathejungalt.domain.
	 * aufgabensammlungen.IAufgabensammlungItem)
	 */
	public boolean addItem(final IAufgabensammlungItem pItem) {
		if (pItem == null) {
			String msg = MatheJungAltException.argumentNullMessage("addItem(IAufgabensammlungItem)");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (!(pItem instanceof Serienitem)) {
			String msg = "addItem(): unzulässiger Typ " + pItem.getClass().getName();
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		boolean added = serienitems.add((Serienitem) pItem);
		if (itemsLoaded && added) {
			fireSerienitemsChanged();
		}
		return added;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung#canAdd(de.egladil.mathejungalt.domain.aufgaben
	 * .Aufgabe)
	 */
	@Override
	public boolean canAdd(Aufgabe pAufgabe) {
		if (beendet == IAufgabensammlung.BEENDET) {
			return false;
		}
		if (pAufgabe == null) {
			return false;
		}
		if (Aufgabe.LOCKED.equals(pAufgabe.getGesperrtFuerWettbewerb())) {
			return false;
		}
		return Aufgabenzweck.S.equals(pAufgabe.getZweck());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung#removeItem(de.egladil.mathejungalt.domain
	 * .aufgabensammlungen.IAufgabensammlungItem)
	 */
	public boolean removeItem(final IAufgabensammlungItem pItem) {
		if (pItem == null) {
			String msg = MatheJungAltException.argumentNullMessage("removeItem(Serienitem)");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		checkItemsLoaded();
		boolean removed = serienitems.remove(pItem);
		if (removed) {
			fireSerienitemsChanged();
		}
		return removed;
	}

	/**
	 * Prueft, ob die Serienitems bereits geladen sind. Falls nicht, wird eine Exception geworfen.
	 *
	 * @throws MatheAGDomainRuntimeException
	 */
	private void checkItemsLoaded() {
		if (!itemsLoaded) {
			throw new MatheJungAltException("Die Serienitems zur Serie sind noch nicht geladen.");
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Serie " + nummer;
	}

	/**
	 * @return the nummer
	 */
	public Integer getNummer() {
		return nummer;
	}

	/**
	 *
	 */
	private void fireSerienitemsChanged() {
		firePropertyChange(ISerieNames.PROP_SERIENITEMS, null, getItems());
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung#isItemsLoaded()
	 */
	public boolean isItemsLoaded() {
		return itemsLoaded;
	}

	/**
	 * @param pSerienitemsLoaded the serienitemsLoaded to set
	 */
	public void setItemsLoaded(boolean pSerienitemsLoaded) {
		if (serienitems == null) {
			serienitems = new ArrayList<Serienitem>();
		}
		itemsLoaded = pSerienitemsLoaded;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung#getDomainObject()
	 */
	@Override
	public AbstractMatheAGObject getDomainObject() {
		return this;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung#getItems()
	 */
	@Override
	public List<IAufgabensammlungItem> getItems() {
		List<IAufgabensammlungItem> items = new ArrayList<IAufgabensammlungItem>();
		for (Serienitem item : serienitems) {
			items.add(item);
		}
		return items;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung#getItems(int)
	 */
	@Override
	public List<IAufgabensammlungItem> getItems(int pStufe) {
		List<IAufgabensammlungItem> items = new ArrayList<IAufgabensammlungItem>();
		for (Serienitem item : serienitems) {
			if (item.getAufgabe().getStufe() == pStufe) {
				items.add(item);
			}
		}
		return items;
	}

	/**
	 *
	 * @param stufe
	 * @return
	 */
	public List<Serienitem> getSerienitemsZuStufe(int stufe) {
		if (!itemsLoaded) {
			throw new EgladilServerException("Serienitems sind noch nicht geladen!");
		}
		List<Serienitem> result = new ArrayList<Serienitem>();
		for (Serienitem i : serienitems) {
			if (i.getStufe() == stufe) {
				result.add(i);
			}
		}
		return result;
	}
}
