/**
 * 
 */
package de.egladil.mathejungalt.domain.quellen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mathejungalt.domain.mitglieder.Mitglied;
import de.egladil.mathejungalt.domain.types.EnumTypes.Quellenart;

/**
 * Ein Mitglied in einem bestimmten Alter und einer bestimmten Klasse.
 * 
 * @author heike
 */
public class Kindquelle extends AbstractQuelle {

	/** */
	private static final long serialVersionUID = 1374123210209080819L;

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(Kindquelle.class);

	/** */
	private Mitglied mitglied;

	/** */
	private Integer jahre;

	/** */
	private Integer klasse;

	/**
	 * 
	 */
	public Kindquelle() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.domain.quellen.AbstractQuelle#toString()
	 */
	@Override
	public String toString() {
		if (LOG.isDebugEnabled())
			LOG.debug("Start toString()");
		String string = super.toString();
		if (mitglied != null) {
			string += "-" + mitglied.getVorname();
			if (mitglied.getNachname() != null) {
				string += " " + mitglied.getNachname();
			}
		}
		if (jahre != null) {
			string += "," + jahre.intValue() + " Jahre";
		}
		if (klasse != null) {
			string += ",Klasse " + klasse.intValue();
		}

		return string;
	}

	/**
	 * @return the mitglied
	 */
	public Mitglied getMitglied() {
		return mitglied;
	}

	/**
	 * @param pMitglied the mitglied to set
	 */
	public void setMitglied(Mitglied pMitglied) {
		firePropertyChange(IKindquelleNames.PROP_MITGLIED, mitglied, mitglied = pMitglied);
	}

	/**
	 * @return the jahre
	 */
	public Integer getJahre() {
		return jahre;
	}

	/**
	 * @param pJahre the jahre to set
	 */
	public void setJahre(Integer pJahre) {
		firePropertyChange(IKindquelleNames.PROP_JAHRE, jahre, jahre = pJahre);
	}

	/**
	 * @return the klasse
	 */
	public Integer getKlasse() {
		return klasse;
	}

	/**
	 * @param pKlasse the klasse to set
	 */
	public void setKlasse(Integer pKlasse) {
		firePropertyChange(IKindquelleNames.PROP_KLASSE, klasse, klasse = pKlasse);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.domain.quellen.AbstractQuelle#getArt()
	 */
	@Override
	public Quellenart getArt() {
		return Quellenart.K;
	}
}
