/**
 * 
 */
package de.egladil.mathejungalt.domain.medien;

import java.beans.PropertyChangeSupport;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.IMatheAGObjectNames;

/**
 * Autor- Entity.
 * 
 * @author heike
 */
public class Autor extends AbstractMatheAGObject {

	/** */
	private static final long serialVersionUID = 5831941139272121916L;

	/** */
	private String schluessel;

	/** */
	private String name;

	/**
	 * 
	 */
	public Autor() {
		super();
	}

	/**
	 * @param pSchluessel
	 */
	public Autor(String pSchluessel) {
		super();
		schluessel = pSchluessel;
		setChangeSupport(new PropertyChangeSupport(this));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return name + " - " + getKey();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.domain.AbstractMatheAGObject#getKey()
	 */
	@Override
	public Object getKey() {
		return schluessel;
	}

	/**
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param pName
	 */
	public void setName(String pName) {
		firePropertyChange(IAutorNames.PROP_NAME, name, name = pName);
	}

	/**
	 * @param pSchluessel
	 */
	public void setSchluessel(String pSchluessel) {
		firePropertyChange(IMatheAGObjectNames.PROP_SCHLUESSEL, schluessel, schluessel = pSchluessel);
	}

	/**
	 * @return the schluessel
	 */
	public String getSchluessel() {
		return schluessel;
	}
}
