/**
 * 
 */
package de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.types.AufgabeMinikaenguruComposedKey;

/**
 * Die Aufgaben des Minikänguru-Wettbewerbs, aufgehübscht um ein paar Attribute.
 * 
 * @author heike
 */
public class Minikaenguruitem extends AbstractMatheAGObject implements IAufgabensammlungItem {

	/** */
	private static final long serialVersionUID = -7326914831670754351L;

	/** */
	private String nummer;

	/** */
	private Aufgabe aufgabe;

	/** */
	private Minikaenguru minikaenguru;

	/**
	 * 
	 */
	public Minikaenguruitem() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.domain.AbstractMatheAGObject#getKey()
	 */
	@Override
	public Object getKey() {
		if (aufgabe == null && minikaenguru == null)
			return getId();
		return new AufgabeMinikaenguruComposedKey(aufgabe, minikaenguru);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(nummer);
		sb.append("-");
		sb.append(minikaenguru.getKey());
		sb.append("-");
		sb.append(aufgabe.getKey());
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem#getAufgabensammlung()
	 */
	@Override
	public IAufgabensammlung getAufgabensammlung() {
		return minikaenguru;
	}

	/**
	 * @return the nummer
	 */
	public String getNummer() {
		return nummer;
	}

	/**
	 * @param pNummer the nummer to set
	 */
	public void setNummer(String pNummer) {
		firePropertyChange(IAufgabensammlungItem.PROP_NUMMER, nummer, nummer = pNummer);
	}

	/**
	 * @return the aufgabe
	 */
	public Aufgabe getAufgabe() {
		return aufgabe;
	}

	/**
	 * @param pAufgabe the aufgabe to set
	 */
	public void setAufgabe(Aufgabe pAufgabe) {
		firePropertyChange(IAufgabensammlungItem.PROP_AUFGABE, aufgabe, aufgabe = pAufgabe);
	}

	/**
	 * @return the minikaenguru
	 */
	public Minikaenguru getMinikaenguru() {
		return minikaenguru;
	}

	/**
	 * @param pMinikaenguru the minikaenguru to set
	 */
	public void setMinikaenguru(Minikaenguru pMinikaenguru) {
		firePropertyChange(IMinikaenguruitemNames.PROP_MINIKAENGURU, minikaenguru, minikaenguru = pMinikaenguru);
	}
}
