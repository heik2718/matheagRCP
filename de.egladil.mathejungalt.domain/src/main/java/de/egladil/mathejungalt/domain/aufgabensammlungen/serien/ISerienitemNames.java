/**
 *
 */
package de.egladil.mathejungalt.domain.aufgabensammlungen.serien;

/**
 * This generated interface defines constants for all attributes and associatations in
 * {@link de.egladil.mathe.domain.quelle.Buchquelle}.
 * <p>
 * These constants are useful for example when building criterias.
 *
 * @author heike
 */
public interface ISerienitemNames {

	public static final String SQL_SERIE = "serie";

	public static final String SQL_AUFGABE = "aufgabe";

	public final static String PROP_SERIE = "serie";

	public final static int LENGTH_NUMMER = 6;

	public final static int LENGTH_AUFGABE = 5;

}
