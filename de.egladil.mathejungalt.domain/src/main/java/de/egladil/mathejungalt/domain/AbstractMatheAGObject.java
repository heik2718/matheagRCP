/**
 *
 */
package de.egladil.mathejungalt.domain;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.eclipse.core.runtime.PlatformObject;

/**
 * Abstrakte Oberklasse fuer alle meine Domain-Objekte.
 *
 * @author heike
 */
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class AbstractMatheAGObject extends PlatformObject implements Serializable,
	Comparable<AbstractMatheAGObject> {

	/** */
	@XmlTransient
	public static final String LOCKED = "J";

	/** */
	@XmlTransient
	public static final String NOT_LOCKED = "N";

	/**
	 *
	 */
	private static final long serialVersionUID = 6432797256453766490L;

	/** */
	@XmlTransient
	private PropertyChangeSupport changeSupport;

	/** */
	@XmlElement(name = "id")
	private Long id;

	private boolean adjusting;

	/**
	 *
	 */
	public AbstractMatheAGObject() {
		super();
		// uuid = UUID.randomUUID().toString();
		setChangeSupport(new PropertyChangeSupport(this));
	}

	/**
	 * Subclass will typically override this method and return the real natural key or UUID key. If it is not overridden
	 * this default implementation returns null, which means that equals and hashCode will be implemented by
	 * java.lang.Object (if those methods are not overridden in subclass).
	 */
	public abstract Object getKey();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!this.getClass().isAssignableFrom(obj.getClass())) {
			return false;
		}
		AbstractMatheAGObject other = (AbstractMatheAGObject) obj;
		EqualsBuilder eb = new EqualsBuilder();
		// eb.append(uuid, other.getUuid());
		eb.append(this.getKey(), other.getKey());
		return eb.isEquals();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		HashCodeBuilder hb = new HashCodeBuilder();
		// hb.append(uuid);
		hb.append(getKey());
		return hb.toHashCode();
	}

	/**
	 * @param pO1
	 * @param pO2
	 * @return boolean
	 */
	protected boolean hasChanged(Object pO1, Object pO2) {
		if (pO1 == null && pO2 == null)
			return false;
		if (pO1 == null && pO2 != null)
			return true;
		if (pO1 != null && pO2 == null)
			return true;
		return pO1.equals(pO2);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(AbstractMatheAGObject pO) {
		return getId().intValue() - pO.getId().intValue();
	}

	/**
	 * Den Listener anmelden
	 *
	 * @param pListener {@link PropertyChangeListener}
	 */
	public void addPropertyChangeListener(final PropertyChangeListener pListener) {
		if (changeSupport != null)
			changeSupport.addPropertyChangeListener(pListener);
	}

	/**
	 * Den Listener wieder abmelden.
	 *
	 * @param pListener {@link PropertyChangeListener}
	 */
	public void removePropertyChangeListener(final PropertyChangeListener pListener) {
		if (changeSupport != null)
			changeSupport.removePropertyChangeListener(pListener);
	}

	/**
	 * @param pChangeSupport the changeSupport to set
	 */
	public void setChangeSupport(PropertyChangeSupport pChangeSupport) {
		changeSupport = pChangeSupport;
	}

	/**
	 * @return the changeSupport
	 */
	private PropertyChangeSupport getChangeSupport() {
		return changeSupport;
	}

	/**
	 * Entfernt die PropertyChangeListeners wieder.
	 */
	public void disposePropertyChangeSupport() {
		PropertyChangeListener[] listeners = changeSupport.getPropertyChangeListeners();
		if (listeners != null) {
			for (int i = 0; i < listeners.length; i++)
				changeSupport.removePropertyChangeListener(listeners[i]);
		}
		//
	}

	/**
	 * Das ist ein bequemer Wrapper um das Werfen eines {@link PropertyChangeEvent} zu kapseln
	 *
	 * @param propertyName The programmatic name of the property that was changed.
	 * @param oldValue The old value of the property.
	 * @param newValue The new value of the property.
	 */
	protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
		if (changeSupport != null)
			changeSupport.firePropertyChange(propertyName, oldValue, newValue);
	}

	/**
	 * Informiert die Listener, dass sich die ID geaendert hat. Grund ist, dass JPA die ID nicht mittels setter setzt,
	 * so dass beim Neuanlegen kein PropertyChangeEvent generiert wird.
	 */
	public final void fireIdChanged() {
		getChangeSupport().firePropertyChange(IMatheAGObjectNames.PROP_ID, null, getId());
	}

	/**
	 * @return
	 */
	public final boolean isNew() {
		return getId() == null;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long pId) {
		if ((this.id != null) && !this.id.equals(pId)) {
			throw new IllegalArgumentException("Not allowed to change the id property.");
		}
		firePropertyChange(IMatheAGObjectNames.PROP_ID, id, id = pId);
	}

	/**
	 * @return the adjusting
	 */
	public final boolean isAdjusting() {
		return adjusting;
	}

	/**
	 * @param pAdjusting the adjusting to set
	 */
	public final void setAdjusting(boolean pAdjusting) {
		adjusting = pAdjusting;
	}

	// /**
	// * @return the uuid
	// */
	// public String getUuid() {
	// return uuid;
	// }
}
