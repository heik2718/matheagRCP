/**
 * 
 */
package de.egladil.mathejungalt.domain.quellen;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.mitglieder.Mitglied;

/**
 * @author aheike
 */
public class MitgliederQuelleItem extends AbstractMatheAGObject {

	/**
	 * @return the quelle
	 */
	public MitgliedergruppenQuelle getQuelle() {
		return quelle;
	}

	/**
	 * @param pQuelle the quelle to set
	 */
	public void setQuelle(MitgliedergruppenQuelle pQuelle) {
		quelle = pQuelle;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 7387938227559967673L;

	/** */
	private Mitglied mitglied;

	/** */
	private MitgliedergruppenQuelle quelle;

	/** */
	private Integer jahre;

	/** */
	private Integer klasse;

	/**
	 * 
	 */
	public MitgliederQuelleItem() {
		super();
	}

	/**
	 * Vervollständigt alle mitgliedabhängigen Attribute aus dem Mitglied.
	 * 
	 * @param pMitglied
	 * @param pQuelle
	 */
	public MitgliederQuelleItem(Mitglied pMitglied, MitgliedergruppenQuelle pQuelle) {
		super();
		mitglied = pMitglied;
		quelle = pQuelle;
		jahre = pMitglied.getAlter();
		klasse = pMitglied.getKlasse();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.domain.AbstractMatheAGObject#getKey()
	 */
	@Override
	public Object getKey() {
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.domain.AbstractMatheAGObject#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object pObj) {
		if (!(pObj instanceof MitgliederQuelleItem)) {
			return false;
		}
		if (this == pObj) {
			return true;
		}
		MitgliederQuelleItem param = (MitgliederQuelleItem) pObj;
		return new EqualsBuilder().append(mitglied, param.getMitglied()).append(quelle, param.getQuelle())
			.append(jahre, param.getJahre()).append(klasse, param.getKlasse()).isEquals();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.domain.AbstractMatheAGObject#hashCode()
	 */
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(mitglied).append(quelle).append(jahre).append(klasse).toHashCode();
	}

	/**
	 * @return the mitglied
	 */
	public Mitglied getMitglied() {
		return mitglied;
	}

	/**
	 * @param pMitglied the mitglied to set
	 */
	public void setMitglied(Mitglied pMitglied) {
		mitglied = pMitglied;
	}

	/**
	 * @return the jahre
	 */
	public Integer getJahre() {
		return jahre;
	}

	/**
	 * @param pJahre the jahre to set
	 */
	public void setJahre(Integer pJahre) {
		jahre = pJahre;
	}

	/**
	 * @return the klasse
	 */
	public Integer getKlasse() {
		return klasse;
	}

	/**
	 * @param pKlasse the klasse to set
	 */
	public void setKlasse(Integer pKlasse) {
		klasse = pKlasse;
	}
}
