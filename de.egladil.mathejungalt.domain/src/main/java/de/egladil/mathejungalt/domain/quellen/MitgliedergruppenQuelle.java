/**
 * 
 */
package de.egladil.mathejungalt.domain.quellen;

import java.util.ArrayList;
import java.util.List;

import de.egladil.mathejungalt.domain.types.EnumTypes.Quellenart;

/**
 * @author aheike
 */
public class MitgliedergruppenQuelle extends AbstractQuelle {

	/** */
	private static final long serialVersionUID = -852362939386366802L;

	/** */
	private List<MitgliederQuelleItem> items;

	/**
	 * 
	 */
	public MitgliedergruppenQuelle() {
		super();
		items = new ArrayList<MitgliederQuelleItem>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.domain.quellen.AbstractQuelle#getArt()
	 */
	@Override
	public Quellenart getArt() {
		return Quellenart.G;
	}

	/**
	 * @param pItem
	 * @return
	 */
	public boolean addItem(MitgliederQuelleItem pItem) {
		if (!items.contains(pItem)) {
			boolean added = items.add(pItem);
			if (added) {
				firePropertyChange(IGruppenquelleNames.PROP_ITEMS, null, items);
			}
			return added;
		}
		return false;
	}

	/**
	 * @param pItem
	 * @return
	 */
	public boolean removeItem(MitgliederQuelleItem pItem) {
		boolean removed = items.remove(pItem);
		if (removed) {
			if (removed) {
				firePropertyChange(IGruppenquelleNames.PROP_ITEMS, items, null);
			}
			return removed;
		}
		return false;
	}

	/**
	 * @return the items
	 */
	public List<MitgliederQuelleItem> getItems() {
		return items;
	}
}
