/**
 *
 */
package de.egladil.mathejungalt.domain.aufgaben;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.IMatheAGObjectNames;
import de.egladil.mathejungalt.domain.quellen.AbstractQuelle;
import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenart;
import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenzweck;
import de.egladil.mathejungalt.domain.types.EnumTypes.Jahreszeit;
import de.egladil.mathejungalt.domain.types.EnumTypes.Quellenart;
import de.egladil.mathejungalt.domain.types.EnumTypes.Thema;

/**
 * @author heike
 */
public class Aufgabe extends AbstractMatheAGObject {

	/** */
	private static final long serialVersionUID = 1256930971529924222L;

	/** */
	private Aufgabenzweck zweck = Aufgabenzweck.S;

	/** */
	private String titel = "";

	/** */
	private String schluessel;

	/** */
	private String verzeichnis;

	/** */
	private Aufgabenart art = Aufgabenart.E;

	/** */
	private Thema thema = Thema.A;

	/** */
	private Integer stufe = -1;

	/** */
	private Jahreszeit jahreszeit = Jahreszeit.I;

	/** */
	private String beschreibung = "";

	/** */
	private Heft heft;

	/** */
	private AbstractQuelle quelle;

	/** */
	private String gesperrtFuerWettbewerb = NOT_LOCKED;

	/** */
	private String gesperrtFuerArbeitsblatt = NOT_LOCKED;

	/** */
	private String zuSchlechtFuerSerie = NOT_LOCKED;

	/** */
	private int anzahlSperrendeReferenzen = 0;

	/**
	 *
	 */
	public Aufgabe() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(getKey());
		sb.append(" ");
		sb.append(getTitel());
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.domain.AbstractMatheAGObject#getKey()
	 */
	@Override
	public Object getKey() {
		return schluessel;
	}

	/**
	 * @return the titel
	 */
	public String getTitel() {
		return titel;
	}

	/**
	 * @param pTitel the titel to set
	 */
	public void setTitel(String pTitel) {
		firePropertyChange(IAufgabeNames.PROP_TITEL, titel, titel = pTitel);
	}

	/**
	 * @return the art
	 */
	public Aufgabenart getArt() {
		return art;
	}

	/**
	 * @param pArt the art to set
	 */
	public void setArt(Aufgabenart pArt) {
		firePropertyChange(IAufgabeNames.PROP_ART, art, art = pArt);
	}

	/**
	 * @return the thema
	 */
	public Thema getThema() {
		return thema;
	}

	/**
	 * @param pThema the thema to set
	 */
	public void setThema(Thema pThema) {
		firePropertyChange(IAufgabeNames.PROP_THEMA, thema, thema = pThema);
	}

	/**
	 * @return the stufe
	 */
	public Integer getStufe() {
		return stufe;
	}

	/**
	 * @param pStufe the stufe to set
	 */
	public void setStufe(Integer pStufe) {
		firePropertyChange(IAufgabeNames.PROP_STUFE, stufe, stufe = pStufe);
	}

	/**
	 * @return the jahreszeit
	 */
	public Jahreszeit getJahreszeit() {
		return jahreszeit;
	}

	/**
	 * @param pJahreszeit the jahreszeit to set
	 */
	public void setJahreszeit(Jahreszeit pJahreszeit) {
		firePropertyChange(IAufgabeNames.PROP_JAHRESZEIT, jahreszeit, jahreszeit = pJahreszeit);
	}

	/**
	 * @return the heft
	 */
	public Heft getHeft() {
		return heft;
	}

	/**
	 * @param pHeft the heft to set
	 */
	public void setHeft(Heft pHeft) {
		firePropertyChange(IAufgabeNames.PROP_HEFT, heft, heft = pHeft);
	}

	/**
	 * @return the beschreibung
	 */
	public String getBeschreibung() {
		return beschreibung;
	}

	/**
	 * @param pBeschreibung the beschreibung to set
	 */
	public void setBeschreibung(String pBeschreibung) {
		firePropertyChange(IAufgabeNames.PROP_BESCHREIBUNG, beschreibung, beschreibung = pBeschreibung);
	}

	/**
	 * @return the quelle
	 */
	public AbstractQuelle getQuelle() {
		return quelle;
	}

	/**
	 * @param pQuelle the quelle to set
	 */
	public void setQuelle(AbstractQuelle pQuelle) {
		if (pQuelle != null && (Quellenart.K.equals(pQuelle.getArt()) || Quellenart.G.equals(pQuelle.getArt()))) {
			setArt(Aufgabenart.Z);
		}
		if (pQuelle != null && Quellenart.N.equals(pQuelle.getArt())) {
			setArt(Aufgabenart.E);
		}
		firePropertyChange(IAufgabeNames.PROP_QUELLE, quelle, quelle = pQuelle);
	}

	/**
	 * @return the schluessel
	 */
	public String getSchluessel() {
		return schluessel;
	}

	/**
	 * @param pSchluessel the schluessel to set
	 */
	public void setSchluessel(String pSchluessel) {
		firePropertyChange(IMatheAGObjectNames.PROP_SCHLUESSEL, schluessel, schluessel = pSchluessel);
	}

	/**
	 * @return the zweck
	 */
	public Aufgabenzweck getZweck() {
		return zweck;
	}

	/**
	 * @param pZweck the zweck to set
	 */
	public void setZweck(Aufgabenzweck pZweck) {
		firePropertyChange(IAufgabeNames.PROP_ZWECK, zweck, zweck = pZweck);
	}

	/**
	 * @return the gesperrt
	 */
	public String getGesperrtFuerWettbewerb() {
		return gesperrtFuerWettbewerb;
	}

	/**
	 * @param pGesperrt the gesperrt to set
	 */
	public void setGesperrtFuerWettbewerb(String pGesperrt) {
		gesperrtFuerWettbewerb = pGesperrt;
	}

	/**
	 * @return the gesperrtFuerArbeitsblatt
	 */
	public String getGesperrtFuerArbeitsblatt() {
		return gesperrtFuerArbeitsblatt;
	}

	/**
	 * @param pGesperrtFuerArbeitsblatt the gesperrtFuerArbeitsblatt to set
	 */
	public void setGesperrtFuerArbeitsblatt(String pGesperrtFuerArbeitsblatt) {
		gesperrtFuerArbeitsblatt = pGesperrtFuerArbeitsblatt;
	}

	/**
	 * @return the dauerhaftFuerSerieGesperrt
	 */
	public final String getZuSchlechtFuerSerie() {
		return zuSchlechtFuerSerie;
	}

	/**
	 * @param pDauerhaftFuerSerieGesperrt the dauerhaftFuerSerieGesperrt to set
	 */
	public final void setZuSchlechtFuerSerie(String pDauerhaftFuerSerieGesperrt) {
		zuSchlechtFuerSerie = pDauerhaftFuerSerieGesperrt;
	}

	/**
	 * @return the anzahlSerien
	 */
	public final int getAnzahlSperrendeReferenzen() {
		return anzahlSperrendeReferenzen;
	}

	/**
	 * @param pAnzahlSerien the anzahlSerien to set
	 */
	public final void setAnzahlSperrendeReferenzen(int pAnzahlSerien) {
		anzahlSperrendeReferenzen = pAnzahlSerien;
	}

	/**
	 * @return the verzeichnis
	 */
	public String getVerzeichnis() {
		return verzeichnis;
	}

	/**
	 * @param pVerzeichnis the verzeichnis to set
	 */
	public void setVerzeichnis(String pVerzeichnis) {
		verzeichnis = pVerzeichnis;
	}
}
