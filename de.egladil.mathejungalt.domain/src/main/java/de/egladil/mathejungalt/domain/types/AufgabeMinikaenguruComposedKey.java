/**
 * 
 */
package de.egladil.mathejungalt.domain.types;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru.Minikaenguru;

/**
 * @author heike
 */
public class AufgabeMinikaenguruComposedKey {

	/** */
	private Aufgabe aufgabe;

	/** */
	private Minikaenguru minikaenguru;

	/**
	 * @param pAufgabe
	 * @param pMinikaenguru
	 */
	public AufgabeMinikaenguruComposedKey(Aufgabe pAufgabe, Minikaenguru pMinikaenguru) {
		super();
		aufgabe = pAufgabe;
		minikaenguru = pMinikaenguru;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object pObj) {
		if (this == pObj)
			return true;
		if (!this.getClass().isAssignableFrom(pObj.getClass())) {
			return false;
		}
		AufgabeMinikaenguruComposedKey param = (AufgabeMinikaenguruComposedKey) pObj;
		EqualsBuilder eb = new EqualsBuilder();
		eb.append(aufgabe, param.getAufgabe());
		eb.append(minikaenguru, param.getMinikaenguru());
		return eb.isEquals();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		HashCodeBuilder hb = new HashCodeBuilder();
		hb.append(aufgabe);
		hb.append(minikaenguru);
		return hb.toHashCode();
	}

	/**
	 * @return the aufgabe
	 */
	public Aufgabe getAufgabe() {
		return aufgabe;
	}

	/**
	 * @return the serie
	 */
	public Minikaenguru getMinikaenguru() {
		return minikaenguru;
	}
}
