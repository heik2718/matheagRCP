/**
 *
 */
package de.egladil.mathejungalt.domain.mcraetsel;

/**
 * @author heike
 */
public final class GeneratorUtils {

	/**
   *
   */
	private GeneratorUtils() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param pPrefix String absoluter Pfad des Arbeitsverzeichnisses erweitert um einen optionalen prefix.
	 * @return
	 */
	public static String getPathPreviewLaTeXAufgabe(String pPrefix) {
		String pathResult = pPrefix + ".tex";
		return pathResult;
	}

	/**
	 * @param pPrefix String absoluter Pfad des Arbeitsverzeichnisses erweitert um einen optionalen prefix.
	 * @return String - der absolute Pfad des Lösung-LaTeX-Files für die Vorschau.
	 */
	public static String getPathPreviewLaTeXLoesung(String pPrefix) {
		String pathResult = pPrefix + "_l.tex";
		return pathResult;
	}

	/**
	 * @param pPrefix String absoluter Pfad des Arbeitsverzeichnisses erweitert um einen optionalen prefix.
	 * @return String - der absolute Pfad des Aufgabe-DVI-Files.
	 */
	public static String getPathPreviewDviAufgabe(String pPrefix) {
		String pathResult = pPrefix + ".dvi";
		return pathResult;
	}

	/**
	 * @param pPrefix String absoluter Pfad des Arbeitsverzeichnisses erweitert um einen optionalen prefix.
	 * @return String - der absolute Pfad des Lösung-DVI-Files.
	 */
	public static String getPathPreviewDviLoesung(String pPrefix) {
		String pathResult = pPrefix + "_l.dvi";
		return pathResult;
	}
}
