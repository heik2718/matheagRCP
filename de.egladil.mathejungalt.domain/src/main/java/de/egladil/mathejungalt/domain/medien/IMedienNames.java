/**
 * 
 */
package de.egladil.mathejungalt.domain.medien;

/**
 * This generated interface defines constants for all attributes and associatations in
 * {@link de.egladil.mathe.domain.medien.Buch}.
 * <p>
 * 
 * @author heike
 */
public interface IMedienNames {

	public static final String PROP_AUTOREN = "autoren";

	public static final String PROP_TITEL = "titel";

	public static final int LENGTH_KEY = 3;

	public static final int LENGTH_TITEL = 100;
}
