/**
 * 
 */
package de.egladil.mathejungalt.domain.medien;

import de.egladil.mathejungalt.domain.types.EnumTypes.Medienart;

/**
 * @author heike
 */
public class Zeitschrift extends AbstractPrintmedium {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2274191804925519281L;

	/**
	 * 
	 */
	public Zeitschrift() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.domain.medien.AbstractPrintmedium#getArt()
	 */
	@Override
	public Medienart getArt() {
		return Medienart.Z;
	}
}
