/**
 * 
 */
package de.egladil.mathejungalt.domain.quellen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mathejungalt.domain.medien.Zeitschrift;
import de.egladil.mathejungalt.domain.types.EnumTypes.Quellenart;

/**
 * @author heike
 */
public class Zeitschriftquelle extends AbstractQuelle {

	/** */
	private static final long serialVersionUID = -4015610781860208889L;

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(Zeitschriftquelle.class);

	/** */
	private Zeitschrift zeitschrift;

	/** */
	private Integer ausgabe;

	/** */
	private Integer jahrgang;

	/**
	 * 
	 */
	public Zeitschriftquelle() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.domain.quellen.AbstractQuelle#toString()
	 */
	@Override
	public String toString() {
		if (LOG.isDebugEnabled())
			LOG.debug("Start toString()");
		String string = super.toString();
		if (zeitschrift != null) {
			string += "-" + zeitschrift.getTitel();
		}
		string += "(" + ausgabe + ")" + jahrgang;
		return string;
	}

	/**
	 * @return the zeitschrift
	 */
	public Zeitschrift getZeitschrift() {
		return zeitschrift;
	}

	/**
	 * @param pZeitschrift the zeitschrift to set
	 */
	public void setZeitschrift(Zeitschrift pZeitschrift) {
		firePropertyChange(IZeitschriftquelleNames.PROP_ZEITSCHRIFT, zeitschrift, zeitschrift = pZeitschrift);
	}

	/**
	 * @return the ausgabe
	 */
	public Integer getAusgabe() {
		return ausgabe;
	}

	/**
	 * @param pAusgabe the ausgabe to set
	 */
	public void setAusgabe(Integer pAusgabe) {
		firePropertyChange(IZeitschriftquelleNames.PROP_AUSGABE, ausgabe, ausgabe = pAusgabe);
	}

	/**
	 * @return the jahrgang
	 */
	public Integer getJahrgang() {
		return jahrgang;
	}

	/**
	 * @param pJahrgang the jahrgang to set
	 */
	public void setJahrgang(Integer pJahrgang) {
		firePropertyChange(IZeitschriftquelleNames.PROP_JAHRGANG, jahrgang, jahrgang = pJahrgang);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.domain.quellen.AbstractQuelle#getArt()
	 */
	@Override
	public Quellenart getArt() {
		return Quellenart.Z;
	}
}
