/**
 *
 */
package de.egladil.mathejungalt.domain.mcraetsel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.StringUtils;

import de.egladil.base.exceptions.MatheJungAltInconsistentEntityException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.types.MCAufgabeRaetselComposedKey;

/**
 * Ein MCRaetselItem hat eine MCAufgabe und eine Nummer.
 *
 * @author heike
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "mcRaetselItem")
public class MCArchivraetselItem extends AbstractMatheAGObject implements IMatheAGEntity {

	public final static String PROP_AUFGABE = "aufgabe";

	public final static String PROP_NUMMER = "nummer";

	/**
   *
   */
	@XmlTransient
	private static final long serialVersionUID = 6671481012807885009L;

	@XmlElement(name = "aufgabe")
	private MCAufgabe aufgabe;

	@XmlElement(name = "nummer")
	private String nummer;

	@XmlTransient
	private MCArchivraetsel raetsel;

	/**
   *
   */
	public MCArchivraetselItem() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.domain.AbstractMatheAGObject#compareTo(de.egladil.mathejungalt.domain.AbstractMatheAGObject
	 * )
	 */
	@Override
	public int compareTo(AbstractMatheAGObject pO) {
		if (pO instanceof MCArchivraetselItem) {
			Integer i1 = Integer.valueOf(nummer);
			Integer i2 = Integer.valueOf(((MCArchivraetselItem) pO).getNummer());
			return i1 - i2;
		}
		return super.compareTo(pO);
	}

	/**
	 * @return the aufgabe
	 */
	public final MCAufgabe getAufgabe() {
		return aufgabe;
	}

	/**
	 * @param pAufgabe the aufgabe to set
	 */
	public final void setAufgabe(MCAufgabe pAufgabe) {
		firePropertyChange(PROP_AUFGABE, aufgabe, aufgabe = pAufgabe);
	}

	/**
	 * @return the nummer
	 */
	public final String getNummer() {
		return nummer;
	}

	/**
	 * @param pNummer the nummer to set
	 */
	public final void setNummer(String pNummer) {
		firePropertyChange(PROP_NUMMER, nummer, nummer = pNummer);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((aufgabe == null) ? 0 : aufgabe.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MCArchivraetselItem other = (MCArchivraetselItem) obj;
		if (aufgabe == null) {
			if (other.aufgabe != null)
				return false;
		} else if (!aufgabe.equals(other.aufgabe))
			return false;
		return true;
	}

	/**
	 * @return the raetsel
	 */
	public final MCArchivraetsel getRaetsel() {
		return raetsel;
	}

	/**
	 * @param pRaetsel the raetsel to set
	 */
	public final void setRaetsel(MCArchivraetsel pRaetsel) {
		raetsel = pRaetsel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.domain.AbstractMatheAGObject#getKey()
	 */
	@Override
	public Object getKey() {
		if (aufgabe == null && raetsel == null) {
			return getId();
		}
		return new MCAufgabeRaetselComposedKey(aufgabe, raetsel);
	}

	@Override
	public void checkConsistent() throws MatheJungAltInconsistentEntityException {
		StringBuffer sb = new StringBuffer();
		boolean consistent = true;
		if (aufgabe == null) {
			sb.append("aufgabe darf nicht null sein\n");
			consistent = false;
		}
		if (nummer == null || nummer.isEmpty() || nummer.length() > 7) {
			sb.append("nummer darf nicht null, leer oder laenger als 7 Zeichen sein\n");
			consistent = false;
		}
		if (raetsel == null) {
			sb.append("raetsel darf nicht null sein\n");
			consistent = false;
		}
		if (!consistent) {
			throw new MatheJungAltInconsistentEntityException("Das R\u00e4tselitem kann nicht gespeichert werden:\n\n"
				+ sb.toString());
		}
	}

	/**
	 * @return
	 */
	public boolean canGenerate() {
		if (aufgabe == null) {
			return false;
		}
		if (!aufgabe.canGenerate()) {
			return false;
		}
		if (nummer == null || nummer.isEmpty()) {
			return false;
		}
		return true;
	}

	/**
	 * @param pPathWorkDir
	 * @return
	 */
	public String getPathPrefix(String pPathWorkDir) {
		return pPathWorkDir + "/" + aufgabe.getSchluessel();
	}
}
