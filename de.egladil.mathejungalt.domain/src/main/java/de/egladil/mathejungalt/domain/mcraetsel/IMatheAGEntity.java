/**
 *
 */
package de.egladil.mathejungalt.domain.mcraetsel;

import de.egladil.base.exceptions.MatheJungAltInconsistentEntityException;

/**
 * @author heike
 */
public interface IMatheAGEntity {

	/**
	 * @throws MatheJungAltInconsistentEntityException
	 */
	void checkConsistent() throws MatheJungAltInconsistentEntityException;
}
