/**
 *
 */
package de.egladil.mathejungalt.domain.types;

import java.util.ArrayList;
import java.util.List;

import de.egladil.base.exceptions.EgladilBaseException;

/**
 * @author heike
 */
public enum MCAufgabenstufen {

	STUFE_12("Klassen 1 und 2") {

		@Override
		public int getStufe() {
			return 2;
		}

		@Override
		public Long getVorgabezeitProAufgabeInMillis() {
			return 240000l;
		}

		@Override
		public int getAnzahlAufgabenJeKategorie() {
			return 5;
		}

		@Override
		public String getHtmlIdSuffix() {
			return "12";
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see de.egladil.mathejungalt.domain.types.MCAufgabenstufen#internalToAndroidStufe()
		 */
		@Override
		String internalToAndroidStufe() {
			return "STUFE_2";
		}
	},

	STUFE_34("Klassen 3 und 4") {

		@Override
		public int getStufe() {
			return 3;
		}

		@Override
		public Long getVorgabezeitProAufgabeInMillis() {
			return 187500l;
		}

		@Override
		public String getHtmlIdSuffix() {
			return "34";
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see de.egladil.mathejungalt.domain.types.MCAufgabenstufen#internalToAndroidStufe()
		 */
		@Override
		String internalToAndroidStufe() {
			return "STUFE_3";
		}
	},

	STUFE_56("Klassen 5 und 6") {

		@Override
		public int getStufe() {
			return 4;
		}

		@Override
		public Long getVorgabezeitProAufgabeInMillis() {
			return 187500l;
		}

		@Override
		public String getHtmlIdSuffix() {
			return "56";
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see de.egladil.mathejungalt.domain.types.MCAufgabenstufen#internalToAndroidStufe()
		 */
		@Override
		String internalToAndroidStufe() {
			return "STUFE_4";
		}

	},

	STUFE_78("Klassen 7 und 8") {

		@Override
		public int getStufe() {
			return 5;
		}

		@Override
		public String getHtmlIdSuffix() {
			return "78";
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see de.egladil.mathejungalt.domain.types.MCAufgabenstufen#internalToAndroidStufe()
		 */
		@Override
		String internalToAndroidStufe() {
			return "STUFE_5";
		}

	},

	STUFE_910("Klassen 9 und 10") {

		@Override
		public int getStufe() {
			return 6;
		}

		@Override
		public String getHtmlIdSuffix() {
			return "910";
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see de.egladil.mathejungalt.domain.types.MCAufgabenstufen#internalToAndroidStufe()
		 */
		@Override
		String internalToAndroidStufe() {
			return "STUFE_6";
		}

	},

	STUFE_ERW("ab Klasse 11") {

		@Override
		public int getStufe() {
			return 7;
		}

		@Override
		public String getHtmlIdSuffix() {
			return "1113";
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see de.egladil.mathejungalt.domain.types.MCAufgabenstufen#internalToAndroidStufe()
		 */
		@Override
		String internalToAndroidStufe() {
			return "STUFE_6";
		}
	};

	private final String bezeichnung;

	/**
	 * @param pLabel
	 */
	private MCAufgabenstufen(String pLabel) {
		bezeichnung = pLabel;
	}

	/**
	 * @return the label
	 */
	public final String getLabel() {
		return bezeichnung;
	}

	public abstract String getHtmlIdSuffix();

	/**
	 * @return
	 */
	public abstract int getStufe();

	/**
	 * Die Standardvorgabezeit je Aufgabe beträgt 2.5 Minuten (analog zum Känguru-Wettbwerb)
	 *
	 * @return Long
	 */
	public Long getVorgabezeitProAufgabeInMillis() {
		Long result = 150000l;
		return result;
	}

	/**
	 * Eine Empfehlung für die Aufgabenanzahl je Ktegorie (3-, 4- bzw. 5-Punkte). Standard ist 6.
	 *
	 * @return int
	 */
	public int getAnzahlAufgabenJeKategorie() {
		return 6;
	}

	/**
	 * @return
	 */
	public static MCAufgabenstufen[] valuesSorted() {
		return new MCAufgabenstufen[] { STUFE_12, STUFE_34, STUFE_56, STUFE_78, STUFE_910, STUFE_ERW };
	}

	/**
	 * @param pInt int Zahl zwischen 1 und 6
	 * @return {@link MCAufgabenstufen}
	 */
	public static MCAufgabenstufen valueOf(int pInt) throws EgladilBaseException {
		switch (pInt) {
		case 2:
			return STUFE_12;
		case 3:
			return STUFE_34;
		case 4:
			return STUFE_56;
		case 5:
			return STUFE_78;
		case 6:
			return STUFE_910;
		case 7:
			return STUFE_ERW;
		default:
			throw new EgladilBaseException("ungueltige Stufe " + pInt + " uebergaben");
		}
	}

	/**
	 *
	 * @param pLabel
	 * @return
	 */
	public static MCAufgabenstufen getByLabel(String pLabel) {
		if (STUFE_12.getLabel().equals(pLabel)) {
			return STUFE_12;
		}
		if (STUFE_34.getLabel().equals(pLabel)) {
			return STUFE_34;
		}
		if (STUFE_56.getLabel().equals(pLabel)) {
			return STUFE_56;
		}
		if (STUFE_78.getLabel().equals(pLabel)) {
			return STUFE_78;
		}
		if (STUFE_910.getLabel().equals(pLabel)) {
			return STUFE_910;
		}
		if (STUFE_ERW.getLabel().equals(pLabel)) {
			return STUFE_ERW;
		}
		throw new IllegalArgumentException("keine MCAufgabenstufe mit Label '" + pLabel + "' bekannt");
	}

	/**
	 * @return
	 */
	public static String[] toLabels() {
		List<String> liste = new ArrayList<String>(values().length);
		for (MCAufgabenstufen stufe : values()) {
			liste.add(stufe.getLabel());
		}
		return liste.toArray(new String[0]);
	}

	/**
	 * @param raetsel
	 * @return
	 */
	public String mcAufgabenstufeToAndroidStufeString(String titel) {
		String stufe = "";
		if (titel.contains("Minikänguru")) {
			stufe = "MINI";
		} else {
			stufe = internalToAndroidStufe();
		}
		return stufe;
	}

	abstract String internalToAndroidStufe();
}
