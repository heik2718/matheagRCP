package de.egladil.mathejungalt.domain;

/**
 * @author Winkelv
 */
public interface IMatheAGObjectNames {

	/** */
	public static final String PROP_ID = "id";

	/** */
	public static final String PROP_SCHLUESSEL = "schluessel";

}
