/**
 * 
 */
package de.egladil.mathejungalt.domain.aufgabensammlungen.minikaenguru;

/**
 * @author Heike Winkelvoss (www.egladil.de)
 */
public interface IMinikaenguruitemNames {

	public static final String PROP_MINIKAENGURU = "minikaenguru";

	public static final int LENGTH_KEY = 4;
}
