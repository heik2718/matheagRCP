/**
 * 
 */
package de.egladil.mathejungalt.domain.schulen;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Zusammengesetzter Schluessel fuer Minikänguruteilnahmen.
 * 
 * @author heike
 */
public class SchuleKontaktComposedKey {

	/** */
	private Schule schule;

	/** */
	private Kontakt kontakt;

	/**
	 * @param pSchule
	 * @param pKontakt
	 */
	public SchuleKontaktComposedKey(Schule pSchule, Kontakt pKontakt) {
		super();
		schule = pSchule;
		kontakt = pKontakt;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object pObj) {
		if (this == pObj)
			return true;
		if (!this.getClass().isAssignableFrom(pObj.getClass())) {
			return false;
		}
		SchuleKontaktComposedKey param = (SchuleKontaktComposedKey) pObj;
		EqualsBuilder eb = new EqualsBuilder();
		eb.append(schule, param.getSchule());
		eb.append(kontakt, param.getKontakt());
		return eb.isEquals();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		HashCodeBuilder hb = new HashCodeBuilder();
		hb.append(schule);
		hb.append(kontakt);
		return hb.toHashCode();
	}

	/**
	 * @return the aufgabe
	 */
	public Schule getSchule() {
		return schule;
	}

	/**
	 * @return the serie
	 */
	public Kontakt getKontakt() {
		return kontakt;
	}
}
