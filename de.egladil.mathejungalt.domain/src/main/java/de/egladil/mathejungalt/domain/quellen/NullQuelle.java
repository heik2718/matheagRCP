/**
 * 
 */
package de.egladil.mathejungalt.domain.quellen;

import de.egladil.mathejungalt.domain.types.EnumTypes.Quellenart;

/**
 * @author Winkelv
 */
public class NullQuelle extends AbstractQuelle {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7943295611076703594L;

	/**
	 * 
	 */
	public NullQuelle() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.domain.quellen.AbstractQuelle#toString()
	 */
	@Override
	public String toString() {
		return "Heike Winkelvo\u00DF";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathe.core.domain.quellen.AbstractQuelle#getArt()
	 */
	@Override
	public Quellenart getArt() {
		return Quellenart.N;
	}
}
