/**
 * 
 */
package de.egladil.mathejungalt.domain.schulen;

/**
 * This generated interface defines constants for all attributes and associatations in
 * {@link de.egladil.mathejungalt.domain.schulen.Schule}.
 * <p>
 * These constants are useful for example when building criterias.
 * 
 * @author heike
 */
public interface IKontaktNames {

	public static final String PROP_NAME = "name";

	public static final String PROP_EMAIL = "email";

	public static final String PROP_ABO = "abo";

	public final static int LENGTH_NAME = 100;

	public final static int LENGTH_EMAIL = 100;

	public final static int LENGTH_ABO = 1;

	public static final int LENGTH_SCHLUESSEL = 3;

	public static final String PROP_SCHULKONTAKTE = "schulkontakte";
}
