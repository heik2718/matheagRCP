/**
 * 
 */
package de.egladil.mathejungalt.domain.aufgabensammlungen.arbeitsblaetter;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.IMatheAGObjectNames;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;

/**
 * Ein Arbeitsblatt (Sammlung einzelner Aufgaben).
 * 
 * @author heike
 */
public class Arbeitsblatt extends AbstractMatheAGObject implements IAufgabensammlung {

	public static final int SPERREND = 1;

	public static final int NICHT_SPERREND = 0;

	/**
	 * 
	 */
	private static final long serialVersionUID = -4248348871863070234L;

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(Arbeitsblatt.class);

	/** */
	private String schluessel;

	/** */
	private String titel = "";

	/** */
	private int beendet;

	/** */
	private int sperrend = NICHT_SPERREND;

	/** */
	private List<Arbeitsblattitem> arbeitsblattitems;

	/** */
	private boolean itemsLoaded = false;

	/**
	 * 
	 */
	public Arbeitsblatt() {
		super();
		arbeitsblattitems = new ArrayList<Arbeitsblattitem>();
		itemsLoaded = false;
	}

	/**
	 * 
	 */
	public void initItems() {
		if (itemsLoaded) {
			return;
		}
		arbeitsblattitems = new ArrayList<Arbeitsblattitem>();
	}

	/**
	 * 
	 */
	public void clearItems() {
		if (!itemsLoaded)
			return;
		arbeitsblattitems.clear();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.domain.aufgabensammlungen.IAufgabensammlung#anzahlAufgaben()
	 */
	public int anzahlAufgaben() {
		if (itemsLoaded) {
			return arbeitsblattitems.size();
		}
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.domain.AbstractMatheAGObject#getKey()
	 */
	@Override
	public Object getKey() {
		return schluessel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung#canAdd(de.egladil.mathejungalt.domain.aufgaben
	 * .Aufgabe)
	 */
	@Override
	public boolean canAdd(Aufgabe pAufgabe) {
		if (pAufgabe == null) {
			return false;
		}
		if (sperrend == SPERREND) {
			return pAufgabe.getGesperrtFuerArbeitsblatt().equals(Aufgabe.NOT_LOCKED);
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return titel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung#addItem(de.egladil.mathejungalt.domain.
	 * aufgabensammlungen.IAufgabensammlungItem)
	 */
	public boolean addItem(final IAufgabensammlungItem pItem) {
		if (pItem == null) {
			String msg = MatheJungAltException.argumentNullMessage("addItem(IAufgabensammlungItem)");
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		if (!(pItem instanceof Arbeitsblattitem)) {
			String msg = "addItem(): unzulässiger Typ " + pItem.getClass().getName();
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		boolean added = arbeitsblattitems.add((Arbeitsblattitem) pItem);
		if (added) {
			fireItemsChanged();
		}
		return added;
	}

	/**
	 * 
	 */
	private void fireItemsChanged() {
		firePropertyChange(IArbeitsblattNames.PROP_ITEMS, null, arbeitsblattitems.iterator());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung#removeItem(de.egladil.mathejungalt.domain
	 * .aufgabensammlungen.IAufgabensammlungItem)
	 */
	public boolean removeItem(IAufgabensammlungItem pItem) {
		LOG.debug("Start removeItem()");
		boolean removed = arbeitsblattitems.remove(pItem);
		if (removed) {
			fireItemsChanged();
		}
		return removed;
	}

	/**
	 * @return the jahr
	 */
	public String getSchluessel() {
		return schluessel;
	}

	/**
	 * @param pSchluessel the jahr to set
	 */
	public void setSchluessel(String pSchluessel) {
		firePropertyChange(IMatheAGObjectNames.PROP_SCHLUESSEL, schluessel, schluessel = pSchluessel);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung#isItemsLoaded()
	 */
	public boolean isItemsLoaded() {
		return itemsLoaded;
	}

	/**
	 * @param pItemsLoaded the itemsLoaded to set
	 */
	public void setItemsLoaded(boolean pItemsLoaded) {
		itemsLoaded = pItemsLoaded;
	}

	/**
	 * @return the minikaenguruitems
	 */
	public List<Arbeitsblattitem> getArbeitsblattitems() {
		return arbeitsblattitems;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung#getDomainObject()
	 */
	@Override
	public AbstractMatheAGObject getDomainObject() {
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung#getItems()
	 */
	@Override
	public List<IAufgabensammlungItem> getItems() {
		List<IAufgabensammlungItem> items = new ArrayList<IAufgabensammlungItem>();
		for (Arbeitsblattitem item : arbeitsblattitems) {
			items.add(item);
		}
		return items;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung#getItems(int)
	 */
	@Override
	public List<IAufgabensammlungItem> getItems(int pStufe) {
		List<IAufgabensammlungItem> items = new ArrayList<IAufgabensammlungItem>();
		for (Arbeitsblattitem item : arbeitsblattitems) {
			if (item.getAufgabe().getStufe() == pStufe) {
				items.add(item);
			}
		}
		return items;
	}

	/**
	 * @return the titel
	 */
	public String getTitel() {
		return titel;
	}

	/**
	 * @param pTitel the titel to set
	 */
	public void setTitel(String pTitel) {
		firePropertyChange(IArbeitsblattNames.PROP_TITEL, titel, titel = pTitel);
	}

	/**
	 * @return
	 */
	public boolean isClosed() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung#getBeendet()
	 */
	public int getBeendet() {
		return beendet;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung#setBeendet(int)
	 */
	public void setBeendet(int pBeendet) {
		firePropertyChange(IArbeitsblattNames.PROP_BEENDET, beendet, beendet = pBeendet);
	}

	/**
	 * @return the sperrend
	 */
	public int getSperrend() {
		return sperrend;
	}

	/**
	 * @param pSperrend the sperrend to set
	 */
	public void setSperrend(int pSperrend) {
		firePropertyChange(IArbeitsblattNames.PROP_SPERREND, sperrend, sperrend = pSperrend);
	}
}
