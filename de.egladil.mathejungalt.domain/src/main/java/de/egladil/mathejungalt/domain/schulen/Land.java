/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.domain.schulen;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;

/**
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class Land extends AbstractMatheAGObject {

	private static final String NULL_LAND = "NULL";

	/**
   *
   */
	private static final long serialVersionUID = 6189327968603652297L;

	private String schluessel;

	private String bezeichnung;

	/**
   *
   */
	public Land() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(AbstractMatheAGObject pO) {
		Land other = (Land) pO;
		if (this.isNullLand()) {
			return -1;
		}
		if (other.isNullLand()) {
			return 1;
		}
		return this.bezeichnung.compareTo(other.getBezeichnung());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.domain.AbstractMatheAGObject#getKey()
	 */
	@Override
	public Object getKey() {
		return schluessel;
	}

	/**
	 * @return the schluessel
	 */
	public String getSchluessel() {
		return schluessel;
	}

	/**
	 * @param schluessel the schluessel to set
	 */
	public void setSchluessel(String schluessel) {
		this.schluessel = schluessel;
	}

	/**
	 * @return the bezeichnung
	 */
	public String getBezeichnung() {
		return bezeichnung;
	}

	/**
	 * @param bezeichnung the bezeichnung to set
	 */
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	/**
	 * @return
	 */
	public static Land createNullLand() {
		Land land = new Land();
		land.setBezeichnung("kein Land");
		land.setSchluessel(NULL_LAND);
		return land;
	}

	/**
	 * @return
	 */
	public boolean isNullLand() {
		return NULL_LAND.equals(this.getSchluessel());
	}

	public String getLabel() {
		return bezeichnung + " - " + schluessel;
	}
}
