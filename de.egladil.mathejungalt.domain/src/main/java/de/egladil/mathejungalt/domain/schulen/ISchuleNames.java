/**
 *
 */
package de.egladil.mathejungalt.domain.schulen;

/**
 * This generated interface defines constants for all attributes and associatations in
 * {@link de.egladil.mathejungalt.domain.schulen.Schule}.
 * <p>
 * These constants are useful for example when building criterias.
 *
 * @author heike
 */
public interface ISchuleNames {

	public static final String PROP_NAME = "name";

	public static final String PROP_ANSCHRIFT = "anschrift";

	public static final String PROP_TELEFONNUMMER = "telefonnummer";

	public static final String PROP_LAND = "land";

	public static final String PROP_MINI_TEILNAHMEN = "minikaenguruTeilnahmen";

	public final static int LENGTH_NAME = 156;

	public final static int LENGTH_ANSCHRIFT = 256;

	public final static int LENGTH_TELEFONNUMMER = 45;

	public final static int LENGTH_LAND = 5;

	public static final int LENGTH_SCHLUESSEL = 3;
}
