/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.domain.schulen;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;

/**
 * <p>
 * NOTE: THIS WIDGET AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 * 
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class Schulkontakt extends AbstractMatheAGObject {

	private static final long serialVersionUID = -2905236335302556903L;

	private Schule schule;

	private Kontakt kontakt;

	/**
   * 
   */
	public Schulkontakt() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.domain.AbstractMatheAGObject#getKey()
	 */
	@Override
	public Object getKey() {
		if (schule == null && kontakt == null) {
			return getId();
		}
		return new SchuleKontaktComposedKey(schule, kontakt);
	}

	/**
	 * @return the schule
	 */
	public final Schule getSchule() {
		return schule;
	}

	/**
	 * @param pSchule the schule to set
	 */
	public final void setSchule(Schule pSchule) {
		schule = pSchule;
	}

	/**
	 * @return the kontakt
	 */
	public final Kontakt getKontakt() {
		return kontakt;
	}

	/**
	 * @param pKontakt the kontakt to set
	 */
	public final void setKontakt(Kontakt pKontakt) {
		kontakt = pKontakt;
	}

	/**
	 * @param pSchule
	 * @param pKontakt
	 * @return
	 */
	public static Schulkontakt create(Schule pSchule, Kontakt pKontakt) {
		Schulkontakt schulkontakt = new Schulkontakt();
		schulkontakt.setSchule(pSchule);
		schulkontakt.setKontakt(pKontakt);
		pKontakt.addSchulkontakt(schulkontakt);
		return schulkontakt;
	}
}
