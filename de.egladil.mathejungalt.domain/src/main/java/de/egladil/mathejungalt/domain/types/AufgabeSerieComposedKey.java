/**
 * 
 */
package de.egladil.mathejungalt.domain.types;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.serien.Serie;

/**
 * @author heike
 */
public class AufgabeSerieComposedKey {

	/** */
	private Aufgabe aufgabe;

	/** */
	private Serie serie;

	/**
	 * @param pAufgabe
	 * @param pSerie
	 */
	public AufgabeSerieComposedKey(Aufgabe pAufgabe, Serie pSerie) {
		super();
		aufgabe = pAufgabe;
		serie = pSerie;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object pObj) {
		if (this == pObj)
			return true;
		if (!this.getClass().isAssignableFrom(pObj.getClass())) {
			return false;
		}
		AufgabeSerieComposedKey param = (AufgabeSerieComposedKey) pObj;
		EqualsBuilder eb = new EqualsBuilder();
		eb.append(aufgabe, param.getAufgabe());
		eb.append(serie, param.getSerie());
		return eb.isEquals();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		HashCodeBuilder hb = new HashCodeBuilder();
		hb.append(aufgabe);
		hb.append(serie);
		return hb.toHashCode();
	}

	/**
	 * @return the aufgabe
	 */
	public Aufgabe getAufgabe() {
		return aufgabe;
	}

	/**
	 * @return the serie
	 */
	public Serie getSerie() {
		return serie;
	}
}
