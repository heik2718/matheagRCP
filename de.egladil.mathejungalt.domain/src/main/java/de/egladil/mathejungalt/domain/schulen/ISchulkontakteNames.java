/**
 * 
 */
package de.egladil.mathejungalt.domain.schulen;

/**
 * This generated interface defines constants for all attributes and associatations in
 * {@link de.egladil.mathejungalt.domain.schulen.Schule}.
 * <p>
 * These constants are useful for example when building criterias.
 * 
 * @author heike
 */
public interface ISchulkontakteNames {

	public static final String PROP_KONTAKT = "kontakt";

	public static final String PROP_SCHULE = "schule";

	public static final String SQL_KONTAKT = "kontakt";

	public static final String SQL_SCHULE = "schule";

	public final static int LENGTH_SCHULE = 20;

	public final static int LENGTH_KONTAKT = 20;
}
