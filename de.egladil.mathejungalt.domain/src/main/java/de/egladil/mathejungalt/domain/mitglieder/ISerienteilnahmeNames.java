/**
 * 
 */
package de.egladil.mathejungalt.domain.mitglieder;

/**
 * This generated interface defines constants for all attributes and associatations in {@link Mitglied}
 * <p>
 * These constants are useful for example when building criterias.
 * 
 * @author heike
 */
public interface ISerienteilnahmeNames {

	public static final String SQL_MITGLIED = "mitglied";

	public static final String SQL_SERIE = "serie";

	public static final String PROP_FRUEHSTARTER = "fruehstarter";

	public static final String PROP_PUNKTE = "punkte";

	public static final String PROP_SERIE = "serie";

	public static final String PROP_MITGLIED = "mitglied";
}
