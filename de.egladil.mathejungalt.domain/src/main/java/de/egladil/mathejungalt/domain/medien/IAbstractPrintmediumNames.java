/**
 * 
 */
package de.egladil.mathejungalt.domain.medien;

/**
 * This generated interface defines constants for all attributes and associatations in
 * {@link de.egladil.medien.domain.AbstractPrintmedium}.
 * <p>
 * These constants are useful for example when building criterias.
 * 
 * @author heike
 */
public interface IAbstractPrintmediumNames {

	public static final String PROP_TITEL = "titel";

	public static final String SQL_ART = "art";

	public static final int LENGTH_SCHLUESSEL = 3;

	public static final int LENGTH_TITEL = 100;
}
