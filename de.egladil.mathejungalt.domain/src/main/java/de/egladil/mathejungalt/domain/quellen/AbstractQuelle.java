/**
 *
 */
package de.egladil.mathejungalt.domain.quellen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.types.EnumTypes.Quellenart;

/**
 * @author heike
 */
public abstract class AbstractQuelle extends AbstractMatheAGObject {

	/**
	 *
	 */
	private static final long serialVersionUID = -3889301699835740213L;

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(AbstractQuelle.class);

	/** */
	private String schluessel;

	/** */
	private Quellenart art;

	/** */
	private boolean completelyLoaded = false;

	/**
	 *
	 */
	public AbstractQuelle() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.domain.AbstractMatheAGObject#getKey()
	 */
	@Override
	public Object getKey() {
		return schluessel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if (LOG.isDebugEnabled())
			LOG.debug("Start toString()");
		return (getKey() + "-" + art.toString());
	}

	/**
	 * @return the schluessel
	 */
	public String getSchluessel() {
		return schluessel;
	}

	/**
	 * @param pSchluessel the schluessel to set
	 */
	public void setSchluessel(String pSchluessel) {
		firePropertyChange(IQuelleNames.SCHLUESSEL, schluessel, schluessel = pSchluessel);
	}

	/**
	 * @return {@link Quellenart}
	 */
	public abstract Quellenart getArt();

	/**
	 * @return the completelyLoaded
	 */
	public boolean isCompletelyLoaded() {
		return completelyLoaded;
	}

	/**
	 * @param pCompletelyLoaded the completelyLoaded to set
	 */
	public void setCompletelyLoaded(boolean pCompletelyLoaded) {
		completelyLoaded = pCompletelyLoaded;
	}

	/**
	 * @param art the art to set
	 */
	public void setArt(Quellenart art) {
		this.art = art;
	}
}
