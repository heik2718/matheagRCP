/**
 * 
 */
package de.egladil.mathejungalt.domain.quellen;

/**
 * This generated interface defines constants for all attributes and associatations in
 * {@link de.egladil.mathe.domain.quelle.Buchquelle}.
 * <p>
 * These constants are useful for example when building criterias.
 * 
 * @author heike
 */
public interface IBuchquelleNames extends IQuelleNames {

	public static final String SQL_SEITE = "seite";

	public static final String SQL_BUCH = "buch";

	public static final String PROP_SEITE = "seite";

	public static final String PROP_BUCH = "buch";
}
