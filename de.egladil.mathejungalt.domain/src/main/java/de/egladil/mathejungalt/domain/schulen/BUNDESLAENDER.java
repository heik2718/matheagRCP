/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.domain.schulen;

/**
 * <p>
 * NOTE: THIS WIDGET AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 * 
 * @author Heike Winkelvoß (public@egladil.de)
 */
public enum BUNDESLAENDER {
	BW("Baden-Württemberg"),

	BY("Bayern"),

	BE("Berlin"),

	BB("Brandenburg"),

	HB("Bremen"),

	CN("China"),

	HH("Hamburg"),

	HE("Hessen"),

	MV("Mecklenburg-Vorpommern"),

	NI("Niedersachsen"),

	NW("Nordrhein-Westfalen"),

	AU("Österreich"),

	RP("Rheinland-Pfalz"),

	RU("Russland"),

	SL("Saarland"),

	SN("Sachsen"),

	ST("Sachsen-Anhlt"),

	SH("Schleswig-Holstein"),

	CH("Schweiz"),

	TL("Thailand"),

	TH("Thüringen");

	private final String bezeichnung;

	/**
	 * @param pBezeichnung
	 */
	private BUNDESLAENDER(String pBezeichnung) {
		bezeichnung = pBezeichnung;
	}

	/**
	 * @return the bezeichnung
	 */
	public final String getBezeichnung() {
		return bezeichnung;
	}

	/**
	 * 
	 * @return
	 */
	public BUNDESLAENDER[] valuesSortedByBezeichnung() {
		return new BUNDESLAENDER[] { BW, BY, BE, BB, HB, HH, HE, MV, NI, NW, AU, RP, SL, SN, ST, SH, CH, TH };
	}

}
