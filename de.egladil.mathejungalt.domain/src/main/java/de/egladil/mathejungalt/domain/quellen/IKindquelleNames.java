/**
 * 
 */
package de.egladil.mathejungalt.domain.quellen;

/**
 * This generated interface defines constants for all attributes and associatations in
 * {@link de.egladil.mathe.domain.quelle.Buchquelle}.
 * <p>
 * These constants are useful for example when building criterias.
 * 
 * @author heike
 */
public interface IKindquelleNames extends IQuelleNames {

	public static final String SQL_JAHRE = "jahre";

	public static final String SQL_KLASSE = "klasse";

	public static final String SQL_MITGLIED = "mitglied";

	public static final String PROP_JAHRE = "jahre";

	public static final String PROP_KLASSE = "klasse";

	public static final String PROP_MITGLIED = "mitglied";
}
