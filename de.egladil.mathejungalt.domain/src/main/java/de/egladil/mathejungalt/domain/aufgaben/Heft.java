/**
 * 
 */
package de.egladil.mathejungalt.domain.aufgaben;

import java.util.ArrayList;
import java.util.List;

import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.IMatheAGObjectNames;

/**
 * @author heike
 */
public class Heft extends AbstractMatheAGObject {

	/** */
	private static final long serialVersionUID = -2129428857517509846L;

	/** */
	private int beendet;

	/** */
	private String titel;

	/** */
	private String schluessel;

	/** */
	private List<Aufgabe> aufgaben;

	/** */
	private boolean aufgabenGeladen = false;

	/**
	 * 
	 */
	public Heft() {
		super();
		beendet = 0;
		titel = "";
		aufgabenGeladen = false;
		aufgaben = new ArrayList<Aufgabe>();
	}

	/**
	 * F�gt gegebene Aufgabe dem Heft hinzu.
	 * 
	 * @param pAufgabe
	 * @return boolean true, falls etwas hinzugef�gt wurde, false sonst.
	 */
	public boolean addAufgabe(final Aufgabe pAufgabe) {
		if (aufgaben.contains(pAufgabe))
			return false;
		boolean added = aufgaben.add(pAufgabe);
		pAufgabe.setHeft(this);
		fireAufgabenChanged();
		return added;
	}

	/**
	 * 
	 */
	private void fireAufgabenChanged() {
		if (aufgabenGeladen)
			firePropertyChange(IHeftNames.PROP_AUFGABEN, null, aufgaben);
	}

	/**
	 * F�gt gegebene Aufgabe dem Heft hinzu.
	 * 
	 * @param pAufgabe
	 * @return boolean true, falls etwas hinzugef�gt wurde, false sonst.
	 */
	public boolean removeAufgabe(final Aufgabe pAufgabe) {
		if (!aufgaben.contains(pAufgabe))
			return false;
		aufgaben.remove(pAufgabe);
		pAufgabe.setHeft(null);
		fireAufgabenChanged();
		return true;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return schluessel;
	}

	/**
	 * @return the titel
	 */
	public String getTitel() {
		return titel;
	}

	/**
	 * @param pTitel the titel to set
	 */
	public void setTitel(String pTitel) {
		firePropertyChange(IHeftNames.PROP_TITEL, titel, titel = pTitel);
	}

	/**
	 * @return
	 */
	public int anzahlAufgaben() {
		if (!aufgabenGeladen)
			return 0;
		return aufgaben.size();
	}

	/**
	 * @return the beendet
	 */
	public int getBeendet() {
		if (aufgaben == null)
			return 0;
		return beendet;
	}

	/**
	 * @param pBeendet the beendet to set
	 */
	public void setBeendet(int pBeendet) {
		firePropertyChange(IHeftNames.PROP_BEENDET, beendet, beendet = pBeendet);
	}

	/**
	 * @return the schluessel
	 */
	public String getSchluessel() {
		return schluessel;
	}

	/**
	 * @param pSchluessel the schluessel to set
	 */
	public void setSchluessel(String pSchluessel) {
		firePropertyChange(IMatheAGObjectNames.PROP_SCHLUESSEL, schluessel, schluessel = pSchluessel);
	}

	/**
	 * @return the aufgabenGeladen
	 */
	public boolean isAufgabenGeladen() {
		return aufgabenGeladen;
	}

	/**
	 * @param pAufgabenGeladen the aufgabenGeladen to set
	 */
	public void setAufgabenGeladen(boolean pAufgabenGeladen) {
		aufgabenGeladen = pAufgabenGeladen;
	}

	/**
	 * @return the aufgaben
	 */
	public List<Aufgabe> getAufgaben() {
		return aufgaben;
	}

}
