/**
 *
 */
package de.egladil.mathejungalt.domain.mcraetsel;

/**
 * @author heike
 */
public enum MCAufgabenarten {
	E("Eigenbau"),

	N("Nachbau"),

	Z("Zitat");

	private final String label;

	/**
	 * @param pLabel
	 */
	private MCAufgabenarten(String pLabel) {
		label = pLabel;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param pLabel
	 * @return
	 */
	public static MCAufgabenarten valueOfLabel(String pLabel) {
		for (MCAufgabenarten thema : values()) {
			if (thema.getLabel().equals(pLabel)) {
				return thema;
			}
		}
		return null;
	}

}
