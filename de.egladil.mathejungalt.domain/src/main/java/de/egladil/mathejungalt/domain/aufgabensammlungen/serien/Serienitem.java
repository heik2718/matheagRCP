/**
 *
 */
package de.egladil.mathejungalt.domain.aufgabensammlungen.serien;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltException;
import de.egladil.mathejungalt.domain.AbstractMatheAGObject;
import de.egladil.mathejungalt.domain.aufgaben.Aufgabe;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlung;
import de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem;
import de.egladil.mathejungalt.domain.types.AufgabeSerieComposedKey;
import de.egladil.mathejungalt.domain.types.EnumTypes.Aufgabenzweck;

/**
 * @author heike
 */
public class Serienitem extends AbstractMatheAGObject implements IAufgabensammlungItem {

	/** */
	private static final long serialVersionUID = 8097421841532651903L;

	/** */
	private static final Logger LOG = LoggerFactory.getLogger(Serienitem.class);

	/** */
	private String nummer = "";

	/** */
	private Aufgabe aufgabe;

	/** */
	private Serie serie;

	/**
	 *
	 */
	public Serienitem() {
		super();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(nummer);
		sb.append("-");
		sb.append(serie.getKey());
		sb.append("-");
		sb.append(aufgabe.getKey());
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.domain.AbstractMatheAGObject#getKey()
	 */
	@Override
	public Object getKey() {
		if (aufgabe == null && serie == null)
			return getId();
		return new AufgabeSerieComposedKey(aufgabe, serie);
	}

	/**
	 * @return the nummer
	 */
	public String getNummer() {
		return nummer;
	}

	/**
	 * @param pNummer the nummer to set
	 */
	public void setNummer(String pNummer) {
		firePropertyChange(IAufgabensammlungItem.PROP_NUMMER, nummer, nummer = pNummer);
	}

	/**
	 * @return
	 */
	public Aufgabe getAufgabe() {
		return aufgabe;
	}

	/**
	 * @param pAufgabe the aufgabe to set
	 */
	public void setAufgabe(Aufgabe pAufgabe) {
		if (!Aufgabenzweck.S.equals(pAufgabe.getZweck())) {
			String msg = "Nur Serienaufgaben duefen zu einer Serie hinzugefuegt werden!";
			LOG.error(msg);
			throw new MatheJungAltException(msg);
		}
		firePropertyChange(IAufgabensammlungItem.PROP_AUFGABE, aufgabe, aufgabe = pAufgabe);
	}

	/**
	 * @return the serie
	 */
	public Serie getSerie() {
		return serie;
	}

	/**
	 * @param pSerie the serie to set
	 */
	public void setSerie(Serie pSerie) {
		firePropertyChange(ISerienitemNames.PROP_SERIE, serie, serie = pSerie);
	}

	/**
	 * Gibt die Klassenstufe fuer diese Aufgabe zurueck.
	 *
	 * @return
	 */
	public int getStufe() {
		if (aufgabe != null)
			return aufgabe.getStufe();
		return 0;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.egladil.mathejungalt.domain.aufgabensammlungen.IAufgabensammlungItem#getAufgabensammlung()
	 */
	@Override
	public IAufgabensammlung getAufgabensammlung() {
		return serie;
	}


}
