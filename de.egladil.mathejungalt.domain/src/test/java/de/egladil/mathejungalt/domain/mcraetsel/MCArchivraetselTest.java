/**
 *
 */
package de.egladil.mathejungalt.domain.mcraetsel;

import junit.framework.TestCase;

import org.eclipse.core.runtime.AssertionFailedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.mathejungalt.domain.types.MCAufgabenstufen;

/**
 * @author heike
 */
public class MCArchivraetselTest extends TestCase {

	private static final Logger LOG = LoggerFactory.getLogger(MCArchivraetselTest.class);

	public void testAddItemInvalidStufe() {
		MCArchivraetsel raetsel = new MCArchivraetsel();
		raetsel.setStufe(MCAufgabenstufen.STUFE_78);

		MCAufgabe aufgabe = new MCAufgabe();
		aufgabe.setStufe(MCAufgabenstufen.STUFE_56);

		MCArchivraetselItem item = new MCArchivraetselItem();
		item.setAufgabe(aufgabe);
		item.setNummer("3-5");
		item.setId(1l);

		try {
			raetsel.addItem(item);
			fail("keine AssertionFailedException");
		} catch (AssertionFailedException e) {
			LOG.info(e.getMessage());
		}
	}

	public void testAddItemNull() {
		MCArchivraetsel raetsel = new MCArchivraetsel();
		raetsel.setStufe(MCAufgabenstufen.STUFE_78);

		MCAufgabe aufgabe = new MCAufgabe();
		aufgabe.setStufe(MCAufgabenstufen.STUFE_56);

		try {
			raetsel.addItem(null);
			fail("keine AssertionFailedException");
		} catch (AssertionFailedException e) {
			LOG.info(e.getMessage());
		}
	}

	public void testAddItemAufgabeNull() {
		MCArchivraetsel raetsel = new MCArchivraetsel();
		raetsel.setStufe(MCAufgabenstufen.STUFE_78);

		MCAufgabe aufgabe = new MCAufgabe();
		aufgabe.setStufe(MCAufgabenstufen.STUFE_56);

		MCArchivraetselItem item = new MCArchivraetselItem();
		item.setNummer("3-5");
		item.setId(1l);

		try {
			raetsel.addItem(item);
			fail("keine AssertionFailedException");
		} catch (AssertionFailedException e) {
			LOG.info(e.getMessage());
		}
	}

	public void testAddItemIncompleteInitialization() {
		MCArchivraetsel raetsel = new MCArchivraetsel();

		MCAufgabe aufgabe = new MCAufgabe();
		aufgabe.setStufe(MCAufgabenstufen.STUFE_56);

		MCArchivraetselItem item = new MCArchivraetselItem();
		item.setAufgabe(aufgabe);
		item.setNummer("3-5");
		item.setId(1l);

		try {
			raetsel.addItem(item);
			fail("keine AssertionFailedException");
		} catch (AssertionFailedException e) {
			LOG.info(e.getMessage());
		}
	}

	public void testAddItemSuccess() {
		MCArchivraetsel raetsel = new MCArchivraetsel();
		raetsel.setStufe(MCAufgabenstufen.STUFE_56);

		MCAufgabe aufgabe = new MCAufgabe();
		aufgabe.setStufe(MCAufgabenstufen.STUFE_56);

		MCArchivraetselItem item = new MCArchivraetselItem();
		item.setAufgabe(aufgabe);
		item.setNummer("3-5");
		item.setId(1l);

		assertEquals(0, raetsel.size());
		raetsel.addItem(item);
		assertEquals(1, raetsel.size());
		assertTrue(raetsel.getItems().contains(item));
		raetsel.removeItem(new MCArchivraetselItem());
		assertEquals(1, raetsel.size());
		raetsel.removeItem(item);
		assertEquals(0, raetsel.size());
	}
}
