/**
 *
 */
package de.egladil.mathejungalt.domain.mcraetsel;

import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import junit.framework.TestCase;

import org.apache.commons.io.IOUtils;

import de.egladil.mathejungalt.domain.JaxbContextProvider;
import de.egladil.mathejungalt.domain.types.MCAufgabenstufen;

/**
 * @author heike
 */
public class MCArchivraetselAnnotationTest extends TestCase {

	/*
	 * (non-Javadoc)
	 * 
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	public void testMarshall() {
		MCAufgabe aufgabe = new MCAufgabe();
		aufgabe.setId(1l);
		aufgabe.setSchluessel("00001");
		aufgabe.setStufe(MCAufgabenstufen.STUFE_34);
		aufgabe.setThema(MCThemen.ARITHMETIK);
		aufgabe.setArt(MCAufgabenarten.E);
		aufgabe.setAntwortA("erste");
		aufgabe.setAntwortB("zweite");
		aufgabe.setAntwortC("dritte");
		aufgabe.setAntwortD("vierte");
		aufgabe.setAntwortE("fünfte");
		aufgabe.setTitel("Apfelstücke");
		aufgabe.setBemerkung("Bemrkung zur Aufgabe 1");
		aufgabe.setQuelle(MCAufgabe.DEFAULT_QUELLE);
		aufgabe.setLoesungsbuchstabe(MCAntwortbuchstaben.D);
		aufgabe.setPunkte(4);
		aufgabe.setVerzeichnis("001");

		MCArchivraetsel raetsel = new MCArchivraetsel();
		raetsel.setTitel("Rätsel 1 (Klassen 3 und 4)");
		raetsel.setSchluessel("00001");
		raetsel.setStufe(MCAufgabenstufen.STUFE_34);
		raetsel.setId(1l);

		for (int i = 1; i < 10; i++) {
			MCArchivraetselItem item = new MCArchivraetselItem();
			item.setAufgabe(aufgabe);
			item.setNummer("" + i);
			raetsel.addItem(item);
		}

		Marshaller marshaller = JaxbContextProvider.getMarshaller();
		StringWriter writer = new StringWriter();

		try {
			marshaller.marshal(raetsel, writer);
			JaxbContextProvider.print(writer, null);
		} catch (JAXBException e) {
			fail(e.getMessage());
		} finally {
			IOUtils.closeQuietly(writer);
		}
	}

	public void _testUnmarshall() {
		InputStream in = null;
		in = getClass().getResourceAsStream("/mcaufgabe.xml");
		assertNotNull(in);

		Unmarshaller unmarshaller = JaxbContextProvider.getUnmarshaller();
		try {
			Object obj = unmarshaller.unmarshal(in);
			assertTrue(obj instanceof MCAufgabe);
			MCAufgabe aufgabe = (MCAufgabe) obj;
			assertEquals(Long.valueOf(1l), aufgabe.getId());
			assertEquals("00001", aufgabe.getSchluessel());
			assertEquals(MCAufgabenstufen.STUFE_34, aufgabe.getStufe());
			assertEquals(MCThemen.ARITHMETIK, aufgabe.getThema());
			assertEquals(MCAufgabenarten.E, aufgabe.getArt());
			assertEquals("erste", aufgabe.getAntwortA());
			assertEquals("zweite", aufgabe.getAntwortB());
			assertEquals("dritte", aufgabe.getAntwortC());
			assertEquals("vierte", aufgabe.getAntwortD());
			assertEquals("fünfte", aufgabe.getAntwortE());
			assertEquals(MCAufgabe.DEFAULT_QUELLE, aufgabe.getQuelle());
			assertEquals(MCAntwortbuchstaben.D, aufgabe.getLoesungsbuchstabe());
		} catch (JAXBException e) {
			fail(e.getMessage());
		} finally {
			IOUtils.closeQuietly(in);

		}
	}
}
