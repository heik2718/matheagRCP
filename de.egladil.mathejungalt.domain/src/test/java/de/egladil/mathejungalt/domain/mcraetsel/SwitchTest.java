/*******************************************************************************
 * Copyright (c) 2014- Heike Winkelvoß.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors :
 *    Heike Winkelvoß (public@egladil.de) - initial API and implementation
 *******************************************************************************/
package de.egladil.mathejungalt.domain.mcraetsel;

import org.junit.Test;

/**
 * <p>
 * NOTE: THIS CLASS AND ITS API ARE STILL UNDER DEVELOPMENT.
 * </p>
 *
 * @author Heike Winkelvoß (public@egladil.de)
 */
public class SwitchTest {

	@Test
	public void test5() {
		System.out.println("SwitchTest.test5()");
		int anzahl = 5;
		System.out.println("(A)");
		System.out.println("(B)");
		switch (anzahl) {
		case 3:
			System.out.println("(C)");
			break;
		case 4:
			System.out.println("(C)");
			System.out.println("(D)");
			break;
		case 5:
			System.out.println("(C)");
			System.out.println("(D)");
			System.out.println("(E)");
			break;
		default:
			break;
		}

	}

	@Test
	public void test4() {
		System.out.println("SwitchTest.test4()");
		int anzahl = 4;
		System.out.println("(A)");
		System.out.println("(B)");
		switch (anzahl) {
		case 3:
			System.out.println("(C)");
			break;
		case 4:
			System.out.println("(C)");
			System.out.println("(D)");
			break;
		case 5:
			System.out.println("(C)");
			System.out.println("(D)");
			System.out.println("(E)");
			break;
		default:
			break;
		}

	}

	@Test
	public void test3() {
		System.out.println("SwitchTest.test3()");
		int anzahl = 3;
		System.out.println("(A)");
		System.out.println("(B)");
		switch (anzahl) {
		case 3:
			System.out.println("(C)");
			break;
		case 4:
			System.out.println("(C)");
			System.out.println("(D)");
			break;
		case 5:
			System.out.println("(C)");
			System.out.println("(D)");
			System.out.println("(E)");
			break;
		default:
			break;
		}

	}

	@Test
	public void test2() {
		System.out.println("SwitchTest.test2()");
		int anzahl = 2;
		System.out.println("(A)");
		System.out.println("(B)");
		switch (anzahl) {
		case 3:
			System.out.println("(C)");
			break;
		case 4:
			System.out.println("(C)");
			System.out.println("(D)");
			break;
		case 5:
			System.out.println("(C)");
			System.out.println("(D)");
			System.out.println("(E)");
			break;
		default:
			break;
		}

	}
}
