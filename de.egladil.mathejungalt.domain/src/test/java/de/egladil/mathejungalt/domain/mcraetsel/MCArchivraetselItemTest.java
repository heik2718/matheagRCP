/**
 *
 */
package de.egladil.mathejungalt.domain.mcraetsel;

import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import junit.framework.TestCase;

import org.apache.commons.io.IOUtils;

import de.egladil.mathejungalt.domain.JaxbContextProvider;
import de.egladil.mathejungalt.domain.types.MCAufgabenstufen;

/**
 * @author heike
 */
public class MCArchivraetselItemTest extends TestCase {

	/*
	 * (non-Javadoc)
	 * 
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	public void testMarshall() {
		MCAufgabe aufgabe = new MCAufgabe();
		aufgabe.setId(1l);
		aufgabe.setSchluessel("00001");
		aufgabe.setStufe(MCAufgabenstufen.STUFE_34);
		aufgabe.setThema(MCThemen.ARITHMETIK);
		aufgabe.setArt(MCAufgabenarten.E);
		aufgabe.setAnzahlAntwortvorschlaege(5);
		aufgabe.setAntwortA("Anton");
		aufgabe.setAntwortB("Beate");
		aufgabe.setAntwortC("Carola");
		aufgabe.setAntwortD("Dennis");
		aufgabe.setAntwortE("Erik");
		aufgabe.setTitel("Apfelstücke");
		aufgabe.setBemerkung("Bemrkung zur Aufgabe 1");
		aufgabe.setQuelle(MCAufgabe.DEFAULT_QUELLE);
		aufgabe.setLoesungsbuchstabe(MCAntwortbuchstaben.E);
		aufgabe.setPunkte(4);
		aufgabe.setVerzeichnis("001");

		MCArchivraetselItem item = new MCArchivraetselItem();
		item.setNummer("3");
		item.setAufgabe(aufgabe);

		Marshaller marshaller = JaxbContextProvider.getMarshaller();
		StringWriter writer = new StringWriter();

		try {
			marshaller.marshal(item, writer);
			JaxbContextProvider.print(writer, null);
		} catch (JAXBException e) {
			fail(e.getMessage());
		} finally {
			IOUtils.closeQuietly(writer);
		}
	}

	public void testUnmarshall() {
		InputStream in = null;
		in = getClass().getResourceAsStream("/mcRaetselItem.xml");
		assertNotNull(in);

		Unmarshaller unmarshaller = JaxbContextProvider.getUnmarshaller();
		try {
			Object obj = unmarshaller.unmarshal(in);
			assertTrue(obj instanceof MCArchivraetselItem);
			MCArchivraetselItem item = (MCArchivraetselItem) obj;
			assertEquals("3", item.getNummer());

			MCAufgabe aufgabe = item.getAufgabe();
			assertEquals(Long.valueOf(1l), aufgabe.getId());
			assertEquals("00001", aufgabe.getSchluessel());
			assertEquals(MCAufgabenstufen.STUFE_34, aufgabe.getStufe());
			assertEquals(MCThemen.ARITHMETIK, aufgabe.getThema());
			assertEquals(MCAufgabenarten.E, aufgabe.getArt());
			assertEquals("Anton", aufgabe.getAntwortA());
			assertEquals("Beate", aufgabe.getAntwortB());
			assertEquals("Carola", aufgabe.getAntwortC());
			assertEquals("Dennis", aufgabe.getAntwortD());
			assertEquals("Erik", aufgabe.getAntwortE());
			assertEquals(MCAufgabe.DEFAULT_QUELLE, aufgabe.getQuelle());
			assertEquals(MCAntwortbuchstaben.E, aufgabe.getLoesungsbuchstabe());
		} catch (JAXBException e) {
			fail(e.getMessage());
		} finally {
			IOUtils.closeQuietly(in);

		}
	}
}
