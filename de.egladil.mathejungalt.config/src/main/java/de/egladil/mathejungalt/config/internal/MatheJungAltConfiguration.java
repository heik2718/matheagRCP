/**
 *
 */
package de.egladil.mathejungalt.config.internal;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.AssertionFailedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.egladil.base.exceptions.MatheJungAltConfigurationException;
import de.egladil.mathejungalt.config.IMatheJungAltConfiguration;

/**
 * @author heike
 */
public class MatheJungAltConfiguration implements IMatheJungAltConfiguration {

	private static final Logger LOG = LoggerFactory.getLogger(MatheJungAltConfiguration.class);

	/** */
	private static final String PROP_FILE = "/matheagRCP.properties";

	/** */
	private Properties properties = null;

	/**
   *
   */
	public MatheJungAltConfiguration() {
		init();
	}

	/**
  *
  */
	private void init() {
		InputStream in = null;
		final String propertiesFile = PROP_FILE;
		try {
			in = getClass().getResourceAsStream(propertiesFile);
			if (in == null) {
				String msg = "Konfigurationsdatei '" + propertiesFile + "' wurde nicht gefunden.";
				LOG.error(msg);
				throw new MatheJungAltConfigurationException(msg);
			}
			properties = new Properties();
			properties.load(in);
			LOG.info(PROP_FILE + " geladen");
		} catch (IOException e) {
			String msg = "IOException beim Lesen der Konfigurationsdatei " + propertiesFile + " ("
				+ e.getLocalizedMessage() + ")";
			LOG.error(msg);
			throw new MatheJungAltConfigurationException(msg, e);
		} finally {
			IOUtils.closeQuietly(in);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.config.IMatheJungAltConfiguration#getConfigValue(java.lang.String)
	 */
	@Override
	public String getConfigValue(String pKey) throws MatheJungAltConfigurationException, AssertionFailedException {
		Assert.isTrue(pKey != null && !pKey.isEmpty(), "Parameter pKey darf nicht null oder leer sein");
		String value = properties.getProperty(pKey);
		if (value == null) {
			String msg = "Der Wert zum Schlüssel '" + pKey + "' ist nicht konfiguriert.\nBitte prüfen Sie die Datei "
				+ PROP_FILE;
			LOG.error(msg);
			throw new MatheJungAltConfigurationException(msg);
		}
		if (LOG.isDebugEnabled()) {
			LOG.debug("[pKey=" + pKey + ",value=" + value + "]");
		}
		return value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.egladil.mathejungalt.config.IMatheJungAltConfiguration#setProperty(java.lang.String, java.lang.String)
	 */
	@Override
	public void setProperty(String pKey, String pValue) {
		Assert.isTrue(pKey != null && !pKey.isEmpty(), "Parameter pKey darf nicht null oder leer sein");
		Assert.isTrue(pValue != null && !pValue.isEmpty(), "Parameter pValue darf nicht null oder leer sein");
		properties.put(pKey, pValue);
	}
}
