/**
 *
 */
package de.egladil.mathejungalt.config;

import org.eclipse.core.runtime.AssertionFailedException;

import de.egladil.base.exceptions.MatheJungAltConfigurationException;

/**
 * @author heike
 */
public interface IMatheJungAltConfiguration {

	public enum RaetsellisteGeneratorModus {
		ANDROID,
		JAVASCRIPT,
		RCP,
		WEB
	};

	final String KEY_EXTERNAL_PROGRAMS_TEMPDIR = "programs.tempdir";

	final String KEY_LATEX_EXE = "program.latex";

	final String KEY_LATEX_INTERACTIONMODE = "programm.latex.interaction";

	final String KEY_DVIPS_EXE = "program.dvips";

	/**
	 * Liest Konfigurationswert aus der Konfiguration.
	 *
	 * @param pKey String der Key darf nicht null sein.
	 * @return String den Wert der Property. Dieser Wert ist nie null. Wenn es keinen Wert zum gegebenen Schlüssel gibt,
	 * wird eine MatheJungAltConfigurationException geworfen.
	 * @throws MatheJungAltConfigurationException , falls der Wert zum gegebenen Schlüssel nicht gesetzt ist.
	 * @throws AssertionFailedException , falls pKey null ist.
	 */
	String getConfigValue(String pKey) throws MatheJungAltConfigurationException, AssertionFailedException;

	/**
	 * Fügt eine neue Property hinzu.
	 *
	 * @param key
	 * @param value
	 */
	void setProperty(String key, String value);

}